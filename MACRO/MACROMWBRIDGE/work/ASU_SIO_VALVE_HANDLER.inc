      Interface
      Subroutine ASU_SIO_VALVE_HANDLER(ext_deltat,R_e,R_i
     &,R_o,R_u,I_id,L_ek,I_ik,I_al,L_ol,L_ul,I_um,L_ap,I_up
     &,I_ar,I_ir,I_or,L_at,L_ut,R8_ov,I_obe,I_ide,R_ode,R_ude
     &,R_afe,R_efe)
C |R_e           |4 4 K|_lcmpJ389       |[]�������� ������ �����������|0.000001|
C |R_i           |4 4 K|_lcmpJ388       |[]�������� ������ �����������|0.99999|
C |R_o           |4 4 K|_lcmpJ387       |[]�������� ������ �����������|0.99999|
C |R_u           |4 4 K|_lcmpJ386       |[]�������� ������ �����������|0.000001|
C |I_id          |2 4 O|XH54            |�������||
C |L_ek          |1 1 S|_qffJ309*       |�������� ������ Q RS-��������  |F|
C |I_ik          |2 4 O|XA41            |���������� ������ ���||
C |I_al          |2 4 I|AUTO_HAND       |����/����||
C |L_ol          |1 1 S|_splsJ278*      |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 S|_splsJ273*      |[TF]���������� ��������� ������������� |F|
C |I_um          |2 4 I|USER_STOP       |������ ����. ������� �� ���������||
C |L_ap          |1 1 I|mlf01           |��� �������||
C |I_up          |2 4 I|USER_CLOSE      |������� �������. ������� �� ���������||
C |I_ar          |2 4 I|YH22            |������� �������. ������� �� ����������||
C |I_ir          |2 4 I|USER_OPEN       |������� �������. ������� �� ���������||
C |I_or          |2 4 I|YH21            |������� �������. ������� �� ����������||
C |L_at          |1 1 S|_qffJ217*       |�������� ������ Q RS-��������  |F|
C |L_ut          |1 1 S|_qffJ216*       |�������� ������ Q RS-��������  |F|
C |R8_ov         |4 8 O|POS             |��������� ��������||
C |I_obe         |2 4 O|XH52            |�������� �������||
C |I_ide         |2 4 O|XH51            |�������� �������||
C |R_ode         |4 4 K|_uintJ2         |����������� ������ ����������� ������|1.0|
C |R_ude         |4 4 K|_tintJ2         |[���]�������� T �����������|20.0|
C |R_afe         |4 4 O|_ointJ2*        |�������� ������ ����������� |0.0|
C |R_efe         |4 4 K|_lintJ2         |����������� ������ ����������� �����|0.0|

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i,R_o,R_u
      INTEGER*4 I_id
      LOGICAL*1 L_ek
      INTEGER*4 I_ik,I_al
      LOGICAL*1 L_ol,L_ul
      INTEGER*4 I_um
      LOGICAL*1 L_ap
      INTEGER*4 I_up,I_ar,I_ir,I_or
      LOGICAL*1 L_at,L_ut
      REAL*8 R8_ov
      INTEGER*4 I_obe,I_ide
      REAL*4 R_ode,R_ude,R_afe,R_efe
      End subroutine ASU_SIO_VALVE_HANDLER
      End interface
