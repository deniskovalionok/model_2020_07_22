      Interface
      Subroutine ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION(ext_deltat
     &,I_o,L_if,I_ek,L_uk,L_al,I_am,L_em,I_ap,I_ep,I_op,I_up
     &,L_es,L_at,R8_ut,R_uv,R_ex,I_abe,R_ibe,I_ade,R_ide,R_ode
     &,R_ude,R_afe,R_efe)
C |I_o           |2 4 O|XH54            |�������||
C |L_if          |1 1 S|_qffJ309*       |�������� ������ Q RS-��������  |F|
C |I_ek          |2 4 I|AUTO_HAND       |����/����||
C |L_uk          |1 1 S|_splsJ278*      |[TF]���������� ��������� ������������� |F|
C |L_al          |1 1 S|_splsJ273*      |[TF]���������� ��������� ������������� |F|
C |I_am          |2 4 I|USER_STOP       |������ ����. ������� �� ���������||
C |L_em          |1 1 I|mlf01           |��� �������||
C |I_ap          |2 4 I|USER_CLOSE      |������� �������. ������� �� ���������||
C |I_ep          |2 4 I|YH22            |������� �������. ������� �� ����������||
C |I_op          |2 4 I|USER_OPEN       |������� �������. ������� �� ���������||
C |I_up          |2 4 I|YH21            |������� �������. ������� �� ����������||
C |L_es          |1 1 S|_qffJ217*       |�������� ������ Q RS-��������  |F|
C |L_at          |1 1 S|_qffJ216*       |�������� ������ Q RS-��������  |F|
C |R8_ut         |4 8 O|POS             |��������� ��������||
C |R_uv          |4 4 K|_lcmpJ49        |[]�������� ������ �����������|0.99999|
C |R_ex          |4 4 K|_lcmpJ48        |[]�������� ������ �����������|0.000001|
C |I_abe         |2 4 O|XH52            |�������� �������||
C |R_ibe         |4 4 K|_lcmpJ22        |[]�������� ������ �����������|0.000001|
C |I_ade         |2 4 O|XH51            |�������� �������||
C |R_ide         |4 4 K|_lcmpJ19        |[]�������� ������ �����������|0.99999|
C |R_ode         |4 4 K|_uintJ2         |����������� ������ ����������� ������|1.0|
C |R_ude         |4 4 K|_tintJ2         |[���]�������� T �����������|20.0|
C |R_afe         |4 4 O|_ointJ2*        |�������� ������ ����������� |0.0|
C |R_efe         |4 4 K|_lintJ2         |����������� ������ ����������� �����|0.0|

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_o
      LOGICAL*1 L_if
      INTEGER*4 I_ek
      LOGICAL*1 L_uk,L_al
      INTEGER*4 I_am
      LOGICAL*1 L_em
      INTEGER*4 I_ap,I_ep,I_op,I_up
      LOGICAL*1 L_es,L_at
      REAL*8 R8_ut
      REAL*4 R_uv,R_ex
      INTEGER*4 I_abe
      REAL*4 R_ibe
      INTEGER*4 I_ade
      REAL*4 R_ide,R_ode,R_ude,R_afe,R_efe
      End subroutine ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION
      End interface
