      Subroutine ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION
     &(ext_deltat,L_id,I_ef,L_uf,L_ak,I_al,I_ul,I_am,I_im
     &,I_om,L_ar,L_ur,R8_os,R_ot,R_av,I_uv,R_ex,I_ux,R_ebe
     &,R_ibe,R_obe,R_ube,R_ade)
C |L_id          |1 1 S|_qffJ590*       |�������� ������ Q RS-��������  |F|
C |I_ef          |2 4 I|AUTO_HAND       |����/����||
C |L_uf          |1 1 S|_splsJ571*      |[TF]���������� ��������� ������������� |F|
C |L_ak          |1 1 S|_splsJ566*      |[TF]���������� ��������� ������������� |F|
C |I_al          |2 4 I|USER_STOP       |������ ����. ������� �� ���������||
C |I_ul          |2 4 I|USER_CLOSE      |������� �������. ������� �� ���������||
C |I_am          |2 4 I|YH22            |������� �������. ������� �� ����������||
C |I_im          |2 4 I|USER_OPEN       |������� �������. ������� �� ���������||
C |I_om          |2 4 I|YH21            |������� �������. ������� �� ����������||
C |L_ar          |1 1 S|_qffJ544*       |�������� ������ Q RS-��������  |F|
C |L_ur          |1 1 S|_qffJ543*       |�������� ������ Q RS-��������  |F|
C |R8_os         |4 8 O|POS             |��������� ��������||
C |R_ot          |4 4 K|_lcmpJ519       |[]�������� ������ �����������|0.999999|
C |R_av          |4 4 K|_lcmpJ518       |[]�������� ������ �����������|0.000001|
C |I_uv          |2 4 O|XH52            |�������� �������||
C |R_ex          |4 4 K|_lcmpJ503       |[]�������� ������ �����������|0.000001|
C |I_ux          |2 4 O|XH51            |�������� �������||
C |R_ebe         |4 4 K|_lcmpJ500       |[]�������� ������ �����������|0.999999|
C |R_ibe         |4 4 K|_uintJ499       |����������� ������ ����������� ������|1.0|
C |R_obe         |4 4 K|_tintJ499       |[���]�������� T �����������|20.0|
C |R_ube         |4 4 O|_ointJ499*      |�������� ������ ����������� |0.0|
C |R_ade         |4 4 K|_lintJ499       |����������� ������ ����������� �����|0.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e,L0_i,L0_o,L0_u,L0_ad,L0_ed,L_id
      INTEGER*4 I0_od,I0_ud,I0_af,I_ef
      LOGICAL*1 L0_if,L0_of,L_uf,L_ak,L0_ek,L0_ik,L0_ok,L0_uk
      INTEGER*4 I_al
      LOGICAL*1 L0_el,L0_il,L0_ol
      INTEGER*4 I_ul,I_am
      LOGICAL*1 L0_em
      INTEGER*4 I_im,I_om
      LOGICAL*1 L0_um,L0_ap,L0_ep,L0_ip,L0_op,L0_up,L_ar
      REAL*4 R0_er,R0_ir,R0_or
      LOGICAL*1 L_ur
      REAL*4 R0_as,R0_es,R0_is
      REAL*8 R8_os
      LOGICAL*1 L0_us
      INTEGER*4 I0_at,I0_et
      LOGICAL*1 L0_it
      REAL*4 R_ot
      LOGICAL*1 L0_ut
      REAL*4 R_av
      LOGICAL*1 L0_ev
      INTEGER*4 I0_iv,I0_ov,I_uv
      LOGICAL*1 L0_ax
      REAL*4 R_ex
      INTEGER*4 I0_ix,I0_ox,I_ux
      LOGICAL*1 L0_abe
      REAL*4 R_ebe,R_ibe,R_obe,R_ube,R_ade,R0_ede

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_e=.false.
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 156, 223):��������� ���������� (�������)
      L0_u=I_ef.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 106, 175):��������� 1->LO
      I0_ud = 0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 133, 183):��������� ������������� IN (�������)
      I0_af = 1
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 133, 185):��������� ������������� IN (�������)
      L0_if=I_am.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 152, 214):��������� 1->LO
      L0_of=I_om.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 152, 247):��������� 1->LO
      L0_uk=I_al.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 152, 221):��������� 1->LO
      L0_ol=I_ul.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 152, 202):��������� 1->LO
      L0_em=I_im.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 152, 234):��������� 1->LO
      R0_or = 0.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 210, 238):��������� (RE4) (�������)
      R0_ir = -1.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 210, 236):��������� (RE4) (�������)
      R0_is = 0.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 210, 251):��������� (RE4) (�������)
      R0_es = 1.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 210, 249):��������� (RE4) (�������)
      I0_et = 0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 262, 262):��������� ������������� IN (�������)
      I0_at = 1
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 262, 260):��������� ������������� IN (�������)
      I0_ov = 0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 262, 244):��������� ������������� IN (�������)
      I0_iv = 1
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 262, 242):��������� ������������� IN (�������)
      I0_ix = 2
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 262, 250):��������� ������������� IN (�������)
      if(L_id) then
         I0_od=I0_ud
      else
         I0_od=I0_af
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 138, 183):���� RE IN LO CH7
C label 33  try33=try33-1
      L0_o=I0_od.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 106, 170):��������� 1->LO
      L0_ad = L0_u.AND.(.NOT.L0_o)
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 117, 171):�
      L_id=(L0_u.or.L_id).and..not.(L0_ad)
      L0_ed=.not.L_id
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 127, 173):RS �������
      L0_i=I0_od.ne.0
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 152, 194):��������� 1->LO
      if(.NOT.L0_i) then
         L0_ip=L0_em
      else
         L0_ip=L0_of
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 161, 243):���� RE IN LO CH7
      if(.NOT.L0_i) then
         L0_um=L0_ol
      else
         L0_um=L0_if
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 161, 212):���� RE IN LO CH7
      if(.NOT.L0_i) then
         L0_el=L0_uk
      else
         L0_el=L0_e
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 161, 221):���� RE IN LO CH7
      L0_ek=L_ar.and..not.L_uf
      L_uf=L_ar
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 183, 238):������������  �� 1 ���
C label 58  try58=try58-1
      if(L_ur) then
         R0_as=R0_es
      else
         R0_as=R0_is
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 214, 250):���� RE IN LO CH7
      if(L_ar) then
         R0_er=R0_ir
      else
         R0_er=R0_or
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 214, 237):���� RE IN LO CH7
      R0_ede = R0_as + R0_er
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 222, 243):��������
      R_ube=R_ube+deltat/R_obe*R0_ede
      if(R_ube.gt.R_ibe) then
         R_ube=R_ibe
      elseif(R_ube.lt.R_ade) then
         R_ube=R_ade
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 233, 241):����������
      L0_ax=R_ube.lt.R_ex
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 254, 239):���������� <
      L0_abe=R_ube.gt.R_ebe
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 254, 247):���������� >
      L0_ev = L0_ax.OR.L0_abe
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 242, 195):���
      L0_il = L0_el.OR.L0_ev
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 175, 230):���
      L0_ik = L_ur.AND.L_ar
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 203, 226):�
      L0_op = L0_il.OR.L0_ek.OR.L0_ik
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 189, 238):���
      L_ur=L0_ip.or.(L_ur.and..not.(L0_op))
      L0_up=.not.L_ur
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 196, 241):RS �������
      L0_ok=L_ur.and..not.L_ak
      L_ak=L_ur
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 183, 209):������������  �� 1 ���
      L0_ap = L0_ok.OR.L0_il.OR.L0_ik
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 189, 207):���
      L_ar=L0_um.or.(L_ar.and..not.(L0_ap))
      L0_ep=.not.L_ar
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 196, 210):RS �������
C sav1=L0_ik
      L0_ik = L_ur.AND.L_ar
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 203, 226):recalc:�
C if(sav1.ne.L0_ik .and. try84.gt.0) goto 84
      if(L0_ax) then
         I_uv=I0_iv
      else
         I_uv=I0_ov
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 265, 242):���� RE IN LO CH7
      R8_os = R_ube
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 256, 273):��������
      L0_it=R_ube.lt.R_ot
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 254, 253):���������� <
      L0_ut=R_ube.gt.R_av
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 254, 259):���������� >
      L0_us = L0_ut.AND.L0_it
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 259, 256):�
      if(L0_us) then
         I0_ox=I0_at
      else
         I0_ox=I0_et
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 265, 260):���� RE IN LO CH7
      if(L0_abe) then
         I_ux=I0_ix
      else
         I_ux=I0_ox
      endif
C ASU_SIO_VALVE_HANDLER_WITHOUT_AUTO_AND_MALFUNCTION.fmg( 265, 250):���� RE IN LO CH7
      End
