      Subroutine SOLENOIDVALVE_MAN(ext_deltat,R_ofi,L_obi
     &,L_ibi,R8_exe,C30_o,L_ed,L_af,L_ef,I_ak,I_ek,R_uk,R_al
     &,R_el,R_il,R_ol,R_ul,I_am,I_om,I_ep,I_ar,L_er,L_ir,L_or
     &,L_ur,L_os,L_us,L_it,L_ot,L_ut,L_ev,L_ade,L_efe,L_ake
     &,L_ike,L_oke,L_ele,L_ile,L_ule,R8_eme,R_ape,R8_ope,L_ove
     &,R_oxe,R_abi,L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki)
C |R_ofi         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |L_obi         |1 1 O|13 cbo          |�� ������� (���)|F|
C |L_ibi         |1 1 O|14 cbc          |�� ������� (���)|F|
C |R8_exe        |4 8 O|16 value        |��� �������� ��������|0.5|
C |C30_o         |3 30 O|state           |||
C |L_ed          |1 1 I|tech_mode       |||
C |L_af          |1 1 I|ruch_mode       |||
C |L_ef          |1 1 I|avt_mode        |||
C |I_ak          |2 4 O|LM              |�����||
C |I_ek          |2 4 O|LF              |����� �������������||
C |R_uk          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_al          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_el          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_il          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ol          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ul          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_am          |2 4 O|LST             |����� ����||
C |I_om          |2 4 O|LCL             |����� �������||
C |I_ep          |2 4 O|LOP             |����� �������||
C |I_ar          |2 4 O|LS              |��������� �������||
C |L_er          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ir          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_or          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ur          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_os          |1 1 O|block           |||
C |L_us          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_it          |1 1 O|STOP            |�������||
C |L_ot          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ut          |1 1 I|uluclose        |������� ������� �� ����������|F|
C |L_ev          |1 1 I|uluopen         |������� ������� �� ����������|F|
C |L_ade         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_efe         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ake         |1 1 I|vlv_kvit        |||
C |L_ike         |1 1 I|instr_fault     |||
C |L_oke         |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_ele         |1 1 O|fault           |�������������||
C |L_ile         |1 1 O|norm            |�����||
C |L_ule         |1 1 O|nopower         |��� ����������||
C |R8_eme        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ape         |4 4 I|power           |�������� ��������||
C |R8_ope        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ove         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_oxe         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_abi         |4 4 I|tcl_top         |����� ����|30.0|
C |L_ufi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_aki         |1 1 I|mlf23           |������� ������� �����||
C |L_eki         |1 1 I|mlf22           |����� ����� ��������||
C |L_iki         |1 1 I|mlf04           |�������� �� ��������||
C |L_oki         |1 1 I|mlf03           |�������� �� ��������||
C |L_uki         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      CHARACTER*30 C30_e,C30_i,C30_o,C30_u,C30_ad
      LOGICAL*1 L_ed
      INTEGER*4 I0_id,I0_od,I0_ud
      LOGICAL*1 L_af,L_ef
      INTEGER*4 I0_if,I0_of,I0_uf,I_ak,I_ek,I0_ik,I0_ok
      REAL*4 R_uk,R_al,R_el,R_il,R_ol,R_ul
      INTEGER*4 I_am,I0_em,I0_im,I_om,I0_um,I0_ap,I_ep,I0_ip
     &,I0_op,I0_up,I_ar
      LOGICAL*1 L_er,L_ir,L_or,L_ur,L0_as,L0_es,L0_is,L_os
     &,L_us,L0_at,L0_et,L_it,L_ot,L_ut,L0_av,L_ev,L0_iv
      INTEGER*4 I0_ov,I0_uv,I0_ax,I0_ex,I0_ix,I0_ox,I0_ux
     &,I0_abe,I0_ebe
      LOGICAL*1 L0_ibe,L0_obe,L0_ube,L_ade,L0_ede,L0_ide,L0_ode
     &,L0_ude,L0_afe,L_efe,L0_ife,L0_ofe,L0_ufe,L_ake,L0_eke
     &,L_ike,L_oke
      REAL*4 R0_uke
      LOGICAL*1 L0_ale,L_ele,L_ile,L0_ole,L_ule
      REAL*4 R0_ame
      REAL*8 R8_eme
      LOGICAL*1 L0_ime,L0_ome
      REAL*4 R0_ume,R_ape,R0_epe,R0_ipe
      REAL*8 R8_ope
      LOGICAL*1 L0_upe,L0_are
      REAL*4 R0_ere,R0_ire
      LOGICAL*1 L0_ore
      REAL*4 R0_ure,R0_ase,R0_ese,R0_ise,R0_ose,R0_use
      LOGICAL*1 L0_ate,L0_ete,L0_ite
      REAL*4 R0_ote,R0_ute,R0_ave,R0_eve
      LOGICAL*1 L0_ive,L_ove
      REAL*4 R0_uve,R0_axe
      REAL*8 R8_exe
      REAL*4 R0_ixe,R_oxe,R0_uxe,R_abi,R0_ebi
      LOGICAL*1 L_ibi,L_obi
      REAL*4 R0_ubi,R0_adi,R0_edi,R0_idi,R0_odi,R0_udi,R0_afi
      LOGICAL*1 L0_efi,L0_ifi
      REAL*4 R_ofi
      LOGICAL*1 L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      C30_e = '������ ������'
C SOLENOIDVALVE_MAN.fmg( 376, 218):��������� ���������� CH20 (CH30) (�������)
      C30_i = ''
C SOLENOIDVALVE_MAN.fmg( 376, 220):��������� ���������� CH20 (CH30) (�������)
      C30_u = '������ ������'
C SOLENOIDVALVE_MAN.fmg( 394, 218):��������� ���������� CH20 (CH30) (�������)
      I0_id = z'01000010'
C SOLENOIDVALVE_MAN.fmg( 201, 160):��������� ������������� IN (�������)
      I0_of = z'01000022'
C SOLENOIDVALVE_MAN.fmg( 163, 162):��������� ������������� IN (�������)
      I0_ud = z'0100000A'
C SOLENOIDVALVE_MAN.fmg( 185, 161):��������� ������������� IN (�������)
      I0_uf = z'0100000A'
C SOLENOIDVALVE_MAN.fmg( 163, 164):��������� ������������� IN (�������)
      if(L_ef) then
         I0_if=I0_of
      else
         I0_if=I0_uf
      endif
C SOLENOIDVALVE_MAN.fmg( 170, 162):���� RE IN LO CH7
      if(L_af) then
         I0_od=I0_ud
      else
         I0_od=I0_if
      endif
C SOLENOIDVALVE_MAN.fmg( 188, 162):���� RE IN LO CH7
      if(L_ed) then
         I_ak=I0_id
      else
         I_ak=I0_od
      endif
C SOLENOIDVALVE_MAN.fmg( 204, 160):���� RE IN LO CH7
      I0_ik = z'01000007'
C SOLENOIDVALVE_MAN.fmg( 148, 180):��������� ������������� IN (�������)
      I0_ok = z'01000003'
C SOLENOIDVALVE_MAN.fmg( 148, 182):��������� ������������� IN (�������)
      L_us=R_al.ne.R_uk
      R_uk=R_al
C SOLENOIDVALVE_MAN.fmg(  22, 208):���������� ������������� ������
      L_er=R_il.ne.R_el
      R_el=R_il
C SOLENOIDVALVE_MAN.fmg(  21, 194):���������� ������������� ������
      L0_av = (.NOT.L_or).AND.L_er
C SOLENOIDVALVE_MAN.fmg(  65, 194):�
      L0_ide = L0_av.OR.L_ut
C SOLENOIDVALVE_MAN.fmg(  69, 192):���
      L_ir=R_ul.ne.R_ol
      R_ol=R_ul
C SOLENOIDVALVE_MAN.fmg(  22, 238):���������� ������������� ������
      L0_iv = L_ir.AND.(.NOT.L_ur)
C SOLENOIDVALVE_MAN.fmg(  65, 236):�
      L0_ofe = L0_iv.OR.L_ev
C SOLENOIDVALVE_MAN.fmg(  69, 234):���
      L0_at = L0_ofe.OR.L0_ide
C SOLENOIDVALVE_MAN.fmg(  74, 203):���
      L_ot=(L_us.or.L_ot).and..not.(L0_at)
      L0_et=.not.L_ot
C SOLENOIDVALVE_MAN.fmg( 104, 205):RS �������,10
      L_it=L_ot
C SOLENOIDVALVE_MAN.fmg( 132, 207):������,STOP
      L0_ibe = L_ot.OR.L_ufi
C SOLENOIDVALVE_MAN.fmg(  96, 214):���
      I0_em = z'01000007'
C SOLENOIDVALVE_MAN.fmg( 112, 214):��������� ������������� IN (�������)
      I0_im = z'01000003'
C SOLENOIDVALVE_MAN.fmg( 112, 216):��������� ������������� IN (�������)
      if(L_ot) then
         I_am=I0_em
      else
         I_am=I0_im
      endif
C SOLENOIDVALVE_MAN.fmg( 116, 214):���� RE IN LO CH7
      I0_um = z'01000010'
C SOLENOIDVALVE_MAN.fmg( 114, 169):��������� ������������� IN (�������)
      I0_ap = z'01000003'
C SOLENOIDVALVE_MAN.fmg( 114, 171):��������� ������������� IN (�������)
      I0_op = z'01000003'
C SOLENOIDVALVE_MAN.fmg( 120, 265):��������� ������������� IN (�������)
      I0_ip = z'0100000A'
C SOLENOIDVALVE_MAN.fmg( 120, 263):��������� ������������� IN (�������)
      I0_up = z'01000097'
C SOLENOIDVALVE_MAN.fmg( 188, 200):��������� ������������� IN (�������)
      I0_ox = z'0100000A'
C SOLENOIDVALVE_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ix = z'01000010'
C SOLENOIDVALVE_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ov = z'0100000A'
C SOLENOIDVALVE_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_abe = z'01000087'
C SOLENOIDVALVE_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      I0_ax = z'01000086'
C SOLENOIDVALVE_MAN.fmg( 166, 201):��������� ������������� IN (�������)
      L0_is=.false.
C SOLENOIDVALVE_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_es=.false.
C SOLENOIDVALVE_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_as=.false.
C SOLENOIDVALVE_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_os = L0_is.OR.L0_es.OR.L0_as.OR.L_ur.OR.L_or
C SOLENOIDVALVE_MAN.fmg(  67, 213):���
      L_oke=(L_ike.or.L_oke).and..not.(L_ake)
      L0_eke=.not.L_oke
C SOLENOIDVALVE_MAN.fmg( 326, 178):RS �������
      R0_uke = DeltaT
C SOLENOIDVALVE_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_abi.ge.0.0) then
         R0_adi=R0_uke/max(R_abi,1.0e-10)
      else
         R0_adi=R0_uke/min(R_abi,-1.0e-10)
      endif
C SOLENOIDVALVE_MAN.fmg( 259, 252):�������� ����������
      L0_ole =.NOT.(L_oki.OR.L_iki.OR.L_uki.OR.L_eki.OR.L_aki.OR.L_ufi
     &)
C SOLENOIDVALVE_MAN.fmg( 319, 191):���
      L0_ale =.NOT.(L0_ole)
C SOLENOIDVALVE_MAN.fmg( 328, 185):���
      L_ele = L0_ale.OR.L_oke
C SOLENOIDVALVE_MAN.fmg( 332, 184):���
      if(L_ele) then
         I_ek=I0_ik
      else
         I_ek=I0_ok
      endif
C SOLENOIDVALVE_MAN.fmg( 152, 181):���� RE IN LO CH7
      L_ile = L0_ole.OR.L_ike
C SOLENOIDVALVE_MAN.fmg( 328, 190):���
      R0_ame = 0.1
C SOLENOIDVALVE_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_ule=R8_eme.lt.R0_ame
C SOLENOIDVALVE_MAN.fmg( 259, 162):���������� <
      R0_ume = 0.0
C SOLENOIDVALVE_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_ose = 0.000001
C SOLENOIDVALVE_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_use = 0.999999
C SOLENOIDVALVE_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_ere = 0.0
C SOLENOIDVALVE_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_ire = 0.0
C SOLENOIDVALVE_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_ore = L_aki.OR.L_eki
C SOLENOIDVALVE_MAN.fmg( 363, 237):���
      R0_ase = 1.0
C SOLENOIDVALVE_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_ese = 0.0
C SOLENOIDVALVE_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ute = 1.0e-10
C SOLENOIDVALVE_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_uve = 0.0
C SOLENOIDVALVE_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_ebi = DeltaT
C SOLENOIDVALVE_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_abi.ge.0.0) then
         R0_edi=R0_ebi/max(R_abi,1.0e-10)
      else
         R0_edi=R0_ebi/min(R_abi,-1.0e-10)
      endif
C SOLENOIDVALVE_MAN.fmg( 259, 262):�������� ����������
      R0_afi = 0.0
C SOLENOIDVALVE_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_obe = (.NOT.L_it).AND.L_ibi
C SOLENOIDVALVE_MAN.fmg( 102, 166):�
C label 133  try133=try133-1
      if(L_eki) then
         R0_ure=R0_ire
      else
         R0_ure=R8_exe
      endif
C SOLENOIDVALVE_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ore) then
         R_oxe=R8_exe
      endif
C SOLENOIDVALVE_MAN.fmg( 384, 252):���� � ������������� �������
      L0_ude = (.NOT.L_it).AND.L_obi
C SOLENOIDVALVE_MAN.fmg( 104, 260):�
      L0_afe = L_obi.OR.L0_ibe.OR.L0_ide
C SOLENOIDVALVE_MAN.fmg( 102, 229):���
      L0_ufe = (.NOT.L_obi).AND.L0_ofe
C SOLENOIDVALVE_MAN.fmg( 102, 235):�
      L_efe=(L0_ufe.or.L_efe).and..not.(L0_afe)
      L0_ife=.not.L_efe
C SOLENOIDVALVE_MAN.fmg( 110, 233):RS �������,1
      L0_ete = (.NOT.L0_ude).AND.L_efe
C SOLENOIDVALVE_MAN.fmg( 128, 236):�
      L0_are = L0_ete.AND.(.NOT.L_iki)
C SOLENOIDVALVE_MAN.fmg( 252, 242):�
      L0_ifi = L0_are.OR.L_oki
C SOLENOIDVALVE_MAN.fmg( 265, 241):���
      if(L0_ifi) then
         R0_odi=R0_edi
      else
         R0_odi=R0_afi
      endif
C SOLENOIDVALVE_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_ube = L0_ibe.OR.L_ibi.OR.L0_ofe
C SOLENOIDVALVE_MAN.fmg( 102, 185):���
      L0_ode = L0_ide.AND.(.NOT.L_ibi)
C SOLENOIDVALVE_MAN.fmg( 102, 191):�
      L_ade=(L0_ode.or.L_ade).and..not.(L0_ube)
      L0_ede=.not.L_ade
C SOLENOIDVALVE_MAN.fmg( 110, 189):RS �������,2
      L0_ate = L_ade.AND.(.NOT.L0_obe)
C SOLENOIDVALVE_MAN.fmg( 128, 190):�
      L0_upe = L0_ate.AND.(.NOT.L_oki)
C SOLENOIDVALVE_MAN.fmg( 252, 228):�
      L0_efi = L0_upe.OR.L_iki
C SOLENOIDVALVE_MAN.fmg( 265, 227):���
      if(L0_efi) then
         R0_idi=R0_adi
      else
         R0_idi=R0_afi
      endif
C SOLENOIDVALVE_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_udi = R0_odi + (-R0_idi)
C SOLENOIDVALVE_MAN.fmg( 293, 246):��������
      if(L_ufi) then
         R0_axe=R0_ere
      else
         R0_axe=R0_udi
      endif
C SOLENOIDVALVE_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_eve = R_oxe + (-R_ofi)
C SOLENOIDVALVE_MAN.fmg( 308, 235):��������
      R0_ote = R0_axe + R0_eve
C SOLENOIDVALVE_MAN.fmg( 314, 236):��������
      R0_ave = R0_ote * R0_eve
C SOLENOIDVALVE_MAN.fmg( 318, 235):����������
      L0_ive=R0_ave.lt.R0_ute
C SOLENOIDVALVE_MAN.fmg( 323, 234):���������� <
      L_ove=(L0_ive.or.L_ove).and..not.(.NOT.L_uki)
      L0_ite=.not.L_ove
C SOLENOIDVALVE_MAN.fmg( 330, 232):RS �������,6
      if(L_ove) then
         R_oxe=R_ofi
      endif
C SOLENOIDVALVE_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ove) then
         R0_ixe=R0_uve
      else
         R0_ixe=R0_axe
      endif
C SOLENOIDVALVE_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_uxe = R_oxe + R0_ixe
C SOLENOIDVALVE_MAN.fmg( 338, 245):��������
      R0_ise=MAX(R0_ese,R0_uxe)
C SOLENOIDVALVE_MAN.fmg( 346, 246):��������
      R0_ubi=MIN(R0_ase,R0_ise)
C SOLENOIDVALVE_MAN.fmg( 354, 247):�������
      L_ibi=R0_ubi.lt.R0_ose
C SOLENOIDVALVE_MAN.fmg( 363, 210):���������� <
C sav1=L0_ube
      L0_ube = L0_ibe.OR.L_ibi.OR.L0_ofe
C SOLENOIDVALVE_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_ube .and. try173.gt.0) goto 173
C sav1=L0_ode
      L0_ode = L0_ide.AND.(.NOT.L_ibi)
C SOLENOIDVALVE_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_ode .and. try175.gt.0) goto 175
C sav1=L0_obe
      L0_obe = (.NOT.L_it).AND.L_ibi
C SOLENOIDVALVE_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_obe .and. try133.gt.0) goto 133
      if(L0_ore) then
         R8_exe=R0_ure
      else
         R8_exe=R0_ubi
      endif
C SOLENOIDVALVE_MAN.fmg( 377, 246):���� RE IN LO CH7
      L_obi=R0_ubi.gt.R0_use
C SOLENOIDVALVE_MAN.fmg( 363, 225):���������� >
C sav1=L0_afe
      L0_afe = L_obi.OR.L0_ibe.OR.L0_ide
C SOLENOIDVALVE_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_afe .and. try153.gt.0) goto 153
C sav1=L0_ufe
      L0_ufe = (.NOT.L_obi).AND.L0_ofe
C SOLENOIDVALVE_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_ufe .and. try155.gt.0) goto 155
C sav1=L0_ude
      L0_ude = (.NOT.L_it).AND.L_obi
C SOLENOIDVALVE_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_ude .and. try148.gt.0) goto 148
      if(L_obi) then
         C30_ad=C30_e
      else
         C30_ad=C30_i
      endif
C SOLENOIDVALVE_MAN.fmg( 380, 219):���� RE IN LO CH20
      if(L_ibi) then
         C30_o=C30_u
      else
         C30_o=C30_ad
      endif
C SOLENOIDVALVE_MAN.fmg( 398, 218):���� RE IN LO CH20
      if(L0_ore) then
         R_oxe=R0_ubi
      endif
C SOLENOIDVALVE_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_eve
      R0_eve = R_oxe + (-R_ofi)
C SOLENOIDVALVE_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_eve .and. try198.gt.0) goto 198
C sav1=R0_uxe
      R0_uxe = R_oxe + R0_ixe
C SOLENOIDVALVE_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_uxe .and. try215.gt.0) goto 215
      L0_ime = L0_ete.OR.L0_ate
C SOLENOIDVALVE_MAN.fmg( 251, 174):���
      L0_ome = L0_ime.AND.(.NOT.L_ule)
C SOLENOIDVALVE_MAN.fmg( 266, 173):�
      if(L0_ome) then
         R0_ipe=R_ape
      else
         R0_ipe=R0_ume
      endif
C SOLENOIDVALVE_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_ude) then
         I_ep=I0_ip
      else
         I_ep=I0_op
      endif
C SOLENOIDVALVE_MAN.fmg( 124, 264):���� RE IN LO CH7
      if(L0_obe) then
         I_om=I0_um
      else
         I_om=I0_ap
      endif
C SOLENOIDVALVE_MAN.fmg( 117, 170):���� RE IN LO CH7
      if(L0_obe) then
         I0_ebe=I0_ix
      else
         I0_ebe=I0_ox
      endif
C SOLENOIDVALVE_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L0_ete) then
         I0_ux=I0_abe
      else
         I0_ux=I0_ebe
      endif
C SOLENOIDVALVE_MAN.fmg( 170, 262):���� RE IN LO CH7
      if(L0_ude) then
         I0_ex=I0_ov
      else
         I0_ex=I0_ux
      endif
C SOLENOIDVALVE_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L0_ate) then
         I0_uv=I0_ax
      else
         I0_uv=I0_ex
      endif
C SOLENOIDVALVE_MAN.fmg( 170, 206):���� RE IN LO CH7
      if(L_ele) then
         I_ar=I0_up
      else
         I_ar=I0_uv
      endif
C SOLENOIDVALVE_MAN.fmg( 191, 206):���� RE IN LO CH7
      R0_epe = R8_ope
C SOLENOIDVALVE_MAN.fmg( 264, 190):��������
C label 278  try278=try278-1
      R8_ope = R0_ipe + R0_epe
C SOLENOIDVALVE_MAN.fmg( 275, 189):��������
C sav1=R0_epe
      R0_epe = R8_ope
C SOLENOIDVALVE_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_epe .and. try278.gt.0) goto 278
      End
