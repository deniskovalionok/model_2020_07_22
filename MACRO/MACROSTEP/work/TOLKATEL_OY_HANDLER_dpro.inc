      Interface
      Subroutine TOLKATEL_OY_HANDLER_dpro(ext_deltat,L_e,L_i
     &,R_o,R_u,R_ad,I_ed,R_ud,R_af,R_ef,R_if,R_ek,R_ik,C20_om
     &,C20_op,I_ir,I_as,R_es,R_is,R_os,R_us,L_at,L_et,I_it
     &,I_av,I_ax,I_ox,C20_obe,C20_ode,C8_ofe,R_ile,R_ole,L_ule
     &,R_ame,R_eme,I_ime,L_ape,L_epe,I_ope,I_ere,L_ure,L_ese
     &,L_ise,L_use,L_ete,L_ite,L_ote,L_ove,L_oxe,L_abi,L_ibi
     &,L_ubi,R8_edi,R_afi,R8_ofi,L_oki,I_uki,L_emi,L_omi,L_upi
     &,L_iri,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi)
C |L_e           |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |L_i           |1 1 I|YA25C           |������� ������� �� ����������|F|
C |R_o           |4 4 K|_uintT_INT_1    |����������� ������ ����������� ������|`p_TOLKATEL_UP`|
C |R_u           |4 4 K|_tintT_INT_1    |[���]�������� T �����������|1|
C |R_ad          |4 4 K|_lintT_INT_1    |����������� ������ ����������� �����|0.0|
C |I_ed          |2 4 O|LWORK           |����� �������||
C |R_ud          |4 4 K|_lcmpJ2850      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_af          |4 4 K|_lcmpJ2849      |[]�������� ������ �����������|1.0|
C |R_ef          |4 4 O|VY01            |��������� ��������� �� ��� X||
C |R_if          |4 4 O|_ointT_INT_1*   |�������� ������ ����������� |0|
C |R_ek          |4 4 O|VY02            |�������� ����������� ���������||
C |R_ik          |4 4 I|tcl_top         |�������� �������|20|
C |C20_om        |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_op        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ir          |2 4 O|LREADY          |����� ����������||
C |I_as          |2 4 O|LBUSY           |����� �����||
C |R_es          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_is          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_os          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_us          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_at          |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_et          |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_it          |2 4 O|state1          |��������� 1||
C |I_av          |2 4 O|state2          |��������� 2||
C |I_ax          |2 4 O|LWORKO          |����� � �������||
C |I_ox          |2 4 O|LINITC          |����� � ��������||
C |C20_obe       |3 20 O|task_state      |���������||
C |C20_ode       |3 20 O|task_name       |������� ���������||
C |C8_ofe        |3 8 I|task            |������� ���������||
C |R_ile         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ole         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ule         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ame         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_eme         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ime         |2 4 O|LERROR          |����� �������������||
C |L_ape         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_epe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_ope         |2 4 O|LINIT           |����� ��������||
C |I_ere         |2 4 O|LZM             |������ "�������"||
C |L_ure         |1 1 I|vlv_kvit        |||
C |L_ese         |1 1 I|instr_fault     |||
C |L_ise         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_use         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ete         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ite         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ote         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ove         |1 1 O|block           |||
C |L_oxe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_abi         |1 1 O|STOP            |�������||
C |L_ibi         |1 1 O|norm            |�����||
C |L_ubi         |1 1 O|nopower         |��� ����������||
C |R8_edi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_afi         |4 4 I|power           |�������� ��������||
C |R8_ofi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_oki         |1 1 O|fault           |�������������||
C |I_uki         |2 4 O|LAM             |������ "�������"||
C |L_emi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_omi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_upi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_iri         |1 1 O|XH54            |�� ������� (���)|F|
C |L_uri         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_asi         |1 1 I|mlf23           |������� ������� �����||
C |L_esi         |1 1 I|mlf22           |����� ����� ��������||
C |L_isi         |1 1 I|mlf04           |�������� �� ��������||
C |L_osi         |1 1 I|mlf03           |�������� �� ��������||
C |L_usi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_i
      REAL*4 R_o,R_u,R_ad
      INTEGER*4 I_ed
      REAL*4 R_ud,R_af,R_ef,R_if,R_ek,R_ik
      CHARACTER*20 C20_om,C20_op
      INTEGER*4 I_ir,I_as
      REAL*4 R_es,R_is,R_os,R_us
      LOGICAL*1 L_at,L_et
      INTEGER*4 I_it,I_av,I_ax,I_ox
      CHARACTER*20 C20_obe,C20_ode
      CHARACTER*8 C8_ofe
      REAL*4 R_ile,R_ole
      LOGICAL*1 L_ule
      REAL*4 R_ame,R_eme
      INTEGER*4 I_ime
      LOGICAL*1 L_ape,L_epe
      INTEGER*4 I_ope,I_ere
      LOGICAL*1 L_ure,L_ese,L_ise,L_use,L_ete,L_ite,L_ote
     &,L_ove,L_oxe,L_abi,L_ibi,L_ubi
      REAL*8 R8_edi
      REAL*4 R_afi
      REAL*8 R8_ofi
      LOGICAL*1 L_oki
      INTEGER*4 I_uki
      LOGICAL*1 L_emi,L_omi,L_upi,L_iri,L_uri,L_asi,L_esi
     &,L_isi,L_osi,L_usi
      End subroutine TOLKATEL_OY_HANDLER_dpro
      End interface
