      Subroutine DAT_ANA_PATM_HANDLER(ext_deltat,R_e,R_i,R_o
     &,R_u,R_od,R_ul,R_em,R_um,R_ep,I_er,R_us,L_at,R_it,L_ot
     &,L_ut,R_ev,R_abe,R_ibe,L_ube)
C |R_e           |4 4 I|ATMOSPHERE_ABS  |���. ��������||
C |R_i           |4 4 I|input           |����||
C |R_o           |4 4 O|nom             |||
C |R_u           |4 4 K|_cJ1739         |�������� ��������� ����������|0.3|
C |R_od          |4 4 I|koeff_units     |����������� �����������||
C |R_ul          |4 4 I|LOWER_warning_setpoint|||
C |R_em          |4 4 I|LOWER_alarm_setpoint|||
C |R_um          |4 4 I|UPPER_alarm_setpoint|||
C |R_ep          |4 4 I|UPPER_warning_setpoint|||
C |I_er          |2 4 O|dat_color       |���� ��������||
C |R_us          |4 4 I|GM05V           |���������� ����������� �������||
C |L_at          |1 1 I|GM05            |���������� ����������� �������||
C |R_it          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_ot          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_ut          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_ev          |4 4 O|KKS             |�����||
C |R_abe         |4 4 I|MAXIMUM         |||
C |R_ibe         |4 4 I|MINIMUM         |||
C |L_ube         |1 1 I|GM01            |����� �� ����������� ������ ���������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i,R_o,R_u,R0_ad,R0_ed,R0_id,R_od,R0_ud
      LOGICAL*1 L0_af,L0_ef,L0_if,L0_of,L0_uf,L0_ak,L0_ek
     &,L0_ik,L0_ok,L0_uk,L0_al,L0_el,L0_il,L0_ol
      REAL*4 R_ul
      LOGICAL*1 L0_am
      REAL*4 R_em
      LOGICAL*1 L0_im,L0_om
      REAL*4 R_um
      LOGICAL*1 L0_ap
      REAL*4 R_ep
      LOGICAL*1 L0_ip,L0_op,L0_up
      INTEGER*4 I0_ar,I_er
      LOGICAL*1 L0_ir,L0_or,L0_ur
      INTEGER*4 I0_as,I0_es,I0_is
      REAL*4 R0_os,R_us
      LOGICAL*1 L_at
      REAL*4 R0_et,R_it
      LOGICAL*1 L_ot,L_ut
      REAL*4 R0_av,R_ev,R0_iv,R0_ov,R0_uv
      LOGICAL*1 L0_ax,L0_ex
      REAL*4 R0_ix
      LOGICAL*1 L0_ox
      REAL*4 R0_ux,R_abe,R0_ebe,R_ibe,R0_obe
      LOGICAL*1 L_ube

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ud = R_i + (-R_e)
C DAT_ANA_PATM_HANDLER.fmg(  48, 258):��������
      R0_obe = R0_ud * R_od
C DAT_ANA_PATM_HANDLER.fmg(  62, 258):����������
      if(L_ube) then
         R0_ebe=R_ibe
      else
         R0_ebe=R0_obe
      endif
C DAT_ANA_PATM_HANDLER.fmg( 148, 256):���� RE IN LO CH7
      if(L_ut) then
         R0_av=R_abe
      else
         R0_av=R0_ebe
      endif
C DAT_ANA_PATM_HANDLER.fmg( 160, 255):���� RE IN LO CH7
      if(L_ot) then
         R0_et=R_it
      else
         R0_et=R0_av
      endif
C DAT_ANA_PATM_HANDLER.fmg( 170, 254):���� RE IN LO CH7
      L0_ap=R0_obe.gt.R_ep
C DAT_ANA_PATM_HANDLER.fmg( 266, 277):���������� >
      L0_am=R0_obe.lt.R_ul
C DAT_ANA_PATM_HANDLER.fmg( 266, 270):���������� <
      L0_op = L0_ap.OR.L0_am
C DAT_ANA_PATM_HANDLER.fmg( 273, 276):���
      L0_om=R0_obe.gt.R_um
C DAT_ANA_PATM_HANDLER.fmg( 266, 262):���������� >
      L0_im=R0_obe.lt.R_em
C DAT_ANA_PATM_HANDLER.fmg( 266, 255):���������� <
      L0_ip = L0_om.OR.L0_im
C DAT_ANA_PATM_HANDLER.fmg( 272, 261):���
      !��������� R0_ad = DAT_ANA_PATM_HANDLERC?? /0.3/
      R0_ad=R_u
C DAT_ANA_PATM_HANDLER.fmg( 348, 211):���������
      R0_id = R_abe + (-R_ibe)
C DAT_ANA_PATM_HANDLER.fmg( 341, 213):��������
      R0_ed = R0_id * R0_ad
C DAT_ANA_PATM_HANDLER.fmg( 351, 212):����������
      R_o = R0_ed + R_ibe
C DAT_ANA_PATM_HANDLER.fmg( 362, 211):��������
      L0_ik=R_it.gt.R_ul
C DAT_ANA_PATM_HANDLER.fmg( 304, 206):���������� >
      L0_ok=R_it.gt.R_em
C DAT_ANA_PATM_HANDLER.fmg( 304, 211):���������� >
      L0_uk=R_it.lt.R_um
C DAT_ANA_PATM_HANDLER.fmg( 304, 216):���������� <
      L0_al=R_it.lt.R_ep
C DAT_ANA_PATM_HANDLER.fmg( 304, 222):���������� <
      L0_el=R_it.lt.R_abe
C DAT_ANA_PATM_HANDLER.fmg( 304, 229):���������� <
      L0_ol=R_it.gt.R_ibe
C DAT_ANA_PATM_HANDLER.fmg( 304, 238):���������� >
      L0_il = L0_ol.AND.L0_el.AND.L0_al.AND.L0_uk.AND.L0_ok.AND.L0_ik
C DAT_ANA_PATM_HANDLER.fmg( 321, 234):�
      L0_or = L_ot.AND.L0_il
C DAT_ANA_PATM_HANDLER.fmg( 213, 235):�
      I0_ar = z'000000FF'
C DAT_ANA_PATM_HANDLER.fmg( 318, 282):��������� ������������� IN (�������)
      I0_es = z'0000FFFF'
C DAT_ANA_PATM_HANDLER.fmg( 232, 236):��������� ������������� IN (�������)
      I0_is = z'0000FF00'
C DAT_ANA_PATM_HANDLER.fmg( 232, 238):��������� ������������� IN (�������)
      R0_iv = 0.01
C DAT_ANA_PATM_HANDLER.fmg( 110, 264):��������� (RE4) (�������)
      R0_uv = R_abe + (-R_ibe)
C DAT_ANA_PATM_HANDLER.fmg( 104, 266):��������
      R0_ov = R0_uv * R0_iv
C DAT_ANA_PATM_HANDLER.fmg( 114, 266):����������
      R0_ux = R_abe + R0_ov
C DAT_ANA_PATM_HANDLER.fmg( 120, 276):��������
      L0_ox=R0_obe.gt.R0_ux
C DAT_ANA_PATM_HANDLER.fmg( 126, 278):���������� >
      R0_ix = R_ibe + (-R0_ov)
C DAT_ANA_PATM_HANDLER.fmg( 120, 270):��������
      L0_ex=R0_obe.lt.R0_ix
C DAT_ANA_PATM_HANDLER.fmg( 126, 272):���������� <
      L0_ax = L0_ox.OR.L0_ex
C DAT_ANA_PATM_HANDLER.fmg( 134, 274):���
      L0_up = L0_op.OR.L0_ip.OR.L0_ax.OR.L_ube.OR.L_ut
C DAT_ANA_PATM_HANDLER.fmg( 318, 272):���
      R0_os = R_us + R_ev
C DAT_ANA_PATM_HANDLER.fmg( 166, 218):��������
C label 81  try81=try81-1
      if(L_at) then
         R_ev=R0_os
      else
         R_ev=R0_et
      endif
C DAT_ANA_PATM_HANDLER.fmg( 177, 253):���� RE IN LO CH7
C sav1=R0_os
      R0_os = R_us + R_ev
C DAT_ANA_PATM_HANDLER.fmg( 166, 218):recalc:��������
C if(sav1.ne.R0_os .and. try81.gt.0) goto 81
      L0_ak=R_ev.gt.R_ibe
C DAT_ANA_PATM_HANDLER.fmg( 304, 196):���������� >
      L0_uf=R_ev.lt.R_abe
C DAT_ANA_PATM_HANDLER.fmg( 304, 186):���������� <
      L0_if=R_ev.lt.R_um
C DAT_ANA_PATM_HANDLER.fmg( 304, 173):���������� <
      L0_af=R_ev.gt.R_ul
C DAT_ANA_PATM_HANDLER.fmg( 304, 163):���������� >
      L0_ef=R_ev.gt.R_em
C DAT_ANA_PATM_HANDLER.fmg( 304, 168):���������� >
      L0_of=R_ev.lt.R_ep
C DAT_ANA_PATM_HANDLER.fmg( 304, 178):���������� <
      L0_ek = L0_ak.AND.L0_uf.AND.L0_of.AND.L0_if.AND.L0_ef.AND.L0_af
C DAT_ANA_PATM_HANDLER.fmg( 321, 190):�
      L0_ir = L_at.AND.L0_ek
C DAT_ANA_PATM_HANDLER.fmg( 213, 228):�
      L0_ur = L0_or.OR.L0_ir
C DAT_ANA_PATM_HANDLER.fmg( 222, 233):���
      if(L0_ur) then
         I0_as=I0_es
      else
         I0_as=I0_is
      endif
C DAT_ANA_PATM_HANDLER.fmg( 236, 236):���� RE IN LO CH7
      if(L0_up) then
         I_er=I0_ar
      else
         I_er=I0_as
      endif
C DAT_ANA_PATM_HANDLER.fmg( 322, 282):���� RE IN LO CH7
      End
