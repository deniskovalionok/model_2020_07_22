      Interface
      Subroutine K2_VLVA(ext_deltat,L_e,L_i,L_if,L_of,R_ek
     &,R_ok,L_uk,R_el,R8_ap,L_ep,R8_ir,I_es,R_at,R_et,R_ot
     &,R_ut,R_uv,R_ax,L_ex,L_ix,R_ox,R_abe,R_ebe,R_efe,R_ole
     &,R_ule,R_eme,R8_ape,R_ipe,R_use,L_ate,R_ave,L_ove,L_uve
     &,L_axe,I_abi,I_ibi,L_edi,L_odi,L_udi,I_afi,I_efi,L_ifi
     &,L_ofi,L_aki,L_oki,R_ali,R_eli,L_ili,R_uli,L_ami,L_emi
     &,L_omi,L_umi,L_epi,L_ipi,L_opi,L_upi,L_asi,L_isi,L_osi
     &,L_ati,L_iti,L_oti,L_uti,I_ovi,I_uvi,I_axi,I_exi,L_ixi
     &,R_oxi,L_uxi,I1_ebo,L_ibo,L_obo,L_ubo,L_ado,L_edo,L_odo
     &,L_udo,L_afo,L_ifo,L_ofo,R_eko,L_iko,L_oko,L_uko,L_alo
     &,L_elo,L_amo,L_emo,L_omo,L_iso,L_oso,L_ato,L_uvo,L_axo
     &,L_exo,L_ixo,L_oxo,L_uxo,L_abu,L_ebu,L_ibu,L_obu,L_ubu
     &,L_adu,L_edu,L_idu,L_odu,L_udu)
C |L_e           |1 1 O|cl_cmd          |������� �������|F|
C |L_i           |1 1 O|op_cmd          |������� �������|F|
C |L_if          |1 1 I|cl_key          |������� �������|F|
C |L_of          |1 1 I|op_key          |������� �������|F|
C |R_ek          |4 4 K|_tdel1          |[]�������� ������� �������� ������|2.0|
C |R_ok          |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_uk          |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |R_el          |4 4 K|_c02            |�������� ��������� ����������|`power_c`|
C |R8_ap         |4 8 I|voltage         |[�]���������� ������ �������� �������||
C |L_ep          |1 1 O|novoltage       |��� ���������� �������||
C |R8_ir         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |I_es          |2 4 I|SF1SWT          |������� ������� �������||
C |R_at          |4 4 O|isu_pif_value   |����� ����� ���� �� ��� ��� ���||
C |R_et          |4 4 O|isu_value       |�����. �����. ���������� �� ���||
C |R_ot          |4 4 O|isu_pif_eps     |��������� ����� ���� �� ��� ��� ���||
C |R_ut          |4 4 O|isu_eps         |��������� �����. ���������� �� ���||
C |R_uv          |4 4 O|pif_eps         |��������� ���������� ��� ���||
C |R_ax          |4 4 I|eps             |��������� ����������||
C |L_ex          |1 1 I|GM146           |����� ������� �������� ��21 "����.�� ����"||
C |L_ix          |1 1 I|GM145           |����� ������� �������� ��21 "����.�� ����"||
C |R_ox          |4 4 O|pif_value       |���.��������� ����� ��� ���|0.5|
C |R_abe         |4 4 O|out_value_prc   |���.��������� ����� � %|50|
C |R_ebe         |4 4 S|_slpb1*         |���������� ��������� ||
C |R_efe         |4 4 I|tau             |���������� ������� �������|0.0|
C |R_ole         |4 4 I|tclose          |������ ����� ��������|30.0|
C |R_ule         |4 4 I|topen           |������ ����� ��������|30.0|
C |R_eme         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_ape        |4 8 O|value           |���.�������� ��������|0.5|
C |R_ipe         |4 4 O|out_value       |���.��������� �����|0.5|
C |R_use         |4 4 I|GM19P           |��������� ������������|0.6|
C |L_ate         |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R_ave         |4 4 O|posfbst         |��������� ���.��|0.5|
C |L_ove         |1 1 S|_spls3*         |[TF]���������� ��������� ������������� |F|
C |L_uve         |1 1 I|ISU_DIST        |������� "����" �� ���||
C |L_axe         |1 1 I|ISU_AVT         |������� "���" �� ���||
C |I_abi         |2 4 S|_sdly1*         |���������� ��������� �������� ||
C |I_ibi         |2 4 O|_ocoder1*       |����� ���������|0|
C |L_edi         |1 1 I|GM143           |����� ������� ����� ��21 "�"||
C |L_odi         |1 1 I|pif_sah3        |���� ����� "�"||
C |L_udi         |1 1 I|ISU_ON          |������� ������� �� ���||
C |I_afi         |2 4 I|ISU_NUM         |����� ���������� �� ��� ����������||
C |I_efi         |2 4 I|DISUN           ||-1|
C |L_ifi         |1 1 I|ISU_OFF         |������� ������� �� ���||
C |L_ofi         |1 1 O|isu_sign        |������� ���������� �� ���|F|
C |L_aki         |1 1 I|gk_close        |������� �� ����� ��.���.�������||
C |L_oki         |1 1 I|gk_open         |������� �� ����� ��.���.�������||
C |R_ali         |4 4 I|SA1             |������� ������ �� ������ "���� ������������ ������\n��� ������ ���� ����������" |0.0|
C |R_eli         |4 4 S|SA1_ST*         |���������� ��������� "���� ������������ ������\n��� ������ ���� ����������" |0.0|
C |L_ili         |1 1 S|SA1_STL*        |���������� ��������� "���� ������������ ������\n��� ������ ���� ����������" |F|
C |R_uli         |4 4 O|SA1_ANGLE*      |���� �������� ����� "���� ������������ ������\n��� ������ ���� ����������" |0.0|
C |L_ami         |1 1 O|SA1_CMDL*       |[TF]������� ����� ���� ������������ ������\n��� ������ ���� ���������� - �����|F|
C |L_emi         |1 1 O|SA1_CMDR*       |[TF]������� ����� ���� ������������ ������\n��� ������ ���� ���������� - ������|F|
C |L_omi         |1 1 I|pif_sa12        |���� ������ �������||
C |L_umi         |1 1 I|pif_sa11        |���� ������ �������||
C |L_epi         |1 1 S|_spls2*         |[TF]���������� ��������� ������������� |F|
C |L_ipi         |1 1 S|_spls1*         |[TF]���������� ��������� ������������� |F|
C |L_opi         |1 1 I|setar           |��������� ������ ���������� (���)||
C |L_upi         |1 1 I|breakar         |���������� ������ ���������� (���)||
C |L_asi         |1 1 I|GM141           |����� ������� ����� ��21 "�"||
C |L_isi         |1 1 I|pif_sah1        |���� ����� "�"||
C |L_osi         |1 1 I|GM142           |����� ������� ����� ��21 "�"||
C |L_ati         |1 1 I|pif_sah2        |���� ����� "�"||
C |L_iti         |1 1 O|sah3            |���� � "�"||
C |L_oti         |1 1 O|sah2            |���� � "�"||
C |L_uti         |1 1 O|sah1            |���� � "�"||
C |I_ovi         |2 4 S|SAH_POS0*       |��������� ������ �������������|2|
C |I_uvi         |2 4 O|SAH_POS         |��������� ��������� �����|2|
C |I_axi         |2 4 S|SAH_4OLD*       |��������� ����� "���� ������ ������ ��21\n��� ������ ���� ����������" |0|
C |I_exi(1:4)    |2 4 I|SAH(1:4)        |������� ������ ����� "���� ������ ������ ��21\n��� ������ ���� ����������" |4*0|
C |L_ixi         |1 1 S|SAH_FLAG*       |����� ��������� � ���� |F|
C |R_oxi         |4 4 O|SAH_ANGLE*      |���� �������� ����� "���� ������ ������ ��21\n��� ������ ���� ����������" |0.0|
C |L_uxi         |1 1 I|blkdist         |����.����.���||
C |I1_ebo(1:10)  |2 1 O|UBK(1:10)       |���������� �����||
C |L_ibo         |1 1 I|GM152           |����� ������� �������� ��� "�������"||
C |L_obo         |1 1 I|GM151           |����� ������� �������� ��� "����"||
C |L_ubo         |1 1 I|GM47            |��� ��������� ���������||
C |L_ado         |1 1 O|rlamp           |��������� ������� ��������||
C |L_edo         |1 1 O|glamp           |��������� ������� ��������||
C |L_odo         |1 1 I|blockreg        |���������� ���� �����������|F|
C |L_udo         |1 1 I|uluclose        |������� ������� (���)||
C |L_afo         |1 1 I|uluopen         |������� ������� (���)||
C |L_ifo         |1 1 I|resmode         |��������� � �������||
C |L_ofo         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |`arstate_c`|
C |R_eko         |4 4 I|tslow           |����������� ���������� �������|1|
C |L_iko         |1 1 I|blockblk        |���������� ���� ����������|F|
C |L_oko         |1 1 O|cbc             |�� ������� (���)|F|
C |L_uko         |1 1 O|cbo             |�� ������� (���)|F|
C |L_alo         |1 1 O|cbcm            |������� 0.001|F|
C |L_elo         |1 1 O|outc            |��� ������� �������|F|
C |L_amo         |1 1 O|RASP            |��� �������� � ����.�������|F|
C |L_emo         |1 1 O|outo            |��� ������� �������|F|
C |L_omo         |1 1 O|cbom            |������� 0.999|F|
C |L_iso         |1 1 O|cmdcl           |������� ������� (���)||
C |L_oso         |1 1 O|arstate         |��������� ����������|F|
C |L_ato         |1 1 O|cmdop           |������� ������� (���)||
C |L_uvo         |1 1 I|tzclose         |������� ������� (��)|F|
C |L_axo         |1 1 I|blkclose        |���� �������� (��)||
C |L_exo         |1 1 I|blkopen         |����. �������� (��)||
C |L_ixo         |1 1 I|tzopen          |������� ������� (��)|F|
C |L_oxo         |1 1 I|GM45            |����. ������ �� ���||
C |L_uxo         |1 1 I|GM03            |������� � ��������||
C |L_abu         |1 1 I|GM04            |������� � ��������||
C |L_ebu         |1 1 I|GM19            |���� ������������||
C |L_ibu         |1 1 I|GM08            |������� �� "�������"||
C |L_obu         |1 1 I|GM09            |������� �� "�������"||
C |L_ubu         |1 1 I|GM22            |����� ����� ��������||
C |L_adu         |1 1 I|GM23            |������� ������� �����||
C |L_edu         |1 1 I|GM24            |��� ���������� �� � ����������||
C |L_idu         |1 1 I|GM46            |�� �����������||
C |L_odu         |1 1 I|GM121           |����� ������� ������ ��� �������||
C |L_udu         |1 1 I|GM122           |����� ������� ������ ��� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_i,L_if,L_of
      REAL*4 R_ek,R_ok
      LOGICAL*1 L_uk
      REAL*4 R_el
      REAL*8 R8_ap
      LOGICAL*1 L_ep
      REAL*8 R8_ir
      INTEGER*4 I_es
      REAL*4 R_at,R_et,R_ot,R_ut,R_uv,R_ax
      LOGICAL*1 L_ex,L_ix
      REAL*4 R_ox,R_abe,R_ebe,R_efe,R_ole,R_ule,R_eme
      REAL*8 R8_ape
      REAL*4 R_ipe,R_use
      LOGICAL*1 L_ate
      REAL*4 R_ave
      LOGICAL*1 L_ove,L_uve,L_axe
      INTEGER*4 I_abi,I_ibi
      LOGICAL*1 L_edi,L_odi,L_udi
      INTEGER*4 I_afi,I_efi
      LOGICAL*1 L_ifi,L_ofi,L_aki,L_oki
      REAL*4 R_ali,R_eli
      LOGICAL*1 L_ili
      REAL*4 R_uli
      LOGICAL*1 L_ami,L_emi,L_omi,L_umi,L_epi,L_ipi,L_opi
     &,L_upi,L_asi,L_isi,L_osi,L_ati,L_iti,L_oti,L_uti
      INTEGER*4 I_ovi,I_uvi,I_axi,I_exi(1:4)
      LOGICAL*1 L_ixi
      REAL*4 R_oxi
      LOGICAL*1 L_uxi
      INTEGER*1 I1_ebo(1:10)
      LOGICAL*1 L_ibo,L_obo,L_ubo,L_ado,L_edo,L_odo,L_udo
     &,L_afo,L_ifo,L_ofo
      REAL*4 R_eko
      LOGICAL*1 L_iko,L_oko,L_uko,L_alo,L_elo,L_amo,L_emo
     &,L_omo,L_iso,L_oso,L_ato,L_uvo,L_axo,L_exo,L_ixo,L_oxo
     &,L_uxo,L_abu,L_ebu,L_ibu,L_obu,L_ubu
      LOGICAL*1 L_adu,L_edu,L_idu,L_odu,L_udu
      End subroutine K2_VLVA
      End interface
