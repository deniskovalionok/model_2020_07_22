      Interface
      Subroutine PUMP_HANDLER(ext_deltat,C30_ad,I_ed,L_ud
     &,L_of,L_uf,I_il,I_am,R_om,R_um,R_ap,R_ep,I_op,L_us,L_at
     &,L_et,L_it,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv,L_ax,L_ex
     &,L_ix,L_ox,L_ux,L_ibe,L_obe,L_ede,L_ide,L_ode,I_ife
     &,R_eke,R_oke,L_uke,L_ile,L_ole,R8_ime,L_ome,R8_ume,R_ere
     &,R_ure,R_ose,R8_use,R_ete,R8_ite,R_ave,R8_eve,R_ove
     &,R_uve)
C |C30_ad        |3 30 O|state           |||
C |I_ed          |2 4 O|LOFF2           |����� ���������||
C |L_ud          |1 1 I|tech_mode       |||
C |L_of          |1 1 I|ruch_mode       |||
C |L_uf          |1 1 I|avt_mode        |||
C |I_il          |2 4 O|LOFF            |����� ���������||
C |I_am          |2 4 O|LON             |����� ��������||
C |R_om          |4 4 S|turn_off_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_um          |4 4 I|turn_off_button |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 S|turn_on_button_ST*|��������� ������ "������� �������� �� ���������" |0.0|
C |R_ep          |4 4 I|turn_on_button  |������� ������ ������ "������� �������� �� ���������" |0.0|
C |I_op          |2 4 O|LM              |�����||
C |L_us          |1 1 O|block           |||
C |L_at          |1 1 O|fault           |�������������||
C |L_et          |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_it          |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |L_ot          |1 1 O|norm            |�����||
C |L_ut          |1 1 I|gm14            |������������� ������� ���������||
C |L_av          |1 1 I|gm12            |������������� ������� ��������||
C |L_ev          |1 1 I|gm13            |���������������� ����������||
C |L_iv          |1 1 I|gm11            |���������������� ���������||
C |L_ov          |1 1 O|cb_out          |��������� �������� ����.||
C |L_uv          |1 1 I|uluboff         |���������� ���������� �� ���������|F|
C |L_ax          |1 1 O|turn_off_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ex          |1 1 I|ulubon          |���������� ��������� �� ���������|F|
C |L_ix          |1 1 O|turn_on_button_CMD*|[TF]����� ������ ������� �������� �� ���������|F|
C |L_ox          |1 1 S|_qffJ1545*      |�������� ������ Q RS-��������  |F|
C |L_ux          |1 1 I|uluoff1         |������� ��������� �� ����������|F|
C |L_ibe         |1 1 S|_qffJ1541*      |�������� ������ Q RS-��������  |F|
C |L_obe         |1 1 I|uluon1          |������� �������� �� ����������|F|
C |L_ede         |1 1 I|uluoff          |������� ��������� �� ����������|F|
C |L_ide         |1 1 I|uluon           |������� �������� �� ����������|F|
C |L_ode         |1 1 O|lstate          |��������� ����������� ���������||
C |I_ife         |2 4 O|LBM             |||
C |R_eke         |4 4 K|_tdel1          |[]�������� ������� �������� ������|2|
C |R_oke         |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_uke         |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |L_ile         |1 1 I|block_ele       |���������� ������������� �������||
C |L_ole         |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R8_ime        |4 8 I|voltage         |[��]���������� �� ������||
C |L_ome         |1 1 I|el_use          |����� ������||
C |R8_ume        |4 8 I|estate          |������������� �������� EN_CAD|0.0|
C |R_ere         |4 4 O|_oapr5*         |�������� ������ ���������. ����� |0.0|
C |R_ure         |4 4 I|kratn           |��������� ��������� ����|3.0|
C |R_ose         |4 4 O|idvig           |[�] ��� ��������� |3.0|
C |R8_use        |4 8 I|pnagr           |[��] �������� �������� ||
C |R_ete         |4 4 O|power           |�������� ���������|3.0|
C |R8_ite        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |R_ave         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_eve        |4 8 O|fizstate        |������������� ��������|0.0|
C |R_ove         |4 4 I|timeon          |���������� ������� ���������|3.0|
C |R_uve         |4 4 I|timeoff         |���������� ������� ����������|3.0|

      IMPLICIT NONE
      REAL*4 ext_deltat
      CHARACTER*30 C30_ad
      INTEGER*4 I_ed
      LOGICAL*1 L_ud,L_of,L_uf
      INTEGER*4 I_il,I_am
      REAL*4 R_om,R_um,R_ap,R_ep
      INTEGER*4 I_op
      LOGICAL*1 L_us,L_at,L_et,L_it,L_ot,L_ut,L_av,L_ev,L_iv
     &,L_ov,L_uv,L_ax,L_ex,L_ix,L_ox,L_ux,L_ibe,L_obe,L_ede
     &,L_ide,L_ode
      INTEGER*4 I_ife
      REAL*4 R_eke,R_oke
      LOGICAL*1 L_uke,L_ile,L_ole
      REAL*8 R8_ime
      LOGICAL*1 L_ome
      REAL*8 R8_ume
      REAL*4 R_ere,R_ure,R_ose
      REAL*8 R8_use
      REAL*4 R_ete
      REAL*8 R8_ite
      REAL*4 R_ave
      REAL*8 R8_eve
      REAL*4 R_ove,R_uve
      End subroutine PUMP_HANDLER
      End interface
