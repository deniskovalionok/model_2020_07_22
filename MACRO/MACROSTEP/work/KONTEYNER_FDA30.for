      Subroutine KONTEYNER_FDA30(ext_deltat,L_e,R_o,R_ad,R_od
     &,R_af,R_if,R_of,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_al,R_el
     &,R_il,R_ol,R_am,R_em,R_im,R_om,R_ap,L_op,R_up,R_or,R_ur
     &,R_es,R_is,R_os,R_at,L_et,L_ut,R_ov,R_uv,R_ox,R_ux,R_ebe
     &,R_ibe,R_obe,R_ade,L_ede,L_ude,R_ofe,L_ufe,L_ake,R_eke
     &,L_uke,L_ole,R_ame,R_ome,R_ipe,R_ope,R_ere,R_ire,R_ore
     &,R_ure,R_ase,R_ese,R_ise,R_ose,R_use,R_ate,R_ite,L_ote
     &,L_eve,R_axe,R_exe)
C |L_e           |1 1 O|TRIG_VAL        |��������� �������||
C |R_o           |4 4 K|_lcmpJ7014      |[]�������� ������ �����������|-5|
C |R_ad          |4 4 K|_lcmpJ7011      |[]�������� ������ �����������|10.0|
C |R_od          |4 4 K|_lcmpJ7005      |[]�������� ������ �����������|-5|
C |R_af          |4 4 K|_lcmpJ7002      |[]�������� ������ �����������|10.0|
C |R_if          |4 4 K|_uintJ6943      |����������� ������ ����������� ������|13146|
C |R_of          |4 4 K|_tintJ6943      |[���]�������� T �����������|1|
C |R_uf          |4 4 K|_lintJ6943      |����������� ������ ����������� �����|0.0|
C |R_ak          |4 4 K|_uintJ6942      |����������� ������ ����������� ������|4992|
C |R_ek          |4 4 K|_tintJ6942      |[���]�������� T �����������|1|
C |R_ik          |4 4 K|_lintJ6942      |����������� ������ ����������� �����|0|
C |R_ok          |4 4 K|_lcmpJ6909      |[]�������� ������ �����������|4260|
C |R_uk          |4 4 K|_lcmpJ6908      |[]�������� ������ �����������|4240|
C |R_al          |4 4 K|_lcmpJ6907      |[]�������� ������ �����������|0.1|
C |R_el          |4 4 K|_lcmpJ6875      |[]�������� ������ �����������|260|
C |R_il          |4 4 K|_lcmpJ6874      |[]�������� ������ �����������|270|
C |R_ol          |4 4 K|_lcmpJ6873      |[]�������� ������ �����������|280|
C |R_am          |4 4 I|FDA32AM001VS01  |��������� ���������||
C |R_em          |4 4 K|_lcmpJ6683      |[]�������� ������ �����������|11889|
C |R_im          |4 4 K|_lcmpJ6682      |[]�������� ������ �����������|11895|
C |R_om          |4 4 O|VS02            |�������� ��������||
C |R_ap          |4 4 I|FDA32AM001VS02  |�������� ���������||
C |L_op          |1 1 O|VP04            |��������� �������||
C |R_up          |4 4 K|_lcmpJ6609      |[]�������� ������ �����������|280|
C |R_or          |4 4 K|_lcmpJ6603      |[]�������� ������ �����������|297|
C |R_ur          |4 4 K|_lcmpJ6597      |[]�������� ������ �����������|303|
C |R_es          |4 4 I|FDA34BB003KE03VY01|���������� ������� �� OY||
C |R_is          |4 4 K|_lcmpJ6592      |[]�������� ������ �����������|6052|
C |R_os          |4 4 K|_lcmpJ6591      |[]�������� ������ �����������|6062|
C |R_at          |4 4 I|FDA34BB003KE03VY02|�������� ������������ �� OX||
C |L_et          |1 1 S|_qffJ6608*      |�������� ������ Q RS-��������  |F|
C |L_ut          |1 1 O|VP03            |��������� �������||
C |R_ov          |4 4 K|_lcmpJ6578      |[]�������� ������ �����������|0.1|
C |R_uv          |4 4 K|_lcmpJ6457      |[]�������� ������ �����������|280|
C |R_ox          |4 4 K|_lcmpJ6451      |[]�������� ������ �����������|297|
C |R_ux          |4 4 K|_lcmpJ6445      |[]�������� ������ �����������|303|
C |R_ebe         |4 4 I|FDA34BB002KE03VY01|���������� ������� �� OY||
C |R_ibe         |4 4 K|_lcmpJ6440      |[]�������� ������ �����������|5152|
C |R_obe         |4 4 K|_lcmpJ6439      |[]�������� ������ �����������|5162|
C |R_ade         |4 4 I|FDA34BB002KE03VY02|�������� ������������ �� OX||
C |L_ede         |1 1 S|_qffJ6456*      |�������� ������ Q RS-��������  |F|
C |L_ude         |1 1 O|VP02            |��������� �������||
C |R_ofe         |4 4 K|_lcmpJ6426      |[]�������� ������ �����������|0.1|
C |L_ufe         |1 1 O|Block3          |��������� ������������ ��������||
C |L_ake         |1 1 O|Block2          |��������� ������������ ��������||
C |R_eke         |4 4 K|_lcmpJ6405      |[]�������� ������ �����������|0.1|
C |L_uke         |1 1 O|Block1          |��������� ������������ ��������||
C |L_ole         |1 1 O|Block           |��������� ������������ ��������||
C |R_ame         |4 4 I|FDA34BB001KE03VY01|���������� ������� �� OY||
C |R_ome         |4 4 I|FDA30AE401-M02VY02|�������� �������  �� OY||
C |R_ipe         |4 4 I|FDA30AE401-M02VY01|���������� ���� �� OY||
C |R_ope         |4 4 I|FDA30AE401-M01VX01|���������� �������  �� OX||
C |R_ere         |4 4 I|FDA30AE401-M01VX02|�������� �������  �� OX||
C |R_ire         |4 4 O|_ointJ6943*     |�������� ������ ����������� |`START_X`|
C |R_ore         |4 4 O|VX02            |�������� ���������� �� X||
C |R_ure         |4 4 O|VY01            |��������� ���������� �� OY||
C |R_ase         |4 4 O|_ointJ6942*     |�������� ������ ����������� |`START_Y`|
C |R_ese         |4 4 O|R3VY02          |�������� ���������� �� OY||
C |R_ise         |4 4 O|R2VY02          |�������� ���������� �� OY||
C |R_ose         |4 4 O|VY02            |�������� ���������� �� OY||
C |R_use         |4 4 O|R0VY02          |�������� ���������� �� Y||
C |R_ate         |4 4 O|R1VY02          |�������� ���������� �� OY||
C |R_ite         |4 4 I|FDA34BB001KE03VY02|�������� ������������ �� OX||
C |L_ote         |1 1 S|_qffJ6362*      |�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 O|VP01            |��������� �������||
C |R_axe         |4 4 O|VS01            |��������� ����������||
C |R_exe         |4 4 O|VX01            |���������� ���������� �� X||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L0_i
      REAL*4 R_o
      LOGICAL*1 L0_u
      REAL*4 R_ad,R0_ed
      LOGICAL*1 L0_id
      REAL*4 R_od
      LOGICAL*1 L0_ud
      REAL*4 R_af,R0_ef,R_if,R_of,R_uf,R_ak,R_ek,R_ik,R_ok
     &,R_uk,R_al,R_el,R_il,R_ol,R0_ul,R_am,R_em,R_im,R_om
     &,R0_um,R_ap
      LOGICAL*1 L0_ep,L0_ip,L_op
      REAL*4 R_up
      LOGICAL*1 L0_ar,L0_er,L0_ir
      REAL*4 R_or,R_ur,R0_as,R_es,R_is,R_os,R0_us,R_at
      LOGICAL*1 L_et,L0_it,L0_ot,L_ut,L0_av,L0_ev,L0_iv
      REAL*4 R_ov,R_uv
      LOGICAL*1 L0_ax,L0_ex,L0_ix
      REAL*4 R_ox,R_ux,R0_abe,R_ebe,R_ibe,R_obe,R0_ube,R_ade
      LOGICAL*1 L_ede,L0_ide,L0_ode,L_ude,L0_afe,L0_efe,L0_ife
      REAL*4 R_ofe
      LOGICAL*1 L_ufe,L_ake
      REAL*4 R_eke
      LOGICAL*1 L0_ike,L0_oke,L_uke,L0_ale,L0_ele,L0_ile,L_ole
      REAL*4 R0_ule,R_ame
      LOGICAL*1 L0_eme
      REAL*4 R0_ime,R_ome
      LOGICAL*1 L0_ume,L0_ape,L0_epe
      REAL*4 R_ipe,R_ope
      LOGICAL*1 L0_upe
      REAL*4 R0_are,R_ere,R_ire,R_ore,R_ure,R_ase,R_ese,R_ise
     &,R_ose,R_use,R_ate,R0_ete,R_ite
      LOGICAL*1 L_ote,L0_ute,L0_ave,L_eve,L0_ive,L0_ove,L0_uve
      REAL*4 R_axe,R_exe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ul = 0.0
C KONTEYNER_FDA30.fmg( 244, 590):��������� (RE4) (�������)
      R0_um = 0.0
C KONTEYNER_FDA30.fmg( 193, 589):��������� (RE4) (�������)
      R0_us = 0.0
C KONTEYNER_FDA30.fmg( 242, 650):��������� (RE4) (�������)
      R0_ube = 0.0
C KONTEYNER_FDA30.fmg( 240, 706):��������� (RE4) (�������)
      L0_ike=R_ome.gt.R_eke
C KONTEYNER_FDA30.fmg( 215, 802):���������� >
      R0_ime = 0.0
C KONTEYNER_FDA30.fmg( 206, 860):��������� (RE4) (�������)
      R0_are = 0.0
C KONTEYNER_FDA30.fmg( 202, 868):��������� (RE4) (�������)
      R0_ete = 0.0
C KONTEYNER_FDA30.fmg( 241, 760):��������� (RE4) (�������)
      R0_ef = R_ope + (-R_exe)
C KONTEYNER_FDA30.fmg( 184, 843):��������
C label 17  try17=try17-1
      L0_uve=R_exe.lt.R_ok
C KONTEYNER_FDA30.fmg( 217, 760):���������� <
      L0_ove=R_exe.gt.R_uk
C KONTEYNER_FDA30.fmg( 217, 754):���������� >
      L0_ip=R_exe.lt.R_im
C KONTEYNER_FDA30.fmg( 217, 576):���������� <
      L0_ep=R_exe.gt.R_em
C KONTEYNER_FDA30.fmg( 217, 570):���������� >
      L_op = L0_ip.AND.L0_ep
C KONTEYNER_FDA30.fmg( 224, 576):�
      if(L_op) then
         R_axe=R_am
      else
         R_axe=R0_ul
      endif
C KONTEYNER_FDA30.fmg( 254, 588):���� RE IN LO CH7
      L0_ive=R_axe.lt.R_al
C KONTEYNER_FDA30.fmg( 217, 748):���������� <
      L0_ud=R0_ef.lt.R_af
C KONTEYNER_FDA30.fmg( 194, 843):���������� <
      L0_id=R0_ef.gt.R_od
C KONTEYNER_FDA30.fmg( 194, 836):���������� >
      L0_ape = L0_ud.AND.L0_id
C KONTEYNER_FDA30.fmg( 203, 842):�
      R0_ed = R_ipe + (-R_ure)
C KONTEYNER_FDA30.fmg( 184, 828):��������
      L0_u=R0_ed.lt.R_ad
C KONTEYNER_FDA30.fmg( 194, 828):���������� <
      L0_i=R0_ed.gt.R_o
C KONTEYNER_FDA30.fmg( 194, 820):���������� >
      L0_ume = L0_u.AND.L0_i
C KONTEYNER_FDA30.fmg( 203, 826):�
      L0_epe = L0_ape.AND.L0_ume
C KONTEYNER_FDA30.fmg( 221, 841):�
      L0_oke = L0_epe.AND.L_ole.AND.L0_ike
C KONTEYNER_FDA30.fmg( 230, 835):�
      if(L_eve) then
         R_ate=R_ite
      else
         R_ate=R0_ete
      endif
C KONTEYNER_FDA30.fmg( 252, 758):���� RE IN LO CH7
      L0_efe=R_exe.lt.R_obe
C KONTEYNER_FDA30.fmg( 216, 705):���������� <
      L0_afe=R_exe.gt.R_ibe
C KONTEYNER_FDA30.fmg( 216, 699):���������� >
      L0_ife=R_axe.lt.R_ofe
C KONTEYNER_FDA30.fmg( 216, 693):���������� <
      R0_abe = R_ebe + (-R_ure)
C KONTEYNER_FDA30.fmg( 204, 686):��������
      L0_ode=R0_abe.lt.R_ux
C KONTEYNER_FDA30.fmg( 216, 686):���������� <
      L0_ide=R0_abe.gt.R_ox
C KONTEYNER_FDA30.fmg( 216, 680):���������� >
      L0_ax=R0_abe.lt.R_uv
C KONTEYNER_FDA30.fmg( 216, 674):���������� <
      L0_ex = L0_ax.AND.L_ole
C KONTEYNER_FDA30.fmg( 222, 670):�
      L_ede=L0_ax.or.(L_ede.and..not.(L0_ex))
      L0_ix=.not.L_ede
C KONTEYNER_FDA30.fmg( 228, 672):RS �������
      L_ude = L0_efe.AND.L0_afe.AND.L0_ife.AND.L0_ode.AND.L0_ide.AND.L_e
     &de
C KONTEYNER_FDA30.fmg( 235, 692):�
      if(L_ude) then
         R_ise=R_ade
      else
         R_ise=R0_ube
      endif
C KONTEYNER_FDA30.fmg( 251, 704):���� RE IN LO CH7
      L0_ev=R_exe.lt.R_os
C KONTEYNER_FDA30.fmg( 218, 649):���������� <
      L0_av=R_exe.gt.R_is
C KONTEYNER_FDA30.fmg( 218, 643):���������� >
      L0_iv=R_axe.lt.R_ov
C KONTEYNER_FDA30.fmg( 218, 637):���������� <
      R0_as = R_es + (-R_ure)
C KONTEYNER_FDA30.fmg( 205, 630):��������
      L0_ot=R0_as.lt.R_ur
C KONTEYNER_FDA30.fmg( 218, 630):���������� <
      L0_it=R0_as.gt.R_or
C KONTEYNER_FDA30.fmg( 218, 624):���������� >
      L0_ar=R0_as.lt.R_up
C KONTEYNER_FDA30.fmg( 218, 618):���������� <
      L0_er = L0_ar.AND.L_ole
C KONTEYNER_FDA30.fmg( 223, 614):�
      L_et=L0_ar.or.(L_et.and..not.(L0_er))
      L0_ir=.not.L_et
C KONTEYNER_FDA30.fmg( 229, 616):RS �������
      L_ut = L0_ev.AND.L0_av.AND.L0_iv.AND.L0_ot.AND.L0_it.AND.L_et
C KONTEYNER_FDA30.fmg( 236, 636):�
      if(L_ut) then
         R_ese=R_at
      else
         R_ese=R0_us
      endif
C KONTEYNER_FDA30.fmg( 252, 648):���� RE IN LO CH7
      R_ose = R_use + R_ate + R_ise + R_ese
C KONTEYNER_FDA30.fmg( 338, 867):��������
      R_ase=R_ase+deltat/R_ek*R_ose
      if(R_ase.gt.R_ak) then
         R_ase=R_ak
      elseif(R_ase.lt.R_ik) then
         R_ase=R_ik
      endif
C KONTEYNER_FDA30.fmg( 352, 856):����������
      R_ure=R_ase
C KONTEYNER_FDA30.fmg( 369, 858):������,VY01
C sav1=R0_ed
      R0_ed = R_ipe + (-R_ure)
C KONTEYNER_FDA30.fmg( 184, 828):recalc:��������
C if(sav1.ne.R0_ed .and. try53.gt.0) goto 53
C sav1=R0_as
      R0_as = R_es + (-R_ure)
C KONTEYNER_FDA30.fmg( 205, 630):recalc:��������
C if(sav1.ne.R0_as .and. try111.gt.0) goto 111
C sav1=R0_abe
      R0_abe = R_ebe + (-R_ure)
C KONTEYNER_FDA30.fmg( 204, 686):recalc:��������
C if(sav1.ne.R0_abe .and. try78.gt.0) goto 78
      R0_ule = R_ame + (-R_ure)
C KONTEYNER_FDA30.fmg( 204, 742):��������
      L0_ave=R0_ule.lt.R_ol
C KONTEYNER_FDA30.fmg( 217, 742):���������� <
      L0_ute=R0_ule.gt.R_il
C KONTEYNER_FDA30.fmg( 217, 736):���������� >
      L0_ale=R0_ule.lt.R_el
C KONTEYNER_FDA30.fmg( 217, 729):���������� <
      L0_ele = L0_ale.AND.L_ole
C KONTEYNER_FDA30.fmg( 222, 725):�
      L_ote=L0_ale.or.(L_ote.and..not.(L0_ele))
      L0_ile=.not.L_ote
C KONTEYNER_FDA30.fmg( 228, 727):RS �������
      L_eve = L0_uve.AND.L0_ove.AND.L0_ive.AND.L0_ave.AND.L0_ute.AND.L_o
     &te
C KONTEYNER_FDA30.fmg( 236, 747):�
      L_uke=L_eve
C KONTEYNER_FDA30.fmg( 273, 747):������,Block1
      L_ake=L_ude
C KONTEYNER_FDA30.fmg( 272, 692):������,Block2
      L_ufe=L_ut
C KONTEYNER_FDA30.fmg( 274, 636):������,Block3
      L_ole = L_uke.OR.L_ake.OR.L_ufe
C KONTEYNER_FDA30.fmg( 346, 838):���
C sav1=L0_oke
      L0_oke = L0_epe.AND.L_ole.AND.L0_ike
C KONTEYNER_FDA30.fmg( 230, 835):recalc:�
C if(sav1.ne.L0_oke .and. try65.gt.0) goto 65
C sav1=L0_ex
      L0_ex = L0_ax.AND.L_ole
C KONTEYNER_FDA30.fmg( 222, 670):recalc:�
C if(sav1.ne.L0_ex .and. try89.gt.0) goto 89
C sav1=L0_er
      L0_er = L0_ar.AND.L_ole
C KONTEYNER_FDA30.fmg( 223, 614):recalc:�
C if(sav1.ne.L0_er .and. try122.gt.0) goto 122
C sav1=L0_ele
      L0_ele = L0_ale.AND.L_ole
C KONTEYNER_FDA30.fmg( 222, 725):recalc:�
C if(sav1.ne.L0_ele .and. try164.gt.0) goto 164
      L0_upe = (.NOT.L_ole).AND.L0_epe
C KONTEYNER_FDA30.fmg( 224, 850):�
      if(L0_upe) then
         R_ore=R_ere
      else
         R_ore=R0_are
      endif
C KONTEYNER_FDA30.fmg( 209, 866):���� RE IN LO CH7
      R_ire=R_ire+deltat/R_of*R_ore
      if(R_ire.gt.R_if) then
         R_ire=R_if
      elseif(R_ire.lt.R_uf) then
         R_ire=R_uf
      endif
C KONTEYNER_FDA30.fmg( 220, 866):����������
      R_exe=R_ire
C KONTEYNER_FDA30.fmg( 238, 867):������,VX01
C sav1=R0_ef
      R0_ef = R_ope + (-R_exe)
C KONTEYNER_FDA30.fmg( 184, 843):recalc:��������
C if(sav1.ne.R0_ef .and. try17.gt.0) goto 17
      L0_eme = L0_upe.OR.L0_oke
C KONTEYNER_FDA30.fmg( 234, 848):���
      if(L0_eme) then
         R_use=R_ome
      else
         R_use=R0_ime
      endif
C KONTEYNER_FDA30.fmg( 212, 858):���� RE IN LO CH7
C sav1=R_ose
      R_ose = R_use + R_ate + R_ise + R_ese
C KONTEYNER_FDA30.fmg( 338, 867):recalc:��������
C if(sav1.ne.R_ose .and. try138.gt.0) goto 138
      L_e=L_ote
C KONTEYNER_FDA30.fmg( 250, 729):������,TRIG_VAL
      if(L_op) then
         R_om=R_ap
      else
         R_om=R0_um
      endif
C KONTEYNER_FDA30.fmg( 204, 587):���� RE IN LO CH7
      End
