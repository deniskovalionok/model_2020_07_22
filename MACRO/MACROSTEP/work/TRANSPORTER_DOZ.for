      Subroutine TRANSPORTER_DOZ(ext_deltat,R_i,R_o,L_u,R_ed
     &,R_id,L_od,R_af,R_ef,L_if,R_uf,R_ak,L_ek,L_ar,L_er,L_or
     &,L_ur,L_as,R_es,R_at,R_et,L_it,L_uv,R_ex,R_ix,L_ox,R_abe
     &,R_ebe,L_ibe,R_ube,R_ade,L_ede,R_ode,R_ude,L_afe,L_ume
     &,L_ape,L_epe,L_upe,L_are,R_ere,R_ase,R_ese,L_ise,L_ose
     &,L_use,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive
     &,L_ove,L_uve,L_axe,L_ixe,L_uxe,L_ebi,L_ibi,L_obi,L_ubi
     &,L_adi,L_edi,L_idi,L_odi,L_udi,L_afi,L_efi,L_ifi,L_ofi
     &,L_ufi,L_aki,R_eki,R_iki,R_oki,R_uki,R_ali,R_eli,R_ili
     &,R_oli,R_uli,R_ami,L_emi,L_imi,L_omi,L_umi,L_api,L_epi
     &,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi
     &,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti,L_oti,L_uti
     &,L_avi,L_evi,L_ivi,L_ovi,L_uvi,L_axi,L_exi,L_oxi,L_abo
     &,L_ibo,L_obo,L_ubo,L_ado,L_edo,L_ido,L_odo,L_udo,L_afo
     &,L_efo,L_ifo,L_ofo,L_ufo,L_ako,L_eko,L_iko,L_oko,L_uko
     &)
C |R_i           |4 4 S|_simpinitimpv4* |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpinitimpv4  |[���]������������ �������� �������������|0.4|
C |L_u           |1 1 S|_limpinitimpv4* |[TF]���������� ��������� ������������� |F|
C |R_ed          |4 4 S|_simpinitimpv3* |[���]���������� ��������� ������������� |0.0|
C |R_id          |4 4 K|_timpinitimpv3  |[���]������������ �������� �������������|0.4|
C |L_od          |1 1 S|_limpinitimpv3* |[TF]���������� ��������� ������������� |F|
C |R_af          |4 4 S|_simpinitimpv2* |[���]���������� ��������� ������������� |0.0|
C |R_ef          |4 4 K|_timpinitimpv2  |[���]������������ �������� �������������|0.4|
C |L_if          |1 1 S|_limpinitimpv2* |[TF]���������� ��������� ������������� |F|
C |R_uf          |4 4 S|_simpinitimpv1* |[���]���������� ��������� ������������� |0.0|
C |R_ak          |4 4 K|_timpinitimpv1  |[���]������������ �������� �������������|0.4|
C |L_ek          |1 1 S|_limpinitimpv1* |[TF]���������� ��������� ������������� |F|
C |L_ar          |1 1 O|C7_stop_avt     |����||
C |L_er          |1 1 O|C8_stop_avt     |����||
C |L_or          |1 1 O|C6_stop_avt     |����||
C |L_ur          |1 1 S|_qfftrinitv*    |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 O|VINITCMPLT      |||
C |R_es          |4 4 O|halfy           |||
C |R_at          |4 4 I|miny            |||
C |R_et          |4 4 I|maxy            |||
C |L_it          |1 1 I|VINIT           |������� � �������� �� ���������||
C |L_uv          |1 1 I|DOWN            |������� ����||
C |R_ex          |4 4 S|_simpinitimph4* |[���]���������� ��������� ������������� |0.0|
C |R_ix          |4 4 K|_timpinitimph4  |[���]������������ �������� �������������|0.4|
C |L_ox          |1 1 S|_limpinitimph4* |[TF]���������� ��������� ������������� |F|
C |R_abe         |4 4 S|_simpinitimph3* |[���]���������� ��������� ������������� |0.0|
C |R_ebe         |4 4 K|_timpinitimph3  |[���]������������ �������� �������������|0.4|
C |L_ibe         |1 1 S|_limpinitimph3* |[TF]���������� ��������� ������������� |F|
C |R_ube         |4 4 S|_simpinitimph2* |[���]���������� ��������� ������������� |0.0|
C |R_ade         |4 4 K|_timpinitimph2  |[���]������������ �������� �������������|0.4|
C |L_ede         |1 1 S|_limpinitimph2* |[TF]���������� ��������� ������������� |F|
C |R_ode         |4 4 S|_simpinitimph1* |[���]���������� ��������� ������������� |0.0|
C |R_ude         |4 4 K|_timpinitimph1  |[���]������������ �������� �������������|0.4|
C |L_afe         |1 1 S|_limpinitimph1* |[TF]���������� ��������� ������������� |F|
C |L_ume         |1 1 O|C5_stop_avt     |����||
C |L_ape         |1 1 O|C4_stop_avt     |����||
C |L_epe         |1 1 O|C3_stop_avt     |����||
C |L_upe         |1 1 S|_qfftrinith*    |�������� ������ Q RS-��������  |F|
C |L_are         |1 1 O|INITCMPLT       |||
C |R_ere         |4 4 O|halfx           |||
C |R_ase         |4 4 I|minx            |||
C |R_ese         |4 4 I|maxx            |||
C |L_ise         |1 1 I|INIT            |INIT �� �����������||
C |L_ose         |1 1 O|C7_YA25         |������� �����||
C |L_use         |1 1 O|C6_YA25         |������� �����||
C |L_ate         |1 1 O|C8_YA25         |������� � �������||
C |L_ete         |1 1 I|C7YA25_B        |������� �����||
C |L_ite         |1 1 I|C6YA25_B        |������� �����||
C |L_ote         |1 1 I|C8YA25_B        |������� � �������||
C |L_ute         |1 1 I|UP              |������� �����||
C |L_ave         |1 1 O|C6_YA26         |������� ����||
C |L_eve         |1 1 O|C7_YA26         |������� ����||
C |L_ive         |1 1 O|C8_YA26         |������� � �������||
C |L_ove         |1 1 I|C7YA26_B        |������� ����||
C |L_uve         |1 1 I|C6YA26_B        |������� ����||
C |L_axe         |1 1 I|C8YA26_B        |������� � �������||
C |L_ixe         |1 1 O|C5_YA25         |||
C |L_uxe         |1 1 O|C4_YA25         |||
C |L_ebi         |1 1 O|C3_YA25         |||
C |L_ibi         |1 1 I|C5YA25_B        |������� �����||
C |L_obi         |1 1 I|C4YA25_B        |������� �����||
C |L_ubi         |1 1 I|C3YA25_B        |������� �����||
C |L_adi         |1 1 I|PREV            |������� � ���������� ����||
C |L_edi         |1 1 O|VZZ02           |��������� ������� �����||
C |L_idi         |1 1 O|VZZ01           |��������� ������� ������||
C |L_odi         |1 1 O|L11             |������ ������||
C |L_udi         |1 1 O|L12             |��������� ����������||
C |L_afi         |1 1 I|C10XH54         |��������� ����������||
C |L_efi         |1 1 O|C11_uluop       |������� ������||
C |L_ifi         |1 1 O|C11_ulucl       |������� ������||
C |L_ofi         |1 1 I|C11_CBC         |������ ������||
C |L_ufi         |1 1 I|C11B            |�������/������� ������||
C |L_aki         |1 1 I|C11_CBO         |������ ������||
C |R_eki         |4 4 O|VX01            |��������� ������������||
C |R_iki         |4 4 O|VS01            |��������� �����������||
C |R_oki         |4 4 O|VZ01            |��������� ������������||
C |R_uki         |4 4 I|C10_VZ01        |��������� �����������||
C |R_ali         |4 4 I|C8_VX01         |��������� �������||
C |R_eli         |4 4 I|C7_VX01         |��������� �������||
C |R_ili         |4 4 I|C6_VX01         |��������� �������||
C |R_oli         |4 4 I|C5_VX01         |��������� �������||
C |R_uli         |4 4 I|C4_VX01         |��������� �������||
C |R_ami         |4 4 I|C3_VX01         |��������� �������||
C |L_emi         |1 1 O|L9              |���� ���� - �����||
C |L_imi         |1 1 O|L10             |���� ���� - �����||
C |L_omi         |1 1 O|L8              |������ ������||
C |L_umi         |1 1 O|L7              |��������� ������||
C |L_api         |1 1 O|L6_input        |���� ���� - �������||
C |L_epi         |1 1 O|L4              |��������� ����||
C |L_ipi         |1 1 O|L5              |���� ���� - �����||
C |L_opi         |1 1 O|L3              |�������� ���������||
C |L_upi         |1 1 O|L1_input        |���������� ����||
C |L_ari         |1 1 O|L2              |���������� ����||
C |L_eri         |1 1 O|L10_input       |���� ���� - �����||
C |L_iri         |1 1 O|L9_input        |���� ���� - �����||
C |L_ori         |1 1 O|L6              |���� ���� - �������||
C |L_uri         |1 1 O|L5_input        |���� ���� - �����||
C |L_asi         |1 1 O|L1              |���������� ����||
C |L_esi         |1 1 O|L4_input        |��������� ����||
C |L_isi         |1 1 O|L3_input        |�������� ���������||
C |L_osi         |1 1 O|L2_input        |���������� ����||
C |L_usi         |1 1 I|C7_XH54         |������� ��������||
C |L_ati         |1 1 I|C6_XH54         |������� ��������||
C |L_eti         |1 1 I|C4_XH54         |����� ��������||
C |L_iti         |1 1 I|C3_XH54         |����� ��������||
C |L_oti         |1 1 I|C5_XH54         |����� ��������||
C |L_uti         |1 1 I|C8_XH53         |������ ��������||
C |L_avi         |1 1 I|C7_XH53         |������ ��������||
C |L_evi         |1 1 I|C6_XH53         |������ ��������||
C |L_ivi         |1 1 I|C5_XH53         |������ ��������||
C |L_ovi         |1 1 I|C4_XH53         |������ ��������||
C |L_uvi         |1 1 I|C3_XH53         |������ ��������||
C |L_axi         |1 1 O|C2_uluop        |������� ������||
C |L_exi         |1 1 O|C2_ulucl        |������� ������||
C |L_oxi         |1 1 O|C5_YA26         |||
C |L_abo         |1 1 O|C4_YA26         |||
C |L_ibo         |1 1 O|C3_YA26         |||
C |L_obo         |1 1 O|C1_uluop        |������� ����||
C |L_ubo         |1 1 O|C1_ulucl        |������� ����||
C |L_ado         |1 1 O|C10_YA26        |��������� ������� ����||
C |L_edo         |1 1 O|C10_YA25        |��������� ������� �����||
C |L_ido         |1 1 I|C10B            |��������� ����������||
C |L_odo         |1 1 I|C5YA26_B        |������� ������||
C |L_udo         |1 1 I|C4YA26_B        |������� ������||
C |L_afo         |1 1 I|NEXT            |������� � ��������� ����||
C |L_efo         |1 1 I|C3YA26_B        |������� ������||
C |L_ifo         |1 1 I|C2_CBC          |������ ������||
C |L_ofo         |1 1 I|C2B             |�������/������� ������||
C |L_ufo         |1 1 I|C2_CBO          |������ ������||
C |L_ako         |1 1 I|C1_CBC          |���� ������||
C |L_eko         |1 1 I|C1B             |�������/������� ����||
C |L_iko         |1 1 I|C1_CBO          |���� ������||
C |L_oko         |1 1 I|C10_XH53        |��������� �� ����������||
C |L_uko         |1 1 I|C10_XH54        |��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R0_ad,R_ed,R_id
      LOGICAL*1 L_od
      REAL*4 R0_ud,R_af,R_ef
      LOGICAL*1 L_if
      REAL*4 R0_of,R_uf,R_ak
      LOGICAL*1 L_ek,L0_ik,L0_ok,L0_uk,L0_al
      REAL*4 R0_el
      LOGICAL*1 L0_il
      REAL*4 R0_ol
      LOGICAL*1 L0_ul
      REAL*4 R0_am
      LOGICAL*1 L0_em
      REAL*4 R0_im
      LOGICAL*1 L0_om
      REAL*4 R0_um
      LOGICAL*1 L0_ap
      REAL*4 R0_ep,R0_ip,R0_op,R0_up
      LOGICAL*1 L_ar,L_er,L0_ir,L_or,L_ur,L_as
      REAL*4 R_es,R0_is,R0_os,R0_us,R_at,R_et
      LOGICAL*1 L_it,L0_ot,L0_ut,L0_av,L0_ev,L0_iv,L0_ov,L_uv
      REAL*4 R0_ax,R_ex,R_ix
      LOGICAL*1 L_ox
      REAL*4 R0_ux,R_abe,R_ebe
      LOGICAL*1 L_ibe
      REAL*4 R0_obe,R_ube,R_ade
      LOGICAL*1 L_ede
      REAL*4 R0_ide,R_ode,R_ude
      LOGICAL*1 L_afe,L0_efe,L0_ife,L0_ofe,L0_ufe
      REAL*4 R0_ake
      LOGICAL*1 L0_eke
      REAL*4 R0_ike
      LOGICAL*1 L0_oke
      REAL*4 R0_uke
      LOGICAL*1 L0_ale
      REAL*4 R0_ele
      LOGICAL*1 L0_ile
      REAL*4 R0_ole
      LOGICAL*1 L0_ule
      REAL*4 R0_ame,R0_eme,R0_ime,R0_ome
      LOGICAL*1 L_ume,L_ape,L_epe,L0_ipe,L0_ope,L_upe,L_are
      REAL*4 R_ere,R0_ire,R0_ore,R0_ure,R_ase,R_ese
      LOGICAL*1 L_ise,L_ose,L_use,L_ate,L_ete,L_ite,L_ote
     &,L_ute,L_ave,L_eve,L_ive,L_ove,L_uve,L_axe,L0_exe,L_ixe
     &,L0_oxe,L_uxe,L0_abi,L_ebi,L_ibi,L_obi
      LOGICAL*1 L_ubi,L_adi,L_edi,L_idi,L_odi,L_udi,L_afi
     &,L_efi,L_ifi,L_ofi,L_ufi,L_aki
      REAL*4 R_eki,R_iki,R_oki,R_uki,R_ali,R_eli,R_ili,R_oli
     &,R_uli,R_ami
      LOGICAL*1 L_emi,L_imi,L_omi,L_umi,L_api,L_epi,L_ipi
     &,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi,L_esi
     &,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti
      LOGICAL*1 L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,L_axi,L_exi,L0_ixi,L_oxi,L0_uxi,L_abo,L0_ebo,L_ibo
     &,L_obo,L_ubo,L_ado,L_edo,L_ido,L_odo,L_udo
      LOGICAL*1 L_afo,L_efo,L_ifo,L_ofo,L_ufo,L_ako,L_eko
     &,L_iko,L_oko,L_uko

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e=R_i
C TRANSPORTER_DOZ.fmg( 213, 206):pre: ������������  �� T,initimpv4
      R0_ud=R_af
C TRANSPORTER_DOZ.fmg( 213, 222):pre: ������������  �� T,initimpv2
      R0_ad=R_ed
C TRANSPORTER_DOZ.fmg( 213, 214):pre: ������������  �� T,initimpv3
      R0_ax=R_ex
C TRANSPORTER_DOZ.fmg(  98, 206):pre: ������������  �� T,initimph4
      R0_ux=R_abe
C TRANSPORTER_DOZ.fmg(  98, 214):pre: ������������  �� T,initimph3
      R0_obe=R_ube
C TRANSPORTER_DOZ.fmg(  98, 222):pre: ������������  �� T,initimph2
      R0_ide=R_ode
C TRANSPORTER_DOZ.fmg(  36, 242):pre: ������������  �� T,initimph1
      R0_of=R_uf
C TRANSPORTER_DOZ.fmg( 161, 235):pre: ������������  �� T,initimpv1
      R_eki=MAX(R_ami,R_uli,R_oli)
C TRANSPORTER_DOZ.fmg( 376, 274):��������
      if(L_it.and..not.L_ek) then
         R_uf=R_ak
      else
         R_uf=max(R0_of-deltat,0.0)
      endif
      L0_ir=R_uf.gt.0.0
      L_ek=L_it
C TRANSPORTER_DOZ.fmg( 161, 235):������������  �� T,initimpv1
      R0_el = -20
C TRANSPORTER_DOZ.fmg( 181, 202):��������� (RE4) (�������)
      R0_ol = 20
C TRANSPORTER_DOZ.fmg( 181, 206):��������� (RE4) (�������)
      R0_am = -20
C TRANSPORTER_DOZ.fmg( 181, 210):��������� (RE4) (�������)
      R0_im = 20
C TRANSPORTER_DOZ.fmg( 181, 214):��������� (RE4) (�������)
      R0_um = -20
C TRANSPORTER_DOZ.fmg( 181, 218):��������� (RE4) (�������)
      R0_ep = 20
C TRANSPORTER_DOZ.fmg( 181, 222):��������� (RE4) (�������)
      R0_is = 0.5
C TRANSPORTER_DOZ.fmg( 158, 222):��������� (RE4) (�������)
      R0_us = (-R_at) + R_et
C TRANSPORTER_DOZ.fmg( 154, 224):��������
      R0_os = R0_us * R0_is
C TRANSPORTER_DOZ.fmg( 162, 224):����������
      R_es = R_at + R0_os
C TRANSPORTER_DOZ.fmg( 167, 224):��������
      R0_ip = (-R_es) + R_eli
C TRANSPORTER_DOZ.fmg( 174, 204):��������
      L0_al=R0_ip.lt.R0_el
C TRANSPORTER_DOZ.fmg( 186, 204):���������� <
      L0_il=R0_ip.gt.R0_ol
C TRANSPORTER_DOZ.fmg( 186, 208):���������� >
      R0_op = (-R_es) + R_ili
C TRANSPORTER_DOZ.fmg( 174, 212):��������
      L0_ul=R0_op.lt.R0_am
C TRANSPORTER_DOZ.fmg( 186, 212):���������� <
      L0_em=R0_op.gt.R0_im
C TRANSPORTER_DOZ.fmg( 186, 216):���������� >
      R0_up = (-R_es) + R_ali
C TRANSPORTER_DOZ.fmg( 174, 220):��������
      L0_om=R0_up.lt.R0_um
C TRANSPORTER_DOZ.fmg( 186, 220):���������� <
      L0_ap=R0_up.gt.R0_ep
C TRANSPORTER_DOZ.fmg( 186, 224):���������� >
      if(L_ise.and..not.L_afe) then
         R_ode=R_ude
      else
         R_ode=max(R0_ide-deltat,0.0)
      endif
      L0_ipe=R_ode.gt.0.0
      L_afe=L_ise
C TRANSPORTER_DOZ.fmg(  36, 242):������������  �� T,initimph1
      R0_ake = -20
C TRANSPORTER_DOZ.fmg(  62, 202):��������� (RE4) (�������)
      R0_ike = 20
C TRANSPORTER_DOZ.fmg(  62, 206):��������� (RE4) (�������)
      R0_uke = -20
C TRANSPORTER_DOZ.fmg(  62, 210):��������� (RE4) (�������)
      R0_ele = 20
C TRANSPORTER_DOZ.fmg(  62, 214):��������� (RE4) (�������)
      R0_ole = -20
C TRANSPORTER_DOZ.fmg(  62, 218):��������� (RE4) (�������)
      R0_ame = 20
C TRANSPORTER_DOZ.fmg(  62, 222):��������� (RE4) (�������)
      R0_ire = 0.5
C TRANSPORTER_DOZ.fmg(  39, 222):��������� (RE4) (�������)
      R0_ure = (-R_ase) + R_ese
C TRANSPORTER_DOZ.fmg(  34, 224):��������
      R0_ore = R0_ure * R0_ire
C TRANSPORTER_DOZ.fmg(  42, 224):����������
      R_ere = R_ase + R0_ore
C TRANSPORTER_DOZ.fmg(  48, 224):��������
      R0_eme = (-R_ere) + R_oli
C TRANSPORTER_DOZ.fmg(  54, 204):��������
      L0_ufe=R0_eme.lt.R0_ake
C TRANSPORTER_DOZ.fmg(  66, 204):���������� <
      L0_eke=R0_eme.gt.R0_ike
C TRANSPORTER_DOZ.fmg(  66, 208):���������� >
      R0_ime = (-R_ere) + R_uli
C TRANSPORTER_DOZ.fmg(  54, 212):��������
      L0_oke=R0_ime.lt.R0_uke
C TRANSPORTER_DOZ.fmg(  66, 212):���������� <
      L0_ale=R0_ime.gt.R0_ele
C TRANSPORTER_DOZ.fmg(  66, 216):���������� >
      R0_ome = (-R_ere) + R_ami
C TRANSPORTER_DOZ.fmg(  54, 220):��������
      L0_ile=R0_ome.lt.R0_ole
C TRANSPORTER_DOZ.fmg(  66, 220):���������� <
      L0_ule=R0_ome.gt.R0_ame
C TRANSPORTER_DOZ.fmg(  66, 224):���������� >
      L_efi = L_ufi.AND.L_ofi
C TRANSPORTER_DOZ.fmg(  44, 157):�
      L_ifi = L_aki.AND.L_ufi
C TRANSPORTER_DOZ.fmg(  44, 163):�
      L_axi = L_ofo.AND.L_ifo
C TRANSPORTER_DOZ.fmg(  44, 172):�
      L_exi = L_ufo.AND.L_ofo
C TRANSPORTER_DOZ.fmg(  44, 178):�
      L_edi=L_ati
C TRANSPORTER_DOZ.fmg( 404, 184):������,VZZ02
      L_idi=L_evi
C TRANSPORTER_DOZ.fmg( 404, 188):������,VZZ01
      L_odi=L_aki
C TRANSPORTER_DOZ.fmg( 329, 170):������,L11
      L_udi=L_afi
C TRANSPORTER_DOZ.fmg( 329, 174):������,L12
      R_iki=R_uki
C TRANSPORTER_DOZ.fmg( 404, 204):������,VS01
      R_oki=R_ili
C TRANSPORTER_DOZ.fmg( 393, 247):������,VZ01
      L_omi=L_ufo
C TRANSPORTER_DOZ.fmg( 329, 178):������,L8
      L_umi=L_iko
C TRANSPORTER_DOZ.fmg( 329, 182):������,L7
      L_esi = L_iti.AND.L_eti.AND.L_oti
C TRANSPORTER_DOZ.fmg( 300, 200):�
      L_epi=L_esi
C TRANSPORTER_DOZ.fmg( 329, 200):������,L4
      L_ori = L_esi.AND.L_ati
C TRANSPORTER_DOZ.fmg( 308, 188):�
      L_api=L_ori
C TRANSPORTER_DOZ.fmg( 329, 192):������,L6_input
      L_uri = L_evi.AND.L_esi
C TRANSPORTER_DOZ.fmg( 308, 212):�
      L_ipi=L_uri
C TRANSPORTER_DOZ.fmg( 329, 212):������,L5
      L_iri = L_uti.AND.L_uri
C TRANSPORTER_DOZ.fmg( 316, 220):�
      L_emi=L_iri
C TRANSPORTER_DOZ.fmg( 329, 220):������,L9
      L_isi =.NOT.(L_uvi.OR.L_iti.OR.L_evi.OR.L_ati)
C TRANSPORTER_DOZ.fmg( 303, 243):���
      L_opi=L_isi
C TRANSPORTER_DOZ.fmg( 329, 243):������,L3
      L_osi = L_uvi.AND.L_ovi.AND.L_ivi
C TRANSPORTER_DOZ.fmg( 303, 275):�
      L_ari=L_osi
C TRANSPORTER_DOZ.fmg( 329, 275):������,L2
      L_asi = L_osi.AND.L_evi
C TRANSPORTER_DOZ.fmg( 310, 262):�
      L_upi=L_asi
C TRANSPORTER_DOZ.fmg( 329, 266):������,L1_input
      L_eri = L_asi.AND.L_uti
C TRANSPORTER_DOZ.fmg( 316, 258):�
      L_imi=L_eri
C TRANSPORTER_DOZ.fmg( 329, 258):������,L10
      L_obo = L_eko.AND.L_ako
C TRANSPORTER_DOZ.fmg(  44, 185):�
      L_ubo = L_iko.AND.L_eko
C TRANSPORTER_DOZ.fmg(  44, 191):�
      L_ado = L_ido.AND.L_oko
C TRANSPORTER_DOZ.fmg( 382, 160):�
      L_edo = L_uko.AND.L_ido
C TRANSPORTER_DOZ.fmg( 382, 166):�
      L0_abi = L_upe.AND.L0_ule
C TRANSPORTER_DOZ.fmg(  74, 224):�
C label 162  try162=try162-1
      L0_exe = L_upe.AND.L0_eke
C TRANSPORTER_DOZ.fmg(  74, 208):�
      L0_ixi = L_upe.AND.L0_ufe
C TRANSPORTER_DOZ.fmg(  74, 204):�
      L0_efe = (.NOT.L0_exe).AND.L_upe.AND.(.NOT.L0_ixi)
C TRANSPORTER_DOZ.fmg(  86, 206):�
      L0_oxe = L_upe.AND.L0_ale
C TRANSPORTER_DOZ.fmg(  74, 216):�
      L0_uxi = L_upe.AND.L0_oke
C TRANSPORTER_DOZ.fmg(  74, 212):�
      L0_ife = (.NOT.L0_oxe).AND.L_upe.AND.(.NOT.L0_uxi)
C TRANSPORTER_DOZ.fmg(  86, 214):�
      L0_ebo = L_upe.AND.L0_ile
C TRANSPORTER_DOZ.fmg(  74, 220):�
      L0_ofe = (.NOT.L0_abi).AND.L_upe.AND.(.NOT.L0_ebo)
C TRANSPORTER_DOZ.fmg(  86, 222):�
      L0_ope = L0_efe.AND.L0_ife.AND.L0_ofe
C TRANSPORTER_DOZ.fmg(  97, 241):�
      L_upe=(L0_ipe.or.L_upe).and..not.(L0_ope)
      L_are=.not.L_upe
C TRANSPORTER_DOZ.fmg(  50, 240):RS �������,trinith
C sav1=L0_exe
      L0_exe = L_upe.AND.L0_eke
C TRANSPORTER_DOZ.fmg(  74, 208):recalc:�
C if(sav1.ne.L0_exe .and. try165.gt.0) goto 165
C sav1=L0_uxi
      L0_uxi = L_upe.AND.L0_oke
C TRANSPORTER_DOZ.fmg(  74, 212):recalc:�
C if(sav1.ne.L0_uxi .and. try177.gt.0) goto 177
C sav1=L0_oxe
      L0_oxe = L_upe.AND.L0_ale
C TRANSPORTER_DOZ.fmg(  74, 216):recalc:�
C if(sav1.ne.L0_oxe .and. try174.gt.0) goto 174
C sav1=L0_ebo
      L0_ebo = L_upe.AND.L0_ile
C TRANSPORTER_DOZ.fmg(  74, 220):recalc:�
C if(sav1.ne.L0_ebo .and. try183.gt.0) goto 183
C sav1=L0_abi
      L0_abi = L_upe.AND.L0_ule
C TRANSPORTER_DOZ.fmg(  74, 224):recalc:�
C if(sav1.ne.L0_abi .and. try162.gt.0) goto 162
C sav1=L0_ife
      L0_ife = (.NOT.L0_oxe).AND.L_upe.AND.(.NOT.L0_uxi)
C TRANSPORTER_DOZ.fmg(  86, 214):recalc:�
C if(sav1.ne.L0_ife .and. try180.gt.0) goto 180
C sav1=L0_ofe
      L0_ofe = (.NOT.L0_abi).AND.L_upe.AND.(.NOT.L0_ebo)
C TRANSPORTER_DOZ.fmg(  86, 222):recalc:�
C if(sav1.ne.L0_ofe .and. try186.gt.0) goto 186
C sav1=L0_efe
      L0_efe = (.NOT.L0_exe).AND.L_upe.AND.(.NOT.L0_ixi)
C TRANSPORTER_DOZ.fmg(  86, 206):recalc:�
C if(sav1.ne.L0_efe .and. try171.gt.0) goto 171
C sav1=L0_ixi
      L0_ixi = L_upe.AND.L0_ufe
C TRANSPORTER_DOZ.fmg(  74, 204):recalc:�
C if(sav1.ne.L0_ixi .and. try168.gt.0) goto 168
      if(L0_ofe.and..not.L_ede) then
         R_ube=R_ade
      else
         R_ube=max(R0_obe-deltat,0.0)
      endif
      L_epe=R_ube.gt.0.0
      L_ede=L0_ofe
C TRANSPORTER_DOZ.fmg(  98, 222):������������  �� T,initimph2
      L_ibo = L_afo.OR.L_efo.OR.L0_ebo
C TRANSPORTER_DOZ.fmg(  97, 280):���
      if(L0_ife.and..not.L_ibe) then
         R_abe=R_ebe
      else
         R_abe=max(R0_ux-deltat,0.0)
      endif
      L_ape=R_abe.gt.0.0
      L_ibe=L0_ife
C TRANSPORTER_DOZ.fmg(  98, 214):������������  �� T,initimph3
      L_abo = L_afo.OR.L_udo.OR.L0_uxi
C TRANSPORTER_DOZ.fmg(  97, 274):���
      L_uxe = L_adi.OR.L_obi.OR.L0_oxe
C TRANSPORTER_DOZ.fmg(  97, 253):���
      if(L0_efe.and..not.L_ox) then
         R_ex=R_ix
      else
         R_ex=max(R0_ax-deltat,0.0)
      endif
      L_ume=R_ex.gt.0.0
      L_ox=L0_efe
C TRANSPORTER_DOZ.fmg(  98, 206):������������  �� T,initimph4
      L_oxi = L_afo.OR.L_odo.OR.L0_ixi
C TRANSPORTER_DOZ.fmg(  97, 268):���
      L_ixe = L_adi.OR.L_ibi.OR.L0_exe
C TRANSPORTER_DOZ.fmg(  97, 247):���
      L_ebi = L_adi.OR.L_ubi.OR.L0_abi
C TRANSPORTER_DOZ.fmg(  97, 259):���
      L_ur=(L0_ir.or.L_ur).and..not.(L_or)
      L_as=.not.L_ur
C TRANSPORTER_DOZ.fmg( 170, 233):RS �������,trinitv
C label 229  try229=try229-1
      L0_ut = L_ur.AND.L0_em
C TRANSPORTER_DOZ.fmg( 194, 216):�
      L0_iv = L_ur.AND.L0_ul
C TRANSPORTER_DOZ.fmg( 194, 212):�
      L0_ok = (.NOT.L0_ut).AND.L_ur.AND.(.NOT.L0_iv)
C TRANSPORTER_DOZ.fmg( 205, 214):�
      if(L0_ok.and..not.L_od) then
         R_ed=R_id
      else
         R_ed=max(R0_ad-deltat,0.0)
      endif
      L_or=R_ed.gt.0.0
      L_od=L0_ok
C TRANSPORTER_DOZ.fmg( 213, 214):������������  �� T,initimpv3
      L_ave = L_uv.OR.L_uve.OR.L0_iv
C TRANSPORTER_DOZ.fmg( 212, 266):���
      L_use = L_ute.OR.L_ite.OR.L0_ut
C TRANSPORTER_DOZ.fmg( 212, 246):���
      L0_ev = L_ur.AND.L0_al
C TRANSPORTER_DOZ.fmg( 194, 204):�
      L_eve = L_uv.OR.L_ove.OR.L0_ev
C TRANSPORTER_DOZ.fmg( 212, 260):���
      L0_av = L_ur.AND.L0_ap
C TRANSPORTER_DOZ.fmg( 194, 224):�
      L_ate = L_ute.OR.L_ote.OR.L0_av
C TRANSPORTER_DOZ.fmg( 212, 252):���
      L0_ov = L_ur.AND.L0_om
C TRANSPORTER_DOZ.fmg( 194, 220):�
      L_ive = L_uv.OR.L_axe.OR.L0_ov
C TRANSPORTER_DOZ.fmg( 212, 272):���
      L0_uk = (.NOT.L0_av).AND.L_ur.AND.(.NOT.L0_ov)
C TRANSPORTER_DOZ.fmg( 205, 222):�
      if(L0_uk.and..not.L_if) then
         R_af=R_ef
      else
         R_af=max(R0_ud-deltat,0.0)
      endif
      L_er=R_af.gt.0.0
      L_if=L0_uk
C TRANSPORTER_DOZ.fmg( 213, 222):������������  �� T,initimpv2
      L0_ot = L_ur.AND.L0_il
C TRANSPORTER_DOZ.fmg( 194, 208):�
      L_ose = L_ute.OR.L_ete.OR.L0_ot
C TRANSPORTER_DOZ.fmg( 212, 240):���
      L0_ik = (.NOT.L0_ot).AND.L_ur.AND.(.NOT.L0_ev)
C TRANSPORTER_DOZ.fmg( 205, 206):�
      if(L0_ik.and..not.L_u) then
         R_i=R_o
      else
         R_i=max(R0_e-deltat,0.0)
      endif
      L_ar=R_i.gt.0.0
      L_u=L0_ik
C TRANSPORTER_DOZ.fmg( 213, 206):������������  �� T,initimpv4
      End
