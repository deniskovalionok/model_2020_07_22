      Interface
      Subroutine TYAGA_OY_HANDLER(ext_deltat,R_udi,R_e,R_ad
     &,R_ud,R_ok,R_al,R_el,R_il,R_ol,L_es,R_it,R_ade,L_ede
     &,R_ode,L_ude,L_ake,L_oke,R_ome,R_ipe,L_upe,R_ere,L_ore
     &,R_ure,R_ase,L_ise,L_ite,L_ute,L_ave,L_ive,L_axe,L_oxe
     &,L_abi,R_afi,L_ofi,L_aki,L_iki,L_oki,L_eli,L_ili,C20_opi
     &,L_upi,L_eri,L_iri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,L_axi,L_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,I_ibo,R_ado
     &,L_odo,R_afo,C20_ilo,C20_imo,I_epo,I_upo,R_aro,R_ero
     &,R_iro,R_oro,L_uro,L_aso,I_eso,I_uso,I_uto,I_ivo,C20_ixo
     &,C8_ibu,R_aku,R_eku,L_iku,R_oku,R_uku,I_alu,L_olu,L_ulu
     &,L_amu,I_imu,I_apu,L_aru,L_iru,L_oru,L_uru,L_asu,L_esu
     &,L_etu,L_evu,L_ovu,L_uvu,R8_exu,R_abad,R8_obad,L_edad
     &,L_udad,I_afad,L_ofad,L_ufad,L_ukad,L_imad,L_ipad)
C |R_udi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 K|_cJ3901         |�������� ��������� ����������|`p_UP`|
C |R_ad          |4 4 K|_cJ3896         |�������� ��������� ����������|`p_LOW`|
C |R_ud          |4 4 S|_slpbJ3889*     |���������� ��������� ||
C |R_ok          |4 4 I|p_TOLKATEL_XH54 |||
C |R_al          |4 4 S|_slpbJ3850*     |���������� ��������� ||
C |R_el          |4 4 S|_slpbJ3849*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3848*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3842*     |���������� ��������� ||
C |L_es          |1 1 S|_qffJ3798*      |�������� ������ Q RS-��������  |F|
C |R_it          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ade         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ede         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ode         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ude         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ake         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_oke         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ome         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ipe         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_upe         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ere         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ore         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ure         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |R_ase         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_ise         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ite         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ute         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 I|OUTC            |�����||
C |L_ive         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_axe         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_oxe         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_abi         |1 1 I|OUTO            |������||
C |R_afi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ofi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_aki         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_iki         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_eli         |1 1 O|limit_switch_error|||
C |L_ili         |1 1 O|flag_mlf19      |||
C |C20_opi       |3 20 O|task_state      |���������||
C |L_upi         |1 1 I|vlv_kvit        |||
C |L_eri         |1 1 I|instr_fault     |||
C |L_iri         |1 1 S|_qffJ3427*      |�������� ������ Q RS-��������  |F|
C |L_asi         |1 1 O|norm            |�����||
C |L_esi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_isi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_osi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_usi         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_ati         |1 1 I|mlf07           |���������� ���� �������||
C |L_eti         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_iti         |1 1 I|mlf09           |����� ��������� �����||
C |L_oti         |1 1 I|mlf08           |����� ��������� ������||
C |L_uti         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_avi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_evi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_ivi         |1 1 I|mlf23           |������� ������� �����||
C |L_ovi         |1 1 I|mlf22           |����� ����� ��������||
C |L_uvi         |1 1 I|mlf19           |���� ������������||
C |L_axi         |1 1 I|YA25C           |������� ������� �� ����������|F|
C |L_exi         |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_ixi         |4 4 I|tcl_top         |�������� �������|20|
C |R_oxi         |4 4 K|_uintT_INT      |����������� ������ ����������� ������|`p_UP`|
C |R_uxi         |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_abo         |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_START`|
C |R_ebo         |4 4 K|_lintT_INT      |����������� ������ ����������� �����|`p_LOW`|
C |I_ibo         |2 4 O|LWORK           |����� �������||
C |R_ado         |4 4 O|VX01            |��������� ��������� �� ��� X||
C |L_odo         |1 1 I|mlf04           |���������������� �������� �����||
C |R_afo         |4 4 O|VX02            |�������� ����������� ���������||
C |C20_ilo       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_imo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_epo         |2 4 O|LREADY          |����� ����������||
C |I_upo         |2 4 O|LBUSY           |����� �����||
C |R_aro         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_ero         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_iro         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_oro         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_uro         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_aso         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_eso         |2 4 O|state1          |��������� 1||
C |I_uso         |2 4 O|state2          |��������� 2||
C |I_uto         |2 4 O|LWORKO          |����� � �������||
C |I_ivo         |2 4 O|LINITC          |����� � ��������||
C |C20_ixo       |3 20 O|task_name       |������� ���������||
C |C8_ibu        |3 8 I|task            |������� ���������||
C |R_aku         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_eku         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_iku         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_oku         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_uku         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_alu         |2 4 O|LERROR          |����� �������������||
C |L_olu         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_ulu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_amu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_imu         |2 4 O|LINIT           |����� ��������||
C |I_apu         |2 4 O|LZM             |������ "�������"||
C |L_aru         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_iru         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_oru         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_uru         |1 1 I|mlf05           |������������� ������� "������"||
C |L_asu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_esu         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_etu         |1 1 O|block           |||
C |L_evu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ovu         |1 1 O|STOP            |�������||
C |L_uvu         |1 1 O|nopower         |��� ����������||
C |R8_exu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_abad        |4 4 I|power           |�������� ��������||
C |R8_obad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_edad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_udad        |1 1 O|fault           |�������������||
C |I_afad        |2 4 O|LAM             |������ "�������"||
C |L_ofad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_ufad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_ukad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_imad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ipad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_ad,R_ud,R_ok,R_al,R_el,R_il,R_ol
      LOGICAL*1 L_es
      REAL*4 R_it,R_ade
      LOGICAL*1 L_ede
      REAL*4 R_ode
      LOGICAL*1 L_ude,L_ake,L_oke
      REAL*4 R_ome,R_ipe
      LOGICAL*1 L_upe
      REAL*4 R_ere
      LOGICAL*1 L_ore
      REAL*4 R_ure,R_ase
      LOGICAL*1 L_ise,L_ite,L_ute,L_ave,L_ive,L_axe,L_oxe
     &,L_abi
      REAL*4 R_udi,R_afi
      LOGICAL*1 L_ofi,L_aki,L_iki,L_oki,L_eli,L_ili
      CHARACTER*20 C20_opi
      LOGICAL*1 L_upi,L_eri,L_iri,L_asi,L_esi,L_isi,L_osi
     &,L_usi,L_ati,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi
     &,L_ovi,L_uvi,L_axi,L_exi
      REAL*4 R_ixi,R_oxi,R_uxi,R_abo,R_ebo
      INTEGER*4 I_ibo
      REAL*4 R_ado
      LOGICAL*1 L_odo
      REAL*4 R_afo
      CHARACTER*20 C20_ilo,C20_imo
      INTEGER*4 I_epo,I_upo
      REAL*4 R_aro,R_ero,R_iro,R_oro
      LOGICAL*1 L_uro,L_aso
      INTEGER*4 I_eso,I_uso,I_uto,I_ivo
      CHARACTER*20 C20_ixo
      CHARACTER*8 C8_ibu
      REAL*4 R_aku,R_eku
      LOGICAL*1 L_iku
      REAL*4 R_oku,R_uku
      INTEGER*4 I_alu
      LOGICAL*1 L_olu,L_ulu,L_amu
      INTEGER*4 I_imu,I_apu
      LOGICAL*1 L_aru,L_iru,L_oru,L_uru,L_asu,L_esu,L_etu
     &,L_evu,L_ovu,L_uvu
      REAL*8 R8_exu
      REAL*4 R_abad
      REAL*8 R8_obad
      LOGICAL*1 L_edad,L_udad
      INTEGER*4 I_afad
      LOGICAL*1 L_ofad,L_ufad,L_ukad,L_imad,L_ipad
      End subroutine TYAGA_OY_HANDLER
      End interface
