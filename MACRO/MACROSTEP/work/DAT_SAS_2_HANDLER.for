      Subroutine DAT_SAS_2_HANDLER(ext_deltat,R_ed,R_id,R_af
     &,R_ef,R_ek,R_ik,I_il,R_ul,L_am,R_im,L_om,L_um,R_ep,R_ip
     &,R_up,L_er)
C |R_ed          |4 4 I|koeff_units     |����������� �����������||
C |R_id          |4 4 I|input           |����||
C |R_af          |4 4 I|LOWER_warning_setpoint|||
C |R_ef          |4 4 I|LOWER_alarm_setpoint|||
C |R_ek          |4 4 I|UPPER_alarm_setpoint|||
C |R_ik          |4 4 I|UPPER_warning_setpoint|||
C |I_il          |2 4 O|dat_color       |���� ��������||
C |R_ul          |4 4 I|GM05V           |���������� ����������� �������||
C |L_am          |1 1 I|GM05            |���������� ����������� �������||
C |R_im          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_om          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_um          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_ep          |4 4 O|KKS             |�����||
C |R_ip          |4 4 I|MAXIMUM         |||
C |R_up          |4 4 I|MINIMUM         |||
C |L_er          |1 1 I|GM01            |����� �� ����������� ������ ���������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i,I0_o,I0_u,I0_ad
      REAL*4 R_ed,R_id
      LOGICAL*1 L0_od,L0_ud
      REAL*4 R_af,R_ef
      LOGICAL*1 L0_if
      INTEGER*4 I0_of,I0_uf
      LOGICAL*1 L0_ak
      REAL*4 R_ek,R_ik
      LOGICAL*1 L0_ok
      INTEGER*4 I0_uk,I0_al,I0_el,I_il
      REAL*4 R0_ol,R_ul
      LOGICAL*1 L_am
      REAL*4 R0_em,R_im
      LOGICAL*1 L_om,L_um
      REAL*4 R0_ap,R_ep,R_ip,R0_op,R_up,R0_ar
      LOGICAL*1 L_er

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_e = z'01000015'
C DAT_SAS_2_HANDLER.fmg( 213, 241):��������� ������������� IN (�������)
      I0_o = z'01000017'
C DAT_SAS_2_HANDLER.fmg( 195, 242):��������� ������������� IN (�������)
      I0_ad = z'01000028'
C DAT_SAS_2_HANDLER.fmg( 178, 243):��������� ������������� IN (�������)
      I0_al = z'01000010'
C DAT_SAS_2_HANDLER.fmg( 162, 244):��������� ������������� IN (�������)
      I0_el = z'01000021'
C DAT_SAS_2_HANDLER.fmg( 162, 246):��������� ������������� IN (�������)
      I0_of = z'01000054'
C DAT_SAS_2_HANDLER.fmg( 235, 240):��������� ������������� IN (�������)
      R0_ar = R_id * R_ed
C DAT_SAS_2_HANDLER.fmg(  51, 252):����������
      if(L_er) then
         R0_op=R_up
      else
         R0_op=R0_ar
      endif
C DAT_SAS_2_HANDLER.fmg(  70, 250):���� RE IN LO CH7
      if(L_um) then
         R0_ap=R_ip
      else
         R0_ap=R0_op
      endif
C DAT_SAS_2_HANDLER.fmg(  82, 250):���� RE IN LO CH7
      if(L_om) then
         R0_em=R_im
      else
         R0_em=R0_ap
      endif
C DAT_SAS_2_HANDLER.fmg(  92, 248):���� RE IN LO CH7
      L0_od=R0_ar.lt.R_af
C DAT_SAS_2_HANDLER.fmg( 194, 224):���������� <
      L0_ud=R0_ar.lt.R_ef
C DAT_SAS_2_HANDLER.fmg( 213, 218):���������� <
      L0_ok=R0_ar.gt.R_ik
C DAT_SAS_2_HANDLER.fmg( 158, 236):���������� >
      if(L0_ok) then
         I0_uk=I0_al
      else
         I0_uk=I0_el
      endif
C DAT_SAS_2_HANDLER.fmg( 165, 244):���� RE IN LO CH7
      L0_ak=R0_ar.gt.R_ek
C DAT_SAS_2_HANDLER.fmg( 175, 230):���������� >
      if(L0_ak) then
         I0_u=I0_ad
      else
         I0_u=I0_uk
      endif
C DAT_SAS_2_HANDLER.fmg( 182, 244):���� RE IN LO CH7
      if(L0_od) then
         I0_i=I0_o
      else
         I0_i=I0_u
      endif
C DAT_SAS_2_HANDLER.fmg( 198, 242):���� RE IN LO CH7
      if(L0_ud) then
         I0_uf=I0_e
      else
         I0_uf=I0_i
      endif
C DAT_SAS_2_HANDLER.fmg( 216, 242):���� RE IN LO CH7
      L0_if = L_er.OR.L_um.OR.L_om.OR.L_am
C DAT_SAS_2_HANDLER.fmg( 236, 216):���
      if(L0_if) then
         I_il=I0_of
      else
         I_il=I0_uf
      endif
C DAT_SAS_2_HANDLER.fmg( 238, 240):���� RE IN LO CH7
      R0_ol = R_ul + R_ep
C DAT_SAS_2_HANDLER.fmg(  88, 213):��������
C label 47  try47=try47-1
      if(L_am) then
         R_ep=R0_ol
      else
         R_ep=R0_em
      endif
C DAT_SAS_2_HANDLER.fmg(  99, 248):���� RE IN LO CH7
C sav1=R0_ol
      R0_ol = R_ul + R_ep
C DAT_SAS_2_HANDLER.fmg(  88, 213):recalc:��������
C if(sav1.ne.R0_ol .and. try47.gt.0) goto 47
      End
