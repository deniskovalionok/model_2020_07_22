      Subroutine TOLKATEL_OZ_HANDLER_dpro(ext_deltat,R_e,R_i
     &,R_o,R_u,R_ad,I_ed,R_ud,R_af,R_uf,C20_em,C20_ep,I_ar
     &,I_or,R_ur,R_as,R_es,R_is,I_os,I_et,I_ev,I_uv,C20_ux
     &,C20_ube,C8_ude,R_oke,R_uke,L_ale,R_ele,R_ile,I_ole
     &,L_eme,L_ime,I_ume,I_ipe,L_are,L_ire,L_ore,L_ase,L_ise
     &,L_ose,L_use,L_ute,L_uve,L_exe,L_oxe,L_abi,R8_ibi,R_edi
     &,R8_udi,L_afi,L_ifi,L_ufi,I_aki,L_ili,L_uli,L_api,L_opi
     &,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi)
C |R_e           |4 4 K|_uintT_INT      |����������� ������ ����������� ������|10000|
C |R_i           |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_o           |4 4 K|_lintT_INT      |����������� ������ ����������� �����|0.0|
C |R_u           |4 4 K|_lcmpJ2903      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_ad          |4 4 K|_lcmpJ2873      |[]�������� ������ �����������|5.0|
C |I_ed          |2 4 O|LWORK           |����� �������||
C |R_ud          |4 4 O|VZ01            |��������� ��������� �� ��� Z||
C |R_af          |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_TOLKATEL_PKS_X`|
C |R_uf          |4 4 O|VZ02            |�������� ����������� ���������||
C |C20_em        |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_ep        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ar          |2 4 O|LREADY          |����� ����������||
C |I_or          |2 4 O|LBUSY           |����� �����||
C |R_ur          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_as          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_es          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_is          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |I_os          |2 4 O|state1          |��������� 1||
C |I_et          |2 4 O|state2          |��������� 2||
C |I_ev          |2 4 O|LWORKO          |����� � �������||
C |I_uv          |2 4 O|LINITC          |����� � ��������||
C |C20_ux        |3 20 O|task_state      |���������||
C |C20_ube       |3 20 O|task_name       |������� ���������||
C |C8_ude        |3 8 I|task            |������� ���������||
C |R_oke         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_uke         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ale         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ele         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ile         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ole         |2 4 O|LERROR          |����� �������������||
C |L_eme         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_ime         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_ume         |2 4 O|LINIT           |����� ��������||
C |I_ipe         |2 4 O|LZM             |������ "�������"||
C |L_are         |1 1 I|vlv_kvit        |||
C |L_ire         |1 1 I|instr_fault     |||
C |L_ore         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ase         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ise         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ose         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_use         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ute         |1 1 O|block           |||
C |L_uve         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_exe         |1 1 O|STOP            |�������||
C |L_oxe         |1 1 O|norm            |�����||
C |L_abi         |1 1 O|nopower         |��� ����������||
C |R8_ibi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_edi         |4 4 I|power           |�������� ��������||
C |R8_udi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_afi         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ifi         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ufi         |1 1 O|fault           |�������������||
C |I_aki         |2 4 O|LAM             |������ "�������"||
C |L_ili         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_uli         |1 1 O|XH53            |�� ������� (���)|F|
C |L_api         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_opi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_ari         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_eri         |1 1 I|mlf23           |������� ������� �����||
C |L_iri         |1 1 I|mlf22           |����� ����� ��������||
C |L_ori         |1 1 I|mlf04           |�������� �� ��������||
C |L_uri         |1 1 I|mlf03           |�������� �� ��������||
C |L_asi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i,R_o,R_u,R_ad
      INTEGER*4 I_ed,I0_id,I0_od
      REAL*4 R_ud,R_af
      LOGICAL*1 L0_ef,L0_if
      REAL*4 R0_of,R_uf,R0_ak,R0_ek,R0_ik,R0_ok,R0_uk
      LOGICAL*1 L0_al,L0_el
      CHARACTER*20 C20_il,C20_ol,C20_ul,C20_am,C20_em,C20_im
     &,C20_om,C20_um,C20_ap,C20_ep
      INTEGER*4 I0_ip,I0_op
      LOGICAL*1 L0_up
      INTEGER*4 I_ar,I0_er,I0_ir,I_or
      REAL*4 R_ur,R_as,R_es,R_is
      INTEGER*4 I_os,I0_us,I0_at,I_et,I0_it,I0_ot,I0_ut,I0_av
     &,I_ev,I0_iv,I0_ov,I_uv
      CHARACTER*20 C20_ax,C20_ex,C20_ix,C20_ox,C20_ux,C20_abe
     &,C20_ebe,C20_ibe,C20_obe,C20_ube
      CHARACTER*8 C8_ade
      LOGICAL*1 L0_ede
      CHARACTER*8 C8_ide
      LOGICAL*1 L0_ode
      CHARACTER*8 C8_ude
      INTEGER*4 I0_afe,I0_efe,I0_ife,I0_ofe,I0_ufe,I0_ake
     &,I0_eke,I0_ike
      REAL*4 R_oke,R_uke
      LOGICAL*1 L_ale
      REAL*4 R_ele,R_ile
      INTEGER*4 I_ole,I0_ule,I0_ame
      LOGICAL*1 L_eme,L_ime,L0_ome
      INTEGER*4 I_ume,I0_ape,I0_epe,I_ipe,I0_ope,I0_upe
      LOGICAL*1 L_are,L0_ere,L_ire,L_ore,L0_ure,L_ase,L0_ese
     &,L_ise,L_ose,L_use,L0_ate,L0_ete,L0_ite,L0_ote,L_ute
     &,L0_ave,L0_eve,L0_ive,L0_ove,L_uve,L0_axe,L_exe
      LOGICAL*1 L0_ixe,L_oxe,L0_uxe,L_abi
      REAL*4 R0_ebi
      REAL*8 R8_ibi
      LOGICAL*1 L0_obi,L0_ubi
      REAL*4 R0_adi,R_edi,R0_idi,R0_odi
      REAL*8 R8_udi
      LOGICAL*1 L_afi,L0_efi,L_ifi,L0_ofi,L_ufi
      INTEGER*4 I_aki,I0_eki,I0_iki
      LOGICAL*1 L0_oki,L0_uki,L0_ali,L0_eli,L_ili,L0_oli,L_uli
     &,L0_ami,L0_emi,L0_imi,L0_omi,L0_umi,L_api,L0_epi,L0_ipi
     &,L_opi,L0_upi,L_ari,L_eri,L_iri,L_ori,L_uri
      LOGICAL*1 L_asi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_od = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 122, 268):��������� ������������� IN (�������)
      I0_id = z'0100000A'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 122, 266):��������� ������������� IN (�������)
      R0_ak = 20
C TOLKATEL_OZ_HANDLER_dpro.fmg( 271, 256):��������� (RE4) (�������)
      R0_of = 0.0
C TOLKATEL_OZ_HANDLER_dpro.fmg( 312, 246):��������� (RE4) (�������)
      R0_uk = 0.0
C TOLKATEL_OZ_HANDLER_dpro.fmg( 298, 246):��������� (RE4) (�������)
      C20_ol = ''
C TOLKATEL_OZ_HANDLER_dpro.fmg( 168, 156):��������� ���������� CH20 (CH30) (�������)
      C20_il = '�������'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 168, 154):��������� ���������� CH20 (CH30) (�������)
      C20_ul = '������'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 189, 157):��������� ���������� CH20 (CH30) (�������)
      C20_om = ''
C TOLKATEL_OZ_HANDLER_dpro.fmg(  74, 158):��������� ���������� CH20 (CH30) (�������)
      C20_im = '� ��������� 2'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  74, 156):��������� ���������� CH20 (CH30) (�������)
      C20_um = '� ��������� 1'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  96, 158):��������� ���������� CH20 (CH30) (�������)
      I0_ip = z'0100000A'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 220, 268):��������� ������������� IN (�������)
      I0_op = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 220, 270):��������� ������������� IN (�������)
      I0_ir = z'0100000A'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 220, 290):��������� ������������� IN (�������)
      I0_er = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 220, 288):��������� ������������� IN (�������)
      L_ase=R_as.ne.R_ur
      R_ur=R_as
C TOLKATEL_OZ_HANDLER_dpro.fmg(  14, 193):���������� ������������� ������
      L_ise=R_is.ne.R_es
      R_es=R_is
C TOLKATEL_OZ_HANDLER_dpro.fmg(  19, 249):���������� ������������� ������
      I0_at = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 138, 162):��������� ������������� IN (�������)
      I0_us = z'01000010'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 138, 160):��������� ������������� IN (�������)
      I0_ot = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 117, 252):��������� ������������� IN (�������)
      I0_it = z'01000010'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 117, 250):��������� ������������� IN (�������)
      I0_av = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 185, 229):��������� ������������� IN (�������)
      I0_ut = z'0100000A'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 185, 227):��������� ������������� IN (�������)
      I0_ov = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 193, 179):��������� ������������� IN (�������)
      I0_iv = z'0100000A'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 193, 177):��������� ������������� IN (�������)
      I0_epe = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 115, 175):��������� ������������� IN (�������)
      I0_ape = z'0100000A'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 115, 173):��������� ������������� IN (�������)
      C20_ix = '�������'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  29, 160):��������� ���������� CH20 (CH30) (�������)
      C20_ox = '������� ���'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  29, 162):��������� ���������� CH20 (CH30) (�������)
      C20_ax = '��������'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  44, 160):��������� ���������� CH20 (CH30) (�������)
      C20_abe = '� ��������'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  50, 171):��������� ���������� CH20 (CH30) (�������)
      C20_ibe = '� �������'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  35, 172):��������� ���������� CH20 (CH30) (�������)
      C20_obe = ''
C TOLKATEL_OZ_HANDLER_dpro.fmg(  35, 174):��������� ���������� CH20 (CH30) (�������)
      C8_ade = 'init'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  18, 180):��������� ���������� CH8 (�������)
      call chcomp(C8_ude,C8_ade,L0_ede)
C TOLKATEL_OZ_HANDLER_dpro.fmg(  23, 184):���������� ���������
      C8_ide = 'work'
C TOLKATEL_OZ_HANDLER_dpro.fmg(  18, 222):��������� ���������� CH8 (�������)
      call chcomp(C8_ude,C8_ide,L0_ode)
C TOLKATEL_OZ_HANDLER_dpro.fmg(  23, 226):���������� ���������
      if(L0_ode) then
         C20_ap=C20_im
      else
         C20_ap=C20_om
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg(  78, 157):���� RE IN LO CH20
      if(L0_ede) then
         C20_ep=C20_um
      else
         C20_ep=C20_ap
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 100, 158):���� RE IN LO CH20
      if(L0_ode) then
         C20_am=C20_il
      else
         C20_am=C20_ol
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 172, 155):���� RE IN LO CH20
      if(L0_ede) then
         C20_em=C20_ul
      else
         C20_em=C20_am
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 194, 158):���� RE IN LO CH20
      if(L0_ode) then
         C20_ebe=C20_ibe
      else
         C20_ebe=C20_obe
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg(  39, 172):���� RE IN LO CH20
      if(L0_ede) then
         C20_ube=C20_abe
      else
         C20_ube=C20_ebe
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg(  55, 172):���� RE IN LO CH20
      I0_afe = z'0100008E'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 170, 194):��������� ������������� IN (�������)
      I0_efe = z'01000086'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 164, 248):��������� ������������� IN (�������)
      I0_ofe = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ufe = z'01000010'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ike = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 159, 209):��������� ������������� IN (�������)
      I0_eke = z'01000010'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 159, 207):��������� ������������� IN (�������)
      L_ime=R_uke.ne.R_oke
      R_oke=R_uke
C TOLKATEL_OZ_HANDLER_dpro.fmg(  14, 213):���������� ������������� ������
      L0_ome = L_ime.OR.L_eme
C TOLKATEL_OZ_HANDLER_dpro.fmg(  48, 212):���
      L_ale=R_ile.ne.R_ele
      R_ele=R_ile
C TOLKATEL_OZ_HANDLER_dpro.fmg(  19, 236):���������� ������������� ������
      L0_ese = L_ale.AND.L0_ode
C TOLKATEL_OZ_HANDLER_dpro.fmg(  30, 234):�
      L0_eve = L_ise.OR.L0_ese
C TOLKATEL_OZ_HANDLER_dpro.fmg(  52, 235):���
      L0_ure = L_ale.AND.L0_ede
C TOLKATEL_OZ_HANDLER_dpro.fmg(  29, 192):�
      L0_ave = L_ase.OR.L0_ure
C TOLKATEL_OZ_HANDLER_dpro.fmg(  52, 193):���
      I0_ope = z'0100000E'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 189, 200):��������� ������������� IN (�������)
      I0_ule = z'01000007'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 150, 180):��������� ������������� IN (�������)
      I0_ame = z'01000003'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 150, 182):��������� ������������� IN (�������)
      I0_eki = z'0100000E'
C TOLKATEL_OZ_HANDLER_dpro.fmg( 182, 255):��������� ������������� IN (�������)
      L_ore=(L_ire.or.L_ore).and..not.(L_are)
      L0_ere=.not.L_ore
C TOLKATEL_OZ_HANDLER_dpro.fmg( 326, 178):RS �������
      L0_ote=.false.
C TOLKATEL_OZ_HANDLER_dpro.fmg(  64, 215):��������� ���������� (�������)
      L0_ite=.false.
C TOLKATEL_OZ_HANDLER_dpro.fmg(  64, 213):��������� ���������� (�������)
      L0_uxe =.NOT.(L_uri.OR.L_ori.OR.L_asi.OR.L_iri.OR.L_eri.OR.L_ari
     &)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 319, 191):���
      L0_ixe =.NOT.(L0_uxe)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 328, 185):���
      L_ufi = L0_ixe.OR.L_ore
C TOLKATEL_OZ_HANDLER_dpro.fmg( 332, 184):���
      L0_ate = L_ufi.OR.L_use
C TOLKATEL_OZ_HANDLER_dpro.fmg(  55, 201):���
      L0_efi = (.NOT.L0_ate).AND.L0_ave
C TOLKATEL_OZ_HANDLER_dpro.fmg(  66, 194):�
      L0_ami = L0_efi.OR.L_afi
C TOLKATEL_OZ_HANDLER_dpro.fmg(  70, 192):���
      L0_ete = L_ufi.OR.L_ose
C TOLKATEL_OZ_HANDLER_dpro.fmg(  58, 243):���
      L_ute = L_ufi.OR.L0_ote.OR.L0_ite.OR.L0_ete.OR.L0_ate
C TOLKATEL_OZ_HANDLER_dpro.fmg(  68, 213):���
      if(L_ute) then
         I_or=I0_ip
      else
         I_or=I0_op
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 224, 269):���� RE IN LO CH7
      L0_up = L_ufi.OR.L_ute
C TOLKATEL_OZ_HANDLER_dpro.fmg( 218, 280):���
      if(L0_up) then
         I_ar=I0_er
      else
         I_ar=I0_ir
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 224, 288):���� RE IN LO CH7
      L0_ofi = L0_eve.AND.(.NOT.L0_ete)
C TOLKATEL_OZ_HANDLER_dpro.fmg(  66, 236):�
      L0_ipi = L0_ofi.OR.L_ifi
C TOLKATEL_OZ_HANDLER_dpro.fmg(  70, 234):���
      L0_ove = L0_ipi.OR.L0_ami
C TOLKATEL_OZ_HANDLER_dpro.fmg(  76, 203):���
      if(L_ufi) then
         I_ole=I0_ule
      else
         I_ole=I0_ame
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 153, 181):���� RE IN LO CH7
      L_oxe = L0_uxe.OR.L_ire
C TOLKATEL_OZ_HANDLER_dpro.fmg( 328, 190):���
      R0_ebi = 0.1
C TOLKATEL_OZ_HANDLER_dpro.fmg( 254, 160):��������� (RE4) (�������)
      L_abi=R8_ibi.lt.R0_ebi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 259, 162):���������� <
      R0_adi = 0.0
C TOLKATEL_OZ_HANDLER_dpro.fmg( 266, 182):��������� (RE4) (�������)
      L0_ive = L_uli.OR.L0_ome.OR.L_opi
C TOLKATEL_OZ_HANDLER_dpro.fmg(  60, 207):���
C label 185  try185=try185-1
      L_uve=(L0_ive.or.L_uve).and..not.(L0_ove)
      L0_axe=.not.L_uve
C TOLKATEL_OZ_HANDLER_dpro.fmg( 106, 205):RS �������,10
      L_exe = L_eme.OR.L_uve
C TOLKATEL_OZ_HANDLER_dpro.fmg( 117, 208):���
      L_opi=R_af.gt.R_u
C TOLKATEL_OZ_HANDLER_dpro.fmg( 351, 235):���������� >
      L0_imi = (.NOT.L_exe).AND.L_opi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 105, 260):�
      L0_oki = L_exe.OR.L_ari
C TOLKATEL_OZ_HANDLER_dpro.fmg(  98, 214):���
      L0_umi = L_opi.OR.L0_oki.OR.L0_ami
C TOLKATEL_OZ_HANDLER_dpro.fmg( 104, 229):���
      L0_upi = (.NOT.L_opi).AND.L0_ipi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 104, 235):�
      L_api=(L0_upi.or.L_api).and..not.(L0_umi)
      L0_epi=.not.L_api
C TOLKATEL_OZ_HANDLER_dpro.fmg( 111, 233):RS �������,1
      L0_omi = (.NOT.L0_imi).AND.L_api
C TOLKATEL_OZ_HANDLER_dpro.fmg( 130, 236):�
      L0_if = L0_omi.AND.(.NOT.L_ori)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 268, 245):�
      L0_el = L0_if.OR.L_uri
C TOLKATEL_OZ_HANDLER_dpro.fmg( 281, 244):���
      if(L0_el) then
         R0_ik=R0_ak
      else
         R0_ik=R0_uk
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 303, 256):���� RE IN LO CH7
      L0_eli = L0_oki.OR.L_uli.OR.L0_ipi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 104, 185):���
      L0_emi = L0_ami.AND.(.NOT.L_uli)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 104, 191):�
      L_ili=(L0_emi.or.L_ili).and..not.(L0_eli)
      L0_oli=.not.L_ili
C TOLKATEL_OZ_HANDLER_dpro.fmg( 111, 189):RS �������,2
      L0_uki = (.NOT.L_exe).AND.L_uli
C TOLKATEL_OZ_HANDLER_dpro.fmg( 103, 166):�
      L0_ali = L_ili.AND.(.NOT.L0_uki)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 130, 190):�
      L0_ef = L0_ali.AND.(.NOT.L_uri)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 268, 231):�
      L0_al = L0_ef.OR.L_ori
C TOLKATEL_OZ_HANDLER_dpro.fmg( 281, 230):���
      if(L0_al) then
         R0_ek=R0_ak
      else
         R0_ek=R0_uk
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 303, 238):���� RE IN LO CH7
      R0_ok = R0_ik + (-R0_ek)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 309, 249):��������
      if(L_ari) then
         R_uf=R0_of
      else
         R_uf=R0_ok
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 316, 248):���� RE IN LO CH7
      R_af=R_af+deltat/R_i*R_uf
      if(R_af.gt.R_e) then
         R_af=R_e
      elseif(R_af.lt.R_o) then
         R_af=R_o
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 332, 228):����������,T_INT
      L_uli=R_af.lt.R_ad
C TOLKATEL_OZ_HANDLER_dpro.fmg( 351, 223):���������� <
C sav1=L0_ive
      L0_ive = L_uli.OR.L0_ome.OR.L_opi
C TOLKATEL_OZ_HANDLER_dpro.fmg(  60, 207):recalc:���
C if(sav1.ne.L0_ive .and. try185.gt.0) goto 185
C sav1=L0_eli
      L0_eli = L0_oki.OR.L_uli.OR.L0_ipi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_eli .and. try241.gt.0) goto 241
C sav1=L0_emi
      L0_emi = L0_ami.AND.(.NOT.L_uli)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_emi .and. try243.gt.0) goto 243
C sav1=L0_uki
      L0_uki = (.NOT.L_exe).AND.L_uli
C TOLKATEL_OZ_HANDLER_dpro.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_uki .and. try247.gt.0) goto 247
      if(L_uli) then
         I_ume=I0_ape
      else
         I_ume=I0_epe
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 118, 174):���� RE IN LO CH7
      if(L_uli) then
         I_os=I0_us
      else
         I_os=I0_at
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 142, 160):���� RE IN LO CH7
      if(L_uli) then
         I0_ife=I0_ofe
      else
         I0_ife=I0_ufe
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 156, 262):���� RE IN LO CH7
      if(L0_omi) then
         I0_iki=I0_efe
      else
         I0_iki=I0_ife
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 168, 262):���� RE IN LO CH7
      if(L_ufi) then
         I_aki=I0_eki
      else
         I_aki=I0_iki
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 186, 260):���� RE IN LO CH7
      R_ud=R_af
C TOLKATEL_OZ_HANDLER_dpro.fmg( 365, 230):������,VZ01
      if(L0_ali) then
         I_uv=I0_iv
      else
         I_uv=I0_ov
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 196, 178):���� RE IN LO CH7
      L0_obi = L0_omi.OR.L0_ali
C TOLKATEL_OZ_HANDLER_dpro.fmg( 251, 174):���
      L0_ubi = L0_obi.AND.(.NOT.L_abi)
C TOLKATEL_OZ_HANDLER_dpro.fmg( 266, 173):�
      if(L0_ubi) then
         R0_odi=R_edi
      else
         R0_odi=R0_adi
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 269, 180):���� RE IN LO CH7
      if(L0_omi) then
         I_ev=I0_ut
      else
         I_ev=I0_av
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 188, 228):���� RE IN LO CH7
      if(L_opi) then
         I_ed=I0_id
      else
         I_ed=I0_od
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 126, 267):���� RE IN LO CH7
      if(L_opi) then
         I_et=I0_it
      else
         I_et=I0_ot
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 120, 250):���� RE IN LO CH7
      if(L_opi) then
         C20_ex=C20_ix
      else
         C20_ex=C20_ox
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg(  33, 161):���� RE IN LO CH20
      if(L_uli) then
         C20_ux=C20_ax
      else
         C20_ux=C20_ex
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg(  49, 160):���� RE IN LO CH20
      if(L_opi) then
         I0_ake=I0_eke
      else
         I0_ake=I0_ike
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 162, 208):���� RE IN LO CH7
      if(L0_ali) then
         I0_upe=I0_afe
      else
         I0_upe=I0_ake
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 173, 206):���� RE IN LO CH7
      if(L_ufi) then
         I_ipe=I0_ope
      else
         I_ipe=I0_upe
      endif
C TOLKATEL_OZ_HANDLER_dpro.fmg( 192, 206):���� RE IN LO CH7
      R0_idi = R8_udi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 264, 190):��������
C label 338  try338=try338-1
      R8_udi = R0_odi + R0_idi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 275, 189):��������
C sav1=R0_idi
      R0_idi = R8_udi
C TOLKATEL_OZ_HANDLER_dpro.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_idi .and. try338.gt.0) goto 338
      End
