      Subroutine LODOCHKA_HANDLER(ext_deltat,L_ad,L_af,L_ak
     &,L_al,L_ol,L_ap,L_ar,L_as,I_iv,I_ex,I_abe,I_ube,I_ude
     &,I_afe,L_ufe,L_ake,R_uke,L_ule,L_ope,L_upe,L_ire,L_ase
     &,L_ite,R_eve,R_ive,L_ove,L_ibi,L_ubi,L_adi,R_odi,R_udi
     &,L_afi,L_ifi,R_ali,R_eli,L_ili,I_imi,I_epi,I_ari,I_uri
     &,I_osi,I_usi,I_oti,L_uti,I_avi,R_axi,R_abo,R_ebo,R_edo
     &,R_udo,R_ofo,R_alo,R_ulo,R_emo,R_imo,R_epo,R_opo,R_upo
     &,L_aro,L_ato,L_eto,R_oto,R_ivo,R_axo,R_uxo,R_obu,R_idu
     &,R_odu,R_eku,R_iku,R_ilu,R_emu,R_omu,R_asu,R_esu,L_isu
     &,R_etu,R_evu,R_exu,L_obad,R_ubad,R_odad,R_ifad,R_ofad
     &,R_elad,R_ilad,L_olad,R_imad,R_ipad,R_irad,R_osad,R_itad
     &,R_evad,R_ivad,L_uxad,L_ebed,L_obed,R_ubed,L_aded,R_eded
     &,R_ufed,R_oked,R_iled,R_oled,L_aped,L_oped,L_ared,R_ered
     &,L_ired,R_ored,R_ised,R_eved,R_exed,R_ixed,R_oxed,L_adid
     &,L_odid,L_udid,L_efid,L_akid,L_ikid,R_olid,R_ulid,R_umid
     &,R_apid,L_ipid,R_irid,I_ofod,R_ikod,R_okod,R_ukod,R_elod
     &,R_ulod,R_imod,R_umod,C20_apod)
C |L_ad          |1 1 I|UNLOAD_TO_FDA90 |�������� ������� � ������������ �������||
C |L_af          |1 1 I|FDA60AE201_CATCH2|������ ������� ������||
C |L_ak          |1 1 I|FDA60AE402_CATCH2|������ ������� �������. ��������||
C |L_al          |1 1 I|FDA60AE500_CATCH2|������ ������� 3-��� ���������� (����)||
C |L_ol          |1 1 I|FDA60AE500_CATCH1|������ ������� 3-���. ���������� (� �����)||
C |L_ap          |1 1 I|FDA60AE403_CATCH2|������ ������� ������� �������� �������� 20FDA60AE403||
C |L_ar          |1 1 I|FDA60AE403_CATCH1|������� � ����� �� ������� �������||
C |L_as          |1 1 I|FDA60AE515_CATCH|������ ������� ���������� 20FDA60AE515||
C |I_iv          |2 4 K|_lcmpJ10773     |�������� ������ �����������|5|
C |I_ex          |2 4 K|_lcmpJ10761     |�������� ������ �����������|4|
C |I_abe         |2 4 K|_lcmpJ10749     |�������� ������ �����������|3|
C |I_ube         |2 4 K|_lcmpJ10737     |�������� ������ �����������|2|
C |I_ude         |2 4 K|_lcmpJ10725     |�������� ������ �����������|1|
C |I_afe         |2 4 I|FDA60AE413_NUM_BOAT|����� ��������� ������� �� �������||
C |L_ufe         |1 1 I|FDA60AE514_CATCH|������ ������� ���������� 20FDA60AE514||
C |L_ake         |1 1 I|LODOCHKA_FREE_STATE|��������� ������� � ��������� "������ �������"||
C |R_uke         |4 4 I|FDA40_BOAT_MASS |����� ����� � �����������||
C |L_ule         |1 1 I|FDA60_LOD_INPUT_GATEWAY_FROM|������� �� �������� �����||
C |L_ope         |1 1 I|FDA60_LOD_TRANSFER_TABLE|������� �� ������������ �������||
C |L_upe         |1 1 I|FDA60_LOD_MAIN_TROLLEY|������� �� �������||
C |L_ire         |1 1 I|FDA60_LOD_INPUT_GATEWAY|������� �� �������� �����||
C |L_ase         |1 1 I|FDA60_LOD_VZV   |������� �� ������������||
C |L_ite         |1 1 I|FDA60_LOD_LIFT  |������� ����������� �� ����� ||
C |R_eve         |4 4 S|_simpJ10082*    |[���]���������� ��������� ������������� |0.0|
C |R_ive         |4 4 K|_timpJ10082     |[���]������������ �������� �������������|1.0|
C |L_ove         |1 1 S|_limpJ10082*    |[TF]���������� ��������� ������������� |F|
C |L_ibi         |1 1 I|FDA40_START     |������ ������� �����������||
C |L_ubi         |1 1 I|FDA91EC001_INIT_COORD|������������� ���������� ������� ��� ��. �����������||
C |L_adi         |1 1 I|FDA91EC003_INIT_COORD|������������� ���������� ������� ��� ��. ���||
C |R_odi         |4 4 S|_simpJ10030*    |[���]���������� ��������� ������������� |0.0|
C |R_udi         |4 4 K|_timpJ10030     |[���]������������ �������� �������������|1.0|
C |L_afi         |1 1 S|_limpJ10030*    |[TF]���������� ��������� ������������� |F|
C |L_ifi         |1 1 I|FDA91EC005_INIT_COORD|������������� ���������� ������� ��� ��. ���������||
C |R_ali         |4 4 S|_simpJ10001*    |[���]���������� ��������� ������������� |0.0|
C |R_eli         |4 4 K|_timpJ10001     |[���]������������ �������� �������������|1.0|
C |L_ili         |1 1 S|_limpJ10001*    |[TF]���������� ��������� ������������� |F|
C |I_imi         |2 4 K|_lcmpJ9990      |�������� ������ �����������|5|
C |I_epi         |2 4 K|_lcmpJ9978      |�������� ������ �����������|4|
C |I_ari         |2 4 K|_lcmpJ9966      |�������� ������ �����������|3|
C |I_uri         |2 4 K|_lcmpJ9954      |�������� ������ �����������|2|
C |I_osi         |2 4 K|_lcmpJ9942      |�������� ������ �����������|1|
C |I_usi         |2 4 I|FDA60AE408_NUM_BOAT|����� ��������� ������� �� �������||
C |I_oti         |2 4 O|CR              |�������������� � �������������||
C |L_uti         |1 1 I|FDA60AE408_CATCH|������ ������� 5��� �������� 20FDA60AE408||
C |I_avi         |2 4 O|LP              |����� �� ������� ����������� �������||
C |R_axi         |4 4 K|_lcmpJ9186      |[]�������� ������ �����������|20|
C |R_abo         |4 4 I|FDA60AE500AVY01 |�������� ��������� 20FDA60AE500A||
C |R_ebo         |4 4 K|_lcmpJ9173      |[]�������� ������ �����������|20|
C |R_edo         |4 4 I|FDA60AE500BVY01 |�������� ��������� 20FDA60AE500B||
C |R_udo         |4 4 I|FDA60AE500�VY01 |�������� ��������� 20FDA60AE500�||
C |R_ofo         |4 4 K|_lcmpJ9156      |[]�������� ������ �����������|20|
C |R_alo         |4 4 K|_lcmpJ9127      |[]�������� ������ �����������|20|
C |R_ulo         |4 4 K|_lcmpJ9057      |[]�������� ������ �����������|2|
C |R_emo         |4 4 I|FDA60AE403_VZ01 |�������� �������������� �������||
C |R_imo         |4 4 K|_lcmpJ9051      |[]�������� ������ �����������|20|
C |R_epo         |4 4 I|FDA60AE403_POS  |��������� ������� �� ��� X||
C |R_opo         |4 4 S|_simpJ9036*     |[���]���������� ��������� ������������� |0.0|
C |R_upo         |4 4 K|_timpJ9036      |[���]������������ �������� �������������|1.0|
C |L_aro         |1 1 S|_limpJ9036*     |[TF]���������� ��������� ������������� |F|
C |L_ato         |1 1 S|_splsJ8856*     |[TF]���������� ��������� ������������� |F|
C |L_eto         |1 1 I|FDA40_HAVE_DELTA_MASS|���� ��������� ����� �������||
C |R_oto         |4 4 I|FDA40_DELTA_MASS|��������� ����� �������||
C |R_ivo         |4 4 O|MASS            |����� ����� ����������� � �������||
C |R_axo         |4 4 K|_lcmpJ8516      |[]�������� ������ �����������|20|
C |R_uxo         |4 4 K|_lcmpJ8503      |[]�������� ������ �����������|20|
C |R_obu         |4 4 K|_lcmpJ8495      |[]�������� ������ �����������|20|
C |R_idu         |4 4 I|FDA91AE001KE01_POS|��������� �1||
C |R_odu         |4 4 K|_lcmpJ8485      |[]�������� ������ �����������|20|
C |R_eku         |4 4 I|FDA91AE001KE01_V01|������� �1 �� ��� X||
C |R_iku         |4 4 I|FDA91AE003KE01_V01|�������� 20FDA91AE003KE01||
C |R_ilu         |4 4 K|_lcmpJ8465      |[]�������� ������ �����������|20|
C |R_emu         |4 4 I|FDA91AE003KE01_POS|��������� �� 20FDA91AE003KE01||
C |R_omu         |4 4 K|_lcmpJ8453      |[]�������� ������ �����������|20|
C |R_asu         |4 4 S|_simpJ8368*     |[���]���������� ��������� ������������� |0.0|
C |R_esu         |4 4 K|_timpJ8368      |[���]������������ �������� �������������|1.0|
C |L_isu         |1 1 S|_limpJ8368*     |[TF]���������� ��������� ������������� |F|
C |R_etu         |4 4 K|_lcmpJ8358      |[]�������� ������ �����������|20|
C |R_evu         |4 4 K|_lcmpJ8350      |[]�������� ������ �����������|20|
C |R_exu         |4 4 K|_lcmpJ8342      |[]�������� ������ �����������|20|
C |L_obad        |1 1 O|FDA91AE012_CATCHED|������� ��������� �� 20FDA91AE012||
C |R_ubad        |4 4 K|_lcmpJ8317      |[]�������� ������ �����������|20|
C |R_odad        |4 4 K|_lcmpJ8308      |[]�������� ������ �����������|20|
C |R_ifad        |4 4 I|FDA91AE012_POS  |��������� �4||
C |R_ofad        |4 4 K|_lcmpJ8297      |[]�������� ������ �����������|20|
C |R_elad        |4 4 S|_simpJ7966*     |[���]���������� ��������� ������������� |0.0|
C |R_ilad        |4 4 K|_timpJ7966      |[���]������������ �������� �������������|1.0|
C |L_olad        |1 1 S|_limpJ7966*     |[TF]���������� ��������� ������������� |F|
C |R_imad        |4 4 K|_lcmpJ7929      |[]�������� ������ �����������|20|
C |R_ipad        |4 4 K|_lcmpJ7921      |[]�������� ������ �����������|20|
C |R_irad        |4 4 K|_lcmpJ7913      |[]�������� ������ �����������|20|
C |R_osad        |4 4 K|_lcmpJ7509      |[]�������� ������ �����������|20|
C |R_itad        |4 4 K|_lcmpJ7500      |[]�������� ������ �����������|20|
C |R_evad        |4 4 I|FDA91AE007_POS  |��������� �4||
C |R_ivad        |4 4 K|_lcmpJ7487      |[]�������� ������ �����������|20|
C |L_uxad        |1 1 O|FDA91AE007_CATCHED|������� ��������� �4||
C |L_ebed        |1 1 S|_qffJ7476*      |�������� ������ Q RS-��������  |F|
C |L_obed        |1 1 I|FDA91AE007_UNCATCH|��������� ������� �4||
C |R_ubed        |4 4 I|FDA91AE007_VZ01 |������� �4 �� ��� Z||
C |L_aded        |1 1 I|FDA91AE007_CATCH|������ ������� �4||
C |R_eded        |4 4 K|_lcmpJ7170      |[]�������� ������ �����������|20|
C |R_ufed        |4 4 K|_lcmpJ6848      |[]�������� ������ �����������|20|
C |R_oked        |4 4 K|_lcmpJ6839      |[]�������� ������ �����������|20|
C |R_iled        |4 4 I|FDA91AE006_POS  |��������� �� 20FDA91AE006||
C |R_oled        |4 4 K|_lcmpJ6826      |[]�������� ������ �����������|20|
C |L_aped        |1 1 O|FDA91AE006_CATCHED|������� ��������� �� 20FDA91AE006||
C |L_oped        |1 1 S|_qffJ6815*      |�������� ������ Q RS-��������  |F|
C |L_ared        |1 1 I|FDA91AE006_UNCATCH|��������� ������� �� 20FDA91AE006||
C |R_ered        |4 4 I|FDA91AE006_VZ01 |������� �� �� ��� Z||
C |L_ired        |1 1 I|FDA91AE006_CATCH|������ ������� �� 20FDA91AE006||
C |R_ored        |4 4 K|_lcmpJ6805      |[]�������� ������ �����������|20|
C |R_ised        |4 4 K|_lcmpJ6796      |[]�������� ������ �����������|20|
C |R_eved        |4 4 K|_lcmpJ6476      |[]�������� ������ �����������|20|
C |R_exed        |4 4 K|_lcmpJ6463      |[]�������� ������ �����������|1000|
C |R_ixed        |4 4 I|FDA91AE014_POS  |��������� �� 20FDA91AE014||
C |R_oxed        |4 4 K|_lcmpJ6181      |[]�������� ������ �����������|20|
C |L_adid        |1 1 O|FDA91AE014_CATCHED|������� ��������� �� 20FDA91AE014||
C |L_odid        |1 1 I|FDA91AE012_UNCATCH|��������� ������� �� 20FDA91AE012||
C |L_udid        |1 1 I|FDA91AE012_CATCH|������ ������� �� 20FDA91AE012||
C |L_efid        |1 1 S|_qffJ6163*      |�������� ������ Q RS-��������  |F|
C |L_akid        |1 1 S|_qffJ6151*      |�������� ������ Q RS-��������  |F|
C |L_ikid        |1 1 I|FDA91AE014_UNCATCH|��������� ������� �� 20FDA91AE014||
C |R_olid        |4 4 O|VZ01            |��������� ������� �� ��� Z||
C |R_ulid        |4 4 S|_ointJ6126*     |����� ����������� |0.0|
C |R_umid        |4 4 I|FDA91AE012_VZ01 |������� �� �� ��� Z||
C |R_apid        |4 4 I|FDA91AE014_VZ01 |������� �� �� ��� Z||
C |L_ipid        |1 1 I|FDA91AE014_CATCH|������ ������� �� 20FDA91AE014||
C |R_irid        |4 4 S|_ointJ5958*     |����� ����������� |`p_LOAD_Y`|
C |I_ofod        |2 4 O|LINE_POS        |����� �� ������� ����������� �������||
C |R_ikod        |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_okod        |4 4 S|_ointJ5909*     |����� ����������� |`p_LOAD_X`|
C |R_ukod        |4 4 K|_lcmpJ2655      |[]�������� ������ �����������|4456|
C |R_elod        |4 4 I|FDA60AE413VX02  |�������� �������� �������||
C |R_ulod        |4 4 I|FDA60AE408VX02  |�������� �������� �������||
C |R_imod        |4 4 K|_lcmpJ871       |[]�������� ������ �����������|1.0|
C |R_umod        |4 4 O|VY01            |��������� ������� �� ��� Y||
C |C20_apod      |3 20 O|SECTION         |������� ������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e
      LOGICAL*1 L0_i,L0_o
      INTEGER*4 I0_u
      LOGICAL*1 L_ad
      INTEGER*4 I0_ed
      LOGICAL*1 L0_id,L0_od
      INTEGER*4 I0_ud
      LOGICAL*1 L_af
      INTEGER*4 I0_ef
      LOGICAL*1 L0_if,L0_of
      INTEGER*4 I0_uf
      LOGICAL*1 L_ak
      INTEGER*4 I0_ek
      LOGICAL*1 L0_ik,L0_ok
      INTEGER*4 I0_uk
      LOGICAL*1 L_al,L0_el
      INTEGER*4 I0_il
      LOGICAL*1 L_ol
      INTEGER*4 I0_ul
      LOGICAL*1 L0_am
      INTEGER*4 I0_em
      LOGICAL*1 L0_im,L0_om
      INTEGER*4 I0_um
      LOGICAL*1 L_ap,L0_ep
      INTEGER*4 I0_ip,I0_op
      LOGICAL*1 L0_up,L_ar
      INTEGER*4 I0_er
      LOGICAL*1 L0_ir,L0_or
      INTEGER*4 I0_ur
      LOGICAL*1 L_as,L0_es
      INTEGER*4 I0_is
      LOGICAL*1 L0_os
      INTEGER*4 I0_us
      LOGICAL*1 L0_at
      INTEGER*4 I0_et
      LOGICAL*1 L0_it
      INTEGER*4 I0_ot
      LOGICAL*1 L0_ut,L0_av,L0_ev
      INTEGER*4 I_iv
      LOGICAL*1 L0_ov,L0_uv,L0_ax
      INTEGER*4 I_ex
      LOGICAL*1 L0_ix,L0_ox,L0_ux
      INTEGER*4 I_abe
      LOGICAL*1 L0_ebe,L0_ibe,L0_obe
      INTEGER*4 I_ube
      LOGICAL*1 L0_ade
      INTEGER*4 I0_ede
      LOGICAL*1 L0_ide,L0_ode
      INTEGER*4 I_ude,I_afe
      LOGICAL*1 L0_efe,L0_ife
      INTEGER*4 I0_ofe
      LOGICAL*1 L_ufe,L_ake
      REAL*4 R0_eke
      INTEGER*4 I0_ike
      LOGICAL*1 L0_oke
      REAL*4 R_uke
      LOGICAL*1 L0_ale
      INTEGER*4 I0_ele
      LOGICAL*1 L0_ile
      INTEGER*4 I0_ole
      LOGICAL*1 L_ule,L0_ame
      INTEGER*4 I0_eme
      LOGICAL*1 L0_ime
      INTEGER*4 I0_ome
      LOGICAL*1 L0_ume
      INTEGER*4 I0_ape
      LOGICAL*1 L0_epe
      INTEGER*4 I0_ipe
      LOGICAL*1 L_ope,L_upe
      INTEGER*4 I0_are
      LOGICAL*1 L0_ere,L_ire
      INTEGER*4 I0_ore
      LOGICAL*1 L0_ure,L_ase
      INTEGER*4 I0_ese
      LOGICAL*1 L0_ise,L0_ose
      INTEGER*4 I0_use,I0_ate
      LOGICAL*1 L0_ete,L_ite,L0_ote
      INTEGER*4 I0_ute
      REAL*4 R0_ave,R_eve,R_ive
      LOGICAL*1 L_ove
      INTEGER*4 I0_uve
      REAL*4 R0_axe,R0_exe
      LOGICAL*1 L0_ixe
      REAL*4 R0_oxe
      LOGICAL*1 L0_uxe,L0_abi
      INTEGER*4 I0_ebi
      LOGICAL*1 L_ibi
      INTEGER*4 I0_obi
      LOGICAL*1 L_ubi,L_adi,L0_edi
      REAL*4 R0_idi,R_odi,R_udi
      LOGICAL*1 L_afi,L0_efi,L_ifi,L0_ofi
      INTEGER*4 I0_ufi
      REAL*4 R0_aki,R0_eki
      LOGICAL*1 L0_iki
      REAL*4 R0_oki,R0_uki,R_ali,R_eli
      LOGICAL*1 L_ili,L0_oli
      INTEGER*4 I0_uli
      LOGICAL*1 L0_ami,L0_emi
      INTEGER*4 I_imi,I0_omi
      LOGICAL*1 L0_umi,L0_api
      INTEGER*4 I_epi,I0_ipi
      LOGICAL*1 L0_opi,L0_upi
      INTEGER*4 I_ari,I0_eri
      LOGICAL*1 L0_iri,L0_ori
      INTEGER*4 I_uri,I0_asi
      LOGICAL*1 L0_esi,L0_isi
      INTEGER*4 I_osi,I_usi
      LOGICAL*1 L0_ati,L0_eti
      INTEGER*4 I0_iti,I_oti
      LOGICAL*1 L_uti
      INTEGER*4 I_avi
      LOGICAL*1 L0_evi
      INTEGER*4 I0_ivi
      LOGICAL*1 L0_ovi
      INTEGER*4 I0_uvi
      REAL*4 R_axi,R0_exi,R0_ixi,R0_oxi
      LOGICAL*1 L0_uxi
      REAL*4 R_abo,R_ebo,R0_ibo,R0_obo,R0_ubo
      LOGICAL*1 L0_ado
      REAL*4 R_edo,R0_ido,R0_odo,R_udo,R0_afo,R0_efo
      LOGICAL*1 L0_ifo
      REAL*4 R_ofo,R0_ufo,R0_ako,R0_eko,R0_iko,R0_oko
      LOGICAL*1 L0_uko
      REAL*4 R_alo,R0_elo,R0_ilo,R0_olo,R_ulo,R0_amo,R_emo
     &,R_imo,R0_omo,R0_umo,R0_apo,R_epo,R0_ipo,R_opo,R_upo
      LOGICAL*1 L_aro
      INTEGER*4 I0_ero
      REAL*4 R0_iro
      LOGICAL*1 L0_oro
      REAL*4 R0_uro
      LOGICAL*1 L0_aso,L0_eso,L0_iso,L0_oso
      INTEGER*4 I0_uso
      LOGICAL*1 L_ato,L_eto,L0_ito
      REAL*4 R_oto,R0_uto
      LOGICAL*1 L0_avo
      REAL*4 R0_evo,R_ivo
      LOGICAL*1 L0_ovo
      INTEGER*4 I0_uvo
      REAL*4 R_axo,R0_exo,R0_ixo,R0_oxo,R_uxo,R0_abu,R0_ebu
     &,R0_ibu,R_obu,R0_ubu,R0_adu,R0_edu,R_idu,R_odu,R0_udu
     &,R0_afu
      LOGICAL*1 L0_efu,L0_ifu,L0_ofu
      REAL*4 R0_ufu
      LOGICAL*1 L0_aku
      REAL*4 R_eku,R_iku,R0_oku
      LOGICAL*1 L0_uku,L0_alu,L0_elu
      REAL*4 R_ilu,R0_olu,R0_ulu,R0_amu,R_emu
      LOGICAL*1 L0_imu
      REAL*4 R_omu,R0_umu,R0_apu,R0_epu
      LOGICAL*1 L0_ipu
      INTEGER*4 I0_opu
      REAL*4 R0_upu,R0_aru
      LOGICAL*1 L0_eru
      INTEGER*4 I0_iru,I0_oru
      REAL*4 R0_uru,R_asu,R_esu
      LOGICAL*1 L_isu,L0_osu,L0_usu,L0_atu
      REAL*4 R_etu,R0_itu,R0_otu,R0_utu
      LOGICAL*1 L0_avu
      REAL*4 R_evu,R0_ivu,R0_ovu,R0_uvu
      LOGICAL*1 L0_axu
      REAL*4 R_exu,R0_ixu,R0_oxu,R0_uxu
      LOGICAL*1 L0_abad
      INTEGER*4 I0_ebad
      LOGICAL*1 L0_ibad,L_obad
      REAL*4 R_ubad,R0_adad,R0_edad,R0_idad,R_odad,R0_udad
     &,R0_afad,R0_efad,R_ifad,R_ofad,R0_ufad,R0_akad
      LOGICAL*1 L0_ekad,L0_ikad,L0_okad
      INTEGER*4 I0_ukad
      REAL*4 R0_alad,R_elad,R_ilad
      LOGICAL*1 L_olad,L0_ulad,L0_amad,L0_emad
      REAL*4 R_imad,R0_omad,R0_umad,R0_apad
      LOGICAL*1 L0_epad
      REAL*4 R_ipad,R0_opad,R0_upad,R0_arad
      LOGICAL*1 L0_erad
      REAL*4 R_irad,R0_orad,R0_urad,R0_asad
      LOGICAL*1 L0_esad
      INTEGER*4 I0_isad
      REAL*4 R_osad,R0_usad,R0_atad,R0_etad,R_itad,R0_otad
     &,R0_utad,R0_avad,R_evad,R_ivad,R0_ovad,R0_uvad
      LOGICAL*1 L0_axad,L0_exad,L0_ixad,L0_oxad,L_uxad,L0_abed
     &,L_ebed,L0_ibed,L_obed
      REAL*4 R_ubed
      LOGICAL*1 L_aded
      REAL*4 R_eded,R0_ided,R0_oded,R0_uded,R0_afed
      LOGICAL*1 L0_efed
      REAL*4 R0_ifed,R0_ofed,R_ufed,R0_aked,R0_eked,R0_iked
     &,R_oked,R0_uked,R0_aled,R0_eled,R_iled,R_oled,R0_uled
     &,R0_amed
      LOGICAL*1 L0_emed,L0_imed,L0_omed,L0_umed,L_aped
      REAL*4 R0_eped
      LOGICAL*1 L0_iped,L_oped,L0_uped,L_ared
      REAL*4 R_ered
      LOGICAL*1 L_ired
      REAL*4 R_ored,R0_ured,R0_ased,R0_esed,R_ised,R0_osed
     &,R0_used,R0_ated,R0_eted,R0_ited,R0_oted
      LOGICAL*1 L0_uted,L0_aved
      REAL*4 R_eved,R0_ived,R0_oved,R0_uved
      LOGICAL*1 L0_axed
      REAL*4 R_exed,R_ixed,R_oxed,R0_uxed,R0_abid
      LOGICAL*1 L0_ebid,L0_ibid,L0_obid,L0_ubid,L_adid,L0_edid
     &,L0_idid,L_odid,L_udid
      REAL*4 R0_afid
      LOGICAL*1 L_efid
      REAL*4 R0_ifid,R0_ofid
      LOGICAL*1 L0_ufid,L_akid,L0_ekid,L_ikid,L0_okid
      REAL*4 R0_ukid,R0_alid,R0_elid,R0_ilid,R_olid,R_ulid
      INTEGER*4 I0_amid
      LOGICAL*1 L0_emid
      REAL*4 R0_imid,R0_omid,R_umid,R_apid,R0_epid
      LOGICAL*1 L_ipid
      REAL*4 R0_opid
      LOGICAL*1 L0_upid
      REAL*4 R0_arid,R0_erid,R_irid,R0_orid
      LOGICAL*1 L0_urid,L0_asid
      REAL*4 R0_esid,R0_isid
      LOGICAL*1 L0_osid
      REAL*4 R0_usid,R0_atid
      LOGICAL*1 L0_etid
      REAL*4 R0_itid,R0_otid,R0_utid
      INTEGER*4 I0_avid,I0_evid
      LOGICAL*1 L0_ivid
      REAL*4 R0_ovid,R0_uvid
      LOGICAL*1 L0_axid
      REAL*4 R0_exid,R0_ixid,R0_oxid
      INTEGER*4 I0_uxid,I0_abod
      LOGICAL*1 L0_ebod
      INTEGER*4 I0_ibod
      REAL*4 R0_obod,R0_ubod
      LOGICAL*1 L0_adod
      REAL*4 R0_edod,R0_idod
      INTEGER*4 I0_odod
      LOGICAL*1 L0_udod
      REAL*4 R0_afod,R0_efod
      INTEGER*4 I0_ifod,I_ofod
      REAL*4 R0_ufod,R0_akod,R0_ekod,R_ikod,R_okod,R_ukod
      LOGICAL*1 L0_alod
      REAL*4 R_elod,R0_ilod,R0_olod,R_ulod,R0_amod
      LOGICAL*1 L0_emod
      REAL*4 R_imod,R0_omod,R_umod
      CHARACTER*20 C20_apod

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_uki=R_ali
C LODOCHKA_HANDLER.fmg( 690,1931):pre: ������������  �� T
      R0_ave=R_eve
C LODOCHKA_HANDLER.fmg( 690,1914):pre: ������������  �� T
      R0_uru=R_asu
C LODOCHKA_HANDLER.fmg( 213,1378):pre: ������������  �� T
      R0_ipo=R_opo
C LODOCHKA_HANDLER.fmg( 209,1329):pre: ������������  �� T
      R0_alad=R_elad
C LODOCHKA_HANDLER.fmg( 213,1424):pre: ������������  �� T
      R0_idi=R_odi
C LODOCHKA_HANDLER.fmg( 690,1940):pre: ������������  �� T
      I0_e = 9096
C LODOCHKA_HANDLER.fmg( 855,1710):��������� ������������� IN (�������)
      I0_u = 0001
C LODOCHKA_HANDLER.fmg( 811,1706):��������� ������������� IN (�������)
      I0_ed = 0001
C LODOCHKA_HANDLER.fmg( 855,1722):��������� ������������� IN (�������)
      I0_ud = 0002
C LODOCHKA_HANDLER.fmg( 811,1718):��������� ������������� IN (�������)
      I0_ef = 0002
C LODOCHKA_HANDLER.fmg( 855,1737):��������� ������������� IN (�������)
      I0_uf = 0003
C LODOCHKA_HANDLER.fmg( 811,1732):��������� ������������� IN (�������)
      I0_ek = 0003
C LODOCHKA_HANDLER.fmg( 855,1750):��������� ������������� IN (�������)
      I0_uk = 0006
C LODOCHKA_HANDLER.fmg( 811,1744):��������� ������������� IN (�������)
      I0_il = 0002
C LODOCHKA_HANDLER.fmg( 654,1808):��������� ������������� IN (�������)
      I0_ul = 0003
C LODOCHKA_HANDLER.fmg( 696,1812):��������� ������������� IN (�������)
      I0_em = 0006
C LODOCHKA_HANDLER.fmg( 855,1760):��������� ������������� IN (�������)
      I0_um = 0035
C LODOCHKA_HANDLER.fmg( 811,1755):��������� ������������� IN (�������)
      I0_ip = 0005
C LODOCHKA_HANDLER.fmg( 654,1774):��������� ������������� IN (�������)
      I0_op = 0006
C LODOCHKA_HANDLER.fmg( 696,1778):��������� ������������� IN (�������)
      I0_er = 0035
C LODOCHKA_HANDLER.fmg( 855,1772):��������� ������������� IN (�������)
      I0_ur = 0034
C LODOCHKA_HANDLER.fmg( 811,1767):��������� ������������� IN (�������)
      I0_is = 0027
C LODOCHKA_HANDLER.fmg( 811,1786):��������� ������������� IN (�������)
      I0_us = 0026
C LODOCHKA_HANDLER.fmg( 811,1800):��������� ������������� IN (�������)
      I0_et = 0025
C LODOCHKA_HANDLER.fmg( 811,1813):��������� ������������� IN (�������)
      I0_ot = 0024
C LODOCHKA_HANDLER.fmg( 811,1826):��������� ������������� IN (�������)
      L0_ev=I_afe.eq.I_iv
C LODOCHKA_HANDLER.fmg( 818,1779):���������� �������������
      L0_ax=I_afe.eq.I_ex
C LODOCHKA_HANDLER.fmg( 818,1792):���������� �������������
      L0_ux=I_afe.eq.I_abe
C LODOCHKA_HANDLER.fmg( 818,1806):���������� �������������
      L0_obe=I_afe.eq.I_ube
C LODOCHKA_HANDLER.fmg( 818,1819):���������� �������������
      I0_ede = 0034
C LODOCHKA_HANDLER.fmg( 853,1838):��������� ������������� IN (�������)
      L0_ode=I_afe.eq.I_ude
C LODOCHKA_HANDLER.fmg( 818,1832):���������� �������������
      I0_ofe = 0023
C LODOCHKA_HANDLER.fmg( 811,1838):��������� ������������� IN (�������)
      R0_eke = 0.0
C LODOCHKA_HANDLER.fmg( 505,1969):��������� (RE4) (�������)
      if(L_ake) then
         R_ivo=R0_eke
      endif
C LODOCHKA_HANDLER.fmg( 510,1968):���� � ������������� �������
      I0_ike = 4001
C LODOCHKA_HANDLER.fmg( 336,1964):��������� ������������� IN (�������)
      I0_ele = 0010
C LODOCHKA_HANDLER.fmg( 654,1755):��������� ������������� IN (�������)
      I0_ole = 00011
C LODOCHKA_HANDLER.fmg( 696,1759):��������� ������������� IN (�������)
      I0_eme = 0006
C LODOCHKA_HANDLER.fmg( 654,1764):��������� ������������� IN (�������)
      I0_ome = 0006
C LODOCHKA_HANDLER.fmg( 654,1784):��������� ������������� IN (�������)
      I0_ape = 0003
C LODOCHKA_HANDLER.fmg( 654,1795):��������� ������������� IN (�������)
      I0_ipe = 0001
C LODOCHKA_HANDLER.fmg( 654,1820):��������� ������������� IN (�������)
      I0_are = 00010
C LODOCHKA_HANDLER.fmg( 696,1768):��������� ������������� IN (�������)
      I0_ore = 0005
C LODOCHKA_HANDLER.fmg( 696,1788):��������� ������������� IN (�������)
      I0_ese = 0006
C LODOCHKA_HANDLER.fmg( 696,1799):��������� ������������� IN (�������)
      I0_use = 0002
C LODOCHKA_HANDLER.fmg( 696,1826):��������� ������������� IN (�������)
      I0_ate = 0001
C LODOCHKA_HANDLER.fmg( 696,1840):��������� ������������� IN (�������)
      I0_ute = 9096
C LODOCHKA_HANDLER.fmg( 654,1835):��������� ������������� IN (�������)
      I0_uve = 40
C LODOCHKA_HANDLER.fmg( 695,1906):��������� ������������� IN (�������)
      R0_axe = 0
C LODOCHKA_HANDLER.fmg( 696,1894):��������� (RE4) (�������)
      R0_exe = 0
C LODOCHKA_HANDLER.fmg( 696,1881):��������� (RE4) (�������)
      R0_oxe = 0
C LODOCHKA_HANDLER.fmg( 696,1868):��������� (RE4) (�������)
      I0_ebi = 94
C LODOCHKA_HANDLER.fmg( 654,1906):��������� ������������� IN (�������)
      I0_obi = 91
C LODOCHKA_HANDLER.fmg( 695,1984):��������� ������������� IN (�������)
      L0_edi = L_ubi.OR.L_adi.OR.L_ifi
C LODOCHKA_HANDLER.fmg( 665,1936):���
      I0_ufi = 95
C LODOCHKA_HANDLER.fmg( 654,1926):��������� ������������� IN (�������)
      R0_aki = 1500.0
C LODOCHKA_HANDLER.fmg( 696,1971):��������� (RE4) (�������)
      R0_eki = 2682.0
C LODOCHKA_HANDLER.fmg( 696,1958):��������� (RE4) (�������)
      R0_oki = 16310.0
C LODOCHKA_HANDLER.fmg( 696,1946):��������� (RE4) (�������)
      I0_uli = 0016
C LODOCHKA_HANDLER.fmg( 696,1688):��������� ������������� IN (�������)
      L0_emi=I_usi.eq.I_imi
C LODOCHKA_HANDLER.fmg( 660,1678):���������� �������������
      I0_omi = 0015
C LODOCHKA_HANDLER.fmg( 696,1703):��������� ������������� IN (�������)
      L0_api=I_usi.eq.I_epi
C LODOCHKA_HANDLER.fmg( 660,1692):���������� �������������
      I0_ipi = 0014
C LODOCHKA_HANDLER.fmg( 696,1716):��������� ������������� IN (�������)
      L0_upi=I_usi.eq.I_ari
C LODOCHKA_HANDLER.fmg( 660,1704):���������� �������������
      I0_eri = 0013
C LODOCHKA_HANDLER.fmg( 696,1729):��������� ������������� IN (�������)
      L0_ori=I_usi.eq.I_uri
C LODOCHKA_HANDLER.fmg( 660,1718):���������� �������������
      I0_asi = 0012
C LODOCHKA_HANDLER.fmg( 696,1742):��������� ������������� IN (�������)
      L0_isi=I_usi.eq.I_osi
C LODOCHKA_HANDLER.fmg( 660,1730):���������� �������������
      I0_iti = 0011
C LODOCHKA_HANDLER.fmg( 654,1742):��������� ������������� IN (�������)
      I0_ivi = 60
C LODOCHKA_HANDLER.fmg( 288,1566):��������� ������������� IN (�������)
      I0_uvi = 60
C LODOCHKA_HANDLER.fmg( 148,1558):��������� ������������� IN (�������)
      R0_exi = 3140
C LODOCHKA_HANDLER.fmg(  36,1856):��������� (RE4) (�������)
      R0_ibo = 2530
C LODOCHKA_HANDLER.fmg(  36,1841):��������� (RE4) (�������)
      R0_odo = 0.0
C LODOCHKA_HANDLER.fmg(  70,1862):��������� (RE4) (�������)
      R0_ufo = 2240
C LODOCHKA_HANDLER.fmg(  36,1822):��������� (RE4) (�������)
      R0_iko = 0.0
C LODOCHKA_HANDLER.fmg(  70,1914):��������� (RE4) (�������)
      R0_elo = 15200
C LODOCHKA_HANDLER.fmg(  36,1904):��������� (RE4) (�������)
      R0_amo=abs(R_emo)
C LODOCHKA_HANDLER.fmg( 169,1316):���������� ��������
      L0_aso=R0_amo.gt.R_ulo
C LODOCHKA_HANDLER.fmg( 180,1316):���������� >
      R0_apo = 3140.0
C LODOCHKA_HANDLER.fmg( 156,1326):��������� (RE4) (�������)
      R0_umo = R0_apo + (-R_epo)
C LODOCHKA_HANDLER.fmg( 163,1324):��������
      R0_omo=abs(R0_umo)
C LODOCHKA_HANDLER.fmg( 169,1324):���������� ��������
      L0_eso=R0_omo.lt.R_imo
C LODOCHKA_HANDLER.fmg( 180,1324):���������� <
      I0_ero = 60
C LODOCHKA_HANDLER.fmg( 214,1324):��������� ������������� IN (�������)
      R0_iro = 15200
C LODOCHKA_HANDLER.fmg( 215,1312):��������� (RE4) (�������)
      R0_uro = 3140
C LODOCHKA_HANDLER.fmg( 215,1298):��������� (RE4) (�������)
      I0_uso = 96
C LODOCHKA_HANDLER.fmg( 173,1330):��������� ������������� IN (�������)
      L0_ito=L_eto.and..not.L_ato
      L_ato=L_eto
C LODOCHKA_HANDLER.fmg( 472,1415):������������  �� 1 ���
      I0_uvo = 40
C LODOCHKA_HANDLER.fmg( 455,1420):��������� ������������� IN (�������)
      R0_exo = 600.0
C LODOCHKA_HANDLER.fmg( 158,1724):��������� (RE4) (�������)
      R0_ufu = 0.0
C LODOCHKA_HANDLER.fmg( 206,1683):��������� (RE4) (�������)
      R0_ibu = 100.0
C LODOCHKA_HANDLER.fmg( 158,1652):��������� (RE4) (�������)
      R0_edu = 788.0
C LODOCHKA_HANDLER.fmg( 158,1660):��������� (RE4) (�������)
      R0_oku = 0.0
C LODOCHKA_HANDLER.fmg( 206,1748):��������� (RE4) (�������)
      R0_olu = 16310
C LODOCHKA_HANDLER.fmg( 158,1715):��������� (RE4) (�������)
      R0_eted = 40.0
C LODOCHKA_HANDLER.fmg( 311,1748):��������� (RE4) (�������)
      I0_opu = 40
C LODOCHKA_HANDLER.fmg( 148,1568):��������� ������������� IN (�������)
      I0_iru = 40
C LODOCHKA_HANDLER.fmg( 288,1574):��������� ������������� IN (�������)
      R0_ifed = 40.0
C LODOCHKA_HANDLER.fmg( 310,1682):��������� (RE4) (�������)
      I0_oru = 91
C LODOCHKA_HANDLER.fmg( 183,1358):��������� ������������� IN (�������)
      R0_utu = 7668.0
C LODOCHKA_HANDLER.fmg( 160,1391):��������� (RE4) (�������)
      R0_uvu = 700.0
C LODOCHKA_HANDLER.fmg( 160,1374):��������� (RE4) (�������)
      R0_uxu = 790.0
C LODOCHKA_HANDLER.fmg( 160,1382):��������� (RE4) (�������)
      I0_ebad = 94
C LODOCHKA_HANDLER.fmg( 219,1389):��������� ������������� IN (�������)
      L0_ibad=.true.
C LODOCHKA_HANDLER.fmg( 214,1966):��������� ���������� (�������)
      R0_idad = 789.0
C LODOCHKA_HANDLER.fmg( 158,1942):��������� (RE4) (�������)
      R0_efad = 7668.0
C LODOCHKA_HANDLER.fmg( 158,1951):��������� (RE4) (�������)
      I0_ukad = 91
C LODOCHKA_HANDLER.fmg( 183,1404):��������� ������������� IN (�������)
      R0_apad = 19709.0
C LODOCHKA_HANDLER.fmg( 160,1436):��������� (RE4) (�������)
      R0_arad = 500.0
C LODOCHKA_HANDLER.fmg( 160,1420):��������� (RE4) (�������)
      R0_asad = 790.0
C LODOCHKA_HANDLER.fmg( 160,1428):��������� (RE4) (�������)
      I0_isad = 96
C LODOCHKA_HANDLER.fmg( 219,1434):��������� ������������� IN (�������)
      R0_etad = 789.0
C LODOCHKA_HANDLER.fmg( 158,1842):��������� (RE4) (�������)
      R0_avad = 19709.0
C LODOCHKA_HANDLER.fmg( 158,1851):��������� (RE4) (�������)
      L0_oxad=.true.
C LODOCHKA_HANDLER.fmg( 214,1866):��������� ���������� (�������)
      R0_afed = 0.0
C LODOCHKA_HANDLER.fmg( 358,1684):��������� (RE4) (�������)
      R0_uded = 0.0
C LODOCHKA_HANDLER.fmg( 310,1676):��������� (RE4) (�������)
      R0_iked = 789.0
C LODOCHKA_HANDLER.fmg( 158,1888):��������� (RE4) (�������)
      R0_eled = 16310.0
C LODOCHKA_HANDLER.fmg( 158,1896):��������� (RE4) (�������)
      L0_umed=.true.
C LODOCHKA_HANDLER.fmg( 214,1912):��������� ���������� (�������)
      R0_esed = 2682.0
C LODOCHKA_HANDLER.fmg( 158,1790):��������� (RE4) (�������)
      R0_ated = 16310.0
C LODOCHKA_HANDLER.fmg( 158,1798):��������� (RE4) (�������)
      R0_ited = 0.0
C LODOCHKA_HANDLER.fmg( 359,1750):��������� (RE4) (�������)
      R0_ived = 1000
C LODOCHKA_HANDLER.fmg( 311,1716):��������� (RE4) (�������)
      L0_ubid=.true.
C LODOCHKA_HANDLER.fmg( 214,1814):��������� ���������� (�������)
      R0_ofid = 0.0
C LODOCHKA_HANDLER.fmg( 206,1978):��������� (RE4) (�������)
      R0_elid = 999999.0
C LODOCHKA_HANDLER.fmg( 508,1504):��������� (RE4) (�������)
      L0_okid=.false.
C LODOCHKA_HANDLER.fmg( 474,1266):��������� ���������� (�������)
      R0_alid = 1.0
C LODOCHKA_HANDLER.fmg( 472,1282):��������� (RE4) (�������)
      R0_ilid = 0.0
C LODOCHKA_HANDLER.fmg( 478,1282):��������� (RE4) (�������)
      R0_imid = 0.0
C LODOCHKA_HANDLER.fmg( 497,1588):��������� (RE4) (�������)
      I0_amid = 91
C LODOCHKA_HANDLER.fmg( 428,1582):��������� ������������� IN (�������)
      L0_upid=.false.
C LODOCHKA_HANDLER.fmg( 474,1302):��������� ���������� (�������)
      R0_opid = 1.0
C LODOCHKA_HANDLER.fmg( 472,1318):��������� (RE4) (�������)
      R0_arid = 0.0
C LODOCHKA_HANDLER.fmg( 478,1318):��������� (RE4) (�������)
      L0_asid=.false.
C LODOCHKA_HANDLER.fmg( 474,1337):��������� ���������� (�������)
      R0_orid = 1.0
C LODOCHKA_HANDLER.fmg( 472,1352):��������� (RE4) (�������)
      R0_esid = 0.0
C LODOCHKA_HANDLER.fmg( 478,1352):��������� (RE4) (�������)
      R0_atid = 999999.0
C LODOCHKA_HANDLER.fmg( 358,1504):��������� (RE4) (�������)
      R0_otid = 6000.0
C LODOCHKA_HANDLER.fmg( 359,1518):��������� (RE4) (�������)
      R0_utid = 999999.0
C LODOCHKA_HANDLER.fmg( 359,1520):��������� (RE4) (�������)
      I0_avid = 91
C LODOCHKA_HANDLER.fmg( 290,1499):��������� ������������� IN (�������)
      I0_evid = 65
C LODOCHKA_HANDLER.fmg( 290,1508):��������� ������������� IN (�������)
      R0_uvid = 999999.0
C LODOCHKA_HANDLER.fmg( 216,1502):��������� (RE4) (�������)
      R0_ixid = 18000.0
C LODOCHKA_HANDLER.fmg( 216,1516):��������� (RE4) (�������)
      R0_oxid = 999999.0
C LODOCHKA_HANDLER.fmg( 216,1518):��������� (RE4) (�������)
      I0_uxid = 91
C LODOCHKA_HANDLER.fmg( 148,1497):��������� ������������� IN (�������)
      I0_abod = 65
C LODOCHKA_HANDLER.fmg( 148,1506):��������� ������������� IN (�������)
      I0_ibod = 91
C LODOCHKA_HANDLER.fmg( 288,1582):��������� ������������� IN (�������)
      R0_ubod = 0.0
C LODOCHKA_HANDLER.fmg( 357,1588):��������� (RE4) (�������)
      I0_odod = 91
C LODOCHKA_HANDLER.fmg( 148,1576):��������� ������������� IN (�������)
      R0_efod = 0.0
C LODOCHKA_HANDLER.fmg( 218,1597):��������� (RE4) (�������)
      I0_ifod = 65
C LODOCHKA_HANDLER.fmg( 148,1586):��������� ������������� IN (�������)
      R0_ilod = 0.0
C LODOCHKA_HANDLER.fmg(  61,1946):��������� (RE4) (�������)
      R0_amod = 0.0
C LODOCHKA_HANDLER.fmg(  61,1965):��������� (RE4) (�������)
      C20_apod = 'FDA60'
C LODOCHKA_HANDLER.fmg(  22,1973):��������� ���������� CH20 (CH30) (�������)
      R0_olo = R0_elo + (-R_okod)
C LODOCHKA_HANDLER.fmg(  43,1902):��������
C label 287  try287=try287-1
      R0_otu = R0_utu + (-R_okod)
C LODOCHKA_HANDLER.fmg( 168,1389):��������
      R0_itu=abs(R0_otu)
C LODOCHKA_HANDLER.fmg( 174,1389):���������� ��������
      L0_atu=R0_itu.lt.R_etu
C LODOCHKA_HANDLER.fmg( 182,1389):���������� <
      R0_umad = R0_apad + (-R_okod)
C LODOCHKA_HANDLER.fmg( 168,1434):��������
      R0_omad=abs(R0_umad)
C LODOCHKA_HANDLER.fmg( 174,1434):���������� ��������
      L0_emad=R0_omad.lt.R_imad
C LODOCHKA_HANDLER.fmg( 182,1434):���������� <
      R0_urad = R0_asad + (-R_irid)
C LODOCHKA_HANDLER.fmg( 168,1426):��������
      R0_orad=abs(R0_urad)
C LODOCHKA_HANDLER.fmg( 174,1426):���������� ��������
      L0_erad=R0_orad.lt.R_irad
C LODOCHKA_HANDLER.fmg( 182,1426):���������� <
      L0_ofi=I_ofod.eq.I0_ufi
C LODOCHKA_HANDLER.fmg( 660,1927):���������� �������������
      L0_efi = L0_edi.AND.L0_ofi
C LODOCHKA_HANDLER.fmg( 672,1932):�
      if(L0_efi.and..not.L_afi) then
         R_odi=R_udi
      else
         R_odi=max(R0_idi-deltat,0.0)
      endif
      L0_iki=R_odi.gt.0.0
      L_afi=L0_efi
C LODOCHKA_HANDLER.fmg( 690,1940):������������  �� T
      if(L0_iki) then
         R0_ukid=R0_aki
      endif
C LODOCHKA_HANDLER.fmg( 701,1970):���� � ������������� �������
      R0_akad = R_ifad + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1958):��������
      R0_ufad=abs(R0_akad)
C LODOCHKA_HANDLER.fmg( 172,1958):���������� ��������
      L0_okad=R0_ufad.lt.R_ofad
C LODOCHKA_HANDLER.fmg( 180,1958):���������� <
      R0_afad = R0_efad + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1949):��������
      R0_udad=abs(R0_afad)
C LODOCHKA_HANDLER.fmg( 172,1949):���������� ��������
      L0_ikad=R0_udad.lt.R_odad
C LODOCHKA_HANDLER.fmg( 180,1949):���������� <
      R0_edad = R0_idad + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1940):��������
      R0_adad=abs(R0_edad)
C LODOCHKA_HANDLER.fmg( 172,1940):���������� ��������
      L0_ekad=R0_adad.lt.R_ubad
C LODOCHKA_HANDLER.fmg( 180,1940):���������� <
      L0_edid = L_udid.AND.L0_okad.AND.L0_ikad.AND.L0_ekad
C LODOCHKA_HANDLER.fmg( 191,1962):�
      L_efid=L0_edid.or.(L_efid.and..not.(L_odid))
      L0_idid=.not.L_efid
C LODOCHKA_HANDLER.fmg( 201,1960):RS �������
      if(L_efid) then
         R0_ifid=R_umid
      else
         R0_ifid=R0_ofid
      endif
C LODOCHKA_HANDLER.fmg( 209,1976):���� RE IN LO CH7
      R0_amed = R_iled + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1903):��������
      R0_uled=abs(R0_amed)
C LODOCHKA_HANDLER.fmg( 172,1903):���������� ��������
      L0_omed=R0_uled.lt.R_oled
C LODOCHKA_HANDLER.fmg( 180,1903):���������� <
      R0_aled = R0_eled + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1894):��������
      R0_uked=abs(R0_aled)
C LODOCHKA_HANDLER.fmg( 172,1894):���������� ��������
      L0_imed=R0_uked.lt.R_oked
C LODOCHKA_HANDLER.fmg( 180,1894):���������� <
      R0_eked = R0_iked + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1886):��������
      R0_aked=abs(R0_eked)
C LODOCHKA_HANDLER.fmg( 172,1886):���������� ��������
      L0_emed=R0_aked.lt.R_ufed
C LODOCHKA_HANDLER.fmg( 180,1886):���������� <
      L0_iped = L_ired.AND.L0_omed.AND.L0_imed.AND.L0_emed
C LODOCHKA_HANDLER.fmg( 191,1908):�
      L_oped=L0_iped.or.(L_oped.and..not.(L_ared))
      L0_uped=.not.L_oped
C LODOCHKA_HANDLER.fmg( 200,1906):RS �������
      if(L_oped) then
         R0_eped=R_ered
      else
         R0_eped=R0_ifid
      endif
C LODOCHKA_HANDLER.fmg( 209,1917):���� RE IN LO CH7
      R0_uvad = R_evad + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1858):��������
      R0_ovad=abs(R0_uvad)
C LODOCHKA_HANDLER.fmg( 172,1858):���������� ��������
      L0_ixad=R0_ovad.lt.R_ivad
C LODOCHKA_HANDLER.fmg( 180,1858):���������� <
      R0_utad = R0_avad + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1849):��������
      R0_otad=abs(R0_utad)
C LODOCHKA_HANDLER.fmg( 172,1849):���������� ��������
      L0_exad=R0_otad.lt.R_itad
C LODOCHKA_HANDLER.fmg( 180,1849):���������� <
      R0_atad = R0_etad + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1840):��������
      R0_usad=abs(R0_atad)
C LODOCHKA_HANDLER.fmg( 172,1840):���������� ��������
      L0_axad=R0_usad.lt.R_osad
C LODOCHKA_HANDLER.fmg( 180,1840):���������� <
      L0_abed = L_aded.AND.L0_ixad.AND.L0_exad.AND.L0_axad
C LODOCHKA_HANDLER.fmg( 191,1862):�
      L_ebed=L0_abed.or.(L_ebed.and..not.(L_obed))
      L0_ibed=.not.L_ebed
C LODOCHKA_HANDLER.fmg( 200,1860):RS �������
      if(L_ebed) then
         R0_afid=R_ubed
      else
         R0_afid=R0_eped
      endif
C LODOCHKA_HANDLER.fmg( 209,1872):���� RE IN LO CH7
      R0_abid = R_ixed + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1805):��������
      R0_uxed=abs(R0_abid)
C LODOCHKA_HANDLER.fmg( 172,1805):���������� ��������
      L0_obid=R0_uxed.lt.R_oxed
C LODOCHKA_HANDLER.fmg( 180,1805):���������� <
      R0_used = R0_ated + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1796):��������
      R0_osed=abs(R0_used)
C LODOCHKA_HANDLER.fmg( 172,1796):���������� ��������
      L0_ibid=R0_osed.lt.R_ised
C LODOCHKA_HANDLER.fmg( 180,1796):���������� <
      R0_ased = R0_esed + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1788):��������
      R0_ured=abs(R0_ased)
C LODOCHKA_HANDLER.fmg( 172,1788):���������� ��������
      L0_ebid=R0_ured.lt.R_ored
C LODOCHKA_HANDLER.fmg( 180,1788):���������� <
      L0_ufid = L_ipid.AND.L0_obid.AND.L0_ibid.AND.L0_ebid
C LODOCHKA_HANDLER.fmg( 191,1810):�
      L_akid=L0_ufid.or.(L_akid.and..not.(L_ikid))
      L0_ekid=.not.L_akid
C LODOCHKA_HANDLER.fmg( 200,1808):RS �������
      if(L_akid) then
         R0_epid=R_apid
      else
         R0_epid=R0_afid
      endif
C LODOCHKA_HANDLER.fmg( 209,1819):���� RE IN LO CH7
      if(L0_iki) then
         I_ofod=I0_obi
      endif
C LODOCHKA_HANDLER.fmg( 701,1983):���� � ������������� �������
      L0_emid=I_ofod.eq.I0_amid
C LODOCHKA_HANDLER.fmg( 452,1582):���������� �������������
      if(L0_emid) then
         R0_omid=R0_epid
      else
         R0_omid=R0_imid
      endif
C LODOCHKA_HANDLER.fmg( 500,1586):���� RE IN LO CH7
      R0_upad = R0_arad + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 168,1418):��������
      R0_opad=abs(R0_upad)
C LODOCHKA_HANDLER.fmg( 174,1418):���������� ��������
      L0_epad=R0_opad.lt.R_ipad
C LODOCHKA_HANDLER.fmg( 182,1418):���������� <
      L0_ulad=I_ofod.eq.I0_ukad
C LODOCHKA_HANDLER.fmg( 189,1405):���������� �������������
      L0_amad = L0_emad.AND.L0_erad.AND.L0_epad.AND.L_obed.AND.L0_ulad
C LODOCHKA_HANDLER.fmg( 200,1424):�
      if(L0_amad.and..not.L_olad) then
         R_elad=R_ilad
      else
         R_elad=max(R0_alad-deltat,0.0)
      endif
      L0_esad=R_elad.gt.0.0
      L_olad=L0_amad
C LODOCHKA_HANDLER.fmg( 213,1424):������������  �� T
      if(L0_esad) then
         I_ofod=I0_isad
      endif
C LODOCHKA_HANDLER.fmg( 225,1434):���� � ������������� �������
      L0_oso=I_ofod.eq.I0_uso
C LODOCHKA_HANDLER.fmg( 179,1331):���������� �������������
      L0_iso = L0_oso.AND.L0_eso.AND.L0_aso
C LODOCHKA_HANDLER.fmg( 191,1329):�
      if(L0_iso.and..not.L_aro) then
         R_opo=R_upo
      else
         R_opo=max(R0_ipo-deltat,0.0)
      endif
      L0_oro=R_opo.gt.0.0
      L_aro=L0_iso
C LODOCHKA_HANDLER.fmg( 209,1329):������������  �� T
      if(L0_oro) then
         R0_erid=R0_uro
      endif
C LODOCHKA_HANDLER.fmg( 220,1298):���� � ������������� �������
      if(L0_iki) then
         R0_erid=R0_eki
      endif
C LODOCHKA_HANDLER.fmg( 701,1958):���� � ������������� �������
      R0_apu = R_emu + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1732):��������
      R0_umu=abs(R0_apu)
C LODOCHKA_HANDLER.fmg( 172,1732):���������� ��������
      L0_imu=R0_umu.lt.R_omu
C LODOCHKA_HANDLER.fmg( 180,1732):���������� <
      R0_oxo = R0_exo + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1722):��������
      R0_ixo=abs(R0_oxo)
C LODOCHKA_HANDLER.fmg( 172,1722):���������� ��������
      L0_uku=R0_ixo.lt.R_axo
C LODOCHKA_HANDLER.fmg( 180,1722):���������� <
      R0_amu = R0_olu + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1712):��������
      R0_ulu=abs(R0_amu)
C LODOCHKA_HANDLER.fmg( 172,1712):���������� ��������
      L0_elu=R0_ulu.lt.R_ilu
C LODOCHKA_HANDLER.fmg( 180,1712):���������� <
      L0_alu = L0_imu.AND.L0_uku.AND.L0_elu
C LODOCHKA_HANDLER.fmg( 188,1730):�
      if(L0_alu) then
         R0_obod=R_iku
      else
         R0_obod=R0_oku
      endif
C LODOCHKA_HANDLER.fmg( 210,1747):���� RE IN LO CH7
      if(L0_oro) then
         I_ofod=I0_ero
      endif
C LODOCHKA_HANDLER.fmg( 220,1324):���� � ������������� �������
      L0_ebod=I_ofod.eq.I0_ibod
C LODOCHKA_HANDLER.fmg( 312,1582):���������� �������������
      if(L0_ebod) then
         R0_aru=R0_obod
      else
         R0_aru=R0_ubod
      endif
C LODOCHKA_HANDLER.fmg( 360,1586):���� RE IN LO CH7
      L0_axed=R_irid.lt.R_exed
C LODOCHKA_HANDLER.fmg( 333,1734):���������� <
      R0_uved = R0_ived + (-R_okod)
C LODOCHKA_HANDLER.fmg( 318,1714):��������
      R0_oved=abs(R0_uved)
C LODOCHKA_HANDLER.fmg( 324,1714):���������� ��������
      L0_aved=R0_oved.lt.R_eved
C LODOCHKA_HANDLER.fmg( 333,1714):���������� <
      L0_uted = L0_axed.AND.L0_aved
C LODOCHKA_HANDLER.fmg( 340,1733):�
      if(L0_uted) then
         R0_oted=R0_eted
      else
         R0_oted=R0_ited
      endif
C LODOCHKA_HANDLER.fmg( 362,1748):���� RE IN LO CH7
      L0_eru=I_ofod.eq.I0_iru
C LODOCHKA_HANDLER.fmg( 312,1575):���������� �������������
      if(L0_eru) then
         R0_upu=R0_oted
      else
         R0_upu=R0_aru
      endif
C LODOCHKA_HANDLER.fmg( 360,1578):���� RE IN LO CH7
      R0_ilo=abs(R0_olo)
C LODOCHKA_HANDLER.fmg(  49,1902):���������� ��������
      L0_uko=R0_ilo.lt.R_alo
C LODOCHKA_HANDLER.fmg(  58,1902):���������� <
      if(L0_uko) then
         R0_oko=R_emo
      else
         R0_oko=R0_iko
      endif
C LODOCHKA_HANDLER.fmg(  74,1912):���� RE IN LO CH7
      L0_evi=I_ofod.eq.I0_ivi
C LODOCHKA_HANDLER.fmg( 312,1566):���������� �������������
      if(L0_evi) then
         R0_ekod=R0_oko
      else
         R0_ekod=R0_upu
      endif
C LODOCHKA_HANDLER.fmg( 360,1570):���� RE IN LO CH7
      L0_etid=I_ofod.eq.I0_evid
C LODOCHKA_HANDLER.fmg( 314,1510):���������� �������������
      if(L0_etid) then
         R0_itid=R0_otid
      else
         R0_itid=R0_utid
      endif
C LODOCHKA_HANDLER.fmg( 362,1518):���� RE IN LO CH7
      L0_osid=I_ofod.eq.I0_avid
C LODOCHKA_HANDLER.fmg( 314,1500):���������� �������������
      if(L0_osid) then
         R0_usid=R0_atid
      else
         R0_usid=R0_itid
      endif
C LODOCHKA_HANDLER.fmg( 362,1504):���� RE IN LO CH7
      R0_oxu = R0_uxu + (-R_irid)
C LODOCHKA_HANDLER.fmg( 168,1380):��������
      R0_ixu=abs(R0_oxu)
C LODOCHKA_HANDLER.fmg( 174,1380):���������� ��������
      L0_axu=R0_ixu.lt.R_exu
C LODOCHKA_HANDLER.fmg( 182,1380):���������� <
      R0_ovu = R0_uvu + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 168,1372):��������
      R0_ivu=abs(R0_ovu)
C LODOCHKA_HANDLER.fmg( 174,1372):���������� ��������
      L0_avu=R0_ivu.lt.R_evu
C LODOCHKA_HANDLER.fmg( 182,1372):���������� <
      L0_osu=I_ofod.eq.I0_oru
C LODOCHKA_HANDLER.fmg( 189,1360):���������� �������������
      L0_usu = L0_atu.AND.L0_axu.AND.L0_avu.AND.L_odid.AND.L0_osu
C LODOCHKA_HANDLER.fmg( 200,1378):�
      if(L0_usu.and..not.L_isu) then
         R_asu=R_esu
      else
         R_asu=max(R0_uru-deltat,0.0)
      endif
      L0_abad=R_asu.gt.0.0
      L_isu=L0_usu
C LODOCHKA_HANDLER.fmg( 213,1378):������������  �� T
      if(L0_abad) then
         I_ofod=I0_ebad
      endif
C LODOCHKA_HANDLER.fmg( 225,1388):���� � ������������� �������
      L0_abi=I_ofod.eq.I0_ebi
C LODOCHKA_HANDLER.fmg( 660,1906):���������� �������������
      L0_uxe = L_ibi.AND.L0_abi
C LODOCHKA_HANDLER.fmg( 672,1914):�
      if(L0_uxe.and..not.L_ove) then
         R_eve=R_ive
      else
         R_eve=max(R0_ave-deltat,0.0)
      endif
      L0_ixe=R_eve.gt.0.0
      L_ove=L0_uxe
C LODOCHKA_HANDLER.fmg( 690,1914):������������  �� T
      if(L0_ixe) then
         R0_isid=R0_oxe
      endif
C LODOCHKA_HANDLER.fmg( 701,1868):���� � ������������� �������
      if(L0_iki) then
         R0_isid=R0_oki
      endif
C LODOCHKA_HANDLER.fmg( 701,1945):���� � ������������� �������
      if(L0_oro) then
         R0_isid=R0_iro
      endif
C LODOCHKA_HANDLER.fmg( 220,1311):���� � ������������� �������
      if(L0_ixe) then
         R0_erid=R0_exe
      endif
C LODOCHKA_HANDLER.fmg( 701,1880):���� � ������������� �������
      L0_oli = L0_efi.OR.L0_uxe
C LODOCHKA_HANDLER.fmg( 681,1931):���
      if(L0_oli.and..not.L_ili) then
         R_ali=R_eli
      else
         R_ali=max(R0_uki-deltat,0.0)
      endif
      L0_urid=R_ali.gt.0.0
      L_ili=L0_oli
C LODOCHKA_HANDLER.fmg( 690,1931):������������  �� T
      if(L0_urid) then
         R_irid=R0_erid
      elseif(L0_upid) then
         R_irid=R_irid
      else
         R_irid=R_irid+deltat/R0_opid*R0_ekod
      endif
      if(R_irid.gt.R0_usid) then
         R_irid=R0_usid
      elseif(R_irid.lt.R0_arid) then
         R_irid=R0_arid
      endif
C LODOCHKA_HANDLER.fmg( 476,1309):����������
C sav1=R0_ased
      R0_ased = R0_esed + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1788):recalc:��������
C if(sav1.ne.R0_ased .and. try398.gt.0) goto 398
C sav1=R0_eked
      R0_eked = R0_iked + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1886):recalc:��������
C if(sav1.ne.R0_eked .and. try354.gt.0) goto 354
C sav1=R0_atad
      R0_atad = R0_etad + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1840):recalc:��������
C if(sav1.ne.R0_atad .and. try376.gt.0) goto 376
C sav1=R0_urad
      R0_urad = R0_asad + (-R_irid)
C LODOCHKA_HANDLER.fmg( 168,1426):recalc:��������
C if(sav1.ne.R0_urad .and. try298.gt.0) goto 298
C sav1=R0_oxu
      R0_oxu = R0_uxu + (-R_irid)
C LODOCHKA_HANDLER.fmg( 168,1380):recalc:��������
C if(sav1.ne.R0_oxu .and. try565.gt.0) goto 565
C sav1=R0_apu
      R0_apu = R_emu + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1732):recalc:��������
C if(sav1.ne.R0_apu .and. try491.gt.0) goto 491
C sav1=R0_edad
      R0_edad = R0_idad + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1940):recalc:��������
C if(sav1.ne.R0_edad .and. try332.gt.0) goto 332
      R_umod=R_irid
C LODOCHKA_HANDLER.fmg( 506,1309):������,VY01
      L0_emod=R_umod.lt.R_imod
C LODOCHKA_HANDLER.fmg(  54,1958):���������� <
      if(L0_emod) then
         R0_omod=R_ulod
      else
         R0_omod=R0_amod
      endif
C LODOCHKA_HANDLER.fmg(  64,1964):���� RE IN LO CH7
      L0_alod=R_umod.gt.R_ukod
C LODOCHKA_HANDLER.fmg(  54,1939):���������� >
      if(L0_alod) then
         R0_olod=R_elod
      else
         R0_olod=R0_ilod
      endif
C LODOCHKA_HANDLER.fmg(  64,1945):���� RE IN LO CH7
      R0_akod = R0_omod + R0_olod
C LODOCHKA_HANDLER.fmg( 212,1595):��������
      if(L0_ixe) then
         I_ofod=I0_uve
      endif
C LODOCHKA_HANDLER.fmg( 701,1906):���� � ������������� �������
      L0_udod=I_ofod.eq.I0_ifod
C LODOCHKA_HANDLER.fmg( 172,1588):���������� �������������
      if(L0_udod) then
         R0_afod=R0_akod
      else
         R0_afod=R0_efod
      endif
C LODOCHKA_HANDLER.fmg( 221,1596):���� RE IN LO CH7
      R0_afu = R_idu + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1671):��������
      R0_udu=abs(R0_afu)
C LODOCHKA_HANDLER.fmg( 172,1671):���������� ��������
      L0_ofu=R0_udu.lt.R_odu
C LODOCHKA_HANDLER.fmg( 180,1671):���������� <
      R0_adu = R0_edu + (-R_irid)
C LODOCHKA_HANDLER.fmg( 166,1658):��������
      R0_ubu=abs(R0_adu)
C LODOCHKA_HANDLER.fmg( 172,1658):���������� ��������
      L0_ifu=R0_ubu.lt.R_obu
C LODOCHKA_HANDLER.fmg( 180,1658):���������� <
      if(L0_ixe) then
         R0_ukid=R0_axe
      endif
C LODOCHKA_HANDLER.fmg( 701,1894):���� � ������������� �������
      if(L0_urid) then
         R_ulid=R0_ukid
      elseif(L0_okid) then
         R_ulid=R_ulid
      else
         R_ulid=R_ulid+deltat/R0_alid*R0_omid
      endif
      if(R_ulid.gt.R0_elid) then
         R_ulid=R0_elid
      elseif(R_ulid.lt.R0_ilid) then
         R_ulid=R0_ilid
      endif
C LODOCHKA_HANDLER.fmg( 476,1273):����������
C sav1=R0_upad
      R0_upad = R0_arad + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 168,1418):recalc:��������
C if(sav1.ne.R0_upad .and. try463.gt.0) goto 463
C sav1=R0_amed
      R0_amed = R_iled + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1903):recalc:��������
C if(sav1.ne.R0_amed .and. try344.gt.0) goto 344
C sav1=R0_oxo
      R0_oxo = R0_exo + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1722):recalc:��������
C if(sav1.ne.R0_oxo .and. try496.gt.0) goto 496
C sav1=R0_uvad
      R0_uvad = R_evad + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1858):recalc:��������
C if(sav1.ne.R0_uvad .and. try366.gt.0) goto 366
C sav1=R0_akad
      R0_akad = R_ifad + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1958):recalc:��������
C if(sav1.ne.R0_akad .and. try322.gt.0) goto 322
C sav1=R0_ovu
      R0_ovu = R0_uvu + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 168,1372):recalc:��������
C if(sav1.ne.R0_ovu .and. try570.gt.0) goto 570
C sav1=R0_abid
      R0_abid = R_ixed + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1805):recalc:��������
C if(sav1.ne.R0_abid .and. try388.gt.0) goto 388
      R0_ebu = R0_ibu + (-R_ulid)
C LODOCHKA_HANDLER.fmg( 166,1650):��������
      R0_abu=abs(R0_ebu)
C LODOCHKA_HANDLER.fmg( 172,1650):���������� ��������
      L0_efu=R0_abu.lt.R_uxo
C LODOCHKA_HANDLER.fmg( 180,1650):���������� <
      L0_aku = L0_ofu.AND.L0_ifu.AND.L0_efu
C LODOCHKA_HANDLER.fmg( 191,1669):�
      if(L0_aku) then
         R0_idod=R_eku
      else
         R0_idod=R0_ufu
      endif
C LODOCHKA_HANDLER.fmg( 209,1682):���� RE IN LO CH7
      L0_adod=I_ofod.eq.I0_odod
C LODOCHKA_HANDLER.fmg( 172,1578):���������� �������������
      if(L0_adod) then
         R0_edod=R0_idod
      else
         R0_edod=R0_afod
      endif
C LODOCHKA_HANDLER.fmg( 220,1582):���� RE IN LO CH7
      R0_oded = R0_uded + (-R_irid)
C LODOCHKA_HANDLER.fmg( 317,1674):��������
      R0_ided=abs(R0_oded)
C LODOCHKA_HANDLER.fmg( 323,1674):���������� ��������
      L0_efed=R0_ided.lt.R_eded
C LODOCHKA_HANDLER.fmg( 332,1674):���������� <
      if(L0_efed) then
         R0_ofed=R0_ifed
      else
         R0_ofed=R0_afed
      endif
C LODOCHKA_HANDLER.fmg( 362,1683):���� RE IN LO CH7
      L0_ipu=I_ofod.eq.I0_opu
C LODOCHKA_HANDLER.fmg( 172,1568):���������� �������������
      if(L0_ipu) then
         R0_epu=R0_ofed
      else
         R0_epu=R0_edod
      endif
C LODOCHKA_HANDLER.fmg( 220,1572):���� RE IN LO CH7
      R0_oxi = R0_exi + (-R_irid)
C LODOCHKA_HANDLER.fmg(  43,1853):��������
      R0_ixi=abs(R0_oxi)
C LODOCHKA_HANDLER.fmg(  49,1853):���������� ��������
      L0_uxi=R0_ixi.lt.R_axi
C LODOCHKA_HANDLER.fmg(  58,1853):���������� <
      if(L0_uxi) then
         R0_ido=R_abo
      else
         R0_ido=R0_odo
      endif
C LODOCHKA_HANDLER.fmg(  74,1860):���� RE IN LO CH7
      R0_ubo = R0_ibo + (-R_irid)
C LODOCHKA_HANDLER.fmg(  43,1838):��������
      R0_obo=abs(R0_ubo)
C LODOCHKA_HANDLER.fmg(  49,1838):���������� ��������
      L0_ado=R0_obo.lt.R_ebo
C LODOCHKA_HANDLER.fmg(  58,1838):���������� <
      if(L0_ado) then
         R0_afo=R_edo
      else
         R0_afo=R0_ido
      endif
C LODOCHKA_HANDLER.fmg(  74,1846):���� RE IN LO CH7
      R0_eko = R0_ufo + (-R_irid)
C LODOCHKA_HANDLER.fmg(  43,1820):��������
      R0_ako=abs(R0_eko)
C LODOCHKA_HANDLER.fmg(  49,1820):���������� ��������
      L0_ifo=R0_ako.lt.R_ofo
C LODOCHKA_HANDLER.fmg(  58,1820):���������� <
      if(L0_ifo) then
         R0_efo=R_udo
      else
         R0_efo=R0_afo
      endif
C LODOCHKA_HANDLER.fmg(  74,1830):���� RE IN LO CH7
      L0_ovi=I_ofod.eq.I0_uvi
C LODOCHKA_HANDLER.fmg( 172,1560):���������� �������������
      if(L0_ovi) then
         R0_ufod=R0_efo
      else
         R0_ufod=R0_epu
      endif
C LODOCHKA_HANDLER.fmg( 220,1563):���� RE IN LO CH7
      L0_axid=I_ofod.eq.I0_abod
C LODOCHKA_HANDLER.fmg( 172,1508):���������� �������������
      if(L0_axid) then
         R0_exid=R0_ixid
      else
         R0_exid=R0_oxid
      endif
C LODOCHKA_HANDLER.fmg( 220,1516):���� RE IN LO CH7
      L0_ivid=I_ofod.eq.I0_uxid
C LODOCHKA_HANDLER.fmg( 172,1498):���������� �������������
      if(L0_ivid) then
         R0_ovid=R0_uvid
      else
         R0_ovid=R0_exid
      endif
C LODOCHKA_HANDLER.fmg( 220,1502):���� RE IN LO CH7
      if(L0_urid) then
         R_okod=R0_isid
      elseif(L0_asid) then
         R_okod=R_okod
      else
         R_okod=R_okod+deltat/R0_orid*R0_ufod
      endif
      if(R_okod.gt.R0_ovid) then
         R_okod=R0_ovid
      elseif(R_okod.lt.R0_esid) then
         R_okod=R0_esid
      endif
C LODOCHKA_HANDLER.fmg( 476,1344):����������
C sav1=R0_aled
      R0_aled = R0_eled + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1894):recalc:��������
C if(sav1.ne.R0_aled .and. try349.gt.0) goto 349
C sav1=R0_afad
      R0_afad = R0_efad + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1949):recalc:��������
C if(sav1.ne.R0_afad .and. try327.gt.0) goto 327
C sav1=R0_used
      R0_used = R0_ated + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1796):recalc:��������
C if(sav1.ne.R0_used .and. try393.gt.0) goto 393
C sav1=R0_uved
      R0_uved = R0_ived + (-R_okod)
C LODOCHKA_HANDLER.fmg( 318,1714):recalc:��������
C if(sav1.ne.R0_uved .and. try521.gt.0) goto 521
C sav1=R0_olo
      R0_olo = R0_elo + (-R_okod)
C LODOCHKA_HANDLER.fmg(  43,1902):recalc:��������
C if(sav1.ne.R0_olo .and. try287.gt.0) goto 287
C sav1=R0_otu
      R0_otu = R0_utu + (-R_okod)
C LODOCHKA_HANDLER.fmg( 168,1389):recalc:��������
C if(sav1.ne.R0_otu .and. try288.gt.0) goto 288
C sav1=R0_afu
      R0_afu = R_idu + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1671):recalc:��������
C if(sav1.ne.R0_afu .and. try674.gt.0) goto 674
C sav1=R0_amu
      R0_amu = R0_olu + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1712):recalc:��������
C if(sav1.ne.R0_amu .and. try501.gt.0) goto 501
C sav1=R0_utad
      R0_utad = R0_avad + (-R_okod)
C LODOCHKA_HANDLER.fmg( 166,1849):recalc:��������
C if(sav1.ne.R0_utad .and. try371.gt.0) goto 371
C sav1=R0_umad
      R0_umad = R0_apad + (-R_okod)
C LODOCHKA_HANDLER.fmg( 168,1434):recalc:��������
C if(sav1.ne.R0_umad .and. try293.gt.0) goto 293
      R_ikod=R_okod
C LODOCHKA_HANDLER.fmg( 506,1344):������,VX01
      R_olid=R_ulid
C LODOCHKA_HANDLER.fmg( 506,1273):������,VZ01
      I_avi=I_ofod
C LODOCHKA_HANDLER.fmg( 497,1248):������,LP
      L0_ovo=I_ofod.eq.I0_uvo
C LODOCHKA_HANDLER.fmg( 478,1420):���������� �������������
      L0_avo = L0_ovo.AND.L0_ito
C LODOCHKA_HANDLER.fmg( 484,1420):�
      if(L_akid) then
         L_adid=L0_ubid
      endif
C LODOCHKA_HANDLER.fmg( 218,1813):���� � ������������� �������
      if(L_ebed) then
         L_uxad=L0_oxad
      endif
C LODOCHKA_HANDLER.fmg( 218,1866):���� � ������������� �������
      if(L_oped) then
         L_aped=L0_umed
      endif
C LODOCHKA_HANDLER.fmg( 218,1911):���� � ������������� �������
      if(L_efid) then
         L_obad=L0_ibad
      endif
C LODOCHKA_HANDLER.fmg( 218,1966):���� � ������������� �������
      L0_el=I_oti.eq.I0_il
C LODOCHKA_HANDLER.fmg( 660,1809):���������� �������������
C label 822  try822=try822-1
      L0_am = L0_el.AND.L_ol
C LODOCHKA_HANDLER.fmg( 673,1808):�
      if(L0_am) then
         I_oti=I0_ul
      endif
C LODOCHKA_HANDLER.fmg( 701,1812):���� � ������������� �������
      L0_od=I_oti.eq.I0_ud
C LODOCHKA_HANDLER.fmg( 817,1718):���������� �������������
      L0_id = L0_od.AND.L_af
C LODOCHKA_HANDLER.fmg( 826,1718):�
      if(L0_id) then
         I_oti=I0_ed
      endif
C LODOCHKA_HANDLER.fmg( 860,1722):���� � ������������� �������
      L0_ife=I_oti.eq.I0_ofe
C LODOCHKA_HANDLER.fmg( 817,1839):���������� �������������
      L0_efe = L0_ife.AND.L_ufe
C LODOCHKA_HANDLER.fmg( 826,1838):�
      L0_ide = L0_efe.AND.L0_ode
C LODOCHKA_HANDLER.fmg( 832,1833):�
      L0_it=I_oti.eq.I0_ot
C LODOCHKA_HANDLER.fmg( 817,1827):���������� �������������
      L0_ebe = L0_it.AND.L_ufe
C LODOCHKA_HANDLER.fmg( 826,1826):�
      L0_ibe = L0_ebe.AND.L0_obe
C LODOCHKA_HANDLER.fmg( 832,1825):�
      L0_at=I_oti.eq.I0_et
C LODOCHKA_HANDLER.fmg( 817,1814):���������� �������������
      L0_ix = L0_at.AND.L_ufe
C LODOCHKA_HANDLER.fmg( 826,1813):�
      L0_ox = L0_ix.AND.L0_ux
C LODOCHKA_HANDLER.fmg( 832,1812):�
      L0_os=I_oti.eq.I0_us
C LODOCHKA_HANDLER.fmg( 817,1800):���������� �������������
      L0_ov = L0_os.AND.L_ufe
C LODOCHKA_HANDLER.fmg( 826,1800):�
      L0_uv = L0_ov.AND.L0_ax
C LODOCHKA_HANDLER.fmg( 832,1798):�
      L0_es=I_oti.eq.I0_is
C LODOCHKA_HANDLER.fmg( 817,1787):���������� �������������
      L0_ut = L0_es.AND.L_ufe
C LODOCHKA_HANDLER.fmg( 826,1786):�
      L0_av = L0_ut.AND.L0_ev
C LODOCHKA_HANDLER.fmg( 832,1785):�
      L0_ade = L0_ide.OR.L0_ibe.OR.L0_ox.OR.L0_uv.OR.L0_av
C LODOCHKA_HANDLER.fmg( 854,1829):���
      if(L0_ade) then
         I_oti=I0_ede
      endif
C LODOCHKA_HANDLER.fmg( 858,1838):���� � ������������� �������
      L0_or=I_oti.eq.I0_ur
C LODOCHKA_HANDLER.fmg( 817,1768):���������� �������������
      L0_ir = L0_or.AND.L_as
C LODOCHKA_HANDLER.fmg( 826,1767):�
      if(L0_ir) then
         I_oti=I0_er
      endif
C LODOCHKA_HANDLER.fmg( 860,1772):���� � ������������� �������
      L0_ale=I_oti.eq.I0_ele
C LODOCHKA_HANDLER.fmg( 660,1756):���������� �������������
      L0_ile = L0_ale.AND.L_ule
C LODOCHKA_HANDLER.fmg( 673,1755):�
      if(L0_ile) then
         I_oti=I0_ole
      endif
C LODOCHKA_HANDLER.fmg( 701,1758):���� � ������������� �������
      L0_ep=I_oti.eq.I0_ip
C LODOCHKA_HANDLER.fmg( 660,1775):���������� �������������
      L0_up = L0_ep.AND.L_ar
C LODOCHKA_HANDLER.fmg( 673,1774):�
      if(L0_up) then
         I_oti=I0_op
      endif
C LODOCHKA_HANDLER.fmg( 701,1778):���� � ������������� �������
      L0_om=I_oti.eq.I0_um
C LODOCHKA_HANDLER.fmg( 817,1756):���������� �������������
      L0_im = L0_om.AND.L_ap
C LODOCHKA_HANDLER.fmg( 826,1755):�
      if(L0_im) then
         I_oti=I0_em
      endif
C LODOCHKA_HANDLER.fmg( 860,1760):���� � ������������� �������
      L0_ame=I_oti.eq.I0_eme
C LODOCHKA_HANDLER.fmg( 660,1766):���������� �������������
      L0_ere = L0_ame.AND.L_ire
C LODOCHKA_HANDLER.fmg( 673,1764):�
      if(L0_ere) then
         I_oti=I0_are
      endif
C LODOCHKA_HANDLER.fmg( 701,1768):���� � ������������� �������
      L0_ime=I_oti.eq.I0_ome
C LODOCHKA_HANDLER.fmg( 660,1786):���������� �������������
      L0_ure = L0_ime.AND.L_ase
C LODOCHKA_HANDLER.fmg( 673,1784):�
      if(L0_ure) then
         I_oti=I0_ore
      endif
C LODOCHKA_HANDLER.fmg( 701,1788):���� � ������������� �������
      L0_ume=I_oti.eq.I0_ape
C LODOCHKA_HANDLER.fmg( 660,1796):���������� �������������
      L0_ise = L0_ume.AND.L_upe
C LODOCHKA_HANDLER.fmg( 673,1795):�
      if(L0_ise) then
         I_oti=I0_ese
      endif
C LODOCHKA_HANDLER.fmg( 701,1798):���� � ������������� �������
      L0_epe=I_oti.eq.I0_ipe
C LODOCHKA_HANDLER.fmg( 660,1822):���������� �������������
      L0_ose = L0_epe.AND.L_ope
C LODOCHKA_HANDLER.fmg( 674,1820):�
      if(L0_ose) then
         I_oti=I0_use
      endif
C LODOCHKA_HANDLER.fmg( 702,1826):���� � ������������� �������
      L0_ote=I_oti.eq.I0_ute
C LODOCHKA_HANDLER.fmg( 660,1836):���������� �������������
      L0_ete = L0_ote.AND.L_ite
C LODOCHKA_HANDLER.fmg( 674,1835):�
      if(L0_ete) then
         I_oti=I0_ate
      endif
C LODOCHKA_HANDLER.fmg( 702,1840):���� � ������������� �������
      L0_o=I_oti.eq.I0_u
C LODOCHKA_HANDLER.fmg( 817,1706):���������� �������������
      L0_i = L0_o.AND.L_ad
C LODOCHKA_HANDLER.fmg( 826,1706):�
      if(L0_i) then
         I_oti=I0_e
      endif
C LODOCHKA_HANDLER.fmg( 860,1710):���� � ������������� �������
      L0_ok=I_oti.eq.I0_uk
C LODOCHKA_HANDLER.fmg( 817,1746):���������� �������������
      L0_ik = L0_ok.AND.L_al
C LODOCHKA_HANDLER.fmg( 826,1744):�
      if(L0_ik) then
         I_oti=I0_ek
      endif
C LODOCHKA_HANDLER.fmg( 860,1749):���� � ������������� �������
      L0_of=I_oti.eq.I0_uf
C LODOCHKA_HANDLER.fmg( 817,1733):���������� �������������
      L0_if = L0_of.AND.L_ak
C LODOCHKA_HANDLER.fmg( 826,1732):�
      if(L0_if) then
         I_oti=I0_ef
      endif
C LODOCHKA_HANDLER.fmg( 860,1736):���� � ������������� �������
      L0_eti=I_oti.eq.I0_iti
C LODOCHKA_HANDLER.fmg( 660,1742):���������� �������������
      L0_ati = L0_eti.AND.L_uti
C LODOCHKA_HANDLER.fmg( 668,1742):�
      L0_iri = L0_ati.AND.L0_ori
C LODOCHKA_HANDLER.fmg( 674,1724):�
      if(L0_iri) then
         I_oti=I0_eri
      endif
C LODOCHKA_HANDLER.fmg( 701,1728):���� � ������������� �������
      L0_opi = L0_ati.AND.L0_upi
C LODOCHKA_HANDLER.fmg( 674,1710):�
      if(L0_opi) then
         I_oti=I0_ipi
      endif
C LODOCHKA_HANDLER.fmg( 701,1716):���� � ������������� �������
      L0_umi = L0_ati.AND.L0_api
C LODOCHKA_HANDLER.fmg( 674,1698):�
      if(L0_umi) then
         I_oti=I0_omi
      endif
C LODOCHKA_HANDLER.fmg( 701,1702):���� � ������������� �������
      L0_ami = L0_ati.AND.L0_emi
C LODOCHKA_HANDLER.fmg( 674,1684):�
      if(L0_ami) then
         I_oti=I0_uli
      endif
C LODOCHKA_HANDLER.fmg( 701,1688):���� � ������������� �������
      L0_esi = L0_ati.AND.L0_isi
C LODOCHKA_HANDLER.fmg( 674,1736):�
      if(L0_esi) then
         I_oti=I0_asi
      endif
C LODOCHKA_HANDLER.fmg( 701,1742):���� � ������������� �������
      L0_oke=I_oti.eq.I0_ike
C LODOCHKA_HANDLER.fmg( 342,1965):���������� �������������
      if(L0_oke) then
         R_ivo=R_uke
      endif
C LODOCHKA_HANDLER.fmg( 373,1970):���� � ������������� �������
      R0_uto = R_ivo
C LODOCHKA_HANDLER.fmg( 468,1432):��������
C label 1047  try1047=try1047-1
      R0_evo = R0_uto + R_oto
C LODOCHKA_HANDLER.fmg( 478,1431):��������
      if(L0_avo) then
         R_ivo=R0_evo
      endif
C LODOCHKA_HANDLER.fmg( 488,1430):���� � ������������� �������
C sav1=R0_uto
      R0_uto = R_ivo
C LODOCHKA_HANDLER.fmg( 468,1432):recalc:��������
C if(sav1.ne.R0_uto .and. try1047.gt.0) goto 1047
      End
