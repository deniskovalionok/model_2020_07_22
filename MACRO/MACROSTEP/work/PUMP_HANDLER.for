      Subroutine PUMP_HANDLER(ext_deltat,C30_ad,I_ed,L_ud
     &,L_of,L_uf,I_il,I_am,R_om,R_um,R_ap,R_ep,I_op,L_us,L_at
     &,L_et,L_it,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv,L_ax,L_ex
     &,L_ix,L_ox,L_ux,L_ibe,L_obe,L_ede,L_ide,L_ode,I_ife
     &,R_eke,R_oke,L_uke,L_ile,L_ole,R8_ime,L_ome,R8_ume,R_ere
     &,R_ure,R_ose,R8_use,R_ete,R8_ite,R_ave,R8_eve,R_ove
     &,R_uve)
C |C30_ad        |3 30 O|state           |||
C |I_ed          |2 4 O|LOFF2           |����� ���������||
C |L_ud          |1 1 I|tech_mode       |||
C |L_of          |1 1 I|ruch_mode       |||
C |L_uf          |1 1 I|avt_mode        |||
C |I_il          |2 4 O|LOFF            |����� ���������||
C |I_am          |2 4 O|LON             |����� ��������||
C |R_om          |4 4 S|turn_off_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_um          |4 4 I|turn_off_button |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 S|turn_on_button_ST*|��������� ������ "������� �������� �� ���������" |0.0|
C |R_ep          |4 4 I|turn_on_button  |������� ������ ������ "������� �������� �� ���������" |0.0|
C |I_op          |2 4 O|LM              |�����||
C |L_us          |1 1 O|block           |||
C |L_at          |1 1 O|fault           |�������������||
C |L_et          |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_it          |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |L_ot          |1 1 O|norm            |�����||
C |L_ut          |1 1 I|gm14            |������������� ������� ���������||
C |L_av          |1 1 I|gm12            |������������� ������� ��������||
C |L_ev          |1 1 I|gm13            |���������������� ����������||
C |L_iv          |1 1 I|gm11            |���������������� ���������||
C |L_ov          |1 1 O|cb_out          |��������� �������� ����.||
C |L_uv          |1 1 I|uluboff         |���������� ���������� �� ���������|F|
C |L_ax          |1 1 O|turn_off_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ex          |1 1 I|ulubon          |���������� ��������� �� ���������|F|
C |L_ix          |1 1 O|turn_on_button_CMD*|[TF]����� ������ ������� �������� �� ���������|F|
C |L_ox          |1 1 S|_qffJ1545*      |�������� ������ Q RS-��������  |F|
C |L_ux          |1 1 I|uluoff1         |������� ��������� �� ����������|F|
C |L_ibe         |1 1 S|_qffJ1541*      |�������� ������ Q RS-��������  |F|
C |L_obe         |1 1 I|uluon1          |������� �������� �� ����������|F|
C |L_ede         |1 1 I|uluoff          |������� ��������� �� ����������|F|
C |L_ide         |1 1 I|uluon           |������� �������� �� ����������|F|
C |L_ode         |1 1 O|lstate          |��������� ����������� ���������||
C |I_ife         |2 4 O|LBM             |||
C |R_eke         |4 4 K|_tdel1          |[]�������� ������� �������� ������|2|
C |R_oke         |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_uke         |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |L_ile         |1 1 I|block_ele       |���������� ������������� �������||
C |L_ole         |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R8_ime        |4 8 I|voltage         |[��]���������� �� ������||
C |L_ome         |1 1 I|el_use          |����� ������||
C |R8_ume        |4 8 I|estate          |������������� �������� EN_CAD|0.0|
C |R_ere         |4 4 O|_oapr5*         |�������� ������ ���������. ����� |0.0|
C |R_ure         |4 4 I|kratn           |��������� ��������� ����|3.0|
C |R_ose         |4 4 O|idvig           |[�] ��� ��������� |3.0|
C |R8_use        |4 8 I|pnagr           |[��] �������� �������� ||
C |R_ete         |4 4 O|power           |�������� ���������|3.0|
C |R8_ite        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |R_ave         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_eve        |4 8 O|fizstate        |������������� ��������|0.0|
C |R_ove         |4 4 I|timeon          |���������� ������� ���������|3.0|
C |R_uve         |4 4 I|timeoff         |���������� ������� ����������|3.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      CHARACTER*30 C30_e,C30_i,C30_o,C30_u,C30_ad
      INTEGER*4 I_ed,I0_id,I0_od
      LOGICAL*1 L_ud
      INTEGER*4 I0_af,I0_ef,I0_if
      LOGICAL*1 L_of,L_uf
      INTEGER*4 I0_ak,I0_ek
      LOGICAL*1 L0_ik
      INTEGER*4 I0_ok
      LOGICAL*1 L0_uk
      INTEGER*4 I0_al,I0_el,I_il,I0_ol,I0_ul,I_am,I0_em,I0_im
      REAL*4 R_om,R_um,R_ap,R_ep
      INTEGER*4 I0_ip,I_op,I0_up,I0_ar
      LOGICAL*1 L0_er,L0_ir,L0_or,L0_ur,L0_as,L0_es,L0_is
     &,L0_os,L_us,L_at,L_et,L_it,L_ot,L_ut,L_av,L_ev,L_iv
     &,L_ov,L_uv,L_ax,L_ex,L_ix
      LOGICAL*1 L_ox,L_ux,L0_abe,L0_ebe,L_ibe,L_obe,L0_ube
     &,L0_ade,L_ede,L_ide,L_ode,L0_ude,L0_afe,L0_efe
      INTEGER*4 I_ife,I0_ofe,I0_ufe,I0_ake
      REAL*4 R_eke,R0_ike,R_oke
      LOGICAL*1 L_uke,L0_ale,L0_ele,L_ile,L_ole
      REAL*4 R0_ule
      LOGICAL*1 L0_ame
      REAL*4 R0_eme
      REAL*8 R8_ime
      LOGICAL*1 L_ome
      REAL*8 R8_ume
      LOGICAL*1 L0_ape
      REAL*4 R0_epe,R0_ipe,R0_ope,R0_upe
      LOGICAL*1 L0_are
      REAL*4 R_ere,R0_ire,R0_ore,R_ure,R0_ase,R0_ese,R0_ise
     &,R_ose
      REAL*8 R8_use
      REAL*4 R0_ate,R_ete
      REAL*8 R8_ite
      LOGICAL*1 L0_ote
      REAL*4 R0_ute,R_ave
      REAL*8 R8_eve
      REAL*4 R0_ive,R_ove,R_uve
      LOGICAL*1 L0_axe
      REAL*4 R0_exe,R0_ixe,R0_oxe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ire=R_ere
C PUMP_HANDLER.fmg( 310, 202):pre: �������������� �����  ,5
      R0_ute=R_ave
C PUMP_HANDLER.fmg( 248, 249):pre: �������������� �����  ,1
      R0_ike=R_oke
C PUMP_HANDLER.fmg( 298, 176):pre: �������� ��������� ������,1
      C30_u = ''
C PUMP_HANDLER.fmg( 331, 268):��������� ���������� CH20 (CH30) (�������)
      C30_o = '�������'
C PUMP_HANDLER.fmg( 331, 266):��������� ���������� CH20 (CH30) (�������)
      C30_e = '��������'
C PUMP_HANDLER.fmg( 349, 265):��������� ���������� CH20 (CH30) (�������)
      I0_id = z'01000010'
C PUMP_HANDLER.fmg( 189, 204):��������� ������������� IN (�������)
      I0_od = z'01000003'
C PUMP_HANDLER.fmg( 189, 206):��������� ������������� IN (�������)
      I0_af = z'01000010'
C PUMP_HANDLER.fmg( 219, 164):��������� ������������� IN (�������)
      I0_ek = z'01000022'
C PUMP_HANDLER.fmg( 181, 166):��������� ������������� IN (�������)
      I0_if = z'0100000A'
C PUMP_HANDLER.fmg( 203, 165):��������� ������������� IN (�������)
      I0_up = z'01000097'
C PUMP_HANDLER.fmg( 178, 269):��������� ������������� IN (�������)
      I0_el = z'01000087'
C PUMP_HANDLER.fmg( 144, 271):��������� ������������� IN (�������)
      I0_ok = z'01000086'
C PUMP_HANDLER.fmg( 160, 270):��������� ������������� IN (�������)
      I0_ufe = z'0100000A'
C PUMP_HANDLER.fmg( 128, 272):��������� ������������� IN (�������)
      I0_ake = z'01000010'
C PUMP_HANDLER.fmg( 128, 274):��������� ������������� IN (�������)
      I0_ol = z'01000010'
C PUMP_HANDLER.fmg( 189, 214):��������� ������������� IN (�������)
      I0_ul = z'01000003'
C PUMP_HANDLER.fmg( 189, 216):��������� ������������� IN (�������)
      I0_im = z'01000003'
C PUMP_HANDLER.fmg( 201, 234):��������� ������������� IN (�������)
      I0_em = z'0100000A'
C PUMP_HANDLER.fmg( 201, 232):��������� ������������� IN (�������)
      L_ax=R_um.ne.R_om
      R_om=R_um
C PUMP_HANDLER.fmg(  70, 219):���������� ������������� ������
      L0_abe = L_ax.AND.(.NOT.L_uv)
C PUMP_HANDLER.fmg( 130, 218):�
      L0_as = L_ut.AND.L_ax
C PUMP_HANDLER.fmg( 132, 167):�
      L0_or = (.NOT.L_av).OR.L_ax
C PUMP_HANDLER.fmg( 138, 170):���
      L_ix=R_ep.ne.R_ap
      R_ap=R_ep
C PUMP_HANDLER.fmg(  70, 246):���������� ������������� ������
      L0_ube = L_ix.AND.(.NOT.L_ex)
C PUMP_HANDLER.fmg( 130, 245):�
      L0_es = L_av.AND.L_ix
C PUMP_HANDLER.fmg( 132, 174):�
      L_it=(L0_es.or.L_it).and..not.(L0_or)
      L0_ur=.not.L_it
C PUMP_HANDLER.fmg( 144, 172):RS �������,155
      L0_er = L_ix.OR.(.NOT.L_ut)
C PUMP_HANDLER.fmg( 137, 163):���
      L_et=(L0_as.or.L_et).and..not.(L0_er)
      L0_ir=.not.L_et
C PUMP_HANDLER.fmg( 144, 165):RS �������,156
      L_ot =.NOT.(L_iv.OR.L_ev.OR.L_it.OR.L_et)
C PUMP_HANDLER.fmg( 155, 186):���
      L_at =.NOT.(L_ot)
C PUMP_HANDLER.fmg( 164, 181):���
      I0_ip = z'0100000A'
C PUMP_HANDLER.fmg( 181, 168):��������� ������������� IN (�������)
      if(L_uf) then
         I0_ak=I0_ek
      else
         I0_ak=I0_ip
      endif
C PUMP_HANDLER.fmg( 188, 166):���� RE IN LO CH7
      if(L_of) then
         I0_ef=I0_if
      else
         I0_ef=I0_ak
      endif
C PUMP_HANDLER.fmg( 206, 165):���� RE IN LO CH7
      if(L_ud) then
         I_op=I0_af
      else
         I_op=I0_ef
      endif
C PUMP_HANDLER.fmg( 222, 164):���� RE IN LO CH7
      L_ox=(L_ev.or.L_ox).and..not.(.NOT.L_ev)
      L0_is=.not.L_ox
C PUMP_HANDLER.fmg( 123, 198):RS �������
      L0_ebe = L0_abe.OR.L_ede.OR.L_ux.OR.L_ox
C PUMP_HANDLER.fmg( 137, 215):���
      L0_afe = L0_ebe.AND.(.NOT.L_ut)
C PUMP_HANDLER.fmg( 142, 214):�
      L_ibe=(L_iv.or.L_ibe).and..not.(.NOT.L_iv)
      L0_os=.not.L_ibe
C PUMP_HANDLER.fmg( 121, 225):RS �������
      L0_ade = L0_ube.OR.L_ide.OR.L_obe.OR.L_ibe
C PUMP_HANDLER.fmg( 137, 242):���
      L0_ude = L0_ade.AND.(.NOT.L_av)
C PUMP_HANDLER.fmg( 142, 241):�
      L_ole=(L0_ude.or.L_ole).and..not.(L0_afe)
      L0_efe=.not.L_ole
C PUMP_HANDLER.fmg( 148, 239):RS �������,3
      L_ode=L_ole
C PUMP_HANDLER.fmg( 176, 241):������,lstate
      L_ov=L_ole
C PUMP_HANDLER.fmg( 178, 235):������,cb_out
      if(L_ov) then
         C30_i=C30_o
      else
         C30_i=C30_u
      endif
C PUMP_HANDLER.fmg( 335, 266):���� RE IN LO CH20
      if(.NOT.L_ov) then
         C30_ad=C30_e
      else
         C30_ad=C30_i
      endif
C PUMP_HANDLER.fmg( 353, 265):���� RE IN LO CH20
      if(L_ole) then
         I_am=I0_em
      else
         I_am=I0_im
      endif
C PUMP_HANDLER.fmg( 204, 232):���� RE IN LO CH7
      if(.NOT.L_ole) then
         I_il=I0_ol
      else
         I_il=I0_ul
      endif
C PUMP_HANDLER.fmg( 192, 214):���� RE IN LO CH7
      if(.NOT.L_ole) then
         I_ed=I0_id
      else
         I_ed=I0_od
      endif
C PUMP_HANDLER.fmg( 192, 204):���� RE IN LO CH7
      L0_uk = L_ide.AND.L_ole
C PUMP_HANDLER.fmg( 136, 264):�
      L0_ik = L_ede.AND.(.NOT.L_ole)
C PUMP_HANDLER.fmg( 136, 258):�
      if(L_ole) then
         I0_ofe=I0_ufe
      else
         I0_ofe=I0_ake
      endif
C PUMP_HANDLER.fmg( 131, 272):���� RE IN LO CH7
      if(L0_uk) then
         I0_al=I0_el
      else
         I0_al=I0_ofe
      endif
C PUMP_HANDLER.fmg( 147, 271):���� RE IN LO CH7
      if(L0_ik) then
         I0_ar=I0_ok
      else
         I0_ar=I0_al
      endif
C PUMP_HANDLER.fmg( 163, 270):���� RE IN LO CH7
      if(L_at) then
         I_ife=I0_up
      else
         I_ife=I0_ar
      endif
C PUMP_HANDLER.fmg( 181, 269):���� RE IN LO CH7
      L_us = L_ex.OR.L_uv
C PUMP_HANDLER.fmg( 152, 207):���
      R0_ule = 1000
C PUMP_HANDLER.fmg( 305, 183):��������� (RE4) (�������)
      R0_ase = R0_ule * R8_ime
C PUMP_HANDLER.fmg( 308, 183):����������
      R0_eme = 0.1
C PUMP_HANDLER.fmg( 276, 174):��������� (RE4) (�������)
      L0_ame=R8_ime.lt.R0_eme
C PUMP_HANDLER.fmg( 280, 176):���������� <
      L0_axe = L_ole.AND.(.NOT.L0_ame)
C PUMP_HANDLER.fmg( 219, 219):�
      if(L0_axe) then
         R0_ive=R_ove
      else
         R0_ive=R_uve
      endif
C PUMP_HANDLER.fmg( 224, 260):���� RE IN LO CH7
      L0_ape = (.NOT.L_ome).AND.L0_axe
C PUMP_HANDLER.fmg( 324, 195):�
      if(.not.L0_ame) then
         R_oke=0.0
      elseif(.not.L_uke) then
         R_oke=R_eke
      else
         R_oke=max(R0_ike-deltat,0.0)
      endif
      L0_ale=L0_ame.and.R_oke.le.0.0
      L_uke=L0_ame
C PUMP_HANDLER.fmg( 298, 176):�������� ��������� ������,1
      L0_ele = L0_ale.AND.(.NOT.L_ile)
C PUMP_HANDLER.fmg( 325, 175):�
      R0_ope = R8_use * R_ure
C PUMP_HANDLER.fmg( 274, 194):����������
      R0_upe = R0_ope + (-R8_use)
C PUMP_HANDLER.fmg( 280, 201):��������
      R0_epe = 0
C PUMP_HANDLER.fmg( 321, 206):��������� (RE4) (�������)
      L0_are=.false.
C PUMP_HANDLER.fmg( 304, 197):��������� ���������� (�������)
      R0_ese = 1.73
C PUMP_HANDLER.fmg( 323, 184):��������� (RE4) (�������)
      R0_ise = R0_ese * R0_ase
C PUMP_HANDLER.fmg( 326, 184):����������
      L0_ote=.false.
C PUMP_HANDLER.fmg( 242, 244):��������� ���������� (�������)
      R0_oxe = 0
C PUMP_HANDLER.fmg( 216, 249):��������� (RE4) (�������)
      R0_ixe = 1
C PUMP_HANDLER.fmg( 216, 247):��������� (RE4) (�������)
      if(L0_axe) then
         R0_exe=R0_ixe
      else
         R0_exe=R0_oxe
      endif
C PUMP_HANDLER.fmg( 219, 248):���� RE IN LO CH7
      if(L0_ote) then
         R_ave=R0_exe
      else
         R_ave=(R0_ive*R0_ute+deltat*R0_exe)/(R0_ive+deltat
     &)
      endif
C PUMP_HANDLER.fmg( 248, 249):�������������� �����  ,1
      if(L_ome) then
         R8_eve=R8_ume
      else
         R8_eve=R_ave
      endif
C PUMP_HANDLER.fmg( 340, 247):���� RE IN LO CH7
      R0_ore = R_ave * R0_upe
C PUMP_HANDLER.fmg( 286, 202):����������
      if(L0_are) then
         R_ere=R0_ore
      else
         R_ere=(R0_ive*R0_ire+deltat*R0_ore)/(R0_ive+deltat
     &)
      endif
C PUMP_HANDLER.fmg( 310, 202):�������������� �����  ,5
      R0_ipe = R0_ope + (-R_ere)
C PUMP_HANDLER.fmg( 318, 201):��������
      if(L0_ape) then
         R_ete=R0_ipe
      else
         R_ete=R0_epe
      endif
C PUMP_HANDLER.fmg( 324, 201):���� RE IN LO CH7
      if(R0_ise.ge.0.0) then
         R_ose=R_ete/max(R0_ise,1.0e-10)
      else
         R_ose=R_ete/min(R0_ise,-1.0e-10)
      endif
C PUMP_HANDLER.fmg( 353, 186):�������� ����������
      R0_ate = R8_ite
C PUMP_HANDLER.fmg( 343, 215):��������
C label 187  try187=try187-1
      R8_ite = R_ete + R0_ate
C PUMP_HANDLER.fmg( 349, 214):��������
C sav1=R0_ate
      R0_ate = R8_ite
C PUMP_HANDLER.fmg( 343, 215):recalc:��������
C if(sav1.ne.R0_ate .and. try187.gt.0) goto 187
      End
