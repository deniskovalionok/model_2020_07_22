      Subroutine ZATVOR_MAN(ext_deltat,R_ali,R8_ote,I_e,I_u
     &,I_id,C8_ik,I_ok,R_il,R_ul,R_am,R_em,R_im,R_om,R_um
     &,R_ap,I_ep,I_up,I_ir,I_as,L_is,L_us,L_at,L_et,L_ot,L_ut
     &,L_ov,L_uv,L_ax,L_ox,L_ux,L_ebe,L_obe,R8_ade,R_ude,R8_ife
     &,L_ofe,L_ufe,L_eke,L_ike,L_ule,I_ame,L_ate,R_ave,R_abi
     &,L_idi,L_udi,L_aki,L_oki,L_eli,L_ili,L_oli,L_uli,L_ami
     &,L_emi)
C |R_ali         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ote        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_e           |2 4 O|LOPO            |����� �����������||
C |I_u           |2 4 O|LCLC            |����� �����������||
C |I_id          |2 4 O|LREADY          |����� ����������||
C |C8_ik         |3 8 O|TEXT            |�������� ���������||
C |I_ok          |2 4 O|LF              |����� �������������||
C |R_il          |4 4 O|POS_CL          |��������||
C |R_ul          |4 4 O|POS_OP          |��������||
C |R_am          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_em          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_im          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_om          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_um          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_ep          |2 4 O|LST             |����� ����||
C |I_up          |2 4 O|LCL             |����� �������||
C |I_ir          |2 4 O|LOP             |����� �������||
C |I_as          |2 4 O|LZM             |������ "�������"||
C |L_is          |1 1 I|vlv_kvit        |||
C |L_us          |1 1 I|instr_fault     |||
C |L_at          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_et          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ot          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ut          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ov          |1 1 O|block           |||
C |L_uv          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ax          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ox          |1 1 O|STOP            |�������||
C |L_ux          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 O|norm            |�����||
C |L_obe         |1 1 O|nopower         |��� ����������||
C |R8_ade        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ude         |4 4 I|power           |�������� ��������||
C |R8_ife        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ofe         |1 1 I|YA27            |������� ������ �������|F|
C |L_ufe         |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_eke         |1 1 I|YA28            |������� ������ �������|F|
C |L_ike         |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_ule         |1 1 O|fault           |�������������||
C |I_ame         |2 4 O|LAM             |������ "�������"||
C |L_ate         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ave         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_abi         |4 4 I|tcl_top         |����� ����|30.0|
C |L_idi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_aki         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 O|XH51            |�� ������� (���)|F|
C |L_eli         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ili         |1 1 I|mlf23           |������� ������� �����||
C |L_oli         |1 1 I|mlf22           |����� ����� ��������||
C |L_uli         |1 1 I|mlf04           |�������� �� ��������||
C |L_ami         |1 1 I|mlf03           |�������� �� ��������||
C |L_emi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I_e,I0_i,I0_o,I_u,I0_ad,I0_ed,I_id,I0_od,I0_ud
      LOGICAL*1 L0_af
      CHARACTER*8 C8_ef,C8_if,C8_of,C8_uf,C8_ak,C8_ek,C8_ik
      INTEGER*4 I_ok,I0_uk,I0_al
      REAL*4 R0_el,R_il,R0_ol,R_ul,R_am,R_em,R_im,R_om,R_um
     &,R_ap
      INTEGER*4 I_ep,I0_ip,I0_op,I_up,I0_ar,I0_er,I_ir,I0_or
     &,I0_ur,I_as,I0_es
      LOGICAL*1 L_is,L0_os,L_us,L_at,L_et
      REAL*4 R0_it
      LOGICAL*1 L_ot,L_ut,L0_av,L0_ev,L0_iv,L_ov,L_uv,L_ax
     &,L0_ex,L0_ix,L_ox,L_ux,L0_abe,L_ebe,L0_ibe,L_obe
      REAL*4 R0_ube
      REAL*8 R8_ade
      LOGICAL*1 L0_ede,L0_ide
      REAL*4 R0_ode,R_ude,R0_afe,R0_efe
      REAL*8 R8_ife
      LOGICAL*1 L_ofe,L_ufe,L0_ake,L_eke,L_ike,L0_oke
      INTEGER*4 I0_uke,I0_ale,I0_ele,I0_ile,I0_ole
      LOGICAL*1 L_ule
      INTEGER*4 I_ame,I0_eme,I0_ime
      LOGICAL*1 L0_ome,L0_ume
      REAL*4 R0_ape,R0_epe
      LOGICAL*1 L0_ipe
      REAL*4 R0_ope,R0_upe,R0_are,R0_ere,R0_ire,R0_ore
      LOGICAL*1 L0_ure
      REAL*4 R0_ase,R0_ese,R0_ise,R0_ose
      LOGICAL*1 L0_use,L_ate
      REAL*4 R0_ete,R0_ite
      REAL*8 R8_ote
      REAL*4 R0_ute,R_ave,R0_eve,R0_ive,R0_ove,R0_uve,R0_axe
     &,R0_exe,R0_ixe,R0_oxe,R0_uxe,R_abi
      LOGICAL*1 L0_ebi,L0_ibi,L0_obi,L0_ubi,L0_adi,L0_edi
     &,L_idi,L0_odi,L_udi,L0_afi,L0_efi,L0_ifi,L0_ofi,L0_ufi
     &,L_aki,L0_eki,L0_iki,L_oki,L0_uki
      REAL*4 R_ali
      LOGICAL*1 L_eli,L_ili,L_oli,L_uli,L_ami,L_emi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_o = z'01000003'
C ZATVOR_MAN.fmg( 170, 230):��������� ������������� IN (�������)
      I0_i = z'0100000A'
C ZATVOR_MAN.fmg( 170, 228):��������� ������������� IN (�������)
      I0_ed = z'01000003'
C ZATVOR_MAN.fmg( 192, 181):��������� ������������� IN (�������)
      I0_ad = z'0100000A'
C ZATVOR_MAN.fmg( 192, 179):��������� ������������� IN (�������)
      I0_er = z'01000003'
C ZATVOR_MAN.fmg( 114, 171):��������� ������������� IN (�������)
      I0_ar = z'0100000A'
C ZATVOR_MAN.fmg( 114, 169):��������� ������������� IN (�������)
      I0_ud = z'01000003'
C ZATVOR_MAN.fmg( 328, 204):��������� ������������� IN (�������)
      I0_od = z'0100000A'
C ZATVOR_MAN.fmg( 328, 202):��������� ������������� IN (�������)
      C8_ef = '������'
C ZATVOR_MAN.fmg( 234, 280):��������� ���������� CH8 (�������)
      C8_of = '������'
C ZATVOR_MAN.fmg( 210, 281):��������� ���������� CH8 (�������)
      C8_ak = '����'
C ZATVOR_MAN.fmg( 194, 282):��������� ���������� CH8 (�������)
      C8_ek = '����'
C ZATVOR_MAN.fmg( 194, 284):��������� ���������� CH8 (�������)
      I0_uke = z'0100000A'
C ZATVOR_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_ale = z'01000003'
C ZATVOR_MAN.fmg( 152, 209):��������� ������������� IN (�������)
      I0_es = z'0100000E'
C ZATVOR_MAN.fmg( 188, 201):��������� ������������� IN (�������)
      I0_uk = z'01000007'
C ZATVOR_MAN.fmg( 148, 180):��������� ������������� IN (�������)
      I0_al = z'01000003'
C ZATVOR_MAN.fmg( 148, 182):��������� ������������� IN (�������)
      R0_el = 100
C ZATVOR_MAN.fmg( 378, 273):��������� (RE4) (�������)
      R0_ol = 100
C ZATVOR_MAN.fmg( 365, 267):��������� (RE4) (�������)
      L_ax=R_em.ne.R_am
      R_am=R_em
C ZATVOR_MAN.fmg(  22, 208):���������� ������������� ������
      L_et=R_om.ne.R_im
      R_im=R_om
C ZATVOR_MAN.fmg(  21, 194):���������� ������������� ������
      L0_ake = (.NOT.L_ot).AND.L_et
C ZATVOR_MAN.fmg(  65, 195):�
      L0_afi = L0_ake.OR.L_ufe.OR.L_ofe
C ZATVOR_MAN.fmg(  69, 192):���
      L_uv=R_ap.ne.R_um
      R_um=R_ap
C ZATVOR_MAN.fmg(  23, 236):���������� ������������� ������
      L0_oke = L_uv.AND.(.NOT.L_ut)
C ZATVOR_MAN.fmg(  65, 237):�
      L0_iki = L0_oke.OR.L_ike.OR.L_eke
C ZATVOR_MAN.fmg(  69, 234):���
      L0_ex = L0_iki.OR.L0_afi
C ZATVOR_MAN.fmg(  74, 203):���
      L_ux=(L_ax.or.L_ux).and..not.(L0_ex)
      L0_ix=.not.L_ux
C ZATVOR_MAN.fmg( 104, 205):RS �������,10
      L_ox=L_ux
C ZATVOR_MAN.fmg( 132, 207):������,STOP
      L0_obi = L_ux.OR.L_eli
C ZATVOR_MAN.fmg(  96, 214):���
      I0_ip = z'01000007'
C ZATVOR_MAN.fmg( 112, 214):��������� ������������� IN (�������)
      I0_op = z'01000003'
C ZATVOR_MAN.fmg( 112, 216):��������� ������������� IN (�������)
      if(L_ux) then
         I_ep=I0_ip
      else
         I_ep=I0_op
      endif
C ZATVOR_MAN.fmg( 116, 214):���� RE IN LO CH7
      I0_ur = z'01000003'
C ZATVOR_MAN.fmg( 120, 265):��������� ������������� IN (�������)
      I0_or = z'0100000A'
C ZATVOR_MAN.fmg( 120, 263):��������� ������������� IN (�������)
      I0_ole = z'0100000A'
C ZATVOR_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ile = z'01000003'
C ZATVOR_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_eme = z'0100000E'
C ZATVOR_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      L_at=(L_us.or.L_at).and..not.(L_is)
      L0_os=.not.L_at
C ZATVOR_MAN.fmg( 326, 178):RS �������
      L0_iv=.false.
C ZATVOR_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_ev=.false.
C ZATVOR_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_av=.false.
C ZATVOR_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_ov = L0_iv.OR.L0_ev.OR.L0_av.OR.L_ut.OR.L_ot
C ZATVOR_MAN.fmg(  67, 213):���
      R0_it = DeltaT
C ZATVOR_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_abi.ge.0.0) then
         R0_uve=R0_it/max(R_abi,1.0e-10)
      else
         R0_uve=R0_it/min(R_abi,-1.0e-10)
      endif
C ZATVOR_MAN.fmg( 259, 252):�������� ����������
      L0_ibe =.NOT.(L_ami.OR.L_uli.OR.L_emi.OR.L_oli.OR.L_ili.OR.L_eli
     &)
C ZATVOR_MAN.fmg( 319, 191):���
      L0_abe =.NOT.(L0_ibe)
C ZATVOR_MAN.fmg( 328, 185):���
      L_ule = L0_abe.OR.L_at
C ZATVOR_MAN.fmg( 332, 184):���
      if(L_ule) then
         I_ok=I0_uk
      else
         I_ok=I0_al
      endif
C ZATVOR_MAN.fmg( 152, 181):���� RE IN LO CH7
      L_ebe = L0_ibe.OR.L_us
C ZATVOR_MAN.fmg( 328, 190):���
      if(L_ebe) then
         I_id=I0_od
      else
         I_id=I0_ud
      endif
C ZATVOR_MAN.fmg( 331, 202):���� RE IN LO CH7
      R0_ube = 0.1
C ZATVOR_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_obe=R8_ade.lt.R0_ube
C ZATVOR_MAN.fmg( 259, 162):���������� <
      R0_ode = 0.0
C ZATVOR_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_ire = 0.000001
C ZATVOR_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_ore = 0.999999
C ZATVOR_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_ape = 0.0
C ZATVOR_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_epe = 0.0
C ZATVOR_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_ipe = L_ili.OR.L_oli
C ZATVOR_MAN.fmg( 363, 237):���
      R0_upe = 1.0
C ZATVOR_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_are = 0.0
C ZATVOR_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ese = 1.0e-10
C ZATVOR_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_ete = 0.0
C ZATVOR_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_ive = DeltaT
C ZATVOR_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_abi.ge.0.0) then
         R0_axe=R0_ive/max(R_abi,1.0e-10)
      else
         R0_axe=R0_ive/min(R_abi,-1.0e-10)
      endif
C ZATVOR_MAN.fmg( 259, 262):�������� ����������
      R0_uxe = 0.0
C ZATVOR_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_ubi = (.NOT.L_ox).AND.L_udi
C ZATVOR_MAN.fmg( 102, 166):�
C label 141  try141=try141-1
      if(L_oli) then
         R0_ope=R0_epe
      else
         R0_ope=R8_ote
      endif
C ZATVOR_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ipe) then
         R_ave=R8_ote
      endif
C ZATVOR_MAN.fmg( 384, 252):���� � ������������� �������
      L0_ifi = (.NOT.L_ox).AND.L_oki
C ZATVOR_MAN.fmg( 104, 260):�
      L0_ufi = L_oki.OR.L0_obi.OR.L0_afi
C ZATVOR_MAN.fmg( 102, 229):���
      L0_uki = (.NOT.L_oki).AND.L0_iki
C ZATVOR_MAN.fmg( 102, 235):�
      L_aki=(L0_uki.or.L_aki).and..not.(L0_ufi)
      L0_eki=.not.L_aki
C ZATVOR_MAN.fmg( 110, 233):RS �������,1
      L0_ofi = (.NOT.L0_ifi).AND.L_aki
C ZATVOR_MAN.fmg( 128, 236):�
      L0_ume = L0_ofi.AND.(.NOT.L_uli)
C ZATVOR_MAN.fmg( 252, 242):�
      L0_ibi = L0_ume.OR.L_ami
C ZATVOR_MAN.fmg( 265, 241):���
      if(L0_ibi) then
         R0_ixe=R0_axe
      else
         R0_ixe=R0_uxe
      endif
C ZATVOR_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_edi = L0_obi.OR.L_udi.OR.L0_iki
C ZATVOR_MAN.fmg( 102, 185):���
      L0_efi = L0_afi.AND.(.NOT.L_udi)
C ZATVOR_MAN.fmg( 102, 191):�
      L_idi=(L0_efi.or.L_idi).and..not.(L0_edi)
      L0_odi=.not.L_idi
C ZATVOR_MAN.fmg( 110, 189):RS �������,2
      L0_adi = L_idi.AND.(.NOT.L0_ubi)
C ZATVOR_MAN.fmg( 128, 190):�
      L0_ome = L0_adi.AND.(.NOT.L_ami)
C ZATVOR_MAN.fmg( 252, 228):�
      L0_ebi = L0_ome.OR.L_uli
C ZATVOR_MAN.fmg( 265, 227):���
      if(L0_ebi) then
         R0_exe=R0_uve
      else
         R0_exe=R0_uxe
      endif
C ZATVOR_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_oxe = R0_ixe + (-R0_exe)
C ZATVOR_MAN.fmg( 293, 246):��������
      if(L_eli) then
         R0_ite=R0_ape
      else
         R0_ite=R0_oxe
      endif
C ZATVOR_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_ose = R_ave + (-R_ali)
C ZATVOR_MAN.fmg( 308, 235):��������
      R0_ase = R0_ite + R0_ose
C ZATVOR_MAN.fmg( 314, 236):��������
      R0_ise = R0_ase * R0_ose
C ZATVOR_MAN.fmg( 318, 235):����������
      L0_use=R0_ise.lt.R0_ese
C ZATVOR_MAN.fmg( 323, 234):���������� <
      L_ate=(L0_use.or.L_ate).and..not.(.NOT.L_emi)
      L0_ure=.not.L_ate
C ZATVOR_MAN.fmg( 330, 232):RS �������,6
      if(L_ate) then
         R_ave=R_ali
      endif
C ZATVOR_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ate) then
         R0_ute=R0_ete
      else
         R0_ute=R0_ite
      endif
C ZATVOR_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_eve = R_ave + R0_ute
C ZATVOR_MAN.fmg( 338, 245):��������
      R0_ere=MAX(R0_are,R0_eve)
C ZATVOR_MAN.fmg( 346, 246):��������
      R0_ove=MIN(R0_upe,R0_ere)
C ZATVOR_MAN.fmg( 354, 247):�������
      L_udi=R0_ove.lt.R0_ire
C ZATVOR_MAN.fmg( 363, 210):���������� <
C sav1=L0_edi
      L0_edi = L0_obi.OR.L_udi.OR.L0_iki
C ZATVOR_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_edi .and. try181.gt.0) goto 181
C sav1=L0_efi
      L0_efi = L0_afi.AND.(.NOT.L_udi)
C ZATVOR_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_efi .and. try183.gt.0) goto 183
C sav1=L0_ubi
      L0_ubi = (.NOT.L_ox).AND.L_udi
C ZATVOR_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_ubi .and. try141.gt.0) goto 141
      if(L0_ipe) then
         R8_ote=R0_ope
      else
         R8_ote=R0_ove
      endif
C ZATVOR_MAN.fmg( 377, 246):���� RE IN LO CH7
      R_ul = R0_ol * R8_ote
C ZATVOR_MAN.fmg( 368, 266):����������
      R_il = R0_el + (-R_ul)
C ZATVOR_MAN.fmg( 382, 272):��������
      L_oki=R0_ove.gt.R0_ore
C ZATVOR_MAN.fmg( 363, 225):���������� >
C sav1=L0_ufi
      L0_ufi = L_oki.OR.L0_obi.OR.L0_afi
C ZATVOR_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_ufi .and. try161.gt.0) goto 161
C sav1=L0_uki
      L0_uki = (.NOT.L_oki).AND.L0_iki
C ZATVOR_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_uki .and. try163.gt.0) goto 163
C sav1=L0_ifi
      L0_ifi = (.NOT.L_ox).AND.L_oki
C ZATVOR_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_ifi .and. try156.gt.0) goto 156
      if(L_oki) then
         C8_uf=C8_ak
      else
         C8_uf=C8_ek
      endif
C ZATVOR_MAN.fmg( 198, 282):���� RE IN LO CH7
      if(L_ule) then
         C8_if=C8_of
      else
         C8_if=C8_uf
      endif
C ZATVOR_MAN.fmg( 214, 282):���� RE IN LO CH7
      L0_af = (.NOT.L_oki).AND.(.NOT.L_udi)
C ZATVOR_MAN.fmg( 236, 274):�
      if(L0_af) then
         C8_ik=C8_ef
      else
         C8_ik=C8_if
      endif
C ZATVOR_MAN.fmg( 238, 280):���� RE IN LO CH7
      if(L0_ipe) then
         R_ave=R0_ove
      endif
C ZATVOR_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_ose
      R0_ose = R_ave + (-R_ali)
C ZATVOR_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_ose .and. try208.gt.0) goto 208
C sav1=R0_eve
      R0_eve = R_ave + R0_ute
C ZATVOR_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_eve .and. try225.gt.0) goto 225
      if(L0_adi) then
         I_u=I0_ad
      else
         I_u=I0_ed
      endif
C ZATVOR_MAN.fmg( 196, 180):���� RE IN LO CH7
      L0_ede = L0_ofi.OR.L0_adi
C ZATVOR_MAN.fmg( 251, 174):���
      L0_ide = L0_ede.AND.(.NOT.L_obe)
C ZATVOR_MAN.fmg( 266, 173):�
      if(L0_ide) then
         R0_efe=R_ude
      else
         R0_efe=R0_ode
      endif
C ZATVOR_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_ofi) then
         I_e=I0_i
      else
         I_e=I0_o
      endif
C ZATVOR_MAN.fmg( 173, 228):���� RE IN LO CH7
      if(L0_ifi) then
         I0_ele=I0_uke
      else
         I0_ele=I0_ale
      endif
C ZATVOR_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L_ule) then
         I_as=I0_es
      else
         I_as=I0_ele
      endif
C ZATVOR_MAN.fmg( 191, 206):���� RE IN LO CH7
      if(L0_ifi) then
         I_ir=I0_or
      else
         I_ir=I0_ur
      endif
C ZATVOR_MAN.fmg( 124, 264):���� RE IN LO CH7
      if(L0_ubi) then
         I_up=I0_ar
      else
         I_up=I0_er
      endif
C ZATVOR_MAN.fmg( 117, 170):���� RE IN LO CH7
      if(L0_ubi) then
         I0_ime=I0_ile
      else
         I0_ime=I0_ole
      endif
C ZATVOR_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L_ule) then
         I_ame=I0_eme
      else
         I_ame=I0_ime
      endif
C ZATVOR_MAN.fmg( 170, 262):���� RE IN LO CH7
      R0_afe = R8_ife
C ZATVOR_MAN.fmg( 264, 190):��������
C label 311  try311=try311-1
      R8_ife = R0_efe + R0_afe
C ZATVOR_MAN.fmg( 275, 189):��������
C sav1=R0_afe
      R0_afe = R8_ife
C ZATVOR_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_afe .and. try311.gt.0) goto 311
      End
