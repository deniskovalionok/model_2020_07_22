      Subroutine TELEGKA5_HANDLER(ext_deltat,R_epi,R_e,R_i
     &,I_o,L_u,L_ad,L_ed,L_id,I_af,R_ef,R_il,R_ol,R_ul,R_am
     &,L_os,R_ut,R_ode,L_ude,R_ife,L_ofe,L_uke,L_ile,R_ipe
     &,R_ire,L_ure,R_ise,L_use,L_ate,L_ete,L_axe,L_ixe,L_oxe
     &,L_ebi,L_obi,L_odi,L_afi,L_efi,L_ifi,L_ufi,L_iki,L_ali
     &,L_ili,R_ipi,L_ari,L_iri,L_uri,C20_usi,L_ati,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi,L_axi,L_exi
     &,L_ixi,L_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_odo,I_uko
     &,I_alo,I_elo,I_amo,I_emo,I_imo,I_epo,I_ipo,I_opo,I_iro
     &,I_oro,I_uro,I_aso,I_uso,I_ato,I_uto,I_ivo,R_ovo,R_uvo
     &,R_axo,R_exo,I_ixo,I_abu,I_adu,I_odu,C20_ofu,C8_oku
     &,R_epu,R_ipu,L_opu,R_upu,R_aru,I_eru,L_uru,L_asu,L_esu
     &,I_osu,I_etu,I_utu,L_ovu,L_axu,L_ixu,L_uxu,L_abad,L_ebad
     &,L_idad,L_afad,L_ifad,L_ofad,R8_akad,R_ukad,R8_ilad
     &,L_olad,L_ulad,L_emad,L_imad,L_umad,I_apad,L_opad,L_upad
     &,L_urad,L_itad,L_ivad)
C |R_epi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 K|_lcmpJ4041      |[]�������� ������ �����������|0.001|
C |R_i           |4 4 K|_lcmpJ4040      |[]�������� ������ �����������|15299|
C |I_o           |2 4 O|NUM_BOAT        |�������� ������� �������||
C |L_u           |1 1 S|_splsJ4028*     |[TF]���������� ��������� ������������� |F|
C |L_ad          |1 1 S|_splsJ4025*     |[TF]���������� ��������� ������������� |F|
C |L_ed          |1 1 I|RESET_BOAT      |�������� ������� �������||
C |L_id          |1 1 I|PREPARE_BOAT    |����������� ������� � �������� �������||
C |I_af          |2 4 S|_COUNTER_OUTCOUNTER*|||
C |R_ef          |4 4 S|_slpbJ3978*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3934*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3933*     |���������� ��������� ||
C |R_ul          |4 4 S|_slpbJ3932*     |���������� ��������� ||
C |R_am          |4 4 S|_slpbJ3926*     |���������� ��������� ||
C |L_os          |1 1 S|_qffJ3882*      |�������� ������ Q RS-��������  |F|
C |R_ut          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ode         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ude         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ife         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ofe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_uke         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ile         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ipe         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ire         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_ure         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ise         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_use         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ate         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ete         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_axe         |1 1 I|vlv_kvit        |||
C |L_ixe         |1 1 I|instr_fault     |||
C |L_oxe         |1 1 S|_qffJ3417*      |�������� ������ Q RS-��������  |F|
C |L_ebi         |1 1 O|norm            |�����||
C |L_obi         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_odi         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_afi         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_efi         |1 1 O|flag_mlf19      |||
C |L_ifi         |1 1 I|OUTC            |�����||
C |L_ufi         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_iki         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ali         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_ili         |1 1 I|OUTO            |������||
C |R_ipi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ari         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_iri         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_uri         |1 1 O|limit_switch_error|||
C |C20_usi       |3 20 O|task_state      |���������||
C |L_ati         |1 1 I|mlf23           |������� ������� �����||
C |L_eti         |1 1 I|mlf22           |����� ����� ��������||
C |L_iti         |1 1 I|mlf19           |���� ������������||
C |L_oti         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_uti         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_avi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_evi         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_ivi         |1 1 I|mlf07           |���������� ���� �������||
C |L_ovi         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_uvi         |1 1 I|mlf09           |����� ��������� �����||
C |L_axi         |1 1 I|mlf08           |����� ��������� ������||
C |L_exi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_ixi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_oxi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |R_uxi         |4 4 K|_uintV_INT_     |����������� ������ ����������� ������|15300|
C |R_abo         |4 4 K|_tintV_INT_     |[���]�������� T �����������|1|
C |R_ebo         |4 4 O|_ointV_INT_*    |�������� ������ ����������� |15300|
C |R_ibo         |4 4 K|_lintV_INT_     |����������� ������ ����������� �����|0.0|
C |R_obo         |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_odo         |4 4 O|VX02            |�������� ����������� �������||
C |I_uko         |2 4 K|_lcmpJ2719      |�������� ������ �����������|0|
C |I_alo         |2 4 I|N5              |����� ������� � ������ 5||
C |I_elo         |2 4 O|LS5             |����� ������� ������� � ������ 5||
C |I_amo         |2 4 K|_lcmpJ2708      |�������� ������ �����������|0|
C |I_emo         |2 4 I|N4              |����� ������� � ������ 4||
C |I_imo         |2 4 O|LS4             |����� ������� ������� � ������ 4||
C |I_epo         |2 4 K|_lcmpJ2697      |�������� ������ �����������|0|
C |I_ipo         |2 4 I|N3              |����� ������� � ������ 3||
C |I_opo         |2 4 O|LS3             |����� ������� ������� � ������ 3||
C |I_iro         |2 4 K|_lcmpJ2682      |�������� ������ �����������|0|
C |I_oro         |2 4 I|N2              |����� ������� � ������ 2||
C |I_uro         |2 4 O|LS2             |����� ������� ������� � ������ 2||
C |I_aso         |2 4 O|LS1             |����� ������� ������� � ������ 1||
C |I_uso         |2 4 K|_lcmpJ2670      |�������� ������ �����������|0|
C |I_ato         |2 4 I|N1              |����� ������� � ������ 1||
C |I_uto         |2 4 O|LREADY          |����� ����������||
C |I_ivo         |2 4 O|LBUSY           |����� �����||
C |R_ovo         |4 4 S|vmpos1_button_ST*|��������� ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_uvo         |4 4 I|vmpos1_button   |������� ������ ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_axo         |4 4 S|vmpos2_button_ST*|��������� ������ "������� � ��������� 2 �� ���������" |0.0|
C |R_exo         |4 4 I|vmpos2_button   |������� ������ ������ "������� � ��������� 2 �� ���������" |0.0|
C |I_ixo         |2 4 O|state1          |��������� 1||
C |I_abu         |2 4 O|state2          |��������� 2||
C |I_adu         |2 4 O|LPOS2O          |����� � ��������� 2||
C |I_odu         |2 4 O|LPOS1C          |����� � ��������� 1||
C |C20_ofu       |3 20 O|task_name       |������� ���������||
C |C8_oku        |3 8 I|task            |������� ���������||
C |R_epu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ipu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_opu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_upu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_aru         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_eru         |2 4 O|LERROR          |����� �������������||
C |L_uru         |1 1 I|YA27            |������� ������� �� ����������|F|
C |L_asu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_esu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_osu         |2 4 O|LPOS1           |����� ��������� 1||
C |I_etu         |2 4 O|LPOS2           |����� ��������� 2||
C |I_utu         |2 4 O|LZM             |������ "�������"||
C |L_ovu         |1 1 O|vmpos1_button_CMD*|[TF]����� ������ ������� � ��������� 1 �� ���������|F|
C |L_axu         |1 1 O|vmpos2_button_CMD*|[TF]����� ������ ������� � ��������� 2 �� ���������|F|
C |L_ixu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_uxu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_abad        |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ebad        |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_idad        |1 1 O|block           |||
C |L_afad        |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ifad        |1 1 O|STOP            |�������||
C |L_ofad        |1 1 O|nopower         |��� ����������||
C |R8_akad       |4 8 I|voltage         |[��]���������� �� ������||
C |R_ukad        |4 4 I|power           |�������� ��������||
C |R8_ilad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_olad        |1 1 I|mlf04           |���������������� �������� �����||
C |L_ulad        |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_emad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_imad        |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_umad        |1 1 O|fault           |�������������||
C |I_apad        |2 4 O|LAM             |������ "�������"||
C |L_opad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_upad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_urad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_itad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i
      INTEGER*4 I_o
      LOGICAL*1 L_u,L_ad,L_ed,L_id,L_od,L_ud
      INTEGER*4 I_af
      REAL*4 R_ef,R0_if,R0_of,R0_uf,R0_ak
      LOGICAL*1 L0_ek,L0_ik,L0_ok,L0_uk
      REAL*4 R0_al,R0_el,R_il,R_ol,R_ul,R_am,R0_em,R0_im,R0_om
     &,R0_um,R0_ap,R0_ep,R0_ip,R0_op,R0_up,R0_ar
      LOGICAL*1 L0_er
      REAL*4 R0_ir,R0_or,R0_ur,R0_as,R0_es
      LOGICAL*1 L0_is,L_os
      REAL*4 R0_us,R0_at,R0_et,R0_it,R0_ot,R_ut,R0_av,R0_ev
     &,R0_iv,R0_ov,R0_uv
      LOGICAL*1 L0_ax,L0_ex
      REAL*4 R0_ix
      LOGICAL*1 L0_ox,L0_ux,L0_abe,L0_ebe,L0_ibe,L0_obe,L0_ube
     &,L0_ade
      REAL*4 R0_ede,R0_ide,R_ode
      LOGICAL*1 L_ude
      REAL*4 R0_afe,R0_efe,R_ife
      LOGICAL*1 L_ofe,L0_ufe,L0_ake,L0_eke,L0_ike,L0_oke,L_uke
     &,L0_ale,L0_ele,L_ile,L0_ole,L0_ule,L0_ame
      REAL*4 R0_eme,R0_ime
      LOGICAL*1 L0_ome,L0_ume
      REAL*4 R0_ape,R0_epe,R_ipe
      LOGICAL*1 L0_ope,L0_upe
      REAL*4 R0_are,R0_ere,R_ire
      LOGICAL*1 L0_ore,L_ure
      REAL*4 R0_ase,R0_ese,R_ise
      LOGICAL*1 L0_ose,L_use,L_ate,L_ete,L0_ite,L0_ote,L0_ute
     &,L0_ave,L0_eve,L0_ive,L0_ove,L0_uve,L_axe,L0_exe,L_ixe
     &,L_oxe,L0_uxe,L0_abi,L_ebi,L0_ibi,L_obi,L0_ubi
      LOGICAL*1 L0_adi,L0_edi,L0_idi,L_odi,L0_udi,L_afi,L_efi
     &,L_ifi,L0_ofi,L_ufi,L0_aki,L0_eki,L_iki,L0_oki,L0_uki
     &,L_ali,L0_eli,L_ili,L0_oli
      REAL*4 R0_uli,R0_ami,R0_emi
      LOGICAL*1 L0_imi
      REAL*4 R0_omi
      LOGICAL*1 L0_umi,L0_api
      REAL*4 R_epi,R_ipi
      LOGICAL*1 L0_opi,L0_upi,L_ari,L0_eri,L_iri,L0_ori,L_uri
      CHARACTER*20 C20_asi,C20_esi,C20_isi,C20_osi,C20_usi
      LOGICAL*1 L_ati,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi
     &,L_ivi,L_ovi,L_uvi,L_axi,L_exi,L_ixi,L_oxi
      REAL*4 R_uxi,R_abo,R_ebo,R_ibo,R_obo,R0_ubo
      LOGICAL*1 L0_ado,L0_edo
      REAL*4 R0_ido,R_odo,R0_udo,R0_afo,R0_efo,R0_ifo,R0_ofo
      LOGICAL*1 L0_ufo,L0_ako
      INTEGER*4 I0_eko,I0_iko
      LOGICAL*1 L0_oko
      INTEGER*4 I_uko,I_alo,I_elo,I0_ilo,I0_olo
      LOGICAL*1 L0_ulo
      INTEGER*4 I_amo,I_emo,I_imo,I0_omo,I0_umo
      LOGICAL*1 L0_apo
      INTEGER*4 I_epo,I_ipo,I_opo,I0_upo,I0_aro
      LOGICAL*1 L0_ero
      INTEGER*4 I_iro,I_oro,I_uro,I_aso,I0_eso,I0_iso
      LOGICAL*1 L0_oso
      INTEGER*4 I_uso,I_ato,I0_eto,I0_ito
      LOGICAL*1 L0_oto
      INTEGER*4 I_uto,I0_avo,I0_evo,I_ivo
      REAL*4 R_ovo,R_uvo,R_axo,R_exo
      INTEGER*4 I_ixo,I0_oxo,I0_uxo,I_abu,I0_ebu,I0_ibu,I0_obu
     &,I0_ubu,I_adu,I0_edu,I0_idu,I_odu
      CHARACTER*20 C20_udu,C20_afu,C20_efu,C20_ifu,C20_ofu
      CHARACTER*8 C8_ufu
      LOGICAL*1 L0_aku
      CHARACTER*8 C8_eku
      LOGICAL*1 L0_iku
      CHARACTER*8 C8_oku
      LOGICAL*1 L0_uku
      INTEGER*4 I0_alu
      LOGICAL*1 L0_elu
      INTEGER*4 I0_ilu
      LOGICAL*1 L0_olu
      INTEGER*4 I0_ulu,I0_amu,I0_emu
      LOGICAL*1 L0_imu
      INTEGER*4 I0_omu,I0_umu,I0_apu
      REAL*4 R_epu,R_ipu
      LOGICAL*1 L_opu
      REAL*4 R_upu,R_aru
      INTEGER*4 I_eru,I0_iru,I0_oru
      LOGICAL*1 L_uru,L_asu,L_esu,L0_isu
      INTEGER*4 I_osu,I0_usu,I0_atu,I_etu,I0_itu,I0_otu,I_utu
     &,I0_avu,I0_evu
      LOGICAL*1 L0_ivu,L_ovu,L0_uvu,L_axu,L0_exu,L_ixu,L0_oxu
     &,L_uxu,L_abad,L_ebad,L0_ibad,L0_obad,L0_ubad,L0_adad
     &,L0_edad,L_idad,L0_odad,L0_udad,L_afad,L0_efad,L_ifad
     &,L_ofad
      REAL*4 R0_ufad
      REAL*8 R8_akad
      LOGICAL*1 L0_ekad,L0_ikad
      REAL*4 R0_okad,R_ukad,R0_alad,R0_elad
      REAL*8 R8_ilad
      LOGICAL*1 L_olad,L_ulad,L0_amad,L_emad,L_imad,L0_omad
     &,L_umad
      INTEGER*4 I_apad,I0_epad,I0_ipad
      LOGICAL*1 L_opad,L_upad,L0_arad,L0_erad,L0_irad,L0_orad
     &,L_urad,L0_asad,L0_esad,L0_isad,L0_osad,L0_usad,L0_atad
     &,L0_etad,L_itad,L0_otad,L0_utad,L0_avad,L0_evad,L_ivad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_um=R_ol
C TELEGKA5_HANDLER.fmg( 394, 406):pre: ����������� ��
      R0_ep=R_il
C TELEGKA5_HANDLER.fmg( 386, 412):pre: ����������� ��
      R0_et=R_ul
C TELEGKA5_HANDLER.fmg( 349, 414):pre: ����������� ��
      R0_as=R_am
C TELEGKA5_HANDLER.fmg( 319, 403):pre: ����������� ��
      R0_ov=R_ef
C TELEGKA5_HANDLER.fmg( 384, 461):pre: ����������� ��
      R0_efe=R_ife
C TELEGKA5_HANDLER.fmg( 249, 347):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_ide=R_ode
C TELEGKA5_HANDLER.fmg( 249, 335):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_ese=R_ise
C TELEGKA5_HANDLER.fmg( 234, 311):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_ere=R_ire
C TELEGKA5_HANDLER.fmg( 234, 298):pre: �������� ��������� ������,nv2dyn$83Dasha
      L_ud=L_ed.and..not.L_u
      L_u=L_ed
C TELEGKA5_HANDLER.fmg( 387, 286):������������  �� 1 ���
      L_od=L_id.and..not.L_ad
      L_ad=L_id
C TELEGKA5_HANDLER.fmg( 387, 296):������������  �� 1 ���
      if (.not.L_ud.and.L_od) then
          I_af = I_af+1
      endif
      if (L_ud) then
          I_af = 0
      endif
C TELEGKA5_HANDLER.fmg( 398, 286):�������,COUNTER
      I_o=I_af
C TELEGKA5_HANDLER.fmg( 415, 276):������,NUM_BOAT
      R0_al = 15300
C TELEGKA5_HANDLER.fmg( 382, 439):��������� (RE4) (�������)
      R0_el = R_ut * R0_al
C TELEGKA5_HANDLER.fmg( 387, 442):����������
      R0_if = 1.0
C TELEGKA5_HANDLER.fmg( 394, 424):��������� (RE4) (�������)
      R0_uf = R0_el + (-R0_if)
C TELEGKA5_HANDLER.fmg( 398, 425):��������
      R0_of = 1.0
C TELEGKA5_HANDLER.fmg( 394, 429):��������� (RE4) (�������)
      R0_ak = R0_el + R0_of
C TELEGKA5_HANDLER.fmg( 398, 430):��������
      R0_em = 0.0
C TELEGKA5_HANDLER.fmg( 428, 414):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_om = 0.99
C TELEGKA5_HANDLER.fmg( 410, 416):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_ati) then
         R0_ar=R0_um
      endif
C TELEGKA5_HANDLER.fmg( 398, 412):���� � ������������� �������
      R0_ip = 1.0
C TELEGKA5_HANDLER.fmg( 363, 412):��������� (RE4) (�������)
      R0_op = 0.0
C TELEGKA5_HANDLER.fmg( 355, 412):��������� (RE4) (�������)
      R0_or = 1.0e-10
C TELEGKA5_HANDLER.fmg( 333, 390):��������� (RE4) (�������)
      R0_us = 0.0
C TELEGKA5_HANDLER.fmg( 343, 404):��������� (RE4) (�������)
      R0_es = R0_as + (-R_ut)
C TELEGKA5_HANDLER.fmg( 322, 396):��������
      R0_av = 0.33
C TELEGKA5_HANDLER.fmg( 324, 475):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_uv = 0.000001
C TELEGKA5_HANDLER.fmg( 138, 412):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ax=R_ipe.lt.R0_uv
C TELEGKA5_HANDLER.fmg( 145, 414):���������� <,nv2dyn$46Dasha
      L0_ex = L_ovi.AND.L0_ax
C TELEGKA5_HANDLER.fmg( 152, 418):�
      R0_ix = 0.999999
C TELEGKA5_HANDLER.fmg( 133, 460):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ox=R_ipe.gt.R0_ix
C TELEGKA5_HANDLER.fmg( 142, 462):���������� >,nv2dyn$45Dasha
      L0_ux = L_exi.AND.L0_ox
C TELEGKA5_HANDLER.fmg( 152, 468):�
      R0_are = 20
C TELEGKA5_HANDLER.fmg( 226, 301):��������� (RE4) (�������)
      if(.not.L_ivi) then
         R_ire=0.0
      elseif(.not.L_ure) then
         R_ire=R0_are
      else
         R_ire=max(R0_ere-deltat,0.0)
      endif
      L0_ore=L_ivi.and.R_ire.le.0.0
      L_ure=L_ivi
C TELEGKA5_HANDLER.fmg( 234, 298):�������� ��������� ������,nv2dyn$83Dasha
      R0_ase = 20
C TELEGKA5_HANDLER.fmg( 226, 314):��������� (RE4) (�������)
      if(.not.L_ivi) then
         R_ise=0.0
      elseif(.not.L_use) then
         R_ise=R0_ase
      else
         R_ise=max(R0_ese-deltat,0.0)
      endif
      L0_ose=L_ivi.and.R_ise.le.0.0
      L_use=L_ivi
C TELEGKA5_HANDLER.fmg( 234, 311):�������� ��������� ������,nv2dyn$82Dasha
      R0_ede = 20
C TELEGKA5_HANDLER.fmg( 243, 338):��������� (RE4) (�������)
      R0_afe = 20
C TELEGKA5_HANDLER.fmg( 244, 352):��������� (RE4) (�������)
      R0_eme = 0.000001
C TELEGKA5_HANDLER.fmg( 206, 259):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ime = 0.999999
C TELEGKA5_HANDLER.fmg( 206, 265):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_ape = 0.000001
C TELEGKA5_HANDLER.fmg( 206, 276):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ope=R_ipe.lt.R0_ape
C TELEGKA5_HANDLER.fmg( 212, 278):���������� <,nv2dyn$46Dasha
      L0_ute = L_ovi.AND.L0_ope
C TELEGKA5_HANDLER.fmg( 243, 276):�
      R0_epe = 0.999999
C TELEGKA5_HANDLER.fmg( 206, 282):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_upe=R_ipe.gt.R0_epe
C TELEGKA5_HANDLER.fmg( 210, 284):���������� >,nv2dyn$45Dasha
      L0_ave = L_exi.AND.L0_upe
C TELEGKA5_HANDLER.fmg( 243, 287):�
      L_oxe=(L_ixe.or.L_oxe).and..not.(L_axe)
      L0_exe=.not.L_oxe
C TELEGKA5_HANDLER.fmg( 285, 275):RS �������
      R0_uli = 0.01
C TELEGKA5_HANDLER.fmg(  26, 282):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ami = R0_uli + R_epi
C TELEGKA5_HANDLER.fmg(  29, 282):��������
      R0_emi = 0.01
C TELEGKA5_HANDLER.fmg(  26, 290):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_omi = (-R0_emi) + R_epi
C TELEGKA5_HANDLER.fmg(  29, 289):��������
      C20_isi = '��������� 2'
C TELEGKA5_HANDLER.fmg(  17, 309):��������� ���������� CH20 (CH30) (�������)
      C20_osi = '������� ���'
C TELEGKA5_HANDLER.fmg(  17, 311):��������� ���������� CH20 (CH30) (�������)
      C20_asi = '��������� 1'
C TELEGKA5_HANDLER.fmg(  32, 308):��������� ���������� CH20 (CH30) (�������)
      R0_ubo = 20
C TELEGKA5_HANDLER.fmg( 322, 479):��������� (RE4) (�������)
      R0_ev = R0_ubo * R0_av
C TELEGKA5_HANDLER.fmg( 330, 476):����������
      if(L_ivi) then
         R0_udo=R0_ev
      else
         R0_udo=R0_ubo
      endif
C TELEGKA5_HANDLER.fmg( 336, 478):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_ido = 0.0
C TELEGKA5_HANDLER.fmg( 358, 469):��������� (RE4) (�������)
      R0_ofo = 0.0
C TELEGKA5_HANDLER.fmg( 344, 469):��������� (RE4) (�������)
      I0_iko = z'01000004'
C TELEGKA5_HANDLER.fmg( 194, 468):��������� ������������� IN (�������)
      I0_eko = z'0100000B'
C TELEGKA5_HANDLER.fmg( 194, 466):��������� ������������� IN (�������)
      L0_oko=I_alo.gt.I_uko
C TELEGKA5_HANDLER.fmg( 195, 457):���������� �������������
      if(L0_oko) then
         I_elo=I0_eko
      else
         I_elo=I0_iko
      endif
C TELEGKA5_HANDLER.fmg( 198, 466):���� RE IN LO CH7
      I0_olo = z'01000004'
C TELEGKA5_HANDLER.fmg( 162, 486):��������� ������������� IN (�������)
      I0_ilo = z'0100000B'
C TELEGKA5_HANDLER.fmg( 162, 484):��������� ������������� IN (�������)
      L0_ulo=I_emo.gt.I_amo
C TELEGKA5_HANDLER.fmg( 162, 474):���������� �������������
      if(L0_ulo) then
         I_imo=I0_ilo
      else
         I_imo=I0_olo
      endif
C TELEGKA5_HANDLER.fmg( 166, 484):���� RE IN LO CH7
      I0_umo = z'01000004'
C TELEGKA5_HANDLER.fmg( 118, 486):��������� ������������� IN (�������)
      I0_omo = z'0100000B'
C TELEGKA5_HANDLER.fmg( 118, 484):��������� ������������� IN (�������)
      L0_apo=I_ipo.gt.I_epo
C TELEGKA5_HANDLER.fmg( 117, 474):���������� �������������
      if(L0_apo) then
         I_opo=I0_omo
      else
         I_opo=I0_umo
      endif
C TELEGKA5_HANDLER.fmg( 122, 484):���� RE IN LO CH7
      I0_aro = z'01000004'
C TELEGKA5_HANDLER.fmg(  74, 486):��������� ������������� IN (�������)
      I0_upo = z'0100000B'
C TELEGKA5_HANDLER.fmg(  74, 484):��������� ������������� IN (�������)
      L0_ero=I_oro.gt.I_iro
C TELEGKA5_HANDLER.fmg(  73, 474):���������� �������������
      if(L0_ero) then
         I_uro=I0_upo
      else
         I_uro=I0_aro
      endif
C TELEGKA5_HANDLER.fmg(  78, 484):���� RE IN LO CH7
      I0_iso = z'01000004'
C TELEGKA5_HANDLER.fmg(  30, 486):��������� ������������� IN (�������)
      I0_eso = z'0100000B'
C TELEGKA5_HANDLER.fmg(  30, 484):��������� ������������� IN (�������)
      L0_oso=I_ato.gt.I_uso
C TELEGKA5_HANDLER.fmg(  30, 474):���������� �������������
      if(L0_oso) then
         I_aso=I0_eso
      else
         I_aso=I0_iso
      endif
C TELEGKA5_HANDLER.fmg(  34, 484):���� RE IN LO CH7
      I0_eto = z'0100000A'
C TELEGKA5_HANDLER.fmg( 236, 480):��������� ������������� IN (�������)
      I0_ito = z'01000003'
C TELEGKA5_HANDLER.fmg( 236, 482):��������� ������������� IN (�������)
      I0_evo = z'0100000A'
C TELEGKA5_HANDLER.fmg( 272, 482):��������� ������������� IN (�������)
      I0_avo = z'01000003'
C TELEGKA5_HANDLER.fmg( 272, 480):��������� ������������� IN (�������)
      L_ovu=R_uvo.ne.R_ovo
      R_ovo=R_uvo
C TELEGKA5_HANDLER.fmg(  16, 384):���������� ������������� ������
      L_axu=R_exo.ne.R_axo
      R_axo=R_exo
C TELEGKA5_HANDLER.fmg(  20, 439):���������� ������������� ������
      I0_uxo = z'01000003'
C TELEGKA5_HANDLER.fmg( 140, 352):��������� ������������� IN (�������)
      I0_oxo = z'01000010'
C TELEGKA5_HANDLER.fmg( 140, 350):��������� ������������� IN (�������)
      I0_ibu = z'01000003'
C TELEGKA5_HANDLER.fmg( 118, 442):��������� ������������� IN (�������)
      I0_ebu = z'01000010'
C TELEGKA5_HANDLER.fmg( 118, 440):��������� ������������� IN (�������)
      I0_ubu = z'01000003'
C TELEGKA5_HANDLER.fmg( 186, 419):��������� ������������� IN (�������)
      I0_obu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 186, 417):��������� ������������� IN (�������)
      I0_idu = z'01000003'
C TELEGKA5_HANDLER.fmg( 194, 369):��������� ������������� IN (�������)
      I0_edu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 194, 367):��������� ������������� IN (�������)
      I0_atu = z'01000003'
C TELEGKA5_HANDLER.fmg( 116, 366):��������� ������������� IN (�������)
      I0_usu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 116, 364):��������� ������������� IN (�������)
      C20_udu = '� ��������� 1'
C TELEGKA5_HANDLER.fmg(  52, 361):��������� ���������� CH20 (CH30) (�������)
      C20_efu = '� ��������� 2'
C TELEGKA5_HANDLER.fmg(  36, 362):��������� ���������� CH20 (CH30) (�������)
      C20_ifu = ''
C TELEGKA5_HANDLER.fmg(  36, 364):��������� ���������� CH20 (CH30) (�������)
      C8_ufu = 'pos1'
C TELEGKA5_HANDLER.fmg(  19, 370):��������� ���������� CH8 (�������)
      call chcomp(C8_oku,C8_ufu,L0_aku)
C TELEGKA5_HANDLER.fmg(  24, 374):���������� ���������
      C8_eku = 'pos2'
C TELEGKA5_HANDLER.fmg(  19, 412):��������� ���������� CH8 (�������)
      call chcomp(C8_oku,C8_eku,L0_iku)
C TELEGKA5_HANDLER.fmg(  24, 416):���������� ���������
      if(L0_iku) then
         C20_afu=C20_efu
      else
         C20_afu=C20_ifu
      endif
C TELEGKA5_HANDLER.fmg(  40, 362):���� RE IN LO CH20
      if(L0_aku) then
         C20_ofu=C20_udu
      else
         C20_ofu=C20_afu
      endif
C TELEGKA5_HANDLER.fmg(  56, 362):���� RE IN LO CH20
      I0_alu = z'0100008E'
C TELEGKA5_HANDLER.fmg( 170, 387):��������� ������������� IN (�������)
      I0_ilu = z'01000086'
C TELEGKA5_HANDLER.fmg( 166, 442):��������� ������������� IN (�������)
      I0_amu = z'01000003'
C TELEGKA5_HANDLER.fmg( 153, 452):��������� ������������� IN (�������)
      I0_emu = z'01000010'
C TELEGKA5_HANDLER.fmg( 153, 454):��������� ������������� IN (�������)
      I0_apu = z'01000003'
C TELEGKA5_HANDLER.fmg( 160, 399):��������� ������������� IN (�������)
      I0_umu = z'01000010'
C TELEGKA5_HANDLER.fmg( 160, 397):��������� ������������� IN (�������)
      L_esu=R_ipu.ne.R_epu
      R_epu=R_ipu
C TELEGKA5_HANDLER.fmg(  16, 402):���������� ������������� ������
      L0_isu = L_esu.OR.L_asu.OR.L_uru
C TELEGKA5_HANDLER.fmg(  55, 400):���
      L_opu=R_aru.ne.R_upu
      R_upu=R_aru
C TELEGKA5_HANDLER.fmg(  20, 427):���������� ������������� ������
      L0_exu = L_opu.AND.L0_iku
C TELEGKA5_HANDLER.fmg(  32, 426):�
      L0_ibad = L0_exu.OR.L_axu
C TELEGKA5_HANDLER.fmg(  52, 424):���
      L0_ame = L_uxu.AND.L0_ibad
C TELEGKA5_HANDLER.fmg( 228, 347):�
      L0_oke = L0_ibad.OR.(.NOT.L_ixu)
C TELEGKA5_HANDLER.fmg( 234, 331):���
      L0_uvu = L_opu.AND.L0_aku
C TELEGKA5_HANDLER.fmg(  30, 382):�
      L0_oxu = L0_uvu.OR.L_ovu
C TELEGKA5_HANDLER.fmg(  54, 381):���
      L0_ule = L_ixu.AND.L0_oxu
C TELEGKA5_HANDLER.fmg( 228, 335):�
      L_uke=(L0_ule.or.L_uke).and..not.(L0_oke)
      L0_ale=.not.L_uke
C TELEGKA5_HANDLER.fmg( 240, 333):RS �������,156
      if(.not.L_uke) then
         R_ode=0.0
      elseif(.not.L_ude) then
         R_ode=R0_ede
      else
         R_ode=max(R0_ide-deltat,0.0)
      endif
      L0_ufe=L_uke.and.R_ode.le.0.0
      L_ude=L_uke
C TELEGKA5_HANDLER.fmg( 249, 335):�������� ��������� ������,nv2dyn$100Dasha
      L_ate=(L0_ufe.or.L_ate).and..not.(L_ili)
      L0_ake=.not.L_ate
C TELEGKA5_HANDLER.fmg( 266, 333):RS �������,nv2dyn$96Dasha
      L0_ele = (.NOT.L_uxu).OR.L0_oxu
C TELEGKA5_HANDLER.fmg( 234, 343):���
      L_ile=(L0_ame.or.L_ile).and..not.(L0_ele)
      L0_ole=.not.L_ile
C TELEGKA5_HANDLER.fmg( 240, 345):RS �������,155
      if(.not.L_ile) then
         R_ife=0.0
      elseif(.not.L_ofe) then
         R_ife=R0_afe
      else
         R_ife=max(R0_efe-deltat,0.0)
      endif
      L0_eke=L_ile.and.R_ife.le.0.0
      L_ofe=L_ile
C TELEGKA5_HANDLER.fmg( 249, 347):�������� ��������� ������,nv2dyn$99Dasha
      L_ete=(L0_eke.or.L_ete).and..not.(L_ifi)
      L0_ike=.not.L_ete
C TELEGKA5_HANDLER.fmg( 266, 345):RS �������,nv2dyn$95Dasha
      I0_avu = z'0100000E'
C TELEGKA5_HANDLER.fmg( 190, 390):��������� ������������� IN (�������)
      I0_iru = z'01000007'
C TELEGKA5_HANDLER.fmg( 146, 372):��������� ������������� IN (�������)
      I0_oru = z'01000003'
C TELEGKA5_HANDLER.fmg( 146, 374):��������� ������������� IN (�������)
      I0_otu = z'01000003'
C TELEGKA5_HANDLER.fmg( 123, 455):��������� ������������� IN (�������)
      I0_itu = z'0100000A'
C TELEGKA5_HANDLER.fmg( 123, 453):��������� ������������� IN (�������)
      I0_epad = z'0100000E'
C TELEGKA5_HANDLER.fmg( 184, 445):��������� ������������� IN (�������)
      L0_edad=.false.
C TELEGKA5_HANDLER.fmg(  66, 405):��������� ���������� (�������)
      L0_adad=.false.
C TELEGKA5_HANDLER.fmg(  66, 403):��������� ���������� (�������)
      R0_ufad = 0.1
C TELEGKA5_HANDLER.fmg( 252, 379):��������� (RE4) (�������)
      L_ofad=R8_akad.lt.R0_ufad
C TELEGKA5_HANDLER.fmg( 257, 380):���������� <
      R0_okad = 0.0
C TELEGKA5_HANDLER.fmg( 264, 400):��������� (RE4) (�������)
      L0_aki = (.NOT.L_iti).AND.L_ufi
C TELEGKA5_HANDLER.fmg( 100, 278):�
C label 267  try267=try267-1
      L0_eki = L_ifi.AND.L0_aki
C TELEGKA5_HANDLER.fmg( 104, 279):�
      L_ufi=(L_iti.or.L_ufi).and..not.(L0_eki)
      L0_ofi=.not.L_ufi
C TELEGKA5_HANDLER.fmg(  96, 275):RS �������,nv2dyn$103Dasha
C sav1=L0_aki
      L0_aki = (.NOT.L_iti).AND.L_ufi
C TELEGKA5_HANDLER.fmg( 100, 278):recalc:�
C if(sav1.ne.L0_aki .and. try267.gt.0) goto 267
      L0_ubi = (.NOT.L_iti).AND.L_obi
C TELEGKA5_HANDLER.fmg( 105, 264):�
C label 271  try271=try271-1
      L0_adi = L_ili.AND.L0_ubi
C TELEGKA5_HANDLER.fmg( 109, 265):�
      L_obi=(L_iti.or.L_obi).and..not.(L0_adi)
      L0_ibi=.not.L_obi
C TELEGKA5_HANDLER.fmg( 100, 261):RS �������,nv2dyn$106Dasha
C sav1=L0_ubi
      L0_ubi = (.NOT.L_iti).AND.L_obi
C TELEGKA5_HANDLER.fmg( 105, 264):recalc:�
C if(sav1.ne.L0_ubi .and. try271.gt.0) goto 271
      L0_ebe = L_upad.AND.L_uti
C TELEGKA5_HANDLER.fmg(  28, 352):�
C label 276  try276=try276-1
      L0_ade=R_obo.lt.R_e
C TELEGKA5_HANDLER.fmg( 404, 446):���������� <
      L0_ibe = L0_ade.AND.(.NOT.L_ovi)
C TELEGKA5_HANDLER.fmg( 440, 444):�
      L_opad = L_uvi.OR.L0_ibe.OR.L_ixi
C TELEGKA5_HANDLER.fmg( 444, 444):���
      L0_uve = L_axi.AND.L_opad
C TELEGKA5_HANDLER.fmg( 250, 322):�
      L0_ove = L_uvi.AND.L_upad
C TELEGKA5_HANDLER.fmg( 250, 317):�
      L0_ive = L0_ose.AND.(.NOT.L_upad)
C TELEGKA5_HANDLER.fmg( 242, 310):�
      L0_eve = L0_ore.AND.(.NOT.L_opad)
C TELEGKA5_HANDLER.fmg( 240, 296):�
      R0_ir = R_odo + R0_es
C TELEGKA5_HANDLER.fmg( 328, 397):��������
      R0_ur = R0_ir * R0_es
C TELEGKA5_HANDLER.fmg( 332, 396):����������
      L0_is=R0_ur.lt.R0_or
C TELEGKA5_HANDLER.fmg( 338, 395):���������� <
      L_os=(L0_is.or.L_os).and..not.(.NOT.L_iti)
      L0_er=.not.L_os
C TELEGKA5_HANDLER.fmg( 344, 393):RS �������
      if(L_os) then
         R0_at=R0_us
      else
         R0_at=R_odo
      endif
C TELEGKA5_HANDLER.fmg( 346, 404):���� RE IN LO CH7
      R0_it = R0_et + R0_at
C TELEGKA5_HANDLER.fmg( 352, 406):��������
      R0_up=MAX(R0_op,R0_it)
C TELEGKA5_HANDLER.fmg( 360, 407):��������
      R0_ot=MIN(R0_ip,R0_up)
C TELEGKA5_HANDLER.fmg( 368, 408):�������
      if(L_ati) then
         R0_ar=R0_ot
      endif
C TELEGKA5_HANDLER.fmg( 380, 412):���� � ������������� �������
      if(L_os) then
         R0_ar=R_ut
      endif
C TELEGKA5_HANDLER.fmg( 338, 412):���� � ������������� �������
      if(L_evi) then
         R0_im=R0_om
      else
         R0_im=R0_ar
      endif
C TELEGKA5_HANDLER.fmg( 414, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_uti) then
         R_ipi=R0_em
      else
         R_ipi=R0_im
      endif
C TELEGKA5_HANDLER.fmg( 432, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      L0_imi=R_ipi.gt.R0_omi
C TELEGKA5_HANDLER.fmg(  34, 293):���������� >,nv2dyn$84Dasha
      L0_oli=R_ipi.lt.R0_ami
C TELEGKA5_HANDLER.fmg(  36, 287):���������� <,nv2dyn$85Dasha
      L0_umi = L0_imi.AND.L0_oli
C TELEGKA5_HANDLER.fmg(  41, 292):�
      L0_api = L0_umi.AND.L_iti
C TELEGKA5_HANDLER.fmg(  53, 284):�
      L_ali=(L_ili.or.L_ali).and..not.(L_opad)
      L0_eli=.not.L_ali
C TELEGKA5_HANDLER.fmg(  54, 277):RS �������,nv2dyn$101Dasha
      L0_uki = L0_api.AND.L_ali
C TELEGKA5_HANDLER.fmg(  72, 283):�
      L_iki=(L0_uki.or.L_iki).and..not.(L0_eki)
      L0_oki=.not.L_iki
C TELEGKA5_HANDLER.fmg( 110, 281):RS �������,nv2dyn$102Dasha
      L_odi=(L_ifi.or.L_odi).and..not.(L_upad)
      L0_idi=.not.L_odi
C TELEGKA5_HANDLER.fmg(  58, 266):RS �������,nv2dyn$104Dasha
      L0_udi = L0_api.AND.L_odi
C TELEGKA5_HANDLER.fmg(  72, 269):�
      L_afi=(L0_udi.or.L_afi).and..not.(L0_adi)
      L0_edi=.not.L_afi
C TELEGKA5_HANDLER.fmg( 118, 267):RS �������,nv2dyn$105Dasha
      L_efi = L_iki.OR.L_afi
C TELEGKA5_HANDLER.fmg( 131, 282):���
      L_iri=(L_upad.or.L_iri).and..not.(L_opad)
      L0_upi=.not.L_iri
C TELEGKA5_HANDLER.fmg( 106, 305):RS �������,nv2dyn$86Dasha
      L0_ori = L_oxi.AND.(.NOT.L_iri)
C TELEGKA5_HANDLER.fmg( 116, 308):�
      L_ari=(L_opad.or.L_ari).and..not.(L_upad)
      L0_opi=.not.L_ari
C TELEGKA5_HANDLER.fmg( 105, 294):RS �������,nv2dyn$87Dasha
      L0_eri = L_ixi.AND.(.NOT.L_ari)
C TELEGKA5_HANDLER.fmg( 114, 299):�
      L_uri = L0_ori.OR.L0_eri
C TELEGKA5_HANDLER.fmg( 122, 307):���
      L0_ume=R_ipi.gt.R0_ime
C TELEGKA5_HANDLER.fmg( 210, 266):���������� >,nv2dyn$45Dasha
      L0_ote = L_avi.AND.L0_ume
C TELEGKA5_HANDLER.fmg( 243, 270):�
      L0_ome=R_ipi.lt.R0_eme
C TELEGKA5_HANDLER.fmg( 212, 260):���������� <,nv2dyn$46Dasha
      L0_ite = L_oti.AND.L0_ome
C TELEGKA5_HANDLER.fmg( 243, 258):�
      L0_uxe =.NOT.(L_emad.OR.L_olad.OR.L0_uve.OR.L0_ove.OR.L0_ive.OR.L0
     &_eve.OR.L0_ave.OR.L0_ute.OR.L_ati.OR.L_evi.OR.L_uti.OR.L_efi.OR.L_
     &uri.OR.L0_ote.OR.L0_ite.OR.L_ete.OR.L_ate)
C TELEGKA5_HANDLER.fmg( 277, 310):���
      L0_abi =.NOT.(L0_uxe)
C TELEGKA5_HANDLER.fmg( 288, 282):���
      L_umad = L0_abi.OR.L_oxe
C TELEGKA5_HANDLER.fmg( 292, 281):���
      L0_ubad = L_umad.OR.L_abad
C TELEGKA5_HANDLER.fmg(  58, 436):���
      L0_omad = L0_ibad.AND.(.NOT.L0_ubad).AND.(.NOT.L_uxu
     &)
C TELEGKA5_HANDLER.fmg(  68, 430):�
      L0_utad = L0_omad.OR.L_imad.OR.L_emad
C TELEGKA5_HANDLER.fmg(  72, 424):���
      L0_obad = L_umad.OR.L_ebad
C TELEGKA5_HANDLER.fmg(  56, 391):���
      L0_amad = (.NOT.L0_obad).AND.L0_oxu.AND.(.NOT.L_ixu
     &)
C TELEGKA5_HANDLER.fmg(  68, 386):�
      L0_isad = L0_amad.OR.L_ulad.OR.L_olad
C TELEGKA5_HANDLER.fmg(  72, 382):���
      L0_udad = L0_utad.OR.L0_isad
C TELEGKA5_HANDLER.fmg(  76, 393):���
      L0_odad = L_opad.OR.L0_isu.OR.L_upad
C TELEGKA5_HANDLER.fmg(  62, 397):���
      L_afad=(L0_odad.or.L_afad).and..not.(L0_udad)
      L0_efad=.not.L_afad
C TELEGKA5_HANDLER.fmg( 107, 395):RS �������,10
      L_ifad = L_asu.OR.L_afad
C TELEGKA5_HANDLER.fmg( 118, 398):���
      L0_avad = L_upad.AND.(.NOT.L_axi)
C TELEGKA5_HANDLER.fmg(  74, 446):�
      L0_usad = (.NOT.L_ifad).AND.L0_avad
C TELEGKA5_HANDLER.fmg( 106, 447):�
      L0_arad = L_ifad.OR.L_ivad
C TELEGKA5_HANDLER.fmg(  98, 404):���
      L0_abe = L_opad.AND.L_evi
C TELEGKA5_HANDLER.fmg(  28, 340):�
      L0_ivu = L0_ebe.OR.L0_abe
C TELEGKA5_HANDLER.fmg(  36, 350):���
      L0_etad = L0_avad.OR.L0_arad.OR.L0_isad.OR.L0_ivu
C TELEGKA5_HANDLER.fmg( 104, 418):���
      L0_evad = (.NOT.L0_avad).AND.L0_utad
C TELEGKA5_HANDLER.fmg( 104, 425):�
      L_itad=(L0_evad.or.L_itad).and..not.(L0_etad)
      L0_otad=.not.L_itad
C TELEGKA5_HANDLER.fmg( 112, 423):RS �������,1
      L0_atad = (.NOT.L0_usad).AND.L_itad
C TELEGKA5_HANDLER.fmg( 130, 426):�
      L0_edo = L0_atad.AND.(.NOT.L_olad)
C TELEGKA5_HANDLER.fmg( 314, 468):�
      L0_ako = L0_edo.OR.L_emad
C TELEGKA5_HANDLER.fmg( 326, 466):���
      if(L0_ako) then
         R0_efo=R0_udo
      else
         R0_efo=R0_ofo
      endif
C TELEGKA5_HANDLER.fmg( 348, 479):���� RE IN LO CH7
      L0_esad = L_opad.AND.(.NOT.L_uvi)
C TELEGKA5_HANDLER.fmg(  94, 353):�
      L0_orad = L0_arad.OR.L0_esad.OR.L0_utad.OR.L0_ivu
C TELEGKA5_HANDLER.fmg( 104, 374):���
      L0_osad = L0_isad.AND.(.NOT.L0_esad)
C TELEGKA5_HANDLER.fmg( 104, 381):�
      L_urad=(L0_osad.or.L_urad).and..not.(L0_orad)
      L0_asad=.not.L_urad
C TELEGKA5_HANDLER.fmg( 112, 379):RS �������,2
      L0_erad = (.NOT.L_ifad).AND.L0_esad
C TELEGKA5_HANDLER.fmg( 104, 354):�
      L0_irad = L_urad.AND.(.NOT.L0_erad)
C TELEGKA5_HANDLER.fmg( 130, 380):�
      L0_ado = L0_irad.AND.(.NOT.L_emad)
C TELEGKA5_HANDLER.fmg( 314, 454):�
      L0_ufo = L0_ado.OR.L_olad
C TELEGKA5_HANDLER.fmg( 326, 452):���
      if(L0_ufo) then
         R0_afo=R0_udo
      else
         R0_afo=R0_ofo
      endif
C TELEGKA5_HANDLER.fmg( 348, 461):���� RE IN LO CH7
      R0_ifo = R0_efo + (-R0_afo)
C TELEGKA5_HANDLER.fmg( 354, 472):��������
      if(L_ivad) then
         R_odo=R0_ido
      else
         R_odo=R0_ifo
      endif
C TELEGKA5_HANDLER.fmg( 362, 470):���� RE IN LO CH7
C sav1=R0_ir
      R0_ir = R_odo + R0_es
C TELEGKA5_HANDLER.fmg( 328, 397):recalc:��������
C if(sav1.ne.R0_ir .and. try314.gt.0) goto 314
      R_ebo=R_ebo+deltat/R_abo*R_odo
      if(R_ebo.gt.R_uxi) then
         R_ebo=R_uxi
      elseif(R_ebo.lt.R_ibo) then
         R_ebo=R_ibo
      endif
C TELEGKA5_HANDLER.fmg( 376, 456):����������,V_INT_
      if(L_ati) then
         R0_iv=R0_ov
      else
         R0_iv=R_ebo
      endif
C TELEGKA5_HANDLER.fmg( 384, 456):���� RE IN LO CH7
      L0_ik=R_obo.lt.R0_ak
C TELEGKA5_HANDLER.fmg( 378, 430):���������� <
      L0_ek=R_obo.gt.R0_uf
C TELEGKA5_HANDLER.fmg( 378, 424):���������� >
      L0_ok = L0_ik.AND.L0_ek
C TELEGKA5_HANDLER.fmg( 384, 430):�
      L0_uk = L_iti.AND.L0_ok
C TELEGKA5_HANDLER.fmg( 388, 435):�
      if(L0_uk) then
         R_obo=R0_el
      else
         R_obo=R0_iv
      endif
C TELEGKA5_HANDLER.fmg( 392, 455):���� RE IN LO CH7
      L0_obe=R_obo.gt.R_i
C TELEGKA5_HANDLER.fmg( 404, 462):���������� >
      L0_ube = L0_obe.AND.(.NOT.L_exi)
C TELEGKA5_HANDLER.fmg( 444, 461):�
      L_upad = L_axi.OR.L0_ube.OR.L_oxi
C TELEGKA5_HANDLER.fmg( 450, 461):���
C sav1=L0_ive
      L0_ive = L0_ose.AND.(.NOT.L_upad)
C TELEGKA5_HANDLER.fmg( 242, 310):recalc:�
C if(sav1.ne.L0_ive .and. try307.gt.0) goto 307
C sav1=L0_odad
      L0_odad = L_opad.OR.L0_isu.OR.L_upad
C TELEGKA5_HANDLER.fmg(  62, 397):recalc:���
C if(sav1.ne.L0_odad .and. try438.gt.0) goto 438
C sav1=L0_ebe
      L0_ebe = L_upad.AND.L_uti
C TELEGKA5_HANDLER.fmg(  28, 352):recalc:�
C if(sav1.ne.L0_ebe .and. try276.gt.0) goto 276
C sav1=L0_ove
      L0_ove = L_uvi.AND.L_upad
C TELEGKA5_HANDLER.fmg( 250, 317):recalc:�
C if(sav1.ne.L0_ove .and. try304.gt.0) goto 304
C sav1=L0_avad
      L0_avad = L_upad.AND.(.NOT.L_axi)
C TELEGKA5_HANDLER.fmg(  74, 446):recalc:�
C if(sav1.ne.L0_avad .and. try450.gt.0) goto 450
      L0_imu = (.NOT.L0_ux).AND.L_upad
C TELEGKA5_HANDLER.fmg( 154, 392):�
      if(L0_imu) then
         I0_omu=I0_umu
      else
         I0_omu=I0_apu
      endif
C TELEGKA5_HANDLER.fmg( 164, 398):���� RE IN LO CH7
      if(L_upad) then
         C20_esi=C20_isi
      else
         C20_esi=C20_osi
      endif
C TELEGKA5_HANDLER.fmg(  21, 310):���� RE IN LO CH20
      if(L_opad) then
         C20_usi=C20_asi
      else
         C20_usi=C20_esi
      endif
C TELEGKA5_HANDLER.fmg(  37, 308):���� RE IN LO CH20
      R_ef=R0_iv
C TELEGKA5_HANDLER.fmg( 384, 461):����������� ��
      if(L0_irad) then
         I_odu=I0_edu
      else
         I_odu=I0_idu
      endif
C TELEGKA5_HANDLER.fmg( 198, 368):���� RE IN LO CH7
      L0_uku = L0_ex.OR.L0_irad
C TELEGKA5_HANDLER.fmg( 173, 384):���
      if(L0_uku) then
         I0_evu=I0_alu
      else
         I0_evu=I0_omu
      endif
C TELEGKA5_HANDLER.fmg( 174, 396):���� RE IN LO CH7
      if(L_umad) then
         I_utu=I0_avu
      else
         I_utu=I0_evu
      endif
C TELEGKA5_HANDLER.fmg( 194, 396):���� RE IN LO CH7
      L0_ekad = L0_atad.OR.L0_irad
C TELEGKA5_HANDLER.fmg( 249, 392):���
      L0_ikad = L0_ekad.AND.(.NOT.L_ofad)
C TELEGKA5_HANDLER.fmg( 264, 392):�
      if(L0_ikad) then
         R0_elad=R_ukad
      else
         R0_elad=R0_okad
      endif
C TELEGKA5_HANDLER.fmg( 267, 399):���� RE IN LO CH7
      if(L0_esad) then
         I_ixo=I0_oxo
      else
         I_ixo=I0_uxo
      endif
C TELEGKA5_HANDLER.fmg( 143, 350):���� RE IN LO CH7
      if(L0_esad) then
         I_osu=I0_usu
      else
         I_osu=I0_atu
      endif
C TELEGKA5_HANDLER.fmg( 119, 365):���� RE IN LO CH7
      if(L0_atad) then
         I_adu=I0_obu
      else
         I_adu=I0_ubu
      endif
C TELEGKA5_HANDLER.fmg( 190, 418):���� RE IN LO CH7
      L0_elu = L0_ux.OR.L0_atad
C TELEGKA5_HANDLER.fmg( 167, 435):���
      if(L0_avad) then
         I_etu=I0_itu
      else
         I_etu=I0_otu
      endif
C TELEGKA5_HANDLER.fmg( 126, 454):���� RE IN LO CH7
      if(L0_avad) then
         I_abu=I0_ebu
      else
         I_abu=I0_ibu
      endif
C TELEGKA5_HANDLER.fmg( 122, 440):���� RE IN LO CH7
      L_idad = L_umad.OR.L0_edad.OR.L0_adad.OR.L0_ubad.OR.L0_obad
C TELEGKA5_HANDLER.fmg(  70, 403):���
      if(L_idad) then
         I_ivo=I0_eto
      else
         I_ivo=I0_ito
      endif
C TELEGKA5_HANDLER.fmg( 240, 481):���� RE IN LO CH7
      L0_oto = L_umad.OR.L_idad
C TELEGKA5_HANDLER.fmg( 270, 472):���
      if(L0_oto) then
         I_uto=I0_avo
      else
         I_uto=I0_evo
      endif
C TELEGKA5_HANDLER.fmg( 275, 480):���� RE IN LO CH7
      if(L_umad) then
         I_eru=I0_iru
      else
         I_eru=I0_oru
      endif
C TELEGKA5_HANDLER.fmg( 150, 372):���� RE IN LO CH7
      L_ebi = L0_uxe.OR.L_ixe
C TELEGKA5_HANDLER.fmg( 288, 287):���
      R_am=R0_ar
C TELEGKA5_HANDLER.fmg( 319, 403):����������� ��
      R_ul=R0_ar
C TELEGKA5_HANDLER.fmg( 349, 414):����������� ��
      if(L_ati) then
         R0_ap=R0_ep
      else
         R0_ap=R0_ot
      endif
C TELEGKA5_HANDLER.fmg( 387, 406):���� RE IN LO CH7
      R_il=R0_ap
C TELEGKA5_HANDLER.fmg( 386, 412):����������� ��
      R_ol=R0_ap
C TELEGKA5_HANDLER.fmg( 394, 406):����������� ��
      L0_olu = L_opad.AND.(.NOT.L0_ex)
C TELEGKA5_HANDLER.fmg( 153, 447):�
      if(L0_olu) then
         I0_ulu=I0_amu
      else
         I0_ulu=I0_emu
      endif
C TELEGKA5_HANDLER.fmg( 156, 452):���� RE IN LO CH7
      if(L0_elu) then
         I0_ipad=I0_ilu
      else
         I0_ipad=I0_ulu
      endif
C TELEGKA5_HANDLER.fmg( 169, 452):���� RE IN LO CH7
      if(L_umad) then
         I_apad=I0_epad
      else
         I_apad=I0_ipad
      endif
C TELEGKA5_HANDLER.fmg( 187, 450):���� RE IN LO CH7
      R0_alad = R8_ilad
C TELEGKA5_HANDLER.fmg( 262, 408):��������
C label 658  try658=try658-1
      R8_ilad = R0_elad + R0_alad
C TELEGKA5_HANDLER.fmg( 273, 408):��������
C sav1=R0_alad
      R0_alad = R8_ilad
C TELEGKA5_HANDLER.fmg( 262, 408):recalc:��������
C if(sav1.ne.R0_alad .and. try658.gt.0) goto 658
      End
