      Interface
      Subroutine LODOCHKA_HANDLER_FDA50_COORD(ext_deltat,L_e
     &,L_o,L_ad,I_af,I_if,I_ek,I_ok,I_il,I_ul,I_om,I_ap,I_up
     &,I_er,I_as,I_is,I_et,I_ot,I_iv,I_uv,I_ax,I_ex,I_abe
     &,I_ibe,I_ede,I_ode,I_ife,I_ufe,I_oke,I_ale,I_ule,I_eme
     &,I_ape,I_ipe,I_ere,I_ore,I_ise,I_use,I_ote,I_ave,I_uve
     &,I_exe,I_abi,I_ibi,I_edi,I_odi,I_ifi,I_ufi,I_oki,I_ali
     &,I_uli,I_emi,I_api,I_ipi,I_eri,I_ori,I_isi,I_usi,I_oti
     &,I_avi,I_uvi,I_exi,I_ebo,I_edo,I_afo,I_ifo,I_eko,I_oko
     &,I_ilo,I_ulo,I_omo,I_apo,I_upo,I_ero,I_aso,I_iso,I_eto
     &,I_oto,I_ivo,I_uvo,I_oxo,I_abu,I_ubu,I_edu,I_afu,I_ifu
     &,I_eku,I_oku,I_ilu,I_ulu,I_omu,I_apu,I_upu,I_eru,I_asu
     &,I_isu,I_etu,I_otu,I_ivu,I_uvu,I_oxu,I_abad,I_ubad,I_edad
     &,I_afad,I_ifad,I_ekad,I_okad,I_ilad,I_ulad,I_omad,I_apad
     &,I_upad,I_erad,I_asad,I_isad,I_etad,I_otad,I_ivad,I_uvad
     &,I_oxad,I_abed,I_ubed,I_eded,I_afed,I_ifed,I_eked,I_oked
     &,I_iled,I_uled,I_omed,I_aped,I_uped,I_ered,I_ased,I_ised
     &,I_eted,I_oted,I_ived,I_uved,I_oxed,I_abid,I_ubid,I_edid
     &,I_afid,I_ifid,I_ekid,I_okid,I_ilid,I_ulid,I_omid,I_apid
     &,I_upid,I_erid,I_asid,I_isid,I_etid,I_otid,I_ivid,I_uvid
     &,I_oxid,I_abod,I_ubod,I_edod,I_idod,I_afod,I_efod,I_ofod
     &,I_ufod,I_akod,I_ekod,L_ikod,L_akik,L_ekik,L_atik,L_etik
     &,L_itik,L_otik,L_utik,L_avik,L_evik,L_ivik,L_ovik,L_uvik
     &,L_axik,L_exik,L_ixik,L_oxik,L_uxik,L_abok,L_ebok,L_ibok
     &,L_odok,I_ofok)
C |L_e           |1 1 I|LODOCHKA_FREE_STATE|��������� ������� � ��������� "������ �������"||
C |L_o           |1 1 I|LODOCHKA_KTS_STATE|��������� ������� � ��������� "��������� �����"||
C |L_ad          |1 1 I|LODOCHKA_PRESS_STATE|��������� ������� � ��������� "������������ �����"||
C |I_af          |2 4 O|CIL_FDA50_80    |�������������� � �������������||
C |I_if          |2 4 K|_lcmpJ25649     |�������� ������ �����������|5080|
C |I_ek          |2 4 O|CIL_FDA50_79    |�������������� � �������������||
C |I_ok          |2 4 K|_lcmpJ25622     |�������� ������ �����������|5079|
C |I_il          |2 4 O|CIL_FDA50_78    |�������������� � �������������||
C |I_ul          |2 4 K|_lcmpJ25595     |�������� ������ �����������|5078|
C |I_om          |2 4 O|CIL_FDA50_77    |�������������� � �������������||
C |I_ap          |2 4 K|_lcmpJ25568     |�������� ������ �����������|5077|
C |I_up          |2 4 O|CIL_FDA50_76    |�������������� � �������������||
C |I_er          |2 4 K|_lcmpJ25541     |�������� ������ �����������|5076|
C |I_as          |2 4 O|CIL_FDA50_75    |�������������� � �������������||
C |I_is          |2 4 K|_lcmpJ25514     |�������� ������ �����������|5075|
C |I_et          |2 4 O|CIL_FDA50_74    |�������������� � �������������||
C |I_ot          |2 4 K|_lcmpJ25487     |�������� ������ �����������|5074|
C |I_iv          |2 4 O|CIL_FDA50_73    |�������������� � �������������||
C |I_uv          |2 4 K|_lcmpJ25460     |�������� ������ �����������|5073|
C |I_ax          |2 4 K|_lcmpJ25303     |�������� ������ �����������|5029|
C |I_ex          |2 4 K|_lcmpJ25302     |�������� ������ �����������|5028|
C |I_abe         |2 4 O|CIL_FDA50_49    |�������������� � �������������||
C |I_ibe         |2 4 K|_lcmpJ25242     |�������� ������ �����������|5049|
C |I_ede         |2 4 O|CIL_FDA50_48    |�������������� � �������������||
C |I_ode         |2 4 K|_lcmpJ25215     |�������� ������ �����������|5048|
C |I_ife         |2 4 O|CIL_FDA50_47    |�������������� � �������������||
C |I_ufe         |2 4 K|_lcmpJ25188     |�������� ������ �����������|5047|
C |I_oke         |2 4 O|CIL_FDA50_46    |�������������� � �������������||
C |I_ale         |2 4 K|_lcmpJ25161     |�������� ������ �����������|5046|
C |I_ule         |2 4 O|CIL_FDA50_45    |�������������� � �������������||
C |I_eme         |2 4 K|_lcmpJ25134     |�������� ������ �����������|5045|
C |I_ape         |2 4 O|CIL_FDA50_44    |�������������� � �������������||
C |I_ipe         |2 4 K|_lcmpJ25107     |�������� ������ �����������|5044|
C |I_ere         |2 4 O|CIL_FDA50_43    |�������������� � �������������||
C |I_ore         |2 4 K|_lcmpJ25080     |�������� ������ �����������|5043|
C |I_ise         |2 4 O|CIL_FDA50_42    |�������������� � �������������||
C |I_use         |2 4 K|_lcmpJ25053     |�������� ������ �����������|5042|
C |I_ote         |2 4 O|CIL_FDA50_41    |�������������� � �������������||
C |I_ave         |2 4 K|_lcmpJ25026     |�������� ������ �����������|5041|
C |I_uve         |2 4 O|CIL_FDA50_40    |�������������� � �������������||
C |I_exe         |2 4 K|_lcmpJ24999     |�������� ������ �����������|5040|
C |I_abi         |2 4 O|CIL_FDA50_39    |�������������� � �������������||
C |I_ibi         |2 4 K|_lcmpJ24972     |�������� ������ �����������|5039|
C |I_edi         |2 4 O|CIL_FDA50_38    |�������������� � �������������||
C |I_odi         |2 4 K|_lcmpJ24945     |�������� ������ �����������|5038|
C |I_ifi         |2 4 O|CIL_FDA50_37    |�������������� � �������������||
C |I_ufi         |2 4 K|_lcmpJ24918     |�������� ������ �����������|5037|
C |I_oki         |2 4 O|CIL_FDA50_36    |�������������� � �������������||
C |I_ali         |2 4 K|_lcmpJ24891     |�������� ������ �����������|5036|
C |I_uli         |2 4 O|CIL_FDA50_35    |�������������� � �������������||
C |I_emi         |2 4 K|_lcmpJ24864     |�������� ������ �����������|5035|
C |I_api         |2 4 O|CIL_FDA50_34    |�������������� � �������������||
C |I_ipi         |2 4 K|_lcmpJ24837     |�������� ������ �����������|5034|
C |I_eri         |2 4 O|CIL_FDA50_33    |�������������� � �������������||
C |I_ori         |2 4 K|_lcmpJ24810     |�������� ������ �����������|5033|
C |I_isi         |2 4 O|CIL_FDA50_32    |�������������� � �������������||
C |I_usi         |2 4 K|_lcmpJ24783     |�������� ������ �����������|5032|
C |I_oti         |2 4 O|CIL_FDA50_31    |�������������� � �������������||
C |I_avi         |2 4 K|_lcmpJ24756     |�������� ������ �����������|5031|
C |I_uvi         |2 4 O|CIL_FDA50_30    |�������������� � �������������||
C |I_exi         |2 4 K|_lcmpJ24729     |�������� ������ �����������|5030|
C |I_ebo         |2 4 O|CIL_FDA50_29    |�������������� � �������������||
C |I_edo         |2 4 O|CIL_FDA50_28    |�������������� � �������������||
C |I_afo         |2 4 O|CIL_FDA50_27    |�������������� � �������������||
C |I_ifo         |2 4 K|_lcmpJ24648     |�������� ������ �����������|5027|
C |I_eko         |2 4 O|CIL_FDA50_26    |�������������� � �������������||
C |I_oko         |2 4 K|_lcmpJ24621     |�������� ������ �����������|5026|
C |I_ilo         |2 4 O|CIL_FDA50_53    |�������������� � �������������||
C |I_ulo         |2 4 K|_lcmpJ24567     |�������� ������ �����������|5053|
C |I_omo         |2 4 O|CIL_FDA50_52    |�������������� � �������������||
C |I_apo         |2 4 K|_lcmpJ24540     |�������� ������ �����������|5052|
C |I_upo         |2 4 O|CIL_FDA50_51    |�������������� � �������������||
C |I_ero         |2 4 K|_lcmpJ24513     |�������� ������ �����������|5051|
C |I_aso         |2 4 O|CIL_FDA50_50    |�������������� � �������������||
C |I_iso         |2 4 K|_lcmpJ24486     |�������� ������ �����������|5050|
C |I_eto         |2 4 O|CIL_FDA50_72    |�������������� � �������������||
C |I_oto         |2 4 K|_lcmpJ24459     |�������� ������ �����������|5072|
C |I_ivo         |2 4 O|CIL_FDA50_71    |�������������� � �������������||
C |I_uvo         |2 4 K|_lcmpJ24432     |�������� ������ �����������|5071|
C |I_oxo         |2 4 O|CIL_FDA50_70    |�������������� � �������������||
C |I_abu         |2 4 K|_lcmpJ24405     |�������� ������ �����������|5070|
C |I_ubu         |2 4 O|CIL_FDA50_69    |�������������� � �������������||
C |I_edu         |2 4 K|_lcmpJ24378     |�������� ������ �����������|5069|
C |I_afu         |2 4 O|CIL_FDA50_68    |�������������� � �������������||
C |I_ifu         |2 4 K|_lcmpJ24351     |�������� ������ �����������|5068|
C |I_eku         |2 4 O|CIL_FDA50_67    |�������������� � �������������||
C |I_oku         |2 4 K|_lcmpJ24324     |�������� ������ �����������|5067|
C |I_ilu         |2 4 O|CIL_FDA50_66    |�������������� � �������������||
C |I_ulu         |2 4 K|_lcmpJ24297     |�������� ������ �����������|5066|
C |I_omu         |2 4 O|CIL_FDA50_65    |�������������� � �������������||
C |I_apu         |2 4 K|_lcmpJ24270     |�������� ������ �����������|5065|
C |I_upu         |2 4 O|CIL_FDA50_64    |�������������� � �������������||
C |I_eru         |2 4 K|_lcmpJ24243     |�������� ������ �����������|5064|
C |I_asu         |2 4 O|CIL_FDA50_63    |�������������� � �������������||
C |I_isu         |2 4 K|_lcmpJ24216     |�������� ������ �����������|5063|
C |I_etu         |2 4 O|CIL_FDA50_62    |�������������� � �������������||
C |I_otu         |2 4 K|_lcmpJ24189     |�������� ������ �����������|5062|
C |I_ivu         |2 4 O|CIL_FDA50_61    |�������������� � �������������||
C |I_uvu         |2 4 K|_lcmpJ24162     |�������� ������ �����������|5061|
C |I_oxu         |2 4 O|CIL_FDA50_60    |�������������� � �������������||
C |I_abad        |2 4 K|_lcmpJ24135     |�������� ������ �����������|5060|
C |I_ubad        |2 4 O|CIL_FDA50_59    |�������������� � �������������||
C |I_edad        |2 4 K|_lcmpJ24108     |�������� ������ �����������|5059|
C |I_afad        |2 4 O|CIL_FDA50_58    |�������������� � �������������||
C |I_ifad        |2 4 K|_lcmpJ24081     |�������� ������ �����������|5058|
C |I_ekad        |2 4 O|CIL_FDA50_57    |�������������� � �������������||
C |I_okad        |2 4 K|_lcmpJ24054     |�������� ������ �����������|5057|
C |I_ilad        |2 4 O|CIL_FDA50_56    |�������������� � �������������||
C |I_ulad        |2 4 K|_lcmpJ24027     |�������� ������ �����������|5056|
C |I_omad        |2 4 O|CIL_FDA50_55    |�������������� � �������������||
C |I_apad        |2 4 K|_lcmpJ24000     |�������� ������ �����������|5055|
C |I_upad        |2 4 O|CIL_FDA50_54    |�������������� � �������������||
C |I_erad        |2 4 K|_lcmpJ23973     |�������� ������ �����������|5054|
C |I_asad        |2 4 O|CIL_FDA50_25    |�������������� � �������������||
C |I_isad        |2 4 K|_lcmpJ23946     |�������� ������ �����������|5025|
C |I_etad        |2 4 O|CIL_FDA50_24    |�������������� � �������������||
C |I_otad        |2 4 K|_lcmpJ23919     |�������� ������ �����������|5024|
C |I_ivad        |2 4 O|CIL_FDA50_23    |�������������� � �������������||
C |I_uvad        |2 4 K|_lcmpJ23892     |�������� ������ �����������|5023|
C |I_oxad        |2 4 O|CIL_FDA50_22    |�������������� � �������������||
C |I_abed        |2 4 K|_lcmpJ23865     |�������� ������ �����������|5022|
C |I_ubed        |2 4 O|CIL_FDA50_21    |�������������� � �������������||
C |I_eded        |2 4 K|_lcmpJ23838     |�������� ������ �����������|5021|
C |I_afed        |2 4 O|CIL_FDA50_20    |�������������� � �������������||
C |I_ifed        |2 4 K|_lcmpJ23811     |�������� ������ �����������|5020|
C |I_eked        |2 4 O|CIL_FDA50_19    |�������������� � �������������||
C |I_oked        |2 4 K|_lcmpJ23784     |�������� ������ �����������|5019|
C |I_iled        |2 4 O|CIL_FDA50_18    |�������������� � �������������||
C |I_uled        |2 4 K|_lcmpJ23757     |�������� ������ �����������|5018|
C |I_omed        |2 4 O|CIL_FDA50_17    |�������������� � �������������||
C |I_aped        |2 4 K|_lcmpJ23730     |�������� ������ �����������|5017|
C |I_uped        |2 4 O|CIL_FDA50_16    |�������������� � �������������||
C |I_ered        |2 4 K|_lcmpJ23703     |�������� ������ �����������|5016|
C |I_ased        |2 4 O|CIL_FDA50_15    |�������������� � �������������||
C |I_ised        |2 4 K|_lcmpJ23676     |�������� ������ �����������|5015|
C |I_eted        |2 4 O|CIL_FDA50_14    |�������������� � �������������||
C |I_oted        |2 4 K|_lcmpJ23649     |�������� ������ �����������|5014|
C |I_ived        |2 4 O|CIL_FDA50_13    |�������������� � �������������||
C |I_uved        |2 4 K|_lcmpJ23622     |�������� ������ �����������|5013|
C |I_oxed        |2 4 O|CIL_FDA50_12    |�������������� � �������������||
C |I_abid        |2 4 K|_lcmpJ23595     |�������� ������ �����������|5012|
C |I_ubid        |2 4 O|CIL_FDA50_11    |�������������� � �������������||
C |I_edid        |2 4 K|_lcmpJ23568     |�������� ������ �����������|5011|
C |I_afid        |2 4 O|CIL_FDA50_10    |�������������� � �������������||
C |I_ifid        |2 4 K|_lcmpJ23541     |�������� ������ �����������|5010|
C |I_ekid        |2 4 O|CIL_FDA50_9     |�������������� � �������������||
C |I_okid        |2 4 K|_lcmpJ23514     |�������� ������ �����������|5009|
C |I_ilid        |2 4 O|CIL_FDA50_8     |�������������� � �������������||
C |I_ulid        |2 4 K|_lcmpJ23487     |�������� ������ �����������|5008|
C |I_omid        |2 4 O|CIL_FDA50_7     |�������������� � �������������||
C |I_apid        |2 4 K|_lcmpJ23460     |�������� ������ �����������|5007|
C |I_upid        |2 4 O|CIL_FDA50_6     |�������������� � �������������||
C |I_erid        |2 4 K|_lcmpJ23433     |�������� ������ �����������|5006|
C |I_asid        |2 4 O|CIL_FDA50_5     |�������������� � �������������||
C |I_isid        |2 4 K|_lcmpJ23406     |�������� ������ �����������|5005|
C |I_etid        |2 4 O|CIL_FDA50_4     |�������������� � �������������||
C |I_otid        |2 4 K|_lcmpJ23379     |�������� ������ �����������|5004|
C |I_ivid        |2 4 O|CIL_FDA50_3     |�������������� � �������������||
C |I_uvid        |2 4 K|_lcmpJ23324     |�������� ������ �����������|5003|
C |I_oxid        |2 4 O|CIL_FDA50_2     |�������������� � �������������||
C |I_abod        |2 4 K|_lcmpJ23297     |�������� ������ �����������|5002|
C |I_ubod        |2 4 K|_lcmpJ23239     |�������� ������ �����������|2|
C |I_edod        |2 4 K|_lcmpJ23238     |�������� ������ �����������|1|
C |I_idod        |2 4 K|_lcmpJ23236     |�������� ������ �����������|0|
C |I_afod        |2 4 O|CIL_FDA50_1     |�������������� � �������������||
C |I_efod        |2 4 O|STATE           |��������� �������||
C |I_ofod        |2 4 K|_lcmpJ23124     |�������� ������ �����������|5001|
C |I_ufod        |2 4 K|_colJ20190*     |�������� ��������� ����� |16#0100000A|
C |I_akod        |2 4 K|_colJ20189*     |�������� ��������� ����� |16#01000007|
C |I_ekod        |2 4 K|_colJ20188*     |�������� ��������� ����� |16#01000010|
C |L_ikod        |1 1 I|FDA52AE401_CATCH2|������� ��������� �� (������ ��������)||
C |L_akik        |1 1 I|FDA52AE501KE01_pos9|�������, ������� 9||
C |L_ekik        |1 1 I|FDA52AE501KE01_pos10|�������, ������� 10||
C |L_atik        |1 1 I|FDA50_ST_CATCH  |������� �� �������� � ����||
C |L_etik        |1 1 I|FDA52AE501KE01_pos8|�������, ������� 8||
C |L_itik        |1 1 I|FDA52AE501KE01_pos7|�������, ������� 7||
C |L_otik        |1 1 I|FDA52AE501KE02_pos1|�����������, ������� 1||
C |L_utik        |1 1 I|FDA52AE501KE02_pos8|�����������, ������� 8||
C |L_avik        |1 1 I|FDA52AE501KE02_pos7|�����������, ������� 7||
C |L_evik        |1 1 I|FDA52AE501KE02_pos6|�����������, ������� 6||
C |L_ivik        |1 1 I|FDA52AE501KE02_pos5|�����������, ������� 5||
C |L_ovik        |1 1 I|FDA52AE501KE02_pos4|�����������, ������� 4||
C |L_uvik        |1 1 I|FDA52AE501KE02_pos3|�����������, ������� 3||
C |L_axik        |1 1 I|FDA52AE501KE02_pos2|�����������, ������� 2||
C |L_exik        |1 1 I|FDA52AE501KE01_pos6|�������, ������� 6||
C |L_ixik        |1 1 I|FDA52AE501KE01_pos5|�������, ������� 5||
C |L_oxik        |1 1 I|FDA52AE501KE01_pos4|�������, ������� 4||
C |L_uxik        |1 1 I|FDA52AE501KE01_pos3|�������, ������� 3||
C |L_abok        |1 1 I|FDA52AE501KE01_pos2|�������, ������� 2||
C |L_ebok        |1 1 I|FDA52AE501KE01_pos1|�������, ������� 1||
C |L_ibok        |1 1 I|FDA52AE401_CATCH|������� ��������� ��||
C |L_odok        |1 1 I|FDA52AE501_CATCH|������� ��������� �������������||
C |I_ofok        |2 4 O|CR              |�������������� � �������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_o,L_ad
      INTEGER*4 I_af,I_if,I_ek,I_ok,I_il,I_ul,I_om,I_ap,I_up
     &,I_er,I_as,I_is,I_et,I_ot,I_iv,I_uv,I_ax,I_ex,I_abe
     &,I_ibe,I_ede,I_ode
      INTEGER*4 I_ife,I_ufe,I_oke,I_ale,I_ule,I_eme,I_ape
     &,I_ipe,I_ere,I_ore,I_ise,I_use,I_ote,I_ave,I_uve,I_exe
     &,I_abi,I_ibi,I_edi,I_odi,I_ifi,I_ufi
      INTEGER*4 I_oki,I_ali,I_uli,I_emi,I_api,I_ipi,I_eri
     &,I_ori,I_isi,I_usi,I_oti,I_avi,I_uvi,I_exi,I_ebo,I_edo
     &,I_afo,I_ifo,I_eko,I_oko,I_ilo,I_ulo
      INTEGER*4 I_omo,I_apo,I_upo,I_ero,I_aso,I_iso,I_eto
     &,I_oto,I_ivo,I_uvo,I_oxo,I_abu,I_ubu,I_edu,I_afu,I_ifu
     &,I_eku,I_oku,I_ilu,I_ulu,I_omu,I_apu
      INTEGER*4 I_upu,I_eru,I_asu,I_isu,I_etu,I_otu,I_ivu
     &,I_uvu,I_oxu,I_abad,I_ubad,I_edad,I_afad,I_ifad,I_ekad
     &,I_okad,I_ilad,I_ulad,I_omad,I_apad,I_upad,I_erad
      INTEGER*4 I_asad,I_isad,I_etad,I_otad,I_ivad,I_uvad
     &,I_oxad,I_abed,I_ubed,I_eded,I_afed,I_ifed,I_eked,I_oked
     &,I_iled,I_uled,I_omed,I_aped,I_uped,I_ered,I_ased,I_ised
      INTEGER*4 I_eted,I_oted,I_ived,I_uved,I_oxed,I_abid
     &,I_ubid,I_edid,I_afid,I_ifid,I_ekid,I_okid,I_ilid,I_ulid
     &,I_omid,I_apid,I_upid,I_erid,I_asid,I_isid,I_etid,I_otid
      INTEGER*4 I_ivid,I_uvid,I_oxid,I_abod,I_ubod,I_edod
     &,I_idod,I_afod,I_efod,I_ofod,I_ufod,I_akod,I_ekod
      LOGICAL*1 L_ikod,L_akik,L_ekik,L_atik,L_etik,L_itik
     &,L_otik,L_utik,L_avik,L_evik,L_ivik,L_ovik,L_uvik,L_axik
     &,L_exik,L_ixik,L_oxik,L_uxik,L_abok,L_ebok,L_ibok,L_odok
      INTEGER*4 I_ofok
      End subroutine LODOCHKA_HANDLER_FDA50_COORD
      End interface
