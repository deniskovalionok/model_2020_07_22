      Interface
      Subroutine TELEGKA_HANDLER(ext_deltat,R_ake,R8_ufu,R_u
     &,L_ad,R_id,L_od,L_uf,L_ik,R_er,L_or,R_as,L_is,L_ot,L_ov
     &,L_ax,L_ex,L_ox,L_ebe,L_ube,L_ede,L_oke,L_ale,L_epe
     &,L_ipe,L_are,L_ise,L_ose,L_use,L_ate,L_ete,L_ite,L_ote
     &,L_ute,L_ave,L_eve,L_ive,L_ove,L_uve,R_ebi,R_obi,I_idi
     &,I_afi,R_efi,R_ifi,R_ofi,R_ufi,I_aki,I_oki,I_oli,I_emi
     &,C20_epi,C20_eri,C8_esi,R_uvi,R_axi,L_exi,R_ixi,R_oxi
     &,I_uxi,R_obo,R_ado,I_edo,I_udo,I_ifo,L_ako,L_iko,L_oko
     &,L_elo,L_olo,L_ulo,L_emo,L_omo,L_umo,R_ipo,L_iro,L_oro
     &,L_eso,L_iso,L_ato,L_eto,R8_oto,R_ivo,R8_axo,L_exo,L_ixo
     &,L_ibu,I_obu,L_edu,L_idu,R_ifu,L_umu,R_oru,R_usu,L_evu
     &,L_exu,L_abad,R_adad,L_edad,L_idad,L_odad,L_udad,L_afad
     &,L_efad)
C |R_ake         |4 4 I|11 mlfpar19     ||0.6|
C |R8_ufu        |4 8 O|16 value        |��� �������� ��������|0.5|
C |R_u           |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ad          |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_id          |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_od          |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_uf          |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ik          |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_er          |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_or          |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_as          |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_is          |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ot          |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ov          |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ax          |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ex          |1 1 O|OUTC            |�����||
C |L_ox          |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ube         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_ede         |1 1 O|OUTO            |������||
C |L_oke         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_ale         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_epe         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ipe         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_are         |1 1 O|flag_mlf19      |||
C |L_ise         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_ose         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_use         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ate         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_ete         |1 1 I|mlf07           |���������� ���� �������||
C |L_ite         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_ote         |1 1 I|mlf09           |����� ��������� �����||
C |L_ute         |1 1 I|mlf08           |����� ��������� ������||
C |L_ave         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_eve         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_ive         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_ove         |1 1 I|YA26�           |������� ������||
C |L_uve         |1 1 I|YA25�           |�������� �����||
C |R_ebi         |4 4 O|VX02            |�������� ����������� �������||
C |R_obi         |4 4 O|VX01            |��������� �������||
C |I_idi         |2 4 O|LREADY          |����� ����������||
C |I_afi         |2 4 O|LBUSY           |����� �����||
C |R_efi         |4 4 S|vmpos1_button_ST*|��������� ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_ifi         |4 4 I|vmpos1_button   |������� ������ ������ "������� � ��������� 1 �� ���������" |0.0|
C |R_ofi         |4 4 S|vmpos2_button_ST*|��������� ������ "������� � ��������� 2 �� ���������" |0.0|
C |R_ufi         |4 4 I|vmpos2_button   |������� ������ ������ "������� � ��������� 2 �� ���������" |0.0|
C |I_aki         |2 4 O|state1          |��������� 1||
C |I_oki         |2 4 O|state2          |��������� 2||
C |I_oli         |2 4 O|LPOS2O          |����� � ��������� 2||
C |I_emi         |2 4 O|LPOS1C          |����� � ��������� 1||
C |C20_epi       |3 20 O|task_state      |���������||
C |C20_eri       |3 20 O|task_name       |������� ���������||
C |C8_esi        |3 8 I|task            |������� ���������||
C |R_uvi         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_axi         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_exi         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ixi         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_oxi         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_uxi         |2 4 O|LERROR          |����� �������������||
C |R_obo         |4 4 O|POS_CL          |��������||
C |R_ado         |4 4 O|POS_OP          |��������||
C |I_edo         |2 4 O|LPOS1           |����� ��������� 1||
C |I_udo         |2 4 O|LPOS2           |����� ��������� 2||
C |I_ifo         |2 4 O|LZM             |������ "�������"||
C |L_ako         |1 1 I|vlv_kvit        |||
C |L_iko         |1 1 I|instr_fault     |||
C |L_oko         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_elo         |1 1 O|vmpos1_button_CMD*|[TF]����� ������ ������� � ��������� 1 �� ���������|F|
C |L_olo         |1 1 O|vmpos2_button_CMD*|[TF]����� ������ ������� � ��������� 2 �� ���������|F|
C |L_ulo         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_emo         |1 1 I|mlf05           |������������� ������� "������"||
C |L_omo         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_umo         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |R_ipo         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |L_iro         |1 1 O|block           |||
C |L_oro         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_eso         |1 1 O|STOP            |�������||
C |L_iso         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ato         |1 1 O|norm            |�����||
C |L_eto         |1 1 O|nopower         |��� ����������||
C |R8_oto        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ivo         |4 4 I|power           |�������� ��������||
C |R8_axo        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_exo         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ixo         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ibu         |1 1 O|fault           |�������������||
C |I_obu         |2 4 O|LAM             |������ "�������"||
C |L_edu         |1 1 O|XH53            |�� ����� (���)|F|
C |L_idu         |1 1 O|XH54            |�� ������ (���)|F|
C |R_ifu         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_umu         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_oru         |4 4 O|value_pos       |��� �������� ��������|0.5|
C |R_usu         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_evu         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_exu         |1 1 O|limit_switch_error|||
C |L_abad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |R_adad        |4 4 I|mlfpar19        |��������� ������������|0.6|
C |L_edad        |1 1 I|mlf24           |��� ���������� � ����������||
C |L_idad        |1 1 I|mlf23           |������� ������� �����||
C |L_odad        |1 1 I|mlf22           |����� ����� ��������||
C |L_udad        |1 1 I|mlf04           |���������������� �������� �����||
C |L_afad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_efad        |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_u
      LOGICAL*1 L_ad
      REAL*4 R_id
      LOGICAL*1 L_od,L_uf,L_ik
      REAL*4 R_er
      LOGICAL*1 L_or
      REAL*4 R_as
      LOGICAL*1 L_is,L_ot,L_ov,L_ax,L_ex,L_ox,L_ebe,L_ube
     &,L_ede
      REAL*4 R_ake
      LOGICAL*1 L_oke,L_ale,L_epe,L_ipe,L_are,L_ise,L_ose
     &,L_use,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive
     &,L_ove,L_uve
      REAL*4 R_ebi,R_obi
      INTEGER*4 I_idi,I_afi
      REAL*4 R_efi,R_ifi,R_ofi,R_ufi
      INTEGER*4 I_aki,I_oki,I_oli,I_emi
      CHARACTER*20 C20_epi,C20_eri
      CHARACTER*8 C8_esi
      REAL*4 R_uvi,R_axi
      LOGICAL*1 L_exi
      REAL*4 R_ixi,R_oxi
      INTEGER*4 I_uxi
      REAL*4 R_obo,R_ado
      INTEGER*4 I_edo,I_udo,I_ifo
      LOGICAL*1 L_ako,L_iko,L_oko,L_elo,L_olo,L_ulo,L_emo
     &,L_omo,L_umo
      REAL*4 R_ipo
      LOGICAL*1 L_iro,L_oro,L_eso,L_iso,L_ato,L_eto
      REAL*8 R8_oto
      REAL*4 R_ivo
      REAL*8 R8_axo
      LOGICAL*1 L_exo,L_ixo,L_ibu
      INTEGER*4 I_obu
      LOGICAL*1 L_edu,L_idu
      REAL*4 R_ifu
      REAL*8 R8_ufu
      LOGICAL*1 L_umu
      REAL*4 R_oru,R_usu
      LOGICAL*1 L_evu,L_exu,L_abad
      REAL*4 R_adad
      LOGICAL*1 L_edad,L_idad,L_odad,L_udad,L_afad,L_efad
      End subroutine TELEGKA_HANDLER
      End interface
