      Subroutine LODOCHKA_HANDLER_IT_MAIN(ext_deltat,R_utid
     &,R_ed,L_ef,L_al,L_el,L_ul,L_im,L_up,R_or,R_ur,L_as,L_ut
     &,L_ev,L_iv,R_ax,R_ex,L_ix,L_ux,R_ide,R_ode,L_ude,I_ufe
     &,I_oke,I_ile,I_eme,I_ape,I_epe,L_are,L_ote,L_eve,L_uve
     &,L_ixe,L_abi,L_obi,L_edi,L_ifi,L_eki,L_ali,I_oli,I_uli
     &,R_umi,R_upi,R_ari,R_asi,R_osi,R_iti,R_uvi,R_oxi,R_abo
     &,R_ebo,R_ado,R_ido,R_odo,L_udo,L_uko,L_alo,R_ilo,R_emo
     &,R_umo,R_opo,R_iro,R_eso,R_iso,R_avo,R_evo,R_exo,R_abu
     &,R_ibu,R_aku,R_eku,L_iku,R_elu,R_emu,R_epu,L_oru,R_uru
     &,R_osu,R_itu,R_otu,R_exu,R_ixu,L_oxu,R_ibad,R_idad,R_ifad
     &,R_okad,R_ilad,R_emad,R_imad,L_upad,L_erad,L_orad,R_urad
     &,L_asad,R_esad,R_utad,R_ovad,R_ixad,R_oxad,L_aded,L_oded
     &,L_afed,R_efed,L_ifed,R_ofed,R_iked,R_emed,R_eped,R_iped
     &,R_oped,L_ased,L_osed,L_used,L_eted,L_aved,L_ived,R_oxed
     &,R_uxed,R_ubid,R_adid,L_idid,R_ifid,I_usid,R_itid,R_otid
     &,R_ivid,R_uvid,C20_axid)
C |R_utid        |4 4 I|20FDB50AE400VX02|�������� �������� �������||
C |R_ed          |4 4 I|FDB40_BOAT_MASS |����� ����� � �����������||
C |L_ef          |1 1 I|FDB60_LOD_INPUT_GATEWAY_FROM|������� �� �������� �����||
C |L_al          |1 1 I|FDB60_LOD_TRANSFER_TABLE|������� �� ������������ �������||
C |L_el          |1 1 I|FDB60_LOD_MAIN_TROLLEY|������� �� �������||
C |L_ul          |1 1 I|FDB60_LOD_INPUT_GATEWAY|������� �� �������� �����||
C |L_im          |1 1 I|FDB60_LOD_VZV   |������� �� ������������||
C |L_up          |1 1 I|FDB60_LOD_VZVESH|������� �� ������������ �������������� �����||
C |R_or          |4 4 S|_simpJ10082*    |[���]���������� ��������� ������������� |0.0|
C |R_ur          |4 4 K|_timpJ10082     |[���]������������ �������� �������������|1.0|
C |L_as          |1 1 S|_limpJ10082*    |[TF]���������� ��������� ������������� |F|
C |L_ut          |1 1 I|FDB40_START     |������ ������� �����������||
C |L_ev          |1 1 I|FDB91EC001_INIT_COORD|������������� ���������� ������� ��� ��. �����������||
C |L_iv          |1 1 I|FDB91EC003_INIT_COORD|������������� ���������� ������� ��� ��. ���||
C |R_ax          |4 4 S|_simpJ10030*    |[���]���������� ��������� ������������� |0.0|
C |R_ex          |4 4 K|_timpJ10030     |[���]������������ �������� �������������|1.0|
C |L_ix          |1 1 S|_limpJ10030*    |[TF]���������� ��������� ������������� |F|
C |L_ux          |1 1 I|FDB91EC005_INIT_COORD|������������� ���������� ������� ��� ��. ���������||
C |R_ide         |4 4 S|_simpJ10001*    |[���]���������� ��������� ������������� |0.0|
C |R_ode         |4 4 K|_timpJ10001     |[���]������������ �������� �������������|1.0|
C |L_ude         |1 1 S|_limpJ10001*    |[TF]���������� ��������� ������������� |F|
C |I_ufe         |2 4 K|_lcmpJ9990      |�������� ������ �����������|5|
C |I_oke         |2 4 K|_lcmpJ9978      |�������� ������ �����������|4|
C |I_ile         |2 4 K|_lcmpJ9966      |�������� ������ �����������|3|
C |I_eme         |2 4 K|_lcmpJ9954      |�������� ������ �����������|2|
C |I_ape         |2 4 K|_lcmpJ9942      |�������� ������ �����������|1|
C |I_epe         |2 4 I|FDB60AE408_NUM_BOAT|����� ��������� ������� �� �������||
C |L_are         |1 1 I|FDB60AE408_CATCH|������ ������� 5��� �������� 20FDB60AE408||
C |L_ote         |1 1 I|FDB61AE401_UNLOAD5|������ ������� ����� 20FDB61AE401||
C |L_eve         |1 1 I|FDB61AE401_UNLOAD4|������ ������� ����� 20FDB61AE401||
C |L_uve         |1 1 I|FDB61AE401_UNLOAD3|������ ������� ����� 20FDB61AE401||
C |L_ixe         |1 1 I|FDB61AE401_UNLOAD2|������ ������� ����� 20FDB61AE401||
C |L_abi         |1 1 I|FDB61AE401_UNLOAD1|������ ������� ����� 20FDB61AE401||
C |L_obi         |1 1 I|FDB61AE401_CATCH5|������ ������� ����� 20FDB61AE401||
C |L_edi         |1 1 I|FDB61AE401_CATCH4|������ ������� ����� 20FDB61AE401||
C |L_ifi         |1 1 I|FDB61AE401_CATCH3|������ ������� ����� 20FDB61AE401||
C |L_eki         |1 1 I|FDB61AE401_CATCH2|������ ������� ����� 20FDB61AE401||
C |L_ali         |1 1 I|FDB61AE401_CATCH1|������ ������� ����� 20FDB61AE401||
C |I_oli         |2 4 O|CR              |�������������� � �������������||
C |I_uli         |2 4 O|LP              |����� �� ������� ����������� �������||
C |R_umi         |4 4 K|_lcmpJ9186      |[]�������� ������ �����������|20|
C |R_upi         |4 4 I|FDB60AE500AVY01 |�������� ��������� 20FDB60AE500A||
C |R_ari         |4 4 K|_lcmpJ9173      |[]�������� ������ �����������|20|
C |R_asi         |4 4 I|FDB60AE500BVY01 |�������� ��������� 20FDB60AE500B||
C |R_osi         |4 4 I|FDB60AE500�VY01 |�������� ��������� 20FDB60AE500�||
C |R_iti         |4 4 K|_lcmpJ9156      |[]�������� ������ �����������|20|
C |R_uvi         |4 4 K|_lcmpJ9127      |[]�������� ������ �����������|20|
C |R_oxi         |4 4 K|_lcmpJ9057      |[]�������� ������ �����������|2|
C |R_abo         |4 4 I|FDB60AE403_VZ01 |�������� �������������� �������||
C |R_ebo         |4 4 K|_lcmpJ9051      |[]�������� ������ �����������|20|
C |R_ado         |4 4 I|FDB60AE403_POS  |��������� ������� �� ��� X||
C |R_ido         |4 4 S|_simpJ9036*     |[���]���������� ��������� ������������� |0.0|
C |R_odo         |4 4 K|_timpJ9036      |[���]������������ �������� �������������|1.0|
C |L_udo         |1 1 S|_limpJ9036*     |[TF]���������� ��������� ������������� |F|
C |L_uko         |1 1 S|_splsJ8856*     |[TF]���������� ��������� ������������� |F|
C |L_alo         |1 1 I|FDB40_HAVE_DELTA_MASS|���� ��������� ����� �������||
C |R_ilo         |4 4 I|FDB40_DELTA_MASS|��������� ����� �������||
C |R_emo         |4 4 O|MASS            |����� ����� ����������� � �������||
C |R_umo         |4 4 K|_lcmpJ8516      |[]�������� ������ �����������|20|
C |R_opo         |4 4 K|_lcmpJ8503      |[]�������� ������ �����������|20|
C |R_iro         |4 4 K|_lcmpJ8495      |[]�������� ������ �����������|20|
C |R_eso         |4 4 I|FDB91AE001KE01_POS|��������� �1||
C |R_iso         |4 4 K|_lcmpJ8485      |[]�������� ������ �����������|20|
C |R_avo         |4 4 I|FDB91AE001KE01_V01|������� �1 �� ��� X||
C |R_evo         |4 4 I|FDB91AE003KE01_V01|�������� 20FDB91AE003KE01||
C |R_exo         |4 4 K|_lcmpJ8465      |[]�������� ������ �����������|20|
C |R_abu         |4 4 I|FDB91AE003KE01_POS|��������� �� 20FDB91AE003KE01||
C |R_ibu         |4 4 K|_lcmpJ8453      |[]�������� ������ �����������|20|
C |R_aku         |4 4 S|_simpJ8368*     |[���]���������� ��������� ������������� |0.0|
C |R_eku         |4 4 K|_timpJ8368      |[���]������������ �������� �������������|1.0|
C |L_iku         |1 1 S|_limpJ8368*     |[TF]���������� ��������� ������������� |F|
C |R_elu         |4 4 K|_lcmpJ8358      |[]�������� ������ �����������|20|
C |R_emu         |4 4 K|_lcmpJ8350      |[]�������� ������ �����������|20|
C |R_epu         |4 4 K|_lcmpJ8342      |[]�������� ������ �����������|20|
C |L_oru         |1 1 O|FDB91AE012_CATCHED|������� ��������� �� 20FDB91AE012||
C |R_uru         |4 4 K|_lcmpJ8317      |[]�������� ������ �����������|20|
C |R_osu         |4 4 K|_lcmpJ8308      |[]�������� ������ �����������|20|
C |R_itu         |4 4 I|FDB91AE012_POS  |��������� �4||
C |R_otu         |4 4 K|_lcmpJ8297      |[]�������� ������ �����������|20|
C |R_exu         |4 4 S|_simpJ7966*     |[���]���������� ��������� ������������� |0.0|
C |R_ixu         |4 4 K|_timpJ7966      |[���]������������ �������� �������������|1.0|
C |L_oxu         |1 1 S|_limpJ7966*     |[TF]���������� ��������� ������������� |F|
C |R_ibad        |4 4 K|_lcmpJ7929      |[]�������� ������ �����������|20|
C |R_idad        |4 4 K|_lcmpJ7921      |[]�������� ������ �����������|20|
C |R_ifad        |4 4 K|_lcmpJ7913      |[]�������� ������ �����������|20|
C |R_okad        |4 4 K|_lcmpJ7509      |[]�������� ������ �����������|20|
C |R_ilad        |4 4 K|_lcmpJ7500      |[]�������� ������ �����������|20|
C |R_emad        |4 4 I|FDB91AE007_POS  |��������� �4||
C |R_imad        |4 4 K|_lcmpJ7487      |[]�������� ������ �����������|20|
C |L_upad        |1 1 O|FDB91AE007_CATCHED|������� ��������� �4||
C |L_erad        |1 1 S|_qffJ7476*      |�������� ������ Q RS-��������  |F|
C |L_orad        |1 1 I|FDB91AE007_UNCATCH|��������� ������� �4||
C |R_urad        |4 4 I|FDB91AE007_VZ01 |������� �4 �� ��� Z||
C |L_asad        |1 1 I|FDB91AE007_CATCH|������ ������� �4||
C |R_esad        |4 4 K|_lcmpJ7170      |[]�������� ������ �����������|20|
C |R_utad        |4 4 K|_lcmpJ6848      |[]�������� ������ �����������|20|
C |R_ovad        |4 4 K|_lcmpJ6839      |[]�������� ������ �����������|20|
C |R_ixad        |4 4 I|FDB91AE006_POS  |��������� �� 20FDB91AE006||
C |R_oxad        |4 4 K|_lcmpJ6826      |[]�������� ������ �����������|20|
C |L_aded        |1 1 O|FDB91AE006_CATCHED|������� ��������� �� 20FDB91AE006||
C |L_oded        |1 1 S|_qffJ6815*      |�������� ������ Q RS-��������  |F|
C |L_afed        |1 1 I|FDB91AE006_UNCATCH|��������� ������� �� 20FDB91AE006||
C |R_efed        |4 4 I|FDB91AE006_VZ01 |������� �� �� ��� Z||
C |L_ifed        |1 1 I|FDB91AE006_CATCH|������ ������� �� 20FDB91AE006||
C |R_ofed        |4 4 K|_lcmpJ6805      |[]�������� ������ �����������|20|
C |R_iked        |4 4 K|_lcmpJ6796      |[]�������� ������ �����������|20|
C |R_emed        |4 4 K|_lcmpJ6476      |[]�������� ������ �����������|20|
C |R_eped        |4 4 K|_lcmpJ6463      |[]�������� ������ �����������|1000|
C |R_iped        |4 4 I|FDB91AE014_POS  |��������� �� 20FDB91AE014||
C |R_oped        |4 4 K|_lcmpJ6181      |[]�������� ������ �����������|20|
C |L_ased        |1 1 O|FDB91AE014_CATCHED|������� ��������� �� 20FDB91AE014||
C |L_osed        |1 1 I|FDB91AE012_UNCATCH|��������� ������� �� 20FDB91AE012||
C |L_used        |1 1 I|FDB91AE012_CATCH|������ ������� �� 20FDB91AE012||
C |L_eted        |1 1 S|_qffJ6163*      |�������� ������ Q RS-��������  |F|
C |L_aved        |1 1 S|_qffJ6151*      |�������� ������ Q RS-��������  |F|
C |L_ived        |1 1 I|FDB91AE014_UNCATCH|��������� ������� �� 20FDB91AE014||
C |R_oxed        |4 4 O|VZ01            |��������� ������� �� ��� Z||
C |R_uxed        |4 4 S|_ointJ6126*     |����� ����������� |0.0|
C |R_ubid        |4 4 I|FDB91AE012_VZ01 |������� �� �� ��� Z||
C |R_adid        |4 4 I|FDB91AE014_VZ01 |������� �� �� ��� Z||
C |L_idid        |1 1 I|FDB91AE014_CATCH|������ ������� �� 20FDB91AE014||
C |R_ifid        |4 4 S|_ointJ5958*     |����� ����������� |`p_LOAD_Y`|
C |I_usid        |2 4 O|LINE_POS        |����� �� ������� ����������� �������||
C |R_itid        |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_otid        |4 4 S|_ointJ5909*     |����� ����������� |`p_LOAD_X`|
C |R_ivid        |4 4 K|_lcmpJ871       |[]�������� ������ �����������|1.0|
C |R_uvid        |4 4 O|VY01            |��������� ������� �� ��� Y||
C |C20_axid      |3 20 O|SECTION         |������� ������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e
      REAL*4 R0_i
      INTEGER*4 I0_o,I0_u
      LOGICAL*1 L0_ad
      REAL*4 R_ed
      LOGICAL*1 L0_id
      INTEGER*4 I0_od,I0_ud
      LOGICAL*1 L0_af,L_ef,L0_if
      INTEGER*4 I0_of
      LOGICAL*1 L0_uf
      INTEGER*4 I0_ak
      LOGICAL*1 L0_ek
      INTEGER*4 I0_ik
      LOGICAL*1 L0_ok
      INTEGER*4 I0_uk
      LOGICAL*1 L_al,L_el
      INTEGER*4 I0_il
      LOGICAL*1 L0_ol,L_ul
      INTEGER*4 I0_am
      LOGICAL*1 L0_em,L_im
      INTEGER*4 I0_om
      LOGICAL*1 L0_um,L0_ap
      INTEGER*4 I0_ep,I0_ip
      LOGICAL*1 L0_op,L_up,L0_ar
      INTEGER*4 I0_er
      REAL*4 R0_ir,R_or,R_ur
      LOGICAL*1 L_as
      INTEGER*4 I0_es
      REAL*4 R0_is,R0_os
      LOGICAL*1 L0_us
      REAL*4 R0_at
      LOGICAL*1 L0_et,L0_it
      INTEGER*4 I0_ot
      LOGICAL*1 L_ut
      INTEGER*4 I0_av
      LOGICAL*1 L_ev,L_iv,L0_ov
      REAL*4 R0_uv,R_ax,R_ex
      LOGICAL*1 L_ix,L0_ox,L_ux,L0_abe
      INTEGER*4 I0_ebe
      REAL*4 R0_ibe,R0_obe
      LOGICAL*1 L0_ube
      REAL*4 R0_ade,R0_ede,R_ide,R_ode
      LOGICAL*1 L_ude,L0_afe
      INTEGER*4 I0_efe
      LOGICAL*1 L0_ife,L0_ofe
      INTEGER*4 I_ufe,I0_ake
      LOGICAL*1 L0_eke,L0_ike
      INTEGER*4 I_oke,I0_uke
      LOGICAL*1 L0_ale,L0_ele
      INTEGER*4 I_ile,I0_ole
      LOGICAL*1 L0_ule,L0_ame
      INTEGER*4 I_eme,I0_ime
      LOGICAL*1 L0_ome,L0_ume
      INTEGER*4 I_ape,I_epe
      LOGICAL*1 L0_ipe,L0_ope
      INTEGER*4 I0_upe
      LOGICAL*1 L_are
      INTEGER*4 I0_ere
      LOGICAL*1 L0_ire
      INTEGER*4 I0_ore
      LOGICAL*1 L0_ure
      INTEGER*4 I0_ase
      LOGICAL*1 L0_ese
      INTEGER*4 I0_ise
      LOGICAL*1 L0_ose,L0_use
      INTEGER*4 I0_ate
      LOGICAL*1 L0_ete
      INTEGER*4 I0_ite
      LOGICAL*1 L_ote,L0_ute
      INTEGER*4 I0_ave
      LOGICAL*1 L_eve,L0_ive
      INTEGER*4 I0_ove
      LOGICAL*1 L_uve,L0_axe
      INTEGER*4 I0_exe
      LOGICAL*1 L_ixe,L0_oxe
      INTEGER*4 I0_uxe
      LOGICAL*1 L_abi,L0_ebi
      INTEGER*4 I0_ibi
      LOGICAL*1 L_obi,L0_ubi
      INTEGER*4 I0_adi
      LOGICAL*1 L_edi,L0_idi,L0_odi,L0_udi,L0_afi
      INTEGER*4 I0_efi
      LOGICAL*1 L_ifi,L0_ofi,L0_ufi
      INTEGER*4 I0_aki
      LOGICAL*1 L_eki,L0_iki,L0_oki
      INTEGER*4 I0_uki
      LOGICAL*1 L_ali,L0_eli
      INTEGER*4 I0_ili,I_oli,I_uli
      LOGICAL*1 L0_ami
      INTEGER*4 I0_emi
      LOGICAL*1 L0_imi
      INTEGER*4 I0_omi
      REAL*4 R_umi,R0_api,R0_epi,R0_ipi
      LOGICAL*1 L0_opi
      REAL*4 R_upi,R_ari,R0_eri,R0_iri,R0_ori
      LOGICAL*1 L0_uri
      REAL*4 R_asi,R0_esi,R0_isi,R_osi,R0_usi,R0_ati
      LOGICAL*1 L0_eti
      REAL*4 R_iti,R0_oti,R0_uti,R0_avi,R0_evi,R0_ivi
      LOGICAL*1 L0_ovi
      REAL*4 R_uvi,R0_axi,R0_exi,R0_ixi,R_oxi,R0_uxi,R_abo
     &,R_ebo,R0_ibo,R0_obo,R0_ubo,R_ado,R0_edo,R_ido,R_odo
      LOGICAL*1 L_udo
      INTEGER*4 I0_afo
      REAL*4 R0_efo
      LOGICAL*1 L0_ifo
      REAL*4 R0_ofo
      LOGICAL*1 L0_ufo,L0_ako,L0_eko,L0_iko
      INTEGER*4 I0_oko
      LOGICAL*1 L_uko,L_alo,L0_elo
      REAL*4 R_ilo,R0_olo
      LOGICAL*1 L0_ulo
      REAL*4 R0_amo,R_emo
      LOGICAL*1 L0_imo
      INTEGER*4 I0_omo
      REAL*4 R_umo,R0_apo,R0_epo,R0_ipo,R_opo,R0_upo,R0_aro
     &,R0_ero,R_iro,R0_oro,R0_uro,R0_aso,R_eso,R_iso,R0_oso
     &,R0_uso
      LOGICAL*1 L0_ato,L0_eto,L0_ito
      REAL*4 R0_oto
      LOGICAL*1 L0_uto
      REAL*4 R_avo,R_evo,R0_ivo
      LOGICAL*1 L0_ovo,L0_uvo,L0_axo
      REAL*4 R_exo,R0_ixo,R0_oxo,R0_uxo,R_abu
      LOGICAL*1 L0_ebu
      REAL*4 R_ibu,R0_obu,R0_ubu,R0_adu,R0_edu
      LOGICAL*1 L0_idu
      INTEGER*4 I0_odu
      REAL*4 R0_udu,R0_afu
      LOGICAL*1 L0_efu
      INTEGER*4 I0_ifu,I0_ofu
      REAL*4 R0_ufu,R_aku,R_eku
      LOGICAL*1 L_iku,L0_oku,L0_uku,L0_alu
      REAL*4 R_elu,R0_ilu,R0_olu,R0_ulu
      LOGICAL*1 L0_amu
      REAL*4 R_emu,R0_imu,R0_omu,R0_umu
      LOGICAL*1 L0_apu
      REAL*4 R_epu,R0_ipu,R0_opu,R0_upu
      LOGICAL*1 L0_aru
      INTEGER*4 I0_eru
      LOGICAL*1 L0_iru,L_oru
      REAL*4 R_uru,R0_asu,R0_esu,R0_isu,R_osu,R0_usu,R0_atu
     &,R0_etu,R_itu,R_otu,R0_utu,R0_avu
      LOGICAL*1 L0_evu,L0_ivu,L0_ovu
      INTEGER*4 I0_uvu
      REAL*4 R0_axu,R_exu,R_ixu
      LOGICAL*1 L_oxu,L0_uxu,L0_abad,L0_ebad
      REAL*4 R_ibad,R0_obad,R0_ubad,R0_adad
      LOGICAL*1 L0_edad
      REAL*4 R_idad,R0_odad,R0_udad,R0_afad
      LOGICAL*1 L0_efad
      REAL*4 R_ifad,R0_ofad,R0_ufad,R0_akad
      LOGICAL*1 L0_ekad
      INTEGER*4 I0_ikad
      REAL*4 R_okad,R0_ukad,R0_alad,R0_elad,R_ilad,R0_olad
     &,R0_ulad,R0_amad,R_emad,R_imad,R0_omad,R0_umad
      LOGICAL*1 L0_apad,L0_epad,L0_ipad,L0_opad,L_upad,L0_arad
     &,L_erad,L0_irad,L_orad
      REAL*4 R_urad
      LOGICAL*1 L_asad
      REAL*4 R_esad,R0_isad,R0_osad,R0_usad,R0_atad
      LOGICAL*1 L0_etad
      REAL*4 R0_itad,R0_otad,R_utad,R0_avad,R0_evad,R0_ivad
     &,R_ovad,R0_uvad,R0_axad,R0_exad,R_ixad,R_oxad,R0_uxad
     &,R0_abed
      LOGICAL*1 L0_ebed,L0_ibed,L0_obed,L0_ubed,L_aded
      REAL*4 R0_eded
      LOGICAL*1 L0_ided,L_oded,L0_uded,L_afed
      REAL*4 R_efed
      LOGICAL*1 L_ifed
      REAL*4 R_ofed,R0_ufed,R0_aked,R0_eked,R_iked,R0_oked
     &,R0_uked,R0_aled,R0_eled,R0_iled,R0_oled
      LOGICAL*1 L0_uled,L0_amed
      REAL*4 R_emed,R0_imed,R0_omed,R0_umed
      LOGICAL*1 L0_aped
      REAL*4 R_eped,R_iped,R_oped,R0_uped,R0_ared
      LOGICAL*1 L0_ered,L0_ired,L0_ored,L0_ured,L_ased,L0_esed
     &,L0_ised,L_osed,L_used
      REAL*4 R0_ated
      LOGICAL*1 L_eted
      REAL*4 R0_ited,R0_oted
      LOGICAL*1 L0_uted,L_aved,L0_eved,L_ived,L0_oved
      REAL*4 R0_uved,R0_axed,R0_exed,R0_ixed,R_oxed,R_uxed
      INTEGER*4 I0_abid
      LOGICAL*1 L0_ebid
      REAL*4 R0_ibid,R0_obid,R_ubid,R_adid,R0_edid
      LOGICAL*1 L_idid
      REAL*4 R0_odid
      LOGICAL*1 L0_udid
      REAL*4 R0_afid,R0_efid,R_ifid,R0_ofid
      LOGICAL*1 L0_ufid,L0_akid
      REAL*4 R0_ekid,R0_ikid
      LOGICAL*1 L0_okid
      REAL*4 R0_ukid,R0_alid
      LOGICAL*1 L0_elid
      REAL*4 R0_ilid,R0_olid,R0_ulid
      INTEGER*4 I0_amid,I0_emid
      LOGICAL*1 L0_imid
      REAL*4 R0_omid,R0_umid
      LOGICAL*1 L0_apid
      REAL*4 R0_epid,R0_ipid,R0_opid
      INTEGER*4 I0_upid,I0_arid
      LOGICAL*1 L0_erid
      INTEGER*4 I0_irid
      REAL*4 R0_orid
      LOGICAL*1 L0_urid
      REAL*4 R0_asid,R0_esid,R0_isid
      INTEGER*4 I0_osid,I_usid
      REAL*4 R0_atid,R0_etid,R_itid,R_otid,R_utid,R0_avid
      LOGICAL*1 L0_evid
      REAL*4 R_ivid,R0_ovid,R_uvid
      CHARACTER*20 C20_axid

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_edo=R_ido
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1329):pre: ������������  �� T
      R0_ufu=R_aku
C LODOCHKA_HANDLER_IT_MAIN.fmg( 213,1378):pre: ������������  �� T
      R0_ede=R_ide
C LODOCHKA_HANDLER_IT_MAIN.fmg( 690,1931):pre: ������������  �� T
      R0_ir=R_or
C LODOCHKA_HANDLER_IT_MAIN.fmg( 690,1914):pre: ������������  �� T
      R0_axu=R_exu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 213,1424):pre: ������������  �� T
      R0_uv=R_ax
C LODOCHKA_HANDLER_IT_MAIN.fmg( 690,1940):pre: ������������  �� T
      R0_isid = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 217,1583):��������� (RE4) (�������)
      R0_i = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 357,1598):��������� (RE4) (�������)
      I0_o = 50
C LODOCHKA_HANDLER_IT_MAIN.fmg( 288,1590):��������� ������������� IN (�������)
      I0_u = 4001
C LODOCHKA_HANDLER_IT_MAIN.fmg( 336,1964):��������� ������������� IN (�������)
      I0_od = 0010
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1792):��������� ������������� IN (�������)
      I0_ud = 00011
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1796):��������� ������������� IN (�������)
      I0_of = 0005
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1802):��������� ������������� IN (�������)
      I0_ak = 0006
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1812):��������� ������������� IN (�������)
      I0_ik = 0002
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1822):��������� ������������� IN (�������)
      I0_uk = 0001
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1832):��������� ������������� IN (�������)
      I0_il = 00010
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1806):��������� ������������� IN (�������)
      I0_am = 0005
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1816):��������� ������������� IN (�������)
      I0_om = 0006
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1826):��������� ������������� IN (�������)
      I0_ep = 0002
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1838):��������� ������������� IN (�������)
      I0_ip = 0001
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1852):��������� ������������� IN (�������)
      I0_er = 5001
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1847):��������� ������������� IN (�������)
      I0_es = 40
C LODOCHKA_HANDLER_IT_MAIN.fmg( 695,1906):��������� ������������� IN (�������)
      R0_is = 0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1894):��������� (RE4) (�������)
      R0_os = 0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1881):��������� (RE4) (�������)
      R0_at = 0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1868):��������� (RE4) (�������)
      I0_ot = 94
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1906):��������� ������������� IN (�������)
      I0_av = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 695,1984):��������� ������������� IN (�������)
      L0_ov = L_ev.OR.L_iv.OR.L_ux
C LODOCHKA_HANDLER_IT_MAIN.fmg( 665,1936):���
      I0_ebe = 95
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1926):��������� ������������� IN (�������)
      R0_ibe = 1500.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1971):��������� (RE4) (�������)
      R0_obe = 2682.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1958):��������� (RE4) (�������)
      R0_ade = 16310.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 696,1946):��������� (RE4) (�������)
      I0_efe = 0016
C LODOCHKA_HANDLER_IT_MAIN.fmg( 680,1611):��������� ������������� IN (�������)
      L0_ofe=I_epe.eq.I_ufe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1600):���������� �������������
      I0_ake = 0015
C LODOCHKA_HANDLER_IT_MAIN.fmg( 680,1624):��������� ������������� IN (�������)
      L0_ike=I_epe.eq.I_oke
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1612):���������� �������������
      I0_uke = 0014
C LODOCHKA_HANDLER_IT_MAIN.fmg( 680,1637):��������� ������������� IN (�������)
      L0_ele=I_epe.eq.I_ile
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1626):���������� �������������
      I0_ole = 0013
C LODOCHKA_HANDLER_IT_MAIN.fmg( 680,1650):��������� ������������� IN (�������)
      L0_ame=I_epe.eq.I_eme
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1638):���������� �������������
      I0_ime = 0012
C LODOCHKA_HANDLER_IT_MAIN.fmg( 680,1663):��������� ������������� IN (�������)
      L0_ume=I_epe.eq.I_ape
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1652):���������� �������������
      I0_upe = 0011
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1662):��������� ������������� IN (�������)
      I0_ere = 0027
C LODOCHKA_HANDLER_IT_MAIN.fmg( 678,1679):��������� ������������� IN (�������)
      I0_ore = 0026
C LODOCHKA_HANDLER_IT_MAIN.fmg( 678,1688):��������� ������������� IN (�������)
      I0_ase = 0025
C LODOCHKA_HANDLER_IT_MAIN.fmg( 678,1698):��������� ������������� IN (�������)
      I0_ise = 0024
C LODOCHKA_HANDLER_IT_MAIN.fmg( 678,1708):��������� ������������� IN (�������)
      I0_ate = 0023
C LODOCHKA_HANDLER_IT_MAIN.fmg( 678,1718):��������� ������������� IN (�������)
      I0_ite = 0028
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1674):��������� ������������� IN (�������)
      I0_ave = 0028
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1684):��������� ������������� IN (�������)
      I0_ove = 0028
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1694):��������� ������������� IN (�������)
      I0_exe = 0028
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1703):��������� ������������� IN (�������)
      I0_uxe = 0028
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1712):��������� ������������� IN (�������)
      I0_ibi = 0042
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1725):��������� ������������� IN (�������)
      I0_adi = 0043
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1734):��������� ������������� IN (�������)
      I0_efi = 0044
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1744):��������� ������������� IN (�������)
      I0_aki = 0045
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1754):��������� ������������� IN (�������)
      I0_uki = 0046
C LODOCHKA_HANDLER_IT_MAIN.fmg( 654,1764):��������� ������������� IN (�������)
      I0_ili = 0028
C LODOCHKA_HANDLER_IT_MAIN.fmg( 686,1770):��������� ������������� IN (�������)
      I0_emi = 60
C LODOCHKA_HANDLER_IT_MAIN.fmg( 288,1566):��������� ������������� IN (�������)
      I0_omi = 60
C LODOCHKA_HANDLER_IT_MAIN.fmg( 148,1558):��������� ������������� IN (�������)
      R0_api = 3140
C LODOCHKA_HANDLER_IT_MAIN.fmg(  36,1856):��������� (RE4) (�������)
      R0_eri = 2530
C LODOCHKA_HANDLER_IT_MAIN.fmg(  36,1841):��������� (RE4) (�������)
      R0_isi = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg(  70,1862):��������� (RE4) (�������)
      R0_oti = 2240
C LODOCHKA_HANDLER_IT_MAIN.fmg(  36,1822):��������� (RE4) (�������)
      R0_evi = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg(  70,1914):��������� (RE4) (�������)
      R0_axi = 15200
C LODOCHKA_HANDLER_IT_MAIN.fmg(  36,1904):��������� (RE4) (�������)
      R0_uxi=abs(R_abo)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 169,1316):���������� ��������
      L0_ufo=R0_uxi.gt.R_oxi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1316):���������� >
      R0_ubo = 3140.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 156,1326):��������� (RE4) (�������)
      R0_obo = R0_ubo + (-R_ado)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 163,1324):��������
      R0_ibo=abs(R0_obo)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 169,1324):���������� ��������
      L0_ako=R0_ibo.lt.R_ebo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1324):���������� <
      I0_afo = 60
C LODOCHKA_HANDLER_IT_MAIN.fmg( 214,1324):��������� ������������� IN (�������)
      R0_efo = 15200
C LODOCHKA_HANDLER_IT_MAIN.fmg( 215,1312):��������� (RE4) (�������)
      R0_ofo = 3140
C LODOCHKA_HANDLER_IT_MAIN.fmg( 215,1298):��������� (RE4) (�������)
      I0_oko = 96
C LODOCHKA_HANDLER_IT_MAIN.fmg( 173,1330):��������� ������������� IN (�������)
      L0_elo=L_alo.and..not.L_uko
      L_uko=L_alo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 472,1415):������������  �� 1 ���
      I0_omo = 40
C LODOCHKA_HANDLER_IT_MAIN.fmg( 455,1420):��������� ������������� IN (�������)
      R0_apo = 600.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1724):��������� (RE4) (�������)
      R0_oto = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 206,1683):��������� (RE4) (�������)
      R0_ero = 100.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1652):��������� (RE4) (�������)
      R0_aso = 788.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1660):��������� (RE4) (�������)
      R0_ivo = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 206,1748):��������� (RE4) (�������)
      R0_ixo = 16310
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1715):��������� (RE4) (�������)
      R0_eled = 40.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 311,1748):��������� (RE4) (�������)
      I0_odu = 40
C LODOCHKA_HANDLER_IT_MAIN.fmg( 148,1568):��������� ������������� IN (�������)
      I0_ifu = 40
C LODOCHKA_HANDLER_IT_MAIN.fmg( 288,1574):��������� ������������� IN (�������)
      R0_itad = 40.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 310,1682):��������� (RE4) (�������)
      I0_ofu = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 183,1358):��������� ������������� IN (�������)
      R0_ulu = 7668.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 160,1391):��������� (RE4) (�������)
      R0_umu = 700.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 160,1374):��������� (RE4) (�������)
      R0_upu = 790.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 160,1382):��������� (RE4) (�������)
      I0_eru = 94
C LODOCHKA_HANDLER_IT_MAIN.fmg( 219,1389):��������� ������������� IN (�������)
      L0_iru=.true.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 214,1966):��������� ���������� (�������)
      R0_isu = 789.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1942):��������� (RE4) (�������)
      R0_etu = 7668.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1951):��������� (RE4) (�������)
      I0_uvu = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 183,1404):��������� ������������� IN (�������)
      R0_adad = 19709.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 160,1436):��������� (RE4) (�������)
      R0_afad = 500.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 160,1420):��������� (RE4) (�������)
      R0_akad = 790.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 160,1428):��������� (RE4) (�������)
      I0_ikad = 96
C LODOCHKA_HANDLER_IT_MAIN.fmg( 219,1434):��������� ������������� IN (�������)
      R0_elad = 789.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1842):��������� (RE4) (�������)
      R0_amad = 19709.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1851):��������� (RE4) (�������)
      L0_opad=.true.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 214,1866):��������� ���������� (�������)
      R0_atad = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 358,1684):��������� (RE4) (�������)
      R0_usad = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 310,1676):��������� (RE4) (�������)
      R0_ivad = 789.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1888):��������� (RE4) (�������)
      R0_exad = 16310.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1896):��������� (RE4) (�������)
      L0_ubed=.true.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 214,1912):��������� ���������� (�������)
      R0_eked = 2682.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1790):��������� (RE4) (�������)
      R0_aled = 16310.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 158,1798):��������� (RE4) (�������)
      R0_iled = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 359,1750):��������� (RE4) (�������)
      R0_imed = 1000
C LODOCHKA_HANDLER_IT_MAIN.fmg( 311,1716):��������� (RE4) (�������)
      L0_ured=.true.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 214,1814):��������� ���������� (�������)
      R0_oted = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 206,1978):��������� (RE4) (�������)
      R0_exed = 999999.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 508,1504):��������� (RE4) (�������)
      L0_oved=.false.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 474,1266):��������� ���������� (�������)
      R0_axed = 1.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 472,1282):��������� (RE4) (�������)
      R0_ixed = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 478,1282):��������� (RE4) (�������)
      R0_ibid = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 497,1588):��������� (RE4) (�������)
      I0_abid = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 428,1582):��������� ������������� IN (�������)
      L0_udid=.false.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 474,1302):��������� ���������� (�������)
      R0_odid = 1.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 472,1318):��������� (RE4) (�������)
      R0_afid = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 478,1318):��������� (RE4) (�������)
      L0_akid=.false.
C LODOCHKA_HANDLER_IT_MAIN.fmg( 474,1337):��������� ���������� (�������)
      R0_ofid = 1.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 472,1352):��������� (RE4) (�������)
      R0_ekid = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 478,1352):��������� (RE4) (�������)
      R0_alid = 999999.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 358,1504):��������� (RE4) (�������)
      R0_olid = 6000.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 359,1518):��������� (RE4) (�������)
      R0_ulid = 999999.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 359,1520):��������� (RE4) (�������)
      I0_amid = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 290,1499):��������� ������������� IN (�������)
      I0_emid = 65
C LODOCHKA_HANDLER_IT_MAIN.fmg( 290,1508):��������� ������������� IN (�������)
      R0_umid = 999999.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 216,1502):��������� (RE4) (�������)
      R0_ipid = 18000.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 216,1516):��������� (RE4) (�������)
      R0_opid = 999999.0
C LODOCHKA_HANDLER_IT_MAIN.fmg( 216,1518):��������� (RE4) (�������)
      I0_upid = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 148,1497):��������� ������������� IN (�������)
      I0_arid = 65
C LODOCHKA_HANDLER_IT_MAIN.fmg( 148,1506):��������� ������������� IN (�������)
      I0_irid = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 288,1582):��������� ������������� IN (�������)
      I0_osid = 91
C LODOCHKA_HANDLER_IT_MAIN.fmg( 148,1576):��������� ������������� IN (�������)
      R0_avid = 0.0
C LODOCHKA_HANDLER_IT_MAIN.fmg(  61,1965):��������� (RE4) (�������)
      C20_axid = 'FDB50'
C LODOCHKA_HANDLER_IT_MAIN.fmg(  22,1973):��������� ���������� CH20 (CH30) (�������)
      R0_ixi = R0_axi + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  43,1902):��������
C label 254  try254=try254-1
      R0_olu = R0_ulu + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1389):��������
      R0_ilu=abs(R0_olu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 174,1389):���������� ��������
      L0_alu=R0_ilu.lt.R_elu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 182,1389):���������� <
      R0_ubad = R0_adad + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1434):��������
      R0_obad=abs(R0_ubad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 174,1434):���������� ��������
      L0_ebad=R0_obad.lt.R_ibad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 182,1434):���������� <
      R0_ufad = R0_akad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1426):��������
      R0_ofad=abs(R0_ufad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 174,1426):���������� ��������
      L0_efad=R0_ofad.lt.R_ifad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 182,1426):���������� <
      L0_abe=I_usid.eq.I0_ebe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1927):���������� �������������
      L0_ox = L0_ov.AND.L0_abe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 672,1932):�
      if(L0_ox.and..not.L_ix) then
         R_ax=R_ex
      else
         R_ax=max(R0_uv-deltat,0.0)
      endif
      L0_ube=R_ax.gt.0.0
      L_ix=L0_ox
C LODOCHKA_HANDLER_IT_MAIN.fmg( 690,1940):������������  �� T
      if(L0_ube) then
         R0_uved=R0_ibe
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1970):���� � ������������� �������
      R0_avu = R_itu + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1958):��������
      R0_utu=abs(R0_avu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1958):���������� ��������
      L0_ovu=R0_utu.lt.R_otu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1958):���������� <
      R0_atu = R0_etu + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1949):��������
      R0_usu=abs(R0_atu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1949):���������� ��������
      L0_ivu=R0_usu.lt.R_osu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1949):���������� <
      R0_esu = R0_isu + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1940):��������
      R0_asu=abs(R0_esu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1940):���������� ��������
      L0_evu=R0_asu.lt.R_uru
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1940):���������� <
      L0_esed = L_used.AND.L0_ovu.AND.L0_ivu.AND.L0_evu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 191,1962):�
      L_eted=L0_esed.or.(L_eted.and..not.(L_osed))
      L0_ised=.not.L_eted
C LODOCHKA_HANDLER_IT_MAIN.fmg( 201,1960):RS �������
      if(L_eted) then
         R0_ited=R_ubid
      else
         R0_ited=R0_oted
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1976):���� RE IN LO CH7
      R0_abed = R_ixad + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1903):��������
      R0_uxad=abs(R0_abed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1903):���������� ��������
      L0_obed=R0_uxad.lt.R_oxad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1903):���������� <
      R0_axad = R0_exad + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1894):��������
      R0_uvad=abs(R0_axad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1894):���������� ��������
      L0_ibed=R0_uvad.lt.R_ovad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1894):���������� <
      R0_evad = R0_ivad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1886):��������
      R0_avad=abs(R0_evad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1886):���������� ��������
      L0_ebed=R0_avad.lt.R_utad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1886):���������� <
      L0_ided = L_ifed.AND.L0_obed.AND.L0_ibed.AND.L0_ebed
C LODOCHKA_HANDLER_IT_MAIN.fmg( 191,1908):�
      L_oded=L0_ided.or.(L_oded.and..not.(L_afed))
      L0_uded=.not.L_oded
C LODOCHKA_HANDLER_IT_MAIN.fmg( 200,1906):RS �������
      if(L_oded) then
         R0_eded=R_efed
      else
         R0_eded=R0_ited
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1917):���� RE IN LO CH7
      R0_umad = R_emad + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1858):��������
      R0_omad=abs(R0_umad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1858):���������� ��������
      L0_ipad=R0_omad.lt.R_imad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1858):���������� <
      R0_ulad = R0_amad + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1849):��������
      R0_olad=abs(R0_ulad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1849):���������� ��������
      L0_epad=R0_olad.lt.R_ilad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1849):���������� <
      R0_alad = R0_elad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1840):��������
      R0_ukad=abs(R0_alad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1840):���������� ��������
      L0_apad=R0_ukad.lt.R_okad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1840):���������� <
      L0_arad = L_asad.AND.L0_ipad.AND.L0_epad.AND.L0_apad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 191,1862):�
      L_erad=L0_arad.or.(L_erad.and..not.(L_orad))
      L0_irad=.not.L_erad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 200,1860):RS �������
      if(L_erad) then
         R0_ated=R_urad
      else
         R0_ated=R0_eded
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1872):���� RE IN LO CH7
      R0_ared = R_iped + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1805):��������
      R0_uped=abs(R0_ared)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1805):���������� ��������
      L0_ored=R0_uped.lt.R_oped
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1805):���������� <
      R0_uked = R0_aled + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1796):��������
      R0_oked=abs(R0_uked)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1796):���������� ��������
      L0_ired=R0_oked.lt.R_iked
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1796):���������� <
      R0_aked = R0_eked + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1788):��������
      R0_ufed=abs(R0_aked)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1788):���������� ��������
      L0_ered=R0_ufed.lt.R_ofed
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1788):���������� <
      L0_uted = L_idid.AND.L0_ored.AND.L0_ired.AND.L0_ered
C LODOCHKA_HANDLER_IT_MAIN.fmg( 191,1810):�
      L_aved=L0_uted.or.(L_aved.and..not.(L_ived))
      L0_eved=.not.L_aved
C LODOCHKA_HANDLER_IT_MAIN.fmg( 200,1808):RS �������
      if(L_aved) then
         R0_edid=R_adid
      else
         R0_edid=R0_ated
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1819):���� RE IN LO CH7
      if(L0_ube) then
         I_usid=I0_av
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1983):���� � ������������� �������
      L0_ebid=I_usid.eq.I0_abid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 452,1582):���������� �������������
      if(L0_ebid) then
         R0_obid=R0_edid
      else
         R0_obid=R0_ibid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 500,1586):���� RE IN LO CH7
      R0_udad = R0_afad + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1418):��������
      R0_odad=abs(R0_udad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 174,1418):���������� ��������
      L0_edad=R0_odad.lt.R_idad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 182,1418):���������� <
      L0_uxu=I_usid.eq.I0_uvu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 189,1405):���������� �������������
      L0_abad = L0_ebad.AND.L0_efad.AND.L0_edad.AND.L_orad.AND.L0_uxu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 200,1424):�
      if(L0_abad.and..not.L_oxu) then
         R_exu=R_ixu
      else
         R_exu=max(R0_axu-deltat,0.0)
      endif
      L0_ekad=R_exu.gt.0.0
      L_oxu=L0_abad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 213,1424):������������  �� T
      if(L0_ekad) then
         I_usid=I0_ikad
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 225,1434):���� � ������������� �������
      L0_it=I_usid.eq.I0_ot
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1906):���������� �������������
      L0_et = L_ut.AND.L0_it
C LODOCHKA_HANDLER_IT_MAIN.fmg( 672,1914):�
      if(L0_et.and..not.L_as) then
         R_or=R_ur
      else
         R_or=max(R0_ir-deltat,0.0)
      endif
      L0_us=R_or.gt.0.0
      L_as=L0_et
C LODOCHKA_HANDLER_IT_MAIN.fmg( 690,1914):������������  �� T
      if(L0_us) then
         R0_efid=R0_os
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1880):���� � ������������� �������
      if(L0_ube) then
         R0_efid=R0_obe
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1958):���� � ������������� �������
      R_uvid=R_ifid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 506,1309):������,VY01
      L0_evid=R_uvid.lt.R_ivid
C LODOCHKA_HANDLER_IT_MAIN.fmg(  54,1958):���������� <
      if(L0_evid) then
         R0_ovid=R_utid
      else
         R0_ovid=R0_avid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg(  64,1964):���� RE IN LO CH7
      if(L0_us) then
         I_usid=I0_es
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1906):���� � ������������� �������
      L0_e=I_usid.eq.I0_o
C LODOCHKA_HANDLER_IT_MAIN.fmg( 312,1592):���������� �������������
      if(L0_e) then
         R0_adu=R0_ovid
      else
         R0_adu=R0_i
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 360,1596):���� RE IN LO CH7
      R0_ubu = R_abu + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1732):��������
      R0_obu=abs(R0_ubu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1732):���������� ��������
      L0_ebu=R0_obu.lt.R_ibu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1732):���������� <
      if(L0_us) then
         R0_uved=R0_is
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1894):���� � ������������� �������
      L0_afe = L0_ox.OR.L0_et
C LODOCHKA_HANDLER_IT_MAIN.fmg( 681,1931):���
      if(L0_afe.and..not.L_ude) then
         R_ide=R_ode
      else
         R_ide=max(R0_ede-deltat,0.0)
      endif
      L0_ufid=R_ide.gt.0.0
      L_ude=L0_afe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 690,1931):������������  �� T
      if(L0_ufid) then
         R_uxed=R0_uved
      elseif(L0_oved) then
         R_uxed=R_uxed
      else
         R_uxed=R_uxed+deltat/R0_axed*R0_obid
      endif
      if(R_uxed.gt.R0_exed) then
         R_uxed=R0_exed
      elseif(R_uxed.lt.R0_ixed) then
         R_uxed=R0_ixed
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 476,1273):����������
C sav1=R0_ared
      R0_ared = R_iped + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1805):recalc:��������
C if(sav1.ne.R0_ared .and. try355.gt.0) goto 355
C sav1=R0_abed
      R0_abed = R_ixad + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1903):recalc:��������
C if(sav1.ne.R0_abed .and. try311.gt.0) goto 311
C sav1=R0_avu
      R0_avu = R_itu + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1958):recalc:��������
C if(sav1.ne.R0_avu .and. try289.gt.0) goto 289
C sav1=R0_umad
      R0_umad = R_emad + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1858):recalc:��������
C if(sav1.ne.R0_umad .and. try333.gt.0) goto 333
C sav1=R0_udad
      R0_udad = R0_afad + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1418):recalc:��������
C if(sav1.ne.R0_udad .and. try430.gt.0) goto 430
      R0_ipo = R0_apo + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1722):��������
      R0_epo=abs(R0_ipo)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1722):���������� ��������
      L0_ovo=R0_epo.lt.R_umo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1722):���������� <
      R0_uxo = R0_ixo + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1712):��������
      R0_oxo=abs(R0_uxo)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1712):���������� ��������
      L0_axo=R0_oxo.lt.R_exo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1712):���������� <
      L0_uvo = L0_ebu.AND.L0_ovo.AND.L0_axo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 188,1730):�
      if(L0_uvo) then
         R0_orid=R_evo
      else
         R0_orid=R0_ivo
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 210,1747):���� RE IN LO CH7
      L0_erid=I_usid.eq.I0_irid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 312,1582):���������� �������������
      if(L0_erid) then
         R0_afu=R0_orid
      else
         R0_afu=R0_adu
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 360,1586):���� RE IN LO CH7
      L0_aped=R_ifid.lt.R_eped
C LODOCHKA_HANDLER_IT_MAIN.fmg( 333,1734):���������� <
      R0_umed = R0_imed + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 318,1714):��������
      R0_omed=abs(R0_umed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 324,1714):���������� ��������
      L0_amed=R0_omed.lt.R_emed
C LODOCHKA_HANDLER_IT_MAIN.fmg( 333,1714):���������� <
      L0_uled = L0_aped.AND.L0_amed
C LODOCHKA_HANDLER_IT_MAIN.fmg( 340,1733):�
      if(L0_uled) then
         R0_oled=R0_eled
      else
         R0_oled=R0_iled
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 362,1748):���� RE IN LO CH7
      L0_efu=I_usid.eq.I0_ifu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 312,1575):���������� �������������
      if(L0_efu) then
         R0_udu=R0_oled
      else
         R0_udu=R0_afu
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 360,1578):���� RE IN LO CH7
      R0_exi=abs(R0_ixi)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  49,1902):���������� ��������
      L0_ovi=R0_exi.lt.R_uvi
C LODOCHKA_HANDLER_IT_MAIN.fmg(  58,1902):���������� <
      if(L0_ovi) then
         R0_ivi=R_abo
      else
         R0_ivi=R0_evi
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg(  74,1912):���� RE IN LO CH7
      L0_ami=I_usid.eq.I0_emi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 312,1566):���������� �������������
      if(L0_ami) then
         R0_etid=R0_ivi
      else
         R0_etid=R0_udu
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 360,1570):���� RE IN LO CH7
      L0_elid=I_usid.eq.I0_emid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 314,1510):���������� �������������
      if(L0_elid) then
         R0_ilid=R0_olid
      else
         R0_ilid=R0_ulid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 362,1518):���� RE IN LO CH7
      L0_okid=I_usid.eq.I0_amid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 314,1500):���������� �������������
      if(L0_okid) then
         R0_ukid=R0_alid
      else
         R0_ukid=R0_ilid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 362,1504):���� RE IN LO CH7
      R0_opu = R0_upu + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1380):��������
      R0_ipu=abs(R0_opu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 174,1380):���������� ��������
      L0_apu=R0_ipu.lt.R_epu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 182,1380):���������� <
      R0_omu = R0_umu + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1372):��������
      R0_imu=abs(R0_omu)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 174,1372):���������� ��������
      L0_amu=R0_imu.lt.R_emu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 182,1372):���������� <
      L0_oku=I_usid.eq.I0_ofu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 189,1360):���������� �������������
      L0_uku = L0_alu.AND.L0_apu.AND.L0_amu.AND.L_osed.AND.L0_oku
C LODOCHKA_HANDLER_IT_MAIN.fmg( 200,1378):�
      if(L0_uku.and..not.L_iku) then
         R_aku=R_eku
      else
         R_aku=max(R0_ufu-deltat,0.0)
      endif
      L0_aru=R_aku.gt.0.0
      L_iku=L0_uku
C LODOCHKA_HANDLER_IT_MAIN.fmg( 213,1378):������������  �� T
      if(L0_aru) then
         I_usid=I0_eru
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 225,1388):���� � ������������� �������
      L0_iko=I_usid.eq.I0_oko
C LODOCHKA_HANDLER_IT_MAIN.fmg( 179,1331):���������� �������������
      L0_eko = L0_iko.AND.L0_ako.AND.L0_ufo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 191,1329):�
      if(L0_eko.and..not.L_udo) then
         R_ido=R_odo
      else
         R_ido=max(R0_edo-deltat,0.0)
      endif
      L0_ifo=R_ido.gt.0.0
      L_udo=L0_eko
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1329):������������  �� T
      if(L0_ifo) then
         R0_ikid=R0_efo
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1311):���� � ������������� �������
      if(L0_ube) then
         R0_ikid=R0_ade
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1945):���� � ������������� �������
      if(L0_us) then
         R0_ikid=R0_at
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1868):���� � ������������� �������
      R0_uso = R_eso + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1671):��������
      R0_oso=abs(R0_uso)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1671):���������� ��������
      L0_ito=R0_oso.lt.R_iso
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1671):���������� <
      if(L0_ifo) then
         R0_efid=R0_ofo
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1298):���� � ������������� �������
      if(L0_ufid) then
         R_ifid=R0_efid
      elseif(L0_udid) then
         R_ifid=R_ifid
      else
         R_ifid=R_ifid+deltat/R0_odid*R0_etid
      endif
      if(R_ifid.gt.R0_ukid) then
         R_ifid=R0_ukid
      elseif(R_ifid.lt.R0_afid) then
         R_ifid=R0_afid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 476,1309):����������
C sav1=R0_ubu
      R0_ubu = R_abu + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1732):recalc:��������
C if(sav1.ne.R0_ubu .and. try476.gt.0) goto 476
C sav1=R0_alad
      R0_alad = R0_elad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1840):recalc:��������
C if(sav1.ne.R0_alad .and. try343.gt.0) goto 343
C sav1=R0_evad
      R0_evad = R0_ivad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1886):recalc:��������
C if(sav1.ne.R0_evad .and. try321.gt.0) goto 321
C sav1=R0_opu
      R0_opu = R0_upu + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1380):recalc:��������
C if(sav1.ne.R0_opu .and. try585.gt.0) goto 585
C sav1=R0_esu
      R0_esu = R0_isu + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1940):recalc:��������
C if(sav1.ne.R0_esu .and. try299.gt.0) goto 299
C sav1=R0_aked
      R0_aked = R0_eked + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1788):recalc:��������
C if(sav1.ne.R0_aked .and. try365.gt.0) goto 365
C sav1=R0_ufad
      R0_ufad = R0_akad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1426):recalc:��������
C if(sav1.ne.R0_ufad .and. try265.gt.0) goto 265
C sav1=R_uvid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 506,1309):recalc:������,VY01
C if(sav1.ne.R_uvid .and. try460.gt.0) goto 460
      R0_uro = R0_aso + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1658):��������
      R0_oro=abs(R0_uro)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1658):���������� ��������
      L0_eto=R0_oro.lt.R_iro
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1658):���������� <
      R0_aro = R0_ero + (-R_uxed)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1650):��������
      R0_upo=abs(R0_aro)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1650):���������� ��������
      L0_ato=R0_upo.lt.R_opo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 180,1650):���������� <
      L0_uto = L0_ito.AND.L0_eto.AND.L0_ato
C LODOCHKA_HANDLER_IT_MAIN.fmg( 191,1669):�
      if(L0_uto) then
         R0_esid=R_avo
      else
         R0_esid=R0_oto
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 209,1682):���� RE IN LO CH7
      if(L0_ifo) then
         I_usid=I0_afo
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1324):���� � ������������� �������
      L0_urid=I_usid.eq.I0_osid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1578):���������� �������������
      if(L0_urid) then
         R0_asid=R0_esid
      else
         R0_asid=R0_isid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1582):���� RE IN LO CH7
      R0_osad = R0_usad + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 317,1674):��������
      R0_isad=abs(R0_osad)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 323,1674):���������� ��������
      L0_etad=R0_isad.lt.R_esad
C LODOCHKA_HANDLER_IT_MAIN.fmg( 332,1674):���������� <
      if(L0_etad) then
         R0_otad=R0_itad
      else
         R0_otad=R0_atad
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 362,1683):���� RE IN LO CH7
      L0_idu=I_usid.eq.I0_odu
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1568):���������� �������������
      if(L0_idu) then
         R0_edu=R0_otad
      else
         R0_edu=R0_asid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1572):���� RE IN LO CH7
      R0_ipi = R0_api + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  43,1853):��������
      R0_epi=abs(R0_ipi)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  49,1853):���������� ��������
      L0_opi=R0_epi.lt.R_umi
C LODOCHKA_HANDLER_IT_MAIN.fmg(  58,1853):���������� <
      if(L0_opi) then
         R0_esi=R_upi
      else
         R0_esi=R0_isi
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg(  74,1860):���� RE IN LO CH7
      R0_ori = R0_eri + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  43,1838):��������
      R0_iri=abs(R0_ori)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  49,1838):���������� ��������
      L0_uri=R0_iri.lt.R_ari
C LODOCHKA_HANDLER_IT_MAIN.fmg(  58,1838):���������� <
      if(L0_uri) then
         R0_usi=R_asi
      else
         R0_usi=R0_esi
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg(  74,1846):���� RE IN LO CH7
      R0_avi = R0_oti + (-R_ifid)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  43,1820):��������
      R0_uti=abs(R0_avi)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  49,1820):���������� ��������
      L0_eti=R0_uti.lt.R_iti
C LODOCHKA_HANDLER_IT_MAIN.fmg(  58,1820):���������� <
      if(L0_eti) then
         R0_ati=R_osi
      else
         R0_ati=R0_usi
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg(  74,1830):���� RE IN LO CH7
      L0_imi=I_usid.eq.I0_omi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1560):���������� �������������
      if(L0_imi) then
         R0_atid=R0_ati
      else
         R0_atid=R0_edu
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1563):���� RE IN LO CH7
      L0_apid=I_usid.eq.I0_arid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1508):���������� �������������
      if(L0_apid) then
         R0_epid=R0_ipid
      else
         R0_epid=R0_opid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1516):���� RE IN LO CH7
      L0_imid=I_usid.eq.I0_upid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 172,1498):���������� �������������
      if(L0_imid) then
         R0_omid=R0_umid
      else
         R0_omid=R0_epid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 220,1502):���� RE IN LO CH7
      if(L0_ufid) then
         R_otid=R0_ikid
      elseif(L0_akid) then
         R_otid=R_otid
      else
         R_otid=R_otid+deltat/R0_ofid*R0_atid
      endif
      if(R_otid.gt.R0_omid) then
         R_otid=R0_omid
      elseif(R_otid.lt.R0_ekid) then
         R_otid=R0_ekid
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 476,1344):����������
C sav1=R0_ubad
      R0_ubad = R0_adad + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1434):recalc:��������
C if(sav1.ne.R0_ubad .and. try260.gt.0) goto 260
C sav1=R0_uked
      R0_uked = R0_aled + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1796):recalc:��������
C if(sav1.ne.R0_uked .and. try360.gt.0) goto 360
C sav1=R0_atu
      R0_atu = R0_etu + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1949):recalc:��������
C if(sav1.ne.R0_atu .and. try294.gt.0) goto 294
C sav1=R0_uso
      R0_uso = R_eso + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1671):recalc:��������
C if(sav1.ne.R0_uso .and. try619.gt.0) goto 619
C sav1=R0_axad
      R0_axad = R0_exad + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1894):recalc:��������
C if(sav1.ne.R0_axad .and. try316.gt.0) goto 316
C sav1=R0_olu
      R0_olu = R0_ulu + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 168,1389):recalc:��������
C if(sav1.ne.R0_olu .and. try255.gt.0) goto 255
C sav1=R0_ixi
      R0_ixi = R0_axi + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg(  43,1902):recalc:��������
C if(sav1.ne.R0_ixi .and. try254.gt.0) goto 254
C sav1=R0_ulad
      R0_ulad = R0_amad + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1849):recalc:��������
C if(sav1.ne.R0_ulad .and. try338.gt.0) goto 338
C sav1=R0_uxo
      R0_uxo = R0_ixo + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 166,1712):recalc:��������
C if(sav1.ne.R0_uxo .and. try522.gt.0) goto 522
C sav1=R0_umed
      R0_umed = R0_imed + (-R_otid)
C LODOCHKA_HANDLER_IT_MAIN.fmg( 318,1714):recalc:��������
C if(sav1.ne.R0_umed .and. try541.gt.0) goto 541
      R_itid=R_otid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 506,1344):������,VX01
      L0_imo=I_usid.eq.I0_omo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 478,1420):���������� �������������
      L0_ulo = L0_imo.AND.L0_elo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 484,1420):�
      I_uli=I_usid
C LODOCHKA_HANDLER_IT_MAIN.fmg( 497,1248):������,LP
      R_oxed=R_uxed
C LODOCHKA_HANDLER_IT_MAIN.fmg( 506,1273):������,VZ01
      if(L_aved) then
         L_ased=L0_ured
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 218,1813):���� � ������������� �������
      if(L_erad) then
         L_upad=L0_opad
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 218,1866):���� � ������������� �������
      if(L_oded) then
         L_aded=L0_ubed
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 218,1911):���� � ������������� �������
      if(L_eted) then
         L_oru=L0_iru
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 218,1966):���� � ������������� �������
      L0_ar=I_oli.eq.I0_er
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1848):���������� �������������
C label 779  try779=try779-1
      L0_op = L0_ar.AND.L_up
C LODOCHKA_HANDLER_IT_MAIN.fmg( 673,1847):�
      if(L0_op) then
         I_oli=I0_ip
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1852):���� � ������������� �������
      L0_oki=I_oli.eq.I0_uki
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1764):���������� �������������
      L0_iki = L0_oki.AND.L_ali
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1764):�
      L0_ufi=I_oli.eq.I0_aki
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1755):���������� �������������
      L0_ofi = L0_ufi.AND.L_eki
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1754):�
      L0_afi=I_oli.eq.I0_efi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1746):���������� �������������
      L0_udi = L0_afi.AND.L_ifi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1744):�
      L0_ubi=I_oli.eq.I0_adi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1736):���������� �������������
      L0_odi = L0_ubi.AND.L_edi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1734):�
      L0_ebi=I_oli.eq.I0_ibi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1726):���������� �������������
      L0_idi = L0_ebi.AND.L_obi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1725):�
      L0_eli = L0_iki.OR.L0_ofi.OR.L0_udi.OR.L0_odi.OR.L0_idi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 688,1760):���
      if(L0_eli) then
         I_oli=I0_ili
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 692,1769):���� � ������������� �������
      L0_ope=I_oli.eq.I0_upe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1664):���������� �������������
      L0_ipe = L0_ope.AND.L_are
C LODOCHKA_HANDLER_IT_MAIN.fmg( 668,1662):�
      L0_ome = L0_ipe.AND.L0_ume
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1658):�
      if(L0_ome) then
         I_oli=I0_ime
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 685,1662):���� � ������������� �������
      L0_ule = L0_ipe.AND.L0_ame
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1644):�
      if(L0_ule) then
         I_oli=I0_ole
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 685,1650):���� � ������������� �������
      L0_eke = L0_ipe.AND.L0_ike
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1618):�
      if(L0_eke) then
         I_oli=I0_ake
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 685,1624):���� � ������������� �������
      L0_ife = L0_ipe.AND.L0_ofe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1606):�
      if(L0_ife) then
         I_oli=I0_efe
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 685,1610):���� � ������������� �������
      L0_ale = L0_ipe.AND.L0_ele
C LODOCHKA_HANDLER_IT_MAIN.fmg( 674,1632):�
      if(L0_ale) then
         I_oli=I0_uke
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 685,1636):���� � ������������� �������
      L0_ete=I_oli.eq.I0_ite
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1675):���������� �������������
      L0_ire = L0_ete.AND.L_ote
C LODOCHKA_HANDLER_IT_MAIN.fmg( 668,1674):�
      if(L0_ire) then
         I_oli=I0_ere
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 683,1678):���� � ������������� �������
      L0_ute=I_oli.eq.I0_ave
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1684):���������� �������������
      L0_ure = L0_ute.AND.L_eve
C LODOCHKA_HANDLER_IT_MAIN.fmg( 668,1684):�
      if(L0_ure) then
         I_oli=I0_ore
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 683,1688):���� � ������������� �������
      L0_ive=I_oli.eq.I0_ove
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1694):���������� �������������
      L0_ese = L0_ive.AND.L_uve
C LODOCHKA_HANDLER_IT_MAIN.fmg( 668,1694):�
      if(L0_ese) then
         I_oli=I0_ase
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 683,1698):���� � ������������� �������
      L0_axe=I_oli.eq.I0_exe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1704):���������� �������������
      L0_ose = L0_axe.AND.L_ixe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 668,1703):�
      if(L0_ose) then
         I_oli=I0_ise
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 683,1708):���� � ������������� �������
      L0_ok=I_oli.eq.I0_uk
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1834):���������� �������������
      L0_ap = L0_ok.AND.L_al
C LODOCHKA_HANDLER_IT_MAIN.fmg( 673,1832):�
      if(L0_ap) then
         I_oli=I0_ep
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1838):���� � ������������� �������
      L0_oxe=I_oli.eq.I0_uxe
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1714):���������� �������������
      L0_use = L0_oxe.AND.L_abi
C LODOCHKA_HANDLER_IT_MAIN.fmg( 668,1712):�
      if(L0_use) then
         I_oli=I0_ate
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 683,1717):���� � ������������� �������
      L0_ek=I_oli.eq.I0_ik
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1822):���������� �������������
      L0_um = L0_ek.AND.L_el
C LODOCHKA_HANDLER_IT_MAIN.fmg( 673,1822):�
      if(L0_um) then
         I_oli=I0_om
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1825):���� � ������������� �������
      L0_uf=I_oli.eq.I0_ak
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1812):���������� �������������
      L0_em = L0_uf.AND.L_im
C LODOCHKA_HANDLER_IT_MAIN.fmg( 673,1812):�
      if(L0_em) then
         I_oli=I0_am
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1815):���� � ������������� �������
      L0_if=I_oli.eq.I0_of
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1802):���������� �������������
      L0_ol = L0_if.AND.L_ul
C LODOCHKA_HANDLER_IT_MAIN.fmg( 673,1802):�
      if(L0_ol) then
         I_oli=I0_il
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 701,1805):���� � ������������� �������
      L0_id=I_oli.eq.I0_od
C LODOCHKA_HANDLER_IT_MAIN.fmg( 660,1793):���������� �������������
      L0_af = L0_id.AND.L_ef
C LODOCHKA_HANDLER_IT_MAIN.fmg( 673,1792):�
      if(L0_af) then
         I_oli=I0_ud
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 700,1796):���� � ������������� �������
      L0_ad=I_oli.eq.I0_u
C LODOCHKA_HANDLER_IT_MAIN.fmg( 342,1965):���������� �������������
      if(L0_ad) then
         R_emo=R_ed
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 373,1970):���� � ������������� �������
      R0_olo = R_emo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 468,1432):��������
C label 970  try970=try970-1
      R0_amo = R0_olo + R_ilo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 478,1431):��������
      if(L0_ulo) then
         R_emo=R0_amo
      endif
C LODOCHKA_HANDLER_IT_MAIN.fmg( 488,1430):���� � ������������� �������
C sav1=R0_olo
      R0_olo = R_emo
C LODOCHKA_HANDLER_IT_MAIN.fmg( 468,1432):recalc:��������
C if(sav1.ne.R0_olo .and. try970.gt.0) goto 970
      End
