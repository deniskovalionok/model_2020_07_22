      Subroutine TOLKATEL_HANDLER_DOZ(ext_deltat,R_ud,R_af
     &,R_uk,R_el,R_il,R_ul,R_am,L_om,L_ap,R_ep,R_ip,R_op,I_ar
     &,R_or,R_ur,R_as,I_ov,I_ex,R_ix,R_ox,R_ux,R_abe,L_ebe
     &,L_ibe,I_obe,I_ede,I_efe,I_ufe,C20_uke,C20_ule,C8_ume
     &,L_are,R_ure,R_ase,L_ese,R_ise,R_ose,I_use,L_ite,L_ote
     &,L_ute,L_ave,I_eve,I_uve,L_ixe,L_uxe,L_abi,L_ebi,L_adi
     &,L_odi,L_udi,L_ifi,L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki
     &,L_ali,L_eli,L_ili,L_oli,L_uli,L_ami,L_imi,R8_umi,R_opi
     &,R8_eri,L_iri,I_ori,L_esi,L_isi,L_eti,L_avi,L_ivi,L_ovi
     &,L_uvi,L_axi)
C |R_ud          |4 4 O|VX02_OE         |�������� ����������� ���������, �.�.*�||
C |R_af          |4 4 I|tcl_top         |�������� �������|`top_c`|
C |R_uk          |4 4 I|CBO_POS         ||`p_TOLKATEL_XH54`|
C |R_el          |4 4 I|CBC_POS         ||`p_TOLKATEL_XH53`|
C |R_il          |4 4 I|MAXX            ||`p_UP`|
C |R_ul          |4 4 O|VX01            |��������� ���������, ��||
C |R_am          |4 4 I|MINX            ||`p_LOW`|
C |L_om          |1 1 I|YA25C           |������� ������� �� ���������� (��)|F|
C |L_ap          |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_ep          |4 4 K|_uintT_INT      |����������� ������ ����������� ������|1|
C |R_ip          |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_op          |4 4 K|_lintT_INT      |����������� ������ ����������� �����|0|
C |I_ar          |2 4 O|LWORK           |����� �������||
C |R_or          |4 4 O|VX01_OE         |��������� ���������, �.�.||
C |R_ur          |4 4 O|_ointT_INT*     |�������� ������ ����������� |0|
C |R_as          |4 4 O|VX02            |�������� ����������� ���������, ��*�||
C |I_ov          |2 4 O|LREADY          |����� ����������||
C |I_ex          |2 4 O|LBUSY           |����� �����||
C |R_ix          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ��������� (�������)" |0.0|
C |R_ox          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ��������� (�������)" |0.0|
C |R_ux          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ��������� (�������)" |0.0|
C |R_abe         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ��������� (�������)" |0.0|
C |L_ebe         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ibe         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_obe         |2 4 O|state1          |��������� 1||
C |I_ede         |2 4 O|state2          |��������� 2||
C |I_efe         |2 4 O|LWORKO          |����� � �������||
C |I_ufe         |2 4 O|LINITC          |����� � ��������||
C |C20_uke       |3 20 O|task_state      |���������||
C |C20_ule       |3 20 O|task_name       |������� ���������||
C |C8_ume        |3 8 I|task            |������� ���������||
C |L_are         |1 1 O|XH54            |�� ������� (���)|F|
C |R_ure         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ase         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ese         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ise         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ose         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_use         |2 4 O|LERROR          |����� �������������||
C |L_ite         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ote         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_ute         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_ave         |1 1 O|XH53            |�� ������� (���)|F|
C |I_eve         |2 4 O|LINIT           |����� ��������||
C |I_uve         |2 4 O|LZM             |������ "�������"||
C |L_ixe         |1 1 I|vlv_kvit        |||
C |L_uxe         |1 1 I|instr_fault     |||
C |L_abi         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ebi         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ��������� (�������)|F|
C |L_adi         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ��������� (�������)|F|
C |L_odi         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_udi         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ifi         |1 1 O|block           |||
C |L_ufi         |1 1 O|norm            |�����||
C |L_aki         |1 1 I|mlf15           |������ ���������������� ����� �������||
C |L_eki         |1 1 I|mlf14           |������ ������������ ����� �������||
C |L_iki         |1 1 I|mlf17           |������ ���������������� ����� �������||
C |L_oki         |1 1 I|mlf16           |������ ������������ ����� �������||
C |L_uki         |1 1 I|mlf27           |������ ������������ ��������� �������||
C |L_ali         |1 1 I|mlf25           |������ ������������ ��������� �������||
C |L_eli         |1 1 I|mlf09           |����� ��������� �������||
C |L_ili         |1 1 I|mlf08           |����� ��������� �������||
C |L_oli         |1 1 I|mlf07           |���������� ���� �������||
C |L_uli         |1 1 I|mlf06           |������������� ������� �������||
C |L_ami         |1 1 I|mlf05           |������������� ������� �������||
C |L_imi         |1 1 O|nopower         |��� ����������||
C |R8_umi        |4 8 I|voltage         |[��]���������� �� ������|0.4|
C |R_opi         |4 4 I|power           |�������� ��������||
C |R8_eri        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_iri         |1 1 O|fault           |�������������||
C |I_ori         |2 4 O|LAM             |������ "�������"||
C |L_esi         |1 1 O|XH51            |�� ������� (���)|F|
C |L_isi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_eti         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_avi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ovi         |1 1 I|mlf04           |�������� �� ��������||
C |L_uvi         |1 1 I|mlf03           |�������� �� ��������||
C |L_axi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R0_i
      LOGICAL*1 L0_o,L0_u,L0_ad
      REAL*4 R0_ed
      LOGICAL*1 L0_id
      REAL*4 R0_od,R_ud,R_af,R0_ef,R0_if,R0_of
      LOGICAL*1 L0_uf,L0_ak,L0_ek,L0_ik,L0_ok
      REAL*4 R_uk
      LOGICAL*1 L0_al
      REAL*4 R_el,R_il,R0_ol,R_ul,R_am,R0_em
      LOGICAL*1 L0_im,L_om,L0_um,L_ap
      REAL*4 R_ep,R_ip,R_op,R0_up
      INTEGER*4 I_ar,I0_er,I0_ir
      REAL*4 R_or,R_ur,R_as
      LOGICAL*1 L0_es,L0_is
      REAL*4 R0_os,R0_us,R0_at,R0_et,R0_it
      LOGICAL*1 L0_ot,L0_ut
      INTEGER*4 I0_av,I0_ev
      LOGICAL*1 L0_iv
      INTEGER*4 I_ov,I0_uv,I0_ax,I_ex
      REAL*4 R_ix,R_ox,R_ux,R_abe
      LOGICAL*1 L_ebe,L_ibe
      INTEGER*4 I_obe,I0_ube,I0_ade,I_ede,I0_ide,I0_ode,I0_ude
     &,I0_afe,I_efe,I0_ife,I0_ofe,I_ufe
      CHARACTER*20 C20_ake,C20_eke,C20_ike,C20_oke,C20_uke
     &,C20_ale,C20_ele,C20_ile,C20_ole,C20_ule
      CHARACTER*8 C8_ame
      LOGICAL*1 L0_eme
      CHARACTER*8 C8_ime
      LOGICAL*1 L0_ome
      CHARACTER*8 C8_ume
      INTEGER*4 I0_ape,I0_epe,I0_ipe,I0_ope,I0_upe
      LOGICAL*1 L_are
      INTEGER*4 I0_ere,I0_ire,I0_ore
      REAL*4 R_ure,R_ase
      LOGICAL*1 L_ese
      REAL*4 R_ise,R_ose
      INTEGER*4 I_use,I0_ate,I0_ete
      LOGICAL*1 L_ite,L_ote,L_ute,L_ave
      INTEGER*4 I_eve,I0_ive,I0_ove,I_uve,I0_axe,I0_exe
      LOGICAL*1 L_ixe,L0_oxe,L_uxe,L_abi,L_ebi,L0_ibi,L0_obi
     &,L0_ubi,L_adi,L0_edi,L0_idi,L_odi,L_udi,L0_afi,L0_efi
     &,L_ifi,L0_ofi,L_ufi,L_aki,L_eki,L_iki,L_oki
      LOGICAL*1 L_uki,L_ali,L_eli,L_ili,L_oli,L_uli,L_ami
     &,L0_emi,L_imi
      REAL*4 R0_omi
      REAL*8 R8_umi
      LOGICAL*1 L0_api,L0_epi
      REAL*4 R0_ipi,R_opi,R0_upi,R0_ari
      REAL*8 R8_eri
      LOGICAL*1 L_iri
      INTEGER*4 I_ori,I0_uri,I0_asi
      LOGICAL*1 L_esi,L_isi,L0_osi,L0_usi,L0_ati,L_eti,L0_iti
     &,L0_oti,L0_uti,L_avi,L0_evi,L_ivi,L_ovi,L_uvi,L_axi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e = 0.33
C TOLKATEL_HANDLER_DOZ.fmg( 294, 382):��������� (RE4) (�������)
      R0_od = 0.001
C TOLKATEL_HANDLER_DOZ.fmg( 314, 272):��������� (RE4) (�������)
      R0_ed = 0.999
C TOLKATEL_HANDLER_DOZ.fmg( 314, 286):��������� (RE4) (�������)
      R0_if = R_il + (-R_am)
C TOLKATEL_HANDLER_DOZ.fmg( 298, 318):��������
      R0_ef=abs(R0_if)
C TOLKATEL_HANDLER_DOZ.fmg( 306, 318):���������� ��������
      if(R0_ef.ge.0.0) then
         R_ud=R_af/max(R0_ef,1.0e-10)
      else
         R_ud=R_af/min(R0_ef,-1.0e-10)
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 345, 320):�������� ����������
      R0_i = R_ud * R0_e
C TOLKATEL_HANDLER_DOZ.fmg( 298, 384):����������
      if(L_oli) then
         R0_os=R0_i
      else
         R0_os=R_ud
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 306, 387):���� RE IN LO CH7
      R0_of = 0.0
C TOLKATEL_HANDLER_DOZ.fmg( 326, 362):��������� (RE4) (�������)
      R0_ol = 1.0
C TOLKATEL_HANDLER_DOZ.fmg( 356, 326):��������� (RE4) (�������)
      R0_em = 0.0
C TOLKATEL_HANDLER_DOZ.fmg( 352, 326):��������� (RE4) (�������)
      R0_it = 0.0
C TOLKATEL_HANDLER_DOZ.fmg( 312, 380):��������� (RE4) (�������)
      L0_im = L_ebe.OR.L_om
C TOLKATEL_HANDLER_DOZ.fmg(  80, 288):���
      L0_um = L_ibe.OR.L_ap
C TOLKATEL_HANDLER_DOZ.fmg(  80, 358):���
      I0_ir = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 346):��������� ������������� IN (�������)
      I0_er = z'0100000A'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 344):��������� ������������� IN (�������)
      L0_afi = L_uli.OR.L_udi
C TOLKATEL_HANDLER_DOZ.fmg(  80, 273):���
      L0_efi = L_ami.OR.L_odi
C TOLKATEL_HANDLER_DOZ.fmg(  80, 371):���
      L_ifi = L0_efi.OR.L0_afi
C TOLKATEL_HANDLER_DOZ.fmg( 104, 324):���
      I0_av = z'0100000A'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 355):��������� ������������� IN (�������)
      I0_ev = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 357):��������� ������������� IN (�������)
      if(L_ifi) then
         I_ex=I0_av
      else
         I_ex=I0_ev
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 356):���� RE IN LO CH7
      I0_ax = z'0100000A'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 368):��������� ������������� IN (�������)
      I0_uv = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 366):��������� ������������� IN (�������)
      L_ebi=R_ox.ne.R_ix
      R_ix=R_ox
C TOLKATEL_HANDLER_DOZ.fmg(  21, 298):���������� ������������� ������
      L_adi=R_abe.ne.R_ux
      R_ux=R_abe
C TOLKATEL_HANDLER_DOZ.fmg(  21, 349):���������� ������������� ������
      I0_ade = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 314):��������� ������������� IN (�������)
      I0_ube = z'01000010'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 312):��������� ������������� IN (�������)
      I0_ode = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 336):��������� ������������� IN (�������)
      I0_ide = z'01000010'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 334):��������� ������������� IN (�������)
      I0_afe = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 251):��������� ������������� IN (�������)
      I0_ude = z'0100000A'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 249):��������� ������������� IN (�������)
      I0_ofe = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 286):��������� ������������� IN (�������)
      I0_ife = z'0100000A'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 284):��������� ������������� IN (�������)
      I0_ove = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 324):��������� ������������� IN (�������)
      I0_ive = z'0100000A'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 322):��������� ������������� IN (�������)
      C20_ike = '�������'
C TOLKATEL_HANDLER_DOZ.fmg(  76, 256):��������� ���������� CH20 (CH30) (�������)
      C20_oke = '������� ���'
C TOLKATEL_HANDLER_DOZ.fmg(  76, 258):��������� ���������� CH20 (CH30) (�������)
      C20_ake = '�������'
C TOLKATEL_HANDLER_DOZ.fmg(  94, 255):��������� ���������� CH20 (CH30) (�������)
      C20_ale = '�������'
C TOLKATEL_HANDLER_DOZ.fmg(  94, 266):��������� ���������� CH20 (CH30) (�������)
      C20_ile = '�������'
C TOLKATEL_HANDLER_DOZ.fmg(  76, 267):��������� ���������� CH20 (CH30) (�������)
      C20_ole = ''
C TOLKATEL_HANDLER_DOZ.fmg(  76, 269):��������� ���������� CH20 (CH30) (�������)
      C8_ame = 'init'
C TOLKATEL_HANDLER_DOZ.fmg(  43, 324):��������� ���������� CH8 (�������)
      call chcomp(C8_ume,C8_ame,L0_eme)
C TOLKATEL_HANDLER_DOZ.fmg(  50, 326):���������� ���������
      C8_ime = 'work'
C TOLKATEL_HANDLER_DOZ.fmg(  43, 328):��������� ���������� CH8 (�������)
      call chcomp(C8_ume,C8_ime,L0_ome)
C TOLKATEL_HANDLER_DOZ.fmg(  50, 330):���������� ���������
      if(L0_ome) then
         C20_ele=C20_ile
      else
         C20_ele=C20_ole
      endif
C TOLKATEL_HANDLER_DOZ.fmg(  80, 268):���� RE IN LO CH20
      if(L0_eme) then
         C20_ule=C20_ale
      else
         C20_ule=C20_ele
      endif
C TOLKATEL_HANDLER_DOZ.fmg(  98, 266):���� RE IN LO CH20
      I0_ape = z'0100008E'
C TOLKATEL_HANDLER_DOZ.fmg( 202, 298):��������� ������������� IN (�������)
      I0_epe = z'01000086'
C TOLKATEL_HANDLER_DOZ.fmg( 202, 264):��������� ������������� IN (�������)
      I0_ope = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 183, 266):��������� ������������� IN (�������)
      I0_upe = z'01000010'
C TOLKATEL_HANDLER_DOZ.fmg( 183, 268):��������� ������������� IN (�������)
      I0_ore = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 184, 300):��������� ������������� IN (�������)
      I0_ire = z'01000010'
C TOLKATEL_HANDLER_DOZ.fmg( 184, 298):��������� ������������� IN (�������)
      L_ite=R_ase.ne.R_ure
      R_ure=R_ase
C TOLKATEL_HANDLER_DOZ.fmg(  21, 310):���������� ������������� ������
      L_ese=R_ose.ne.R_ise
      R_ise=R_ose
C TOLKATEL_HANDLER_DOZ.fmg(  21, 337):���������� ������������� ������
      L0_ubi = L_ese.AND.L0_ome
C TOLKATEL_HANDLER_DOZ.fmg(  65, 336):�
      L0_edi = L_adi.OR.L0_ubi
C TOLKATEL_HANDLER_DOZ.fmg(  72, 336):���
      L0_ak = (.NOT.L0_edi).AND.L0_im
C TOLKATEL_HANDLER_DOZ.fmg(  86, 294):�
      L0_ibi = L_ese.AND.L0_eme
C TOLKATEL_HANDLER_DOZ.fmg(  65, 326):�
      L0_obi = L0_ibi.OR.L_ebi
C TOLKATEL_HANDLER_DOZ.fmg(  72, 298):���
      L0_ofi = L0_obi.OR.L0_ak
C TOLKATEL_HANDLER_DOZ.fmg(  94, 297):���
      L0_usi = L0_ofi.AND.(.NOT.L0_afi)
C TOLKATEL_HANDLER_DOZ.fmg( 104, 296):�
      L0_uf = L0_um.AND.(.NOT.L0_obi)
C TOLKATEL_HANDLER_DOZ.fmg(  86, 342):�
      L0_idi = L0_uf.OR.L0_edi
C TOLKATEL_HANDLER_DOZ.fmg(  94, 338):���
      L0_oti = (.NOT.L0_efi).AND.L0_idi
C TOLKATEL_HANDLER_DOZ.fmg( 104, 338):�
      I0_axe = z'0100000E'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 296):��������� ������������� IN (�������)
      I0_ate = z'01000007'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 377):��������� ������������� IN (�������)
      I0_ete = z'01000003'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 379):��������� ������������� IN (�������)
      I0_uri = z'0100000E'
C TOLKATEL_HANDLER_DOZ.fmg( 220, 264):��������� ������������� IN (�������)
      L_abi=(L_uxe.or.L_abi).and..not.(L_ixe)
      L0_oxe=.not.L_abi
C TOLKATEL_HANDLER_DOZ.fmg( 476, 290):RS �������
      L0_emi = L_uvi.OR.L_ovi.OR.L_ami.OR.L_uli.OR.L_oli.OR.L_ili.OR.L_e
     &li.OR.L_axi.OR.L_ivi.OR.L_ali.OR.L_uki.OR.L_oki.OR.L_iki.OR.L_eki.
     &OR.L_aki
C TOLKATEL_HANDLER_DOZ.fmg( 464, 310):���
      L_iri = L0_emi.OR.L_abi
C TOLKATEL_HANDLER_DOZ.fmg( 484, 306):���
      if(L_iri) then
         I_use=I0_ate
      else
         I_use=I0_ete
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 378):���� RE IN LO CH7
      L0_osi = L_ute.OR.L_ote.OR.L_ite.OR.L_axi.OR.L_iri
C TOLKATEL_HANDLER_DOZ.fmg(  94, 310):���
      L0_iv = L_iri.OR.L_ifi
C TOLKATEL_HANDLER_DOZ.fmg( 196, 362):���
      if(L0_iv) then
         I_ov=I0_uv
      else
         I_ov=I0_ax
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 366):���� RE IN LO CH7
      L_ufi = (.NOT.L0_emi).OR.L_uxe
C TOLKATEL_HANDLER_DOZ.fmg( 484, 310):���
      R0_omi = 0.1
C TOLKATEL_HANDLER_DOZ.fmg( 308, 228):��������� (RE4) (�������)
      L_imi=R8_umi.lt.R0_omi
C TOLKATEL_HANDLER_DOZ.fmg( 312, 229):���������� <
      R0_ipi = 0.0
C TOLKATEL_HANDLER_DOZ.fmg( 318, 248):��������� (RE4) (�������)
      L0_uti = L0_usi.OR.L0_osi.OR.L_isi
C TOLKATEL_HANDLER_DOZ.fmg( 132, 334):���
C label 201  try201=try201-1
      L_avi=(L0_oti.or.L_avi).and..not.(L0_uti)
      L0_evi=.not.L_avi
C TOLKATEL_HANDLER_DOZ.fmg( 140, 336):RS �������,1
      L0_is = L_avi.AND.(.NOT.L_ovi)
C TOLKATEL_HANDLER_DOZ.fmg( 302, 370):�
      L0_ut = L_uvi.OR.L0_is
C TOLKATEL_HANDLER_DOZ.fmg( 308, 371):���
      if(L0_ut) then
         R0_at=R0_os
      else
         R0_at=R0_it
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 316, 380):���� RE IN LO CH7
      L0_id=R_ur.lt.R0_od
C TOLKATEL_HANDLER_DOZ.fmg( 324, 273):���������� <
      L0_o = L0_id.AND.(.NOT.L_iki)
C TOLKATEL_HANDLER_DOZ.fmg( 334, 272):�
      L_esi = L0_o.OR.L_imi.OR.L_aki
C TOLKATEL_HANDLER_DOZ.fmg( 343, 270):���
      L0_ati = L0_osi.OR.L0_oti.OR.L_esi
C TOLKATEL_HANDLER_DOZ.fmg( 132, 292):���
      L_eti=(L0_usi.or.L_eti).and..not.(L0_ati)
      L0_iti=.not.L_eti
C TOLKATEL_HANDLER_DOZ.fmg( 140, 294):RS �������,2
      L0_es = L_eti.AND.(.NOT.L_uvi)
C TOLKATEL_HANDLER_DOZ.fmg( 297, 360):�
      L0_ot = L_ovi.OR.L0_es
C TOLKATEL_HANDLER_DOZ.fmg( 308, 362):���
      if(L0_ot) then
         R0_us=R0_os
      else
         R0_us=R0_it
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 316, 366):���� RE IN LO CH7
      R0_et = R0_at + (-R0_us)
C TOLKATEL_HANDLER_DOZ.fmg( 322, 373):��������
      if(L_ivi) then
         R0_up=R0_of
      else
         R0_up=R0_et
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 330, 363):���� RE IN LO CH7
      R_ur=R_ur+deltat/R_ip*R0_up
      if(R_ur.gt.R_ep) then
         R_ur=R_ep
      elseif(R_ur.lt.R_op) then
         R_ur=R_op
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 340, 362):����������,T_INT
      L0_ad=R_ur.gt.R0_ed
C TOLKATEL_HANDLER_DOZ.fmg( 324, 288):���������� >
      L0_u = L0_ad.AND.(.NOT.L_oki)
C TOLKATEL_HANDLER_DOZ.fmg( 334, 286):�
      L_isi = L0_u.OR.L_imi.OR.L_eki
C TOLKATEL_HANDLER_DOZ.fmg( 343, 284):���
C sav1=L0_uti
      L0_uti = L0_usi.OR.L0_osi.OR.L_isi
C TOLKATEL_HANDLER_DOZ.fmg( 132, 334):recalc:���
C if(sav1.ne.L0_uti .and. try201.gt.0) goto 201
      R_or=R_ur
C TOLKATEL_HANDLER_DOZ.fmg( 372, 364):������,VX01_OE
      if(R_ur.le.R0_em) then
         R_ul=R_am
      elseif(R_ur.gt.R0_ol) then
         R_ul=R_il
      else
         R_ul=R_am+(R_ur-(R0_em))*(R_il-(R_am))/(R0_ol-(R0_em
     &))
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 354, 332):��������������� ��������� �����������
      L0_al=R_ur.lt.R_el
C TOLKATEL_HANDLER_DOZ.fmg( 324, 300):���������� <
      L0_ek = L0_al.AND.(.NOT.L_eli)
C TOLKATEL_HANDLER_DOZ.fmg( 334, 300):�
      L_ave = L0_ek.OR.L_imi.OR.L_uki
C TOLKATEL_HANDLER_DOZ.fmg( 343, 298):���
      if(L_ave) then
         I0_ipe=I0_ope
      else
         I0_ipe=I0_upe
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 188, 266):���� RE IN LO CH7
      if(L_avi) then
         I0_asi=I0_epe
      else
         I0_asi=I0_ipe
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 206, 265):���� RE IN LO CH7
      if(L_iri) then
         I_ori=I0_uri
      else
         I_ori=I0_asi
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 264):���� RE IN LO CH7
      if(L_ave) then
         I_obe=I0_ube
      else
         I_obe=I0_ade
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 312):���� RE IN LO CH7
      if(L_ave) then
         I_eve=I0_ive
      else
         I_eve=I0_ove
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 322):���� RE IN LO CH7
      L0_ok=R_ur.gt.R_uk
C TOLKATEL_HANDLER_DOZ.fmg( 324, 312):���������� >
      L0_ik = L0_ok.AND.(.NOT.L_ili)
C TOLKATEL_HANDLER_DOZ.fmg( 334, 312):�
      L_are = L0_ik.OR.L_imi.OR.L_ali
C TOLKATEL_HANDLER_DOZ.fmg( 343, 310):���
      if(L_are) then
         I0_ere=I0_ire
      else
         I0_ere=I0_ore
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 188, 299):���� RE IN LO CH7
      if(L_eti) then
         I0_exe=I0_ape
      else
         I0_exe=I0_ere
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 206, 298):���� RE IN LO CH7
      if(L_iri) then
         I_uve=I0_axe
      else
         I_uve=I0_exe
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 297):���� RE IN LO CH7
      if(L_are) then
         C20_eke=C20_ike
      else
         C20_eke=C20_oke
      endif
C TOLKATEL_HANDLER_DOZ.fmg(  80, 256):���� RE IN LO CH20
      if(L_ave) then
         C20_uke=C20_ake
      else
         C20_uke=C20_eke
      endif
C TOLKATEL_HANDLER_DOZ.fmg(  98, 256):���� RE IN LO CH20
      if(L_are) then
         I_ar=I0_er
      else
         I_ar=I0_ir
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 344):���� RE IN LO CH7
      if(L_are) then
         I_ede=I0_ide
      else
         I_ede=I0_ode
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 334):���� RE IN LO CH7
      R_as = R0_et * R0_ef
C TOLKATEL_HANDLER_DOZ.fmg( 344, 372):����������
      L0_api = L_avi.OR.L_eti
C TOLKATEL_HANDLER_DOZ.fmg( 304, 241):���
      L0_epi = L0_api.AND.(.NOT.L_imi)
C TOLKATEL_HANDLER_DOZ.fmg( 319, 240):�
      if(L0_epi) then
         R0_ari=R_opi
      else
         R0_ari=R0_ipi
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 322, 246):���� RE IN LO CH7
      if(L_eti) then
         I_ufe=I0_ife
      else
         I_ufe=I0_ofe
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 284):���� RE IN LO CH7
      if(L_avi) then
         I_efe=I0_ude
      else
         I_efe=I0_afe
      endif
C TOLKATEL_HANDLER_DOZ.fmg( 225, 250):���� RE IN LO CH7
      R0_upi = R8_eri
C TOLKATEL_HANDLER_DOZ.fmg( 304, 252):��������
C label 348  try348=try348-1
      R8_eri = R0_ari + R0_upi
C TOLKATEL_HANDLER_DOZ.fmg( 328, 251):��������
C sav1=R0_upi
      R0_upi = R8_eri
C TOLKATEL_HANDLER_DOZ.fmg( 304, 252):recalc:��������
C if(sav1.ne.R0_upi .and. try348.gt.0) goto 348
      End
