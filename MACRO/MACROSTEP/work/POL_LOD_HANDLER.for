      Subroutine POL_LOD_HANDLER(ext_deltat,L_u,R_ad,R_ed
     &,L_id,R_od,R_ud,L_af,R_ef,R_if,L_of,L_ek,L_ik,I_il)
C |L_u           |1 1 O|fail_pol_lod_button_CMD*|[TF]����� ������ |F|
C |R_ad          |4 4 S|fail_pol_lod_button_ST*|��������� ������ "" |0.0|
C |R_ed          |4 4 I|fail_pol_lod_button|������� ������ ������ "" |0.0|
C |L_id          |1 1 O|lod_button_CMD* |[TF]����� ������ |F|
C |R_od          |4 4 S|lod_button_ST*  |��������� ������ "" |0.0|
C |R_ud          |4 4 I|lod_button      |������� ������ ������ "" |0.0|
C |L_af          |1 1 O|pod_button_CMD* |[TF]����� ������ |F|
C |R_ef          |4 4 S|pod_button_ST*  |��������� ������ "" |0.0|
C |R_if          |4 4 I|pod_button      |������� ������ ������ "" |0.0|
C |L_of          |1 1 I|lod             |������� �������||
C |L_ek          |1 1 I|fail_pol_lod    |���������� �������� � �������||
C |L_ik          |1 1 I|pod             |������� ��������||
C |I_il          |2 4 O|LS              |��������� �����||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i
      LOGICAL*1 L0_o,L_u
      REAL*4 R_ad,R_ed
      LOGICAL*1 L_id
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af
      REAL*4 R_ef,R_if
      LOGICAL*1 L_of,L0_uf
      INTEGER*4 I0_ak
      LOGICAL*1 L_ek,L_ik,L0_ok
      INTEGER*4 I0_uk,I0_al,I0_el,I_il

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_e = z'01000005'
C POL_LOD_HANDLER.fmg( 130, 186):��������� ������������� IN (�������)
      I0_al = z'01000021'
C POL_LOD_HANDLER.fmg( 166, 183):��������� ������������� IN (�������)
      I0_i = z'0100000A'
C POL_LOD_HANDLER.fmg( 130, 184):��������� ������������� IN (�������)
      I0_ak = z'01000005'
C POL_LOD_HANDLER.fmg( 206, 182):��������� ������������� IN (�������)
      L_u=R_ed.ne.R_ad
      R_ad=R_ed
C POL_LOD_HANDLER.fmg( 172, 161):���������� ������������� ������
      L0_uf = L_ek.OR.L_u
C POL_LOD_HANDLER.fmg( 204, 171):���
      L_id=R_ud.ne.R_od
      R_od=R_ud
C POL_LOD_HANDLER.fmg(  92, 175):���������� ������������� ������
      L0_o = L_of.OR.L_id
C POL_LOD_HANDLER.fmg( 126, 178):���
      if(L0_o) then
         I0_el=I0_i
      else
         I0_el=I0_e
      endif
C POL_LOD_HANDLER.fmg( 133, 184):���� RE IN LO CH7
      L_af=R_if.ne.R_ef
      R_ef=R_if
C POL_LOD_HANDLER.fmg( 140, 168):���������� ������������� ������
      L0_ok = L_ik.OR.L_af
C POL_LOD_HANDLER.fmg( 164, 176):���
      if(L0_ok) then
         I0_uk=I0_al
      else
         I0_uk=I0_el
      endif
C POL_LOD_HANDLER.fmg( 170, 184):���� RE IN LO CH7
      if(L0_uf) then
         I_il=I0_ak
      else
         I_il=I0_uk
      endif
C POL_LOD_HANDLER.fmg( 209, 182):���� RE IN LO CH7
      End
