      Subroutine TOLKATEL_OY_HANDLER(ext_deltat,R_abi,R_e
     &,R_ed,R_ef,R_of,R_uf,R_ak,R_ek,L_up,R_as,R_ex,L_ix,R_ux
     &,L_abe,L_ede,L_ude,R_uke,R_ole,L_ame,R_ime,L_ume,R_ape
     &,R_epe,L_ope,L_ore,L_ase,L_ese,L_ose,L_ete,L_ute,L_eve
     &,R_ebi,L_ubi,L_edi,L_odi,L_udi,L_ifi,L_ofi,C20_uli,L_ami
     &,L_imi,L_omi,L_epi,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri
     &,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
     &,L_iti,R_oti,R_uti,R_avi,R_evi,R_ivi,I_ovi,R_ixi,R_uxi
     &,R_abo,L_obo,R_ado,C20_iko,C20_ilo,I_emo,I_umo,R_apo
     &,R_epo,R_ipo,R_opo,L_upo,L_aro,I_ero,I_uro,I_uso,I_ito
     &,C20_ivo,C8_ixo,R_afu,R_efu,L_ifu,R_ofu,R_ufu,I_aku
     &,L_oku,L_uku,I_elu,I_ulu,L_umu,L_epu,L_ipu,L_opu,L_upu
     &,L_aru,L_asu,L_atu,L_itu,L_otu,R8_avu,R_uvu,R8_ixu,L_abad
     &,L_obad,I_ubad,L_idad,L_odad,L_ofad,L_elad,L_emad)
C |R_abi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 S|_slpbJ3665*     |���������� ��������� ||
C |R_ed          |4 4 I|VX01            |||
C |R_ef          |4 4 I|p_TOLKATEL_XH54 |||
C |R_of          |4 4 S|_slpbJ3618*     |���������� ��������� ||
C |R_uf          |4 4 S|_slpbJ3617*     |���������� ��������� ||
C |R_ak          |4 4 S|_slpbJ3616*     |���������� ��������� ||
C |R_ek          |4 4 S|_slpbJ3610*     |���������� ��������� ||
C |L_up          |1 1 S|_qffJ3566*      |�������� ������ Q RS-��������  |F|
C |R_as          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ex          |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ix          |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ux          |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_abe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ede         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ude         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_uke         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ole         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_ame         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ime         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ume         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ape         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |R_epe         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_ope         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ore         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ase         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ese         |1 1 I|OUTC            |�����||
C |L_ose         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ete         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ute         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 I|OUTO            |������||
C |R_ebi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ubi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_edi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_odi         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_ifi         |1 1 O|limit_switch_error|||
C |L_ofi         |1 1 O|flag_mlf19      |||
C |C20_uli       |3 20 O|task_state      |���������||
C |L_ami         |1 1 I|vlv_kvit        |||
C |L_imi         |1 1 I|instr_fault     |||
C |L_omi         |1 1 S|_qffJ3139*      |�������� ������ Q RS-��������  |F|
C |L_epi         |1 1 O|norm            |�����||
C |L_ipi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_opi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_upi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ari         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_eri         |1 1 I|mlf07           |���������� ���� �������||
C |L_iri         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_ori         |1 1 I|mlf09           |����� ��������� �����||
C |L_uri         |1 1 I|mlf08           |����� ��������� ������||
C |L_asi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_esi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_isi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_osi         |1 1 I|mlf23           |������� ������� �����||
C |L_usi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ati         |1 1 I|mlf19           |���� ������������||
C |L_eti         |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |L_iti         |1 1 I|YA25C           |������� ������� �� ����������|F|
C |R_oti         |4 4 I|tcl_top         |�������� �������|20|
C |R_uti         |4 4 K|_uintT_INT_1    |����������� ������ ����������� ������|`p_TOLKATEL_UP`|
C |R_avi         |4 4 K|_tintT_INT_1    |[���]�������� T �����������|1|
C |R_evi         |4 4 O|_ointT_INT_1*   |�������� ������ ����������� |0|
C |R_ivi         |4 4 K|_lintT_INT_1    |����������� ������ ����������� �����|0.0|
C |I_ovi         |2 4 O|LWORK           |����� �������||
C |R_ixi         |4 4 K|_lcmpJ2850      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_uxi         |4 4 K|_lcmpJ2849      |[]�������� ������ �����������|1.0|
C |R_abo         |4 4 O|VY01            |��������� ��������� �� ��� X||
C |L_obo         |1 1 I|mlf04           |���������������� �������� �����||
C |R_ado         |4 4 O|VY02            |�������� ����������� ���������||
C |C20_iko       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_ilo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_emo         |2 4 O|LREADY          |����� ����������||
C |I_umo         |2 4 O|LBUSY           |����� �����||
C |R_apo         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_epo         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_ipo         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_opo         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_upo         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_aro         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_ero         |2 4 O|state1          |��������� 1||
C |I_uro         |2 4 O|state2          |��������� 2||
C |I_uso         |2 4 O|LWORKO          |����� � �������||
C |I_ito         |2 4 O|LINITC          |����� � ��������||
C |C20_ivo       |3 20 O|task_name       |������� ���������||
C |C8_ixo        |3 8 I|task            |������� ���������||
C |R_afu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_efu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ifu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ofu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ufu         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_aku         |2 4 O|LERROR          |����� �������������||
C |L_oku         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_uku         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_elu         |2 4 O|LINIT           |����� ��������||
C |I_ulu         |2 4 O|LZM             |������ "�������"||
C |L_umu         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_epu         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ipu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_opu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_upu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_aru         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_asu         |1 1 O|block           |||
C |L_atu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_itu         |1 1 O|STOP            |�������||
C |L_otu         |1 1 O|nopower         |��� ����������||
C |R8_avu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_uvu         |4 4 I|power           |�������� ��������||
C |R8_ixu        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_abad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_obad        |1 1 O|fault           |�������������||
C |I_ubad        |2 4 O|LAM             |������ "�������"||
C |L_idad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_odad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_ofad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_elad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_emad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R0_i,R0_o,R0_u,R0_ad,R_ed
      LOGICAL*1 L0_id,L0_od,L0_ud,L0_af
      REAL*4 R_ef,R0_if,R_of,R_uf,R_ak,R_ek,R0_ik,R0_ok,R0_uk
     &,R0_al,R0_el,R0_il,R0_ol,R0_ul,R0_am,R0_em
      LOGICAL*1 L0_im
      REAL*4 R0_om,R0_um,R0_ap,R0_ep,R0_ip
      LOGICAL*1 L0_op,L_up
      REAL*4 R0_ar,R0_er,R0_ir,R0_or,R0_ur,R_as,R0_es,R0_is
     &,R0_os,R0_us,R0_at
      LOGICAL*1 L0_et,L0_it
      REAL*4 R0_ot
      LOGICAL*1 L0_ut,L0_av,L0_ev,L0_iv,L0_ov,L0_uv
      REAL*4 R0_ax,R_ex
      LOGICAL*1 L_ix
      REAL*4 R0_ox,R_ux
      LOGICAL*1 L_abe,L0_ebe,L0_ibe,L0_obe,L0_ube,L0_ade,L_ede
     &,L0_ide,L0_ode,L_ude,L0_afe,L0_efe,L0_ife
      REAL*4 R0_ofe,R0_ufe
      LOGICAL*1 L0_ake,L0_eke
      REAL*4 R0_ike,R0_oke,R_uke
      LOGICAL*1 L0_ale,L0_ele
      REAL*4 R0_ile,R_ole
      LOGICAL*1 L0_ule,L_ame
      REAL*4 R0_eme,R_ime
      LOGICAL*1 L0_ome,L_ume
      REAL*4 R_ape,R_epe
      LOGICAL*1 L0_ipe,L_ope,L0_upe,L0_are,L0_ere,L0_ire,L_ore
     &,L0_ure,L_ase,L_ese,L0_ise,L_ose,L0_use,L0_ate,L_ete
     &,L0_ite,L0_ote,L_ute,L0_ave,L_eve,L0_ive
      REAL*4 R0_ove,R0_uve,R0_axe
      LOGICAL*1 L0_exe
      REAL*4 R0_ixe
      LOGICAL*1 L0_oxe,L0_uxe
      REAL*4 R_abi,R_ebi
      LOGICAL*1 L0_ibi,L0_obi,L_ubi,L0_adi,L_edi,L0_idi,L_odi
     &,L_udi,L0_afi,L0_efi,L_ifi,L_ofi,L0_ufi,L0_aki,L0_eki
     &,L0_iki,L0_oki,L0_uki
      CHARACTER*20 C20_ali,C20_eli,C20_ili,C20_oli,C20_uli
      LOGICAL*1 L_ami,L0_emi,L_imi,L_omi,L0_umi,L0_api,L_epi
     &,L_ipi,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi
     &,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
      LOGICAL*1 L_iti
      REAL*4 R_oti,R_uti,R_avi,R_evi,R_ivi
      INTEGER*4 I_ovi,I0_uvi,I0_axi
      LOGICAL*1 L0_exi
      REAL*4 R_ixi
      LOGICAL*1 L0_oxi
      REAL*4 R_uxi,R_abo
      LOGICAL*1 L0_ebo,L0_ibo,L_obo
      REAL*4 R0_ubo,R_ado,R0_edo,R0_ido,R0_odo,R0_udo,R0_afo
      LOGICAL*1 L0_efo,L0_ifo
      CHARACTER*20 C20_ofo,C20_ufo,C20_ako,C20_eko,C20_iko
     &,C20_oko,C20_uko,C20_alo,C20_elo,C20_ilo
      INTEGER*4 I0_olo,I0_ulo
      LOGICAL*1 L0_amo
      INTEGER*4 I_emo,I0_imo,I0_omo,I_umo
      REAL*4 R_apo,R_epo,R_ipo,R_opo
      LOGICAL*1 L_upo,L_aro
      INTEGER*4 I_ero,I0_iro,I0_oro,I_uro,I0_aso,I0_eso,I0_iso
     &,I0_oso,I_uso,I0_ato,I0_eto,I_ito
      CHARACTER*20 C20_oto,C20_uto,C20_avo,C20_evo,C20_ivo
      CHARACTER*8 C8_ovo
      LOGICAL*1 L0_uvo
      CHARACTER*8 C8_axo
      LOGICAL*1 L0_exo
      CHARACTER*8 C8_ixo
      LOGICAL*1 L0_oxo
      INTEGER*4 I0_uxo
      LOGICAL*1 L0_abu
      INTEGER*4 I0_ebu
      LOGICAL*1 L0_ibu
      INTEGER*4 I0_obu,I0_ubu,I0_adu
      LOGICAL*1 L0_edu
      INTEGER*4 I0_idu,I0_odu,I0_udu
      REAL*4 R_afu,R_efu
      LOGICAL*1 L_ifu
      REAL*4 R_ofu,R_ufu
      INTEGER*4 I_aku,I0_eku,I0_iku
      LOGICAL*1 L_oku,L_uku,L0_alu
      INTEGER*4 I_elu,I0_ilu,I0_olu,I_ulu,I0_amu,I0_emu
      LOGICAL*1 L0_imu,L0_omu,L_umu,L0_apu,L_epu,L_ipu,L_opu
     &,L_upu,L_aru,L0_eru,L0_iru,L0_oru,L0_uru,L_asu,L0_esu
     &,L0_isu,L0_osu,L0_usu,L_atu,L0_etu,L_itu,L_otu
      REAL*4 R0_utu
      REAL*8 R8_avu
      LOGICAL*1 L0_evu,L0_ivu
      REAL*4 R0_ovu,R_uvu,R0_axu,R0_exu
      REAL*8 R8_ixu
      LOGICAL*1 L0_oxu,L0_uxu,L_abad,L0_ebad,L0_ibad,L_obad
      INTEGER*4 I_ubad,I0_adad,I0_edad
      LOGICAL*1 L_idad,L_odad,L0_udad,L0_afad,L0_efad,L0_ifad
     &,L_ofad,L0_ufad,L0_akad,L0_ekad,L0_ikad,L0_okad,L0_ukad
     &,L0_alad,L_elad,L0_ilad,L0_olad,L0_ulad,L0_amad,L_emad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_us=R_e
C TOLKATEL_OY_HANDLER.fmg( 347, 458):pre: ����������� ��
      R0_al=R_uf
C TOLKATEL_OY_HANDLER.fmg( 400, 396):pre: ����������� ��
      R0_il=R_of
C TOLKATEL_OY_HANDLER.fmg( 392, 402):pre: ����������� ��
      R0_ir=R_ak
C TOLKATEL_OY_HANDLER.fmg( 355, 403):pre: ����������� ��
      R0_ep=R_ek
C TOLKATEL_OY_HANDLER.fmg( 325, 392):pre: ����������� ��
      R0_ox=R_ux
C TOLKATEL_OY_HANDLER.fmg( 258, 344):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_ax=R_ex
C TOLKATEL_OY_HANDLER.fmg( 258, 332):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_eme=R_ime
C TOLKATEL_OY_HANDLER.fmg( 242, 308):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_ile=R_ole
C TOLKATEL_OY_HANDLER.fmg( 242, 294):pre: �������� ��������� ������,nv2dyn$83Dasha
      R0_i = 1.0
C TOLKATEL_OY_HANDLER.fmg( 360, 418):��������� (RE4) (�������)
      R0_o = 1.0
C TOLKATEL_OY_HANDLER.fmg( 360, 424):��������� (RE4) (�������)
      R0_if = R_as * R_ef
C TOLKATEL_OY_HANDLER.fmg( 354, 438):����������
      R0_u = R0_if + (-R0_i)
C TOLKATEL_OY_HANDLER.fmg( 364, 420):��������
      L0_id=R_ed.gt.R0_u
C TOLKATEL_OY_HANDLER.fmg( 344, 420):���������� >
      R0_ad = R0_if + R0_o
C TOLKATEL_OY_HANDLER.fmg( 364, 426):��������
      L0_od=R_ed.lt.R0_ad
C TOLKATEL_OY_HANDLER.fmg( 344, 426):���������� <
      L0_ud = L0_od.AND.L0_id
C TOLKATEL_OY_HANDLER.fmg( 350, 424):�
      L0_af = L_ati.AND.L0_ud
C TOLKATEL_OY_HANDLER.fmg( 354, 430):�
      R0_ik = 0.0
C TOLKATEL_OY_HANDLER.fmg( 434, 404):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_uk = 0.99
C TOLKATEL_OY_HANDLER.fmg( 416, 405):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_osi) then
         R0_em=R0_al
      endif
C TOLKATEL_OY_HANDLER.fmg( 404, 402):���� � ������������� �������
      R0_ol = 1.0
C TOLKATEL_OY_HANDLER.fmg( 369, 402):��������� (RE4) (�������)
      R0_ul = 0.0
C TOLKATEL_OY_HANDLER.fmg( 361, 402):��������� (RE4) (�������)
      R0_um = 1.0e-10
C TOLKATEL_OY_HANDLER.fmg( 339, 379):��������� (RE4) (�������)
      R0_ar = 0.0
C TOLKATEL_OY_HANDLER.fmg( 349, 393):��������� (RE4) (�������)
      R0_ip = R0_ep + (-R_as)
C TOLKATEL_OY_HANDLER.fmg( 328, 386):��������
      R0_es = 0.33
C TOLKATEL_OY_HANDLER.fmg( 282, 470):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_is = R_oti * R0_es
C TOLKATEL_OY_HANDLER.fmg( 288, 472):����������
      if(L_eri) then
         R0_edo=R0_is
      else
         R0_edo=R_oti
      endif
C TOLKATEL_OY_HANDLER.fmg( 294, 473):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_at = 0.000001
C TOLKATEL_OY_HANDLER.fmg( 150, 425):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_et=R_uke.lt.R0_at
C TOLKATEL_OY_HANDLER.fmg( 157, 426):���������� <,nv2dyn$46Dasha
      L0_it = L_iri.AND.L0_et
C TOLKATEL_OY_HANDLER.fmg( 164, 432):�
      R0_ot = 0.999999
C TOLKATEL_OY_HANDLER.fmg( 144, 478):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ut=R_uke.gt.R0_ot
C TOLKATEL_OY_HANDLER.fmg( 153, 479):���������� >,nv2dyn$45Dasha
      L0_av = L_asi.AND.L0_ut
C TOLKATEL_OY_HANDLER.fmg( 163, 485):�
      R0_ofe = 0.000001
C TOLKATEL_OY_HANDLER.fmg( 215, 256):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ufe = 0.999999
C TOLKATEL_OY_HANDLER.fmg( 214, 262):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_ike = 0.000001
C TOLKATEL_OY_HANDLER.fmg( 215, 274):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ale=R_uke.lt.R0_ike
C TOLKATEL_OY_HANDLER.fmg( 220, 275):���������� <,nv2dyn$46Dasha
      L0_ufi = L_iri.AND.L0_ale
C TOLKATEL_OY_HANDLER.fmg( 252, 273):�
      R0_oke = 0.999999
C TOLKATEL_OY_HANDLER.fmg( 214, 280):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ele=R_uke.gt.R0_oke
C TOLKATEL_OY_HANDLER.fmg( 219, 281):���������� >,nv2dyn$45Dasha
      L0_aki = L_asi.AND.L0_ele
C TOLKATEL_OY_HANDLER.fmg( 252, 284):�
      if(.not.L_eri) then
         R_ole=0.0
      elseif(.not.L_ame) then
         R_ole=R_ape
      else
         R_ole=max(R0_ile-deltat,0.0)
      endif
      L0_ule=L_eri.and.R_ole.le.0.0
      L_ame=L_eri
C TOLKATEL_OY_HANDLER.fmg( 242, 294):�������� ��������� ������,nv2dyn$83Dasha
      if(.not.L_eri) then
         R_ime=0.0
      elseif(.not.L_ume) then
         R_ime=R_epe
      else
         R_ime=max(R0_eme-deltat,0.0)
      endif
      L0_ome=L_eri.and.R_ime.le.0.0
      L_ume=L_eri
C TOLKATEL_OY_HANDLER.fmg( 242, 308):�������� ��������� ������,nv2dyn$82Dasha
      R0_ove = 0.01
C TOLKATEL_OY_HANDLER.fmg(  38, 282):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_uve = R0_ove + R_abi
C TOLKATEL_OY_HANDLER.fmg(  42, 282):��������
      R0_axe = 0.01
C TOLKATEL_OY_HANDLER.fmg(  38, 289):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ixe = (-R0_axe) + R_abi
C TOLKATEL_OY_HANDLER.fmg(  42, 288):��������
      C20_ili = '��������� 2'
C TOLKATEL_OY_HANDLER.fmg(  31, 308):��������� ���������� CH20 (CH30) (�������)
      C20_oli = '������� ���'
C TOLKATEL_OY_HANDLER.fmg(  31, 310):��������� ���������� CH20 (CH30) (�������)
      C20_ali = '��������� 1'
C TOLKATEL_OY_HANDLER.fmg(  46, 307):��������� ���������� CH20 (CH30) (�������)
      L_omi=(L_imi.or.L_omi).and..not.(L_ami)
      L0_emi=.not.L_omi
C TOLKATEL_OY_HANDLER.fmg( 294, 272):RS �������
      L0_ebad = L_aro.OR.L_eti
C TOLKATEL_OY_HANDLER.fmg(  74, 432):���
      L0_oxu = L_upo.OR.L_iti.OR.L_obo
C TOLKATEL_OY_HANDLER.fmg(  72, 389):���
      I0_axi = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 128, 472):��������� ������������� IN (�������)
      I0_uvi = z'0100000A'
C TOLKATEL_OY_HANDLER.fmg( 128, 470):��������� ������������� IN (�������)
      R0_ubo = 0.0
C TOLKATEL_OY_HANDLER.fmg( 318, 464):��������� (RE4) (�������)
      R0_afo = 0.0
C TOLKATEL_OY_HANDLER.fmg( 304, 464):��������� (RE4) (�������)
      C20_ufo = ''
C TOLKATEL_OY_HANDLER.fmg( 174, 360):��������� ���������� CH20 (CH30) (�������)
      C20_ofo = '�������'
C TOLKATEL_OY_HANDLER.fmg( 174, 358):��������� ���������� CH20 (CH30) (�������)
      C20_ako = '������'
C TOLKATEL_OY_HANDLER.fmg( 194, 360):��������� ���������� CH20 (CH30) (�������)
      C20_uko = ''
C TOLKATEL_OY_HANDLER.fmg(  80, 362):��������� ���������� CH20 (CH30) (�������)
      C20_oko = '� ��������� 2'
C TOLKATEL_OY_HANDLER.fmg(  80, 360):��������� ���������� CH20 (CH30) (�������)
      C20_alo = '� ��������� 1'
C TOLKATEL_OY_HANDLER.fmg( 101, 360):��������� ���������� CH20 (CH30) (�������)
      I0_olo = z'0100000A'
C TOLKATEL_OY_HANDLER.fmg( 226, 472):��������� ������������� IN (�������)
      I0_ulo = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 226, 474):��������� ������������� IN (�������)
      I0_omo = z'0100000A'
C TOLKATEL_OY_HANDLER.fmg( 226, 493):��������� ������������� IN (�������)
      I0_imo = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 226, 491):��������� ������������� IN (�������)
      L_umu=R_epo.ne.R_apo
      R_apo=R_epo
C TOLKATEL_OY_HANDLER.fmg(  20, 396):���������� ������������� ������
      L_epu=R_opo.ne.R_ipo
      R_ipo=R_opo
C TOLKATEL_OY_HANDLER.fmg(  24, 451):���������� ������������� ������
      I0_oro = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 144, 365):��������� ������������� IN (�������)
      I0_iro = z'01000010'
C TOLKATEL_OY_HANDLER.fmg( 144, 363):��������� ������������� IN (�������)
      I0_eso = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 122, 454):��������� ������������� IN (�������)
      I0_aso = z'01000010'
C TOLKATEL_OY_HANDLER.fmg( 122, 452):��������� ������������� IN (�������)
      I0_oso = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 190, 432):��������� ������������� IN (�������)
      I0_iso = z'0100000A'
C TOLKATEL_OY_HANDLER.fmg( 190, 430):��������� ������������� IN (�������)
      I0_eto = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 198, 382):��������� ������������� IN (�������)
      I0_ato = z'0100000A'
C TOLKATEL_OY_HANDLER.fmg( 198, 380):��������� ������������� IN (�������)
      I0_olu = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 120, 378):��������� ������������� IN (�������)
      I0_ilu = z'0100000A'
C TOLKATEL_OY_HANDLER.fmg( 120, 376):��������� ������������� IN (�������)
      C20_oto = '� ��������'
C TOLKATEL_OY_HANDLER.fmg(  56, 374):��������� ���������� CH20 (CH30) (�������)
      C20_avo = '� �������'
C TOLKATEL_OY_HANDLER.fmg(  40, 375):��������� ���������� CH20 (CH30) (�������)
      C20_evo = ''
C TOLKATEL_OY_HANDLER.fmg(  40, 377):��������� ���������� CH20 (CH30) (�������)
      C8_ovo = 'init'
C TOLKATEL_OY_HANDLER.fmg(  24, 384):��������� ���������� CH8 (�������)
      call chcomp(C8_ixo,C8_ovo,L0_uvo)
C TOLKATEL_OY_HANDLER.fmg(  28, 387):���������� ���������
      C8_axo = 'work'
C TOLKATEL_OY_HANDLER.fmg(  24, 426):��������� ���������� CH8 (�������)
      call chcomp(C8_ixo,C8_axo,L0_exo)
C TOLKATEL_OY_HANDLER.fmg(  28, 429):���������� ���������
      if(L0_exo) then
         C20_elo=C20_oko
      else
         C20_elo=C20_uko
      endif
C TOLKATEL_OY_HANDLER.fmg(  84, 360):���� RE IN LO CH20
      if(L0_uvo) then
         C20_ilo=C20_alo
      else
         C20_ilo=C20_elo
      endif
C TOLKATEL_OY_HANDLER.fmg( 106, 361):���� RE IN LO CH20
      if(L0_exo) then
         C20_eko=C20_ofo
      else
         C20_eko=C20_ufo
      endif
C TOLKATEL_OY_HANDLER.fmg( 178, 358):���� RE IN LO CH20
      if(L0_uvo) then
         C20_iko=C20_ako
      else
         C20_iko=C20_eko
      endif
C TOLKATEL_OY_HANDLER.fmg( 199, 360):���� RE IN LO CH20
      if(L0_exo) then
         C20_uto=C20_avo
      else
         C20_uto=C20_evo
      endif
C TOLKATEL_OY_HANDLER.fmg(  44, 376):���� RE IN LO CH20
      if(L0_uvo) then
         C20_ivo=C20_oto
      else
         C20_ivo=C20_uto
      endif
C TOLKATEL_OY_HANDLER.fmg(  60, 374):���� RE IN LO CH20
      I0_uxo = z'0100008E'
C TOLKATEL_OY_HANDLER.fmg( 175, 400):��������� ������������� IN (�������)
      I0_ebu = z'01000086'
C TOLKATEL_OY_HANDLER.fmg( 170, 452):��������� ������������� IN (�������)
      I0_ubu = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 158, 465):��������� ������������� IN (�������)
      I0_adu = z'01000010'
C TOLKATEL_OY_HANDLER.fmg( 158, 467):��������� ������������� IN (�������)
      I0_udu = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 164, 412):��������� ������������� IN (�������)
      I0_odu = z'01000010'
C TOLKATEL_OY_HANDLER.fmg( 164, 410):��������� ������������� IN (�������)
      L_uku=R_efu.ne.R_afu
      R_afu=R_efu
C TOLKATEL_OY_HANDLER.fmg(  20, 416):���������� ������������� ������
      L0_alu = L_uku.OR.L_oku
C TOLKATEL_OY_HANDLER.fmg(  54, 414):���
      L_ifu=R_ufu.ne.R_ofu
      R_ofu=R_ufu
C TOLKATEL_OY_HANDLER.fmg(  24, 438):���������� ������������� ������
      L0_apu = L_ifu.AND.L0_exo
C TOLKATEL_OY_HANDLER.fmg(  36, 437):�
      L0_isu = L_epu.OR.L0_apu
C TOLKATEL_OY_HANDLER.fmg(  57, 438):���
      L0_ife = L_opu.AND.L0_isu
C TOLKATEL_OY_HANDLER.fmg( 237, 344):�
      L0_ade = L0_isu.OR.(.NOT.L_ipu)
C TOLKATEL_OY_HANDLER.fmg( 242, 328):���
      L0_omu = L_ifu.AND.L0_uvo
C TOLKATEL_OY_HANDLER.fmg(  34, 395):�
      L0_esu = L_umu.OR.L0_omu
C TOLKATEL_OY_HANDLER.fmg(  58, 396):���
      L0_efe = L_ipu.AND.L0_esu
C TOLKATEL_OY_HANDLER.fmg( 237, 332):�
      L_ede=(L0_efe.or.L_ede).and..not.(L0_ade)
      L0_ide=.not.L_ede
C TOLKATEL_OY_HANDLER.fmg( 248, 330):RS �������,156
      if(.not.L_ede) then
         R_ex=0.0
      elseif(.not.L_ix) then
         R_ex=R_ape
      else
         R_ex=max(R0_ax-deltat,0.0)
      endif
      L0_ebe=L_ede.and.R_ex.le.0.0
      L_ix=L_ede
C TOLKATEL_OY_HANDLER.fmg( 258, 332):�������� ��������� ������,nv2dyn$100Dasha
      L_odi=(L0_ebe.or.L_odi).and..not.(L_eve)
      L0_ibe=.not.L_odi
C TOLKATEL_OY_HANDLER.fmg( 274, 330):RS �������,nv2dyn$96Dasha
      L0_ode = (.NOT.L_opu).OR.L0_esu
C TOLKATEL_OY_HANDLER.fmg( 243, 340):���
      L_ude=(L0_ife.or.L_ude).and..not.(L0_ode)
      L0_afe=.not.L_ude
C TOLKATEL_OY_HANDLER.fmg( 248, 342):RS �������,155
      if(.not.L_ude) then
         R_ux=0.0
      elseif(.not.L_abe) then
         R_ux=R_epe
      else
         R_ux=max(R0_ox-deltat,0.0)
      endif
      L0_obe=L_ude.and.R_ux.le.0.0
      L_abe=L_ude
C TOLKATEL_OY_HANDLER.fmg( 258, 344):�������� ��������� ������,nv2dyn$99Dasha
      L_udi=(L0_obe.or.L_udi).and..not.(L_ese)
      L0_ube=.not.L_udi
C TOLKATEL_OY_HANDLER.fmg( 274, 342):RS �������,nv2dyn$95Dasha
      I0_amu = z'0100000E'
C TOLKATEL_OY_HANDLER.fmg( 194, 403):��������� ������������� IN (�������)
      I0_eku = z'01000007'
C TOLKATEL_OY_HANDLER.fmg( 150, 387):��������� ������������� IN (�������)
      I0_iku = z'01000003'
C TOLKATEL_OY_HANDLER.fmg( 150, 389):��������� ������������� IN (�������)
      I0_adad = z'0100000E'
C TOLKATEL_OY_HANDLER.fmg( 188, 458):��������� ������������� IN (�������)
      L0_uru=.false.
C TOLKATEL_OY_HANDLER.fmg(  70, 418):��������� ���������� (�������)
      L0_oru=.false.
C TOLKATEL_OY_HANDLER.fmg(  70, 416):��������� ���������� (�������)
      R0_utu = 0.1
C TOLKATEL_OY_HANDLER.fmg( 260, 364):��������� (RE4) (�������)
      L_otu=R8_avu.lt.R0_utu
C TOLKATEL_OY_HANDLER.fmg( 264, 365):���������� <
      R0_ovu = 0.0
C TOLKATEL_OY_HANDLER.fmg( 271, 384):��������� (RE4) (�������)
      L0_use = (.NOT.L_ati).AND.L_ose
C TOLKATEL_OY_HANDLER.fmg( 113, 278):�
C label 244  try244=try244-1
      L0_ate = L_ese.AND.L0_use
C TOLKATEL_OY_HANDLER.fmg( 117, 278):�
      L_ose=(L_ati.or.L_ose).and..not.(L0_ate)
      L0_ise=.not.L_ose
C TOLKATEL_OY_HANDLER.fmg( 108, 274):RS �������,nv2dyn$103Dasha
C sav1=L0_use
      L0_use = (.NOT.L_ati).AND.L_ose
C TOLKATEL_OY_HANDLER.fmg( 113, 278):recalc:�
C if(sav1.ne.L0_use .and. try244.gt.0) goto 244
      L0_upe = (.NOT.L_ati).AND.L_ope
C TOLKATEL_OY_HANDLER.fmg( 118, 264):�
C label 248  try248=try248-1
      L0_are = L_eve.AND.L0_upe
C TOLKATEL_OY_HANDLER.fmg( 122, 264):�
      L_ope=(L_ati.or.L_ope).and..not.(L0_are)
      L0_ipe=.not.L_ope
C TOLKATEL_OY_HANDLER.fmg( 114, 260):RS �������,nv2dyn$106Dasha
C sav1=L0_upe
      L0_upe = (.NOT.L_ati).AND.L_ope
C TOLKATEL_OY_HANDLER.fmg( 118, 264):recalc:�
C if(sav1.ne.L0_upe .and. try248.gt.0) goto 248
      L0_exe=R_ebi.gt.R0_ixe
C TOLKATEL_OY_HANDLER.fmg(  47, 292):���������� >,nv2dyn$84Dasha
C label 253  try253=try253-1
      R_evi=R_evi+deltat/R_avi*R_ado
      if(R_evi.gt.R_uti) then
         R_evi=R_uti
      elseif(R_evi.lt.R_ivi) then
         R_evi=R_ivi
      endif
C TOLKATEL_OY_HANDLER.fmg( 338, 451):����������,T_INT_1
      if(L_osi) then
         R0_os=R0_us
      else
         R0_os=R_evi
      endif
C TOLKATEL_OY_HANDLER.fmg( 346, 451):���� RE IN LO CH7
      if(L0_af) then
         R_abo=R0_if
      else
         R_abo=R0_os
      endif
C TOLKATEL_OY_HANDLER.fmg( 359, 450):���� RE IN LO CH7
      L0_oxi=R_abo.lt.R_uxi
C TOLKATEL_OY_HANDLER.fmg( 380, 436):���������� <
      L0_ov = L0_oxi.AND.(.NOT.L_iri)
C TOLKATEL_OY_HANDLER.fmg( 433, 436):�
      L_idad = L_ori.OR.L0_ov.OR.L_esi
C TOLKATEL_OY_HANDLER.fmg( 437, 436):���
      L0_uki = L_uri.AND.L_idad
C TOLKATEL_OY_HANDLER.fmg( 259, 319):�
      L0_exi=R_abo.gt.R_ixi
C TOLKATEL_OY_HANDLER.fmg( 380, 456):���������� >
      L0_uv = L0_exi.AND.(.NOT.L_asi)
C TOLKATEL_OY_HANDLER.fmg( 431, 454):�
      L_odad = L_uri.OR.L0_uv.OR.L_isi
C TOLKATEL_OY_HANDLER.fmg( 437, 454):���
      L0_oki = L_ori.AND.L_odad
C TOLKATEL_OY_HANDLER.fmg( 259, 314):�
      L0_iki = L0_ome.AND.(.NOT.L_odad)
C TOLKATEL_OY_HANDLER.fmg( 251, 307):�
      L0_eki = L0_ule.AND.(.NOT.L_idad)
C TOLKATEL_OY_HANDLER.fmg( 248, 294):�
      L0_ive=R_ebi.lt.R0_uve
C TOLKATEL_OY_HANDLER.fmg(  49, 286):���������� <,nv2dyn$85Dasha
      L0_oxe = L0_exe.AND.L0_ive
C TOLKATEL_OY_HANDLER.fmg(  54, 292):�
      L0_uxe = L0_oxe.AND.L_ati
C TOLKATEL_OY_HANDLER.fmg(  66, 284):�
      L_ute=(L_eve.or.L_ute).and..not.(L_idad)
      L0_ave=.not.L_ute
C TOLKATEL_OY_HANDLER.fmg(  66, 276):RS �������,nv2dyn$101Dasha
      L0_ote = L0_uxe.AND.L_ute
C TOLKATEL_OY_HANDLER.fmg(  85, 282):�
      L_ete=(L0_ote.or.L_ete).and..not.(L0_ate)
      L0_ite=.not.L_ete
C TOLKATEL_OY_HANDLER.fmg( 122, 280):RS �������,nv2dyn$102Dasha
      L_ore=(L_ese.or.L_ore).and..not.(L_odad)
      L0_ire=.not.L_ore
C TOLKATEL_OY_HANDLER.fmg(  72, 266):RS �������,nv2dyn$104Dasha
      L0_ure = L0_uxe.AND.L_ore
C TOLKATEL_OY_HANDLER.fmg(  85, 268):�
      L_ase=(L0_ure.or.L_ase).and..not.(L0_are)
      L0_ere=.not.L_ase
C TOLKATEL_OY_HANDLER.fmg( 130, 266):RS �������,nv2dyn$105Dasha
      L_ofi = L_ete.OR.L_ase
C TOLKATEL_OY_HANDLER.fmg( 144, 282):���
      L_edi=(L_odad.or.L_edi).and..not.(L_idad)
      L0_obi=.not.L_edi
C TOLKATEL_OY_HANDLER.fmg( 123, 300):RS �������,nv2dyn$86Dasha
      L0_idi = L_isi.AND.(.NOT.L_edi)
C TOLKATEL_OY_HANDLER.fmg( 132, 302):�
      L_ubi=(L_idad.or.L_ubi).and..not.(L_odad)
      L0_ibi=.not.L_ubi
C TOLKATEL_OY_HANDLER.fmg( 122, 288):RS �������,nv2dyn$87Dasha
      L0_adi = L_esi.AND.(.NOT.L_ubi)
C TOLKATEL_OY_HANDLER.fmg( 132, 294):�
      L_ifi = L0_idi.OR.L0_adi
C TOLKATEL_OY_HANDLER.fmg( 138, 302):���
      L0_eke=R_ebi.gt.R0_ufe
C TOLKATEL_OY_HANDLER.fmg( 219, 264):���������� >,nv2dyn$45Dasha
      L0_efi = L_upi.AND.L0_eke
C TOLKATEL_OY_HANDLER.fmg( 252, 266):�
      L0_ake=R_ebi.lt.R0_ofe
C TOLKATEL_OY_HANDLER.fmg( 220, 258):���������� <,nv2dyn$46Dasha
      L0_afi = L_ipi.AND.L0_ake
C TOLKATEL_OY_HANDLER.fmg( 252, 256):�
      L0_umi =.NOT.(L_abad.OR.L_obo.OR.L0_uki.OR.L0_oki.OR.L0_iki.OR.L0_
     &eki.OR.L0_aki.OR.L0_ufi.OR.L_osi.OR.L_ari.OR.L_opi.OR.L_ofi.OR.L_i
     &fi.OR.L0_efi.OR.L0_afi.OR.L_udi.OR.L_odi)
C TOLKATEL_OY_HANDLER.fmg( 286, 307):���
      L0_api =.NOT.(L0_umi)
C TOLKATEL_OY_HANDLER.fmg( 296, 279):���
      L_obad = L0_api.OR.L_omi
C TOLKATEL_OY_HANDLER.fmg( 300, 278):���
      L0_iru = L_obad.OR.L_upu
C TOLKATEL_OY_HANDLER.fmg(  64, 446):���
      L0_ibad = L0_isu.AND.(.NOT.L0_iru).AND.(.NOT.L_opu)
C TOLKATEL_OY_HANDLER.fmg(  74, 439):�
      L0_olad = L0_ibad.OR.L0_ebad.OR.L_abad
C TOLKATEL_OY_HANDLER.fmg(  83, 437):���
      L0_eru = L_obad.OR.L_aru
C TOLKATEL_OY_HANDLER.fmg(  60, 404):���
      L0_uxu = (.NOT.L0_eru).AND.L0_esu.AND.(.NOT.L_ipu)
C TOLKATEL_OY_HANDLER.fmg(  72, 396):�
      L0_ekad = L0_uxu.OR.L0_oxu
C TOLKATEL_OY_HANDLER.fmg(  76, 395):���
      L0_usu = L0_olad.OR.L0_ekad
C TOLKATEL_OY_HANDLER.fmg(  90, 406):���
      L0_osu = L_idad.OR.L0_alu.OR.L_odad
C TOLKATEL_OY_HANDLER.fmg(  66, 410):���
      L_atu=(L0_osu.or.L_atu).and..not.(L0_usu)
      L0_etu=.not.L_atu
C TOLKATEL_OY_HANDLER.fmg( 112, 408):RS �������,10
      L_itu = L_oku.OR.L_atu
C TOLKATEL_OY_HANDLER.fmg( 122, 411):���
      L0_ulad = L_odad.AND.(.NOT.L_uri)
C TOLKATEL_OY_HANDLER.fmg(  76, 467):�
      L0_okad = (.NOT.L_itu).AND.L0_ulad
C TOLKATEL_OY_HANDLER.fmg( 110, 463):�
      L0_udad = L_itu.OR.L_emad
C TOLKATEL_OY_HANDLER.fmg( 103, 417):���
      L0_iv = L_odad.AND.L_opi
C TOLKATEL_OY_HANDLER.fmg(  52, 349):�
      L0_ev = L_idad.AND.L_ari
C TOLKATEL_OY_HANDLER.fmg(  52, 337):�
      L0_imu = L0_iv.OR.L0_ev
C TOLKATEL_OY_HANDLER.fmg(  60, 348):���
      L0_alad = L0_ulad.OR.L0_udad.OR.L0_ekad.OR.L0_imu
C TOLKATEL_OY_HANDLER.fmg( 109, 431):���
      L0_amad = (.NOT.L0_ulad).AND.L0_olad
C TOLKATEL_OY_HANDLER.fmg( 109, 438):�
      L_elad=(L0_amad.or.L_elad).and..not.(L0_alad)
      L0_ilad=.not.L_elad
C TOLKATEL_OY_HANDLER.fmg( 116, 436):RS �������,1
      L0_ukad = (.NOT.L0_okad).AND.L_elad
C TOLKATEL_OY_HANDLER.fmg( 135, 439):�
      L0_ibo = L0_ukad.AND.(.NOT.L_obo)
C TOLKATEL_OY_HANDLER.fmg( 274, 462):�
      L0_ifo = L0_ibo.OR.L_abad
C TOLKATEL_OY_HANDLER.fmg( 286, 462):���
      if(L0_ifo) then
         R0_odo=R0_edo
      else
         R0_odo=R0_afo
      endif
C TOLKATEL_OY_HANDLER.fmg( 308, 474):���� RE IN LO CH7
      L0_akad = L_idad.AND.(.NOT.L_ori)
C TOLKATEL_OY_HANDLER.fmg(  80, 368):�
      L0_ifad = L0_udad.OR.L0_akad.OR.L0_olad.OR.L0_imu
C TOLKATEL_OY_HANDLER.fmg( 109, 387):���
      L0_ikad = L0_ekad.AND.(.NOT.L0_akad)
C TOLKATEL_OY_HANDLER.fmg( 109, 394):�
      L_ofad=(L0_ikad.or.L_ofad).and..not.(L0_ifad)
      L0_ufad=.not.L_ofad
C TOLKATEL_OY_HANDLER.fmg( 116, 392):RS �������,2
      L0_afad = (.NOT.L_itu).AND.L0_akad
C TOLKATEL_OY_HANDLER.fmg( 108, 369):�
      L0_efad = L_ofad.AND.(.NOT.L0_afad)
C TOLKATEL_OY_HANDLER.fmg( 135, 393):�
      L0_ebo = L0_efad.AND.(.NOT.L_abad)
C TOLKATEL_OY_HANDLER.fmg( 274, 448):�
      L0_efo = L0_ebo.OR.L_obo
C TOLKATEL_OY_HANDLER.fmg( 286, 448):���
      if(L0_efo) then
         R0_ido=R0_edo
      else
         R0_ido=R0_afo
      endif
C TOLKATEL_OY_HANDLER.fmg( 308, 456):���� RE IN LO CH7
      R0_udo = R0_odo + (-R0_ido)
C TOLKATEL_OY_HANDLER.fmg( 314, 466):��������
      if(L_emad) then
         R_ado=R0_ubo
      else
         R_ado=R0_udo
      endif
C TOLKATEL_OY_HANDLER.fmg( 322, 465):���� RE IN LO CH7
      R0_om = R_ado + R0_ip
C TOLKATEL_OY_HANDLER.fmg( 334, 386):��������
      R0_ap = R0_om * R0_ip
C TOLKATEL_OY_HANDLER.fmg( 338, 386):����������
      L0_op=R0_ap.lt.R0_um
C TOLKATEL_OY_HANDLER.fmg( 344, 384):���������� <
      L_up=(L0_op.or.L_up).and..not.(.NOT.L_ati)
      L0_im=.not.L_up
C TOLKATEL_OY_HANDLER.fmg( 350, 382):RS �������
      if(L_up) then
         R0_er=R0_ar
      else
         R0_er=R_ado
      endif
C TOLKATEL_OY_HANDLER.fmg( 352, 394):���� RE IN LO CH7
      R0_or = R0_ir + R0_er
C TOLKATEL_OY_HANDLER.fmg( 358, 396):��������
      R0_am=MAX(R0_ul,R0_or)
C TOLKATEL_OY_HANDLER.fmg( 366, 396):��������
      R0_ur=MIN(R0_ol,R0_am)
C TOLKATEL_OY_HANDLER.fmg( 374, 398):�������
      if(L_osi) then
         R0_em=R0_ur
      endif
C TOLKATEL_OY_HANDLER.fmg( 386, 402):���� � ������������� �������
      if(L_up) then
         R0_em=R_as
      endif
C TOLKATEL_OY_HANDLER.fmg( 344, 402):���� � ������������� �������
      if(L_ari) then
         R0_ok=R0_uk
      else
         R0_ok=R0_em
      endif
C TOLKATEL_OY_HANDLER.fmg( 420, 406):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_opi) then
         R_ebi=R0_ik
      else
         R_ebi=R0_ok
      endif
C TOLKATEL_OY_HANDLER.fmg( 438, 405):���� RE IN LO CH7,nv2dyn$56Dasha
      R_ek=R0_em
C TOLKATEL_OY_HANDLER.fmg( 325, 392):����������� ��
      R_ak=R0_em
C TOLKATEL_OY_HANDLER.fmg( 355, 403):����������� ��
      if(L_osi) then
         R0_el=R0_il
      else
         R0_el=R0_ur
      endif
C TOLKATEL_OY_HANDLER.fmg( 393, 396):���� RE IN LO CH7
      R_of=R0_el
C TOLKATEL_OY_HANDLER.fmg( 392, 402):����������� ��
      R_uf=R0_el
C TOLKATEL_OY_HANDLER.fmg( 400, 396):����������� ��
      if(L0_efad) then
         I_ito=I0_ato
      else
         I_ito=I0_eto
      endif
C TOLKATEL_OY_HANDLER.fmg( 202, 380):���� RE IN LO CH7
      L0_oxo = L0_it.OR.L0_efad
C TOLKATEL_OY_HANDLER.fmg( 176, 396):���
      L0_evu = L0_ukad.OR.L0_efad
C TOLKATEL_OY_HANDLER.fmg( 256, 377):���
      L0_ivu = L0_evu.AND.(.NOT.L_otu)
C TOLKATEL_OY_HANDLER.fmg( 272, 376):�
      if(L0_ivu) then
         R0_exu=R_uvu
      else
         R0_exu=R0_ovu
      endif
C TOLKATEL_OY_HANDLER.fmg( 274, 384):���� RE IN LO CH7
      if(L0_akad) then
         I_ero=I0_iro
      else
         I_ero=I0_oro
      endif
C TOLKATEL_OY_HANDLER.fmg( 148, 364):���� RE IN LO CH7
      if(L0_akad) then
         I_elu=I0_ilu
      else
         I_elu=I0_olu
      endif
C TOLKATEL_OY_HANDLER.fmg( 124, 376):���� RE IN LO CH7
      if(L0_ukad) then
         I_uso=I0_iso
      else
         I_uso=I0_oso
      endif
C TOLKATEL_OY_HANDLER.fmg( 194, 430):���� RE IN LO CH7
      L0_abu = L0_av.OR.L0_ukad
C TOLKATEL_OY_HANDLER.fmg( 172, 446):���
      if(L0_ulad) then
         I_ovi=I0_uvi
      else
         I_ovi=I0_axi
      endif
C TOLKATEL_OY_HANDLER.fmg( 131, 471):���� RE IN LO CH7
      if(L0_ulad) then
         I_uro=I0_aso
      else
         I_uro=I0_eso
      endif
C TOLKATEL_OY_HANDLER.fmg( 126, 453):���� RE IN LO CH7
      L_asu = L_obad.OR.L0_uru.OR.L0_oru.OR.L0_iru.OR.L0_eru
C TOLKATEL_OY_HANDLER.fmg(  74, 416):���
      L0_amo = L_obad.OR.L_asu
C TOLKATEL_OY_HANDLER.fmg( 224, 484):���
      if(L0_amo) then
         I_emo=I0_imo
      else
         I_emo=I0_omo
      endif
C TOLKATEL_OY_HANDLER.fmg( 229, 492):���� RE IN LO CH7
      if(L_asu) then
         I_umo=I0_olo
      else
         I_umo=I0_ulo
      endif
C TOLKATEL_OY_HANDLER.fmg( 229, 472):���� RE IN LO CH7
      if(L_obad) then
         I_aku=I0_eku
      else
         I_aku=I0_iku
      endif
C TOLKATEL_OY_HANDLER.fmg( 154, 388):���� RE IN LO CH7
      L_epi = L0_umi.OR.L_imi
C TOLKATEL_OY_HANDLER.fmg( 296, 284):���
      L0_edu = (.NOT.L0_av).AND.L_odad
C TOLKATEL_OY_HANDLER.fmg( 162, 404):�
      if(L0_edu) then
         I0_idu=I0_odu
      else
         I0_idu=I0_udu
      endif
C TOLKATEL_OY_HANDLER.fmg( 168, 410):���� RE IN LO CH7
      if(L0_oxo) then
         I0_emu=I0_uxo
      else
         I0_emu=I0_idu
      endif
C TOLKATEL_OY_HANDLER.fmg( 178, 410):���� RE IN LO CH7
      if(L_obad) then
         I_ulu=I0_amu
      else
         I_ulu=I0_emu
      endif
C TOLKATEL_OY_HANDLER.fmg( 198, 408):���� RE IN LO CH7
      if(L_odad) then
         C20_eli=C20_ili
      else
         C20_eli=C20_oli
      endif
C TOLKATEL_OY_HANDLER.fmg(  35, 308):���� RE IN LO CH20
      if(L_idad) then
         C20_uli=C20_ali
      else
         C20_uli=C20_eli
      endif
C TOLKATEL_OY_HANDLER.fmg(  51, 308):���� RE IN LO CH20
      L0_ibu = L_idad.AND.(.NOT.L0_it)
C TOLKATEL_OY_HANDLER.fmg( 154, 459):�
      if(L0_ibu) then
         I0_obu=I0_ubu
      else
         I0_obu=I0_adu
      endif
C TOLKATEL_OY_HANDLER.fmg( 161, 466):���� RE IN LO CH7
      if(L0_abu) then
         I0_edad=I0_ebu
      else
         I0_edad=I0_obu
      endif
C TOLKATEL_OY_HANDLER.fmg( 174, 464):���� RE IN LO CH7
      if(L_obad) then
         I_ubad=I0_adad
      else
         I_ubad=I0_edad
      endif
C TOLKATEL_OY_HANDLER.fmg( 192, 464):���� RE IN LO CH7
      R_e=R0_os
C TOLKATEL_OY_HANDLER.fmg( 347, 458):����������� ��
      R0_axu = R8_ixu
C TOLKATEL_OY_HANDLER.fmg( 270, 393):��������
C label 628  try628=try628-1
      R8_ixu = R0_exu + R0_axu
C TOLKATEL_OY_HANDLER.fmg( 280, 392):��������
C sav1=R0_axu
      R0_axu = R8_ixu
C TOLKATEL_OY_HANDLER.fmg( 270, 393):recalc:��������
C if(sav1.ne.R0_axu .and. try628.gt.0) goto 628
      End
