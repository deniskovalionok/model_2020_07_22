      Subroutine K2_ZU11(ext_deltat,I_i,I_u,R_ud,R_ef,R_if
     &,R_of,R_ak,R_ul)
C |I_i           |2 4 S|_sdel1*         |���������� ��������� �������� ������ |0|
C |I_u           |2 4 K|_ndel1          |�������� �������� ������ � �����|1|
C |R_ud          |4 4 S|_sdly1*         |���������� ��������� �������� ||
C |R_ef          |4 4 I|keycom          |������� �� �������������� ������||
C |R_if          |4 4 I|smax_c          |������� ������� �������|100.0|
C |R_of          |4 4 I|smin_c          |������ ������� �������|0.0|
C |R_ak          |4 4 O|setpoint        |�������� �������|0.5|
C |R_ul          |4 4 I|tmove           |������ ����� ���� ��11|30.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I_i
      LOGICAL*1 L0_o
      INTEGER*4 I_u
      LOGICAL*1 L0_ad
      REAL*4 R0_ed
      LOGICAL*1 L0_id,L0_od
      REAL*4 R_ud,R0_af,R_ef,R_if,R_of,R0_uf,R_ak,R0_ek,R0_ik
     &,R0_ok,R0_uk,R0_al,R0_el,R0_il,R0_ol,R_ul
      LOGICAL*1 L0_am,L0_em

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_af=R_ud
C K2_ZU11.fmg( 135, 189):pre: �������� �� ���,1
      I0_e=I_i
C K2_ZU11.fmg( 165, 193):pre: �������� ������ �� N �����,1
      R0_ed = R_if + (-R_of)
C K2_ZU11.fmg( 145, 223):��������
      L0_id=R_ef.gt.R0_af
C K2_ZU11.fmg( 147, 200):���������� >
      L0_od=R_ef.lt.R0_af
C K2_ZU11.fmg( 147, 186):���������� <
      L0_ad = L0_id.OR.L0_od
C K2_ZU11.fmg( 157, 193):���
      L0_o=.false.
      if(L0_ad) then
         if(I_i.ge.I_u)  L0_o=.true.
         I_i=I0_e+1
      else
         I_i=0
      endif
C K2_ZU11.fmg( 165, 193):�������� ������ �� N �����,1
      L0_em = L0_id.AND.L0_o
C K2_ZU11.fmg( 177, 199):�
      L0_am = L0_o.AND.L0_od
C K2_ZU11.fmg( 177, 187):�
      R_ud=R_ef
C K2_ZU11.fmg( 135, 189):�������� �� ���,1
      R0_ol = 0.0
C K2_ZU11.fmg( 184, 201):��������� (RE4) (�������)
      R0_ek = DeltaT
C K2_ZU11.fmg( 130, 209):��������� (RE4) (�������)
      if(R_ul.ge.0.0) then
         R0_ik=R0_ek/max(R_ul,1.0e-10)
      else
         R0_ik=R0_ek/min(R_ul,-1.0e-10)
      endif
C K2_ZU11.fmg( 136, 208):�������� ����������
      R0_uk = R0_ed * R0_ik
C K2_ZU11.fmg( 153, 209):����������
      if(L0_em) then
         R0_el=R0_uk
      else
         R0_el=R0_ol
      endif
C K2_ZU11.fmg( 188, 209):���� RE IN LO CH7
      if(L0_am) then
         R0_al=R0_uk
      else
         R0_al=R0_ol
      endif
C K2_ZU11.fmg( 188, 194):���� RE IN LO CH7
      R0_il = R0_el + (-R0_al)
C K2_ZU11.fmg( 194, 205):��������
      R0_ok = R_ak + R0_il
C K2_ZU11.fmg( 201, 206):��������
C label 34  try34=try34-1
      R0_uf=MAX(R_of,R0_ok)
C K2_ZU11.fmg( 210, 207):��������
      R_ak=MIN(R_if,R0_uf)
C K2_ZU11.fmg( 220, 208):�������
C sav1=R0_ok
      R0_ok = R_ak + R0_il
C K2_ZU11.fmg( 201, 206):recalc:��������
C if(sav1.ne.R0_ok .and. try34.gt.0) goto 34
      End
