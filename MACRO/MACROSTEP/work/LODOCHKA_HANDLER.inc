      Interface
      Subroutine LODOCHKA_HANDLER(ext_deltat,L_ad,L_af,L_ak
     &,L_al,L_ol,L_ap,L_ar,L_as,I_iv,I_ex,I_abe,I_ube,I_ude
     &,I_afe,L_ufe,L_ake,R_uke,L_ule,L_ope,L_upe,L_ire,L_ase
     &,L_ite,R_eve,R_ive,L_ove,L_ibi,L_ubi,L_adi,R_odi,R_udi
     &,L_afi,L_ifi,R_ali,R_eli,L_ili,I_imi,I_epi,I_ari,I_uri
     &,I_osi,I_usi,I_oti,L_uti,I_avi,R_axi,R_abo,R_ebo,R_edo
     &,R_udo,R_ofo,R_alo,R_ulo,R_emo,R_imo,R_epo,R_opo,R_upo
     &,L_aro,L_ato,L_eto,R_oto,R_ivo,R_axo,R_uxo,R_obu,R_idu
     &,R_odu,R_eku,R_iku,R_ilu,R_emu,R_omu,R_asu,R_esu,L_isu
     &,R_etu,R_evu,R_exu,L_obad,R_ubad,R_odad,R_ifad,R_ofad
     &,R_elad,R_ilad,L_olad,R_imad,R_ipad,R_irad,R_osad,R_itad
     &,R_evad,R_ivad,L_uxad,L_ebed,L_obed,R_ubed,L_aded,R_eded
     &,R_ufed,R_oked,R_iled,R_oled,L_aped,L_oped,L_ared,R_ered
     &,L_ired,R_ored,R_ised,R_eved,R_exed,R_ixed,R_oxed,L_adid
     &,L_odid,L_udid,L_efid,L_akid,L_ikid,R_olid,R_ulid,R_umid
     &,R_apid,L_ipid,R_irid,I_ofod,R_ikod,R_okod,R_ukod,R_elod
     &,R_ulod,R_imod,R_umod,C20_apod)
C |L_ad          |1 1 I|UNLOAD_TO_FDA90 |�������� ������� � ������������ �������||
C |L_af          |1 1 I|FDA60AE201_CATCH2|������ ������� ������||
C |L_ak          |1 1 I|FDA60AE402_CATCH2|������ ������� �������. ��������||
C |L_al          |1 1 I|FDA60AE500_CATCH2|������ ������� 3-��� ���������� (����)||
C |L_ol          |1 1 I|FDA60AE500_CATCH1|������ ������� 3-���. ���������� (� �����)||
C |L_ap          |1 1 I|FDA60AE403_CATCH2|������ ������� ������� �������� �������� 20FDA60AE403||
C |L_ar          |1 1 I|FDA60AE403_CATCH1|������� � ����� �� ������� �������||
C |L_as          |1 1 I|FDA60AE515_CATCH|������ ������� ���������� 20FDA60AE515||
C |I_iv          |2 4 K|_lcmpJ10773     |�������� ������ �����������|5|
C |I_ex          |2 4 K|_lcmpJ10761     |�������� ������ �����������|4|
C |I_abe         |2 4 K|_lcmpJ10749     |�������� ������ �����������|3|
C |I_ube         |2 4 K|_lcmpJ10737     |�������� ������ �����������|2|
C |I_ude         |2 4 K|_lcmpJ10725     |�������� ������ �����������|1|
C |I_afe         |2 4 I|FDA60AE413_NUM_BOAT|����� ��������� ������� �� �������||
C |L_ufe         |1 1 I|FDA60AE514_CATCH|������ ������� ���������� 20FDA60AE514||
C |L_ake         |1 1 I|LODOCHKA_FREE_STATE|��������� ������� � ��������� "������ �������"||
C |R_uke         |4 4 I|FDA40_BOAT_MASS |����� ����� � �����������||
C |L_ule         |1 1 I|FDA60_LOD_INPUT_GATEWAY_FROM|������� �� �������� �����||
C |L_ope         |1 1 I|FDA60_LOD_TRANSFER_TABLE|������� �� ������������ �������||
C |L_upe         |1 1 I|FDA60_LOD_MAIN_TROLLEY|������� �� �������||
C |L_ire         |1 1 I|FDA60_LOD_INPUT_GATEWAY|������� �� �������� �����||
C |L_ase         |1 1 I|FDA60_LOD_VZV   |������� �� ������������||
C |L_ite         |1 1 I|FDA60_LOD_LIFT  |������� ����������� �� ����� ||
C |R_eve         |4 4 S|_simpJ10082*    |[���]���������� ��������� ������������� |0.0|
C |R_ive         |4 4 K|_timpJ10082     |[���]������������ �������� �������������|1.0|
C |L_ove         |1 1 S|_limpJ10082*    |[TF]���������� ��������� ������������� |F|
C |L_ibi         |1 1 I|FDA40_START     |������ ������� �����������||
C |L_ubi         |1 1 I|FDA91EC001_INIT_COORD|������������� ���������� ������� ��� ��. �����������||
C |L_adi         |1 1 I|FDA91EC003_INIT_COORD|������������� ���������� ������� ��� ��. ���||
C |R_odi         |4 4 S|_simpJ10030*    |[���]���������� ��������� ������������� |0.0|
C |R_udi         |4 4 K|_timpJ10030     |[���]������������ �������� �������������|1.0|
C |L_afi         |1 1 S|_limpJ10030*    |[TF]���������� ��������� ������������� |F|
C |L_ifi         |1 1 I|FDA91EC005_INIT_COORD|������������� ���������� ������� ��� ��. ���������||
C |R_ali         |4 4 S|_simpJ10001*    |[���]���������� ��������� ������������� |0.0|
C |R_eli         |4 4 K|_timpJ10001     |[���]������������ �������� �������������|1.0|
C |L_ili         |1 1 S|_limpJ10001*    |[TF]���������� ��������� ������������� |F|
C |I_imi         |2 4 K|_lcmpJ9990      |�������� ������ �����������|5|
C |I_epi         |2 4 K|_lcmpJ9978      |�������� ������ �����������|4|
C |I_ari         |2 4 K|_lcmpJ9966      |�������� ������ �����������|3|
C |I_uri         |2 4 K|_lcmpJ9954      |�������� ������ �����������|2|
C |I_osi         |2 4 K|_lcmpJ9942      |�������� ������ �����������|1|
C |I_usi         |2 4 I|FDA60AE408_NUM_BOAT|����� ��������� ������� �� �������||
C |I_oti         |2 4 O|CR              |�������������� � �������������||
C |L_uti         |1 1 I|FDA60AE408_CATCH|������ ������� 5��� �������� 20FDA60AE408||
C |I_avi         |2 4 O|LP              |����� �� ������� ����������� �������||
C |R_axi         |4 4 K|_lcmpJ9186      |[]�������� ������ �����������|20|
C |R_abo         |4 4 I|FDA60AE500AVY01 |�������� ��������� 20FDA60AE500A||
C |R_ebo         |4 4 K|_lcmpJ9173      |[]�������� ������ �����������|20|
C |R_edo         |4 4 I|FDA60AE500BVY01 |�������� ��������� 20FDA60AE500B||
C |R_udo         |4 4 I|FDA60AE500�VY01 |�������� ��������� 20FDA60AE500�||
C |R_ofo         |4 4 K|_lcmpJ9156      |[]�������� ������ �����������|20|
C |R_alo         |4 4 K|_lcmpJ9127      |[]�������� ������ �����������|20|
C |R_ulo         |4 4 K|_lcmpJ9057      |[]�������� ������ �����������|2|
C |R_emo         |4 4 I|FDA60AE403_VZ01 |�������� �������������� �������||
C |R_imo         |4 4 K|_lcmpJ9051      |[]�������� ������ �����������|20|
C |R_epo         |4 4 I|FDA60AE403_POS  |��������� ������� �� ��� X||
C |R_opo         |4 4 S|_simpJ9036*     |[���]���������� ��������� ������������� |0.0|
C |R_upo         |4 4 K|_timpJ9036      |[���]������������ �������� �������������|1.0|
C |L_aro         |1 1 S|_limpJ9036*     |[TF]���������� ��������� ������������� |F|
C |L_ato         |1 1 S|_splsJ8856*     |[TF]���������� ��������� ������������� |F|
C |L_eto         |1 1 I|FDA40_HAVE_DELTA_MASS|���� ��������� ����� �������||
C |R_oto         |4 4 I|FDA40_DELTA_MASS|��������� ����� �������||
C |R_ivo         |4 4 O|MASS            |����� ����� ����������� � �������||
C |R_axo         |4 4 K|_lcmpJ8516      |[]�������� ������ �����������|20|
C |R_uxo         |4 4 K|_lcmpJ8503      |[]�������� ������ �����������|20|
C |R_obu         |4 4 K|_lcmpJ8495      |[]�������� ������ �����������|20|
C |R_idu         |4 4 I|FDA91AE001KE01_POS|��������� �1||
C |R_odu         |4 4 K|_lcmpJ8485      |[]�������� ������ �����������|20|
C |R_eku         |4 4 I|FDA91AE001KE01_V01|������� �1 �� ��� X||
C |R_iku         |4 4 I|FDA91AE003KE01_V01|�������� 20FDA91AE003KE01||
C |R_ilu         |4 4 K|_lcmpJ8465      |[]�������� ������ �����������|20|
C |R_emu         |4 4 I|FDA91AE003KE01_POS|��������� �� 20FDA91AE003KE01||
C |R_omu         |4 4 K|_lcmpJ8453      |[]�������� ������ �����������|20|
C |R_asu         |4 4 S|_simpJ8368*     |[���]���������� ��������� ������������� |0.0|
C |R_esu         |4 4 K|_timpJ8368      |[���]������������ �������� �������������|1.0|
C |L_isu         |1 1 S|_limpJ8368*     |[TF]���������� ��������� ������������� |F|
C |R_etu         |4 4 K|_lcmpJ8358      |[]�������� ������ �����������|20|
C |R_evu         |4 4 K|_lcmpJ8350      |[]�������� ������ �����������|20|
C |R_exu         |4 4 K|_lcmpJ8342      |[]�������� ������ �����������|20|
C |L_obad        |1 1 O|FDA91AE012_CATCHED|������� ��������� �� 20FDA91AE012||
C |R_ubad        |4 4 K|_lcmpJ8317      |[]�������� ������ �����������|20|
C |R_odad        |4 4 K|_lcmpJ8308      |[]�������� ������ �����������|20|
C |R_ifad        |4 4 I|FDA91AE012_POS  |��������� �4||
C |R_ofad        |4 4 K|_lcmpJ8297      |[]�������� ������ �����������|20|
C |R_elad        |4 4 S|_simpJ7966*     |[���]���������� ��������� ������������� |0.0|
C |R_ilad        |4 4 K|_timpJ7966      |[���]������������ �������� �������������|1.0|
C |L_olad        |1 1 S|_limpJ7966*     |[TF]���������� ��������� ������������� |F|
C |R_imad        |4 4 K|_lcmpJ7929      |[]�������� ������ �����������|20|
C |R_ipad        |4 4 K|_lcmpJ7921      |[]�������� ������ �����������|20|
C |R_irad        |4 4 K|_lcmpJ7913      |[]�������� ������ �����������|20|
C |R_osad        |4 4 K|_lcmpJ7509      |[]�������� ������ �����������|20|
C |R_itad        |4 4 K|_lcmpJ7500      |[]�������� ������ �����������|20|
C |R_evad        |4 4 I|FDA91AE007_POS  |��������� �4||
C |R_ivad        |4 4 K|_lcmpJ7487      |[]�������� ������ �����������|20|
C |L_uxad        |1 1 O|FDA91AE007_CATCHED|������� ��������� �4||
C |L_ebed        |1 1 S|_qffJ7476*      |�������� ������ Q RS-��������  |F|
C |L_obed        |1 1 I|FDA91AE007_UNCATCH|��������� ������� �4||
C |R_ubed        |4 4 I|FDA91AE007_VZ01 |������� �4 �� ��� Z||
C |L_aded        |1 1 I|FDA91AE007_CATCH|������ ������� �4||
C |R_eded        |4 4 K|_lcmpJ7170      |[]�������� ������ �����������|20|
C |R_ufed        |4 4 K|_lcmpJ6848      |[]�������� ������ �����������|20|
C |R_oked        |4 4 K|_lcmpJ6839      |[]�������� ������ �����������|20|
C |R_iled        |4 4 I|FDA91AE006_POS  |��������� �� 20FDA91AE006||
C |R_oled        |4 4 K|_lcmpJ6826      |[]�������� ������ �����������|20|
C |L_aped        |1 1 O|FDA91AE006_CATCHED|������� ��������� �� 20FDA91AE006||
C |L_oped        |1 1 S|_qffJ6815*      |�������� ������ Q RS-��������  |F|
C |L_ared        |1 1 I|FDA91AE006_UNCATCH|��������� ������� �� 20FDA91AE006||
C |R_ered        |4 4 I|FDA91AE006_VZ01 |������� �� �� ��� Z||
C |L_ired        |1 1 I|FDA91AE006_CATCH|������ ������� �� 20FDA91AE006||
C |R_ored        |4 4 K|_lcmpJ6805      |[]�������� ������ �����������|20|
C |R_ised        |4 4 K|_lcmpJ6796      |[]�������� ������ �����������|20|
C |R_eved        |4 4 K|_lcmpJ6476      |[]�������� ������ �����������|20|
C |R_exed        |4 4 K|_lcmpJ6463      |[]�������� ������ �����������|1000|
C |R_ixed        |4 4 I|FDA91AE014_POS  |��������� �� 20FDA91AE014||
C |R_oxed        |4 4 K|_lcmpJ6181      |[]�������� ������ �����������|20|
C |L_adid        |1 1 O|FDA91AE014_CATCHED|������� ��������� �� 20FDA91AE014||
C |L_odid        |1 1 I|FDA91AE012_UNCATCH|��������� ������� �� 20FDA91AE012||
C |L_udid        |1 1 I|FDA91AE012_CATCH|������ ������� �� 20FDA91AE012||
C |L_efid        |1 1 S|_qffJ6163*      |�������� ������ Q RS-��������  |F|
C |L_akid        |1 1 S|_qffJ6151*      |�������� ������ Q RS-��������  |F|
C |L_ikid        |1 1 I|FDA91AE014_UNCATCH|��������� ������� �� 20FDA91AE014||
C |R_olid        |4 4 O|VZ01            |��������� ������� �� ��� Z||
C |R_ulid        |4 4 S|_ointJ6126*     |����� ����������� |0.0|
C |R_umid        |4 4 I|FDA91AE012_VZ01 |������� �� �� ��� Z||
C |R_apid        |4 4 I|FDA91AE014_VZ01 |������� �� �� ��� Z||
C |L_ipid        |1 1 I|FDA91AE014_CATCH|������ ������� �� 20FDA91AE014||
C |R_irid        |4 4 S|_ointJ5958*     |����� ����������� |`p_LOAD_Y`|
C |I_ofod        |2 4 O|LINE_POS        |����� �� ������� ����������� �������||
C |R_ikod        |4 4 O|VX01            |��������� ������� �� ��� X||
C |R_okod        |4 4 S|_ointJ5909*     |����� ����������� |`p_LOAD_X`|
C |R_ukod        |4 4 K|_lcmpJ2655      |[]�������� ������ �����������|4456|
C |R_elod        |4 4 I|FDA60AE413VX02  |�������� �������� �������||
C |R_ulod        |4 4 I|FDA60AE408VX02  |�������� �������� �������||
C |R_imod        |4 4 K|_lcmpJ871       |[]�������� ������ �����������|1.0|
C |R_umod        |4 4 O|VY01            |��������� ������� �� ��� Y||
C |C20_apod      |3 20 O|SECTION         |������� ������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_ad,L_af,L_ak,L_al,L_ol,L_ap,L_ar,L_as
      INTEGER*4 I_iv,I_ex,I_abe,I_ube,I_ude,I_afe
      LOGICAL*1 L_ufe,L_ake
      REAL*4 R_uke
      LOGICAL*1 L_ule,L_ope,L_upe,L_ire,L_ase,L_ite
      REAL*4 R_eve,R_ive
      LOGICAL*1 L_ove,L_ibi,L_ubi,L_adi
      REAL*4 R_odi,R_udi
      LOGICAL*1 L_afi,L_ifi
      REAL*4 R_ali,R_eli
      LOGICAL*1 L_ili
      INTEGER*4 I_imi,I_epi,I_ari,I_uri,I_osi,I_usi,I_oti
      LOGICAL*1 L_uti
      INTEGER*4 I_avi
      REAL*4 R_axi,R_abo,R_ebo,R_edo,R_udo,R_ofo,R_alo,R_ulo
     &,R_emo,R_imo,R_epo,R_opo,R_upo
      LOGICAL*1 L_aro,L_ato,L_eto
      REAL*4 R_oto,R_ivo,R_axo,R_uxo,R_obu,R_idu,R_odu,R_eku
     &,R_iku,R_ilu,R_emu,R_omu,R_asu,R_esu
      LOGICAL*1 L_isu
      REAL*4 R_etu,R_evu,R_exu
      LOGICAL*1 L_obad
      REAL*4 R_ubad,R_odad,R_ifad,R_ofad,R_elad,R_ilad
      LOGICAL*1 L_olad
      REAL*4 R_imad,R_ipad,R_irad,R_osad,R_itad,R_evad,R_ivad
      LOGICAL*1 L_uxad,L_ebed,L_obed
      REAL*4 R_ubed
      LOGICAL*1 L_aded
      REAL*4 R_eded,R_ufed,R_oked,R_iled,R_oled
      LOGICAL*1 L_aped,L_oped,L_ared
      REAL*4 R_ered
      LOGICAL*1 L_ired
      REAL*4 R_ored,R_ised,R_eved,R_exed,R_ixed,R_oxed
      LOGICAL*1 L_adid,L_odid,L_udid,L_efid,L_akid,L_ikid
      REAL*4 R_olid,R_ulid,R_umid,R_apid
      LOGICAL*1 L_ipid
      REAL*4 R_irid
      INTEGER*4 I_ofod
      REAL*4 R_ikod,R_okod,R_ukod,R_elod,R_ulod,R_imod,R_umod
      CHARACTER*20 C20_apod
      End subroutine LODOCHKA_HANDLER
      End interface
