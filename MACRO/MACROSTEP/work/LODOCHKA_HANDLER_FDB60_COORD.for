      Subroutine LODOCHKA_HANDLER_FDB60_COORD(ext_deltat,I_i
     &,R_ad,L_od,L_of,L_ok,L_ol,L_om,L_op,L_or,L_os,L_ot,L_ov
     &,L_ox,L_obe,L_ode,L_ofe,I_eke,R_ike,R_oke,I_ale,I_ele
     &,L_ule,I_eme)
C |I_i           |2 4 K|_lcmpJ10599     |�������� ������ �����������|5008|
C |R_ad          |4 4 O|CIL_IT_PKS_MASS_AFTER|||
C |L_od          |1 1 I|FDB50AE403_UNCATCH_BOAT|������� �� ����������� �� ����� ��||
C |L_of          |1 1 I|FDB50AE403_WEIGHT_1|������� �� ������� �����������||
C |L_ok          |1 1 I|FDB50AE403_CATCH_BOAT_1|������� ������������ �� ������� �����������||
C |L_ol          |1 1 I|FDB50AE403_UNMOUNT|������� ��������� �� ��������.||
C |L_om          |1 1 I|FDB50AE403_CATCH_1|������� � �� �� ����� ��������||
C |L_op          |1 1 I|FDB50AE506_PUSH |������� � ��������� �� ���������� ������������.||
C |L_or          |1 1 I|FDB50AE505_PUSH |������� �� ������ ����� ����.||
C |L_os          |1 1 I|FDB50AE504_PUSH |������� �� �����. ��������� � ������ ����� ����.||
C |L_ot          |1 1 I|FDB50AE503_PUSH |������� � ����. ������� ���������||
C |L_ov          |1 1 I|FDB50AE502_PUSH |������� ���� ��� ������ ����� ����||
C |L_ox          |1 1 I|FDB50AE501_PUSH |�� ������� ����� ���||
C |L_obe         |1 1 I|FDB50AE500_PUSH |������� �� ����� ��������� � ������������� �����||
C |L_ode         |1 1 I|FDB50AE403_MOUNT_ADD|������� �� ������ ����� ���������||
C |L_ofe         |1 1 I|FDB50AE403_WEIGHT|������� �� ������� �����������||
C |I_eke         |2 4 K|_lcmpJ10325     |�������� ������ �����������|5003|
C |R_ike         |4 4 I|MASS            |����� �������||
C |R_oke         |4 4 O|CIL_IT_PKS_MASS_BEFORE|||
C |I_ale         |2 4 P|num_boat        |����� �������||
C |I_ele         |2 4 O|CIL_IT_PKS_N    |||
C |L_ule         |1 1 I|FDB50AE403_CATCH_BOAT|������� ����� ������������� ������ (��)||
C |I_eme         |2 4 O|CR              |�������������� � �������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e
      INTEGER*4 I_i
      LOGICAL*1 L0_o
      REAL*4 R0_u,R_ad
      INTEGER*4 I0_ed
      LOGICAL*1 L0_id,L_od,L0_ud
      INTEGER*4 I0_af,I0_ef
      LOGICAL*1 L0_if,L_of,L0_uf
      INTEGER*4 I0_ak,I0_ek
      LOGICAL*1 L0_ik,L_ok,L0_uk
      INTEGER*4 I0_al,I0_el
      LOGICAL*1 L0_il,L_ol,L0_ul
      INTEGER*4 I0_am,I0_em
      LOGICAL*1 L0_im,L_om,L0_um
      INTEGER*4 I0_ap,I0_ep
      LOGICAL*1 L0_ip,L_op,L0_up
      INTEGER*4 I0_ar,I0_er
      LOGICAL*1 L0_ir,L_or,L0_ur
      INTEGER*4 I0_as,I0_es
      LOGICAL*1 L0_is,L_os,L0_us
      INTEGER*4 I0_at,I0_et
      LOGICAL*1 L0_it,L_ot,L0_ut
      INTEGER*4 I0_av,I0_ev
      LOGICAL*1 L0_iv,L_ov,L0_uv
      INTEGER*4 I0_ax,I0_ex
      LOGICAL*1 L0_ix,L_ox,L0_ux
      INTEGER*4 I0_abe,I0_ebe
      LOGICAL*1 L0_ibe,L_obe,L0_ube
      INTEGER*4 I0_ade,I0_ede
      LOGICAL*1 L0_ide,L_ode,L0_ude
      INTEGER*4 I0_afe,I0_efe
      LOGICAL*1 L0_ife,L_ofe,L0_ufe
      INTEGER*4 I0_ake,I_eke
      REAL*4 R_ike,R_oke
      LOGICAL*1 L0_uke
      INTEGER*4 I_ale,I_ele,I0_ile
      LOGICAL*1 L0_ole,L_ule,L0_ame
      INTEGER*4 I_eme,I0_ime

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e = -1.0
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 564,1044):��������� (RE4) (�������)
      R0_u = R_ike + R0_e
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 570,1048):��������
      I0_ed = 5001
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 854):��������� ������������� IN (�������)
      I0_af = 5002
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 375, 848):��������� ������������� IN (�������)
      I0_ef = 5002
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 872):��������� ������������� IN (�������)
      I0_ak = 5003
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 866):��������� ������������� IN (�������)
      I0_ek = 5003
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 886):��������� ������������� IN (�������)
      I0_al = 5004
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 881):��������� ������������� IN (�������)
      I0_el = 5004
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 904):��������� ������������� IN (�������)
      I0_am = 5012
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 899):��������� ������������� IN (�������)
      I0_em = 5012
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 920):��������� ������������� IN (�������)
      I0_ap = 5011
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 915):��������� ������������� IN (�������)
      I0_ep = 5011
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 940):��������� ������������� IN (�������)
      I0_ar = 5010
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 935):��������� ������������� IN (�������)
      I0_er = 5010
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 960):��������� ������������� IN (�������)
      I0_as = 5009
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 954):��������� ������������� IN (�������)
      I0_es = 5009
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 981):��������� ������������� IN (�������)
      I0_at = 5008
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 976):��������� ������������� IN (�������)
      I0_et = 5008
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418, 998):��������� ������������� IN (�������)
      I0_av = 5007
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376, 993):��������� ������������� IN (�������)
      I0_ev = 5007
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418,1020):��������� ������������� IN (�������)
      I0_ax = 5006
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376,1014):��������� ������������� IN (�������)
      I0_ex = 5006
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418,1036):��������� ������������� IN (�������)
      I0_abe = 5005
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376,1031):��������� ������������� IN (�������)
      I0_ebe = 5005
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418,1050):��������� ������������� IN (�������)
      I0_ade = 5004
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 375,1044):��������� ������������� IN (�������)
      I0_ede = 5004
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418,1064):��������� ������������� IN (�������)
      I0_afe = 5003
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376,1059):��������� ������������� IN (�������)
      I0_efe = 5003
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418,1078):��������� ������������� IN (�������)
      I0_ake = 5002
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376,1073):��������� ������������� IN (�������)
      I0_ile = 5002
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 418,1092):��������� ������������� IN (�������)
      I0_ime = 5001
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 376,1086):��������� ������������� IN (�������)
      L0_ube=I_eme.eq.I0_ade
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 381,1045):���������� �������������
C label 65  try65=try65-1
      L0_ibe = L0_ube.AND.L_obe
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 394,1044):�
      if(L0_ibe) then
         I_eme=I0_ebe
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 422,1049):���� � ������������� �������
      L0_ame=I_eme.eq.I0_ime
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382,1087):���������� �������������
      L0_ole = L0_ame.AND.L_ule
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395,1086):�
      if(L0_ole) then
         I_eme=I0_ile
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423,1091):���� � ������������� �������
      L0_up=I_eme.eq.I0_ar
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 936):���������� �������������
      L0_ip = L0_up.AND.L_op
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 935):�
      if(L0_ip) then
         I_eme=I0_ep
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 940):���� � ������������� �������
      L0_um=I_eme.eq.I0_ap
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 916):���������� �������������
      L0_im = L0_um.AND.L_om
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 915):�
      if(L0_im) then
         I_eme=I0_em
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 920):���� � ������������� �������
      L0_ur=I_eme.eq.I0_as
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 956):���������� �������������
      L0_ir = L0_ur.AND.L_or
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 954):�
      if(L0_ir) then
         I_eme=I0_er
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 960):���� � ������������� �������
      L0_us=I_eme.eq.I0_at
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 976):���������� �������������
      L0_is = L0_us.AND.L_os
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 396, 976):�
      if(L0_is) then
         I_eme=I0_es
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 424, 980):���� � ������������� �������
      L0_ut=I_eme.eq.I0_av
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 994):���������� �������������
      L0_it = L0_ut.AND.L_ot
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 993):�
      if(L0_it) then
         I_eme=I0_et
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 998):���� � ������������� �������
      L0_ul=I_eme.eq.I0_am
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 900):���������� �������������
      L0_il = L0_ul.AND.L_ol
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 899):�
      if(L0_il) then
         I_eme=I0_el
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 904):���� � ������������� �������
      L0_uv=I_eme.eq.I0_ax
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382,1015):���������� �������������
      L0_iv = L0_uv.AND.L_ov
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395,1014):�
      if(L0_iv) then
         I_eme=I0_ev
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423,1019):���� � ������������� �������
      L0_ux=I_eme.eq.I0_abe
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382,1032):���������� �������������
      L0_ix = L0_ux.AND.L_ox
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395,1031):�
      if(L0_ix) then
         I_eme=I0_ex
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423,1036):���� � ������������� �������
      L0_uk=I_eme.eq.I0_al
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 882):���������� �������������
      L0_ik = L0_uk.AND.L_ok
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 881):�
      if(L0_ik) then
         I_eme=I0_ek
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 886):���� � ������������� �������
      L0_ude=I_eme.eq.I0_afe
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382,1060):���������� �������������
      L0_ide = L0_ude.AND.L_ode
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395,1059):�
      if(L0_ide) then
         I_eme=I0_ede
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423,1064):���� � ������������� �������
      L0_uf=I_eme.eq.I0_ak
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382, 868):���������� �������������
      L0_if = L0_uf.AND.L_of
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395, 866):�
      if(L0_if) then
         I_eme=I0_ef
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423, 872):���� � ������������� �������
      L0_ufe=I_eme.eq.I0_ake
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 382,1074):���������� �������������
      L0_ife = L0_ufe.AND.L_ofe
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 395,1073):�
      if(L0_ife) then
         I_eme=I0_efe
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 423,1078):���� � ������������� �������
      L0_ud=I_eme.eq.I0_af
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 381, 850):���������� �������������
      L0_id = L0_ud.AND.L_od
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 394, 848):�
      if(L0_id) then
         I_eme=I0_ed
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 422, 854):���� � ������������� �������
      L0_o=I_eme.eq.I_i
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 524,1030):���������� �������������
      if(L0_o) then
         R_ad=R0_u
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 578,1048):���� � ������������� �������
      L0_uke=I_eme.eq.I_eke
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 524,1083):���������� �������������
      if(L0_uke) then
         I_ele=I_ale
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 577,1086):���� � ������������� �������
      if(L0_uke) then
         R_oke=R_ike
      endif
C LODOCHKA_HANDLER_FDB60_COORD.fmg( 577,1092):���� � ������������� �������
      End
