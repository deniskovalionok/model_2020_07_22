      Interface
      Subroutine TOLKATEL_OZ_HANDLER_dpro(ext_deltat,R_e,R_i
     &,R_o,R_u,R_ad,I_ed,R_ud,R_af,R_uf,C20_em,C20_ep,I_ar
     &,I_or,R_ur,R_as,R_es,R_is,I_os,I_et,I_ev,I_uv,C20_ux
     &,C20_ube,C8_ude,R_oke,R_uke,L_ale,R_ele,R_ile,I_ole
     &,L_eme,L_ime,I_ume,I_ipe,L_are,L_ire,L_ore,L_ase,L_ise
     &,L_ose,L_use,L_ute,L_uve,L_exe,L_oxe,L_abi,R8_ibi,R_edi
     &,R8_udi,L_afi,L_ifi,L_ufi,I_aki,L_ili,L_uli,L_api,L_opi
     &,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi)
C |R_e           |4 4 K|_uintT_INT      |����������� ������ ����������� ������|10000|
C |R_i           |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_o           |4 4 K|_lintT_INT      |����������� ������ ����������� �����|0.0|
C |R_u           |4 4 K|_lcmpJ2903      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_ad          |4 4 K|_lcmpJ2873      |[]�������� ������ �����������|5.0|
C |I_ed          |2 4 O|LWORK           |����� �������||
C |R_ud          |4 4 O|VZ01            |��������� ��������� �� ��� Z||
C |R_af          |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_TOLKATEL_PKS_X`|
C |R_uf          |4 4 O|VZ02            |�������� ����������� ���������||
C |C20_em        |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_ep        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ar          |2 4 O|LREADY          |����� ����������||
C |I_or          |2 4 O|LBUSY           |����� �����||
C |R_ur          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_as          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_es          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_is          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |I_os          |2 4 O|state1          |��������� 1||
C |I_et          |2 4 O|state2          |��������� 2||
C |I_ev          |2 4 O|LWORKO          |����� � �������||
C |I_uv          |2 4 O|LINITC          |����� � ��������||
C |C20_ux        |3 20 O|task_state      |���������||
C |C20_ube       |3 20 O|task_name       |������� ���������||
C |C8_ude        |3 8 I|task            |������� ���������||
C |R_oke         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_uke         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ale         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ele         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ile         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ole         |2 4 O|LERROR          |����� �������������||
C |L_eme         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_ime         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_ume         |2 4 O|LINIT           |����� ��������||
C |I_ipe         |2 4 O|LZM             |������ "�������"||
C |L_are         |1 1 I|vlv_kvit        |||
C |L_ire         |1 1 I|instr_fault     |||
C |L_ore         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ase         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ise         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ose         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_use         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ute         |1 1 O|block           |||
C |L_uve         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_exe         |1 1 O|STOP            |�������||
C |L_oxe         |1 1 O|norm            |�����||
C |L_abi         |1 1 O|nopower         |��� ����������||
C |R8_ibi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_edi         |4 4 I|power           |�������� ��������||
C |R8_udi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_afi         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ifi         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ufi         |1 1 O|fault           |�������������||
C |I_aki         |2 4 O|LAM             |������ "�������"||
C |L_ili         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_uli         |1 1 O|XH53            |�� ������� (���)|F|
C |L_api         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_opi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_ari         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_eri         |1 1 I|mlf23           |������� ������� �����||
C |L_iri         |1 1 I|mlf22           |����� ����� ��������||
C |L_ori         |1 1 I|mlf04           |�������� �� ��������||
C |L_uri         |1 1 I|mlf03           |�������� �� ��������||
C |L_asi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i,R_o,R_u,R_ad
      INTEGER*4 I_ed
      REAL*4 R_ud,R_af,R_uf
      CHARACTER*20 C20_em,C20_ep
      INTEGER*4 I_ar,I_or
      REAL*4 R_ur,R_as,R_es,R_is
      INTEGER*4 I_os,I_et,I_ev,I_uv
      CHARACTER*20 C20_ux,C20_ube
      CHARACTER*8 C8_ude
      REAL*4 R_oke,R_uke
      LOGICAL*1 L_ale
      REAL*4 R_ele,R_ile
      INTEGER*4 I_ole
      LOGICAL*1 L_eme,L_ime
      INTEGER*4 I_ume,I_ipe
      LOGICAL*1 L_are,L_ire,L_ore,L_ase,L_ise,L_ose,L_use
     &,L_ute,L_uve,L_exe,L_oxe,L_abi
      REAL*8 R8_ibi
      REAL*4 R_edi
      REAL*8 R8_udi
      LOGICAL*1 L_afi,L_ifi,L_ufi
      INTEGER*4 I_aki
      LOGICAL*1 L_ili,L_uli,L_api,L_opi,L_ari,L_eri,L_iri
     &,L_ori,L_uri,L_asi
      End subroutine TOLKATEL_OZ_HANDLER_dpro
      End interface
