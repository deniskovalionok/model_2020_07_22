      Interface
      Subroutine TOLKATEL_HANDLER_DOZ(ext_deltat,R_ud,R_af
     &,R_uk,R_el,R_il,R_ul,R_am,L_om,L_ap,R_ep,R_ip,R_op,I_ar
     &,R_or,R_ur,R_as,I_ov,I_ex,R_ix,R_ox,R_ux,R_abe,L_ebe
     &,L_ibe,I_obe,I_ede,I_efe,I_ufe,C20_uke,C20_ule,C8_ume
     &,L_are,R_ure,R_ase,L_ese,R_ise,R_ose,I_use,L_ite,L_ote
     &,L_ute,L_ave,I_eve,I_uve,L_ixe,L_uxe,L_abi,L_ebi,L_adi
     &,L_odi,L_udi,L_ifi,L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki
     &,L_ali,L_eli,L_ili,L_oli,L_uli,L_ami,L_imi,R8_umi,R_opi
     &,R8_eri,L_iri,I_ori,L_esi,L_isi,L_eti,L_avi,L_ivi,L_ovi
     &,L_uvi,L_axi)
C |R_ud          |4 4 O|VX02_OE         |�������� ����������� ���������, �.�.*�||
C |R_af          |4 4 I|tcl_top         |�������� �������|`top_c`|
C |R_uk          |4 4 I|CBO_POS         ||`p_TOLKATEL_XH54`|
C |R_el          |4 4 I|CBC_POS         ||`p_TOLKATEL_XH53`|
C |R_il          |4 4 I|MAXX            ||`p_UP`|
C |R_ul          |4 4 O|VX01            |��������� ���������, ��||
C |R_am          |4 4 I|MINX            ||`p_LOW`|
C |L_om          |1 1 I|YA25C           |������� ������� �� ���������� (��)|F|
C |L_ap          |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_ep          |4 4 K|_uintT_INT      |����������� ������ ����������� ������|1|
C |R_ip          |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_op          |4 4 K|_lintT_INT      |����������� ������ ����������� �����|0|
C |I_ar          |2 4 O|LWORK           |����� �������||
C |R_or          |4 4 O|VX01_OE         |��������� ���������, �.�.||
C |R_ur          |4 4 O|_ointT_INT*     |�������� ������ ����������� |0|
C |R_as          |4 4 O|VX02            |�������� ����������� ���������, ��*�||
C |I_ov          |2 4 O|LREADY          |����� ����������||
C |I_ex          |2 4 O|LBUSY           |����� �����||
C |R_ix          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ��������� (�������)" |0.0|
C |R_ox          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ��������� (�������)" |0.0|
C |R_ux          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ��������� (�������)" |0.0|
C |R_abe         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ��������� (�������)" |0.0|
C |L_ebe         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ibe         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_obe         |2 4 O|state1          |��������� 1||
C |I_ede         |2 4 O|state2          |��������� 2||
C |I_efe         |2 4 O|LWORKO          |����� � �������||
C |I_ufe         |2 4 O|LINITC          |����� � ��������||
C |C20_uke       |3 20 O|task_state      |���������||
C |C20_ule       |3 20 O|task_name       |������� ���������||
C |C8_ume        |3 8 I|task            |������� ���������||
C |L_are         |1 1 O|XH54            |�� ������� (���)|F|
C |R_ure         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ase         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ese         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ise         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ose         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_use         |2 4 O|LERROR          |����� �������������||
C |L_ite         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ote         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_ute         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_ave         |1 1 O|XH53            |�� ������� (���)|F|
C |I_eve         |2 4 O|LINIT           |����� ��������||
C |I_uve         |2 4 O|LZM             |������ "�������"||
C |L_ixe         |1 1 I|vlv_kvit        |||
C |L_uxe         |1 1 I|instr_fault     |||
C |L_abi         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ebi         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ��������� (�������)|F|
C |L_adi         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ��������� (�������)|F|
C |L_odi         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_udi         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ifi         |1 1 O|block           |||
C |L_ufi         |1 1 O|norm            |�����||
C |L_aki         |1 1 I|mlf15           |������ ���������������� ����� �������||
C |L_eki         |1 1 I|mlf14           |������ ������������ ����� �������||
C |L_iki         |1 1 I|mlf17           |������ ���������������� ����� �������||
C |L_oki         |1 1 I|mlf16           |������ ������������ ����� �������||
C |L_uki         |1 1 I|mlf27           |������ ������������ ��������� �������||
C |L_ali         |1 1 I|mlf25           |������ ������������ ��������� �������||
C |L_eli         |1 1 I|mlf09           |����� ��������� �������||
C |L_ili         |1 1 I|mlf08           |����� ��������� �������||
C |L_oli         |1 1 I|mlf07           |���������� ���� �������||
C |L_uli         |1 1 I|mlf06           |������������� ������� �������||
C |L_ami         |1 1 I|mlf05           |������������� ������� �������||
C |L_imi         |1 1 O|nopower         |��� ����������||
C |R8_umi        |4 8 I|voltage         |[��]���������� �� ������|0.4|
C |R_opi         |4 4 I|power           |�������� ��������||
C |R8_eri        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_iri         |1 1 O|fault           |�������������||
C |I_ori         |2 4 O|LAM             |������ "�������"||
C |L_esi         |1 1 O|XH51            |�� ������� (���)|F|
C |L_isi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_eti         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_avi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ivi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ovi         |1 1 I|mlf04           |�������� �� ��������||
C |L_uvi         |1 1 I|mlf03           |�������� �� ��������||
C |L_axi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_ud,R_af,R_uk,R_el,R_il,R_ul,R_am
      LOGICAL*1 L_om,L_ap
      REAL*4 R_ep,R_ip,R_op
      INTEGER*4 I_ar
      REAL*4 R_or,R_ur,R_as
      INTEGER*4 I_ov,I_ex
      REAL*4 R_ix,R_ox,R_ux,R_abe
      LOGICAL*1 L_ebe,L_ibe
      INTEGER*4 I_obe,I_ede,I_efe,I_ufe
      CHARACTER*20 C20_uke,C20_ule
      CHARACTER*8 C8_ume
      LOGICAL*1 L_are
      REAL*4 R_ure,R_ase
      LOGICAL*1 L_ese
      REAL*4 R_ise,R_ose
      INTEGER*4 I_use
      LOGICAL*1 L_ite,L_ote,L_ute,L_ave
      INTEGER*4 I_eve,I_uve
      LOGICAL*1 L_ixe,L_uxe,L_abi,L_ebi,L_adi,L_odi,L_udi
     &,L_ifi,L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki,L_ali,L_eli
     &,L_ili,L_oli,L_uli,L_ami,L_imi
      REAL*8 R8_umi
      REAL*4 R_opi
      REAL*8 R8_eri
      LOGICAL*1 L_iri
      INTEGER*4 I_ori
      LOGICAL*1 L_esi,L_isi,L_eti,L_avi,L_ivi,L_ovi,L_uvi
     &,L_axi
      End subroutine TOLKATEL_HANDLER_DOZ
      End interface
