      Interface
      Subroutine PIDmod(ext_deltat,L_i,R_ad,L_ed,R8_od,L_ud
     &,R8_ak,R_uk,R_ul,R_im,R_om,R_um,R_ap,R_ep,R_ip,R_up
     &,L_ur,R_es,R_at)
C |L_i           |1 1 I|fladd           |����� �������||
C |R_ad          |4 4 I|addstp          |��������||
C |L_ed          |1 1 O|tracmod1        |����� ��������||
C |R8_od         |4 8 I|posin           |���� ��������� ������� (��������)||
C |L_ud          |1 1 I|flagmod         |����� ������� �����||
C |R8_ak         |4 8 O|out             |����� ��������� ��������||
C |R_uk          |4 4 I|k4              |�-� ����������� ��||
C |R_ul          |4 4 I|t3              |���������� ������� ���������������||
C |R_im          |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_om          |4 4 I|k3              |�-� ���������������||
C |R_um          |4 4 I|u1              |����������� �����������||
C |R_ap          |4 4 I|k2              |�-� ������������||
C |R_ep          |4 4 O|dlt             |������� ��������� ����������||
C |R_ip          |4 4 I|k1              |�-� ����������������||
C |R_up          |4 4 O|_odif1*         |�������� ������ |0.0|
C |L_ur          |1 1 I|tracmod         |����� ��������||
C |R_es          |4 4 S|_oint1*         |����� ����������� |0.0|
C |R_at          |4 4 I|delta           |������� ��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_i
      REAL*4 R_ad
      LOGICAL*1 L_ed
      REAL*8 R8_od
      LOGICAL*1 L_ud
      REAL*8 R8_ak
      REAL*4 R_uk,R_ul,R_im,R_om,R_um,R_ap,R_ep,R_ip,R_up
      LOGICAL*1 L_ur
      REAL*4 R_es,R_at
      End subroutine PIDmod
      End interface
