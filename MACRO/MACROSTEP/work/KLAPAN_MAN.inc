      Interface
      Subroutine KLAPAN_MAN(ext_deltat,R_aki,R8_ose,I_e,C8_ef
     &,I_if,R_ek,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul,I_am,I_om
     &,I_ep,I_up,L_er,L_or,L_ur,L_as,L_es,R_os,L_us,L_at,L_ut
     &,L_av,L_ov,L_uv,L_ex,L_ox,R8_abe,R_ube,R8_ide,L_ode
     &,L_ude,L_efe,L_ife,L_uke,I_ale,L_ase,R_ate,R_axe,L_ibi
     &,L_ubi,L_afi,L_ofi,L_eki,L_iki,L_oki,L_uki,L_ali,L_eli
     &)
C |R_aki         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ose        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_e           |2 4 O|LREADY          |����� ����������||
C |C8_ef         |3 8 O|TEXT            |�������� ���������||
C |I_if          |2 4 O|LF              |����� �������������||
C |R_ek          |4 4 O|POS_CL          |��������||
C |R_ok          |4 4 O|POS_OP          |��������||
C |R_uk          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_al          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_el          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_il          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ol          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ul          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_am          |2 4 O|LST             |����� ����||
C |I_om          |2 4 O|LCL             |����� �������||
C |I_ep          |2 4 O|LOP             |����� �������||
C |I_up          |2 4 O|LZM             |������ "�������"||
C |L_er          |1 1 I|vlv_kvit        |||
C |L_or          |1 1 I|instr_fault     |||
C |L_ur          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_es          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |R_os          |4 4 I|tcl             |����� ��������|30.0|
C |L_us          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_at          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ut          |1 1 O|block           |||
C |L_av          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ov          |1 1 O|STOP            |�������||
C |L_uv          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ex          |1 1 O|norm            |�����||
C |L_ox          |1 1 O|nopower         |��� ����������||
C |R8_abe        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ube         |4 4 I|power           |�������� ��������||
C |R8_ide        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ode         |1 1 I|YA22C           |������� ������� �� ����������|F|
C |L_ude         |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_efe         |1 1 I|YA21C           |������� ������� �� ����������|F|
C |L_ife         |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_uke         |1 1 O|fault           |�������������||
C |I_ale         |2 4 O|LAM             |������ "�������"||
C |L_ase         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ate         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_axe         |4 4 I|top             |����� ��������|30.0|
C |L_ibi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ubi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_afi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ofi         |1 1 O|XH51            |�� ������� (���)|F|
C |L_eki         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iki         |1 1 I|mlf23           |������� ������� �����||
C |L_oki         |1 1 I|mlf22           |����� ����� ��������||
C |L_uki         |1 1 I|mlf04           |�������� �� ��������||
C |L_ali         |1 1 I|mlf03           |�������� �� ��������||
C |L_eli         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_e
      CHARACTER*8 C8_ef
      INTEGER*4 I_if
      REAL*4 R_ek,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul
      INTEGER*4 I_am,I_om,I_ep,I_up
      LOGICAL*1 L_er,L_or,L_ur,L_as,L_es
      REAL*4 R_os
      LOGICAL*1 L_us,L_at,L_ut,L_av,L_ov,L_uv,L_ex,L_ox
      REAL*8 R8_abe
      REAL*4 R_ube
      REAL*8 R8_ide
      LOGICAL*1 L_ode,L_ude,L_efe,L_ife,L_uke
      INTEGER*4 I_ale
      LOGICAL*1 L_ase
      REAL*8 R8_ose
      REAL*4 R_ate,R_axe
      LOGICAL*1 L_ibi,L_ubi,L_afi,L_ofi
      REAL*4 R_aki
      LOGICAL*1 L_eki,L_iki,L_oki,L_uki,L_ali,L_eli
      End subroutine KLAPAN_MAN
      End interface
