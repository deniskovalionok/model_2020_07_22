      Subroutine SHIB_HANDLER_dpro(ext_deltat,R8_e,R_i,R_id
     &,R_if,R_of,L_uf,R_ik,R_ok,L_uk,R_il,R_ol,R_ul,R_am,R_em
     &,R_im,R_om,R_ip,R_op,C20_us,I_ot,I_ev,R_iv,R_ov,R_uv
     &,R_ax,I_ex,I_ux,I_ube,I_ide,C20_ife,C20_ike,C8_ile,L_epe
     &,R_ipe,R_ope,L_upe,R_are,R_ere,I_ire,I_ase,I_ose,I_ete
     &,L_ute,L_eve,L_ive,L_uve,L_exe,L_ixe,L_oxe,L_ibi,L_edi
     &,L_idi,L_udi,L_efi,R8_ofi,R_iki,R8_ali,L_eli,L_ili,L_uli
     &,L_ami,L_imi,I_omi,L_upi,L_eri,L_esi,L_usi,L_eti,L_iti
     &,L_oti,L_uti,L_avi,L_evi)
C |R8_e          |4 8 O|tg_pos          |��������� ������ � �.�. ��� ��||
C |R_i           |4 4 K|_cJ3086         |�������� ��������� ����������|`p_SHIBER_UP`|
C |R_id          |4 4 K|_cJ3015         |�������� ��������� ����������|`p_SHIBER_UP`|
C |R_if          |4 4 S|_simpJ3009*     |[���]���������� ��������� ������������� |0.0|
C |R_of          |4 4 K|_timpJ3009      |[���]������������ �������� �������������|0.4|
C |L_uf          |1 1 S|_limpJ3009*     |[TF]���������� ��������� ������������� |F|
C |R_ik          |4 4 S|_simpJ3007*     |[���]���������� ��������� ������������� |0.0|
C |R_ok          |4 4 K|_timpJ3007      |[���]������������ �������� �������������|0.4|
C |L_uk          |1 1 S|_limpJ3007*     |[TF]���������� ��������� ������������� |F|
C |R_il          |4 4 K|_uintV_INT_SH   |����������� ������ ����������� ������|`p_SHIBER_UP`|
C |R_ol          |4 4 K|_tintV_INT_SH   |[���]�������� T �����������|1|
C |R_ul          |4 4 K|_lintV_INT_SH   |����������� ������ ����������� �����|0.0|
C |R_am          |4 4 K|_lcmpJ2871      |[]�������� ������ �����������|0.001|
C |R_em          |4 4 K|_lcmpJ2841      |[]�������� ������ �����������|`p_SHIBER_XH54`|
C |R_im          |4 4 O|VZ01            |��������� ������ �� ��� Z||
C |R_om          |4 4 O|_ointV_INT_SH*  |�������� ������ ����������� |0|
C |R_ip          |4 4 O|VZ02            |�������� ����������� ������||
C |R_op          |4 4 I|tcl_top         |����� ����|20.0|
C |C20_us        |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_ot          |2 4 O|LREADY          |����� ����������||
C |I_ev          |2 4 O|LBUSY           |����� �����||
C |R_iv          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ov          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_uv          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ax          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_ex          |2 4 O|state1          |��������� 1||
C |I_ux          |2 4 O|state2          |��������� 2||
C |I_ube         |2 4 O|LOPENO          |����� �����������||
C |I_ide         |2 4 O|LCLOSEC         |����� �����������||
C |C20_ife       |3 20 O|task_state      |���������||
C |C20_ike       |3 20 O|task_name       |������� ���������||
C |C8_ile        |3 8 I|task            |������� ���������||
C |L_epe         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |R_ipe         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ope         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_upe         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_are         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ere         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ire         |2 4 O|LERROR          |����� �������������||
C |I_ase         |2 4 O|LCLOSE          |����� ������||
C |I_ose         |2 4 O|LOPEN           |����� ������||
C |I_ete         |2 4 O|LZM             |������ "�������"||
C |L_ute         |1 1 I|vlv_kvit        |||
C |L_eve         |1 1 I|instr_fault     |||
C |L_ive         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_uve         |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_exe         |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ixe         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_oxe         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ibi         |1 1 O|block           |||
C |L_edi         |1 1 O|STOP            |�������||
C |L_idi         |1 1 S|_qffJ3027*      |�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 O|norm            |�����||
C |L_efi         |1 1 O|nopower         |��� ����������||
C |R8_ofi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_iki         |4 4 I|power           |�������� ��������||
C |R8_ali        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_eli         |1 1 I|YA27            |������� ������ �������|F|
C |L_ili         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_uli         |1 1 I|YA28            |������� ������ �������|F|
C |L_ami         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_imi         |1 1 O|fault           |�������������||
C |I_omi         |2 4 O|LAM             |������ "�������"||
C |L_upi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_eri         |1 1 O|XH53            |�� ������� (���)|F|
C |L_esi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_usi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_eti         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iti         |1 1 I|mlf23           |������� ������� �����||
C |L_oti         |1 1 I|mlf22           |����� ����� ��������||
C |L_uti         |1 1 I|mlf04           |�������� �� ��������||
C |L_avi         |1 1 I|mlf03           |�������� �� ��������||
C |L_evi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*8 R8_e
      REAL*4 R_i,R0_o,R0_u
      LOGICAL*1 L0_ad,L0_ed
      REAL*4 R_id,R0_od,R0_ud,R0_af,R0_ef,R_if,R_of
      LOGICAL*1 L_uf,L0_ak
      REAL*4 R0_ek,R_ik,R_ok
      LOGICAL*1 L_uk,L0_al,L0_el
      REAL*4 R_il,R_ol,R_ul,R_am,R_em,R_im,R_om
      LOGICAL*1 L0_um,L0_ap
      REAL*4 R0_ep,R_ip,R_op,R0_up,R0_ar,R0_er,R0_ir
      LOGICAL*1 L0_or,L0_ur
      CHARACTER*20 C20_as,C20_es,C20_is,C20_os,C20_us
      INTEGER*4 I0_at,I0_et
      LOGICAL*1 L0_it
      INTEGER*4 I_ot,I0_ut,I0_av,I_ev
      REAL*4 R_iv,R_ov,R_uv,R_ax
      INTEGER*4 I_ex,I0_ix,I0_ox,I_ux,I0_abe,I0_ebe,I0_ibe
     &,I0_obe,I_ube,I0_ade,I0_ede,I_ide
      CHARACTER*20 C20_ode,C20_ude,C20_afe,C20_efe,C20_ife
     &,C20_ofe,C20_ufe,C20_ake,C20_eke,C20_ike
      CHARACTER*8 C8_oke
      LOGICAL*1 L0_uke
      CHARACTER*8 C8_ale
      LOGICAL*1 L0_ele
      CHARACTER*8 C8_ile
      INTEGER*4 I0_ole,I0_ule,I0_ame,I0_eme,I0_ime,I0_ome
     &,I0_ume,I0_ape
      LOGICAL*1 L_epe
      REAL*4 R_ipe,R_ope
      LOGICAL*1 L_upe
      REAL*4 R_are,R_ere
      INTEGER*4 I_ire,I0_ore,I0_ure,I_ase,I0_ese,I0_ise,I_ose
     &,I0_use,I0_ate,I_ete,I0_ite,I0_ote
      LOGICAL*1 L_ute,L0_ave,L_eve,L_ive,L0_ove,L_uve,L0_axe
     &,L_exe,L_ixe,L_oxe,L0_uxe,L0_abi,L0_ebi,L_ibi,L0_obi
     &,L0_ubi,L0_adi,L_edi,L_idi,L0_odi,L_udi,L0_afi
      LOGICAL*1 L_efi
      REAL*4 R0_ifi
      REAL*8 R8_ofi
      LOGICAL*1 L0_ufi,L0_aki
      REAL*4 R0_eki,R_iki,R0_oki,R0_uki
      REAL*8 R8_ali
      LOGICAL*1 L_eli,L_ili,L0_oli,L_uli,L_ami,L0_emi,L_imi
      INTEGER*4 I_omi,I0_umi,I0_api
      LOGICAL*1 L0_epi,L0_ipi,L0_opi,L_upi,L0_ari,L_eri,L0_iri
     &,L0_ori,L0_uri,L0_asi,L_esi,L0_isi,L0_osi,L_usi,L0_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ef=R_if
C SHIB_HANDLER_dpro.fmg( 360, 208):pre: ������������  �� T
      R0_ek=R_ik
C SHIB_HANDLER_dpro.fmg( 360, 218):pre: ������������  �� T
      !��������� R0_o = SHIB_HANDLER_dproC?? /`p_SHIBER_UP`
C /
      R0_o=R_i
C SHIB_HANDLER_dpro.fmg( 338, 198):���������
      !��������� R0_ud = SHIB_HANDLER_dproC?? /`p_SHIBER_UP`
C /
      R0_ud=R_id
C SHIB_HANDLER_dpro.fmg( 338, 208):���������
      R0_od = 0.01
C SHIB_HANDLER_dpro.fmg( 338, 206):��������� (RE4) (�������)
      R0_af = R0_ud + (-R0_od)
C SHIB_HANDLER_dpro.fmg( 341, 208):��������
      R0_ep = 0.0
C SHIB_HANDLER_dpro.fmg( 312, 246):��������� (RE4) (�������)
      R0_ir = 0.0
C SHIB_HANDLER_dpro.fmg( 298, 246):��������� (RE4) (�������)
      C20_es = ''
C SHIB_HANDLER_dpro.fmg(  74, 158):��������� ���������� CH20 (CH30) (�������)
      C20_as = '�������'
C SHIB_HANDLER_dpro.fmg(  74, 156):��������� ���������� CH20 (CH30) (�������)
      C20_is = '�������'
C SHIB_HANDLER_dpro.fmg(  96, 158):��������� ���������� CH20 (CH30) (�������)
      I0_at = z'0100000A'
C SHIB_HANDLER_dpro.fmg( 220, 268):��������� ������������� IN (�������)
      I0_et = z'01000003'
C SHIB_HANDLER_dpro.fmg( 220, 270):��������� ������������� IN (�������)
      I0_av = z'0100000A'
C SHIB_HANDLER_dpro.fmg( 220, 290):��������� ������������� IN (�������)
      I0_ut = z'01000003'
C SHIB_HANDLER_dpro.fmg( 220, 288):��������� ������������� IN (�������)
      L_uve=R_ov.ne.R_iv
      R_iv=R_ov
C SHIB_HANDLER_dpro.fmg(  14, 194):���������� ������������� ������
      L_exe=R_ax.ne.R_uv
      R_uv=R_ax
C SHIB_HANDLER_dpro.fmg(  19, 249):���������� ������������� ������
      I0_ox = z'01000003'
C SHIB_HANDLER_dpro.fmg( 138, 162):��������� ������������� IN (�������)
      I0_ix = z'01000010'
C SHIB_HANDLER_dpro.fmg( 138, 160):��������� ������������� IN (�������)
      I0_ebe = z'01000003'
C SHIB_HANDLER_dpro.fmg( 117, 252):��������� ������������� IN (�������)
      I0_abe = z'01000010'
C SHIB_HANDLER_dpro.fmg( 117, 250):��������� ������������� IN (�������)
      I0_obe = z'01000003'
C SHIB_HANDLER_dpro.fmg( 185, 229):��������� ������������� IN (�������)
      I0_ibe = z'0100000A'
C SHIB_HANDLER_dpro.fmg( 185, 227):��������� ������������� IN (�������)
      I0_ede = z'01000003'
C SHIB_HANDLER_dpro.fmg( 193, 179):��������� ������������� IN (�������)
      I0_ade = z'0100000A'
C SHIB_HANDLER_dpro.fmg( 193, 177):��������� ������������� IN (�������)
      I0_ise = z'01000003'
C SHIB_HANDLER_dpro.fmg( 115, 171):��������� ������������� IN (�������)
      I0_ese = z'0100000A'
C SHIB_HANDLER_dpro.fmg( 115, 169):��������� ������������� IN (�������)
      C20_afe = '������'
C SHIB_HANDLER_dpro.fmg(  29, 160):��������� ���������� CH20 (CH30) (�������)
      C20_efe = '������� ���'
C SHIB_HANDLER_dpro.fmg(  29, 162):��������� ���������� CH20 (CH30) (�������)
      C20_ode = '������'
C SHIB_HANDLER_dpro.fmg(  44, 160):��������� ���������� CH20 (CH30) (�������)
      C20_ofe = '�����������'
C SHIB_HANDLER_dpro.fmg(  50, 171):��������� ���������� CH20 (CH30) (�������)
      C20_ake = '�����������'
C SHIB_HANDLER_dpro.fmg(  34, 172):��������� ���������� CH20 (CH30) (�������)
      C20_eke = ''
C SHIB_HANDLER_dpro.fmg(  34, 174):��������� ���������� CH20 (CH30) (�������)
      C8_oke = 'close'
C SHIB_HANDLER_dpro.fmg(  18, 180):��������� ���������� CH8 (�������)
      call chcomp(C8_ile,C8_oke,L0_uke)
C SHIB_HANDLER_dpro.fmg(  23, 184):���������� ���������
      C8_ale = 'open'
C SHIB_HANDLER_dpro.fmg(  18, 222):��������� ���������� CH8 (�������)
      call chcomp(C8_ile,C8_ale,L0_ele)
C SHIB_HANDLER_dpro.fmg(  23, 226):���������� ���������
      if(L0_ele) then
         C20_os=C20_as
      else
         C20_os=C20_es
      endif
C SHIB_HANDLER_dpro.fmg(  78, 157):���� RE IN LO CH20
      if(L0_uke) then
         C20_us=C20_is
      else
         C20_us=C20_os
      endif
C SHIB_HANDLER_dpro.fmg( 100, 158):���� RE IN LO CH20
      if(L0_ele) then
         C20_ufe=C20_ake
      else
         C20_ufe=C20_eke
      endif
C SHIB_HANDLER_dpro.fmg(  38, 172):���� RE IN LO CH20
      if(L0_uke) then
         C20_ike=C20_ofe
      else
         C20_ike=C20_ufe
      endif
C SHIB_HANDLER_dpro.fmg(  55, 172):���� RE IN LO CH20
      I0_ole = z'0100008E'
C SHIB_HANDLER_dpro.fmg( 170, 194):��������� ������������� IN (�������)
      I0_ule = z'01000086'
C SHIB_HANDLER_dpro.fmg( 164, 248):��������� ������������� IN (�������)
      I0_eme = z'01000003'
C SHIB_HANDLER_dpro.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ime = z'01000010'
C SHIB_HANDLER_dpro.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ape = z'01000003'
C SHIB_HANDLER_dpro.fmg( 159, 209):��������� ������������� IN (�������)
      I0_ume = z'01000010'
C SHIB_HANDLER_dpro.fmg( 159, 207):��������� ������������� IN (�������)
      L_epe=R_ope.ne.R_ipe
      R_ipe=R_ope
C SHIB_HANDLER_dpro.fmg(  14, 208):���������� ������������� ������
      L_upe=R_ere.ne.R_are
      R_are=R_ere
C SHIB_HANDLER_dpro.fmg(  19, 238):���������� ������������� ������
      L0_axe = L_upe.AND.L0_ele
C SHIB_HANDLER_dpro.fmg(  30, 236):�
      L0_ubi = L_exe.OR.L0_axe
C SHIB_HANDLER_dpro.fmg(  52, 237):���
      L0_emi = L0_ubi.AND.(.NOT.L_oxe)
C SHIB_HANDLER_dpro.fmg(  66, 236):�
      L0_osi = L0_emi.OR.L_ami.OR.L_uli
C SHIB_HANDLER_dpro.fmg(  70, 233):���
      L0_ove = L_upe.AND.L0_uke
C SHIB_HANDLER_dpro.fmg(  29, 192):�
      L0_obi = L_uve.OR.L0_ove
C SHIB_HANDLER_dpro.fmg(  52, 193):���
      L0_oli = (.NOT.L_ixe).AND.L0_obi
C SHIB_HANDLER_dpro.fmg(  66, 194):�
      L0_iri = L0_oli.OR.L_ili.OR.L_eli
C SHIB_HANDLER_dpro.fmg(  70, 192):���
      L0_adi = L0_osi.OR.L0_iri
C SHIB_HANDLER_dpro.fmg(  76, 203):���
      I0_ite = z'0100000E'
C SHIB_HANDLER_dpro.fmg( 189, 200):��������� ������������� IN (�������)
      I0_ore = z'01000007'
C SHIB_HANDLER_dpro.fmg( 150, 180):��������� ������������� IN (�������)
      I0_ure = z'01000003'
C SHIB_HANDLER_dpro.fmg( 150, 182):��������� ������������� IN (�������)
      I0_ate = z'01000003'
C SHIB_HANDLER_dpro.fmg( 122, 265):��������� ������������� IN (�������)
      I0_use = z'0100000A'
C SHIB_HANDLER_dpro.fmg( 122, 263):��������� ������������� IN (�������)
      I0_umi = z'0100000E'
C SHIB_HANDLER_dpro.fmg( 182, 255):��������� ������������� IN (�������)
      L_ive=(L_eve.or.L_ive).and..not.(L_ute)
      L0_ave=.not.L_ive
C SHIB_HANDLER_dpro.fmg( 326, 156):RS �������
      L0_ebi=.false.
C SHIB_HANDLER_dpro.fmg(  62, 217):��������� ���������� (�������)
      L0_abi=.false.
C SHIB_HANDLER_dpro.fmg(  62, 215):��������� ���������� (�������)
      L0_uxe=.false.
C SHIB_HANDLER_dpro.fmg(  62, 213):��������� ���������� (�������)
      L_ibi = L0_ebi.OR.L0_abi.OR.L0_uxe.OR.L_oxe.OR.L_ixe
C SHIB_HANDLER_dpro.fmg(  68, 213):���
      if(L_ibi) then
         I_ev=I0_at
      else
         I_ev=I0_et
      endif
C SHIB_HANDLER_dpro.fmg( 224, 269):���� RE IN LO CH7
      L0_afi =.NOT.(L_avi.OR.L_uti.OR.L_evi.OR.L_oti.OR.L_iti.OR.L_eti
     &)
C SHIB_HANDLER_dpro.fmg( 319, 170):���
      L0_odi =.NOT.(L0_afi)
C SHIB_HANDLER_dpro.fmg( 328, 164):���
      L_imi = L0_odi.OR.L_ive
C SHIB_HANDLER_dpro.fmg( 332, 162):���
      L0_it = L_imi.OR.L_ibi
C SHIB_HANDLER_dpro.fmg( 218, 280):���
      if(L0_it) then
         I_ot=I0_ut
      else
         I_ot=I0_av
      endif
C SHIB_HANDLER_dpro.fmg( 224, 288):���� RE IN LO CH7
      if(L_imi) then
         I_ire=I0_ore
      else
         I_ire=I0_ure
      endif
C SHIB_HANDLER_dpro.fmg( 153, 181):���� RE IN LO CH7
      L_udi = L0_afi.OR.L_eve
C SHIB_HANDLER_dpro.fmg( 328, 168):���
      R0_ifi = 0.1
C SHIB_HANDLER_dpro.fmg( 254, 160):��������� (RE4) (�������)
      L_efi=R8_ofi.lt.R0_ifi
C SHIB_HANDLER_dpro.fmg( 259, 162):���������� <
      R0_eki = 0.0
C SHIB_HANDLER_dpro.fmg( 266, 182):��������� (RE4) (�������)
      L0_epi = L_idi.OR.L_eti
C SHIB_HANDLER_dpro.fmg(  98, 214):���
C label 165  try165=try165-1
      L_usi=R_om.gt.R_em
C SHIB_HANDLER_dpro.fmg( 351, 239):���������� >
      L0_asi = L_usi.OR.L0_epi.OR.L0_iri
C SHIB_HANDLER_dpro.fmg( 104, 229):���
      L0_ati = (.NOT.L_usi).AND.L0_osi
C SHIB_HANDLER_dpro.fmg( 104, 235):�
      L_esi=(L0_ati.or.L_esi).and..not.(L0_asi)
      L0_isi=.not.L_esi
C SHIB_HANDLER_dpro.fmg( 111, 233):RS �������,1
      L0_uri = (.NOT.L_usi).AND.L_esi
C SHIB_HANDLER_dpro.fmg( 130, 236):�
      L0_ap = L0_uri.AND.(.NOT.L_uti)
C SHIB_HANDLER_dpro.fmg( 268, 245):�
      L0_ur = L0_ap.OR.L_avi
C SHIB_HANDLER_dpro.fmg( 281, 244):���
      if(L0_ur) then
         R0_ar=R_op
      else
         R0_ar=R0_ir
      endif
C SHIB_HANDLER_dpro.fmg( 303, 256):���� RE IN LO CH7
      L0_opi = L0_epi.OR.L_eri.OR.L0_osi
C SHIB_HANDLER_dpro.fmg( 104, 185):���
      L0_ori = L0_iri.AND.(.NOT.L_eri)
C SHIB_HANDLER_dpro.fmg( 104, 191):�
      L_upi=(L0_ori.or.L_upi).and..not.(L0_opi)
      L0_ari=.not.L_upi
C SHIB_HANDLER_dpro.fmg( 111, 189):RS �������,2
      L0_ipi = L_upi.AND.(.NOT.L_eri)
C SHIB_HANDLER_dpro.fmg( 130, 190):�
      L0_um = L0_ipi.AND.(.NOT.L_avi)
C SHIB_HANDLER_dpro.fmg( 268, 231):�
      L0_or = L0_um.OR.L_uti
C SHIB_HANDLER_dpro.fmg( 281, 230):���
      if(L0_or) then
         R0_up=R_op
      else
         R0_up=R0_ir
      endif
C SHIB_HANDLER_dpro.fmg( 303, 238):���� RE IN LO CH7
      R0_er = R0_ar + (-R0_up)
C SHIB_HANDLER_dpro.fmg( 309, 249):��������
      if(L_eti) then
         R_ip=R0_ep
      else
         R_ip=R0_er
      endif
C SHIB_HANDLER_dpro.fmg( 316, 248):���� RE IN LO CH7
      R_om=R_om+deltat/R_ol*R_ip
      if(R_om.gt.R_il) then
         R_om=R_il
      elseif(R_om.lt.R_ul) then
         R_om=R_ul
      endif
C SHIB_HANDLER_dpro.fmg( 332, 232):����������,V_INT_SH
      L_eri=R_om.lt.R_am
C SHIB_HANDLER_dpro.fmg( 352, 227):���������� <
C sav1=L0_opi
      L0_opi = L0_epi.OR.L_eri.OR.L0_osi
C SHIB_HANDLER_dpro.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_opi .and. try203.gt.0) goto 203
C sav1=L0_ori
      L0_ori = L0_iri.AND.(.NOT.L_eri)
C SHIB_HANDLER_dpro.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_ori .and. try205.gt.0) goto 205
C sav1=L0_ipi
      L0_ipi = L_upi.AND.(.NOT.L_eri)
C SHIB_HANDLER_dpro.fmg( 130, 190):recalc:�
C if(sav1.ne.L0_ipi .and. try209.gt.0) goto 209
      if(L_eri.and..not.L_uk) then
         R_ik=R_ok
      else
         R_ik=max(R0_ek-deltat,0.0)
      endif
      L0_el=R_ik.gt.0.0
      L_uk=L_eri
C SHIB_HANDLER_dpro.fmg( 360, 218):������������  �� T
      L0_ak=R_om.gt.R0_af
C SHIB_HANDLER_dpro.fmg( 350, 208):���������� >
      if(L0_ak.and..not.L_uf) then
         R_if=R_of
      else
         R_if=max(R0_ef-deltat,0.0)
      endif
      L0_al=R_if.gt.0.0
      L_uf=L0_ak
C SHIB_HANDLER_dpro.fmg( 360, 208):������������  �� T
      L0_ed = L0_el.OR.L_epe.OR.L0_al
C SHIB_HANDLER_dpro.fmg(  58, 207):���
      L_idi=L0_ed.or.(L_idi.and..not.(L0_adi))
      L0_ad=.not.L_idi
C SHIB_HANDLER_dpro.fmg( 106, 205):RS �������
C sav1=L0_epi
      L0_epi = L_idi.OR.L_eti
C SHIB_HANDLER_dpro.fmg(  98, 214):recalc:���
C if(sav1.ne.L0_epi .and. try165.gt.0) goto 165
      L_edi=L_idi
C SHIB_HANDLER_dpro.fmg( 125, 207):������,STOP
      if(L_eri) then
         I_ex=I0_ix
      else
         I_ex=I0_ox
      endif
C SHIB_HANDLER_dpro.fmg( 142, 160):���� RE IN LO CH7
      if(L_eri) then
         I_ase=I0_ese
      else
         I_ase=I0_ise
      endif
C SHIB_HANDLER_dpro.fmg( 118, 170):���� RE IN LO CH7
      if(L_eri) then
         I0_ame=I0_eme
      else
         I0_ame=I0_ime
      endif
C SHIB_HANDLER_dpro.fmg( 156, 262):���� RE IN LO CH7
      if(L0_uri) then
         I0_api=I0_ule
      else
         I0_api=I0_ame
      endif
C SHIB_HANDLER_dpro.fmg( 168, 262):���� RE IN LO CH7
      if(L_imi) then
         I_omi=I0_umi
      else
         I_omi=I0_api
      endif
C SHIB_HANDLER_dpro.fmg( 186, 260):���� RE IN LO CH7
      R_im=R_om
C SHIB_HANDLER_dpro.fmg( 386, 234):������,VZ01
      if(R0_o.ge.0.0) then
         R0_u=R_om/max(R0_o,1.0e-10)
      else
         R0_u=R_om/min(R0_o,-1.0e-10)
      endif
C SHIB_HANDLER_dpro.fmg( 350, 200):�������� ����������
      R8_e = R0_u
C SHIB_HANDLER_dpro.fmg( 368, 200):��������
      if(L0_ipi) then
         I_ide=I0_ade
      else
         I_ide=I0_ede
      endif
C SHIB_HANDLER_dpro.fmg( 196, 178):���� RE IN LO CH7
      L0_ufi = L0_uri.OR.L0_ipi
C SHIB_HANDLER_dpro.fmg( 251, 174):���
      L0_aki = L0_ufi.AND.(.NOT.L_efi)
C SHIB_HANDLER_dpro.fmg( 266, 173):�
      if(L0_aki) then
         R0_uki=R_iki
      else
         R0_uki=R0_eki
      endif
C SHIB_HANDLER_dpro.fmg( 269, 180):���� RE IN LO CH7
      if(L0_uri) then
         I_ube=I0_ibe
      else
         I_ube=I0_obe
      endif
C SHIB_HANDLER_dpro.fmg( 188, 228):���� RE IN LO CH7
      if(L_usi) then
         I_ose=I0_use
      else
         I_ose=I0_ate
      endif
C SHIB_HANDLER_dpro.fmg( 126, 264):���� RE IN LO CH7
      if(L_usi) then
         I_ux=I0_abe
      else
         I_ux=I0_ebe
      endif
C SHIB_HANDLER_dpro.fmg( 120, 250):���� RE IN LO CH7
      if(L_usi) then
         C20_ude=C20_afe
      else
         C20_ude=C20_efe
      endif
C SHIB_HANDLER_dpro.fmg(  33, 161):���� RE IN LO CH20
      if(L_eri) then
         C20_ife=C20_ode
      else
         C20_ife=C20_ude
      endif
C SHIB_HANDLER_dpro.fmg(  49, 160):���� RE IN LO CH20
      if(L_usi) then
         I0_ome=I0_ume
      else
         I0_ome=I0_ape
      endif
C SHIB_HANDLER_dpro.fmg( 162, 208):���� RE IN LO CH7
      if(L0_ipi) then
         I0_ote=I0_ole
      else
         I0_ote=I0_ome
      endif
C SHIB_HANDLER_dpro.fmg( 173, 206):���� RE IN LO CH7
      if(L_imi) then
         I_ete=I0_ite
      else
         I_ete=I0_ote
      endif
C SHIB_HANDLER_dpro.fmg( 192, 206):���� RE IN LO CH7
      R0_oki = R8_ali
C SHIB_HANDLER_dpro.fmg( 264, 190):��������
C label 320  try320=try320-1
      R8_ali = R0_uki + R0_oki
C SHIB_HANDLER_dpro.fmg( 275, 189):��������
C sav1=R0_oki
      R0_oki = R8_ali
C SHIB_HANDLER_dpro.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_oki .and. try320.gt.0) goto 320
      End
