
       subroutine k2vlva2_mnemo(outo,outc,rasp,
     & cbo,cbc,LAM,LZM,rlamp,glamp,
     & HLR,HLG,LAVT,LDIST,
     & WARD)
       implicit none
       logical*1 outo,outc,rasp,cbc,cbo,rlamp,glamp,
     & CHK, WARD
       integer*4 LAM,LZM,LAVT,LDIST,hlr,hlg
       integer*1 pif_hlr,pif_hlg
      
! ��������� ���������� �� ����������, �������� �� ����������� ������, ������������ �������� �� �������� �����

        LAM=16#01000013         !LT_MAGENTA
        LZM=16#01000013         !LT_MAGENTA
        LAVT=16#40000000        !����������
        HLR=16#00000040         !RED1
        LDIST=16#40000000       !����������
        HLG=16#0100003B         !GREEN1
        

       if(.not.rasp) then
       
! ��������� �������

       if(outo) then             !������� �������
         LAM=16#01000098         !��������� �������
        elseif(cbc) then         !�������
          LAM =16#0100000A       !�������
        else
          LAM=16#000000FF        !�������
        endif 

        if(outc) then            !������� �������
         LZM=16#01000094         !��������� �������
        elseif(cbo) then         !�������
          LZM=16#000000FF        !�������
        else
          LZM=16#0100000A        !�������
        endif 
       
! ����� ����������
        if( .not. WARD) then     ! ������� ������������ ( ��-�������)
			if(rlamp) then           !����� "�������"
				LAVT=16#000000FF        !�������

			endif

			if(glamp) then           !����� "���������"
				LDIST=16#0100000A    !�������

			endif
		else   !   ���� ���������� �����
!
!  ����� ���������� ����� ��� ���-�
!
			if(rlamp.and..not.glamp) then !������� - �����  ������� � �������
				LAVT=16#000000FF     !�������
				LDIST=16#0100000A    !�������

			endif
			if(rlamp.and.glamp) then !���������� - ����� ������ �������
				LAVT=16#000000FF     !�������

			endif
		
			if(glamp.and..not.rlamp) then           !����� "���������"
				LDIST=16#0100000A    !�������

			endif
 		endif    ! WARDMODE signalisation

       endif



       end
      

   