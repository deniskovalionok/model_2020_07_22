      SUBROUTINE putotable(nomer,scode,massa,day_og,mon_og,year_og,
     &hour_og,min_og,sec_og,day_sz,mon_sz,year_sz,hour_sz,min_sz,
     &sec_sz,content,valid,put,contcur)
          
      implicit none
      integer*4 nomer, scode, day_og,mon_og,year_og,hour_og,
     &min_og,sec_og,day_sz,mon_sz,year_sz,hour_sz,min_sz,sec_sz
      real*4 massa
      logical*1 valid,put
      character*255 content(1:40), contcur
          
      if(put) then
         if(valid)then
           write(content(nomer),100)nomer,scode,'valid',massa,
     &       day_og,mon_og,year_og,hour_og,min_og,sec_og,
     &       day_sz,mon_sz,year_sz,hour_sz,min_sz,sec_sz
           else
          write(content(nomer),100)nomer,scode,'not valid',massa,
     &       day_og,mon_og,year_og,hour_og,min_og,sec_og,
     &       day_sz,mon_sz,year_sz,hour_sz,min_sz,sec_sz
         endif
		 contcur=content(nomer)
      endif

100   format(I2.2,'  |  ',I10.10,'|',A9,'| ',F10.2,'| ',I2.2,
     &'  | ',I2.2,'  |',I4.4,' | ',I2.2,'  | ',I2.2,'  | ',I2.2,
     &'  | ',I2.2,'  | ',I2.2,'  |',I4.4,' | ',I2.2,'  | ',I2.2,
     &'  | ',I2.2)
      end
