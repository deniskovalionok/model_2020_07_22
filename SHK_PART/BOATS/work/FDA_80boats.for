      Subroutine FDA_80boats(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA_80boats.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 134, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT80
      I_(10)=I_e !CopyBack
C FDA_80boats.fgi( 134, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT80:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 154, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT79
      I_(9)=I_i !CopyBack
C FDA_80boats.fgi( 154, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT79:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 174, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT78
      I_(8)=I_o !CopyBack
C FDA_80boats.fgi( 174, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT78:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 194, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT77
      I_(7)=I_u !CopyBack
C FDA_80boats.fgi( 194, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT77:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 214, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT76
      I_(6)=I_ad !CopyBack
C FDA_80boats.fgi( 214, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT76:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 234, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT75
      I_(5)=I_ed !CopyBack
C FDA_80boats.fgi( 234, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT75:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 254, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT74
      I_(4)=I_id !CopyBack
C FDA_80boats.fgi( 254, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT74:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 274, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT73
      I_(3)=I_od !CopyBack
C FDA_80boats.fgi( 274, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT73:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 294, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT72
      I_(2)=I_ud !CopyBack
C FDA_80boats.fgi( 294, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT72:CR
      I_imop=0
      I_omop=0
      I_emop=0
      I_amop=0
      I_ulop=0
      I_okop=0
      I_ikop=0
      I_ekop=0
      I_akop=0
      I_ufop=0
      I_etep=0
      I_atep=0
      I_usep=0
      I_osep=0
      I_isep=0
      I_etip=0
      I_atip=0
      I_usip=0
      I_osip=0
      I_isip=0
      I_erep=0
      I_arep=0
      I_upep=0
      I_opep=0
      I_ipep=0
      I_erip=0
      I_arip=0
      I_upip=0
      I_opip=0
      I_ipip=0
      I_emep=0
      I_amep=0
      I_ulep=0
      I_olep=0
      I_ilep=0
      I_emip=0
      I_amip=0
      I_ulip=0
      I_olip=0
      I_ilip=0
      I_ekep=0
      I_akep=0
      I_ufep=0
      I_ofep=0
      I_ifep=0
      I_ekip=0
      I_akip=0
      I_ufip=0
      I_ofip=0
      I_ifip=0
      I_edep=0
      I_adep=0
      I_ubep=0
      I_obep=0
      I_ibep=0
      I_edip=0
      I_adip=0
      I_ubip=0
      I_obip=0
      I_ibip=0
      I_exap=0
      I_axap=0
      I_uvap=0
      I_ovap=0
      I_ivap=0
      I_exep=0
      I_axep=0
      I_uvep=0
      I_ovep=0
      I_ivep=0
      R_ipop=0
      R_opop=0
      R_epop=0
      R_apop=0
      R_umop=0
      R_olop=0
      R_ilop=0
      R_elop=0
      R_alop=0
      R_ukop=0
      R_evep=0
      R_avep=0
      R_utep=0
      R_otep=0
      R_itep=0
      R_evip=0
      R_avip=0
      R_utip=0
      R_otip=0
      R_itip=0
      R_esep=0
      R_asep=0
      R_urep=0
      R_orep=0
      R_irep=0
      R_esip=0
      R_asip=0
      R_urip=0
      R_orip=0
      R_irip=0
      R_epep=0
      R_apep=0
      R_umep=0
      R_omep=0
      R_imep=0
      R_epip=0
      R_apip=0
      R_umip=0
      R_omip=0
      R_imip=0
      R_elep=0
      R_alep=0
      R_ukep=0
      R_okep=0
      R_ikep=0
      R_elip=0
      R_alip=0
      R_ukip=0
      R_okip=0
      R_ikip=0
      R_efep=0
      R_afep=0
      R_udep=0
      R_odep=0
      R_idep=0
      R_efip=0
      R_afip=0
      R_udip=0
      R_odip=0
      R_idip=0
      R_ebep=0
      R_abep=0
      R_uxap=0
      R_oxap=0
      R_ixap=0
      R_ebip=0
      R_abip=0
      R_uxep=0
      R_oxep=0
      R_ixep=0
      I_asop=0
      R_esop=0
      I_usop=0
      R_atop=0
      I_orop=0
      R_urop=0
      I_isop=0
      R_osop=0
      I_erop=0
      R_irop=0
      I_efop=0
      R_ofop=0
      I_afop=0
      R_ifop=0
      I_upop=0
      R_arop=0
      I_ibop=0
      R_udop=0
      I_ebop=0
      R_odop=0
      I_abop=0
      R_idop=0
      I_uxip=0
      R_edop=0
      I_oxip=0
      R_adop=0
      I_ixip=0
      R_ubop=0
      I_exip=0
      R_obop=0
      I_uvip=0
      R_axip=0
      I_ivip=0
      R_ovip=0
      I_etop=0
      R_itop=0
C FDA_80boats.fgi( 314, 238):pre: ���������� ���������� ������� �� ������� FDA60,BOAT71
      I_(1)=I_af !CopyBack
C FDA_80boats.fgi( 314, 266):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT71:CR
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 134, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT80
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 154, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT79
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 174, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT78
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 194, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT77
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 214, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT76
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 234, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT75
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 254, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT74
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 274, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT73
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 294, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT72
      I_axuf=z'1000021'
      I_usuf=z'1000021'
      I_opuf=z'1000021'
      I_iluf=z'1000021'
      I_efuf=z'1000021'
      I_abuf=z'1000021'
      I_utof=z'1000021'
      I_orof=z'1000021'
      I_erof=z'1000021'
      I_uvuf=z'1000021'
      I_ovuf=z'1000021'
      I_ivuf=z'1000021'
      I_evuf=z'1000021'
      I_avuf=z'1000021'
      I_utuf=z'1000021'
      I_otuf=z'1000021'
      I_ituf=z'1000021'
      I_etuf=z'1000021'
      I_atuf=z'1000021'
      I_osuf=z'1000021'
      I_isuf=z'1000021'
      I_esuf=z'1000021'
      I_asuf=z'1000021'
      I_uruf=z'1000021'
      I_oruf=z'1000021'
      I_iruf=z'1000021'
      I_eruf=z'1000021'
      I_aruf=z'1000021'
      I_upuf=z'1000021'
      I_ipuf=z'1000021'
      I_epuf=z'1000021'
      I_apuf=z'1000021'
      I_umuf=z'1000021'
      I_omuf=z'1000021'
      I_imuf=z'1000021'
      I_emuf=z'1000021'
      I_amuf=z'1000021'
      I_uluf=z'1000021'
      I_oluf=z'1000021'
      I_eluf=z'1000021'
      I_aluf=z'1000021'
      I_ukuf=z'1000021'
      I_okuf=z'1000021'
      I_ikuf=z'1000021'
      I_ekuf=z'1000021'
      I_akuf=z'1000021'
      I_ufuf=z'1000021'
      I_ofuf=z'1000021'
      I_ifuf=z'1000021'
      I_afuf=z'1000021'
      I_uduf=z'1000021'
      I_oduf=z'1000021'
      I_iduf=z'1000021'
      I_eduf=z'1000021'
      I_aduf=z'1000021'
      I_ubuf=z'1000021'
      I_obuf=z'1000021'
      I_ibuf=z'1000021'
      I_ebuf=z'1000021'
      I_uxof=z'1000021'
      I_oxof=z'1000021'
      I_ixof=z'1000021'
      I_exof=z'1000021'
      I_axof=z'1000021'
      I_uvof=z'1000021'
      I_ovof=z'1000021'
      I_ivof=z'1000021'
      I_evof=z'1000021'
      I_avof=z'1000021'
      I_otof=z'1000021'
      I_itof=z'1000021'
      I_etof=z'1000021'
      I_atof=z'1000021'
      I_usof=z'1000021'
      I_osof=z'1000021'
      I_isof=z'1000021'
      I_esof=z'1000021'
      I_asof=z'1000021'
      I_urof=z'1000021'
      I_irof=z'1000021'
C FDA_80boats.fgi( 314, 210):pre: ���������� ���������� ������� �� ������� FDA50,BOAT71
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_if
     &,
     & I_urof,I_of,I_asof,I_uf,I_esof,
     & I_ak,I_isof,I_ek,I_osof,I_ik,
     & I_usof,I_ok,I_atof,I_uk,I_al,
     & I_el,I_ifuf,I_il,I_ofuf,I_ol,
     & I_ufuf,I_ul,I_akuf,I_am,I_ekuf,
     & I_em,I_ikuf,I_im,I_okuf,I_om,
     & I_ukuf,I_um,I_aluf,I_ap,I_eluf,
     & I_ep,I_oluf,I_ip,I_uluf,I_op,
     & I_amuf,I_up,I_emuf,I_ar,I_imuf,
     & I_er,I_omuf,I_ir,I_umuf,I_or,
     & I_apuf,I_ur,I_epuf,I_as,I_ipuf,
     & I_es,I_upuf,I_aruf,I_eruf,I_is,
     & I_iruf,I_os,I_iduf,I_us,I_oduf,
     & I_at,I_uduf,I_et,I_afuf,I_it,
     & I_etof,I_ot,I_itof,I_ut,I_otof,
     & I_av,I_avof,I_ev,I_evof,I_iv,
     & I_ivof,I_ov,I_ovof,I_uv,I_uvof,
     & I_ax,I_axof,I_ex,I_exof,I_ix,
     & I_ixof,I_ox,I_oxof,I_ux,I_uxof,
     & I_abe,I_ebuf,I_ebe,I_ibuf,I_ibe,
     & I_obuf,I_obe,I_ubuf,I_ube,I_aduf,
     & I_ade,I_eduf,I_ede,I_oruf,I_ide,
     & I_uruf,I_ode,I_asuf,I_ude,I_esuf,
     & I_afe,I_isuf,I_efe,I_osuf,I_ife,
     & I_atuf,I_ofe,I_etuf,I_ufe,I_ituf,
     & I_ake,I_otuf,I_eke,I_utuf,I_ike,
     & I_avuf,I_oke,I_evuf,I_uke,I_ivuf,
     & I_ale,I_ovuf,I_ele,I_uvuf,I_ile,
     & I_erof,I_ole,I_orof,I_ule,I_utof,
     & I_ame,I_abuf,I_eme,I_efuf,I_ime,
     & I_iluf,I_ome,I_opuf,I_ume,I_usuf,
     & I_ape,I_epe,I_ipe,I_ope,I_axuf,
     & INT(I_ef,4),I_upe,I_are,I_ere,I_ire,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(1))
C FDA_80boats.fgi( 314, 210):���������� ���������� ������� �� ������� FDA50,BOAT71
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_ure
     &,
     & I_urof,I_ase,I_asof,I_ese,I_esof,
     & I_ise,I_isof,I_ose,I_osof,I_use,
     & I_usof,I_ate,I_atof,I_ete,I_ite,
     & I_ote,I_ifuf,I_ute,I_ofuf,I_ave,
     & I_ufuf,I_eve,I_akuf,I_ive,I_ekuf,
     & I_ove,I_ikuf,I_uve,I_okuf,I_axe,
     & I_ukuf,I_exe,I_aluf,I_ixe,I_eluf,
     & I_oxe,I_oluf,I_uxe,I_uluf,I_abi,
     & I_amuf,I_ebi,I_emuf,I_ibi,I_imuf,
     & I_obi,I_omuf,I_ubi,I_umuf,I_adi,
     & I_apuf,I_edi,I_epuf,I_idi,I_ipuf,
     & I_odi,I_upuf,I_aruf,I_eruf,I_udi,
     & I_iruf,I_afi,I_iduf,I_efi,I_oduf,
     & I_ifi,I_uduf,I_ofi,I_afuf,I_ufi,
     & I_etof,I_aki,I_itof,I_eki,I_otof,
     & I_iki,I_avof,I_oki,I_evof,I_uki,
     & I_ivof,I_ali,I_ovof,I_eli,I_uvof,
     & I_ili,I_axof,I_oli,I_exof,I_uli,
     & I_ixof,I_ami,I_oxof,I_emi,I_uxof,
     & I_imi,I_ebuf,I_omi,I_ibuf,I_umi,
     & I_obuf,I_api,I_ubuf,I_epi,I_aduf,
     & I_ipi,I_eduf,I_opi,I_oruf,I_upi,
     & I_uruf,I_ari,I_asuf,I_eri,I_esuf,
     & I_iri,I_isuf,I_ori,I_osuf,I_uri,
     & I_atuf,I_asi,I_etuf,I_esi,I_ituf,
     & I_isi,I_otuf,I_osi,I_utuf,I_usi,
     & I_avuf,I_ati,I_evuf,I_eti,I_ivuf,
     & I_iti,I_ovuf,I_oti,I_uvuf,I_uti,
     & I_erof,I_avi,I_orof,I_evi,I_utof,
     & I_ivi,I_abuf,I_ovi,I_efuf,I_uvi,
     & I_iluf,I_axi,I_opuf,I_exi,I_usuf,
     & I_ixi,I_oxi,I_uxi,I_abo,I_axuf,
     & INT(I_ore,4),I_ebo,I_ibo,I_obo,I_ubo,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(2))
C FDA_80boats.fgi( 294, 210):���������� ���������� ������� �� ������� FDA50,BOAT72
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_edo
     &,
     & I_urof,I_ido,I_asof,I_odo,I_esof,
     & I_udo,I_isof,I_afo,I_osof,I_efo,
     & I_usof,I_ifo,I_atof,I_ofo,I_ufo,
     & I_ako,I_ifuf,I_eko,I_ofuf,I_iko,
     & I_ufuf,I_oko,I_akuf,I_uko,I_ekuf,
     & I_alo,I_ikuf,I_elo,I_okuf,I_ilo,
     & I_ukuf,I_olo,I_aluf,I_ulo,I_eluf,
     & I_amo,I_oluf,I_emo,I_uluf,I_imo,
     & I_amuf,I_omo,I_emuf,I_umo,I_imuf,
     & I_apo,I_omuf,I_epo,I_umuf,I_ipo,
     & I_apuf,I_opo,I_epuf,I_upo,I_ipuf,
     & I_aro,I_upuf,I_aruf,I_eruf,I_ero,
     & I_iruf,I_iro,I_iduf,I_oro,I_oduf,
     & I_uro,I_uduf,I_aso,I_afuf,I_eso,
     & I_etof,I_iso,I_itof,I_oso,I_otof,
     & I_uso,I_avof,I_ato,I_evof,I_eto,
     & I_ivof,I_ito,I_ovof,I_oto,I_uvof,
     & I_uto,I_axof,I_avo,I_exof,I_evo,
     & I_ixof,I_ivo,I_oxof,I_ovo,I_uxof,
     & I_uvo,I_ebuf,I_axo,I_ibuf,I_exo,
     & I_obuf,I_ixo,I_ubuf,I_oxo,I_aduf,
     & I_uxo,I_eduf,I_abu,I_oruf,I_ebu,
     & I_uruf,I_ibu,I_asuf,I_obu,I_esuf,
     & I_ubu,I_isuf,I_adu,I_osuf,I_edu,
     & I_atuf,I_idu,I_etuf,I_odu,I_ituf,
     & I_udu,I_otuf,I_afu,I_utuf,I_efu,
     & I_avuf,I_ifu,I_evuf,I_ofu,I_ivuf,
     & I_ufu,I_ovuf,I_aku,I_uvuf,I_eku,
     & I_erof,I_iku,I_orof,I_oku,I_utof,
     & I_uku,I_abuf,I_alu,I_efuf,I_elu,
     & I_iluf,I_ilu,I_opuf,I_olu,I_usuf,
     & I_ulu,I_amu,I_emu,I_imu,I_axuf,
     & INT(I_ado,4),I_omu,I_umu,I_apu,I_epu,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(3))
C FDA_80boats.fgi( 274, 210):���������� ���������� ������� �� ������� FDA50,BOAT73
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_opu
     &,
     & I_urof,I_upu,I_asof,I_aru,I_esof,
     & I_eru,I_isof,I_iru,I_osof,I_oru,
     & I_usof,I_uru,I_atof,I_asu,I_esu,
     & I_isu,I_ifuf,I_osu,I_ofuf,I_usu,
     & I_ufuf,I_atu,I_akuf,I_etu,I_ekuf,
     & I_itu,I_ikuf,I_otu,I_okuf,I_utu,
     & I_ukuf,I_avu,I_aluf,I_evu,I_eluf,
     & I_ivu,I_oluf,I_ovu,I_uluf,I_uvu,
     & I_amuf,I_axu,I_emuf,I_exu,I_imuf,
     & I_ixu,I_omuf,I_oxu,I_umuf,I_uxu,
     & I_apuf,I_abad,I_epuf,I_ebad,I_ipuf,
     & I_ibad,I_upuf,I_aruf,I_eruf,I_obad,
     & I_iruf,I_ubad,I_iduf,I_adad,I_oduf,
     & I_edad,I_uduf,I_idad,I_afuf,I_odad,
     & I_etof,I_udad,I_itof,I_afad,I_otof,
     & I_efad,I_avof,I_ifad,I_evof,I_ofad,
     & I_ivof,I_ufad,I_ovof,I_akad,I_uvof,
     & I_ekad,I_axof,I_ikad,I_exof,I_okad,
     & I_ixof,I_ukad,I_oxof,I_alad,I_uxof,
     & I_elad,I_ebuf,I_ilad,I_ibuf,I_olad,
     & I_obuf,I_ulad,I_ubuf,I_amad,I_aduf,
     & I_emad,I_eduf,I_imad,I_oruf,I_omad,
     & I_uruf,I_umad,I_asuf,I_apad,I_esuf,
     & I_epad,I_isuf,I_ipad,I_osuf,I_opad,
     & I_atuf,I_upad,I_etuf,I_arad,I_ituf,
     & I_erad,I_otuf,I_irad,I_utuf,I_orad,
     & I_avuf,I_urad,I_evuf,I_asad,I_ivuf,
     & I_esad,I_ovuf,I_isad,I_uvuf,I_osad,
     & I_erof,I_usad,I_orof,I_atad,I_utof,
     & I_etad,I_abuf,I_itad,I_efuf,I_otad,
     & I_iluf,I_utad,I_opuf,I_avad,I_usuf,
     & I_evad,I_ivad,I_ovad,I_uvad,I_axuf,
     & INT(I_ipu,4),I_axad,I_exad,I_ixad,I_oxad,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(4))
C FDA_80boats.fgi( 254, 210):���������� ���������� ������� �� ������� FDA50,BOAT74
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_abed
     &,
     & I_urof,I_ebed,I_asof,I_ibed,I_esof,
     & I_obed,I_isof,I_ubed,I_osof,I_aded,
     & I_usof,I_eded,I_atof,I_ided,I_oded,
     & I_uded,I_ifuf,I_afed,I_ofuf,I_efed,
     & I_ufuf,I_ifed,I_akuf,I_ofed,I_ekuf,
     & I_ufed,I_ikuf,I_aked,I_okuf,I_eked,
     & I_ukuf,I_iked,I_aluf,I_oked,I_eluf,
     & I_uked,I_oluf,I_aled,I_uluf,I_eled,
     & I_amuf,I_iled,I_emuf,I_oled,I_imuf,
     & I_uled,I_omuf,I_amed,I_umuf,I_emed,
     & I_apuf,I_imed,I_epuf,I_omed,I_ipuf,
     & I_umed,I_upuf,I_aruf,I_eruf,I_aped,
     & I_iruf,I_eped,I_iduf,I_iped,I_oduf,
     & I_oped,I_uduf,I_uped,I_afuf,I_ared,
     & I_etof,I_ered,I_itof,I_ired,I_otof,
     & I_ored,I_avof,I_ured,I_evof,I_ased,
     & I_ivof,I_esed,I_ovof,I_ised,I_uvof,
     & I_osed,I_axof,I_used,I_exof,I_ated,
     & I_ixof,I_eted,I_oxof,I_ited,I_uxof,
     & I_oted,I_ebuf,I_uted,I_ibuf,I_aved,
     & I_obuf,I_eved,I_ubuf,I_ived,I_aduf,
     & I_oved,I_eduf,I_uved,I_oruf,I_axed,
     & I_uruf,I_exed,I_asuf,I_ixed,I_esuf,
     & I_oxed,I_isuf,I_uxed,I_osuf,I_abid,
     & I_atuf,I_ebid,I_etuf,I_ibid,I_ituf,
     & I_obid,I_otuf,I_ubid,I_utuf,I_adid,
     & I_avuf,I_edid,I_evuf,I_idid,I_ivuf,
     & I_odid,I_ovuf,I_udid,I_uvuf,I_afid,
     & I_erof,I_efid,I_orof,I_ifid,I_utof,
     & I_ofid,I_abuf,I_ufid,I_efuf,I_akid,
     & I_iluf,I_ekid,I_opuf,I_ikid,I_usuf,
     & I_okid,I_ukid,I_alid,I_elid,I_axuf,
     & INT(I_uxad,4),I_ilid,I_olid,I_ulid,I_amid,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(5))
C FDA_80boats.fgi( 234, 210):���������� ���������� ������� �� ������� FDA50,BOAT75
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_imid
     &,
     & I_urof,I_omid,I_asof,I_umid,I_esof,
     & I_apid,I_isof,I_epid,I_osof,I_ipid,
     & I_usof,I_opid,I_atof,I_upid,I_arid,
     & I_erid,I_ifuf,I_irid,I_ofuf,I_orid,
     & I_ufuf,I_urid,I_akuf,I_asid,I_ekuf,
     & I_esid,I_ikuf,I_isid,I_okuf,I_osid,
     & I_ukuf,I_usid,I_aluf,I_atid,I_eluf,
     & I_etid,I_oluf,I_itid,I_uluf,I_otid,
     & I_amuf,I_utid,I_emuf,I_avid,I_imuf,
     & I_evid,I_omuf,I_ivid,I_umuf,I_ovid,
     & I_apuf,I_uvid,I_epuf,I_axid,I_ipuf,
     & I_exid,I_upuf,I_aruf,I_eruf,I_ixid,
     & I_iruf,I_oxid,I_iduf,I_uxid,I_oduf,
     & I_abod,I_uduf,I_ebod,I_afuf,I_ibod,
     & I_etof,I_obod,I_itof,I_ubod,I_otof,
     & I_adod,I_avof,I_edod,I_evof,I_idod,
     & I_ivof,I_odod,I_ovof,I_udod,I_uvof,
     & I_afod,I_axof,I_efod,I_exof,I_ifod,
     & I_ixof,I_ofod,I_oxof,I_ufod,I_uxof,
     & I_akod,I_ebuf,I_ekod,I_ibuf,I_ikod,
     & I_obuf,I_okod,I_ubuf,I_ukod,I_aduf,
     & I_alod,I_eduf,I_elod,I_oruf,I_ilod,
     & I_uruf,I_olod,I_asuf,I_ulod,I_esuf,
     & I_amod,I_isuf,I_emod,I_osuf,I_imod,
     & I_atuf,I_omod,I_etuf,I_umod,I_ituf,
     & I_apod,I_otuf,I_epod,I_utuf,I_ipod,
     & I_avuf,I_opod,I_evuf,I_upod,I_ivuf,
     & I_arod,I_ovuf,I_erod,I_uvuf,I_irod,
     & I_erof,I_orod,I_orof,I_urod,I_utof,
     & I_asod,I_abuf,I_esod,I_efuf,I_isod,
     & I_iluf,I_osod,I_opuf,I_usod,I_usuf,
     & I_atod,I_etod,I_itod,I_otod,I_axuf,
     & INT(I_emid,4),I_utod,I_avod,I_evod,I_ivod,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(6))
C FDA_80boats.fgi( 214, 210):���������� ���������� ������� �� ������� FDA50,BOAT76
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_uvod
     &,
     & I_urof,I_axod,I_asof,I_exod,I_esof,
     & I_ixod,I_isof,I_oxod,I_osof,I_uxod,
     & I_usof,I_abud,I_atof,I_ebud,I_ibud,
     & I_obud,I_ifuf,I_ubud,I_ofuf,I_adud,
     & I_ufuf,I_edud,I_akuf,I_idud,I_ekuf,
     & I_odud,I_ikuf,I_udud,I_okuf,I_afud,
     & I_ukuf,I_efud,I_aluf,I_ifud,I_eluf,
     & I_ofud,I_oluf,I_ufud,I_uluf,I_akud,
     & I_amuf,I_ekud,I_emuf,I_ikud,I_imuf,
     & I_okud,I_omuf,I_ukud,I_umuf,I_alud,
     & I_apuf,I_elud,I_epuf,I_ilud,I_ipuf,
     & I_olud,I_upuf,I_aruf,I_eruf,I_ulud,
     & I_iruf,I_amud,I_iduf,I_emud,I_oduf,
     & I_imud,I_uduf,I_omud,I_afuf,I_umud,
     & I_etof,I_apud,I_itof,I_epud,I_otof,
     & I_ipud,I_avof,I_opud,I_evof,I_upud,
     & I_ivof,I_arud,I_ovof,I_erud,I_uvof,
     & I_irud,I_axof,I_orud,I_exof,I_urud,
     & I_ixof,I_asud,I_oxof,I_esud,I_uxof,
     & I_isud,I_ebuf,I_osud,I_ibuf,I_usud,
     & I_obuf,I_atud,I_ubuf,I_etud,I_aduf,
     & I_itud,I_eduf,I_otud,I_oruf,I_utud,
     & I_uruf,I_avud,I_asuf,I_evud,I_esuf,
     & I_ivud,I_isuf,I_ovud,I_osuf,I_uvud,
     & I_atuf,I_axud,I_etuf,I_exud,I_ituf,
     & I_ixud,I_otuf,I_oxud,I_utuf,I_uxud,
     & I_avuf,I_abaf,I_evuf,I_ebaf,I_ivuf,
     & I_ibaf,I_ovuf,I_obaf,I_uvuf,I_ubaf,
     & I_erof,I_adaf,I_orof,I_edaf,I_utof,
     & I_idaf,I_abuf,I_odaf,I_efuf,I_udaf,
     & I_iluf,I_afaf,I_opuf,I_efaf,I_usuf,
     & I_ifaf,I_ofaf,I_ufaf,I_akaf,I_axuf,
     & INT(I_ovod,4),I_ekaf,I_ikaf,I_okaf,I_ukaf,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(7))
C FDA_80boats.fgi( 194, 210):���������� ���������� ������� �� ������� FDA50,BOAT77
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_elaf
     &,
     & I_urof,I_ilaf,I_asof,I_olaf,I_esof,
     & I_ulaf,I_isof,I_amaf,I_osof,I_emaf,
     & I_usof,I_imaf,I_atof,I_omaf,I_umaf,
     & I_apaf,I_ifuf,I_epaf,I_ofuf,I_ipaf,
     & I_ufuf,I_opaf,I_akuf,I_upaf,I_ekuf,
     & I_araf,I_ikuf,I_eraf,I_okuf,I_iraf,
     & I_ukuf,I_oraf,I_aluf,I_uraf,I_eluf,
     & I_asaf,I_oluf,I_esaf,I_uluf,I_isaf,
     & I_amuf,I_osaf,I_emuf,I_usaf,I_imuf,
     & I_ataf,I_omuf,I_etaf,I_umuf,I_itaf,
     & I_apuf,I_otaf,I_epuf,I_utaf,I_ipuf,
     & I_avaf,I_upuf,I_aruf,I_eruf,I_evaf,
     & I_iruf,I_ivaf,I_iduf,I_ovaf,I_oduf,
     & I_uvaf,I_uduf,I_axaf,I_afuf,I_exaf,
     & I_etof,I_ixaf,I_itof,I_oxaf,I_otof,
     & I_uxaf,I_avof,I_abef,I_evof,I_ebef,
     & I_ivof,I_ibef,I_ovof,I_obef,I_uvof,
     & I_ubef,I_axof,I_adef,I_exof,I_edef,
     & I_ixof,I_idef,I_oxof,I_odef,I_uxof,
     & I_udef,I_ebuf,I_afef,I_ibuf,I_efef,
     & I_obuf,I_ifef,I_ubuf,I_ofef,I_aduf,
     & I_ufef,I_eduf,I_akef,I_oruf,I_ekef,
     & I_uruf,I_ikef,I_asuf,I_okef,I_esuf,
     & I_ukef,I_isuf,I_alef,I_osuf,I_elef,
     & I_atuf,I_ilef,I_etuf,I_olef,I_ituf,
     & I_ulef,I_otuf,I_amef,I_utuf,I_emef,
     & I_avuf,I_imef,I_evuf,I_omef,I_ivuf,
     & I_umef,I_ovuf,I_apef,I_uvuf,I_epef,
     & I_erof,I_ipef,I_orof,I_opef,I_utof,
     & I_upef,I_abuf,I_aref,I_efuf,I_eref,
     & I_iluf,I_iref,I_opuf,I_oref,I_usuf,
     & I_uref,I_asef,I_esef,I_isef,I_axuf,
     & INT(I_alaf,4),I_osef,I_usef,I_atef,I_etef,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(8))
C FDA_80boats.fgi( 174, 210):���������� ���������� ������� �� ������� FDA50,BOAT78
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_otef
     &,
     & I_urof,I_utef,I_asof,I_avef,I_esof,
     & I_evef,I_isof,I_ivef,I_osof,I_ovef,
     & I_usof,I_uvef,I_atof,I_axef,I_exef,
     & I_ixef,I_ifuf,I_oxef,I_ofuf,I_uxef,
     & I_ufuf,I_abif,I_akuf,I_ebif,I_ekuf,
     & I_ibif,I_ikuf,I_obif,I_okuf,I_ubif,
     & I_ukuf,I_adif,I_aluf,I_edif,I_eluf,
     & I_idif,I_oluf,I_odif,I_uluf,I_udif,
     & I_amuf,I_afif,I_emuf,I_efif,I_imuf,
     & I_ifif,I_omuf,I_ofif,I_umuf,I_ufif,
     & I_apuf,I_akif,I_epuf,I_ekif,I_ipuf,
     & I_ikif,I_upuf,I_aruf,I_eruf,I_okif,
     & I_iruf,I_ukif,I_iduf,I_alif,I_oduf,
     & I_elif,I_uduf,I_ilif,I_afuf,I_olif,
     & I_etof,I_ulif,I_itof,I_amif,I_otof,
     & I_emif,I_avof,I_imif,I_evof,I_omif,
     & I_ivof,I_umif,I_ovof,I_apif,I_uvof,
     & I_epif,I_axof,I_ipif,I_exof,I_opif,
     & I_ixof,I_upif,I_oxof,I_arif,I_uxof,
     & I_erif,I_ebuf,I_irif,I_ibuf,I_orif,
     & I_obuf,I_urif,I_ubuf,I_asif,I_aduf,
     & I_esif,I_eduf,I_isif,I_oruf,I_osif,
     & I_uruf,I_usif,I_asuf,I_atif,I_esuf,
     & I_etif,I_isuf,I_itif,I_osuf,I_otif,
     & I_atuf,I_utif,I_etuf,I_avif,I_ituf,
     & I_evif,I_otuf,I_ivif,I_utuf,I_ovif,
     & I_avuf,I_uvif,I_evuf,I_axif,I_ivuf,
     & I_exif,I_ovuf,I_ixif,I_uvuf,I_oxif,
     & I_erof,I_uxif,I_orof,I_abof,I_utof,
     & I_ebof,I_abuf,I_ibof,I_efuf,I_obof,
     & I_iluf,I_ubof,I_opuf,I_adof,I_usuf,
     & I_edof,I_idof,I_odof,I_udof,I_axuf,
     & INT(I_itef,4),I_afof,I_efof,I_ifof,I_ofof,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(9))
C FDA_80boats.fgi( 154, 210):���������� ���������� ������� �� ������� FDA50,BOAT79
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_irof,I_exuf
     &,
     & I_urof,I_ixuf,I_asof,I_oxuf,I_esof,
     & I_uxuf,I_isof,I_abak,I_osof,I_ebak,
     & I_usof,I_ibak,I_atof,I_obak,I_ubak,
     & I_adak,I_ifuf,I_edak,I_ofuf,I_idak,
     & I_ufuf,I_odak,I_akuf,I_udak,I_ekuf,
     & I_afak,I_ikuf,I_efak,I_okuf,I_ifak,
     & I_ukuf,I_ofak,I_aluf,I_ufak,I_eluf,
     & I_akak,I_oluf,I_ekak,I_uluf,I_ikak,
     & I_amuf,I_okak,I_emuf,I_ukak,I_imuf,
     & I_alak,I_omuf,I_elak,I_umuf,I_ilak,
     & I_apuf,I_olak,I_epuf,I_ulak,I_ipuf,
     & I_amak,I_upuf,I_aruf,I_eruf,I_emak,
     & I_iruf,I_imak,I_iduf,I_omak,I_oduf,
     & I_umak,I_uduf,I_apak,I_afuf,I_epak,
     & I_etof,I_ipak,I_itof,I_opak,I_otof,
     & I_upak,I_avof,I_arak,I_evof,I_erak,
     & I_ivof,I_irak,I_ovof,I_orak,I_uvof,
     & I_urak,I_axof,I_asak,I_exof,I_esak,
     & I_ixof,I_isak,I_oxof,I_osak,I_uxof,
     & I_usak,I_ebuf,I_atak,I_ibuf,I_etak,
     & I_obuf,I_itak,I_ubuf,I_otak,I_aduf,
     & I_utak,I_eduf,I_avak,I_oruf,I_evak,
     & I_uruf,I_ivak,I_asuf,I_ovak,I_esuf,
     & I_uvak,I_isuf,I_axak,I_osuf,I_exak,
     & I_atuf,I_ixak,I_etuf,I_oxak,I_ituf,
     & I_uxak,I_otuf,I_abek,I_utuf,I_ebek,
     & I_avuf,I_ibek,I_evuf,I_obek,I_ivuf,
     & I_ubek,I_ovuf,I_adek,I_uvuf,I_edek,
     & I_erof,I_idek,I_orof,I_odek,I_utof,
     & I_udek,I_abuf,I_afek,I_efuf,I_efek,
     & I_iluf,I_ifek,I_opuf,I_ofek,I_usuf,
     & I_ufek,I_akek,I_ekek,I_ikek,I_axuf,
     & INT(I_ufof,4),I_okek,I_ukek,I_alek,I_elek,
     & L_upof,L_olof,L_epof,
     & L_arof,L_ulof,L_amof,
     & L_ilof,L_akof,L_ekof,
     & L_ikof,L_okof,L_ukof,
     & L_alof,L_elof,L_emof,
     & L_imof,L_omof,L_umof,
     & L_apof,L_ipof,L_apek,
     & L_opof,I_(10))
C FDA_80boats.fgi( 134, 210):���������� ���������� ������� �� ������� FDA50,BOAT80
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(1))
C FDA_80boats.fgi( 314, 222):���������� ���������� ������� �� ������� FDA90,BOAT71
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(2))
C FDA_80boats.fgi( 294, 222):���������� ���������� ������� �� ������� FDA90,BOAT72
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(3))
C FDA_80boats.fgi( 274, 222):���������� ���������� ������� �� ������� FDA90,BOAT73
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(4))
C FDA_80boats.fgi( 254, 222):���������� ���������� ������� �� ������� FDA90,BOAT74
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(5))
C FDA_80boats.fgi( 234, 222):���������� ���������� ������� �� ������� FDA90,BOAT75
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(6))
C FDA_80boats.fgi( 214, 222):���������� ���������� ������� �� ������� FDA90,BOAT76
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(7))
C FDA_80boats.fgi( 194, 222):���������� ���������� ������� �� ������� FDA90,BOAT77
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(8))
C FDA_80boats.fgi( 174, 222):���������� ���������� ������� �� ������� FDA90,BOAT78
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(9))
C FDA_80boats.fgi( 154, 222):���������� ���������� ������� �� ������� FDA90,BOAT79
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_ilek,
     & L_amek,L_imek,L_umek,
     & L_apek,L_emek,L_epek,
     & L_omek,L_olek,L_opabe,L_ulek,
     & L_osabe,L_otabe,L_orabe,I_(10))
C FDA_80boats.fgi( 134, 222):���������� ���������� ������� �� ������� FDA90,BOAT80
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(1))
C FDA_80boats.fgi( 314, 252):���������� ������� � ���� ���,BOAT71
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(2))
C FDA_80boats.fgi( 294, 252):���������� ������� � ���� ���,BOAT72
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(3))
C FDA_80boats.fgi( 274, 252):���������� ������� � ���� ���,BOAT73
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(4))
C FDA_80boats.fgi( 254, 252):���������� ������� � ���� ���,BOAT74
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(5))
C FDA_80boats.fgi( 234, 252):���������� ������� � ���� ���,BOAT75
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(6))
C FDA_80boats.fgi( 214, 252):���������� ������� � ���� ���,BOAT76
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(7))
C FDA_80boats.fgi( 194, 252):���������� ������� � ���� ���,BOAT77
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(8))
C FDA_80boats.fgi( 174, 252):���������� ������� � ���� ���,BOAT78
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(9))
C FDA_80boats.fgi( 154, 252):���������� ������� � ���� ���,BOAT79
      Call LODOCHKA_HANDLER_KTS(deltat,L_atar,L_etar,
     & L_itar,L_otar,L_utar,
     & L_avar,L_evar,L_ivar,L_ovar,
     & L_uvar,L_aper,L_eper,
     & L_iper,L_oper,L_uper,
     & L_arer,L_erer,L_irer,L_orer,
     & L_urer,L_akir,L_ekir,
     & L_ikir,L_okir,L_ukir,
     & L_alir,L_elir,L_ilir,L_olir,
     & L_ulir,L_abor,L_ebor,
     & L_ibor,L_obor,L_ubor,
     & L_ador,L_edor,L_idor,L_odor,
     & L_udor,L_ator,L_etor,
     & L_itor,L_otor,L_utor,
     & L_avor,L_evor,L_ivor,L_ovor,
     & L_uvor,L_apar,L_epar,
     & L_ipar,L_opar,L_upar,
     & L_arar,L_erar,L_irar,
     & L_orar,L_urar,L_asar,
     & L_esar,L_isar,L_osar,
     & L_usar,L_aker,L_eker,
     & L_iker,L_oker,L_uker,
     & L_aler,L_eler,L_iler,
     & L_oler,L_uler,L_amer,
     & L_emer,L_imer,L_omer,
     & L_umer,L_abir,L_ebir,
     & L_ibir,L_obir,L_ubir,
     & L_adir,L_edir,L_idir,
     & L_odir,L_udir,L_afir,
     & L_efir,L_ifir,L_ofir,
     & L_ufir,L_apor,L_epor,
     & L_ipor,L_opor,L_upor,
     & L_aror,L_eror,L_iror,
     & L_oror,L_uror,L_asor,
     & L_esor,L_isor,L_osor,
     & L_usor,L_atir,L_etir,
     & L_itir,L_otir,L_utir,
     & L_avir,L_evir,L_ivir,
     & L_ovir,L_uvir,L_axir,
     & L_exir,L_ixir,L_oxir,
     & L_uxir,L_apur,L_epur,
     & L_ipur,L_opur,L_upur,
     & L_arur,L_erur,L_irur,L_orur,
     & L_urur,L_akur,L_ekur,
     & L_ikur,L_okur,L_ukur,
     & L_alur,L_elur,L_ilur,
     & L_olur,L_ulur,L_amur,
     & L_emur,L_imur,L_omur,
     & L_umur,L_akar,L_ekar,
     & L_ikar,L_ukar,L_alar,L_amar,
     & L_emar,L_imar,L_omar,L_umar,
     & L_afar,L_efar,L_ifar,L_ofar,
     & L_ufar,L_okar,L_elar,L_ilar,
     & L_olar,L_ular,L_aber,L_eber,
     & L_iber,L_uber,L_ader,L_afer,
     & L_efer,L_ifer,L_ofer,L_ufer,
     & L_axar,L_exar,L_ixar,L_oxar,
     & L_uxar,L_ober,L_eder,L_ider,
     & L_oder,L_uder,L_ater,L_eter,
     & L_iter,L_uter,L_aver,L_axer,
     & L_exer,L_ixer,L_oxer,L_uxer,
     & L_aser,L_eser,L_iser,L_oser,
     & L_user,L_oter,L_ever,L_iver,
     & L_over,L_uver,L_apir,L_epir,
     & L_ipir,L_upir,L_arir,L_asir,
     & L_esir,L_isir,L_osir,L_usir,
     & L_amir,L_emir,L_imir,L_omir,
     & L_umir,L_opir,L_erir,L_irir,
     & L_orir,L_urir,L_akor,L_ekor,
     & L_ikor,L_ukor,L_alor,L_amor,
     & L_emor,L_imor,L_omor,L_umor,
     & L_afor,L_efor,L_ifor,L_ofor,
     & L_ufor,L_okor,L_elor,L_ilor,
     & L_olor,L_ulor,L_abur,L_ebur,
     & L_ibur,L_ubur,L_adur,L_afur,
     & L_efur,L_ifur,L_ofur,L_ufur,
     & L_axor,L_exor,L_ixor,L_oxor,
     & L_uxor,L_obur,L_edur,L_idur,
     & L_odur,L_udur,I_(10))
C FDA_80boats.fgi( 134, 252):���������� ������� � ���� ���,BOAT80
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_utas,R_isas,L_udes,L_emebe,
     & L_omabe,L_imabe,R_avas,R_osas,
     & L_afes,L_emabe,R_evas,R_usas,
     & L_efes,I_ifes,I_ofes,I_ufes,I_akes,I_ekes,
     & INT(I_akebe,4),L_ekebe,L_emas,
     & L_imas,L_omas,L_umas,
     & L_apas,L_epas,L_ipas,
     & L_opas,L_upas,L_aras,I_(1),I_ufas,
     & R_ikes,REAL(R_afebe,4),R_okes,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ukes,R_ales,
     & R_eles,REAL(R_ikebe,4),R_iles,
     & REAL(R_okebe,4),R_etas,R_uras,L_edes,L_atas,
     & L_imebe,REAL(R_omebe,4),R_ofas,R_ules,
     & R_ames,R_emes,REAL(R_uvabe,4),R_imes,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_omes,
     & REAL(R_avabe,4),R_umes,R_itas,R_asas,
     & L_ides,R_apes,R_epes,R_ipes,L_elas,
     & R_opes,R_upes,REAL(R_erabe,4),R_ares,R_otas,
     & R_esas,L_odes,R_eres,R_ires,R_ores,R_ures,
     & R_ases,REAL(R_esabe,4),R_eses,L_ilas,
     & L_ivas,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_ises,R_oses,R_uses,REAL(R_etabe,4),R_ates,
     & L_olas,L_ovas,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_etes,R_ites,
     & R_otes,R_utes,REAL(R_epabe,4),R_aves,
     & L_alas,L_arabe,L_orabe,L_uvas,
     & L_axas,L_apabe,R_usur,R_uxas,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_abes,I_ekas,R_ebas,R_ebes,R_eves,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_oles,R_ixur,C20_obas
     &)
C FDA_80boats.fgi( 314, 266):���������� �������,BOAT71
      I_af=I_(1)
C FDA_80boats.fgi( 314, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT71:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_obok,R_evap
     &,
     & I_avap,R_itop,I_ubok,I_etop,I_adok,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_usek,
     & R_ovip,I_ivip,I_atek,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_etek,R_ifop,
     & I_afop,I_itek,R_ofop,
     & I_efop,I_otek,R_irop,I_erop,
     & I_utek,R_osop,I_isop,I_avek,
     & R_urop,I_orop,I_evek,
     & R_atop,I_usop,I_ivek,
     & R_esop,I_asop,I_ovek,I_uvek,
     & I_axek,I_exek,I_ixek,I_oxek,I_uxek,I_abik,
     & I_ebik,I_ibik,I_obik,I_ubik,I_adik,I_edik,
     & I_idik,I_odik,I_udik,I_afik,I_efik,I_ifik,
     & I_ofik,I_ufik,I_akik,I_ekik,I_ikik,I_okik,
     & I_ukik,I_alik,I_elik,I_ilik,I_olik,I_ulik,
     & I_amik,I_emik,I_imik,I_omik,I_umik,I_apik,
     & I_epik,I_ipik,I_opik,I_upik,I_arik,I_erik,
     & I_irik,I_orik,I_urik,I_asik,I_esik,I_isik,
     & I_osik,I_usik,I_atik,I_etik,I_itik,I_otik,
     & I_utik,I_avik,I_evik,I_ivik,I_ovik,I_uvik,
     & I_axik,I_exik,I_ixik,I_oxik,I_uxik,I_abok,
     & I_ebok,I_ibok,INT(I_af,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_ofas,4),
     & R_evep,L_asek,L_urek,L_orek,L_esek,L_isek,L_osek,
     & L_upek,L_opek,L_ipek,L_arek,L_erek,L_irek,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,71,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 314, 238):���������� ���������� ������� �� ������� FDA60,BOAT71
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_obos,R_exis,L_olos,L_emebe,
     & L_omabe,L_imabe,R_ubos,R_ixis,
     & L_ulos,L_emabe,R_ados,R_oxis,
     & L_amos,I_emos,I_imos,I_omos,I_umos,I_apos,
     & INT(I_akebe,4),L_ekebe,L_asis,
     & L_esis,L_isis,L_osis,
     & L_usis,L_atis,L_etis,
     & L_itis,L_otis,L_utis,I_(2),I_omis,
     & R_epos,REAL(R_afebe,4),R_ipos,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_opos,R_upos,
     & R_aros,REAL(R_ikebe,4),R_eros,
     & REAL(R_okebe,4),R_abos,R_ovis,L_alos,L_uxis,
     & L_imebe,REAL(R_omebe,4),R_imis,R_oros,
     & R_uros,R_asos,REAL(R_uvabe,4),R_esos,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_isos,
     & REAL(R_avabe,4),R_osos,R_ebos,R_uvis,
     & L_elos,R_usos,R_atos,R_etos,L_aris,
     & R_itos,R_otos,REAL(R_erabe,4),R_utos,R_ibos,
     & R_axis,L_ilos,R_avos,R_evos,R_ivos,R_ovos,
     & R_uvos,REAL(R_esabe,4),R_axos,L_eris,
     & L_edos,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_exos,R_ixos,R_oxos,REAL(R_etabe,4),R_uxos,
     & L_iris,L_idos,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_abus,R_ebus,
     & R_ibus,R_obus,REAL(R_epabe,4),R_ubus,
     & L_upis,L_arabe,L_orabe,L_odos,
     & L_udos,L_apabe,R_oxes,R_ofos,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_ufos,I_apis,R_akis,R_akos,R_adus,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_iros,R_efis,C20_ikis
     &)
C FDA_80boats.fgi( 294, 266):���������� �������,BOAT72
      I_ud=I_(2)
C FDA_80boats.fgi( 294, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT72:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_iruk,R_evap
     &,
     & I_avap,R_itop,I_oruk,I_etop,I_uruk,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_okok,
     & R_ovip,I_ivip,I_ukok,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_alok,R_ifop,
     & I_afop,I_elok,R_ofop,
     & I_efop,I_ilok,R_irop,I_erop,
     & I_olok,R_osop,I_isop,I_ulok,
     & R_urop,I_orop,I_amok,
     & R_atop,I_usop,I_emok,
     & R_esop,I_asop,I_imok,I_omok,
     & I_umok,I_apok,I_epok,I_ipok,I_opok,I_upok,
     & I_arok,I_erok,I_irok,I_orok,I_urok,I_asok,
     & I_esok,I_isok,I_osok,I_usok,I_atok,I_etok,
     & I_itok,I_otok,I_utok,I_avok,I_evok,I_ivok,
     & I_ovok,I_uvok,I_axok,I_exok,I_ixok,I_oxok,
     & I_uxok,I_abuk,I_ebuk,I_ibuk,I_obuk,I_ubuk,
     & I_aduk,I_eduk,I_iduk,I_oduk,I_uduk,I_afuk,
     & I_efuk,I_ifuk,I_ofuk,I_ufuk,I_akuk,I_ekuk,
     & I_ikuk,I_okuk,I_ukuk,I_aluk,I_eluk,I_iluk,
     & I_oluk,I_uluk,I_amuk,I_emuk,I_imuk,I_omuk,
     & I_umuk,I_apuk,I_epuk,I_ipuk,I_opuk,I_upuk,
     & I_aruk,I_eruk,INT(I_ud,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_imis,4),
     & R_evep,L_ufok,L_ofok,L_ifok,L_akok,L_ekok,L_ikok,
     & L_odok,L_idok,L_edok,L_udok,L_afok,L_efok,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,72,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 294, 238):���������� ���������� ������� �� ������� FDA60,BOAT72
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_ikat,R_afat,L_irat,L_emebe,
     & L_omabe,L_imabe,R_okat,R_efat,
     & L_orat,L_emabe,R_ukat,R_ifat,
     & L_urat,I_asat,I_esat,I_isat,I_osat,I_usat,
     & INT(I_akebe,4),L_ekebe,L_uvus,
     & L_axus,L_exus,L_ixus,
     & L_oxus,L_uxus,L_abat,
     & L_ebat,L_ibat,L_obat,I_(3),I_isus,
     & R_atat,REAL(R_afebe,4),R_etat,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_itat,R_otat,
     & R_utat,REAL(R_ikebe,4),R_avat,
     & REAL(R_okebe,4),R_ufat,R_idat,L_upat,L_ofat,
     & L_imebe,REAL(R_omebe,4),R_esus,R_ivat,
     & R_ovat,R_uvat,REAL(R_uvabe,4),R_axat,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_exat,
     & REAL(R_avabe,4),R_ixat,R_akat,R_odat,
     & L_arat,R_oxat,R_uxat,R_abet,L_utus,
     & R_ebet,R_ibet,REAL(R_erabe,4),R_obet,R_ekat,
     & R_udat,L_erat,R_ubet,R_adet,R_edet,R_idet,
     & R_odet,REAL(R_esabe,4),R_udet,L_avus,
     & L_alat,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_afet,R_efet,R_ifet,REAL(R_etabe,4),R_ofet,
     & L_evus,L_elat,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_ufet,R_aket,
     & R_eket,R_iket,REAL(R_epabe,4),R_oket,
     & L_otus,L_arabe,L_orabe,L_ilat,
     & L_olat,L_apabe,R_ifus,R_imat,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_omat,I_usus,R_umus,R_umat,R_uket,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_evat,R_amus,C20_epus
     &)
C FDA_80boats.fgi( 274, 266):���������� �������,BOAT73
      I_od=I_(3)
C FDA_80boats.fgi( 274, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT73:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_efel,R_evap
     &,
     & I_avap,R_itop,I_ifel,I_etop,I_ofel,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_ivuk,
     & R_ovip,I_ivip,I_ovuk,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_uvuk,R_ifop,
     & I_afop,I_axuk,R_ofop,
     & I_efop,I_exuk,R_irop,I_erop,
     & I_ixuk,R_osop,I_isop,I_oxuk,
     & R_urop,I_orop,I_uxuk,
     & R_atop,I_usop,I_abal,
     & R_esop,I_asop,I_ebal,I_ibal,
     & I_obal,I_ubal,I_adal,I_edal,I_idal,I_odal,
     & I_udal,I_afal,I_efal,I_ifal,I_ofal,I_ufal,
     & I_akal,I_ekal,I_ikal,I_okal,I_ukal,I_alal,
     & I_elal,I_ilal,I_olal,I_ulal,I_amal,I_emal,
     & I_imal,I_omal,I_umal,I_apal,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_eral,I_iral,I_oral,
     & I_ural,I_asal,I_esal,I_isal,I_osal,I_usal,
     & I_atal,I_etal,I_ital,I_otal,I_utal,I_aval,
     & I_eval,I_ival,I_oval,I_uval,I_axal,I_exal,
     & I_ixal,I_oxal,I_uxal,I_abel,I_ebel,I_ibel,
     & I_obel,I_ubel,I_adel,I_edel,I_idel,I_odel,
     & I_udel,I_afel,INT(I_od,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_esus,4),
     & R_evep,L_otuk,L_ituk,L_etuk,L_utuk,L_avuk,L_evuk,
     & L_isuk,L_esuk,L_asuk,L_osuk,L_usuk,L_atuk,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,73,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 274, 238):���������� ���������� ������� �� ������� FDA60,BOAT73
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_epit,R_ulit,L_evit,L_emebe,
     & L_omabe,L_imabe,R_ipit,R_amit,
     & L_ivit,L_emabe,R_opit,R_emit,
     & L_ovit,I_uvit,I_axit,I_exit,I_ixit,I_oxit,
     & INT(I_akebe,4),L_ekebe,L_odit,
     & L_udit,L_afit,L_efit,
     & L_ifit,L_ofit,L_ufit,
     & L_akit,L_ekit,L_ikit,I_(4),I_exet,
     & R_uxit,REAL(R_afebe,4),R_abot,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ebot,R_ibot,
     & R_obot,REAL(R_ikebe,4),R_ubot,
     & REAL(R_okebe,4),R_omit,R_elit,L_otit,L_imit,
     & L_imebe,REAL(R_omebe,4),R_axet,R_edot,
     & R_idot,R_odot,REAL(R_uvabe,4),R_udot,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_afot,
     & REAL(R_avabe,4),R_efot,R_umit,R_ilit,
     & L_utit,R_ifot,R_ofot,R_ufot,L_obit,
     & R_akot,R_ekot,REAL(R_erabe,4),R_ikot,R_apit,
     & R_olit,L_avit,R_okot,R_ukot,R_alot,R_elot,
     & R_ilot,REAL(R_esabe,4),R_olot,L_ubit,
     & L_upit,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_ulot,R_amot,R_emot,REAL(R_etabe,4),R_imot,
     & L_adit,L_arit,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_omot,R_umot,
     & R_apot,R_epot,REAL(R_epabe,4),R_ipot,
     & L_ibit,L_arabe,L_orabe,L_erit,
     & L_irit,L_apabe,R_emet,R_esit,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_isit,I_oxet,R_oset,R_osit,R_opot,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_adot,R_uret,C20_atet
     &)
C FDA_80boats.fgi( 254, 266):���������� �������,BOAT74
      I_id=I_(4)
C FDA_80boats.fgi( 254, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT74:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_atil,R_evap
     &,
     & I_avap,R_itop,I_etil,I_etop,I_itil,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_emel,
     & R_ovip,I_ivip,I_imel,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_omel,R_ifop,
     & I_afop,I_umel,R_ofop,
     & I_efop,I_apel,R_irop,I_erop,
     & I_epel,R_osop,I_isop,I_ipel,
     & R_urop,I_orop,I_opel,
     & R_atop,I_usop,I_upel,
     & R_esop,I_asop,I_arel,I_erel,
     & I_irel,I_orel,I_urel,I_asel,I_esel,I_isel,
     & I_osel,I_usel,I_atel,I_etel,I_itel,I_otel,
     & I_utel,I_avel,I_evel,I_ivel,I_ovel,I_uvel,
     & I_axel,I_exel,I_ixel,I_oxel,I_uxel,I_abil,
     & I_ebil,I_ibil,I_obil,I_ubil,I_adil,I_edil,
     & I_idil,I_odil,I_udil,I_afil,I_efil,I_ifil,
     & I_ofil,I_ufil,I_akil,I_ekil,I_ikil,I_okil,
     & I_ukil,I_alil,I_elil,I_ilil,I_olil,I_ulil,
     & I_amil,I_emil,I_imil,I_omil,I_umil,I_apil,
     & I_epil,I_ipil,I_opil,I_upil,I_aril,I_eril,
     & I_iril,I_oril,I_uril,I_asil,I_esil,I_isil,
     & I_osil,I_usil,INT(I_id,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_axet,4),
     & R_evep,L_ilel,L_elel,L_alel,L_olel,L_ulel,L_amel,
     & L_ekel,L_akel,L_ufel,L_ikel,L_okel,L_ukel,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,74,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 254, 238):���������� ���������� ������� �� ������� FDA60,BOAT74
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_atut,R_orut,L_adav,L_emebe,
     & L_omabe,L_imabe,R_etut,R_urut,
     & L_edav,L_emabe,R_itut,R_asut,
     & L_idav,I_odav,I_udav,I_afav,I_efav,I_ifav,
     & INT(I_akebe,4),L_ekebe,L_ilut,
     & L_olut,L_ulut,L_amut,
     & L_emut,L_imut,L_omut,
     & L_umut,L_aput,L_eput,I_(5),I_afut,
     & R_ofav,REAL(R_afebe,4),R_ufav,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_akav,R_ekav,
     & R_ikav,REAL(R_ikebe,4),R_okav,
     & REAL(R_okebe,4),R_isut,R_arut,L_ibav,L_esut,
     & L_imebe,REAL(R_omebe,4),R_udut,R_alav,
     & R_elav,R_ilav,REAL(R_uvabe,4),R_olav,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ulav,
     & REAL(R_avabe,4),R_amav,R_osut,R_erut,
     & L_obav,R_emav,R_imav,R_omav,L_ikut,
     & R_umav,R_apav,REAL(R_erabe,4),R_epav,R_usut,
     & R_irut,L_ubav,R_ipav,R_opav,R_upav,R_arav,
     & R_erav,REAL(R_esabe,4),R_irav,L_okut,
     & L_otut,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_orav,R_urav,R_asav,REAL(R_etabe,4),R_esav,
     & L_ukut,L_utut,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_isav,R_osav,
     & R_usav,R_atav,REAL(R_epabe,4),R_etav,
     & L_ekut,L_arabe,L_orabe,L_avut,
     & L_evut,L_apabe,R_asot,R_axut,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_exut,I_ifut,R_ixot,R_ixut,R_itav,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ukav,R_ovot,C20_uxot
     &)
C FDA_80boats.fgi( 234, 266):���������� �������,BOAT75
      I_ed=I_(5)
C FDA_80boats.fgi( 234, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT75:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ukul,R_evap
     &,
     & I_avap,R_itop,I_alul,I_etop,I_elul,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_abol,
     & R_ovip,I_ivip,I_ebol,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_ibol,R_ifop,
     & I_afop,I_obol,R_ofop,
     & I_efop,I_ubol,R_irop,I_erop,
     & I_adol,R_osop,I_isop,I_edol,
     & R_urop,I_orop,I_idol,
     & R_atop,I_usop,I_odol,
     & R_esop,I_asop,I_udol,I_afol,
     & I_efol,I_ifol,I_ofol,I_ufol,I_akol,I_ekol,
     & I_ikol,I_okol,I_ukol,I_alol,I_elol,I_ilol,
     & I_olol,I_ulol,I_amol,I_emol,I_imol,I_omol,
     & I_umol,I_apol,I_epol,I_ipol,I_opol,I_upol,
     & I_arol,I_erol,I_irol,I_orol,I_urol,I_asol,
     & I_esol,I_isol,I_osol,I_usol,I_atol,I_etol,
     & I_itol,I_otol,I_utol,I_avol,I_evol,I_ivol,
     & I_ovol,I_uvol,I_axol,I_exol,I_ixol,I_oxol,
     & I_uxol,I_abul,I_ebul,I_ibul,I_obul,I_ubul,
     & I_adul,I_edul,I_idul,I_odul,I_udul,I_aful,
     & I_eful,I_iful,I_oful,I_uful,I_akul,I_ekul,
     & I_ikul,I_okul,INT(I_ed,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_udut,4),
     & R_evep,L_exil,L_axil,L_uvil,L_ixil,L_oxil,L_uxil,
     & L_avil,L_util,L_otil,L_evil,L_ivil,L_ovil,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,75,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 234, 238):���������� ���������� ������� �� ������� FDA60,BOAT75
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_uxev,R_ivev,L_ukiv,L_emebe,
     & L_omabe,L_imabe,R_abiv,R_ovev,
     & L_aliv,L_emabe,R_ebiv,R_uvev,
     & L_eliv,I_iliv,I_oliv,I_uliv,I_amiv,I_emiv,
     & INT(I_akebe,4),L_ekebe,L_erev,
     & L_irev,L_orev,L_urev,
     & L_asev,L_esev,L_isev,
     & L_osev,L_usev,L_atev,I_(6),I_ulev,
     & R_imiv,REAL(R_afebe,4),R_omiv,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_umiv,R_apiv,
     & R_epiv,REAL(R_ikebe,4),R_ipiv,
     & REAL(R_okebe,4),R_exev,R_utev,L_ekiv,L_axev,
     & L_imebe,REAL(R_omebe,4),R_olev,R_upiv,
     & R_ariv,R_eriv,REAL(R_uvabe,4),R_iriv,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_oriv,
     & REAL(R_avabe,4),R_uriv,R_ixev,R_avev,
     & L_ikiv,R_asiv,R_esiv,R_isiv,L_epev,
     & R_osiv,R_usiv,REAL(R_erabe,4),R_ativ,R_oxev,
     & R_evev,L_okiv,R_etiv,R_itiv,R_otiv,R_utiv,
     & R_aviv,REAL(R_esabe,4),R_eviv,L_ipev,
     & L_ibiv,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_iviv,R_oviv,R_uviv,REAL(R_etabe,4),R_axiv,
     & L_opev,L_obiv,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_exiv,R_ixiv,
     & R_oxiv,R_uxiv,REAL(R_epabe,4),R_abov,
     & L_apev,L_arabe,L_orabe,L_ubiv,
     & L_adiv,L_apabe,R_uvav,R_udiv,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_afiv,I_emev,R_efev,R_efiv,R_ebov,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_opiv,R_idev,C20_ofev
     &)
C FDA_80boats.fgi( 214, 266):���������� �������,BOAT76
      I_ad=I_(6)
C FDA_80boats.fgi( 214, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT76:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ovam,R_evap
     &,
     & I_avap,R_itop,I_uvam,I_etop,I_axam,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_upul,
     & R_ovip,I_ivip,I_arul,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_erul,R_ifop,
     & I_afop,I_irul,R_ofop,
     & I_efop,I_orul,R_irop,I_erop,
     & I_urul,R_osop,I_isop,I_asul,
     & R_urop,I_orop,I_esul,
     & R_atop,I_usop,I_isul,
     & R_esop,I_asop,I_osul,I_usul,
     & I_atul,I_etul,I_itul,I_otul,I_utul,I_avul,
     & I_evul,I_ivul,I_ovul,I_uvul,I_axul,I_exul,
     & I_ixul,I_oxul,I_uxul,I_abam,I_ebam,I_ibam,
     & I_obam,I_ubam,I_adam,I_edam,I_idam,I_odam,
     & I_udam,I_afam,I_efam,I_ifam,I_ofam,I_ufam,
     & I_akam,I_ekam,I_ikam,I_okam,I_ukam,I_alam,
     & I_elam,I_ilam,I_olam,I_ulam,I_amam,I_emam,
     & I_imam,I_omam,I_umam,I_apam,I_epam,I_ipam,
     & I_opam,I_upam,I_aram,I_eram,I_iram,I_oram,
     & I_uram,I_asam,I_esam,I_isam,I_osam,I_usam,
     & I_atam,I_etam,I_itam,I_otam,I_utam,I_avam,
     & I_evam,I_ivam,INT(I_ad,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_olev,4),
     & R_evep,L_apul,L_umul,L_omul,L_epul,L_ipul,L_opul,
     & L_ulul,L_olul,L_ilul,L_amul,L_emul,L_imul,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,76,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 214, 238):���������� ���������� ������� �� ������� FDA60,BOAT76
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_ofuv,R_eduv,L_opuv,L_emebe,
     & L_omabe,L_imabe,R_ufuv,R_iduv,
     & L_upuv,L_emabe,R_akuv,R_oduv,
     & L_aruv,I_eruv,I_iruv,I_oruv,I_uruv,I_asuv,
     & INT(I_akebe,4),L_ekebe,L_avov,
     & L_evov,L_ivov,L_ovov,
     & L_uvov,L_axov,L_exov,
     & L_ixov,L_oxov,L_uxov,I_(7),I_orov,
     & R_esuv,REAL(R_afebe,4),R_isuv,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_osuv,R_usuv,
     & R_atuv,REAL(R_ikebe,4),R_etuv,
     & REAL(R_okebe,4),R_afuv,R_obuv,L_apuv,L_uduv,
     & L_imebe,REAL(R_omebe,4),R_irov,R_otuv,
     & R_utuv,R_avuv,REAL(R_uvabe,4),R_evuv,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ivuv,
     & REAL(R_avabe,4),R_ovuv,R_efuv,R_ubuv,
     & L_epuv,R_uvuv,R_axuv,R_exuv,L_atov,
     & R_ixuv,R_oxuv,REAL(R_erabe,4),R_uxuv,R_ifuv,
     & R_aduv,L_ipuv,R_abax,R_ebax,R_ibax,R_obax,
     & R_ubax,REAL(R_esabe,4),R_adax,L_etov,
     & L_ekuv,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_edax,R_idax,R_odax,REAL(R_etabe,4),R_udax,
     & L_itov,L_ikuv,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_afax,R_efax,
     & R_ifax,R_ofax,REAL(R_epabe,4),R_ufax,
     & L_usov,L_arabe,L_orabe,L_okuv,
     & L_ukuv,L_apabe,R_odov,R_oluv,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_uluv,I_asov,R_amov,R_amuv,R_akax,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ituv,R_elov,C20_imov
     &)
C FDA_80boats.fgi( 194, 266):���������� �������,BOAT77
      I_u=I_(7)
C FDA_80boats.fgi( 194, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT77:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_imim,R_evap
     &,
     & I_avap,R_itop,I_omim,I_etop,I_umim,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_odem,
     & R_ovip,I_ivip,I_udem,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_afem,R_ifop,
     & I_afop,I_efem,R_ofop,
     & I_efop,I_ifem,R_irop,I_erop,
     & I_ofem,R_osop,I_isop,I_ufem,
     & R_urop,I_orop,I_akem,
     & R_atop,I_usop,I_ekem,
     & R_esop,I_asop,I_ikem,I_okem,
     & I_ukem,I_alem,I_elem,I_ilem,I_olem,I_ulem,
     & I_amem,I_emem,I_imem,I_omem,I_umem,I_apem,
     & I_epem,I_ipem,I_opem,I_upem,I_arem,I_erem,
     & I_irem,I_orem,I_urem,I_asem,I_esem,I_isem,
     & I_osem,I_usem,I_atem,I_etem,I_item,I_otem,
     & I_utem,I_avem,I_evem,I_ivem,I_ovem,I_uvem,
     & I_axem,I_exem,I_ixem,I_oxem,I_uxem,I_abim,
     & I_ebim,I_ibim,I_obim,I_ubim,I_adim,I_edim,
     & I_idim,I_odim,I_udim,I_afim,I_efim,I_ifim,
     & I_ofim,I_ufim,I_akim,I_ekim,I_ikim,I_okim,
     & I_ukim,I_alim,I_elim,I_ilim,I_olim,I_ulim,
     & I_amim,I_emim,INT(I_u,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_irov,4),
     & R_evep,L_ubem,L_obem,L_ibem,L_adem,L_edem,L_idem,
     & L_oxam,L_ixam,L_exam,L_uxam,L_abem,L_ebem,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,77,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 194, 238):���������� ���������� ������� �� ������� FDA60,BOAT77
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_imex,R_alex,L_itex,L_emebe,
     & L_omabe,L_imabe,R_omex,R_elex,
     & L_otex,L_emabe,R_umex,R_ilex,
     & L_utex,I_avex,I_evex,I_ivex,I_ovex,I_uvex,
     & INT(I_akebe,4),L_ekebe,L_ubex,
     & L_adex,L_edex,L_idex,
     & L_odex,L_udex,L_afex,
     & L_efex,L_ifex,L_ofex,I_(8),I_ivax,
     & R_axex,REAL(R_afebe,4),R_exex,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ixex,R_oxex,
     & R_uxex,REAL(R_ikebe,4),R_abix,
     & REAL(R_okebe,4),R_ulex,R_ikex,L_usex,L_olex,
     & L_imebe,REAL(R_omebe,4),R_evax,R_ibix,
     & R_obix,R_ubix,REAL(R_uvabe,4),R_adix,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_edix,
     & REAL(R_avabe,4),R_idix,R_amex,R_okex,
     & L_atex,R_odix,R_udix,R_afix,L_uxax,
     & R_efix,R_ifix,REAL(R_erabe,4),R_ofix,R_emex,
     & R_ukex,L_etex,R_ufix,R_akix,R_ekix,R_ikix,
     & R_okix,REAL(R_esabe,4),R_ukix,L_abex,
     & L_apex,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_alix,R_elix,R_ilix,REAL(R_etabe,4),R_olix,
     & L_ebex,L_epex,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_ulix,R_amix,
     & R_emix,R_imix,REAL(R_epabe,4),R_omix,
     & L_oxax,L_arabe,L_orabe,L_ipex,
     & L_opex,L_apabe,R_ilax,R_irex,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_orex,I_uvax,R_urax,R_urex,R_umix,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ebix,R_arax,C20_esax
     &)
C FDA_80boats.fgi( 174, 266):���������� �������,BOAT78
      I_o=I_(8)
C FDA_80boats.fgi( 174, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT78:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ebum,R_evap
     &,
     & I_avap,R_itop,I_ibum,I_etop,I_obum,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_isim,
     & R_ovip,I_ivip,I_osim,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_usim,R_ifop,
     & I_afop,I_atim,R_ofop,
     & I_efop,I_etim,R_irop,I_erop,
     & I_itim,R_osop,I_isop,I_otim,
     & R_urop,I_orop,I_utim,
     & R_atop,I_usop,I_avim,
     & R_esop,I_asop,I_evim,I_ivim,
     & I_ovim,I_uvim,I_axim,I_exim,I_ixim,I_oxim,
     & I_uxim,I_abom,I_ebom,I_ibom,I_obom,I_ubom,
     & I_adom,I_edom,I_idom,I_odom,I_udom,I_afom,
     & I_efom,I_ifom,I_ofom,I_ufom,I_akom,I_ekom,
     & I_ikom,I_okom,I_ukom,I_alom,I_elom,I_ilom,
     & I_olom,I_ulom,I_amom,I_emom,I_imom,I_omom,
     & I_umom,I_apom,I_epom,I_ipom,I_opom,I_upom,
     & I_arom,I_erom,I_irom,I_orom,I_urom,I_asom,
     & I_esom,I_isom,I_osom,I_usom,I_atom,I_etom,
     & I_itom,I_otom,I_utom,I_avom,I_evom,I_ivom,
     & I_ovom,I_uvom,I_axom,I_exom,I_ixom,I_oxom,
     & I_uxom,I_abum,INT(I_o,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_evax,4),
     & R_evep,L_orim,L_irim,L_erim,L_urim,L_asim,L_esim,
     & L_ipim,L_epim,L_apim,L_opim,L_upim,L_arim,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,78,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 174, 238):���������� ���������� ������� �� ������� FDA60,BOAT78
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_esox,R_upox,L_ebux,L_emebe,
     & L_omabe,L_imabe,R_isox,R_arox,
     & L_ibux,L_emabe,R_osox,R_erox,
     & L_obux,I_ubux,I_adux,I_edux,I_idux,I_odux,
     & INT(I_akebe,4),L_ekebe,L_okox,
     & L_ukox,L_alox,L_elox,
     & L_ilox,L_olox,L_ulox,
     & L_amox,L_emox,L_imox,I_(9),I_edox,
     & R_udux,REAL(R_afebe,4),R_afux,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_efux,R_ifux,
     & R_ofux,REAL(R_ikebe,4),R_ufux,
     & REAL(R_okebe,4),R_orox,R_epox,L_oxox,L_irox,
     & L_imebe,REAL(R_omebe,4),R_adox,R_ekux,
     & R_ikux,R_okux,REAL(R_uvabe,4),R_ukux,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_alux,
     & REAL(R_avabe,4),R_elux,R_urox,R_ipox,
     & L_uxox,R_ilux,R_olux,R_ulux,L_ofox,
     & R_amux,R_emux,REAL(R_erabe,4),R_imux,R_asox,
     & R_opox,L_abux,R_omux,R_umux,R_apux,R_epux,
     & R_ipux,REAL(R_esabe,4),R_opux,L_ufox,
     & L_usox,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_upux,R_arux,R_erux,REAL(R_etabe,4),R_irux,
     & L_akox,L_atox,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_orux,R_urux,
     & R_asux,R_esux,REAL(R_epabe,4),R_isux,
     & L_ifox,L_arabe,L_orabe,L_etox,
     & L_itox,L_apabe,R_erix,R_evox,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_ivox,I_odox,R_ovix,R_ovox,R_osux,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_akux,R_utix,C20_axix
     &)
C FDA_80boats.fgi( 154, 266):���������� �������,BOAT79
      I_i=I_(9)
C FDA_80boats.fgi( 154, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT79:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_arap,R_evap
     &,
     & I_avap,R_itop,I_erap,I_etop,I_irap,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_ekum,
     & R_ovip,I_ivip,I_ikum,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_okum,R_ifop,
     & I_afop,I_ukum,R_ofop,
     & I_efop,I_alum,R_irop,I_erop,
     & I_elum,R_osop,I_isop,I_ilum,
     & R_urop,I_orop,I_olum,
     & R_atop,I_usop,I_ulum,
     & R_esop,I_asop,I_amum,I_emum,
     & I_imum,I_omum,I_umum,I_apum,I_epum,I_ipum,
     & I_opum,I_upum,I_arum,I_erum,I_irum,I_orum,
     & I_urum,I_asum,I_esum,I_isum,I_osum,I_usum,
     & I_atum,I_etum,I_itum,I_otum,I_utum,I_avum,
     & I_evum,I_ivum,I_ovum,I_uvum,I_axum,I_exum,
     & I_ixum,I_oxum,I_uxum,I_abap,I_ebap,I_ibap,
     & I_obap,I_ubap,I_adap,I_edap,I_idap,I_odap,
     & I_udap,I_afap,I_efap,I_ifap,I_ofap,I_ufap,
     & I_akap,I_ekap,I_ikap,I_okap,I_ukap,I_alap,
     & I_elap,I_ilap,I_olap,I_ulap,I_amap,I_emap,
     & I_imap,I_omap,I_umap,I_apap,I_epap,I_ipap,
     & I_opap,I_upap,INT(I_i,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_adox,4),
     & R_evep,L_ifum,L_efum,L_afum,L_ofum,L_ufum,L_akum,
     & L_edum,L_adum,L_ubum,L_idum,L_odum,L_udum,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,79,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 154, 238):���������� ���������� ������� �� ������� FDA60,BOAT79
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_osebe,R_erebe,L_obibe,L_emebe,
     & L_omabe,L_imabe,R_usebe,R_irebe,
     & L_ubibe,L_emabe,R_atebe,R_orebe,
     & L_adibe,I_edibe,I_idibe,I_odibe,I_udibe,I_afibe,
     & INT(I_akebe,4),L_ekebe,L_ixabe,
     & L_oxabe,L_uxabe,L_abebe,
     & L_ebebe,L_ibebe,L_obebe,
     & L_ubebe,L_adebe,L_edebe,I_(10),I_alabe,
     & R_efibe,REAL(R_afebe,4),R_ifibe,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ofibe,R_ufibe,
     & R_akibe,REAL(R_ikebe,4),R_ekibe,
     & REAL(R_okebe,4),R_asebe,R_opebe,L_abibe,L_urebe,
     & L_imebe,REAL(R_omebe,4),R_ukabe,R_okibe,
     & R_ukibe,R_alibe,REAL(R_uvabe,4),R_elibe,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ilibe,
     & REAL(R_avabe,4),R_olibe,R_esebe,R_upebe,
     & L_ebibe,R_ulibe,R_amibe,R_emibe,L_irabe,
     & R_imibe,R_omibe,REAL(R_erabe,4),R_umibe,R_isebe,
     & R_arebe,L_ibibe,R_apibe,R_epibe,R_ipibe,R_opibe,
     & R_upibe,REAL(R_esabe,4),R_aribe,L_isabe,
     & L_etebe,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_eribe,R_iribe,R_oribe,REAL(R_etabe,4),R_uribe,
     & L_itabe,L_itebe,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_asibe,R_esibe,
     & R_isibe,R_osibe,REAL(R_epabe,4),R_usibe,
     & L_ipabe,L_arabe,L_orabe,L_otebe,
     & L_utebe,L_apabe,R_avux,R_ovebe,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_uvebe,I_ilabe,R_idabe,R_axebe,R_atibe,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ikibe,R_obabe,C20_udabe
     &)
C FDA_80boats.fgi( 134, 266):���������� �������,BOAT80
      I_e=I_(10)
C FDA_80boats.fgi( 134, 266):������-�������: ���������� ��� �������������� ������,cpy BOAT80:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_idar,R_evap
     &,
     & I_avap,R_itop,I_odar,I_etop,I_udar,
     & I_exip,R_obop,I_ixip,
     & R_ubop,I_oxip,R_adop,
     & I_uxip,R_edop,I_abop,
     & R_idop,I_ebop,R_odop,
     & I_ibop,R_udop,I_otop,
     & R_ovip,I_ivip,I_utop,
     & R_axip,I_uvip,R_arop,
     & I_upop,I_avop,R_ifop,
     & I_afop,I_evop,R_ofop,
     & I_efop,I_ivop,R_irop,I_erop,
     & I_ovop,R_osop,I_isop,I_uvop,
     & R_urop,I_orop,I_axop,
     & R_atop,I_usop,I_exop,
     & R_esop,I_asop,I_ixop,I_oxop,
     & I_uxop,I_abup,I_ebup,I_ibup,I_obup,I_ubup,
     & I_adup,I_edup,I_idup,I_odup,I_udup,I_afup,
     & I_efup,I_ifup,I_ofup,I_ufup,I_akup,I_ekup,
     & I_ikup,I_okup,I_ukup,I_alup,I_elup,I_ilup,
     & I_olup,I_ulup,I_amup,I_emup,I_imup,I_omup,
     & I_umup,I_apup,I_epup,I_ipup,I_opup,I_upup,
     & I_arup,I_erup,I_irup,I_orup,I_urup,I_asup,
     & I_esup,I_isup,I_osup,I_usup,I_atup,I_etup,
     & I_itup,I_otup,I_utup,I_avup,I_evup,I_ivup,
     & I_ovup,I_uvup,I_axup,I_exup,I_ixup,I_oxup,
     & I_uxup,I_abar,I_ebar,I_ibar,I_obar,I_ubar,
     & I_adar,I_edar,INT(I_e,4),R_ebep,R_abep,
     & R_uxap,R_oxap,R_ixap,R_ebip,
     & R_abip,R_uxep,R_oxep,R_ixep,R_idip,
     & R_odip,R_udip,R_afip,R_efip,
     & R_idep,R_odep,R_udep,R_afep,
     & R_efep,R_ikip,R_okip,R_ukip,
     & R_alip,R_elip,R_ikep,R_okep,
     & R_ukep,R_alep,R_elep,R_opop,
     & R_ipop,R_epop,R_apop,
     & R_umop,R_olop,R_ilop,
     & R_elop,R_alop,R_ukop,
     & R_imip,R_omip,R_umip,R_apip,R_epip,
     & R_imep,R_omep,R_umep,R_apep,
     & R_epep,R_esep,R_asep,R_urep,
     & R_orep,R_irep,R_esip,R_asip,
     & R_urip,R_orip,R_irip,R_itip,R_otip,
     & R_utip,R_avip,R_evip,R_itep,
     & R_otep,R_utep,R_avep,REAL(R_ukabe,4),
     & R_evep,L_etap,L_atap,L_usap,L_itap,L_otap,L_utap,
     & L_asap,L_urap,L_orap,L_esap,L_isap,L_osap,I_exap,
     & I_axap,I_ivep,I_ovep,I_uvep,I_axep,I_exep,
     & I_ivap,I_ovap,I_uvap,I_edep,I_adep,
     & I_ibip,I_obip,I_ubip,I_adip,I_edip,I_ibep,
     & I_obep,I_ubep,I_ekep,I_akep,I_ifip,
     & I_ofip,I_ufip,I_akip,I_ekip,I_ifep,I_ofep,
     & I_ufep,I_emep,I_amep,I_ilip,I_olip,
     & I_ulip,I_amip,I_emip,I_ilep,I_olep,
     & I_ulep,I_erep,I_arep,I_ipip,I_opip,
     & I_upip,I_arip,I_erip,I_ipep,I_opep,
     & I_upep,I_etep,I_atep,I_isip,I_osip,
     & I_usip,I_atip,I_etip,I_isep,I_osep,
     & I_usep,I_ufop,I_akop,I_ekop,
     & I_ikop,I_okop,80,I_ulop,
     & I_amop,I_emop,I_imop,
     & I_omop)
C FDA_80boats.fgi( 134, 238):���������� ���������� ������� �� ������� FDA60,BOAT80
      End
