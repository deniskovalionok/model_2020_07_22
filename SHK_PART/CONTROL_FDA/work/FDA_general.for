      Subroutine FDA_general(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA_general.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA_general_ConIn
      R_(17)=R0_atuxi
C FDA_doz_logic.fgi(  57, 473):pre: ������������  �� T
      R_(196)=R_o !CopyBack
C FDA_doz_logic.fgi(  76, 479):pre: ������-�������: ���������� ��� �������������� ������
      R_(49)=R0_ebebo
C FDA_doz_logic.fgi(  57, 479):pre: ������������  �� T
      R_(50)=R0_ubebo
C FDA_doz_logic.fgi(  57, 491):pre: ������������  �� T
      R_(194)=R0_apirad
C FDA_doz_logic.fgi(  46, 634):pre: ������������  �� T
      R_(193)=R0_imirad
C FDA_doz_logic.fgi(  46, 628):pre: ������������  �� T
      R_(192)=R0_ulirad
C FDA_doz_logic.fgi(  46, 605):pre: ������������  �� T
      R_(191)=R0_elirad
C FDA_doz_logic.fgi(  46, 599):pre: ������������  �� T
      R_(190)=R0_okirad
C FDA_doz_logic.fgi(  46, 577):pre: ������������  �� T
      R_(189)=R0_akirad
C FDA_doz_logic.fgi(  46, 571):pre: ������������  �� T
      I_(109)=I0_ifirad
C FDA_doz_logic.fgi(  48, 427):pre: �������� ������ �� N �����
      I_(108)=I0_udirad
C FDA_doz_logic.fgi(  48, 415):pre: �������� ������ �� N �����
      I_(107)=I0_edirad
C FDA_doz_logic.fgi(  48, 404):pre: �������� ������ �� N �����
      I_(106)=I0_obirad
C FDA_doz_logic.fgi(  48, 392):pre: �������� ������ �� N �����
      I_(105)=I0_abirad
C FDA_doz_logic.fgi(  48, 381):pre: �������� ������ �� N �����
      I_(104)=I0_ixerad
C FDA_doz_logic.fgi(  48, 370):pre: �������� ������ �� N �����
      I_(103)=I0_uverad
C FDA_doz_logic.fgi(  48, 359):pre: �������� ������ �� N �����
      I_(102)=I0_everad
C FDA_doz_logic.fgi(  48, 348):pre: �������� ������ �� N �����
      R_(198)=R_e !CopyBack
C FDA_doz_logic.fgi(  76, 546):pre: ������-�������: ���������� ��� �������������� ������
      R_(59)=R0_emebo
C FDA_doz_logic.fgi(  57, 546):pre: ������������  �� T
      R_(58)=R0_olebo
C FDA_doz_logic.fgi(  57, 540):pre: ������������  �� T
      R_(57)=R0_alebo
C FDA_doz_logic.fgi(  57, 534):pre: ������������  �� T
      R_(56)=R0_ikebo
C FDA_doz_logic.fgi(  57, 528):pre: ������������  �� T
      R_(55)=R0_ufebo
C FDA_doz_logic.fgi(  57, 522):pre: ������������  �� T
      R_(54)=R0_efebo
C FDA_doz_logic.fgi(  57, 516):pre: ������������  �� T
      R_(53)=R0_odebo
C FDA_doz_logic.fgi(  57, 510):pre: ������������  �� T
      R_(48)=R0_ovabo
C FDA_doz_logic.fgi(  57, 485):pre: ������������  �� T
      R_(47)=R0_avabo
C FDA_doz_logic.fgi(  57, 500):pre: ������������  �� T
      R_(44)=R0_osabo
C FDA_doz_logic.fgi(  57, 552):pre: ������������  �� T
      R_(42)=R0_orabo
C FDA_doz_logic.fgi(  57, 558):pre: ������������  �� T
      R_(197)=R_i !CopyBack
C FDA_doz_logic.fgi( 169, 558):pre: ������-�������: ���������� ��� �������������� ������
      R_(34)=R0_omabo
C FDA_doz_logic.fgi( 150, 546):pre: ������������  �� T
      R_(33)=R0_amabo
C FDA_doz_logic.fgi( 150, 540):pre: ������������  �� T
      R_(32)=R0_ilabo
C FDA_doz_logic.fgi( 150, 534):pre: ������������  �� T
      R_(31)=R0_ukabo
C FDA_doz_logic.fgi( 150, 528):pre: ������������  �� T
      R_(30)=R0_ekabo
C FDA_doz_logic.fgi( 150, 522):pre: ������������  �� T
      R_(29)=R0_ofabo
C FDA_doz_logic.fgi( 150, 516):pre: ������������  �� T
      R_(28)=R0_afabo
C FDA_doz_logic.fgi( 150, 510):pre: ������������  �� T
      R_(27)=R0_idabo
C FDA_doz_logic.fgi( 150, 500):pre: ������������  �� T
      R_(24)=R0_ibabo
C FDA_doz_logic.fgi( 150, 552):pre: ������������  �� T
      R_(22)=R0_oxuxi
C FDA_doz_logic.fgi( 150, 558):pre: ������������  �� T
      R_(20)=R0_ovuxi
C FDA_doz_logic.fgi(  57, 564):pre: ������������  �� T
      R_(18)=R0_utuxi
C FDA_doz_logic.fgi( 150, 564):pre: ������������  �� T
      R_(13)=R0_afixi
C FDA_lamp.fgi(  68, 728):pre: �������� ��������� ������
      R_(12)=R0_idixi
C FDA_lamp.fgi(  68, 717):pre: �������� ��������� ������
      R_(11)=R0_ubixi
C FDA_lamp.fgi(  68, 706):pre: �������� ��������� ������
      R_(10)=R0_ebixi
C FDA_lamp.fgi(  68, 695):pre: �������� ��������� ������
      R_(9)=R0_oxexi
C FDA_lamp.fgi(  73, 750):pre: �������� ��������� ������
      R_(8)=R0_axexi
C FDA_lamp.fgi(  67, 767):pre: �������� ��������� ������
      R_(7)=R0_ivexi
C FDA_lamp.fgi(  67, 778):pre: �������� ��������� ������
      R_(6)=R0_utexi
C FDA_lamp.fgi(  67, 789):pre: �������� ��������� ������
      R_(5)=R0_udexi
C FDA_lamp.fgi(  69, 651):pre: �������� ��������� ������
      R_(4)=R0_edexi
C FDA_lamp.fgi(  69, 662):pre: �������� ��������� ������
      R_(3)=R0_obexi
C FDA_lamp.fgi(  69, 673):pre: �������� ��������� ������
      R_(2)=R0_avaxi
C FDA_lamp.fgi(  69, 624):pre: �������� ��������� ������
      R_(1)=R0_itaxi
C FDA_lamp.fgi(  69, 635):pre: �������� ��������� ������
      I_(26)=I_ake !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(24)=I_ike !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(27)=I_ufe !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(25)=I_eke !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(23)=I_oke !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(20)=I_ele !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(97)=I_u !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=I_ad !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(95)=I_ed !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(94)=I_id !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(93)=I_od !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(92)=I_ud !CopyBack
C FDA_10boats.fgi( 200, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(91)=I_af !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(90)=I_ef !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(89)=I_if !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(88)=I_of !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(22)=I_uke !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(21)=I_ale !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(19)=I_ile !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(18)=I_ole !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(17)=I_ule !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(16)=I_ame !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(15)=I_eme !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(14)=I_ime !CopyBack
C FDA_10boats.fgi( 180, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(13)=I_ome !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(12)=I_ume !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(11)=I_ape !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(57)=I_us !CopyBack
C FDA_10boats.fgi( 200, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(56)=I_at !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(55)=I_et !CopyBack
C FDA_10boats.fgi( 180, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(54)=I_it !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(53)=I_ot !CopyBack
C FDA_10boats.fgi( 160, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(52)=I_ut !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(51)=I_av !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(50)=I_ev !CopyBack
C FDA_10boats.fgi( 180, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(49)=I_iv !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(48)=I_ov !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(47)=I_uv !CopyBack
C FDA_10boats.fgi( 180, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(46)=I_ax !CopyBack
C FDA_10boats.fgi( 160, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(45)=I_ex !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(44)=I_ix !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(43)=I_ox !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(42)=I_ux !CopyBack
C FDA_10boats.fgi( 160, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(41)=I_abe !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(40)=I_ebe !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(39)=I_ibe !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(38)=I_obe !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(37)=I_ube !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(36)=I_ade !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(35)=I_ede !CopyBack
C FDA_10boats.fgi( 160, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(34)=I_ide !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(33)=I_ode !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(32)=I_ude !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(31)=I_afe !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(30)=I_efe !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(29)=I_ife !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(28)=I_ofe !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(87)=I_uf !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(86)=I_ak !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(85)=I_ek !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(84)=I_ik !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(83)=I_ok !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(82)=I_uk !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(81)=I_al !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(80)=I_el !CopyBack
C FDA_10boats.fgi( 180, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(79)=I_il !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(78)=I_ol !CopyBack
C FDA_10boats.fgi( 200, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(77)=I_ul !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(76)=I_am !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(75)=I_em !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(74)=I_im !CopyBack
C FDA_10boats.fgi( 200, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(73)=I_om !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(72)=I_um !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(71)=I_ap !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(70)=I_ep !CopyBack
C FDA_10boats.fgi( 160, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(69)=I_ip !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(68)=I_op !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(67)=I_up !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(66)=I_ar !CopyBack
C FDA_10boats.fgi( 100, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(65)=I_er !CopyBack
C FDA_10boats.fgi(  40, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(64)=I_ir !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(63)=I_or !CopyBack
C FDA_10boats.fgi( 120, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(62)=I_ur !CopyBack
C FDA_10boats.fgi(  80, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(61)=I_as !CopyBack
C FDA_10boats.fgi( 160, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(60)=I_es !CopyBack
C FDA_10boats.fgi( 140, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(59)=I_is !CopyBack
C FDA_10boats.fgi(  60, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(58)=I_os !CopyBack
C FDA_10boats.fgi( 220, 230):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi(  40, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT10
      I_(10)=I_epe !CopyBack
C FDA_10boats.fgi(  39, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi(  60, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT09
      I_(9)=I_ipe !CopyBack
C FDA_10boats.fgi(  59, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi(  80, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT08
      I_(8)=I_ope !CopyBack
C FDA_10boats.fgi(  79, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 100, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT07
      I_(7)=I_upe !CopyBack
C FDA_10boats.fgi(  99, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 120, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT06
      I_(6)=I_are !CopyBack
C FDA_10boats.fgi( 119, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 140, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT05
      I_(5)=I_ere !CopyBack
C FDA_10boats.fgi( 139, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 160, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT04
      I_(4)=I_ire !CopyBack
C FDA_10boats.fgi( 159, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 180, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT03
      I_(3)=I_ore !CopyBack
C FDA_10boats.fgi( 179, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 200, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT02
      I_(2)=I_ure !CopyBack
C FDA_10boats.fgi( 199, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_(96)=0
      I_(97)=0
      I_(95)=0
      I_(94)=0
      I_(93)=0
      I_(92)=0
      I_(91)=0
      I_(90)=0
      I_(89)=0
      I_(88)=0
      I_(87)=0
      I_(86)=0
      I_(85)=0
      I_(84)=0
      I_(83)=0
      I_(57)=0
      I_(56)=0
      I_(55)=0
      I_(54)=0
      I_(53)=0
      I_(82)=0
      I_(81)=0
      I_(80)=0
      I_(79)=0
      I_(78)=0
      I_(52)=0
      I_(51)=0
      I_(50)=0
      I_(49)=0
      I_(48)=0
      I_(77)=0
      I_(76)=0
      I_(75)=0
      I_(74)=0
      I_(73)=0
      I_(47)=0
      I_(46)=0
      I_(45)=0
      I_(44)=0
      I_(43)=0
      I_(72)=0
      I_(71)=0
      I_(70)=0
      I_(69)=0
      I_(68)=0
      I_(42)=0
      I_(41)=0
      I_(40)=0
      I_(39)=0
      I_(38)=0
      I_(67)=0
      I_(66)=0
      I_(65)=0
      I_(64)=0
      I_(63)=0
      I_(37)=0
      I_(36)=0
      I_(35)=0
      I_(34)=0
      I_(33)=0
      I_(62)=0
      I_(61)=0
      I_(60)=0
      I_(59)=0
      I_(58)=0
      I_(32)=0
      I_(31)=0
      I_(30)=0
      I_(29)=0
      I_(28)=0
      R_odil=0
      R_udil=0
      R_idil=0
      R_edil=0
      R_adil=0
      R_ubil=0
      R_obil=0
      R_ibil=0
      R_ebil=0
      R_abil=0
      R_ekel=0
      R_akel=0
      R_ufel=0
      R_ofel=0
      R_ifel=0
      R_etel=0
      R_atel=0
      R_usel=0
      R_osel=0
      R_isel=0
      R_efel=0
      R_afel=0
      R_udel=0
      R_odel=0
      R_idel=0
      R_esel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_irel=0
      R_edel=0
      R_adel=0
      R_ubel=0
      R_obel=0
      R_ibel=0
      R_erel=0
      R_arel=0
      R_upel=0
      R_opel=0
      R_ipel=0
      R_ebel=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_imel=0
      R_exal=0
      R_axal=0
      R_uval=0
      R_oval=0
      R_ival=0
      R_emel=0
      R_amel=0
      R_ulel=0
      R_olel=0
      R_ilel=0
      R_eval=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_elel=0
      R_alel=0
      R_ukel=0
      R_okel=0
      R_ikel=0
      I_(27)=0
      R_ofil=0
      I_(26)=0
      R_akil=0
      I_(25)=0
      R_ifil=0
      I_(24)=0
      R_ufil=0
      I_(23)=0
      R_efil=0
      I_(22)=0
      R_uxel=0
      I_(21)=0
      R_oxel=0
      I_(20)=0
      R_afil=0
      I_(19)=0
      R_ixel=0
      I_(18)=0
      R_exel=0
      I_(17)=0
      R_axel=0
      I_(16)=0
      R_uvel=0
      I_(15)=0
      R_ovel=0
      I_(14)=0
      R_ivel=0
      I_(13)=0
      R_evel=0
      I_(12)=0
      R_avel=0
      I_(11)=0
      R_utel=0
      I_okil=0
      R_ukil=0
      I_ekil=0
      R_ikil=0
      I_itel=0
      R_otel=0
C FDA_10boats.fgi( 220, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT01
      I_(1)=I_ase !CopyBack
C FDA_10boats.fgi( 219, 276):pre: ������-�������: ���������� ��� �������������� ������
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi(  40, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT10
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi(  60, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT09
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi(  80, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT08
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 100, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT07
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 120, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT06
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 140, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT05
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 160, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT04
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 180, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT03
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 200, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT02
      I_apor=z'1000021'
      I_ukor=z'1000021'
      I_odor=z'1000021'
      I_ixir=z'1000021'
      I_etir=z'1000021'
      I_arir=z'1000021'
      I_ulir=z'1000021'
      I_ofir=z'1000021'
      I_efir=z'1000021'
      I_umor=z'1000021'
      I_omor=z'1000021'
      I_imor=z'1000021'
      I_emor=z'1000021'
      I_amor=z'1000021'
      I_ulor=z'1000021'
      I_olor=z'1000021'
      I_ilor=z'1000021'
      I_elor=z'1000021'
      I_alor=z'1000021'
      I_okor=z'1000021'
      I_ikor=z'1000021'
      I_ekor=z'1000021'
      I_akor=z'1000021'
      I_ufor=z'1000021'
      I_ofor=z'1000021'
      I_ifor=z'1000021'
      I_efor=z'1000021'
      I_afor=z'1000021'
      I_udor=z'1000021'
      I_idor=z'1000021'
      I_edor=z'1000021'
      I_ador=z'1000021'
      I_ubor=z'1000021'
      I_obor=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_ivir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_esir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_apir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_ukir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ifir=z'1000021'
C FDA_10boats.fgi( 220, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT01
      R_(16)=R0_asuxi
C FDA_doz_logic.fgi( 227, 704):pre: ���������������� ����� 
      R_(67)=R0_etebo
C FDA_doz_logic.fgi( 412, 719):pre: ���������������� ����� 
      Call FDA_general_1(ext_deltat)
      End
      Subroutine FDA_general_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA_general.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !{
      Call SHIB_HANDLER(deltat,R8_omoli,R_akuli,R_ekuli,R_ibuli
     &,R_abuli,
     & L_efuli,R_obuli,R_ebuli,L_ifuli,R_oxoli,
     & R_uxoli,R_afuli,R_ofuli,R_ufuli,R_efoli,
     & R_uduli,R_afoli,REAL(R_umoli,4),C20_epoli,I_itoli,I_ovoli
     &,
     & R_iloli,REAL(R_uloli,4),R_ukoli,
     & REAL(R_eloli,4),I_eroli,I_aroli,I_otoli,I_evoli,C20_apoli
     &,
     & C20_ipoli,C8_opoli,L_ufoli,R_ofoli,
     & REAL(R_akoli,4),L_ikoli,R_ekoli,
     & REAL(R_okoli,4),I_avoli,I_ivoli,I_utoli,I_etoli,L_amoli
     &,
     & L_axoli,L_aduli,L_ololi,L_aloli,
     & L_imoli,L_emoli,L_ixoli,L_upoli,L_ubuli,L_oroli,L_uroli
     &,
     & REAL(R8_ifoli,8),REAL(1.0,4),R8_iroli,L_(126),L_idoli
     &,L_(125),L_edoli,
     & L_exoli,I_uvoli,L_eduli,L_udoli,L_oduli,L_odoli,L_asoli
     &,L_esoli,L_isoli,L_usoli,
     & L_atoli,L_osoli)
      !}
C FDA_mechanic_vlv.fgi( 325, 941):���������� ������,20FDA61AB806
      !{
      Call SHIB_HANDLER(deltat,R8_okari,R_aderi,R_ederi,R_ivari
     &,R_avari,
     & L_eberi,R_ovari,R_evari,L_iberi,R_otari,
     & R_utari,R_aberi,R_oberi,R_uberi,R_ebari,
     & R_uxari,R_abari,REAL(R_ukari,4),C20_elari,I_irari,I_osari
     &,
     & R_ifari,REAL(R_ufari,4),R_udari,
     & REAL(R_efari,4),I_emari,I_amari,I_orari,I_esari,C20_alari
     &,
     & C20_ilari,C8_olari,L_ubari,R_obari,
     & REAL(R_adari,4),L_idari,R_edari,
     & REAL(R_odari,4),I_asari,I_isari,I_urari,I_erari,L_akari
     &,
     & L_atari,L_axari,L_ofari,L_afari,
     & L_ikari,L_ekari,L_itari,L_ulari,L_uvari,L_omari,L_umari
     &,
     & REAL(R8_ibari,8),REAL(1.0,4),R8_imari,L_(146),L_ixupi
     &,L_(145),L_exupi,
     & L_etari,I_usari,L_exari,L_uxupi,L_oxari,L_oxupi,L_apari
     &,L_epari,L_ipari,L_upari,
     & L_arari,L_opari)
      !}
C FDA_mechanic_vlv.fgi( 164, 915):���������� ������,20FDA61AB801
      !{
      Call SHIB_HANDLER(deltat,R8_umeri,R_ekiri,R_ikiri,R_obiri
     &,R_ebiri,
     & L_ifiri,R_ubiri,R_ibiri,L_ofiri,R_uxeri,
     & R_abiri,R_efiri,R_ufiri,R_akiri,R_iferi,
     & R_afiri,R_eferi,REAL(R_aperi,4),C20_iperi,I_oteri,I_uveri
     &,
     & R_oleri,REAL(R_ameri,4),R_aleri,
     & REAL(R_ileri,4),I_ireri,I_ereri,I_uteri,I_iveri,C20_eperi
     &,
     & C20_operi,C8_uperi,L_akeri,R_uferi,
     & REAL(R_ekeri,4),L_okeri,R_ikeri,
     & REAL(R_ukeri,4),I_everi,I_overi,I_averi,I_iteri,L_emeri
     &,
     & L_exeri,L_ediri,L_uleri,L_eleri,
     & L_omeri,L_imeri,L_oxeri,L_areri,L_adiri,L_ureri,L_aseri
     &,
     & REAL(R8_oferi,8),REAL(1.0,4),R8_oreri,L_(148),L_oderi
     &,L_(147),L_ideri,
     & L_ixeri,I_axeri,L_idiri,L_aferi,L_udiri,L_uderi,L_eseri
     &,L_iseri,L_oseri,L_ateri,
     & L_eteri,L_useri)
      !}
C FDA_mechanic_vlv.fgi( 148, 915):���������� ������,20FDA61AB800
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_efupad,REAL(R_umupad
     &,4),
     & REAL(R_idarad,4),REAL(R_odarad,4),REAL(R_axupad,4)
     &,R_ofupad,REAL(R_uvupad,4),
     & L_(622),L_(621),R_afarad,R_efarad,R_ekarad,I_oxupad
     &,R_ifupad,
     & R_akarad,R_ebasad,I_uxupad,I_obarad,R_olupad,
     & REAL(R_amupad,4),R_ufupad,REAL(R_ekupad,4),L_edupad
     &,
     & L_adupad,I_arupad,I_upupad,I_ixupad,I_abarad,C20_apupad
     &,C20_epupad,C8_ipupad,
     & L_idupad,R_ikupad,REAL(R_ukupad,4),L_elupad,
     & R_alupad,REAL(R_ilupad,4),I_ibarad,L_okupad,
     & L_(620),L_opupad,L_odupad,I_ebarad,I_exupad,L_emupad
     &,L_adarad,L_ifarad,
     & L_ulupad,L_akupad,L_imupad,L_omupad,L_udarad,L_irupad
     &,
     & L_atupad,L_etupad,L_osupad,L_usupad,L_urupad,L_asupad
     &,L_itupad,L_otupad,L_utupad,L_avupad,
     & L_evupad,L_orupad,REAL(R8_axorad,8),REAL(1.0,4),R8_erupad
     &,L_edarad,I_ubarad,
     & L_afupad,L_udupad,L_ofarad,L_ufarad,L_esupad,L_ivupad
     &,L_ovupad,L_isupad)
      !}
C FDA_doz_logic.fgi(  79, 749):���������� ��������� ������� ����������� ���������,20FDA21AE704M8
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_utepad,REAL(R_idipad
     &,4),
     & REAL(R_atipad,4),REAL(R_etipad,4),REAL(R_opipad,4)
     &,R_evepad,REAL(R_ipipad,4),
     & L_(616),L_(615),R_otipad,R_utipad,R_uvipad,I_eripad
     &,R_avepad,
     & R_ovipad,R_uxurad,I_iripad,I_esipad,R_ebipad,
     & REAL(R_obipad,4),R_ivepad,REAL(R_uvepad,4),L_usepad
     &,
     & L_osepad,I_ofipad,I_ifipad,I_aripad,I_oripad,C20_odipad
     &,C20_udipad,C8_afipad,
     & L_atepad,R_axepad,REAL(R_ixepad,4),L_uxepad,
     & R_oxepad,REAL(R_abipad,4),I_asipad,L_exepad,
     & L_(614),L_efipad,L_etepad,I_uripad,I_upipad,L_ubipad
     &,L_osipad,L_avipad,
     & L_ibipad,L_ovepad,L_adipad,L_edipad,L_itipad,L_akipad
     &,
     & L_olipad,L_ulipad,L_elipad,L_ilipad,L_ikipad,L_okipad
     &,L_amipad,L_emipad,L_imipad,L_omipad,
     & L_umipad,L_ekipad,REAL(R8_axorad,8),REAL(1.0,4),R8_ufipad
     &,L_usipad,I_isipad,
     & L_otepad,L_itepad,L_evipad,L_ivipad,L_ukipad,L_apipad
     &,L_epipad,L_alipad)
      !}
C FDA_doz_logic.fgi( 117, 749):���������� ��������� ������� ����������� ���������,20FDA21AE704M6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_asirad,REAL(R_oxirad
     &,4),
     & REAL(R_erorad,4),REAL(R_irorad,4),REAL(R_ulorad,4)
     &,R_isirad,REAL(R_olorad,4),
     & L_(637),L_(636),R_urorad,R_asorad,R_atorad,I_imorad
     &,R_esirad,
     & R_usorad,R_obasad,I_omorad,I_iporad,R_ivirad,
     & REAL(R_uvirad,4),R_osirad,REAL(R_atirad,4),L_arirad
     &,
     & L_upirad,I_uborad,I_oborad,I_emorad,I_umorad,C20_uxirad
     &,C20_aborad,C8_eborad,
     & L_erirad,R_etirad,REAL(R_otirad,4),L_avirad,
     & R_utirad,REAL(R_evirad,4),I_eporad,L_itirad,
     & L_(635),L_iborad,L_irirad,I_aporad,I_amorad,L_axirad
     &,L_uporad,L_esorad,
     & L_ovirad,L_usirad,L_exirad,L_ixirad,L_ororad,L_edorad
     &,
     & L_uforad,L_akorad,L_iforad,L_oforad,L_odorad,L_udorad
     &,L_ekorad,L_ikorad,L_okorad,L_ukorad,
     & L_alorad,L_idorad,REAL(R8_axorad,8),REAL(1.0,4),R8_adorad
     &,L_arorad,I_oporad,
     & L_urirad,L_orirad,L_isorad,L_osorad,L_aforad,L_elorad
     &,L_ilorad,L_eforad)
      !}
C FDA_doz_logic.fgi(  38, 749):���������� ��������� ������� ����������� ���������,20FDA21AE704M4
      !{
      Call SHIB_HANDLER(deltat,R8_axuki,R_isali,R_osali,R_umali
     &,R_imali,
     & L_orali,R_apali,R_omali,L_urali,R_amali,
     & R_emali,R_irali,R_asali,R_esali,R_oruki,
     & R_erali,R_iruki,REAL(R_exuki,4),C20_oxuki,I_ufali,I_alali
     &,
     & R_utuki,REAL(R_evuki,4),R_etuki,
     & REAL(R_otuki,4),I_obali,I_ibali,I_akali,I_okali,C20_ixuki
     &,
     & C20_uxuki,C8_abali,L_esuki,R_asuki,
     & REAL(R_isuki,4),L_usuki,R_osuki,
     & REAL(R_atuki,4),I_ikali,I_ukali,I_ekali,I_ofali,L_ivuki
     &,
     & L_ilali,L_ipali,L_avuki,L_ituki,
     & L_uvuki,L_ovuki,L_ulali,L_ebali,L_epali,L_adali,L_edali
     &,
     & REAL(R8_uruki,8),REAL(1.0,4),R8_ubali,L_(120),L_upuki
     &,L_(119),L_opuki,
     & L_olali,I_elali,L_opali,L_eruki,L_arali,L_aruki,L_idali
     &,L_odali,L_udali,L_efali,
     & L_ifali,L_afali)
      !}
C FDA_mechanic_vlv.fgi( 370, 941):���������� ������,20FDA64AB806
      !{
      Call SHIB_HANDLER(deltat,R8_olumi,R_afapi,R_efapi,R_ixumi
     &,R_axumi,
     & L_edapi,R_oxumi,R_exumi,L_idapi,R_ovumi,
     & R_uvumi,R_adapi,R_odapi,R_udapi,R_edumi,
     & R_ubapi,R_adumi,REAL(R_ulumi,4),C20_emumi,I_isumi,I_otumi
     &,
     & R_ikumi,REAL(R_ukumi,4),R_ufumi,
     & REAL(R_ekumi,4),I_epumi,I_apumi,I_osumi,I_etumi,C20_amumi
     &,
     & C20_imumi,C8_omumi,L_udumi,R_odumi,
     & REAL(R_afumi,4),L_ifumi,R_efumi,
     & REAL(R_ofumi,4),I_atumi,I_itumi,I_usumi,I_esumi,L_alumi
     &,
     & L_avumi,L_abapi,L_okumi,L_akumi,
     & L_ilumi,L_elumi,L_ivumi,L_umumi,L_uxumi,L_opumi,L_upumi
     &,
     & REAL(R8_idumi,8),REAL(1.0,4),R8_ipumi,L_(136),L_ibumi
     &,L_(135),L_ebumi,
     & L_evumi,I_utumi,L_ebapi,L_ubumi,L_obapi,L_obumi,L_arumi
     &,L_erumi,L_irumi,L_urumi,
     & L_asumi,L_orumi)
      !}
C FDA_mechanic_vlv.fgi( 245, 941):���������� ������,20FDA64AB801
      !{
      Call SHIB_HANDLER(deltat,R8_upapi,R_elepi,R_ilepi,R_odepi
     &,R_edepi,
     & L_ikepi,R_udepi,R_idepi,L_okepi,R_ubepi,
     & R_adepi,R_ekepi,R_ukepi,R_alepi,R_ikapi,
     & R_akepi,R_ekapi,REAL(R_arapi,4),C20_irapi,I_ovapi,I_uxapi
     &,
     & R_omapi,REAL(R_apapi,4),R_amapi,
     & REAL(R_imapi,4),I_isapi,I_esapi,I_uvapi,I_ixapi,C20_erapi
     &,
     & C20_orapi,C8_urapi,L_alapi,R_ukapi,
     & REAL(R_elapi,4),L_olapi,R_ilapi,
     & REAL(R_ulapi,4),I_exapi,I_oxapi,I_axapi,I_ivapi,L_epapi
     &,
     & L_ebepi,L_efepi,L_umapi,L_emapi,
     & L_opapi,L_ipapi,L_obepi,L_asapi,L_afepi,L_usapi,L_atapi
     &,
     & REAL(R8_okapi,8),REAL(1.0,4),R8_osapi,L_(138),L_ofapi
     &,L_(137),L_ifapi,
     & L_ibepi,I_abepi,L_ifepi,L_akapi,L_ufepi,L_ufapi,L_etapi
     &,L_itapi,L_otapi,L_avapi,
     & L_evapi,L_utapi)
      !}
C FDA_mechanic_vlv.fgi( 229, 941):���������� ������,20FDA64AB800
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_avemo,REAL(R_udimo
     &,4),
     & REAL(R_itimo,4),REAL(R_otimo,4),REAL(R_arimo,4),R_ovemo
     &,REAL(R_upimo,4),
     & L_(369),L_(368),R_avimo,R_evimo,R_eximo,I_orimo,R_ivemo
     &,
     & R_aximo,R_evemo,I_urimo,I_osimo,R_obimo,
     & REAL(R_adimo,4),R_uvemo,REAL(R_exemo,4),L_atemo,
     & L_usemo,I_akimo,I_ufimo,I_irimo,I_asimo,C20_afimo,C20_efimo
     &,C8_ifimo,
     & L_etemo,R_ixemo,REAL(R_uxemo,4),L_ebimo,
     & R_abimo,REAL(R_ibimo,4),I_isimo,L_oxemo,
     & L_(367),L_ofimo,L_itemo,I_esimo,I_erimo,L_edimo,L_atimo
     &,L_ivimo,
     & L_ubimo,L_axemo,L_idimo,L_odimo,L_utimo,L_ikimo,
     & L_amimo,L_emimo,L_olimo,L_ulimo,L_ukimo,L_alimo,L_imimo
     &,L_omimo,L_umimo,L_apimo,
     & L_epimo,L_okimo,REAL(R8_axorad,8),REAL(1.0,4),R8_ekimo
     &,L_etimo,I_usimo,
     & L_utemo,L_otemo,L_ovimo,L_uvimo,L_elimo,L_ipimo,L_opimo
     &,L_ilimo)
      !}
C FDA_doz_logic.fgi( 365, 750):���������� ��������� ������� ����������� ���������,20FDA60CONTS01VS2
      R_opemo = R_evemo * R_adasad
C FDA_doz_logic.fgi( 331, 662):����������,1
      R_ipemo=R_ovemo
C FDA_doz_logic.fgi( 358, 671):������,20FDA60CONTS01VS21
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ekumo,REAL(R_arumo
     &,4),
     & REAL(R_ofapo,4),REAL(R_ufapo,4),REAL(R_ebapo,4),R_ukumo
     &,REAL(R_abapo,4),
     & L_(375),L_(374),R_ekapo,R_ikapo,R_ilapo,I_ubapo,R_okumo
     &,
     & R_elapo,R_ikumo,I_adapo,I_udapo,R_umumo,
     & REAL(R_epumo,4),R_alumo,REAL(R_ilumo,4),L_efumo,
     & L_afumo,I_esumo,I_asumo,I_obapo,I_edapo,C20_erumo,C20_irumo
     &,C8_orumo,
     & L_ifumo,R_olumo,REAL(R_amumo,4),L_imumo,
     & R_emumo,REAL(R_omumo,4),I_odapo,L_ulumo,
     & L_(373),L_urumo,L_ofumo,I_idapo,I_ibapo,L_ipumo,L_efapo
     &,L_okapo,
     & L_apumo,L_elumo,L_opumo,L_upumo,L_akapo,L_osumo,
     & L_evumo,L_ivumo,L_utumo,L_avumo,L_atumo,L_etumo,L_ovumo
     &,L_uvumo,L_axumo,L_exumo,
     & L_ixumo,L_usumo,REAL(R8_axorad,8),REAL(1.0,4),R8_isumo
     &,L_ifapo,I_afapo,
     & L_akumo,L_ufumo,L_ukapo,L_alapo,L_itumo,L_oxumo,L_uxumo
     &,L_otumo)
      !}
C FDA_doz_logic.fgi( 326, 750):���������� ��������� ������� ����������� ���������,20FDA60CONTS01VS1
      R_usolo = R_ikumo * R_adasad
C FDA_doz_logic.fgi( 331, 693):����������,1
      R_osolo=R_ukumo
C FDA_doz_logic.fgi( 358, 701):������,20FDA60CONTS01VS11
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ubamo,REAL(R_elamo
     &,4),
     & REAL(R_uxamo,4),REAL(R_abemo,4),REAL(R_itamo,4),R_osemo
     &,REAL(R_etamo,4),
     & L_(345),L_(344),R_ibemo,R_obemo,R_odemo,I_avamo,R_adamo
     &,
     & R_idemo,R_esemo,I_evamo,I_axamo,R_akamo,
     & REAL(R_ikamo,4),R_edamo,REAL(R_odamo,4),L_uxulo,
     & L_oxulo,I_imamo,I_emamo,I_utamo,I_ivamo,C20_ilamo,C20_olamo
     &,C8_ulamo,
     & L_abamo,R_udamo,REAL(R_efamo,4),L_ofamo,
     & R_ifamo,REAL(R_ufamo,4),I_uvamo,L_afamo,
     & L_(343),L_amamo,L_ebamo,I_ovamo,I_otamo,L_okamo,L_ixamo
     &,L_ubemo,
     & L_ekamo,L_idamo,L_ukamo,L_alamo,L_ebemo,L_umamo,
     & L_iramo,L_oramo,L_aramo,L_eramo,L_epamo,L_ipamo,L_uramo
     &,L_asamo,L_esamo,L_isamo,
     & L_osamo,L_apamo,REAL(R8_axorad,8),REAL(1.0,4),R8_omamo
     &,L_oxamo,I_examo,
     & L_obamo,L_ibamo,L_ademo,L_edemo,L_opamo,L_usamo,L_atamo
     &,L_upamo)
      !}
C FDA_doz_logic.fgi( 561, 749):���������� ��������� ������� ����������� ���������,20FDA21AB001VZ
      R_isemo = R_esemo * R_adasad
C FDA_doz_logic.fgi( 583, 692):����������,1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ovolo,REAL(R_afulo
     &,4),
     & REAL(R_otulo,4),REAL(R_utulo,4),REAL(R_erulo,4),R_iremo
     &,REAL(R_arulo,4),
     & L_(342),L_(341),R_evulo,R_ivulo,R_ixulo,I_urulo,R_uvolo
     &,
     & R_exulo,R_aremo,I_asulo,I_usulo,R_ubulo,
     & REAL(R_edulo,4),R_axolo,REAL(R_ixolo,4),L_otolo,
     & L_itolo,I_ekulo,I_akulo,I_orulo,I_esulo,C20_efulo,C20_ifulo
     &,C8_ofulo,
     & L_utolo,R_oxolo,REAL(R_abulo,4),L_ibulo,
     & R_ebulo,REAL(R_obulo,4),I_osulo,L_uxolo,
     & L_(340),L_ufulo,L_avolo,I_isulo,I_irulo,L_idulo,L_etulo
     &,L_ovulo,
     & L_adulo,L_exolo,L_odulo,L_udulo,L_avulo,L_okulo,
     & L_emulo,L_imulo,L_ululo,L_amulo,L_alulo,L_elulo,L_omulo
     &,L_umulo,L_apulo,L_epulo,
     & L_ipulo,L_ukulo,REAL(R8_axorad,8),REAL(1.0,4),R8_ikulo
     &,L_itulo,I_atulo,
     & L_ivolo,L_evolo,L_uvulo,L_axulo,L_ilulo,L_opulo,L_upulo
     &,L_olulo)
      !}
C FDA_doz_logic.fgi( 582, 749):���������� ��������� ������� ����������� ���������,20FDA21AB001VS
      R_eremo = R_aremo * R_adasad
C FDA_doz_logic.fgi( 583, 676):����������,1
      R_upemo=R_iremo
C FDA_doz_logic.fgi( 612, 685):������,20FDA21AB001VS01
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_osamad,REAL(R_ebemad
     &,4),
     & REAL(R_uremad,4),REAL(R_asemad,4),REAL(R_imemad,4)
     &,R_atamad,REAL(R_ememad,4),
     & L_(575),L_(574),R_isemad,R_osemad,R_otemad,I_apemad
     &,R_usamad,
     & R_itemad,R_ixemad,I_epemad,I_aremad,R_axamad,
     & REAL(R_ixamad,4),R_etamad,REAL(R_otamad,4),L_oramad
     &,
     & L_iramad,I_idemad,I_edemad,I_umemad,I_ipemad,C20_ibemad
     &,C20_obemad,C8_ubemad,
     & L_uramad,R_utamad,REAL(R_evamad,4),L_ovamad,
     & R_ivamad,REAL(R_uvamad,4),I_upemad,L_avamad,
     & L_(573),L_ademad,L_asamad,I_opemad,I_omemad,L_oxamad
     &,L_iremad,L_usemad,
     & L_examad,L_itamad,L_uxamad,L_abemad,L_esemad,L_udemad
     &,
     & L_ikemad,L_okemad,L_akemad,L_ekemad,L_efemad,L_ifemad
     &,L_ukemad,L_alemad,L_elemad,L_ilemad,
     & L_olemad,L_afemad,REAL(R8_axorad,8),REAL(1.0,4),R8_odemad
     &,L_oremad,I_eremad,
     & L_isamad,L_esamad,L_atemad,L_etemad,L_ofemad,L_ulemad
     &,L_amemad,L_ufemad)
      !}
C FDA_doz_logic.fgi( 396, 750):���������� ��������� ������� ����������� ���������,20FDA21AB002
      R_eramad = R_ixemad * R_adasad
C FDA_doz_logic.fgi( 415, 701):����������,1
      R_otebo=R_atamad
C FDA_doz_logic.fgi( 444, 708):������,20FDA21AB002VZ01
      R0_itebo=(R_otebo-R_(67)+R0_atebo*R0_itebo)/(R0_atebo
     &+deltat)
      R0_etebo=R_otebo
C FDA_doz_logic.fgi( 412, 719):���������������� ����� 
      L_avebo=R0_itebo.gt.R0_usebo
C FDA_doz_logic.fgi( 425, 721):���������� >
      L_utebo=R0_itebo.lt.R0_osebo
C FDA_doz_logic.fgi( 425, 714):���������� <
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_oxaki,4),R_udiki
     &,R_afiki,
     & R_ireki,REAL(R_ovaki,4),R_oreki,R_ureki,
     & R_aseki,R_eseki,L_exeki,REAL(R_oxaki,4),
     & R_ateki,L_odiki,R_iseki,
     & L_adiki,L_uxeki,L_abiki,REAL(R_ipaki,4),
     & R_oseki,L_ediki,R_useki,
     & L_idiki,REAL(R_oraki,4),REAL(R_iraki,4),L_aveki,
     & L_iveki,L_eveki,L_axaki,L_oveki,
     & L_uveki,L_axeki,L_uvaki,R_okeki,
     & L_oteki,L_uteki,L_eteki,
     & L_iteki,L_imeki,L_ipeki,C20_esaki,L_epaki,
     & L_epeki,L_ixeki,L_exaki,L_ideki,L_odeki,L_udeki,L_afeki
     &,L_ofeki,
     & L_uxaki,L_efeki,L_ifeki,L_ebeki,L_abeki,L_ibeki,L_ubeki
     &,L_adeki,L_edeki,L_(114),
     & L_(113),REAL(R_asaki,4),R_areki,R_ereki,R_obiki,R_ubiki
     &,
     & I_eleki,R_okaki,L_ekeki,R_ikaki,C20_isaki,C20_osaki
     &,I_oleki,I_umeki,
     & R_omaki,REAL(R_apaki,4),R_ukaki,
     & REAL(R_elaki,4),L_ufaki,L_ofaki,I_utaki,I_otaki,I_aleki
     &,I_ameki,
     & C20_usaki,C8_ataki,R_ilaki,REAL(R_ulaki,4),
     & L_emaki,R_amaki,REAL(R_imaki,4),I_omeki,
     & L_(112),L_etaki,L_olaki,I_emeki,I_ukeki,L_umaki,
     & L_alaki,L_ufeki,L_akeki,L_upaki,L_araki,L_upeki,L_ebiki
     &,
     & L_itaki,L_ixaki,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_ikeki,L_opeki,
     & I_apeki,L_ekaki,L_akaki,L_oxeki,L_ibiki,L_obeki)
      !}
C FDA_mechanic_vlv.fgi( 416, 942):���������� ���� OY,20FDA61AE406
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_ufise,4),R_amose
     &,R_emose,
     & R_ovise,REAL(R_udise,4),R_uvise,R_axise,
     & R_exise,R_ixise,L_ifose,REAL(R_ufise,4),
     & R_ebose,L_ulose,R_oxise,
     & L_elose,L_akose,L_ekose,REAL(R_otese,4),
     & R_uxise,L_ilose,R_abose,
     & L_olose,REAL(R_uvese,4),REAL(R_ovese,4),L_edose,
     & L_odose,L_idose,L_efise,L_udose,
     & L_afose,L_efose,L_afise,R_upise,
     & L_ubose,L_adose,L_ibose,
     & L_obose,L_osise,L_otise,C20_ixese,L_itese,
     & L_itise,L_ofose,L_ifise,L_olise,L_ulise,L_amise,L_emise
     &,L_umise,
     & L_akise,L_imise,L_omise,L_ikise,L_ekise,L_okise,L_alise
     &,L_elise,L_ilise,L_(71),
     & L_(70),REAL(R_exese,4),R_evise,R_ivise,R_ukose,R_alose
     &,
     & I_irise,R_upese,L_ipise,R_opese,C20_oxese,C20_uxese
     &,I_urise,I_atise,
     & R_usese,REAL(R_etese,4),R_arese,
     & REAL(R_irese,4),L_apese,L_umese,I_adise,I_ubise,I_erise
     &,I_esise,
     & C20_abise,C8_ebise,R_orese,REAL(R_asese,4),
     & L_isese,R_esese,REAL(R_osese,4),I_usise,
     & L_(69),L_ibise,L_urese,I_isise,I_arise,L_atese,
     & L_erese,L_apise,L_epise,L_avese,L_evese,L_avise,L_ikose
     &,
     & L_obise,L_ofise,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_opise,L_utise,
     & I_etise,L_ipese,L_epese,L_ufose,L_okose,L_ukise)
      !}
C FDA_mechanic_vlv.fgi( 708, 974):���������� ���� OY,20FDA61AE401
      !{
      Call SHIB_HANDLER(deltat,R8_ikili,R_uboli,R_adoli,R_evili
     &,R_utili,
     & L_aboli,R_ivili,R_avili,L_eboli,R_itili,
     & R_otili,R_uxili,R_iboli,R_oboli,R_abili,
     & R_oxili,R_uxeli,REAL(R_okili,4),C20_alili,I_erili,I_isili
     &,
     & R_efili,REAL(R_ofili,4),R_odili,
     & REAL(R_afili,4),I_amili,I_ulili,I_irili,I_asili,C20_ukili
     &,
     & C20_elili,C8_ilili,L_obili,R_ibili,
     & REAL(R_ubili,4),L_edili,R_adili,
     & REAL(R_idili,4),I_urili,I_esili,I_orili,I_arili,L_ufili
     &,
     & L_usili,L_uvili,L_ifili,L_udili,
     & L_ekili,L_akili,L_etili,L_olili,L_ovili,L_imili,L_omili
     &,
     & REAL(R8_ebili,8),REAL(1.0,4),R8_emili,L_(124),L_exeli
     &,L_(123),L_axeli,
     & L_atili,I_osili,L_axili,L_oxeli,L_ixili,L_ixeli,L_umili
     &,L_apili,L_epili,L_opili,
     & L_upili,L_ipili)
      !}
C FDA_mechanic_vlv.fgi( 340, 941):���������� ������,20FDA62AB806
      !{
      Call SHIB_HANDLER(deltat,R8_ikode,R_ubude,R_adude,R_evode
     &,R_utode,
     & L_abude,R_ivode,R_avode,L_ebude,R_itode,
     & R_otode,R_uxode,R_ibude,R_obude,R_abode,
     & R_oxode,R_uxide,REAL(R_okode,4),C20_alode,I_erode,I_isode
     &,
     & R_efode,REAL(R_ofode,4),R_odode,
     & REAL(R_afode,4),I_amode,I_ulode,I_irode,I_asode,C20_ukode
     &,
     & C20_elode,C8_ilode,L_obode,R_ibode,
     & REAL(R_ubode,4),L_edode,R_adode,
     & REAL(R_idode,4),I_urode,I_esode,I_orode,I_arode,L_ufode
     &,
     & L_usode,L_uvode,L_ifode,L_udode,
     & L_ekode,L_akode,L_etode,L_olode,L_ovode,L_imode,L_omode
     &,
     & REAL(R8_ebode,8),REAL(1.0,4),R8_emode,L_(20),L_exide
     &,L_(19),L_axide,
     & L_atode,I_osode,L_axode,L_oxide,L_ixode,L_ixide,L_umode
     &,L_apode,L_epode,L_opode,
     & L_upode,L_ipode)
      !}
C FDA_mechanic_vlv.fgi( 196, 915):���������� ������,20FDA62AB801
      !{
      Call SHIB_HANDLER(deltat,R8_idupi,R_uvupi,R_axupi,R_esupi
     &,R_urupi,
     & L_avupi,R_isupi,R_asupi,L_evupi,R_irupi,
     & R_orupi,R_utupi,R_ivupi,R_ovupi,R_avopi,
     & R_otupi,R_utopi,REAL(R_odupi,4),C20_afupi,I_emupi,I_ipupi
     &,
     & R_ebupi,REAL(R_obupi,4),R_oxopi,
     & REAL(R_abupi,4),I_akupi,I_ufupi,I_imupi,I_apupi,C20_udupi
     &,
     & C20_efupi,C8_ifupi,L_ovopi,R_ivopi,
     & REAL(R_uvopi,4),L_exopi,R_axopi,
     & REAL(R_ixopi,4),I_umupi,I_epupi,I_omupi,I_amupi,L_ubupi
     &,
     & L_upupi,L_usupi,L_ibupi,L_uxopi,
     & L_edupi,L_adupi,L_erupi,L_ofupi,L_osupi,L_ikupi,L_okupi
     &,
     & REAL(R8_evopi,8),REAL(1.0,4),R8_ekupi,L_(144),L_etopi
     &,L_(143),L_atopi,
     & L_arupi,I_opupi,L_atupi,L_otopi,L_itupi,L_itopi,L_ukupi
     &,L_alupi,L_elupi,L_olupi,
     & L_ulupi,L_ilupi)
      !}
C FDA_mechanic_vlv.fgi( 180, 915):���������� ������,20FDA62AB800
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_ubafi,4),R_akefi
     &,R_ekefi,
     & R_osafi,REAL(R_uxudi,4),R_usafi,R_atafi,
     & R_etafi,R_itafi,L_ibefi,REAL(R_ubafi,4),
     & R_evafi,L_ufefi,R_otafi,
     & L_efefi,L_adefi,L_edefi,REAL(R_orudi,4),
     & R_utafi,L_ifefi,R_avafi,
     & L_ofefi,REAL(R_usudi,4),REAL(R_osudi,4),L_exafi,
     & L_oxafi,L_ixafi,L_ebafi,L_uxafi,
     & L_abefi,L_ebefi,L_abafi,R_ulafi,
     & L_uvafi,L_axafi,L_ivafi,
     & L_ovafi,L_opafi,L_orafi,C20_itudi,L_irudi,
     & L_irafi,L_obefi,L_ibafi,L_ofafi,L_ufafi,L_akafi,L_ekafi
     &,L_ukafi,
     & L_adafi,L_ikafi,L_okafi,L_idafi,L_edafi,L_odafi,L_afafi
     &,L_efafi,L_ifafi,L_(105),
     & L_(104),REAL(R_etudi,4),R_esafi,R_isafi,R_udefi,R_afefi
     &,
     & I_imafi,R_uludi,L_ilafi,R_oludi,C20_otudi,C20_utudi
     &,I_umafi,I_arafi,
     & R_upudi,REAL(R_erudi,4),R_amudi,
     & REAL(R_imudi,4),L_aludi,L_ukudi,I_axudi,I_uvudi,I_emafi
     &,I_epafi,
     & C20_avudi,C8_evudi,R_omudi,REAL(R_apudi,4),
     & L_ipudi,R_epudi,REAL(R_opudi,4),I_upafi,
     & L_(103),L_ivudi,L_umudi,I_ipafi,I_amafi,L_arudi,
     & L_emudi,L_alafi,L_elafi,L_asudi,L_esudi,L_asafi,L_idefi
     &,
     & L_ovudi,L_obafi,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_olafi,L_urafi,
     & I_erafi,L_iludi,L_eludi,L_ubefi,L_odefi,L_udafi)
      !}
C FDA_mechanic_vlv.fgi( 461, 942):���������� ���� OY,20FDA64AE406
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_alere,4),R_epire
     &,R_ipire,
     & R_uxere,REAL(R_akere,4),R_abire,R_ebire,
     & R_ibire,R_obire,L_okire,REAL(R_alere,4),
     & R_idire,L_apire,R_ubire,
     & L_imire,L_elire,L_ilire,REAL(R_uvare,4),
     & R_adire,L_omire,R_edire,
     & L_umire,REAL(R_abere,4),REAL(R_uxare,4),L_ifire,
     & L_ufire,L_ofire,L_ikere,L_akire,
     & L_ekire,L_ikire,L_ekere,R_asere,
     & L_afire,L_efire,L_odire,
     & L_udire,L_utere,L_uvere,C20_obere,L_ovare,
     & L_overe,L_ukire,L_okere,L_umere,L_apere,L_epere,L_ipere
     &,L_arere,
     & L_elere,L_opere,L_upere,L_olere,L_ilere,L_ulere,L_emere
     &,L_imere,L_omere,L_(62),
     & L_(61),REAL(R_ibere,4),R_ixere,R_oxere,R_amire,R_emire
     &,
     & I_osere,R_asare,L_orere,R_urare,C20_ubere,C20_adere
     &,I_atere,I_evere,
     & R_avare,REAL(R_ivare,4),R_esare,
     & REAL(R_osare,4),L_erare,L_arare,I_efere,I_afere,I_isere
     &,I_itere,
     & C20_edere,C8_idere,R_usare,REAL(R_etare,4),
     & L_otare,R_itare,REAL(R_utare,4),I_avere,
     & L_(60),L_odere,L_atare,I_otere,I_esere,L_evare,
     & L_isare,L_erere,L_irere,L_exare,L_ixare,L_exere,L_olire
     &,
     & L_udere,L_ukere,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_urere,L_axere,
     & I_ivere,L_orare,L_irare,L_alire,L_ulire,L_amere)
      !}
C FDA_mechanic_vlv.fgi( 744, 974):���������� ���� OY,20FDA64AE401
      !{
      Call SHIB_HANDLER(deltat,R8_usoki,R_epuki,R_ipuki,R_okuki
     &,R_ekuki,
     & L_imuki,R_ukuki,R_ikuki,L_omuki,R_ufuki,
     & R_akuki,R_emuki,R_umuki,R_apuki,R_imoki,
     & R_amuki,R_emoki,REAL(R_atoki,4),C20_itoki,I_obuki,I_uduki
     &,
     & R_oroki,REAL(R_asoki,4),R_aroki,
     & REAL(R_iroki,4),I_ivoki,I_evoki,I_ubuki,I_iduki,C20_etoki
     &,
     & C20_otoki,C8_utoki,L_apoki,R_umoki,
     & REAL(R_epoki,4),L_opoki,R_ipoki,
     & REAL(R_upoki,4),I_eduki,I_oduki,I_aduki,I_ibuki,L_esoki
     &,
     & L_efuki,L_eluki,L_uroki,L_eroki,
     & L_osoki,L_isoki,L_ofuki,L_avoki,L_aluki,L_uvoki,L_axoki
     &,
     & REAL(R8_omoki,8),REAL(1.0,4),R8_ovoki,L_(118),L_oloki
     &,L_(117),L_iloki,
     & L_ifuki,I_afuki,L_iluki,L_amoki,L_uluki,L_uloki,L_exoki
     &,L_ixoki,L_oxoki,L_abuki,
     & L_ebuki,L_uxoki)
      !}
C FDA_mechanic_vlv.fgi( 384, 941):���������� ������,20FDA65AB806
      !{
      Call SHIB_HANDLER(deltat,R8_ebimi,R_otimi,R_utimi,R_arimi
     &,R_opimi,
     & L_usimi,R_erimi,R_upimi,L_atimi,R_epimi,
     & R_ipimi,R_osimi,R_etimi,R_itimi,R_usemi,
     & R_isimi,R_osemi,REAL(R_ibimi,4),C20_ubimi,I_alimi,I_emimi
     &,
     & R_axemi,REAL(R_ixemi,4),R_ivemi,
     & REAL(R_uvemi,4),I_udimi,I_odimi,I_elimi,I_ulimi,C20_obimi
     &,
     & C20_adimi,C8_edimi,L_itemi,R_etemi,
     & REAL(R_otemi,4),L_avemi,R_utemi,
     & REAL(R_evemi,4),I_olimi,I_amimi,I_ilimi,I_ukimi,L_oxemi
     &,
     & L_omimi,L_orimi,L_exemi,L_ovemi,
     & L_abimi,L_uxemi,L_apimi,L_idimi,L_irimi,L_efimi,L_ifimi
     &,
     & REAL(R8_atemi,8),REAL(1.0,4),R8_afimi,L_(132),L_asemi
     &,L_(131),L_uremi,
     & L_umimi,I_imimi,L_urimi,L_isemi,L_esimi,L_esemi,L_ofimi
     &,L_ufimi,L_akimi,L_ikimi,
     & L_okimi,L_ekimi)
      !}
C FDA_mechanic_vlv.fgi( 278, 941):���������� ������,20FDA65AB801
      !{
      Call SHIB_HANDLER(deltat,R8_ifomi,R_uxomi,R_abumi,R_etomi
     &,R_usomi,
     & L_axomi,R_itomi,R_atomi,L_exomi,R_isomi,
     & R_osomi,R_uvomi,R_ixomi,R_oxomi,R_aximi,
     & R_ovomi,R_uvimi,REAL(R_ofomi,4),C20_akomi,I_epomi,I_iromi
     &,
     & R_edomi,REAL(R_odomi,4),R_obomi,
     & REAL(R_adomi,4),I_alomi,I_ukomi,I_ipomi,I_aromi,C20_ufomi
     &,
     & C20_ekomi,C8_ikomi,L_oximi,R_iximi,
     & REAL(R_uximi,4),L_ebomi,R_abomi,
     & REAL(R_ibomi,4),I_upomi,I_eromi,I_opomi,I_apomi,L_udomi
     &,
     & L_uromi,L_utomi,L_idomi,L_ubomi,
     & L_efomi,L_afomi,L_esomi,L_okomi,L_otomi,L_ilomi,L_olomi
     &,
     & REAL(R8_eximi,8),REAL(1.0,4),R8_elomi,L_(134),L_evimi
     &,L_(133),L_avimi,
     & L_asomi,I_oromi,L_avomi,L_ovimi,L_ivomi,L_ivimi,L_ulomi
     &,L_amomi,L_emomi,L_omomi,
     & L_umomi,L_imomi)
      !}
C FDA_mechanic_vlv.fgi( 262, 941):���������� ������,20FDA65AB800
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_akome,4),REAL(R_adasad
     &,4),R_upume,
     & R_arume,R_ivome,REAL(R_udome,4),R_ovome,R_uvome,
     & R_axome,R_exome,L_ikume,REAL(R_akome,4),
     & R_edume,L_opume,R_obume,
     & L_apume,L_ilume,L_olume,REAL(R_ovime,4),
     & R_ubume,L_epume,R_adume,
     & L_ipume,REAL(R_ixime,4),REAL(R_exime,4),L_efume,
     & L_ofume,L_ifume,L_ifome,L_ufume,
     & L_akume,L_ekume,L_efome,R_arome,
     & L_udume,L_afume,L_idume,
     & L_odume,L_isome,L_itome,C20_uxime,L_ivime,
     & L_etome,L_ukume,L_ofome,L_ulome,L_amome,L_emome,L_imome
     &,L_apome,
     & L_ekome,L_omome,L_umome,L_okome,L_ikome,L_ukome,L_elome
     &,L_ilome,L_olome,L_(50),
     & L_(49),REAL(R_oxime,4),R_avome,R_evome,R_emume,R_imume
     &,
     & I_orome,R_orime,R_irime,L_opome,C20_abome,C20_ebome
     &,I_urome,I_usome,
     & R_utime,REAL(R_evime,4),R_asime,
     & REAL(R_isime,4),L_upime,L_opime,I_idome,I_edome,I_irome
     &,I_asome,
     & C20_ibome,C8_obome,R_osime,REAL(R_atime,4),
     & L_itime,R_etime,REAL(R_otime,4),I_osome,
     & L_(48),L_ubome,L_usime,I_esome,I_erome,L_avime,
     & L_esime,L_epome,L_ipome,L_uvime,L_axime,L_utome,L_ulume
     &,
     & L_adome,L_ufome,REAL(R8_urime,8),REAL(1.0,4),R8_odome
     &,L_upome,L_otome,
     & I_atome,L_erime,L_arime,L_elume,L_amume,L_alome)
      !}
C FDA_mechanic_vlv.fgi( 646, 974):���������� ���������,20FDA62AE500
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_edile,4),REAL(R_adasad
     &,4),R_amole,
     & R_emole,R_osile,REAL(R_abile,4),R_usile,R_atile,
     & R_etile,R_itile,L_odole,REAL(R_edile,4),
     & R_ixile,L_ulole,R_uvile,
     & L_elole,L_ofole,L_ufole,REAL(R_usele,4),
     & R_axile,L_ilole,R_exile,
     & L_olole,REAL(R_otele,4),REAL(R_itele,4),L_ibole,
     & L_ubole,L_obole,L_obile,L_adole,
     & L_edole,L_idole,L_ibile,R_emile,
     & L_abole,L_ebole,L_oxile,
     & L_uxile,L_opile,L_orile,C20_avele,L_osele,
     & L_irile,L_afole,L_ubile,L_akile,L_ekile,L_ikile,L_okile
     &,L_elile,
     & L_idile,L_ukile,L_alile,L_udile,L_odile,L_afile,L_ifile
     &,L_ofile,L_ufile,L_(41),
     & L_(40),REAL(R_utele,4),R_esile,R_isile,R_ikole,R_okole
     &,
     & I_umile,R_umele,R_omele,L_ulile,C20_evele,C20_ivele
     &,I_apile,I_arile,
     & R_asele,REAL(R_isele,4),R_epele,
     & REAL(R_opele,4),L_amele,L_ulele,I_oxele,I_ixele,I_omile
     &,I_epile,
     & C20_ovele,C8_uvele,R_upele,REAL(R_erele,4),
     & L_orele,R_irele,REAL(R_urele,4),I_upile,
     & L_(39),L_axele,L_arele,I_ipile,I_imile,L_esele,
     & L_ipele,L_ilile,L_olile,L_atele,L_etele,L_asile,L_akole
     &,
     & L_exele,L_adile,REAL(R8_apele,8),REAL(1.0,4),R8_uxele
     &,L_amile,L_urile,
     & I_erile,L_imele,L_emele,L_ifole,L_ekole,L_efile)
      !}
C FDA_mechanic_vlv.fgi( 682, 974):���������� ���������,20FDA65AE500
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_uxolad,REAL(R_okulad
     &,4),
     & REAL(R_exulad,4),REAL(R_ixulad,4),REAL(R_usulad,4)
     &,R_ibulad,REAL(R_osulad,4),
     & L_(556),L_(555),R_uxulad,R_abamad,R_adamad,I_itulad
     &,R_ebulad,
     & R_ubamad,R_abulad,I_otulad,I_ivulad,R_ifulad,
     & REAL(R_ufulad,4),R_obulad,REAL(R_adulad,4),L_uvolad
     &,
     & L_ovolad,I_ululad,I_olulad,I_etulad,I_utulad,C20_ukulad
     &,C20_alulad,C8_elulad,
     & L_axolad,R_edulad,REAL(R_odulad,4),L_afulad,
     & R_udulad,REAL(R_efulad,4),I_evulad,L_idulad,
     & L_(554),L_ilulad,L_exolad,I_avulad,I_atulad,L_akulad
     &,L_uvulad,L_ebamad,
     & L_ofulad,L_ubulad,L_ekulad,L_ikulad,L_oxulad,L_emulad
     &,
     & L_upulad,L_arulad,L_ipulad,L_opulad,L_omulad,L_umulad
     &,L_erulad,L_irulad,L_orulad,L_urulad,
     & L_asulad,L_imulad,REAL(R8_axorad,8),REAL(1.0,4),R8_amulad
     &,L_axulad,I_ovulad,
     & L_oxolad,L_ixolad,L_ibamad,L_obamad,L_apulad,L_esulad
     &,L_isulad,L_epulad)
      !}
C FDA_doz_logic.fgi( 471, 749):���������� ��������� ������� ����������� ���������,20FDA60TELEZ
      R_ekolad=R_ibulad
C FDA_doz_logic.fgi( 528, 719):������,20FDA60TELEZHKAVX01
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_upede,4),REAL(R_adasad
     &,4),R_ovide,
     & R_uvide,R_efide,REAL(R_omede,4),R_ifide,R_ofide,
     & R_ufide,R_akide,L_eride,REAL(R_upede,4),
     & R_amide,L_ivide,R_ilide,
     & L_utide,L_eside,L_iside,REAL(R_ifede,4),
     & R_olide,L_avide,R_ulide,
     & L_evide,REAL(R_ekede,4),REAL(R_akede,4),L_apide,
     & L_ipide,L_epide,L_epede,L_opide,
     & L_upide,L_aride,L_apede,R_uvede,
     & L_omide,L_umide,L_emide,
     & L_imide,L_ebide,L_edide,C20_okede,L_efede,
     & L_adide,L_oride,L_ipede,L_osede,L_usede,L_atede,L_etede
     &,L_utede,
     & L_arede,L_itede,L_otede,L_irede,L_erede,L_orede,L_asede
     &,L_esede,L_isede,L_(18),
     & L_(17),REAL(R_ikede,4),R_udide,R_afide,R_atide,R_etide
     &,
     & I_ixede,R_ixade,R_exade,L_ivede,C20_ukede,C20_alede
     &,I_oxede,I_obide,
     & R_odede,REAL(R_afede,4),R_uxade,
     & REAL(R_ebede,4),L_ovade,L_ivade,I_emede,I_amede,I_exede
     &,I_uxede,
     & C20_elede,C8_ilede,R_ibede,REAL(R_ubede,4),
     & L_edede,R_adede,REAL(R_idede,4),I_ibide,
     & L_(16),L_olede,L_obede,I_abide,I_axede,L_udede,
     & L_abede,L_avede,L_evede,L_ofede,L_ufede,L_odide,L_oside
     &,
     & L_ulede,L_opede,REAL(R8_oxade,8),REAL(1.0,4),R8_imede
     &,L_ovede,L_idide,
     & I_ubide,L_axade,L_uvade,L_aside,L_uside,L_urede)
      !}
C FDA_mechanic_vlv.fgi(  33, 942):���������� ���������,20FDA60AE201
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_orumad,REAL(R_exumad
     &,4),
     & REAL(R_upapad,4),REAL(R_arapad,4),REAL(R_ilapad,4)
     &,R_asumad,REAL(R_elapad,4),
     & L_(613),L_(612),R_irapad,R_orapad,R_osapad,I_amapad
     &,R_urumad,
     & R_isapad,R_otapad,I_emapad,I_apapad,R_avumad,
     & REAL(R_ivumad,4),R_esumad,REAL(R_osumad,4),L_opumad
     &,
     & L_ipumad,I_ibapad,I_ebapad,I_ulapad,I_imapad,C20_ixumad
     &,C20_oxumad,C8_uxumad,
     & L_upumad,R_usumad,REAL(R_etumad,4),L_otumad,
     & R_itumad,REAL(R_utumad,4),I_umapad,L_atumad,
     & L_(611),L_abapad,L_arumad,I_omapad,I_olapad,L_ovumad
     &,L_ipapad,L_urapad,
     & L_evumad,L_isumad,L_uvumad,L_axumad,L_erapad,L_ubapad
     &,
     & L_ifapad,L_ofapad,L_afapad,L_efapad,L_edapad,L_idapad
     &,L_ufapad,L_akapad,L_ekapad,L_ikapad,
     & L_okapad,L_adapad,REAL(R8_axorad,8),REAL(1.0,4),R8_obapad
     &,L_opapad,I_epapad,
     & L_irumad,L_erumad,L_asapad,L_esapad,L_odapad,L_ukapad
     &,L_alapad,L_udapad)
      !}
C FDA_doz_logic.fgi( 203, 748):���������� ��������� ������� ����������� ���������,20FDA21AE702
      L_eruxi=L_arumad
C FDA_doz_logic.fgi( 258, 727):������,20FDA21AE702YB12
      R_alumad = R_otapad * R_adasad
C FDA_doz_logic.fgi( 230, 716):����������,1
      R_isuxi=R_asumad
C FDA_doz_logic.fgi( 258, 723):������,20FDA21AE702VS01
      R0_esuxi=(R_isuxi-R_(16)+R0_uruxi*R0_esuxi)/(R0_uruxi
     &+deltat)
      R0_asuxi=R_isuxi
C FDA_doz_logic.fgi( 227, 704):���������������� ����� 
      L_usuxi=R0_esuxi.gt.R0_oruxi
C FDA_doz_logic.fgi( 240, 710):���������� >
      L_osuxi=R0_esuxi.lt.R0_iruxi
C FDA_doz_logic.fgi( 240, 704):���������� <
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_arimad,REAL(R_ovimad
     &,4),
     & REAL(R_epomad,4),REAL(R_ipomad,4),REAL(R_ukomad,4)
     &,R_irimad,REAL(R_okomad,4),
     & L_(594),L_(593),R_upomad,R_aromad,R_asomad,I_ilomad
     &,R_erimad,
     & R_uromad,R_atomad,I_olomad,I_imomad,R_itimad,
     & REAL(R_utimad,4),R_orimad,REAL(R_asimad,4),L_apimad
     &,
     & L_umimad,I_uximad,I_oximad,I_elomad,I_ulomad,C20_uvimad
     &,C20_aximad,C8_eximad,
     & L_epimad,R_esimad,REAL(R_osimad,4),L_atimad,
     & R_usimad,REAL(R_etimad,4),I_emomad,L_isimad,
     & L_(592),L_iximad,L_ipimad,I_amomad,I_alomad,L_avimad
     &,L_umomad,L_eromad,
     & L_otimad,L_urimad,L_evimad,L_ivimad,L_opomad,L_ebomad
     &,
     & L_udomad,L_afomad,L_idomad,L_odomad,L_obomad,L_ubomad
     &,L_efomad,L_ifomad,L_ofomad,L_ufomad,
     & L_akomad,L_ibomad,REAL(R8_axorad,8),REAL(1.0,4),R8_abomad
     &,L_apomad,I_omomad,
     & L_upimad,L_opimad,L_iromad,L_oromad,L_adomad,L_ekomad
     &,L_ikomad,L_edomad)
      !}
C FDA_doz_logic.fgi( 289, 750):���������� ��������� ������� ����������� ���������,20FDA21AE701
      R_omimad = R_atomad * R_adasad
C FDA_doz_logic.fgi( 331, 722):����������,1
      R_aruxi=R_irimad
C FDA_doz_logic.fgi( 358, 729):������,20FDA21AE701VS01
      Call TELEGKA_TRANSP_HANDLER(deltat,L_uxose,L_(72),L_efuse
     &,L_(73),R_imuse,
     & R_omuse,R_ikuse,R_akuse,L_amuse,R_okuse,R_ekuse,
     & L_emuse,L_omose,L_umose,L_(74),L_uduse,R_ofuse,R_ufuse
     &,
     & R_uluse,R_ubuse,R_oluse,L_iduse,L_eduse,R_esose,
     & REAL(R_asose,4),R_uvose,REAL(R_exose,4),L_ivose,
     & R_evose,REAL(R_ovose,4),L_osose,R_isose,
     & REAL(R_usose,4),L_ifuse,L_(77),L_(78),L_axose,
     & L_ibuse,L_ukuse,L_(76),L_(75),L_aluse,L_iluse,L_aduse
     &)
C FDA_mechanic_vlv.fgi(  64, 968):���������� ������� ������������,20FDA60AE402
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(5))
C FDA_10boats.fgi( 140, 260):���������� ���������� ������� �� ������� FDA90,BOAT05
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(4))
C FDA_10boats.fgi( 160, 260):���������� ���������� ������� �� ������� FDA90,BOAT04
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(3))
C FDA_10boats.fgi( 180, 260):���������� ���������� ������� �� ������� FDA90,BOAT03
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(2))
C FDA_10boats.fgi( 200, 260):���������� ���������� ������� �� ������� FDA90,BOAT02
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(1))
C FDA_10boats.fgi( 220, 260):���������� ���������� ������� �� ������� FDA90,BOAT01
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(9))
C FDA_10boats.fgi(  60, 260):���������� ���������� ������� �� ������� FDA90,BOAT09
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(10))
C FDA_10boats.fgi(  40, 260):���������� ���������� ������� �� ������� FDA90,BOAT10
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(8))
C FDA_10boats.fgi(  80, 260):���������� ���������� ������� �� ������� FDA90,BOAT08
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(7))
C FDA_10boats.fgi( 100, 260):���������� ���������� ������� �� ������� FDA90,BOAT07
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_adas,L_oxur
     &,
     & L_ebas,L_obas,L_efuse,
     & L_edas,L_ibas,L_ixur,
     & L_ubas,L_uxur,L_adibe,L_abas,
     & L_akibe,L_alibe,L_afibe,I_(6))
C FDA_10boats.fgi( 120, 260):���������� ���������� ������� �� ������� FDA90,BOAT06
      !{
      Call TELEGKA_HANDLER(deltat,REAL(R_oduti,4),R8_avoti
     &,R_avuti,
     & L_efavi,R_ituti,L_odavi,
     & L_ubavi,L_adavi,R_otuti,L_udavi,
     & R_ututi,L_afavi,L_axuti,
     & L_ixuti,L_exuti,L_aduti,L_oxuti,
     & L_uxuti,L_abavi,L_ubuti,L_ovuti,
     & L_uvuti,L_evuti,L_ivuti,L_isuti,
     & L_ikuti,L_okuti,L_ukuti,L_aluti,L_oluti,L_uduti,L_eluti
     &,L_iluti,L_efuti,L_afuti,
     & L_ifuti,L_(163),L_(164),R_upoti,R_aroti,I_eputi,I_uruti
     &,R_atoti,
     & REAL(R_itoti,4),R_isoti,REAL(R_usoti,4),I_abuti,
     & I_uxoti,I_iputi,I_uputi,C20_axoti,C20_exoti,C8_ixoti
     &,R_eroti,
     & REAL(R_oroti,4),L_asoti,R_uroti,
     & REAL(R_esoti,4),I_oruti,R_ibuti,R_ebuti,I_aruti,I_oputi
     &,I_umuti,
     & L_ototi,L_esuti,L_ebavi,L_etoti,L_osoti,
     & L_uluti,L_amuti,L_evoti,L_ivoti,REAL(R_uvoti,4),L_usuti
     &,
     & L_iroti,L_oxoti,L_edavi,L_eduti,L_iduti,REAL(R8_axorad
     &,8),
     & REAL(1.0,4),R8_udepad,L_epoti,L_apoti,L_osuti,I_asuti
     &,L_opoti,L_ipoti,R_omuti,
     & L_ibavi,R_utoti,REAL(R_ovoti,4),L_obavi,L_iruti,L_idavi
     &,
     & REAL(R_oduti,4),L_ofuti,L_ufuti,L_akuti,L_emuti,L_imuti
     &,L_ekuti)
      !}
C FDA_mechanic_vlv.fgi(  46, 967):���������� �������,20FDA60AE401
      !{
      Call TELEGKA_HANDLER(deltat,REAL(R_avavi,4),R8_ipavi
     &,R_ipevi,
     & L_ovevi,R_umevi,L_avevi,
     & L_etevi,L_itevi,R_apevi,L_evevi,
     & R_epevi,L_ivevi,L_irevi,
     & L_urevi,L_orevi,L_itavi,L_asevi,
     & L_esevi,L_isevi,L_etavi,L_arevi,
     & L_erevi,L_opevi,L_upevi,L_ulevi,
     & L_uxavi,L_abevi,L_ebevi,L_ibevi,L_adevi,L_evavi,L_obevi
     &,L_ubevi,L_ovavi,L_ivavi,
     & L_uvavi,L_(165),L_(166),R_ekavi,R_ikavi,I_ofevi,I_elevi
     &,R_imavi,
     & REAL(R_umavi,4),R_ulavi,REAL(R_emavi,4),I_isavi,
     & I_esavi,I_ufevi,I_ekevi,C20_iravi,C20_oravi,C8_uravi
     &,R_okavi,
     & REAL(R_alavi,4),L_ilavi,R_elavi,
     & REAL(R_olavi,4),I_alevi,R_usavi,R_osavi,I_ikevi,I_akevi
     &,I_efevi,
     & L_apavi,L_olevi,L_osevi,L_omavi,L_amavi,
     & L_edevi,L_idevi,L_opavi,L_upavi,REAL(R_eravi,4),L_emevi
     &,
     & L_ukavi,L_asavi,L_otevi,L_otavi,L_utavi,REAL(R8_axorad
     &,8),
     & REAL(1.0,4),R8_udepad,L_ofavi,L_ifavi,L_amevi,I_ilevi
     &,L_akavi,L_ufavi,R_afevi,
     & L_usevi,R_epavi,REAL(R_aravi,4),L_atevi,L_ukevi,L_utevi
     &,
     & REAL(R_avavi,4),L_axavi,L_exavi,L_ixavi,L_odevi,L_udevi
     &,L_oxavi)
      !}
C FDA_mechanic_vlv.fgi(  28, 967):���������� �������,20FDA60AE400
      !{
      Call SHIB_HANDLER(deltat,R8_ibasi,R_utasi,R_avasi,R_erasi
     &,R_upasi,
     & L_atasi,R_irasi,R_arasi,L_etasi,R_ipasi,
     & R_opasi,R_usasi,R_itasi,R_otasi,R_aturi,
     & R_osasi,R_usuri,REAL(R_obasi,4),C20_adasi,I_elasi,I_imasi
     &,
     & R_exuri,REAL(R_oxuri,4),R_ovuri,
     & REAL(R_axuri,4),I_afasi,I_udasi,I_ilasi,I_amasi,C20_ubasi
     &,
     & C20_edasi,C8_idasi,L_oturi,R_ituri,
     & REAL(R_uturi,4),L_evuri,R_avuri,
     & REAL(R_ivuri,4),I_ulasi,I_emasi,I_olasi,I_alasi,L_uxuri
     &,
     & L_umasi,L_urasi,L_ixuri,L_uvuri,
     & L_ebasi,L_abasi,L_epasi,L_odasi,L_orasi,L_ifasi,L_ofasi
     &,
     & REAL(R8_eturi,8),REAL(1.0,4),R8_efasi,L_(150),L_esuri
     &,L_(149),L_asuri,
     & L_apasi,I_omasi,L_asasi,L_osuri,L_isasi,L_isuri,L_ufasi
     &,L_akasi,L_ekasi,L_okasi,
     & L_ukasi,L_ikasi)
      !}
C FDA_mechanic_vlv.fgi( 148, 941):���������� ������,20FDA60AB807
      !{
      Call SHIB_HANDLER(deltat,R8_ofesi,R_abisi,R_ebisi,R_itesi
     &,R_atesi,
     & L_exesi,R_otesi,R_etesi,L_ixesi,R_osesi,
     & R_usesi,R_axesi,R_oxesi,R_uxesi,R_exasi,
     & R_uvesi,R_axasi,REAL(R_ufesi,4),C20_ekesi,I_ipesi,I_oresi
     &,
     & R_idesi,REAL(R_udesi,4),R_ubesi,
     & REAL(R_edesi,4),I_elesi,I_alesi,I_opesi,I_eresi,C20_akesi
     &,
     & C20_ikesi,C8_okesi,L_uxasi,R_oxasi,
     & REAL(R_abesi,4),L_ibesi,R_ebesi,
     & REAL(R_obesi,4),I_aresi,I_iresi,I_upesi,I_epesi,L_afesi
     &,
     & L_asesi,L_avesi,L_odesi,L_adesi,
     & L_ifesi,L_efesi,L_isesi,L_ukesi,L_utesi,L_olesi,L_ulesi
     &,
     & REAL(R8_ixasi,8),REAL(1.0,4),R8_ilesi,L_(152),L_ivasi
     &,L_(151),L_evasi,
     & L_esesi,I_uresi,L_evesi,L_uvasi,L_ovesi,L_ovasi,L_amesi
     &,L_emesi,L_imesi,L_umesi,
     & L_apesi,L_omesi)
      !}
C FDA_mechanic_vlv.fgi( 131, 941):���������� ������,20FDA60AB806
      !{
      Call SHIB_HANDLER(deltat,R8_ulisi,R_efosi,R_ifosi,R_oxisi
     &,R_exisi,
     & L_idosi,R_uxisi,R_ixisi,L_odosi,R_uvisi,
     & R_axisi,R_edosi,R_udosi,R_afosi,R_idisi,
     & R_adosi,R_edisi,REAL(R_amisi,4),C20_imisi,I_osisi,I_utisi
     &,
     & R_okisi,REAL(R_alisi,4),R_akisi,
     & REAL(R_ikisi,4),I_ipisi,I_episi,I_usisi,I_itisi,C20_emisi
     &,
     & C20_omisi,C8_umisi,L_afisi,R_udisi,
     & REAL(R_efisi,4),L_ofisi,R_ifisi,
     & REAL(R_ufisi,4),I_etisi,I_otisi,I_atisi,I_isisi,L_elisi
     &,
     & L_evisi,L_ebosi,L_ukisi,L_ekisi,
     & L_olisi,L_ilisi,L_ovisi,L_apisi,L_abosi,L_upisi,L_arisi
     &,
     & REAL(R8_odisi,8),REAL(1.0,4),R8_opisi,L_(154),L_obisi
     &,L_(153),L_ibisi,
     & L_ivisi,I_avisi,L_ibosi,L_adisi,L_ubosi,L_ubisi,L_erisi
     &,L_irisi,L_orisi,L_asisi,
     & L_esisi,L_urisi)
      !}
C FDA_mechanic_vlv.fgi(  63, 915):���������� ������,20FDA60AB801
      !{
      Call SHIB_HANDLER(deltat,R8_arosi,R_ilusi,R_olusi,R_udusi
     &,R_idusi,
     & L_okusi,R_afusi,R_odusi,L_ukusi,R_adusi,
     & R_edusi,R_ikusi,R_alusi,R_elusi,R_okosi,
     & R_ekusi,R_ikosi,REAL(R_erosi,4),C20_orosi,I_uvosi,I_abusi
     &,
     & R_umosi,REAL(R_eposi,4),R_emosi,
     & REAL(R_omosi,4),I_ososi,I_isosi,I_axosi,I_oxosi,C20_irosi
     &,
     & C20_urosi,C8_asosi,L_elosi,R_alosi,
     & REAL(R_ilosi,4),L_ulosi,R_olosi,
     & REAL(R_amosi,4),I_ixosi,I_uxosi,I_exosi,I_ovosi,L_iposi
     &,
     & L_ibusi,L_ifusi,L_aposi,L_imosi,
     & L_uposi,L_oposi,L_ubusi,L_esosi,L_efusi,L_atosi,L_etosi
     &,
     & REAL(R8_ukosi,8),REAL(1.0,4),R8_usosi,L_(156),L_ufosi
     &,L_(155),L_ofosi,
     & L_obusi,I_ebusi,L_ofusi,L_ekosi,L_akusi,L_akosi,L_itosi
     &,L_otosi,L_utosi,L_evosi,
     & L_ivosi,L_avosi)
      !}
C FDA_mechanic_vlv.fgi(  49, 915):���������� ������,20FDA60AB800
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_abufi,4),R_efaki
     &,R_ifaki,
     & R_urufi,REAL(R_axofi,4),R_asufi,R_esufi,
     & R_isufi,R_osufi,L_oxufi,REAL(R_abufi,4),
     & R_itufi,L_afaki,R_usufi,
     & L_idaki,L_ebaki,L_ibaki,REAL(R_upofi,4),
     & R_atufi,L_odaki,R_etufi,
     & L_udaki,REAL(R_asofi,4),REAL(R_urofi,4),L_ivufi,
     & L_uvufi,L_ovufi,L_ixofi,L_axufi,
     & L_exufi,L_ixufi,L_exofi,R_alufi,
     & L_avufi,L_evufi,L_otufi,
     & L_utufi,L_umufi,L_upufi,C20_osofi,L_opofi,
     & L_opufi,L_uxufi,L_oxofi,L_udufi,L_afufi,L_efufi,L_ifufi
     &,L_akufi,
     & L_ebufi,L_ofufi,L_ufufi,L_obufi,L_ibufi,L_ubufi,L_edufi
     &,L_idufi,L_odufi,L_(111),
     & L_(110),REAL(R_isofi,4),R_irufi,R_orufi,R_adaki,R_edaki
     &,
     & I_olufi,R_alofi,L_okufi,R_ukofi,C20_usofi,C20_atofi
     &,I_amufi,I_epufi,
     & R_apofi,REAL(R_ipofi,4),R_elofi,
     & REAL(R_olofi,4),L_ekofi,L_akofi,I_evofi,I_avofi,I_ilufi
     &,I_imufi,
     & C20_etofi,C8_itofi,R_ulofi,REAL(R_emofi,4),
     & L_omofi,R_imofi,REAL(R_umofi,4),I_apufi,
     & L_(109),L_otofi,L_amofi,I_omufi,I_elufi,L_epofi,
     & L_ilofi,L_ekufi,L_ikufi,L_erofi,L_irofi,L_erufi,L_obaki
     &,
     & L_utofi,L_uxofi,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_ukufi,L_arufi,
     & I_ipufi,L_okofi,L_ikofi,L_abaki,L_ubaki,L_adufi)
      !}
C FDA_mechanic_vlv.fgi( 431, 942):���������� ���� OY,20FDA62AE406
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_ekase,4),R_imese
     &,R_omese,
     & R_axase,REAL(R_efase,4),R_exase,R_ixase,
     & R_oxase,R_uxase,L_ufese,REAL(R_ekase,4),
     & R_obese,L_emese,R_abese,
     & L_olese,L_ikese,L_okese,REAL(R_avure,4),
     & R_ebese,L_ulese,R_ibese,
     & L_amese,REAL(R_exure,4),REAL(R_axure,4),L_odese,
     & L_afese,L_udese,L_ofase,L_efese,
     & L_ifese,L_ofese,L_ifase,R_erase,
     & L_edese,L_idese,L_ubese,
     & L_adese,L_atase,L_avase,C20_uxure,L_uture,
     & L_utase,L_akese,L_ufase,L_amase,L_emase,L_imase,L_omase
     &,L_epase,
     & L_ikase,L_umase,L_apase,L_ukase,L_okase,L_alase,L_ilase
     &,L_olase,L_ulase,L_(68),
     & L_(67),REAL(R_oxure,4),R_ovase,R_uvase,R_elese,R_ilese
     &,
     & I_urase,R_erure,L_upase,R_arure,C20_abase,C20_ebase
     &,I_esase,I_itase,
     & R_eture,REAL(R_oture,4),R_irure,
     & REAL(R_urure,4),L_ipure,L_epure,I_idase,I_edase,I_orase
     &,I_osase,
     & C20_ibase,C8_obase,R_asure,REAL(R_isure,4),
     & L_usure,R_osure,REAL(R_ature,4),I_etase,
     & L_(66),L_ubase,L_esure,I_usase,I_irase,L_iture,
     & L_orure,L_ipase,L_opase,L_ivure,L_ovure,L_ivase,L_ukese
     &,
     & L_adase,L_akase,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_arase,L_evase,
     & I_otase,L_upure,L_opure,L_ekese,L_alese,L_elase)
      !}
C FDA_mechanic_vlv.fgi( 720, 974):���������� ���� OY,20FDA62AE401
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_ixake,4),REAL(R_adasad
     &,4),R_ekike,
     & R_ikike,R_upeke,REAL(R_evake,4),R_areke,R_ereke,
     & R_ireke,R_oreke,L_uxeke,REAL(R_ixake,4),
     & R_oteke,L_akike,R_ateke,
     & L_ifike,L_ubike,L_adike,REAL(R_arake,4),
     & R_eteke,L_ofike,R_iteke,
     & L_ufike,REAL(R_urake,4),REAL(R_orake,4),L_oveke,
     & L_axeke,L_uveke,L_uvake,L_exeke,
     & L_ixeke,L_oxeke,L_ovake,R_ikeke,
     & L_eveke,L_iveke,L_uteke,
     & L_aveke,L_uleke,L_umeke,C20_esake,L_upake,
     & L_omeke,L_ebike,L_axake,L_edeke,L_ideke,L_odeke,L_udeke
     &,L_ifeke,
     & L_oxake,L_afeke,L_efeke,L_abeke,L_uxake,L_ebeke,L_obeke
     &,L_ubeke,L_adeke,L_(32),
     & L_(31),REAL(R_asake,4),R_ipeke,R_opeke,R_odike,R_udike
     &,
     & I_aleke,R_alake,R_ukake,L_akeke,C20_isake,C20_osake
     &,I_eleke,I_emeke,
     & R_epake,REAL(R_opake,4),R_ilake,
     & REAL(R_ulake,4),L_ekake,L_akake,I_utake,I_otake,I_ukeke
     &,I_ileke,
     & C20_usake,C8_atake,R_amake,REAL(R_imake,4),
     & L_umake,R_omake,REAL(R_apake,4),I_ameke,
     & L_(30),L_etake,L_emake,I_oleke,I_okeke,L_ipake,
     & L_olake,L_ofeke,L_ufeke,L_erake,L_irake,L_epeke,L_edike
     &,
     & L_itake,L_exake,REAL(R8_elake,8),REAL(1.0,4),R8_avake
     &,L_ekeke,L_apeke,
     & I_imeke,L_okake,L_ikake,L_obike,L_idike,L_ibeke)
      !}
C FDA_mechanic_vlv.fgi(  97, 916):���������� ���������,20FDA60AE515
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_ibifi,4),R_ofofi
     &,R_ufofi,
     & R_esifi,REAL(R_ixefi,4),R_isifi,R_osifi,
     & R_usifi,R_atifi,L_abofi,REAL(R_ibifi,4),
     & R_utifi,L_ifofi,R_etifi,
     & L_udofi,L_obofi,L_ubofi,REAL(R_erefi,4),
     & R_itifi,L_afofi,R_otifi,
     & L_efofi,REAL(R_isefi,4),REAL(R_esefi,4),L_uvifi,
     & L_exifi,L_axifi,L_uxefi,L_ixifi,
     & L_oxifi,L_uxifi,L_oxefi,R_ilifi,
     & L_ivifi,L_ovifi,L_avifi,
     & L_evifi,L_epifi,L_erifi,C20_atefi,L_arefi,
     & L_arifi,L_ebofi,L_abifi,L_efifi,L_ififi,L_ofifi,L_ufifi
     &,L_ikifi,
     & L_obifi,L_akifi,L_ekifi,L_adifi,L_ubifi,L_edifi,L_odifi
     &,L_udifi,L_afifi,L_(108),
     & L_(107),REAL(R_usefi,4),R_urifi,R_asifi,R_idofi,R_odofi
     &,
     & I_amifi,R_ilefi,L_alifi,R_elefi,C20_etefi,C20_itefi
     &,I_imifi,I_opifi,
     & R_ipefi,REAL(R_upefi,4),R_olefi,
     & REAL(R_amefi,4),L_okefi,L_ikefi,I_ovefi,I_ivefi,I_ulifi
     &,I_umifi,
     & C20_otefi,C8_utefi,R_emefi,REAL(R_omefi,4),
     & L_apefi,R_umefi,REAL(R_epefi,4),I_ipifi,
     & L_(106),L_avefi,L_imefi,I_apifi,I_olifi,L_opefi,
     & L_ulefi,L_okifi,L_ukifi,L_orefi,L_urefi,L_orifi,L_adofi
     &,
     & L_evefi,L_ebifi,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_elifi,L_irifi,
     & I_upifi,L_alefi,L_ukefi,L_ibofi,L_edofi,L_idifi)
      !}
C FDA_mechanic_vlv.fgi( 446, 942):���������� ���� OY,20FDA63AE406
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_aboke,4),REAL(R_adasad
     &,4),R_ukuke,
     & R_aluke,R_iroke,REAL(R_uvike,4),R_oroke,R_uroke,
     & R_asoke,R_esoke,L_ibuke,REAL(R_aboke,4),
     & R_evoke,L_okuke,R_otoke,
     & L_akuke,L_iduke,L_oduke,REAL(R_orike,4),
     & R_utoke,L_ekuke,R_avoke,
     & L_ikuke,REAL(R_isike,4),REAL(R_esike,4),L_exoke,
     & L_oxoke,L_ixoke,L_ixike,L_uxoke,
     & L_abuke,L_ebuke,L_exike,R_aloke,
     & L_uvoke,L_axoke,L_ivoke,
     & L_ovoke,L_imoke,L_ipoke,C20_usike,L_irike,
     & L_epoke,L_ubuke,L_oxike,L_udoke,L_afoke,L_efoke,L_ifoke
     &,L_akoke,
     & L_eboke,L_ofoke,L_ufoke,L_oboke,L_iboke,L_uboke,L_edoke
     &,L_idoke,L_odoke,L_(35),
     & L_(34),REAL(R_osike,4),R_aroke,R_eroke,R_efuke,R_ifuke
     &,
     & I_oloke,R_olike,R_ilike,L_okoke,C20_atike,C20_etike
     &,I_uloke,I_umoke,
     & R_upike,REAL(R_erike,4),R_amike,
     & REAL(R_imike,4),L_ukike,L_okike,I_ivike,I_evike,I_iloke
     &,I_amoke,
     & C20_itike,C8_otike,R_omike,REAL(R_apike,4),
     & L_ipike,R_epike,REAL(R_opike,4),I_omoke,
     & L_(33),L_utike,L_umike,I_emoke,I_eloke,L_arike,
     & L_emike,L_ekoke,L_ikoke,L_urike,L_asike,L_upoke,L_uduke
     &,
     & L_avike,L_uxike,REAL(R8_ulike,8),REAL(1.0,4),R8_ovike
     &,L_ukoke,L_opoke,
     & I_apoke,L_elike,L_alike,L_eduke,L_afuke,L_adoke)
      !}
C FDA_mechanic_vlv.fgi(  80, 916):���������� ���������,20FDA60AE514
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_okore,4),R_umure
     &,R_apure,
     & R_ixore,REAL(R_ofore,4),R_oxore,R_uxore,
     & R_abure,R_ebure,L_ekure,REAL(R_okore,4),
     & R_adure,L_omure,R_ibure,
     & L_amure,L_ukure,L_alure,REAL(R_ivire,4),
     & R_obure,L_emure,R_ubure,
     & L_imure,REAL(R_oxire,4),REAL(R_ixire,4),L_afure,
     & L_ifure,L_efure,L_akore,L_ofure,
     & L_ufure,L_akure,L_ufore,R_orore,
     & L_odure,L_udure,L_edure,
     & L_idure,L_itore,L_ivore,C20_ebore,L_evire,
     & L_evore,L_ikure,L_ekore,L_imore,L_omore,L_umore,L_apore
     &,L_opore,
     & L_ukore,L_epore,L_ipore,L_elore,L_alore,L_ilore,L_ulore
     &,L_amore,L_emore,L_(65),
     & L_(64),REAL(R_abore,4),R_axore,R_exore,R_olure,R_ulure
     &,
     & I_esore,R_orire,L_erore,R_irire,C20_ibore,C20_obore
     &,I_osore,I_utore,
     & R_otire,REAL(R_avire,4),R_urire,
     & REAL(R_esire,4),L_upire,L_opire,I_udore,I_odore,I_asore
     &,I_atore,
     & C20_ubore,C8_adore,R_isire,REAL(R_usire,4),
     & L_etire,R_atire,REAL(R_itire,4),I_otore,
     & L_(63),L_edore,L_osire,I_etore,I_urore,L_utire,
     & L_asire,L_upore,L_arore,L_uvire,L_axire,L_uvore,L_elure
     &,
     & L_idore,L_ikore,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_irore,L_ovore,
     & I_avore,L_erire,L_arire,L_okure,L_ilure,L_olore)
      !}
C FDA_mechanic_vlv.fgi( 732, 974):���������� ���� OY,20FDA63AE401
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_uvofe,4),REAL(R_adasad
     &,4),R_ofake,
     & R_ufake,R_epufe,REAL(R_otofe,4),R_ipufe,R_opufe,
     & R_upufe,R_arufe,L_exufe,REAL(R_uvofe,4),
     & R_atufe,L_ifake,R_isufe,
     & L_udake,L_ebake,L_ibake,REAL(R_ipofe,4),
     & R_osufe,L_afake,R_usufe,
     & L_efake,REAL(R_erofe,4),REAL(R_arofe,4),L_avufe,
     & L_ivufe,L_evufe,L_evofe,L_ovufe,
     & L_uvufe,L_axufe,L_avofe,R_ufufe,
     & L_otufe,L_utufe,L_etufe,
     & L_itufe,L_elufe,L_emufe,C20_orofe,L_epofe,
     & L_amufe,L_oxufe,L_ivofe,L_obufe,L_ubufe,L_adufe,L_edufe
     &,L_udufe,
     & L_axofe,L_idufe,L_odufe,L_ixofe,L_exofe,L_oxofe,L_abufe
     &,L_ebufe,L_ibufe,L_(29),
     & L_(28),REAL(R_irofe,4),R_umufe,R_apufe,R_adake,R_edake
     &,
     & I_ikufe,R_ikofe,R_ekofe,L_ifufe,C20_urofe,C20_asofe
     &,I_okufe,I_olufe,
     & R_omofe,REAL(R_apofe,4),R_ukofe,
     & REAL(R_elofe,4),L_ofofe,L_ifofe,I_etofe,I_atofe,I_ekufe
     &,I_ukufe,
     & C20_esofe,C8_isofe,R_ilofe,REAL(R_ulofe,4),
     & L_emofe,R_amofe,REAL(R_imofe,4),I_ilufe,
     & L_(27),L_osofe,L_olofe,I_alufe,I_akufe,L_umofe,
     & L_alofe,L_afufe,L_efufe,L_opofe,L_upofe,L_omufe,L_obake
     &,
     & L_usofe,L_ovofe,REAL(R8_okofe,8),REAL(1.0,4),R8_itofe
     &,L_ofufe,L_imufe,
     & I_ulufe,L_akofe,L_ufofe,L_abake,L_ubake,L_uxofe)
      !}
C FDA_mechanic_vlv.fgi( 115, 942):���������� ���������,20FDA60AE509
      !{
      Call SHIB_HANDLER(deltat,R8_edeli,R_oveli,R_uveli,R_aseli
     &,R_oreli,
     & L_uteli,R_eseli,R_ureli,L_aveli,R_ereli,
     & R_ireli,R_oteli,R_eveli,R_iveli,R_utali,
     & R_iteli,R_otali,REAL(R_ideli,4),C20_udeli,I_ameli,I_epeli
     &,
     & R_abeli,REAL(R_ibeli,4),R_ixali,
     & REAL(R_uxali,4),I_ufeli,I_ofeli,I_emeli,I_umeli,C20_odeli
     &,
     & C20_afeli,C8_efeli,L_ivali,R_evali,
     & REAL(R_ovali,4),L_axali,R_uvali,
     & REAL(R_exali,4),I_omeli,I_apeli,I_imeli,I_uleli,L_obeli
     &,
     & L_opeli,L_oseli,L_ebeli,L_oxali,
     & L_adeli,L_ubeli,L_areli,L_ifeli,L_iseli,L_ekeli,L_ikeli
     &,
     & REAL(R8_avali,8),REAL(1.0,4),R8_akeli,L_(122),L_atali
     &,L_(121),L_usali,
     & L_upeli,I_ipeli,L_useli,L_itali,L_eteli,L_etali,L_okeli
     &,L_ukeli,L_aleli,L_ileli,
     & L_oleli,L_eleli)
      !}
C FDA_mechanic_vlv.fgi( 355, 941):���������� ������,20FDA63AB806
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_evefe,4),REAL(R_adasad
     &,4),R_afofe,
     & R_efofe,R_omife,REAL(R_atefe,4),R_umife,R_apife,
     & R_epife,R_ipife,L_ovife,REAL(R_evefe,4),
     & R_isife,L_udofe,R_urife,
     & L_edofe,L_oxife,L_uxife,REAL(R_umefe,4),
     & R_asife,L_idofe,R_esife,
     & L_odofe,REAL(R_opefe,4),REAL(R_ipefe,4),L_itife,
     & L_utife,L_otife,L_otefe,L_avife,
     & L_evife,L_ivife,L_itefe,R_efife,
     & L_atife,L_etife,L_osife,
     & L_usife,L_okife,L_olife,C20_arefe,L_omefe,
     & L_ilife,L_axife,L_utefe,L_abife,L_ebife,L_ibife,L_obife
     &,L_edife,
     & L_ivefe,L_ubife,L_adife,L_uvefe,L_ovefe,L_axefe,L_ixefe
     &,L_oxefe,L_uxefe,L_(26),
     & L_(25),REAL(R_upefe,4),R_emife,R_imife,R_ibofe,R_obofe
     &,
     & I_ufife,R_ufefe,R_ofefe,L_udife,C20_erefe,C20_irefe
     &,I_akife,I_alife,
     & R_amefe,REAL(R_imefe,4),R_ekefe,
     & REAL(R_okefe,4),L_afefe,L_udefe,I_osefe,I_isefe,I_ofife
     &,I_ekife,
     & C20_orefe,C8_urefe,R_ukefe,REAL(R_elefe,4),
     & L_olefe,R_ilefe,REAL(R_ulefe,4),I_ukife,
     & L_(24),L_asefe,L_alefe,I_ikife,I_ifife,L_emefe,
     & L_ikefe,L_idife,L_odife,L_apefe,L_epefe,L_amife,L_abofe
     &,
     & L_esefe,L_avefe,REAL(R8_akefe,8),REAL(1.0,4),R8_usefe
     &,L_afife,L_ulife,
     & I_elife,L_ifefe,L_efefe,L_ixife,L_ebofe,L_exefe)
      !}
C FDA_mechanic_vlv.fgi( 100, 942):���������� ���������,20FDA60AE504
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_otude,4),REAL(R_adasad
     &,4),R_idefe,
     & R_odefe,R_amafe,REAL(R_isude,4),R_emafe,R_imafe,
     & R_omafe,R_umafe,L_avafe,REAL(R_otude,4),
     & R_urafe,L_edefe,R_erafe,
     & L_obefe,L_axafe,L_exafe,REAL(R_emude,4),
     & R_irafe,L_ubefe,R_orafe,
     & L_adefe,REAL(R_apude,4),REAL(R_umude,4),L_usafe,
     & L_etafe,L_atafe,L_atude,L_itafe,
     & L_otafe,L_utafe,L_usude,R_odafe,
     & L_isafe,L_osafe,L_asafe,
     & L_esafe,L_akafe,L_alafe,C20_ipude,L_amude,
     & L_ukafe,L_ivafe,L_etude,L_ixude,L_oxude,L_uxude,L_abafe
     &,L_obafe,
     & L_utude,L_ebafe,L_ibafe,L_evude,L_avude,L_ivude,L_uvude
     &,L_axude,L_exude,L_(23),
     & L_(22),REAL(R_epude,4),R_olafe,R_ulafe,R_uxafe,R_abefe
     &,
     & I_efafe,R_efude,R_afude,L_edafe,C20_opude,C20_upude
     &,I_ifafe,I_ikafe,
     & R_ilude,REAL(R_ulude,4),R_ofude,
     & REAL(R_akude,4),L_idude,L_edude,I_asude,I_urude,I_afafe
     &,I_ofafe,
     & C20_arude,C8_erude,R_ekude,REAL(R_okude,4),
     & L_alude,R_ukude,REAL(R_elude,4),I_ekafe,
     & L_(21),L_irude,L_ikude,I_ufafe,I_udafe,L_olude,
     & L_ufude,L_ubafe,L_adafe,L_imude,L_omude,L_ilafe,L_ixafe
     &,
     & L_orude,L_itude,REAL(R8_ifude,8),REAL(1.0,4),R8_esude
     &,L_idafe,L_elafe,
     & I_okafe,L_udude,L_odude,L_uvafe,L_oxafe,L_ovude)
      !}
C FDA_mechanic_vlv.fgi(  80, 942):���������� ���������,20FDA60AE503
      !{
      Call SHIB_HANDLER(deltat,R8_atepi,R_ipipi,R_opipi,R_ukipi
     &,R_ikipi,
     & L_omipi,R_alipi,R_okipi,L_umipi,R_akipi,
     & R_ekipi,R_imipi,R_apipi,R_epipi,R_omepi,
     & R_emipi,R_imepi,REAL(R_etepi,4),C20_otepi,I_ubipi,I_afipi
     &,
     & R_urepi,REAL(R_esepi,4),R_erepi,
     & REAL(R_orepi,4),I_ovepi,I_ivepi,I_adipi,I_odipi,C20_itepi
     &,
     & C20_utepi,C8_avepi,L_epepi,R_apepi,
     & REAL(R_ipepi,4),L_upepi,R_opepi,
     & REAL(R_arepi,4),I_idipi,I_udipi,I_edipi,I_obipi,L_isepi
     &,
     & L_ifipi,L_ilipi,L_asepi,L_irepi,
     & L_usepi,L_osepi,L_ufipi,L_evepi,L_elipi,L_axepi,L_exepi
     &,
     & REAL(R8_umepi,8),REAL(1.0,4),R8_uvepi,L_(140),L_ulepi
     &,L_(139),L_olepi,
     & L_ofipi,I_efipi,L_olipi,L_emepi,L_amipi,L_amepi,L_ixepi
     &,L_oxepi,L_uxepi,L_ebipi,
     & L_ibipi,L_abipi)
      !}
C FDA_mechanic_vlv.fgi( 212, 941):���������� ������,20FDA63AB801
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_edati,4),REAL(R_adasad
     &,4),R_ameti,
     & R_emeti,R_osati,REAL(R_abati,4),R_usati,R_atati,
     & R_etati,R_itati,L_odeti,REAL(R_edati,4),
     & R_ixati,L_uleti,R_uvati,
     & L_eleti,L_ofeti,L_ufeti,REAL(R_ususi,4),
     & R_axati,L_ileti,R_exati,
     & L_oleti,REAL(R_otusi,4),REAL(R_itusi,4),L_ibeti,
     & L_ubeti,L_obeti,L_obati,L_adeti,
     & L_edeti,L_ideti,L_ibati,R_emati,
     & L_abeti,L_ebeti,L_oxati,
     & L_uxati,L_opati,L_orati,C20_avusi,L_osusi,
     & L_irati,L_afeti,L_ubati,L_akati,L_ekati,L_ikati,L_okati
     &,L_elati,
     & L_idati,L_ukati,L_alati,L_udati,L_odati,L_afati,L_ifati
     &,L_ofati,L_ufati,L_(159),
     & L_(158),REAL(R_utusi,4),R_esati,R_isati,R_iketi,R_oketi
     &,
     & I_umati,R_umusi,R_omusi,L_ulati,C20_evusi,C20_ivusi
     &,I_apati,I_arati,
     & R_asusi,REAL(R_isusi,4),R_epusi,
     & REAL(R_opusi,4),L_amusi,L_ulusi,I_oxusi,I_ixusi,I_omati
     &,I_epati,
     & C20_ovusi,C8_uvusi,R_upusi,REAL(R_erusi,4),
     & L_orusi,R_irusi,REAL(R_urusi,4),I_upati,
     & L_(157),L_axusi,L_arusi,I_ipati,I_imati,L_esusi,
     & L_ipusi,L_ilati,L_olati,L_atusi,L_etusi,L_asati,L_aketi
     &,
     & L_exusi,L_adati,REAL(R8_apusi,8),REAL(1.0,4),R8_uxusi
     &,L_amati,L_urati,
     & I_erati,L_imusi,L_emusi,L_ifeti,L_eketi,L_efati)
      !}
C FDA_mechanic_vlv.fgi(  64, 942):���������� ���������,20FDA60AE502
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_akixe,4),REAL(R_adasad
     &,4),R_upoxe,
     & R_aroxe,R_ivixe,REAL(R_udixe,4),R_ovixe,R_uvixe,
     & R_axixe,R_exixe,L_ikoxe,REAL(R_akixe,4),
     & R_edoxe,L_opoxe,R_oboxe,
     & L_apoxe,L_iloxe,L_oloxe,REAL(R_ovexe,4),
     & R_uboxe,L_epoxe,R_adoxe,
     & L_ipoxe,REAL(R_ixexe,4),REAL(R_exexe,4),L_efoxe,
     & L_ofoxe,L_ifoxe,L_ifixe,L_ufoxe,
     & L_akoxe,L_ekoxe,L_efixe,R_arixe,
     & L_udoxe,L_afoxe,L_idoxe,
     & L_odoxe,L_isixe,L_itixe,C20_uxexe,L_ivexe,
     & L_etixe,L_ukoxe,L_ofixe,L_ulixe,L_amixe,L_emixe,L_imixe
     &,L_apixe,
     & L_ekixe,L_omixe,L_umixe,L_okixe,L_ikixe,L_ukixe,L_elixe
     &,L_ilixe,L_olixe,L_(91),
     & L_(90),REAL(R_oxexe,4),R_avixe,R_evixe,R_emoxe,R_imoxe
     &,
     & I_orixe,R_orexe,R_irexe,L_opixe,C20_abixe,C20_ebixe
     &,I_urixe,I_usixe,
     & R_utexe,REAL(R_evexe,4),R_asexe,
     & REAL(R_isexe,4),L_upexe,L_opexe,I_idixe,I_edixe,I_irixe
     &,I_asixe,
     & C20_ibixe,C8_obixe,R_osexe,REAL(R_atexe,4),
     & L_itexe,R_etexe,REAL(R_otexe,4),I_osixe,
     & L_(89),L_ubixe,L_usexe,I_esixe,I_erixe,L_avexe,
     & L_esexe,L_epixe,L_ipixe,L_uvexe,L_axexe,L_utixe,L_uloxe
     &,
     & L_adixe,L_ufixe,REAL(R8_urexe,8),REAL(1.0,4),R8_odixe
     &,L_upixe,L_otixe,
     & I_atixe,L_erexe,L_arexe,L_eloxe,L_amoxe,L_alixe)
      !}
C FDA_mechanic_vlv.fgi(  49, 942):���������� ���������,20FDA60AE501
      !{
      Call SHIB_HANDLER(deltat,R8_exipi,R_osopi,R_usopi,R_apopi
     &,R_omopi,
     & L_uropi,R_epopi,R_umopi,L_asopi,R_emopi,
     & R_imopi,R_oropi,R_esopi,R_isopi,R_uripi,
     & R_iropi,R_oripi,REAL(R_ixipi,4),C20_uxipi,I_akopi,I_elopi
     &,
     & R_avipi,REAL(R_ivipi,4),R_itipi,
     & REAL(R_utipi,4),I_ubopi,I_obopi,I_ekopi,I_ukopi,C20_oxipi
     &,
     & C20_abopi,C8_ebopi,L_isipi,R_esipi,
     & REAL(R_osipi,4),L_atipi,R_usipi,
     & REAL(R_etipi,4),I_okopi,I_alopi,I_ikopi,I_ufopi,L_ovipi
     &,
     & L_olopi,L_opopi,L_evipi,L_otipi,
     & L_axipi,L_uvipi,L_amopi,L_ibopi,L_ipopi,L_edopi,L_idopi
     &,
     & REAL(R8_asipi,8),REAL(1.0,4),R8_adopi,L_(142),L_aripi
     &,L_(141),L_upipi,
     & L_ulopi,I_ilopi,L_upopi,L_iripi,L_eropi,L_eripi,L_odopi
     &,L_udopi,L_afopi,L_ifopi,
     & L_ofopi,L_efopi)
      !}
C FDA_mechanic_vlv.fgi( 196, 941):���������� ������,20FDA63AB800
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_okuxe,4),REAL(R_adasad
     &,4),R_irabi,
     & R_orabi,R_axuxe,REAL(R_ifuxe,4),R_exuxe,R_ixuxe,
     & R_oxuxe,R_uxuxe,L_alabi,REAL(R_okuxe,4),
     & R_udabi,L_erabi,R_edabi,
     & L_opabi,L_amabi,L_emabi,REAL(R_exoxe,4),
     & R_idabi,L_upabi,R_odabi,
     & L_arabi,REAL(R_abuxe,4),REAL(R_uxoxe,4),L_ufabi,
     & L_ekabi,L_akabi,L_akuxe,L_ikabi,
     & L_okabi,L_ukabi,L_ufuxe,R_oruxe,
     & L_ifabi,L_ofabi,L_afabi,
     & L_efabi,L_atuxe,L_avuxe,C20_ibuxe,L_axoxe,
     & L_utuxe,L_ilabi,L_ekuxe,L_imuxe,L_omuxe,L_umuxe,L_apuxe
     &,L_opuxe,
     & L_ukuxe,L_epuxe,L_ipuxe,L_eluxe,L_aluxe,L_iluxe,L_uluxe
     &,L_amuxe,L_emuxe,L_(94),
     & L_(93),REAL(R_ebuxe,4),R_ovuxe,R_uvuxe,R_umabi,R_apabi
     &,
     & I_esuxe,R_esoxe,R_asoxe,L_eruxe,C20_obuxe,C20_ubuxe
     &,I_isuxe,I_ituxe,
     & R_ivoxe,REAL(R_uvoxe,4),R_osoxe,
     & REAL(R_atoxe,4),L_iroxe,L_eroxe,I_afuxe,I_uduxe,I_asuxe
     &,I_osuxe,
     & C20_aduxe,C8_eduxe,R_etoxe,REAL(R_otoxe,4),
     & L_avoxe,R_utoxe,REAL(R_evoxe,4),I_etuxe,
     & L_(92),L_iduxe,L_itoxe,I_usuxe,I_uruxe,L_ovoxe,
     & L_usoxe,L_upuxe,L_aruxe,L_ixoxe,L_oxoxe,L_ivuxe,L_imabi
     &,
     & L_oduxe,L_ikuxe,REAL(R8_isoxe,8),REAL(1.0,4),R8_efuxe
     &,L_iruxe,L_evuxe,
     & I_otuxe,L_uroxe,L_oroxe,L_ulabi,L_omabi,L_oluxe)
      !}
C FDA_mechanic_vlv.fgi( 618, 974):���������� ���������,20FDA60AE500
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_okape,4),REAL(R_adasad
     &,4),R_irepe,
     & R_orepe,R_axape,REAL(R_ifape,4),R_exape,R_ixape,
     & R_oxape,R_uxape,L_alepe,REAL(R_okape,4),
     & R_udepe,L_erepe,R_edepe,
     & L_opepe,L_amepe,L_emepe,REAL(R_exume,4),
     & R_idepe,L_upepe,R_odepe,
     & L_arepe,REAL(R_abape,4),REAL(R_uxume,4),L_ufepe,
     & L_ekepe,L_akepe,L_akape,L_ikepe,
     & L_okepe,L_ukepe,L_ufape,R_orape,
     & L_ifepe,L_ofepe,L_afepe,
     & L_efepe,L_atape,L_avape,C20_ibape,L_axume,
     & L_utape,L_ilepe,L_ekape,L_imape,L_omape,L_umape,L_apape
     &,L_opape,
     & L_ukape,L_epape,L_ipape,L_elape,L_alape,L_ilape,L_ulape
     &,L_amape,L_emape,L_(53),
     & L_(52),REAL(R_ebape,4),R_ovape,R_uvape,R_umepe,R_apepe
     &,
     & I_esape,R_esume,R_asume,L_erape,C20_obape,C20_ubape
     &,I_isape,I_itape,
     & R_ivume,REAL(R_uvume,4),R_osume,
     & REAL(R_atume,4),L_irume,L_erume,I_afape,I_udape,I_asape
     &,I_osape,
     & C20_adape,C8_edape,R_etume,REAL(R_otume,4),
     & L_avume,R_utume,REAL(R_evume,4),I_etape,
     & L_(51),L_idape,L_itume,I_usape,I_urape,L_ovume,
     & L_usume,L_upape,L_arape,L_ixume,L_oxume,L_ivape,L_imepe
     &,
     & L_odape,L_ikape,REAL(R8_isume,8),REAL(1.0,4),R8_efape
     &,L_irape,L_evape,
     & I_otape,L_urume,L_orume,L_ulepe,L_omepe,L_olape)
      !}
C FDA_mechanic_vlv.fgi( 634, 974):���������� ���������,20FDA61AE500
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_edodi,4),R_ikudi
     &,R_okudi,
     & R_atodi,REAL(R_ebodi,4),R_etodi,R_itodi,
     & R_otodi,R_utodi,L_ubudi,REAL(R_edodi,4),
     & R_ovodi,L_ekudi,R_avodi,
     & L_ofudi,L_idudi,L_odudi,REAL(R_asidi,4),
     & R_evodi,L_ufudi,R_ivodi,
     & L_akudi,REAL(R_etidi,4),REAL(R_atidi,4),L_oxodi,
     & L_abudi,L_uxodi,L_obodi,L_ebudi,
     & L_ibudi,L_obudi,L_ibodi,R_emodi,
     & L_exodi,L_ixodi,L_uvodi,
     & L_axodi,L_arodi,L_asodi,C20_utidi,L_uridi,
     & L_urodi,L_adudi,L_ubodi,L_akodi,L_ekodi,L_ikodi,L_okodi
     &,L_elodi,
     & L_idodi,L_ukodi,L_alodi,L_udodi,L_ododi,L_afodi,L_ifodi
     &,L_ofodi,L_ufodi,L_(102),
     & L_(101),REAL(R_otidi,4),R_osodi,R_usodi,R_efudi,R_ifudi
     &,
     & I_umodi,R_emidi,L_ulodi,R_amidi,C20_avidi,C20_evidi
     &,I_epodi,I_irodi,
     & R_eridi,REAL(R_oridi,4),R_imidi,
     & REAL(R_umidi,4),L_ilidi,L_elidi,I_ixidi,I_exidi,I_omodi
     &,I_opodi,
     & C20_ividi,C8_ovidi,R_apidi,REAL(R_ipidi,4),
     & L_upidi,R_opidi,REAL(R_aridi,4),I_erodi,
     & L_(100),L_uvidi,L_epidi,I_upodi,I_imodi,L_iridi,
     & L_omidi,L_ilodi,L_olodi,L_isidi,L_osidi,L_isodi,L_ududi
     &,
     & L_axidi,L_adodi,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_amodi,L_esodi,
     & I_orodi,L_ulidi,L_olidi,L_edudi,L_afudi,L_efodi)
      !}
C FDA_mechanic_vlv.fgi( 477, 942):���������� ���� OY,20FDA65AE406
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_ilupe,4),R_opare
     &,R_upare,
     & R_ebare,REAL(R_ikupe,4),R_ibare,R_obare,
     & R_ubare,R_adare,L_alare,REAL(R_ilupe,4),
     & R_udare,L_ipare,R_edare,
     & L_umare,L_olare,L_ulare,REAL(R_exope,4),
     & R_idare,L_apare,R_odare,
     & L_epare,REAL(R_ibupe,4),REAL(R_ebupe,4),L_ufare,
     & L_ekare,L_akare,L_ukupe,L_ikare,
     & L_okare,L_ukare,L_okupe,R_isupe,
     & L_ifare,L_ofare,L_afare,
     & L_efare,L_evupe,L_exupe,C20_adupe,L_axope,
     & L_axupe,L_elare,L_alupe,L_epupe,L_ipupe,L_opupe,L_upupe
     &,L_irupe,
     & L_olupe,L_arupe,L_erupe,L_amupe,L_ulupe,L_emupe,L_omupe
     &,L_umupe,L_apupe,L_(59),
     & L_(58),REAL(R_ubupe,4),R_uxupe,R_abare,R_imare,R_omare
     &,
     & I_atupe,R_isope,L_asupe,R_esope,C20_edupe,C20_idupe
     &,I_itupe,I_ovupe,
     & R_ivope,REAL(R_uvope,4),R_osope,
     & REAL(R_atope,4),L_orope,L_irope,I_ofupe,I_ifupe,I_usupe
     &,I_utupe,
     & C20_odupe,C8_udupe,R_etope,REAL(R_otope,4),
     & L_avope,R_utope,REAL(R_evope,4),I_ivupe,
     & L_(57),L_afupe,L_itope,I_avupe,I_osupe,L_ovope,
     & L_usope,L_orupe,L_urupe,L_oxope,L_uxope,L_oxupe,L_amare
     &,
     & L_efupe,L_elupe,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_esupe,L_ixupe,
     & I_uvupe,L_asope,L_urope,L_ilare,L_emare,L_imupe)
      !}
C FDA_mechanic_vlv.fgi( 756, 974):���������� ���� OY,20FDA65AE401
      Call MECHANIC_PRIVOD(deltat,L_opade,REAL(950,4),L_(14
     &),
     & L_ebade,REAL(900,4),L_(5),L_adade,L_udade,L_ofade,
     & REAL(800,4),L_(6),REAL(3500,4),L_(7),REAL(2300,4),L_
     &(8),
     & L_ikade,REAL(2050,4),L_(9),L_elade,REAL(1760,4),L_
     &(10),
     & L_amade,L_umade,REAL(1440,4),L_(11),REAL(730,4),L_
     &(12),
     & L_epade,L_uxube,L_obade,L_idade,L_efade,L_upade,L_ibade
     &,
     & L_edade,L_afade,L_ufade,L_ipade,L_abade,L_ubade,
     & L_odade,L_ifade,L_akade,L_okade,L_ekade,L_ukade,L_olade
     &,
     & L_imade,L_ilade,L_emade,L_apade,L_alade,L_ulade,
     & L_omade,REAL(0,4),L_erade,L_arade,L_orade,L_irade,
     & L_(13),L_axube,L_(1),L_atade,L_(2),R_oxube,L_etade
     &,L_itade,
     & REAL(20,4),L_esade,L_(15),L_urade,L_asade,L_(4),
     & L_(3),INT(I_exube,4),L_ixube,INT(I_osade,4),L_usade
     &,
     & INT(I_ovube,4),L_uvube,R_isade,REAL(3500,4),REAL(0
     &,4),R_avade,L_evube,
     & L_ivube,L_otade,L_utade)
C FDA_mechanic_vlv.fgi( 164, 972):������,20FDA60AE403
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_odedi,4),R_ukidi
     &,R_alidi,
     & R_itedi,REAL(R_obedi,4),R_otedi,R_utedi,
     & R_avedi,R_evedi,L_edidi,REAL(R_odedi,4),
     & R_axedi,L_okidi,R_ivedi,
     & L_akidi,L_udidi,L_afidi,REAL(R_isadi,4),
     & R_ovedi,L_ekidi,R_uvedi,
     & L_ikidi,REAL(R_otadi,4),REAL(R_itadi,4),L_abidi,
     & L_ibidi,L_ebidi,L_adedi,L_obidi,
     & L_ubidi,L_adidi,L_ubedi,R_omedi,
     & L_oxedi,L_uxedi,L_exedi,
     & L_ixedi,L_iredi,L_isedi,C20_evadi,L_esadi,
     & L_esedi,L_ididi,L_ededi,L_ikedi,L_okedi,L_ukedi,L_aledi
     &,L_oledi,
     & L_udedi,L_eledi,L_iledi,L_efedi,L_afedi,L_ifedi,L_ufedi
     &,L_akedi,L_ekedi,L_(99),
     & L_(98),REAL(R_avadi,4),R_atedi,R_etedi,R_ofidi,R_ufidi
     &,
     & I_epedi,R_omadi,L_emedi,R_imadi,C20_ivadi,C20_ovadi
     &,I_opedi,I_uredi,
     & R_oradi,REAL(R_asadi,4),R_umadi,
     & REAL(R_epadi,4),L_uladi,L_oladi,I_uxadi,I_oxadi,I_apedi
     &,I_aredi,
     & C20_uvadi,C8_axadi,R_ipadi,REAL(R_upadi,4),
     & L_eradi,R_aradi,REAL(R_iradi,4),I_oredi,
     & L_(97),L_exadi,L_opadi,I_eredi,I_umedi,L_uradi,
     & L_apadi,L_uledi,L_amedi,L_usadi,L_atadi,L_usedi,L_efidi
     &,
     & L_ixadi,L_idedi,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_imedi,L_osedi,
     & I_asedi,L_emadi,L_amadi,L_odidi,L_ifidi,L_ofedi)
      !}
C FDA_mechanic_vlv.fgi( 492, 942):���������� ���� OY,20FDA66AE406
      !{
      Call TYAGA_OY_HANDLER(deltat,REAL(R_ulipe,4),R_arope
     &,R_erope,
     & R_obope,REAL(R_ukipe,4),R_ubope,R_adope,
     & R_edope,R_idope,L_ilope,REAL(R_ulipe,4),
     & R_efope,L_upope,R_odope,
     & L_epope,L_amope,L_emope,REAL(R_oxepe,4),
     & R_udope,L_ipope,R_afope,
     & L_opope,REAL(R_ubipe,4),REAL(R_obipe,4),L_ekope,
     & L_okope,L_ikope,L_elipe,L_ukope,
     & L_alope,L_elope,L_alipe,R_usipe,
     & L_ufope,L_akope,L_ifope,
     & L_ofope,L_ovipe,L_oxipe,C20_idipe,L_ixepe,
     & L_ixipe,L_olope,L_ilipe,L_opipe,L_upipe,L_aripe,L_eripe
     &,L_uripe,
     & L_amipe,L_iripe,L_oripe,L_imipe,L_emipe,L_omipe,L_apipe
     &,L_epipe,L_ipipe,L_(56),
     & L_(55),REAL(R_edipe,4),R_ebope,R_ibope,R_umope,R_apope
     &,
     & I_itipe,R_usepe,L_isipe,R_osepe,C20_odipe,C20_udipe
     &,I_utipe,I_axipe,
     & R_uvepe,REAL(R_exepe,4),R_atepe,
     & REAL(R_itepe,4),L_asepe,L_urepe,I_akipe,I_ufipe,I_etipe
     &,I_evipe,
     & C20_afipe,C8_efipe,R_otepe,REAL(R_avepe,4),
     & L_ivepe,R_evepe,REAL(R_ovepe,4),I_uvipe,
     & L_(54),L_ifipe,L_utepe,I_ivipe,I_atipe,L_axepe,
     & L_etepe,L_asipe,L_esipe,L_abipe,L_ebipe,L_abope,L_imope
     &,
     & L_ofipe,L_olipe,REAL(R8_axorad,8),REAL(1.0,4),R8_udepad
     &,L_osipe,L_uxipe,
     & I_exipe,L_isepe,L_esepe,L_ulope,L_omope,L_umipe)
      !}
C FDA_mechanic_vlv.fgi( 768, 974):���������� ���� OY,20FDA66AE401
      !{
      Call SHIB_HANDLER(deltat,R8_opiki,R_aloki,R_eloki,R_idoki
     &,R_adoki,
     & L_ekoki,R_odoki,R_edoki,L_ikoki,R_oboki,
     & R_uboki,R_akoki,R_okoki,R_ukoki,R_ekiki,
     & R_ufoki,R_akiki,REAL(R_upiki,4),C20_eriki,I_iviki,I_oxiki
     &,
     & R_imiki,REAL(R_umiki,4),R_uliki,
     & REAL(R_emiki,4),I_esiki,I_asiki,I_oviki,I_exiki,C20_ariki
     &,
     & C20_iriki,C8_oriki,L_ukiki,R_okiki,
     & REAL(R_aliki,4),L_iliki,R_eliki,
     & REAL(R_oliki,4),I_axiki,I_ixiki,I_uviki,I_eviki,L_apiki
     &,
     & L_aboki,L_afoki,L_omiki,L_amiki,
     & L_ipiki,L_epiki,L_iboki,L_uriki,L_udoki,L_osiki,L_usiki
     &,
     & REAL(R8_ikiki,8),REAL(1.0,4),R8_isiki,L_(116),L_ifiki
     &,L_(115),L_efiki,
     & L_eboki,I_uxiki,L_efoki,L_ufiki,L_ofoki,L_ofiki,L_atiki
     &,L_etiki,L_itiki,L_utiki,
     & L_aviki,L_otiki)
      !}
C FDA_mechanic_vlv.fgi( 399, 941):���������� ������,20FDA66AB806
      !{
      Call SHIB_HANDLER(deltat,R8_uruli,R_emami,R_imami,R_ofami
     &,R_efami,
     & L_ilami,R_ufami,R_ifami,L_olami,R_udami,
     & R_afami,R_elami,R_ulami,R_amami,R_iluli,
     & R_alami,R_eluli,REAL(R_asuli,4),C20_isuli,I_oxuli,I_ubami
     &,
     & R_opuli,REAL(R_aruli,4),R_apuli,
     & REAL(R_ipuli,4),I_ituli,I_etuli,I_uxuli,I_ibami,C20_esuli
     &,
     & C20_osuli,C8_usuli,L_amuli,R_ululi,
     & REAL(R_emuli,4),L_omuli,R_imuli,
     & REAL(R_umuli,4),I_ebami,I_obami,I_abami,I_ixuli,L_eruli
     &,
     & L_edami,L_ekami,L_upuli,L_epuli,
     & L_oruli,L_iruli,L_odami,L_atuli,L_akami,L_utuli,L_avuli
     &,
     & REAL(R8_oluli,8),REAL(1.0,4),R8_otuli,L_(128),L_okuli
     &,L_(127),L_ikuli,
     & L_idami,I_adami,L_ikami,L_aluli,L_ukami,L_ukuli,L_evuli
     &,L_ivuli,L_ovuli,L_axuli,
     & L_exuli,L_uvuli)
      !}
C FDA_mechanic_vlv.fgi( 310, 941):���������� ������,20FDA66AB801
      !{
      Call SHIB_HANDLER(deltat,R8_avami,R_iremi,R_oremi,R_ulemi
     &,R_ilemi,
     & L_opemi,R_amemi,R_olemi,L_upemi,R_alemi,
     & R_elemi,R_ipemi,R_aremi,R_eremi,R_opami,
     & R_epemi,R_ipami,REAL(R_evami,4),C20_ovami,I_udemi,I_akemi
     &,
     & R_usami,REAL(R_etami,4),R_esami,
     & REAL(R_osami,4),I_oxami,I_ixami,I_afemi,I_ofemi,C20_ivami
     &,
     & C20_uvami,C8_axami,L_erami,R_arami,
     & REAL(R_irami,4),L_urami,R_orami,
     & REAL(R_asami,4),I_ifemi,I_ufemi,I_efemi,I_odemi,L_itami
     &,
     & L_ikemi,L_imemi,L_atami,L_isami,
     & L_utami,L_otami,L_ukemi,L_exami,L_ememi,L_abemi,L_ebemi
     &,
     & REAL(R8_upami,8),REAL(1.0,4),R8_uxami,L_(130),L_umami
     &,L_(129),L_omami,
     & L_okemi,I_ekemi,L_omemi,L_epami,L_apemi,L_apami,L_ibemi
     &,L_obemi,L_ubemi,L_edemi,
     & L_idemi,L_ademi)
      !}
C FDA_mechanic_vlv.fgi( 294, 941):���������� ������,20FDA66AB800
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_ifeme,4),REAL(R_adasad
     &,4),R_epime,
     & R_ipime,R_uteme,REAL(R_edeme,4),R_aveme,R_eveme,
     & R_iveme,R_oveme,L_ufime,REAL(R_ifeme,4),
     & R_obime,L_apime,R_abime,
     & L_imime,L_ukime,L_alime,REAL(R_avame,4),
     & R_ebime,L_omime,R_ibime,
     & L_umime,REAL(R_uvame,4),REAL(R_ovame,4),L_odime,
     & L_afime,L_udime,L_udeme,L_efime,
     & L_ifime,L_ofime,L_odeme,R_ipeme,
     & L_edime,L_idime,L_ubime,
     & L_adime,L_ureme,L_useme,C20_exame,L_utame,
     & L_oseme,L_ekime,L_afeme,L_eleme,L_ileme,L_oleme,L_uleme
     &,L_imeme,
     & L_ofeme,L_ameme,L_ememe,L_akeme,L_ufeme,L_ekeme,L_okeme
     &,L_ukeme,L_aleme,L_(47),
     & L_(46),REAL(R_axame,4),R_iteme,R_oteme,R_olime,R_ulime
     &,
     & I_areme,R_arame,R_upame,L_apeme,C20_ixame,C20_oxame
     &,I_ereme,I_eseme,
     & R_etame,REAL(R_otame,4),R_irame,
     & REAL(R_urame,4),L_epame,L_apame,I_ubeme,I_obeme,I_upeme
     &,I_ireme,
     & C20_uxame,C8_abeme,R_asame,REAL(R_isame,4),
     & L_usame,R_osame,REAL(R_atame,4),I_aseme,
     & L_(45),L_ebeme,L_esame,I_oreme,I_opeme,L_itame,
     & L_orame,L_omeme,L_umeme,L_evame,L_ivame,L_eteme,L_elime
     &,
     & L_ibeme,L_efeme,REAL(R8_erame,8),REAL(1.0,4),R8_ademe
     &,L_epeme,L_ateme,
     & I_iseme,L_opame,L_ipame,L_okime,L_ilime,L_ikeme)
      !}
C FDA_mechanic_vlv.fgi( 658, 974):���������� ���������,20FDA63AE500
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_obomo,REAL(R_ilomo
     &,4),
     & REAL(R_abumo,4),REAL(R_ebumo,4),REAL(R_otomo,4),R_edomo
     &,REAL(R_itomo,4),
     & L_(372),L_(371),R_obumo,R_ubumo,R_udumo,I_evomo,R_adomo
     &,
     & R_odumo,R_ubomo,I_ivomo,I_exomo,R_ekomo,
     & REAL(R_okomo,4),R_idomo,REAL(R_udomo,4),L_oximo,
     & L_iximo,I_omomo,I_imomo,I_avomo,I_ovomo,C20_olomo,C20_ulomo
     &,C8_amomo,
     & L_uximo,R_afomo,REAL(R_ifomo,4),L_ufomo,
     & R_ofomo,REAL(R_akomo,4),I_axomo,L_efomo,
     & L_(370),L_emomo,L_abomo,I_uvomo,I_utomo,L_ukomo,L_oxomo
     &,L_adumo,
     & L_ikomo,L_odomo,L_alomo,L_elomo,L_ibumo,L_apomo,
     & L_oromo,L_uromo,L_eromo,L_iromo,L_ipomo,L_opomo,L_asomo
     &,L_esomo,L_isomo,L_osomo,
     & L_usomo,L_epomo,REAL(R8_axorad,8),REAL(1.0,4),R8_umomo
     &,L_uxomo,I_ixomo,
     & L_ibomo,L_ebomo,L_edumo,L_idumo,L_upomo,L_atomo,L_etomo
     &,L_aromo)
      !}
C FDA_doz_logic.fgi( 346, 750):���������� ��������� ������� ����������� ���������,20FDA60CONTS01VZ2
      R_uremo = R_ubomo * R_adasad
C FDA_doz_logic.fgi( 331, 678):����������,1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_umapo,REAL(R_otapo
     &,4),
     & REAL(R_emepo,4),REAL(R_imepo,4),REAL(R_ufepo,4),R_ipapo
     &,REAL(R_ofepo,4),
     & L_(378),L_(377),R_umepo,R_apepo,R_arepo,I_ikepo,R_epapo
     &,
     & R_upepo,R_apapo,I_okepo,I_ilepo,R_isapo,
     & REAL(R_usapo,4),R_opapo,REAL(R_arapo,4),L_ulapo,
     & L_olapo,I_uvapo,I_ovapo,I_ekepo,I_ukepo,C20_utapo,C20_avapo
     &,C8_evapo,
     & L_amapo,R_erapo,REAL(R_orapo,4),L_asapo,
     & R_urapo,REAL(R_esapo,4),I_elepo,L_irapo,
     & L_(376),L_ivapo,L_emapo,I_alepo,I_akepo,L_atapo,L_ulepo
     &,L_epepo,
     & L_osapo,L_upapo,L_etapo,L_itapo,L_omepo,L_exapo,
     & L_ubepo,L_adepo,L_ibepo,L_obepo,L_oxapo,L_uxapo,L_edepo
     &,L_idepo,L_odepo,L_udepo,
     & L_afepo,L_ixapo,REAL(R8_axorad,8),REAL(1.0,4),R8_axapo
     &,L_amepo,I_olepo,
     & L_omapo,L_imapo,L_ipepo,L_opepo,L_abepo,L_efepo,L_ifepo
     &,L_ebepo)
      !}
C FDA_doz_logic.fgi( 307, 750):���������� ��������� ������� ����������� ���������,20FDA60CONTS01VZ1
      R_etolo = R_apapo * R_adasad
C FDA_doz_logic.fgi( 331, 707):����������,1
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_udule,4),REAL(R_adasad
     &,4),R_omame,
     & R_umame,R_etule,REAL(R_obule,4),R_itule,R_otule,
     & R_utule,R_avule,L_efame,REAL(R_udule,4),
     & R_abame,L_imame,R_ixule,
     & L_ulame,L_ekame,L_ikame,REAL(R_itole,4),
     & R_oxule,L_amame,R_uxule,
     & L_emame,REAL(R_evole,4),REAL(R_avole,4),L_adame,
     & L_idame,L_edame,L_edule,L_odame,
     & L_udame,L_afame,L_adule,R_umule,
     & L_obame,L_ubame,L_ebame,
     & L_ibame,L_erule,L_esule,C20_ovole,L_etole,
     & L_asule,L_ofame,L_idule,L_okule,L_ukule,L_alule,L_elule
     &,L_ulule,
     & L_afule,L_ilule,L_olule,L_ifule,L_efule,L_ofule,L_akule
     &,L_ekule,L_ikule,L_(44),
     & L_(43),REAL(R_ivole,4),R_usule,R_atule,R_alame,R_elame
     &,
     & I_ipule,R_ipole,R_epole,L_imule,C20_uvole,C20_axole
     &,I_opule,I_orule,
     & R_osole,REAL(R_atole,4),R_upole,
     & REAL(R_erole,4),L_omole,L_imole,I_ebule,I_abule,I_epule
     &,I_upule,
     & C20_exole,C8_ixole,R_irole,REAL(R_urole,4),
     & L_esole,R_asole,REAL(R_isole,4),I_irule,
     & L_(42),L_oxole,L_orole,I_arule,I_apule,L_usole,
     & L_arole,L_amule,L_emule,L_otole,L_utole,L_osule,L_okame
     &,
     & L_uxole,L_odule,REAL(R8_opole,8),REAL(1.0,4),R8_ibule
     &,L_omule,L_isule,
     & I_urule,L_apole,L_umole,L_akame,L_ukame,L_ufule)
      !}
C FDA_mechanic_vlv.fgi( 670, 974):���������� ���������,20FDA64AE500
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_obale,4),REAL(R_adasad
     &,4),R_ilele,
     & R_olele,R_asale,REAL(R_ixuke,4),R_esale,R_isale,
     & R_osale,R_usale,L_adele,REAL(R_obale,4),
     & R_uvale,L_elele,R_evale,
     & L_okele,L_afele,L_efele,REAL(R_esuke,4),
     & R_ivale,L_ukele,R_ovale,
     & L_alele,REAL(R_atuke,4),REAL(R_usuke,4),L_uxale,
     & L_ebele,L_abele,L_abale,L_ibele,
     & L_obele,L_ubele,L_uxuke,R_olale,
     & L_ixale,L_oxale,L_axale,
     & L_exale,L_apale,L_arale,C20_ituke,L_asuke,
     & L_upale,L_idele,L_ebale,L_ifale,L_ofale,L_ufale,L_akale
     &,L_okale,
     & L_ubale,L_ekale,L_ikale,L_edale,L_adale,L_idale,L_udale
     &,L_afale,L_efale,L_(38),
     & L_(37),REAL(R_etuke,4),R_orale,R_urale,R_ufele,R_akele
     &,
     & I_emale,R_emuke,R_amuke,L_elale,C20_otuke,C20_utuke
     &,I_imale,I_ipale,
     & R_iruke,REAL(R_uruke,4),R_omuke,
     & REAL(R_apuke,4),L_iluke,L_eluke,I_axuke,I_uvuke,I_amale
     &,I_omale,
     & C20_avuke,C8_evuke,R_epuke,REAL(R_opuke,4),
     & L_aruke,R_upuke,REAL(R_eruke,4),I_epale,
     & L_(36),L_ivuke,L_ipuke,I_umale,I_ulale,L_oruke,
     & L_umuke,L_ukale,L_alale,L_isuke,L_osuke,L_irale,L_ifele
     &,
     & L_ovuke,L_ibale,REAL(R8_imuke,8),REAL(1.0,4),R8_exuke
     &,L_ilale,L_erale,
     & I_opale,L_uluke,L_oluke,L_udele,L_ofele,L_odale)
      !}
C FDA_mechanic_vlv.fgi( 694, 974):���������� ���������,20FDA66AE500
      !{
      Call SHIB_HANDLER(deltat,R8_ulote,R_efute,R_ifute,R_oxote
     &,R_exote,
     & L_idute,R_uxote,R_ixote,L_odute,R_uvote,
     & R_axote,R_edute,R_udute,R_afute,R_idote,
     & R_adute,R_edote,REAL(R_amote,4),C20_imote,I_osote,I_utote
     &,
     & R_okote,REAL(R_alote,4),R_akote,
     & REAL(R_ikote,4),I_ipote,I_epote,I_usote,I_itote,C20_emote
     &,
     & C20_omote,C8_umote,L_afote,R_udote,
     & REAL(R_efote,4),L_ofote,R_ifote,
     & REAL(R_ufote,4),I_etote,I_otote,I_atote,I_isote,L_elote
     &,
     & L_evote,L_ebute,L_ukote,L_ekote,
     & L_olote,L_ilote,L_ovote,L_apote,L_abute,L_upote,L_arote
     &,
     & REAL(R8_odote,8),REAL(1.0,4),R8_opote,L_(86),L_obote
     &,L_(85),L_ibote,
     & L_ivote,I_avote,L_ibute,L_adote,L_ubute,L_ubote,L_erote
     &,L_irote,L_orote,L_asote,
     & L_esote,L_urote)
      !}
C FDA_mechanic_vlv.fgi( 855, 949):���������� ������,20FDA91AB005
      !{
      Call SHIB_HANDLER(deltat,R8_ebebi,R_otebi,R_utebi,R_arebi
     &,R_opebi,
     & L_usebi,R_erebi,R_upebi,L_atebi,R_epebi,
     & R_ipebi,R_osebi,R_etebi,R_itebi,R_usabi,
     & R_isebi,R_osabi,REAL(R_ibebi,4),C20_ubebi,I_alebi,I_emebi
     &,
     & R_axabi,REAL(R_ixabi,4),R_ivabi,
     & REAL(R_uvabi,4),I_udebi,I_odebi,I_elebi,I_ulebi,C20_obebi
     &,
     & C20_adebi,C8_edebi,L_itabi,R_etabi,
     & REAL(R_otabi,4),L_avabi,R_utabi,
     & REAL(R_evabi,4),I_olebi,I_amebi,I_ilebi,I_ukebi,L_oxabi
     &,
     & L_omebi,L_orebi,L_exabi,L_ovabi,
     & L_abebi,L_uxabi,L_apebi,L_idebi,L_irebi,L_efebi,L_ifebi
     &,
     & REAL(R8_atabi,8),REAL(1.0,4),R8_afebi,L_(96),L_asabi
     &,L_(95),L_urabi,
     & L_umebi,I_imebi,L_urebi,L_isabi,L_esebi,L_esabi,L_ofebi
     &,L_ufebi,L_akebi,L_ikebi,
     & L_okebi,L_ekebi)
      !}
C FDA_mechanic_vlv.fgi( 131, 967):���������� ������,20FDA33AE002
      !{
      Call SHIB_HANDLER(deltat,R8_ibete,R_utete,R_avete,R_erete
     &,R_upete,
     & L_atete,R_irete,R_arete,L_etete,R_ipete,
     & R_opete,R_usete,R_itete,R_otete,R_atate,
     & R_osete,R_usate,REAL(R_obete,4),C20_adete,I_elete,I_imete
     &,
     & R_exate,REAL(R_oxate,4),R_ovate,
     & REAL(R_axate,4),I_afete,I_udete,I_ilete,I_amete,C20_ubete
     &,
     & C20_edete,C8_idete,L_otate,R_itate,
     & REAL(R_utate,4),L_evate,R_avate,
     & REAL(R_ivate,4),I_ulete,I_emete,I_olete,I_alete,L_uxate
     &,
     & L_umete,L_urete,L_ixate,L_uvate,
     & L_ebete,L_abete,L_epete,L_odete,L_orete,L_ifete,L_ofete
     &,
     & REAL(R8_etate,8),REAL(1.0,4),R8_efete,L_(82),L_esate
     &,L_(81),L_asate,
     & L_apete,I_omete,L_asete,L_osate,L_isete,L_isate,L_ufete
     &,L_akete,L_ekete,L_okete,
     & L_ukete,L_ikete)
      !}
C FDA_mechanic_vlv.fgi( 869, 949):���������� ������,20FDA91AB004
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_uditi,4),REAL(R_adasad
     &,4),R_omoti,
     & R_umoti,R_etiti,REAL(R_obiti,4),R_ititi,R_otiti,
     & R_utiti,R_aviti,L_efoti,REAL(R_uditi,4),
     & R_aboti,L_imoti,R_ixiti,
     & L_uloti,L_ekoti,L_ikoti,REAL(R_iteti,4),
     & R_oxiti,L_amoti,R_uxiti,
     & L_emoti,REAL(R_eveti,4),REAL(R_aveti,4),L_adoti,
     & L_idoti,L_edoti,L_editi,L_odoti,
     & L_udoti,L_afoti,L_aditi,R_umiti,
     & L_oboti,L_uboti,L_eboti,
     & L_iboti,L_eriti,L_esiti,C20_oveti,L_eteti,
     & L_asiti,L_ofoti,L_iditi,L_okiti,L_ukiti,L_aliti,L_eliti
     &,L_uliti,
     & L_afiti,L_iliti,L_oliti,L_ifiti,L_efiti,L_ofiti,L_akiti
     &,L_ekiti,L_ikiti,L_(162),
     & L_(161),REAL(R_iveti,4),R_usiti,R_atiti,R_aloti,R_eloti
     &,
     & I_ipiti,R_ipeti,R_epeti,L_imiti,C20_uveti,C20_axeti
     &,I_opiti,I_oriti,
     & R_oseti,REAL(R_ateti,4),R_upeti,
     & REAL(R_ereti,4),L_ometi,L_imeti,I_ebiti,I_abiti,I_epiti
     &,I_upiti,
     & C20_exeti,C8_ixeti,R_ireti,REAL(R_ureti,4),
     & L_eseti,R_aseti,REAL(R_iseti,4),I_iriti,
     & L_(160),L_oxeti,L_oreti,I_ariti,I_apiti,L_useti,
     & L_areti,L_amiti,L_emiti,L_oteti,L_uteti,L_ositi,L_okoti
     &,
     & L_uxeti,L_oditi,REAL(R8_opeti,8),REAL(1.0,4),R8_ibiti
     &,L_omiti,L_isiti,
     & I_uriti,L_apeti,L_umeti,L_akoti,L_ukoti,L_ufiti)
      !}
C FDA_mechanic_vlv.fgi(  11, 968):���������� ���������,U
      !{
      Call SHIB_HANDLER(deltat,R8_evuse,R_orate,R_urate,R_amate
     &,R_olate,
     & L_upate,R_emate,R_ulate,L_arate,R_elate,
     & R_ilate,R_opate,R_erate,R_irate,R_upuse,
     & R_ipate,R_opuse,REAL(R_ivuse,4),C20_uvuse,I_afate,I_ekate
     &,
     & R_atuse,REAL(R_ituse,4),R_isuse,
     & REAL(R_ususe,4),I_uxuse,I_oxuse,I_efate,I_ufate,C20_ovuse
     &,
     & C20_axuse,C8_exuse,L_iruse,R_eruse,
     & REAL(R_oruse,4),L_asuse,R_uruse,
     & REAL(R_esuse,4),I_ofate,I_akate,I_ifate,I_udate,L_otuse
     &,
     & L_okate,L_omate,L_etuse,L_osuse,
     & L_avuse,L_utuse,L_alate,L_ixuse,L_imate,L_ebate,L_ibate
     &,
     & REAL(R8_aruse,8),REAL(1.0,4),R8_abate,L_(80),L_apuse
     &,L_(79),L_umuse,
     & L_ukate,I_ikate,L_umate,L_ipuse,L_epate,L_epuse,L_obate
     &,L_ubate,L_adate,L_idate,
     & L_odate,L_edate)
      !}
C FDA_mechanic_vlv.fgi( 883, 949):���������� ������,20FDA91AB003
      !{
      Call SHIB_HANDLER(deltat,R8_ofite,R_abote,R_ebote,R_itite
     &,R_atite,
     & L_exite,R_otite,R_etite,L_ixite,R_osite,
     & R_usite,R_axite,R_oxite,R_uxite,R_exete,
     & R_uvite,R_axete,REAL(R_ufite,4),C20_ekite,I_ipite,I_orite
     &,
     & R_idite,REAL(R_udite,4),R_ubite,
     & REAL(R_edite,4),I_elite,I_alite,I_opite,I_erite,C20_akite
     &,
     & C20_ikite,C8_okite,L_uxete,R_oxete,
     & REAL(R_abite,4),L_ibite,R_ebite,
     & REAL(R_obite,4),I_arite,I_irite,I_upite,I_epite,L_afite
     &,
     & L_asite,L_avite,L_odite,L_adite,
     & L_ifite,L_efite,L_isite,L_ukite,L_utite,L_olite,L_ulite
     &,
     & REAL(R8_ixete,8),REAL(1.0,4),R8_ilite,L_(84),L_ivete
     &,L_(83),L_evete,
     & L_esite,I_urite,L_evite,L_uvete,L_ovite,L_ovete,L_amite
     &,L_emite,L_imite,L_umite,
     & L_apite,L_omite)
      !}
C FDA_mechanic_vlv.fgi( 841, 949):���������� ������,20FDA91AB002
      !{
      Call SHIB_HANDLER(deltat,R8_arute,R_ilave,R_olave,R_udave
     &,R_idave,
     & L_okave,R_afave,R_odave,L_ukave,R_adave,
     & R_edave,R_ikave,R_alave,R_elave,R_okute,
     & R_ekave,R_ikute,REAL(R_erute,4),C20_orute,I_uvute,I_abave
     &,
     & R_umute,REAL(R_epute,4),R_emute,
     & REAL(R_omute,4),I_osute,I_isute,I_axute,I_oxute,C20_irute
     &,
     & C20_urute,C8_asute,L_elute,R_alute,
     & REAL(R_ilute,4),L_ulute,R_olute,
     & REAL(R_amute,4),I_ixute,I_uxute,I_exute,I_ovute,L_ipute
     &,
     & L_ibave,L_ifave,L_apute,L_imute,
     & L_upute,L_opute,L_ubave,L_esute,L_efave,L_atute,L_etute
     &,
     & REAL(R8_ukute,8),REAL(1.0,4),R8_usute,L_(88),L_ufute
     &,L_(87),L_ofute,
     & L_obave,I_ebave,L_ofave,L_ekute,L_akave,L_akute,L_itute
     &,L_otute,L_utute,L_evute,
     & L_ivute,L_avute)
      !}
C FDA_mechanic_vlv.fgi( 827, 949):���������� ������,20FDA91AB001
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(1))
C FDA_10boats.fgi( 220, 214):���������� ������� � ���� ���,BOAT01
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(2))
C FDA_10boats.fgi( 200, 214):���������� ������� � ���� ���,BOAT02
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(3))
C FDA_10boats.fgi( 180, 214):���������� ������� � ���� ���,BOAT03
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(4))
C FDA_10boats.fgi( 160, 214):���������� ������� � ���� ���,BOAT04
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(5))
C FDA_10boats.fgi( 140, 214):���������� ������� � ���� ���,BOAT05
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(6))
C FDA_10boats.fgi( 120, 214):���������� ������� � ���� ���,BOAT06
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(7))
C FDA_10boats.fgi( 100, 214):���������� ������� � ���� ���,BOAT07
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(8))
C FDA_10boats.fgi(  80, 214):���������� ������� � ���� ���,BOAT08
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(9))
C FDA_10boats.fgi(  60, 214):���������� ������� � ���� ���,BOAT09
      Call LODOCHKA_HANDLER_KTS(deltat,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_ekad,L_ikad,
     & L_okad,L_ukad,L_alad,
     & L_elad,L_ilad,L_olad,L_ulad,
     & L_amad,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_ebad,L_ibad,
     & L_obad,L_ubad,L_adad,
     & L_edad,L_idad,L_odad,
     & L_udad,L_afad,L_efad,
     & L_ifad,L_ofad,L_ufad,
     & L_akad,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ebed,L_ibed,
     & L_obed,L_ubed,L_aded,
     & L_eded,L_ided,L_oded,L_uded,
     & L_afed,L_etad,L_itad,
     & L_otad,L_utad,L_avad,
     & L_evad,L_ivad,L_ovad,
     & L_uvad,L_axad,L_exad,
     & L_ixad,L_oxad,L_uxad,
     & L_abed,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,L_etu,L_itu,
     & L_otu,L_avu,L_evu,L_exu,
     & L_ixu,L_oxu,L_uxu,L_abad,
     & L_esu,L_isu,L_osu,L_usu,
     & L_atu,L_utu,L_ivu,L_ovu,
     & L_uvu,L_axu,L_epad,L_ipad,
     & L_opad,L_arad,L_erad,L_esad,
     & L_isad,L_osad,L_usad,L_atad,
     & L_emad,L_imad,L_omad,L_umad,
     & L_apad,L_upad,L_irad,L_orad,
     & L_urad,L_asad,I_(10))
C FDA_10boats.fgi(  40, 214):���������� ������� � ���� ���,BOAT10
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_atol,
     & I_ufir,I_etol,I_akir,I_itol,I_ekir,
     & I_otol,I_ikir,I_utol,I_okir,I_avol,
     & I_ukir,I_evol,I_alir,I_ivol,I_ovol,
     & I_uvol,I_itir,I_axol,I_otir,I_exol,
     & I_utir,I_ixol,I_avir,I_oxol,I_evir,
     & I_uxol,I_ivir,I_abul,I_ovir,I_ebul,
     & I_uvir,I_ibul,I_axir,I_obul,I_exir,
     & I_ubul,I_oxir,I_adul,I_uxir,I_edul,
     & I_abor,I_idul,I_ebor,I_odul,I_ibor,
     & I_udul,I_obor,I_aful,I_ubor,I_eful,
     & I_ador,I_iful,I_edor,I_oful,I_idor,
     & I_uful,I_udor,I_afor,I_efor,I_akul,
     & I_ifor,I_ekul,I_isir,I_ikul,I_osir,
     & I_okul,I_usir,I_ukul,I_atir,I_alul,
     & I_elir,I_elul,I_ilir,I_ilul,I_olir,
     & I_olul,I_amir,I_ulul,I_emir,I_amul,
     & I_imir,I_emul,I_omir,I_imul,I_umir,
     & I_omul,I_apir,I_umul,I_epir,I_apul,
     & I_ipir,I_epul,I_opir,I_ipul,I_upir,
     & I_opul,I_erir,I_upul,I_irir,I_arul,
     & I_orir,I_erul,I_urir,I_irul,I_asir,
     & I_orul,I_esir,I_urul,I_ofor,I_asul,
     & I_ufor,I_esul,I_akor,I_isul,I_ekor,
     & I_osul,I_ikor,I_usul,I_okor,I_atul,
     & I_alor,I_etul,I_elor,I_itul,I_ilor,
     & I_otul,I_olor,I_utul,I_ulor,I_avul,
     & I_amor,I_evul,I_emor,I_ivul,I_imor,
     & I_ovul,I_omor,I_uvul,I_umor,I_axul,
     & I_efir,I_exul,I_ofir,I_ixul,I_ulir,
     & I_oxul,I_arir,I_uxul,I_etir,I_abam,
     & I_ixir,I_ebam,I_odor,I_ibam,I_ukor,
     & I_obam,I_ubam,I_adam,I_edam,I_apor,I_usol,
     & I_idam,I_odam,I_udam,I_afam,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(1))
C FDA_10boats.fgi( 220, 244):���������� ���������� ������� �� ������� FDA50,BOAT01
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_ifam,
     & I_ufir,I_ofam,I_akir,I_ufam,I_ekir,
     & I_akam,I_ikir,I_ekam,I_okir,I_ikam,
     & I_ukir,I_okam,I_alir,I_ukam,I_alam,
     & I_elam,I_itir,I_ilam,I_otir,I_olam,
     & I_utir,I_ulam,I_avir,I_amam,I_evir,
     & I_emam,I_ivir,I_imam,I_ovir,I_omam,
     & I_uvir,I_umam,I_axir,I_apam,I_exir,
     & I_epam,I_oxir,I_ipam,I_uxir,I_opam,
     & I_abor,I_upam,I_ebor,I_aram,I_ibor,
     & I_eram,I_obor,I_iram,I_ubor,I_oram,
     & I_ador,I_uram,I_edor,I_asam,I_idor,
     & I_esam,I_udor,I_afor,I_efor,I_isam,
     & I_ifor,I_osam,I_isir,I_usam,I_osir,
     & I_atam,I_usir,I_etam,I_atir,I_itam,
     & I_elir,I_otam,I_ilir,I_utam,I_olir,
     & I_avam,I_amir,I_evam,I_emir,I_ivam,
     & I_imir,I_ovam,I_omir,I_uvam,I_umir,
     & I_axam,I_apir,I_exam,I_epir,I_ixam,
     & I_ipir,I_oxam,I_opir,I_uxam,I_upir,
     & I_abem,I_erir,I_ebem,I_irir,I_ibem,
     & I_orir,I_obem,I_urir,I_ubem,I_asir,
     & I_adem,I_esir,I_edem,I_ofor,I_idem,
     & I_ufor,I_odem,I_akor,I_udem,I_ekor,
     & I_afem,I_ikor,I_efem,I_okor,I_ifem,
     & I_alor,I_ofem,I_elor,I_ufem,I_ilor,
     & I_akem,I_olor,I_ekem,I_ulor,I_ikem,
     & I_amor,I_okem,I_emor,I_ukem,I_imor,
     & I_alem,I_omor,I_elem,I_umor,I_ilem,
     & I_efir,I_olem,I_ofir,I_ulem,I_ulir,
     & I_amem,I_arir,I_emem,I_etir,I_imem,
     & I_ixir,I_omem,I_odor,I_umem,I_ukor,
     & I_apem,I_epem,I_ipem,I_opem,I_apor,I_efam,
     & I_upem,I_arem,I_erem,I_irem,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(2))
C FDA_10boats.fgi( 200, 244):���������� ���������� ������� �� ������� FDA50,BOAT02
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_urem,
     & I_ufir,I_asem,I_akir,I_esem,I_ekir,
     & I_isem,I_ikir,I_osem,I_okir,I_usem,
     & I_ukir,I_atem,I_alir,I_etem,I_item,
     & I_otem,I_itir,I_utem,I_otir,I_avem,
     & I_utir,I_evem,I_avir,I_ivem,I_evir,
     & I_ovem,I_ivir,I_uvem,I_ovir,I_axem,
     & I_uvir,I_exem,I_axir,I_ixem,I_exir,
     & I_oxem,I_oxir,I_uxem,I_uxir,I_abim,
     & I_abor,I_ebim,I_ebor,I_ibim,I_ibor,
     & I_obim,I_obor,I_ubim,I_ubor,I_adim,
     & I_ador,I_edim,I_edor,I_idim,I_idor,
     & I_odim,I_udor,I_afor,I_efor,I_udim,
     & I_ifor,I_afim,I_isir,I_efim,I_osir,
     & I_ifim,I_usir,I_ofim,I_atir,I_ufim,
     & I_elir,I_akim,I_ilir,I_ekim,I_olir,
     & I_ikim,I_amir,I_okim,I_emir,I_ukim,
     & I_imir,I_alim,I_omir,I_elim,I_umir,
     & I_ilim,I_apir,I_olim,I_epir,I_ulim,
     & I_ipir,I_amim,I_opir,I_emim,I_upir,
     & I_imim,I_erir,I_omim,I_irir,I_umim,
     & I_orir,I_apim,I_urir,I_epim,I_asir,
     & I_ipim,I_esir,I_opim,I_ofor,I_upim,
     & I_ufor,I_arim,I_akor,I_erim,I_ekor,
     & I_irim,I_ikor,I_orim,I_okor,I_urim,
     & I_alor,I_asim,I_elor,I_esim,I_ilor,
     & I_isim,I_olor,I_osim,I_ulor,I_usim,
     & I_amor,I_atim,I_emor,I_etim,I_imor,
     & I_itim,I_omor,I_otim,I_umor,I_utim,
     & I_efir,I_avim,I_ofir,I_evim,I_ulir,
     & I_ivim,I_arir,I_ovim,I_etir,I_uvim,
     & I_ixir,I_axim,I_odor,I_exim,I_ukor,
     & I_ixim,I_oxim,I_uxim,I_abom,I_apor,I_orem,
     & I_ebom,I_ibom,I_obom,I_ubom,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(3))
C FDA_10boats.fgi( 180, 244):���������� ���������� ������� �� ������� FDA50,BOAT03
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_edom,
     & I_ufir,I_idom,I_akir,I_odom,I_ekir,
     & I_udom,I_ikir,I_afom,I_okir,I_efom,
     & I_ukir,I_ifom,I_alir,I_ofom,I_ufom,
     & I_akom,I_itir,I_ekom,I_otir,I_ikom,
     & I_utir,I_okom,I_avir,I_ukom,I_evir,
     & I_alom,I_ivir,I_elom,I_ovir,I_ilom,
     & I_uvir,I_olom,I_axir,I_ulom,I_exir,
     & I_amom,I_oxir,I_emom,I_uxir,I_imom,
     & I_abor,I_omom,I_ebor,I_umom,I_ibor,
     & I_apom,I_obor,I_epom,I_ubor,I_ipom,
     & I_ador,I_opom,I_edor,I_upom,I_idor,
     & I_arom,I_udor,I_afor,I_efor,I_erom,
     & I_ifor,I_irom,I_isir,I_orom,I_osir,
     & I_urom,I_usir,I_asom,I_atir,I_esom,
     & I_elir,I_isom,I_ilir,I_osom,I_olir,
     & I_usom,I_amir,I_atom,I_emir,I_etom,
     & I_imir,I_itom,I_omir,I_otom,I_umir,
     & I_utom,I_apir,I_avom,I_epir,I_evom,
     & I_ipir,I_ivom,I_opir,I_ovom,I_upir,
     & I_uvom,I_erir,I_axom,I_irir,I_exom,
     & I_orir,I_ixom,I_urir,I_oxom,I_asir,
     & I_uxom,I_esir,I_abum,I_ofor,I_ebum,
     & I_ufor,I_ibum,I_akor,I_obum,I_ekor,
     & I_ubum,I_ikor,I_adum,I_okor,I_edum,
     & I_alor,I_idum,I_elor,I_odum,I_ilor,
     & I_udum,I_olor,I_afum,I_ulor,I_efum,
     & I_amor,I_ifum,I_emor,I_ofum,I_imor,
     & I_ufum,I_omor,I_akum,I_umor,I_ekum,
     & I_efir,I_ikum,I_ofir,I_okum,I_ulir,
     & I_ukum,I_arir,I_alum,I_etir,I_elum,
     & I_ixir,I_ilum,I_odor,I_olum,I_ukor,
     & I_ulum,I_amum,I_emum,I_imum,I_apor,I_adom,
     & I_omum,I_umum,I_apum,I_epum,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(4))
C FDA_10boats.fgi( 160, 244):���������� ���������� ������� �� ������� FDA50,BOAT04
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_opum,
     & I_ufir,I_upum,I_akir,I_arum,I_ekir,
     & I_erum,I_ikir,I_irum,I_okir,I_orum,
     & I_ukir,I_urum,I_alir,I_asum,I_esum,
     & I_isum,I_itir,I_osum,I_otir,I_usum,
     & I_utir,I_atum,I_avir,I_etum,I_evir,
     & I_itum,I_ivir,I_otum,I_ovir,I_utum,
     & I_uvir,I_avum,I_axir,I_evum,I_exir,
     & I_ivum,I_oxir,I_ovum,I_uxir,I_uvum,
     & I_abor,I_axum,I_ebor,I_exum,I_ibor,
     & I_ixum,I_obor,I_oxum,I_ubor,I_uxum,
     & I_ador,I_abap,I_edor,I_ebap,I_idor,
     & I_ibap,I_udor,I_afor,I_efor,I_obap,
     & I_ifor,I_ubap,I_isir,I_adap,I_osir,
     & I_edap,I_usir,I_idap,I_atir,I_odap,
     & I_elir,I_udap,I_ilir,I_afap,I_olir,
     & I_efap,I_amir,I_ifap,I_emir,I_ofap,
     & I_imir,I_ufap,I_omir,I_akap,I_umir,
     & I_ekap,I_apir,I_ikap,I_epir,I_okap,
     & I_ipir,I_ukap,I_opir,I_alap,I_upir,
     & I_elap,I_erir,I_ilap,I_irir,I_olap,
     & I_orir,I_ulap,I_urir,I_amap,I_asir,
     & I_emap,I_esir,I_imap,I_ofor,I_omap,
     & I_ufor,I_umap,I_akor,I_apap,I_ekor,
     & I_epap,I_ikor,I_ipap,I_okor,I_opap,
     & I_alor,I_upap,I_elor,I_arap,I_ilor,
     & I_erap,I_olor,I_irap,I_ulor,I_orap,
     & I_amor,I_urap,I_emor,I_asap,I_imor,
     & I_esap,I_omor,I_isap,I_umor,I_osap,
     & I_efir,I_usap,I_ofir,I_atap,I_ulir,
     & I_etap,I_arir,I_itap,I_etir,I_otap,
     & I_ixir,I_utap,I_odor,I_avap,I_ukor,
     & I_evap,I_ivap,I_ovap,I_uvap,I_apor,I_ipum,
     & I_axap,I_exap,I_ixap,I_oxap,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(5))
C FDA_10boats.fgi( 140, 244):���������� ���������� ������� �� ������� FDA50,BOAT05
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_abep,
     & I_ufir,I_ebep,I_akir,I_ibep,I_ekir,
     & I_obep,I_ikir,I_ubep,I_okir,I_adep,
     & I_ukir,I_edep,I_alir,I_idep,I_odep,
     & I_udep,I_itir,I_afep,I_otir,I_efep,
     & I_utir,I_ifep,I_avir,I_ofep,I_evir,
     & I_ufep,I_ivir,I_akep,I_ovir,I_ekep,
     & I_uvir,I_ikep,I_axir,I_okep,I_exir,
     & I_ukep,I_oxir,I_alep,I_uxir,I_elep,
     & I_abor,I_ilep,I_ebor,I_olep,I_ibor,
     & I_ulep,I_obor,I_amep,I_ubor,I_emep,
     & I_ador,I_imep,I_edor,I_omep,I_idor,
     & I_umep,I_udor,I_afor,I_efor,I_apep,
     & I_ifor,I_epep,I_isir,I_ipep,I_osir,
     & I_opep,I_usir,I_upep,I_atir,I_arep,
     & I_elir,I_erep,I_ilir,I_irep,I_olir,
     & I_orep,I_amir,I_urep,I_emir,I_asep,
     & I_imir,I_esep,I_omir,I_isep,I_umir,
     & I_osep,I_apir,I_usep,I_epir,I_atep,
     & I_ipir,I_etep,I_opir,I_itep,I_upir,
     & I_otep,I_erir,I_utep,I_irir,I_avep,
     & I_orir,I_evep,I_urir,I_ivep,I_asir,
     & I_ovep,I_esir,I_uvep,I_ofor,I_axep,
     & I_ufor,I_exep,I_akor,I_ixep,I_ekor,
     & I_oxep,I_ikor,I_uxep,I_okor,I_abip,
     & I_alor,I_ebip,I_elor,I_ibip,I_ilor,
     & I_obip,I_olor,I_ubip,I_ulor,I_adip,
     & I_amor,I_edip,I_emor,I_idip,I_imor,
     & I_odip,I_omor,I_udip,I_umor,I_afip,
     & I_efir,I_efip,I_ofir,I_ifip,I_ulir,
     & I_ofip,I_arir,I_ufip,I_etir,I_akip,
     & I_ixir,I_ekip,I_odor,I_ikip,I_ukor,
     & I_okip,I_ukip,I_alip,I_elip,I_apor,I_uxap,
     & I_ilip,I_olip,I_ulip,I_amip,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(6))
C FDA_10boats.fgi( 120, 244):���������� ���������� ������� �� ������� FDA50,BOAT06
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_imip,
     & I_ufir,I_omip,I_akir,I_umip,I_ekir,
     & I_apip,I_ikir,I_epip,I_okir,I_ipip,
     & I_ukir,I_opip,I_alir,I_upip,I_arip,
     & I_erip,I_itir,I_irip,I_otir,I_orip,
     & I_utir,I_urip,I_avir,I_asip,I_evir,
     & I_esip,I_ivir,I_isip,I_ovir,I_osip,
     & I_uvir,I_usip,I_axir,I_atip,I_exir,
     & I_etip,I_oxir,I_itip,I_uxir,I_otip,
     & I_abor,I_utip,I_ebor,I_avip,I_ibor,
     & I_evip,I_obor,I_ivip,I_ubor,I_ovip,
     & I_ador,I_uvip,I_edor,I_axip,I_idor,
     & I_exip,I_udor,I_afor,I_efor,I_ixip,
     & I_ifor,I_oxip,I_isir,I_uxip,I_osir,
     & I_abop,I_usir,I_ebop,I_atir,I_ibop,
     & I_elir,I_obop,I_ilir,I_ubop,I_olir,
     & I_adop,I_amir,I_edop,I_emir,I_idop,
     & I_imir,I_odop,I_omir,I_udop,I_umir,
     & I_afop,I_apir,I_efop,I_epir,I_ifop,
     & I_ipir,I_ofop,I_opir,I_ufop,I_upir,
     & I_akop,I_erir,I_ekop,I_irir,I_ikop,
     & I_orir,I_okop,I_urir,I_ukop,I_asir,
     & I_alop,I_esir,I_elop,I_ofor,I_ilop,
     & I_ufor,I_olop,I_akor,I_ulop,I_ekor,
     & I_amop,I_ikor,I_emop,I_okor,I_imop,
     & I_alor,I_omop,I_elor,I_umop,I_ilor,
     & I_apop,I_olor,I_epop,I_ulor,I_ipop,
     & I_amor,I_opop,I_emor,I_upop,I_imor,
     & I_arop,I_omor,I_erop,I_umor,I_irop,
     & I_efir,I_orop,I_ofir,I_urop,I_ulir,
     & I_asop,I_arir,I_esop,I_etir,I_isop,
     & I_ixir,I_osop,I_odor,I_usop,I_ukor,
     & I_atop,I_etop,I_itop,I_otop,I_apor,I_emip,
     & I_utop,I_avop,I_evop,I_ivop,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(7))
C FDA_10boats.fgi( 100, 244):���������� ���������� ������� �� ������� FDA50,BOAT07
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_uvop,
     & I_ufir,I_axop,I_akir,I_exop,I_ekir,
     & I_ixop,I_ikir,I_oxop,I_okir,I_uxop,
     & I_ukir,I_abup,I_alir,I_ebup,I_ibup,
     & I_obup,I_itir,I_ubup,I_otir,I_adup,
     & I_utir,I_edup,I_avir,I_idup,I_evir,
     & I_odup,I_ivir,I_udup,I_ovir,I_afup,
     & I_uvir,I_efup,I_axir,I_ifup,I_exir,
     & I_ofup,I_oxir,I_ufup,I_uxir,I_akup,
     & I_abor,I_ekup,I_ebor,I_ikup,I_ibor,
     & I_okup,I_obor,I_ukup,I_ubor,I_alup,
     & I_ador,I_elup,I_edor,I_ilup,I_idor,
     & I_olup,I_udor,I_afor,I_efor,I_ulup,
     & I_ifor,I_amup,I_isir,I_emup,I_osir,
     & I_imup,I_usir,I_omup,I_atir,I_umup,
     & I_elir,I_apup,I_ilir,I_epup,I_olir,
     & I_ipup,I_amir,I_opup,I_emir,I_upup,
     & I_imir,I_arup,I_omir,I_erup,I_umir,
     & I_irup,I_apir,I_orup,I_epir,I_urup,
     & I_ipir,I_asup,I_opir,I_esup,I_upir,
     & I_isup,I_erir,I_osup,I_irir,I_usup,
     & I_orir,I_atup,I_urir,I_etup,I_asir,
     & I_itup,I_esir,I_otup,I_ofor,I_utup,
     & I_ufor,I_avup,I_akor,I_evup,I_ekor,
     & I_ivup,I_ikor,I_ovup,I_okor,I_uvup,
     & I_alor,I_axup,I_elor,I_exup,I_ilor,
     & I_ixup,I_olor,I_oxup,I_ulor,I_uxup,
     & I_amor,I_abar,I_emor,I_ebar,I_imor,
     & I_ibar,I_omor,I_obar,I_umor,I_ubar,
     & I_efir,I_adar,I_ofir,I_edar,I_ulir,
     & I_idar,I_arir,I_odar,I_etir,I_udar,
     & I_ixir,I_afar,I_odor,I_efar,I_ukor,
     & I_ifar,I_ofar,I_ufar,I_akar,I_apor,I_ovop,
     & I_ekar,I_ikar,I_okar,I_ukar,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(8))
C FDA_10boats.fgi(  80, 244):���������� ���������� ������� �� ������� FDA50,BOAT08
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_elar,
     & I_ufir,I_ilar,I_akir,I_olar,I_ekir,
     & I_ular,I_ikir,I_amar,I_okir,I_emar,
     & I_ukir,I_imar,I_alir,I_omar,I_umar,
     & I_apar,I_itir,I_epar,I_otir,I_ipar,
     & I_utir,I_opar,I_avir,I_upar,I_evir,
     & I_arar,I_ivir,I_erar,I_ovir,I_irar,
     & I_uvir,I_orar,I_axir,I_urar,I_exir,
     & I_asar,I_oxir,I_esar,I_uxir,I_isar,
     & I_abor,I_osar,I_ebor,I_usar,I_ibor,
     & I_atar,I_obor,I_etar,I_ubor,I_itar,
     & I_ador,I_otar,I_edor,I_utar,I_idor,
     & I_avar,I_udor,I_afor,I_efor,I_evar,
     & I_ifor,I_ivar,I_isir,I_ovar,I_osir,
     & I_uvar,I_usir,I_axar,I_atir,I_exar,
     & I_elir,I_ixar,I_ilir,I_oxar,I_olir,
     & I_uxar,I_amir,I_aber,I_emir,I_eber,
     & I_imir,I_iber,I_omir,I_ober,I_umir,
     & I_uber,I_apir,I_ader,I_epir,I_eder,
     & I_ipir,I_ider,I_opir,I_oder,I_upir,
     & I_uder,I_erir,I_afer,I_irir,I_efer,
     & I_orir,I_ifer,I_urir,I_ofer,I_asir,
     & I_ufer,I_esir,I_aker,I_ofor,I_eker,
     & I_ufor,I_iker,I_akor,I_oker,I_ekor,
     & I_uker,I_ikor,I_aler,I_okor,I_eler,
     & I_alor,I_iler,I_elor,I_oler,I_ilor,
     & I_uler,I_olor,I_amer,I_ulor,I_emer,
     & I_amor,I_imer,I_emor,I_omer,I_imor,
     & I_umer,I_omor,I_aper,I_umor,I_eper,
     & I_efir,I_iper,I_ofir,I_oper,I_ulir,
     & I_uper,I_arir,I_arer,I_etir,I_erer,
     & I_ixir,I_irer,I_odor,I_orer,I_ukor,
     & I_urer,I_aser,I_eser,I_iser,I_apor,I_alar,
     & I_oser,I_user,I_ater,I_eter,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(9))
C FDA_10boats.fgi(  60, 244):���������� ���������� ������� �� ������� FDA50,BOAT09
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,L_ivebe,
     & L_uter,L_oter,I_ifir,I_epor,
     & I_ufir,I_ipor,I_akir,I_opor,I_ekir,
     & I_upor,I_ikir,I_aror,I_okir,I_eror,
     & I_ukir,I_iror,I_alir,I_oror,I_uror,
     & I_asor,I_itir,I_esor,I_otir,I_isor,
     & I_utir,I_osor,I_avir,I_usor,I_evir,
     & I_ator,I_ivir,I_etor,I_ovir,I_itor,
     & I_uvir,I_otor,I_axir,I_utor,I_exir,
     & I_avor,I_oxir,I_evor,I_uxir,I_ivor,
     & I_abor,I_ovor,I_ebor,I_uvor,I_ibor,
     & I_axor,I_obor,I_exor,I_ubor,I_ixor,
     & I_ador,I_oxor,I_edor,I_uxor,I_idor,
     & I_abur,I_udor,I_afor,I_efor,I_ebur,
     & I_ifor,I_ibur,I_isir,I_obur,I_osir,
     & I_ubur,I_usir,I_adur,I_atir,I_edur,
     & I_elir,I_idur,I_ilir,I_odur,I_olir,
     & I_udur,I_amir,I_afur,I_emir,I_efur,
     & I_imir,I_ifur,I_omir,I_ofur,I_umir,
     & I_ufur,I_apir,I_akur,I_epir,I_ekur,
     & I_ipir,I_ikur,I_opir,I_okur,I_upir,
     & I_ukur,I_erir,I_alur,I_irir,I_elur,
     & I_orir,I_ilur,I_urir,I_olur,I_asir,
     & I_ulur,I_esir,I_amur,I_ofor,I_emur,
     & I_ufor,I_imur,I_akor,I_omur,I_ekor,
     & I_umur,I_ikor,I_apur,I_okor,I_epur,
     & I_alor,I_ipur,I_elor,I_opur,I_ilor,
     & I_upur,I_olor,I_arur,I_ulor,I_erur,
     & I_amor,I_irur,I_emor,I_orur,I_imor,
     & I_urur,I_omor,I_asur,I_umor,I_esur,
     & I_efir,I_isur,I_ofir,I_osur,I_ulir,
     & I_usur,I_arir,I_atur,I_etir,I_etur,
     & I_ixir,I_itur,I_odor,I_otur,I_ukor,
     & I_utur,I_avur,I_evur,I_ivur,I_apor,I_iter,
     & I_ovur,I_uvur,I_axur,I_exur,L_udir,
     & L_oxer,L_edir,L_afir,
     & L_uxer,L_abir,L_ixer,
     & L_aver,L_ever,L_iver,
     & L_over,L_uver,L_axer,
     & L_exer,L_ebir,L_ibir,
     & L_obir,L_ubir,L_adir,
     & L_idir,L_edas,L_odir,I_(10))
C FDA_10boats.fgi(  40, 244):���������� ���������� ������� �� ������� FDA50,BOAT10
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_elis,I_ilis,
     & I_olis,I_ulis,I_amis,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_okes,R_efes,L_ores,L_udobe,
     & L_abibe,L_uxebe,R_ukes,R_ifes,
     & L_ures,L_oxebe,R_ales,R_ofes,
     & L_ases,I_eses,I_ises,I_oses,I_uses,I_ates,
     & INT(I_uvibe,4),I_(1),L_axibe,I_esas,R_etes,
     & REAL(R_atibe,4),R_ites,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_otes,R_utes,R_aves,
     & REAL(R_exibe,4),R_eves,REAL(R_ixibe,4),R_akes,
     & R_odes,L_ares,L_ufes,L_afobe,
     & REAL(R_efobe,4),R_asas,R_oves,R_uves,R_axes,
     & REAL(R_emibe,4),R_exes,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_ixes,REAL(R_ilibe,4),
     & R_oxes,R_ekes,R_udes,L_eres,R_uxes,R_abis,
     & R_ebis,L_otas,R_ibis,R_obis,
     & REAL(R_odibe,4),R_ubis,R_ikes,R_afes,L_ires,
     & R_adis,R_edis,R_idis,R_odis,R_udis,
     & REAL(R_ofibe,4),R_afis,L_utas,L_eles,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_efis,
     & R_ifis,R_ofis,REAL(R_okibe,4),R_ufis,
     & L_avas,L_iles,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_akis,R_ekis,
     & R_ikis,R_okis,REAL(R_obibe,4),R_ukis,
     & L_itas,L_idibe,L_afibe,L_oles,
     & L_ules,L_ibibe,R_efas,R_omes,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_umes,I_osas,R_omas,R_apes,R_alis,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_ives,R_ulas,C20_apas
     &)
C FDA_10boats.fgi( 219, 276):���������� �������,BOAT01
      I_ase=I_(1)
C FDA_10boats.fgi( 219, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_isid
     &,
     & I_ekil,R_otel,I_osid,I_itel,
     & R_ukil,I_atid,I_okil,I_etid,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_oled,
     & R_utel,I_(11),I_uled,
     & R_avel,I_(12),R_afil,
     & I_(20),I_amed,R_oxel,
     & I_(21),I_emed,R_uxel,
     & I_(22),I_imed,R_efil,I_(23),
     & I_omed,R_ufil,I_(24),I_umed,
     & R_ifil,I_(25),I_aped,
     & R_akil,I_(26),I_eped,
     & R_ofil,I_(27),I_iped,I_oped,
     & I_uped,I_ared,I_ered,I_ired,I_ored,I_ured,
     & I_ased,I_esed,I_ised,I_osed,I_used,I_ated,
     & I_eted,I_ited,I_oted,I_uted,I_aved,I_eved,
     & I_ived,I_oved,I_uved,I_axed,I_exed,I_ixed,
     & I_oxed,I_uxed,I_abid,I_ebid,I_ibid,I_obid,
     & I_ubid,I_adid,I_edid,I_idid,I_odid,I_udid,
     & I_afid,I_efid,I_ifid,I_ofid,I_ufid,I_akid,
     & I_ekid,I_ikid,I_okid,I_ukid,I_alid,I_elid,
     & I_ilid,I_olid,I_ulid,I_amid,I_emid,I_imid,
     & I_omid,I_umid,I_apid,I_epid,I_ipid,I_opid,
     & I_upid,I_arid,I_erid,I_irid,I_orid,I_urid,
     & I_asid,I_esid,INT(I_ase,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_asas,4),
     & R_ekel,L_uked,L_oked,L_iked,L_aled,L_eled,L_iled,
     & L_ofed,L_ifed,L_efed,L_ufed,L_aked,L_eked,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),1,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 220, 230):���������� ���������� ������� �� ������� FDA60,BOAT01
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_isus,I_osus,
     & I_usus,I_atus,I_etus,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_uros,R_ipos,L_uxos,L_udobe,
     & L_abibe,L_uxebe,R_asos,R_opos,
     & L_abus,L_oxebe,R_esos,R_upos,
     & L_ebus,I_ibus,I_obus,I_ubus,I_adus,I_edus,
     & INT(I_uvibe,4),I_(2),L_axibe,I_ibos,R_idus,
     & REAL(R_atibe,4),R_odus,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_udus,R_afus,R_efus,
     & REAL(R_exibe,4),R_ifus,REAL(R_ixibe,4),R_eros,
     & R_umos,L_exos,L_aros,L_afobe,
     & REAL(R_efobe,4),R_ebos,R_ufus,R_akus,R_ekus,
     & REAL(R_emibe,4),R_ikus,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_okus,REAL(R_ilibe,4),
     & R_ukus,R_iros,R_apos,L_ixos,R_alus,R_elus,
     & R_ilus,L_udos,R_olus,R_ulus,
     & REAL(R_odibe,4),R_amus,R_oros,R_epos,L_oxos,
     & R_emus,R_imus,R_omus,R_umus,R_apus,
     & REAL(R_ofibe,4),R_epus,L_afos,L_isos,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_ipus,
     & R_opus,R_upus,REAL(R_okibe,4),R_arus,
     & L_efos,L_osos,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_erus,R_irus,
     & R_orus,R_urus,REAL(R_obibe,4),R_asus,
     & L_odos,L_idibe,L_afibe,L_usos,
     & L_atos,L_ibibe,R_ipis,R_utos,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_avos,I_ubos,R_utis,R_evos,R_esus,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_ofus,R_atis,C20_evis
     &)
C FDA_10boats.fgi( 199, 276):���������� �������,BOAT02
      I_ure=I_(2)
C FDA_10boats.fgi( 199, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_okud
     &,
     & I_ekil,R_otel,I_ukud,I_itel,
     & R_ukil,I_elud,I_okil,I_ilud,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_uxid,
     & R_utel,I_(11),I_abod,
     & R_avel,I_(12),R_afil,
     & I_(20),I_ebod,R_oxel,
     & I_(21),I_ibod,R_uxel,
     & I_(22),I_obod,R_efil,I_(23),
     & I_ubod,R_ufil,I_(24),I_adod,
     & R_ifil,I_(25),I_edod,
     & R_akil,I_(26),I_idod,
     & R_ofil,I_(27),I_odod,I_udod,
     & I_afod,I_efod,I_ifod,I_ofod,I_ufod,I_akod,
     & I_ekod,I_ikod,I_okod,I_ukod,I_alod,I_elod,
     & I_ilod,I_olod,I_ulod,I_amod,I_emod,I_imod,
     & I_omod,I_umod,I_apod,I_epod,I_ipod,I_opod,
     & I_upod,I_arod,I_erod,I_irod,I_orod,I_urod,
     & I_asod,I_esod,I_isod,I_osod,I_usod,I_atod,
     & I_etod,I_itod,I_otod,I_utod,I_avod,I_evod,
     & I_ivod,I_ovod,I_uvod,I_axod,I_exod,I_ixod,
     & I_oxod,I_uxod,I_abud,I_ebud,I_ibud,I_obud,
     & I_ubud,I_adud,I_edud,I_idud,I_odud,I_udud,
     & I_afud,I_efud,I_ifud,I_ofud,I_ufud,I_akud,
     & I_ekud,I_ikud,INT(I_ure,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_ebos,4),
     & R_ekel,L_axid,L_uvid,L_ovid,L_exid,L_ixid,L_oxid,
     & L_utid,L_otid,L_itid,L_avid,L_evid,L_ivid,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),2,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 200, 230):���������� ���������� ������� �� ������� FDA60,BOAT02
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_obit,I_ubit,
     & I_adit,I_edit,I_idit,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_abet,R_ovat,L_alet,L_udobe,
     & L_abibe,L_uxebe,R_ebet,R_uvat,
     & L_elet,L_oxebe,R_ibet,R_axat,
     & L_ilet,I_olet,I_ulet,I_amet,I_emet,I_imet,
     & INT(I_uvibe,4),I_(3),L_axibe,I_olat,R_omet,
     & REAL(R_atibe,4),R_umet,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_apet,R_epet,R_ipet,
     & REAL(R_exibe,4),R_opet,REAL(R_ixibe,4),R_ixat,
     & R_avat,L_iket,L_exat,L_afobe,
     & REAL(R_efobe,4),R_ilat,R_aret,R_eret,R_iret,
     & REAL(R_emibe,4),R_oret,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_uret,REAL(R_ilibe,4),
     & R_aset,R_oxat,R_evat,L_oket,R_eset,R_iset,
     & R_oset,L_apat,R_uset,R_atet,
     & REAL(R_odibe,4),R_etet,R_uxat,R_ivat,L_uket,
     & R_itet,R_otet,R_utet,R_avet,R_evet,
     & REAL(R_ofibe,4),R_ivet,L_epat,L_obet,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_ovet,
     & R_uvet,R_axet,REAL(R_okibe,4),R_exet,
     & L_ipat,L_ubet,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_ixet,R_oxet,
     & R_uxet,R_abit,REAL(R_obibe,4),R_ebit,
     & L_umat,L_idibe,L_afibe,L_adet,
     & L_edet,L_ibibe,R_ovus,R_afet,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_efet,I_amat,R_afat,R_ifet,R_ibit,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_upet,R_edat,C20_ifat
     &)
C FDA_10boats.fgi( 179, 276):���������� �������,BOAT03
      I_ore=I_(3)
C FDA_10boats.fgi( 179, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_uvaf
     &,
     & I_ekil,R_otel,I_axaf,I_itel,
     & R_ukil,I_ixaf,I_okil,I_oxaf,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_arud,
     & R_utel,I_(11),I_erud,
     & R_avel,I_(12),R_afil,
     & I_(20),I_irud,R_oxel,
     & I_(21),I_orud,R_uxel,
     & I_(22),I_urud,R_efil,I_(23),
     & I_asud,R_ufil,I_(24),I_esud,
     & R_ifil,I_(25),I_isud,
     & R_akil,I_(26),I_osud,
     & R_ofil,I_(27),I_usud,I_atud,
     & I_etud,I_itud,I_otud,I_utud,I_avud,I_evud,
     & I_ivud,I_ovud,I_uvud,I_axud,I_exud,I_ixud,
     & I_oxud,I_uxud,I_abaf,I_ebaf,I_ibaf,I_obaf,
     & I_ubaf,I_adaf,I_edaf,I_idaf,I_odaf,I_udaf,
     & I_afaf,I_efaf,I_ifaf,I_ofaf,I_ufaf,I_akaf,
     & I_ekaf,I_ikaf,I_okaf,I_ukaf,I_alaf,I_elaf,
     & I_ilaf,I_olaf,I_ulaf,I_amaf,I_emaf,I_imaf,
     & I_omaf,I_umaf,I_apaf,I_epaf,I_ipaf,I_opaf,
     & I_upaf,I_araf,I_eraf,I_iraf,I_oraf,I_uraf,
     & I_asaf,I_esaf,I_isaf,I_osaf,I_usaf,I_ataf,
     & I_etaf,I_itaf,I_otaf,I_utaf,I_avaf,I_evaf,
     & I_ivaf,I_ovaf,INT(I_ore,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_ilat,4),
     & R_ekel,L_epud,L_apud,L_umud,L_ipud,L_opud,L_upud,
     & L_amud,L_ulud,L_olud,L_emud,L_imud,L_omud,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),3,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 180, 230):���������� ���������� ������� �� ������� FDA60,BOAT03
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_ulut,I_amut,
     & I_emut,I_imut,I_omut,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_elot,R_ufot,L_esot,L_udobe,
     & L_abibe,L_uxebe,R_ilot,R_akot,
     & L_isot,L_oxebe,R_olot,R_ekot,
     & L_osot,I_usot,I_atot,I_etot,I_itot,I_otot,
     & INT(I_uvibe,4),I_(4),L_axibe,I_usit,R_utot,
     & REAL(R_atibe,4),R_avot,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_evot,R_ivot,R_ovot,
     & REAL(R_exibe,4),R_uvot,REAL(R_ixibe,4),R_okot,
     & R_efot,L_orot,L_ikot,L_afobe,
     & REAL(R_efobe,4),R_osit,R_exot,R_ixot,R_oxot,
     & REAL(R_emibe,4),R_uxot,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_abut,REAL(R_ilibe,4),
     & R_ebut,R_ukot,R_ifot,L_urot,R_ibut,R_obut,
     & R_ubut,L_evit,R_adut,R_edut,
     & REAL(R_odibe,4),R_idut,R_alot,R_ofot,L_asot,
     & R_odut,R_udut,R_afut,R_efut,R_ifut,
     & REAL(R_ofibe,4),R_ofut,L_ivit,L_ulot,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_ufut,
     & R_akut,R_ekut,REAL(R_okibe,4),R_ikut,
     & L_ovit,L_amot,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_okut,R_ukut,
     & R_alut,R_elut,REAL(R_obibe,4),R_ilut,
     & L_avit,L_idibe,L_afibe,L_emot,
     & L_imot,L_ibibe,R_ufit,R_epot,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_ipot,I_etit,R_epit,R_opot,R_olut,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_axot,R_imit,C20_opit
     &)
C FDA_10boats.fgi( 159, 276):���������� �������,BOAT04
      I_ire=I_(4)
C FDA_10boats.fgi( 159, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_apif
     &,
     & I_ekil,R_otel,I_epif,I_itel,
     & R_ukil,I_opif,I_okil,I_upif,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_efef,
     & R_utel,I_(11),I_ifef,
     & R_avel,I_(12),R_afil,
     & I_(20),I_ofef,R_oxel,
     & I_(21),I_ufef,R_uxel,
     & I_(22),I_akef,R_efil,I_(23),
     & I_ekef,R_ufil,I_(24),I_ikef,
     & R_ifil,I_(25),I_okef,
     & R_akil,I_(26),I_ukef,
     & R_ofil,I_(27),I_alef,I_elef,
     & I_ilef,I_olef,I_ulef,I_amef,I_emef,I_imef,
     & I_omef,I_umef,I_apef,I_epef,I_ipef,I_opef,
     & I_upef,I_aref,I_eref,I_iref,I_oref,I_uref,
     & I_asef,I_esef,I_isef,I_osef,I_usef,I_atef,
     & I_etef,I_itef,I_otef,I_utef,I_avef,I_evef,
     & I_ivef,I_ovef,I_uvef,I_axef,I_exef,I_ixef,
     & I_oxef,I_uxef,I_abif,I_ebif,I_ibif,I_obif,
     & I_ubif,I_adif,I_edif,I_idif,I_odif,I_udif,
     & I_afif,I_efif,I_ifif,I_ofif,I_ufif,I_akif,
     & I_ekif,I_ikif,I_okif,I_ukif,I_alif,I_elif,
     & I_ilif,I_olif,I_ulif,I_amif,I_emif,I_imif,
     & I_omif,I_umif,INT(I_ire,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_osit,4),
     & R_ekel,L_idef,L_edef,L_adef,L_odef,L_udef,L_afef,
     & L_ebef,L_abef,L_uxaf,L_ibef,L_obef,L_ubef,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),4,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 160, 230):���������� ���������� ������� �� ������� FDA60,BOAT04
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_atev,I_etev,
     & I_itev,I_otev,I_utev,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_isav,R_arav,L_ibev,L_udobe,
     & L_abibe,L_uxebe,R_osav,R_erav,
     & L_obev,L_oxebe,R_usav,R_irav,
     & L_ubev,I_adev,I_edev,I_idev,I_odev,I_udev,
     & INT(I_uvibe,4),I_(5),L_axibe,I_adav,R_afev,
     & REAL(R_atibe,4),R_efev,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_ifev,R_ofev,R_ufev,
     & REAL(R_exibe,4),R_akev,REAL(R_ixibe,4),R_urav,
     & R_ipav,L_uxav,L_orav,L_afobe,
     & REAL(R_efobe,4),R_ubav,R_ikev,R_okev,R_ukev,
     & REAL(R_emibe,4),R_alev,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_elev,REAL(R_ilibe,4),
     & R_ilev,R_asav,R_opav,L_abev,R_olev,R_ulev,
     & R_amev,L_ifav,R_emev,R_imev,
     & REAL(R_odibe,4),R_omev,R_esav,R_upav,L_ebev,
     & R_umev,R_apev,R_epev,R_ipev,R_opev,
     & REAL(R_ofibe,4),R_upev,L_ofav,L_atav,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_arev,
     & R_erev,R_irev,REAL(R_okibe,4),R_orev,
     & L_ufav,L_etav,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_urev,R_asev,
     & R_esev,R_isev,REAL(R_obibe,4),R_osev,
     & L_efav,L_idibe,L_afibe,L_itav,
     & L_otav,L_ibibe,R_arut,R_ivav,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_ovav,I_idav,R_ivut,R_uvav,R_usev,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_ekev,R_otut,C20_uvut
     &)
C FDA_10boats.fgi( 139, 276):���������� �������,BOAT05
      I_ere=I_(5)
C FDA_10boats.fgi( 139, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_eduf
     &,
     & I_ekil,R_otel,I_iduf,I_itel,
     & R_ukil,I_uduf,I_okil,I_afuf,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_itif,
     & R_utel,I_(11),I_otif,
     & R_avel,I_(12),R_afil,
     & I_(20),I_utif,R_oxel,
     & I_(21),I_avif,R_uxel,
     & I_(22),I_evif,R_efil,I_(23),
     & I_ivif,R_ufil,I_(24),I_ovif,
     & R_ifil,I_(25),I_uvif,
     & R_akil,I_(26),I_axif,
     & R_ofil,I_(27),I_exif,I_ixif,
     & I_oxif,I_uxif,I_abof,I_ebof,I_ibof,I_obof,
     & I_ubof,I_adof,I_edof,I_idof,I_odof,I_udof,
     & I_afof,I_efof,I_ifof,I_ofof,I_ufof,I_akof,
     & I_ekof,I_ikof,I_okof,I_ukof,I_alof,I_elof,
     & I_ilof,I_olof,I_ulof,I_amof,I_emof,I_imof,
     & I_omof,I_umof,I_apof,I_epof,I_ipof,I_opof,
     & I_upof,I_arof,I_erof,I_irof,I_orof,I_urof,
     & I_asof,I_esof,I_isof,I_osof,I_usof,I_atof,
     & I_etof,I_itof,I_otof,I_utof,I_avof,I_evof,
     & I_ivof,I_ovof,I_uvof,I_axof,I_exof,I_ixof,
     & I_oxof,I_uxof,I_abuf,I_ebuf,I_ibuf,I_obuf,
     & I_ubuf,I_aduf,INT(I_ere,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_ubav,4),
     & R_ekel,L_osif,L_isif,L_esif,L_usif,L_atif,L_etif,
     & L_irif,L_erif,L_arif,L_orif,L_urif,L_asif,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),5,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 140, 230):���������� ���������� ������� �� ������� FDA60,BOAT05
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_eduv,I_iduv,
     & I_oduv,I_uduv,I_afuv,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_obov,R_exiv,L_olov,L_udobe,
     & L_abibe,L_uxebe,R_ubov,R_ixiv,
     & L_ulov,L_oxebe,R_adov,R_oxiv,
     & L_amov,I_emov,I_imov,I_omov,I_umov,I_apov,
     & INT(I_uvibe,4),I_(6),L_axibe,I_emiv,R_epov,
     & REAL(R_atibe,4),R_ipov,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_opov,R_upov,R_arov,
     & REAL(R_exibe,4),R_erov,REAL(R_ixibe,4),R_abov,
     & R_oviv,L_alov,L_uxiv,L_afobe,
     & REAL(R_efobe,4),R_amiv,R_orov,R_urov,R_asov,
     & REAL(R_emibe,4),R_esov,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_isov,REAL(R_ilibe,4),
     & R_osov,R_ebov,R_uviv,L_elov,R_usov,R_atov,
     & R_etov,L_opiv,R_itov,R_otov,
     & REAL(R_odibe,4),R_utov,R_ibov,R_axiv,L_ilov,
     & R_avov,R_evov,R_ivov,R_ovov,R_uvov,
     & REAL(R_ofibe,4),R_axov,L_upiv,L_edov,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_exov,
     & R_ixov,R_oxov,REAL(R_okibe,4),R_uxov,
     & L_ariv,L_idov,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_abuv,R_ebuv,
     & R_ibuv,R_obuv,REAL(R_obibe,4),R_ubuv,
     & L_ipiv,L_idibe,L_afibe,L_odov,
     & L_udov,L_ibibe,R_exev,R_ofov,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_ufov,I_omiv,R_ofiv,R_akov,R_aduv,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_irov,R_udiv,C20_akiv
     &)
C FDA_10boats.fgi( 119, 276):���������� �������,BOAT06
      I_are=I_(6)
C FDA_10boats.fgi( 119, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_isak
     &,
     & I_ekil,R_otel,I_osak,I_itel,
     & R_ukil,I_atak,I_okil,I_etak,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_oluf,
     & R_utel,I_(11),I_uluf,
     & R_avel,I_(12),R_afil,
     & I_(20),I_amuf,R_oxel,
     & I_(21),I_emuf,R_uxel,
     & I_(22),I_imuf,R_efil,I_(23),
     & I_omuf,R_ufil,I_(24),I_umuf,
     & R_ifil,I_(25),I_apuf,
     & R_akil,I_(26),I_epuf,
     & R_ofil,I_(27),I_ipuf,I_opuf,
     & I_upuf,I_aruf,I_eruf,I_iruf,I_oruf,I_uruf,
     & I_asuf,I_esuf,I_isuf,I_osuf,I_usuf,I_atuf,
     & I_etuf,I_ituf,I_otuf,I_utuf,I_avuf,I_evuf,
     & I_ivuf,I_ovuf,I_uvuf,I_axuf,I_exuf,I_ixuf,
     & I_oxuf,I_uxuf,I_abak,I_ebak,I_ibak,I_obak,
     & I_ubak,I_adak,I_edak,I_idak,I_odak,I_udak,
     & I_afak,I_efak,I_ifak,I_ofak,I_ufak,I_akak,
     & I_ekak,I_ikak,I_okak,I_ukak,I_alak,I_elak,
     & I_ilak,I_olak,I_ulak,I_amak,I_emak,I_imak,
     & I_omak,I_umak,I_apak,I_epak,I_ipak,I_opak,
     & I_upak,I_arak,I_erak,I_irak,I_orak,I_urak,
     & I_asak,I_esak,INT(I_are,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_amiv,4),
     & R_ekel,L_ukuf,L_okuf,L_ikuf,L_aluf,L_eluf,L_iluf,
     & L_ofuf,L_ifuf,L_efuf,L_ufuf,L_akuf,L_ekuf,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),6,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 120, 230):���������� ���������� ������� �� ������� FDA60,BOAT06
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_imex,I_omex,
     & I_umex,I_apex,I_epex,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_ulax,R_ikax,L_usax,L_udobe,
     & L_abibe,L_uxebe,R_amax,R_okax,
     & L_atax,L_oxebe,R_emax,R_ukax,
     & L_etax,I_itax,I_otax,I_utax,I_avax,I_evax,
     & INT(I_uvibe,4),I_(7),L_axibe,I_ituv,R_ivax,
     & REAL(R_atibe,4),R_ovax,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_uvax,R_axax,R_exax,
     & REAL(R_exibe,4),R_ixax,REAL(R_ixibe,4),R_elax,
     & R_ufax,L_esax,L_alax,L_afobe,
     & REAL(R_efobe,4),R_etuv,R_uxax,R_abex,R_ebex,
     & REAL(R_emibe,4),R_ibex,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_obex,REAL(R_ilibe,4),
     & R_ubex,R_ilax,R_akax,L_isax,R_adex,R_edex,
     & R_idex,L_uvuv,R_odex,R_udex,
     & REAL(R_odibe,4),R_afex,R_olax,R_ekax,L_osax,
     & R_efex,R_ifex,R_ofex,R_ufex,R_akex,
     & REAL(R_ofibe,4),R_ekex,L_axuv,L_imax,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_ikex,
     & R_okex,R_ukex,REAL(R_okibe,4),R_alex,
     & L_exuv,L_omax,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_elex,R_ilex,
     & R_olex,R_ulex,REAL(R_obibe,4),R_amex,
     & L_ovuv,L_idibe,L_afibe,L_umax,
     & L_apax,L_ibibe,R_ikuv,R_upax,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_arax,I_utuv,R_upuv,R_erax,R_emex,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_oxax,R_apuv,C20_eruv
     &)
C FDA_10boats.fgi(  99, 276):���������� �������,BOAT07
      I_upe=I_(7)
C FDA_10boats.fgi(  99, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_okik
     &,
     & I_ekil,R_otel,I_ukik,I_itel,
     & R_ukil,I_elik,I_okil,I_ilik,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_uxak,
     & R_utel,I_(11),I_abek,
     & R_avel,I_(12),R_afil,
     & I_(20),I_ebek,R_oxel,
     & I_(21),I_ibek,R_uxel,
     & I_(22),I_obek,R_efil,I_(23),
     & I_ubek,R_ufil,I_(24),I_adek,
     & R_ifil,I_(25),I_edek,
     & R_akil,I_(26),I_idek,
     & R_ofil,I_(27),I_odek,I_udek,
     & I_afek,I_efek,I_ifek,I_ofek,I_ufek,I_akek,
     & I_ekek,I_ikek,I_okek,I_ukek,I_alek,I_elek,
     & I_ilek,I_olek,I_ulek,I_amek,I_emek,I_imek,
     & I_omek,I_umek,I_apek,I_epek,I_ipek,I_opek,
     & I_upek,I_arek,I_erek,I_irek,I_orek,I_urek,
     & I_asek,I_esek,I_isek,I_osek,I_usek,I_atek,
     & I_etek,I_itek,I_otek,I_utek,I_avek,I_evek,
     & I_ivek,I_ovek,I_uvek,I_axek,I_exek,I_ixek,
     & I_oxek,I_uxek,I_abik,I_ebik,I_ibik,I_obik,
     & I_ubik,I_adik,I_edik,I_idik,I_odik,I_udik,
     & I_afik,I_efik,I_ifik,I_ofik,I_ufik,I_akik,
     & I_ekik,I_ikik,INT(I_upe,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_etuv,4),
     & R_ekel,L_axak,L_uvak,L_ovak,L_exak,L_ixak,L_oxak,
     & L_utak,L_otak,L_itak,L_avak,L_evak,L_ivak,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),7,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi( 100, 230):���������� ���������� ������� �� ������� FDA60,BOAT07
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_otox,I_utox,
     & I_avox,I_evox,I_ivox,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_atix,R_orix,L_adox,L_udobe,
     & L_abibe,L_uxebe,R_etix,R_urix,
     & L_edox,L_oxebe,R_itix,R_asix,
     & L_idox,I_odox,I_udox,I_afox,I_efox,I_ifox,
     & INT(I_uvibe,4),I_(8),L_axibe,I_odix,R_ofox,
     & REAL(R_atibe,4),R_ufox,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_akox,R_ekox,R_ikox,
     & REAL(R_exibe,4),R_okox,REAL(R_ixibe,4),R_isix,
     & R_arix,L_ibox,L_esix,L_afobe,
     & REAL(R_efobe,4),R_idix,R_alox,R_elox,R_ilox,
     & REAL(R_emibe,4),R_olox,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_ulox,REAL(R_ilibe,4),
     & R_amox,R_osix,R_erix,L_obox,R_emox,R_imox,
     & R_omox,L_akix,R_umox,R_apox,
     & REAL(R_odibe,4),R_epox,R_usix,R_irix,L_ubox,
     & R_ipox,R_opox,R_upox,R_arox,R_erox,
     & REAL(R_ofibe,4),R_irox,L_ekix,L_otix,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_orox,
     & R_urox,R_asox,REAL(R_okibe,4),R_esox,
     & L_ikix,L_utix,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_isox,R_osox,
     & R_usox,R_atox,REAL(R_obibe,4),R_etox,
     & L_ufix,L_idibe,L_afibe,L_avix,
     & L_evix,L_ibibe,R_orex,R_axix,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_exix,I_afix,R_axex,R_ixix,R_itox,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_ukox,R_evex,C20_ixex
     &)
C FDA_10boats.fgi(  79, 276):���������� �������,BOAT08
      I_ope=I_(8)
C FDA_10boats.fgi(  79, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_uvok
     &,
     & I_ekil,R_otel,I_axok,I_itel,
     & R_ukil,I_ixok,I_okil,I_oxok,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_arik,
     & R_utel,I_(11),I_erik,
     & R_avel,I_(12),R_afil,
     & I_(20),I_irik,R_oxel,
     & I_(21),I_orik,R_uxel,
     & I_(22),I_urik,R_efil,I_(23),
     & I_asik,R_ufil,I_(24),I_esik,
     & R_ifil,I_(25),I_isik,
     & R_akil,I_(26),I_osik,
     & R_ofil,I_(27),I_usik,I_atik,
     & I_etik,I_itik,I_otik,I_utik,I_avik,I_evik,
     & I_ivik,I_ovik,I_uvik,I_axik,I_exik,I_ixik,
     & I_oxik,I_uxik,I_abok,I_ebok,I_ibok,I_obok,
     & I_ubok,I_adok,I_edok,I_idok,I_odok,I_udok,
     & I_afok,I_efok,I_ifok,I_ofok,I_ufok,I_akok,
     & I_ekok,I_ikok,I_okok,I_ukok,I_alok,I_elok,
     & I_ilok,I_olok,I_ulok,I_amok,I_emok,I_imok,
     & I_omok,I_umok,I_apok,I_epok,I_ipok,I_opok,
     & I_upok,I_arok,I_erok,I_irok,I_orok,I_urok,
     & I_asok,I_esok,I_isok,I_osok,I_usok,I_atok,
     & I_etok,I_itok,I_otok,I_utok,I_avok,I_evok,
     & I_ivok,I_ovok,INT(I_ope,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_idix,4),
     & R_ekel,L_epik,L_apik,L_umik,L_ipik,L_opik,L_upik,
     & L_amik,L_ulik,L_olik,L_emik,L_imik,L_omik,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),8,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi(  80, 230):���������� ���������� ������� �� ������� FDA60,BOAT08
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_udebe,I_afebe,
     & I_efebe,I_ifebe,I_ofebe,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_edabe,R_uxux,L_emabe,L_udobe,
     & L_abibe,L_uxebe,R_idabe,R_ababe,
     & L_imabe,L_oxebe,R_odabe,R_ebabe,
     & L_omabe,I_umabe,I_apabe,I_epabe,I_ipabe,I_opabe,
     & INT(I_uvibe,4),I_(9),L_axibe,I_umux,R_upabe,
     & REAL(R_atibe,4),R_arabe,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_erabe,R_irabe,R_orabe,
     & REAL(R_exibe,4),R_urabe,REAL(R_ixibe,4),R_obabe,
     & R_exux,L_olabe,L_ibabe,L_afobe,
     & REAL(R_efobe,4),R_omux,R_esabe,R_isabe,R_osabe,
     & REAL(R_emibe,4),R_usabe,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_atabe,REAL(R_ilibe,4),
     & R_etabe,R_ubabe,R_ixux,L_ulabe,R_itabe,R_otabe,
     & R_utabe,L_erux,R_avabe,R_evabe,
     & REAL(R_odibe,4),R_ivabe,R_adabe,R_oxux,L_amabe,
     & R_ovabe,R_uvabe,R_axabe,R_exabe,R_ixabe,
     & REAL(R_ofibe,4),R_oxabe,L_irux,L_udabe,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_uxabe,
     & R_abebe,R_ebebe,REAL(R_okibe,4),R_ibebe,
     & L_orux,L_afabe,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_obebe,R_ubebe,
     & R_adebe,R_edebe,REAL(R_obibe,4),R_idebe,
     & L_arux,L_idibe,L_afibe,L_efabe,
     & L_ifabe,L_ibibe,R_uxox,R_ekabe,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_ikabe,I_epux,R_ekux,R_okabe,R_odebe,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_asabe,R_ifux,C20_okux
     &)
C FDA_10boats.fgi(  59, 276):���������� �������,BOAT09
      I_ipe=I_(9)
C FDA_10boats.fgi(  59, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_apal
     &,
     & I_ekil,R_otel,I_epal,I_itel,
     & R_ukil,I_opal,I_okil,I_upal,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_efuk,
     & R_utel,I_(11),I_ifuk,
     & R_avel,I_(12),R_afil,
     & I_(20),I_ofuk,R_oxel,
     & I_(21),I_ufuk,R_uxel,
     & I_(22),I_akuk,R_efil,I_(23),
     & I_ekuk,R_ufil,I_(24),I_ikuk,
     & R_ifil,I_(25),I_okuk,
     & R_akil,I_(26),I_ukuk,
     & R_ofil,I_(27),I_aluk,I_eluk,
     & I_iluk,I_oluk,I_uluk,I_amuk,I_emuk,I_imuk,
     & I_omuk,I_umuk,I_apuk,I_epuk,I_ipuk,I_opuk,
     & I_upuk,I_aruk,I_eruk,I_iruk,I_oruk,I_uruk,
     & I_asuk,I_esuk,I_isuk,I_osuk,I_usuk,I_atuk,
     & I_etuk,I_ituk,I_otuk,I_utuk,I_avuk,I_evuk,
     & I_ivuk,I_ovuk,I_uvuk,I_axuk,I_exuk,I_ixuk,
     & I_oxuk,I_uxuk,I_abal,I_ebal,I_ibal,I_obal,
     & I_ubal,I_adal,I_edal,I_idal,I_odal,I_udal,
     & I_afal,I_efal,I_ifal,I_ofal,I_ufal,I_akal,
     & I_ekal,I_ikal,I_okal,I_ukal,I_alal,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_emal,I_imal,
     & I_omal,I_umal,INT(I_ipe,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_omux,4),
     & R_ekel,L_iduk,L_eduk,L_aduk,L_oduk,L_uduk,L_afuk,
     & L_ebuk,L_abuk,L_uxok,L_ibuk,L_obuk,L_ubuk,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),9,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi(  60, 230):���������� ���������� ������� �� ������� FDA60,BOAT09
      Call LODOCHKA_HANDLER(deltat,L_orebe,L_ebobe,
     & L_abobe,L_etibe,L_itibe,L_oxibe,
     & L_uxibe,L_esibe,I_umube,I_apube,
     & I_epube,I_ipube,I_opube,INT(I_avibe,4),
     & L_isibe,L_ivebe,REAL(R_ifobe,4),
     & L_edobe,L_obobe,
     & L_ubobe,L_idobe,L_ibobe,
     & L_adobe,R_emobe,R_ukobe,L_etobe,L_udobe,
     & L_abibe,L_uxebe,R_imobe,R_alobe,
     & L_itobe,L_oxebe,R_omobe,R_elobe,
     & L_otobe,I_utobe,I_avobe,I_evobe,I_ivobe,I_ovobe,
     & INT(I_uvibe,4),I_(10),L_axibe,I_evebe,R_uvobe,
     & REAL(R_atibe,4),R_axobe,REAL(R_usibe,4),
     & REAL(R_osibe,4),R_exobe,R_ixobe,R_oxobe,
     & REAL(R_exibe,4),R_uxobe,REAL(R_ixibe,4),R_olobe,
     & R_ekobe,L_osobe,L_ilobe,L_afobe,
     & REAL(R_efobe,4),R_avebe,R_ebube,R_ibube,R_obube,
     & REAL(R_emibe,4),R_ubube,REAL(R_ulibe,4),
     & REAL(R_elibe,4),R_adube,REAL(R_ilibe,4),
     & R_edube,R_ulobe,R_ikobe,L_usobe,R_idube,R_odube,
     & R_udube,L_udibe,R_afube,R_efube,
     & REAL(R_odibe,4),R_ifube,R_amobe,R_okobe,L_atobe,
     & R_ofube,R_ufube,R_akube,R_ekube,R_ikube,
     & REAL(R_ofibe,4),R_okube,L_ufibe,L_umobe,
     & L_ifibe,REAL(R_efibe,4),L_akibe,R_ukube,
     & R_alube,R_elube,REAL(R_okibe,4),R_ilube,
     & L_ukibe,L_apobe,L_ikibe,
     & REAL(R_ekibe,4),L_alibe,R_olube,R_ulube,
     & R_amube,R_emube,REAL(R_obibe,4),R_imube,
     & L_ubibe,L_idibe,L_afibe,L_epobe,
     & L_ipobe,L_ibibe,R_alebe,R_erobe,
     & REAL(R_edibe,4),REAL(R_ebibe,4),L_adibe,
     & R_irobe,I_uvebe,R_irebe,R_orobe,R_omube,
     & REAL(R_otibe,4),REAL(R_ivibe,4),R_abube,R_opebe,C20_asebe
     &)
C FDA_10boats.fgi(  39, 276):���������� �������,BOAT10
      I_epe=I_(10)
C FDA_10boats.fgi(  39, 276):������-�������: ���������� ��� �������������� ������
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,R_ikil,I_urol
     &,
     & I_ekil,R_otel,I_asol,I_itel,
     & R_ukil,I_isol,I_okil,I_osol,
     & I_(13),R_evel,I_(14),
     & R_ivel,I_(15),R_ovel,
     & I_(16),R_uvel,I_(17),
     & R_axel,I_(18),R_exel,
     & I_(19),R_ixel,I_alil,
     & R_utel,I_(11),I_elil,
     & R_avel,I_(12),R_afil,
     & I_(20),I_ilil,R_oxel,
     & I_(21),I_olil,R_uxel,
     & I_(22),I_ulil,R_efil,I_(23),
     & I_amil,R_ufil,I_(24),I_emil,
     & R_ifil,I_(25),I_imil,
     & R_akil,I_(26),I_omil,
     & R_ofil,I_(27),I_umil,I_apil,
     & I_epil,I_ipil,I_opil,I_upil,I_aril,I_eril,
     & I_iril,I_oril,I_uril,I_asil,I_esil,I_isil,
     & I_osil,I_usil,I_atil,I_etil,I_itil,I_otil,
     & I_util,I_avil,I_evil,I_ivil,I_ovil,I_uvil,
     & I_axil,I_exil,I_ixil,I_oxil,I_uxil,I_abol,
     & I_ebol,I_ibol,I_obol,I_ubol,I_adol,I_edol,
     & I_idol,I_odol,I_udol,I_afol,I_efol,I_ifol,
     & I_ofol,I_ufol,I_akol,I_ekol,I_ikol,I_okol,
     & I_ukol,I_alol,I_elol,I_ilol,I_olol,I_ulol,
     & I_amol,I_emol,I_imol,I_omol,I_umol,I_apol,
     & I_epol,I_ipol,I_opol,I_upol,I_arol,I_erol,
     & I_irol,I_orol,INT(I_epe,4),R_eval,R_aval,
     & R_utal,R_otal,R_ital,R_elel,
     & R_alel,R_ukel,R_okel,R_ikel,R_ilel,
     & R_olel,R_ulel,R_amel,R_emel,
     & R_ival,R_oval,R_uval,R_axal,
     & R_exal,R_imel,R_omel,R_umel,
     & R_apel,R_epel,R_ixal,R_oxal,
     & R_uxal,R_abel,R_ebel,R_udil,
     & R_odil,R_idil,R_edil,
     & R_adil,R_ubil,R_obil,
     & R_ibil,R_ebil,R_abil,
     & R_ipel,R_opel,R_upel,R_arel,R_erel,
     & R_ibel,R_obel,R_ubel,R_adel,
     & R_edel,R_efel,R_afel,R_udel,
     & R_odel,R_idel,R_esel,R_asel,
     & R_urel,R_orel,R_irel,R_isel,R_osel,
     & R_usel,R_atel,R_etel,R_ifel,
     & R_ofel,R_ufel,R_akel,REAL(R_avebe,4),
     & R_ekel,L_osal,L_isal,L_esal,L_usal,L_atal,L_etal,
     & L_iral,L_eral,L_aral,L_oral,L_ural,L_asal,I_(62),
     & I_(61),I_(28),I_(29),I_(30),I_(31),I_(32),
     & I_(58),I_(59),I_(60),I_(67),I_(66),
     & I_(33),I_(34),I_(35),I_(36),I_(37),I_(63),
     & I_(64),I_(65),I_(72),I_(71),I_(38),
     & I_(39),I_(40),I_(41),I_(42),I_(68),I_(69),
     & I_(70),I_(77),I_(76),I_(43),I_(44),
     & I_(45),I_(46),I_(47),I_(73),I_(74),
     & I_(75),I_(82),I_(81),I_(48),I_(49),
     & I_(50),I_(51),I_(52),I_(78),I_(79),
     & I_(80),I_(87),I_(86),I_(53),I_(54),
     & I_(55),I_(56),I_(57),I_(83),I_(84),
     & I_(85),I_(88),I_(89),I_(90),
     & I_(91),I_(92),10,I_(93),
     & I_(94),I_(95),I_(96),
     & I_(97))
C FDA_10boats.fgi(  40, 230):���������� ���������� ������� �� ������� FDA60,BOAT10
      I_os=I_(58)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ulobi,INT(I_os,4),I_olobi
     &)
C FDA_mechanic_vlv.fgi( 866, 781):���������� ���������� �������,CIL_PKS6_N5
      I_is=I_(59)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_imobi,INT(I_is,4),I_emobi
     &)
C FDA_mechanic_vlv.fgi( 846, 781):���������� ���������� �������,CIL_PKS6_N4
      I_es=I_(60)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_apobi,INT(I_es,4),I_umobi
     &)
C FDA_mechanic_vlv.fgi( 826, 781):���������� ���������� �������,CIL_PKS6_N3
      I_as=I_(61)
C FDA_10boats.fgi( 160, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_opobi,INT(I_as,4),I_ipobi
     &)
C FDA_mechanic_vlv.fgi( 806, 781):���������� ���������� �������,CIL_PKS6_N2
      I_ur=I_(62)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_erobi,INT(I_ur,4),I_arobi
     &)
C FDA_mechanic_vlv.fgi( 786, 781):���������� ���������� �������,CIL_PKS6_N1
      I_or=I_(63)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_urobi,INT(I_or,4),I_orobi
     &)
C FDA_mechanic_vlv.fgi( 766, 781):���������� ���������� �������,CIL_PKS5_N5
      I_ir=I_(64)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_isobi,INT(I_ir,4),I_esobi
     &)
C FDA_mechanic_vlv.fgi( 746, 781):���������� ���������� �������,CIL_PKS5_N4
      I_er=I_(65)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_atobi,INT(I_er,4),I_usobi
     &)
C FDA_mechanic_vlv.fgi( 726, 781):���������� ���������� �������,CIL_PKS5_N3
      I_ar=I_(66)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_otobi,INT(I_ar,4),I_itobi
     &)
C FDA_mechanic_vlv.fgi( 706, 781):���������� ���������� �������,CIL_PKS5_N2
      I_up=I_(67)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_evobi,INT(I_up,4),I_avobi
     &)
C FDA_mechanic_vlv.fgi( 686, 781):���������� ���������� �������,CIL_PKS5_N1
      I_op=I_(68)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_uvobi,INT(I_op,4),I_ovobi
     &)
C FDA_mechanic_vlv.fgi( 666, 781):���������� ���������� �������,CIL_PKS4_N5
      I_ip=I_(69)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ixobi,INT(I_ip,4),I_exobi
     &)
C FDA_mechanic_vlv.fgi( 646, 781):���������� ���������� �������,CIL_PKS4_N4
      I_ep=I_(70)
C FDA_10boats.fgi( 160, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_abubi,INT(I_ep,4),I_uxobi
     &)
C FDA_mechanic_vlv.fgi( 626, 781):���������� ���������� �������,CIL_PKS4_N3
      I_ap=I_(71)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_obubi,INT(I_ap,4),I_ibubi
     &)
C FDA_mechanic_vlv.fgi( 606, 781):���������� ���������� �������,CIL_PKS4_N2
      I_um=I_(72)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_edubi,INT(I_um,4),I_adubi
     &)
C FDA_mechanic_vlv.fgi( 586, 781):���������� ���������� �������,CIL_PKS4_N1
      I_om=I_(73)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_udubi,INT(I_om,4),I_odubi
     &)
C FDA_mechanic_vlv.fgi( 566, 781):���������� ���������� �������,CIL_PKS3_N5
      I_im=I_(74)
C FDA_10boats.fgi( 200, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ifubi,INT(I_im,4),I_efubi
     &)
C FDA_mechanic_vlv.fgi( 546, 781):���������� ���������� �������,CIL_PKS3_N4
      I_em=I_(75)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_akubi,INT(I_em,4),I_ufubi
     &)
C FDA_mechanic_vlv.fgi( 526, 781):���������� ���������� �������,CIL_PKS3_N3
      I_am=I_(76)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_okubi,INT(I_am,4),I_ikubi
     &)
C FDA_mechanic_vlv.fgi( 506, 781):���������� ���������� �������,CIL_PKS3_N2
      I_ul=I_(77)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_elubi,INT(I_ul,4),I_alubi
     &)
C FDA_mechanic_vlv.fgi( 486, 781):���������� ���������� �������,CIL_PKS3_N1
      I_ol=I_(78)
C FDA_10boats.fgi( 200, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ulubi,INT(I_ol,4),I_olubi
     &)
C FDA_mechanic_vlv.fgi( 866, 751):���������� ���������� �������,CIL_PKS2_N5
      I_il=I_(79)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_imubi,INT(I_il,4),I_emubi
     &)
C FDA_mechanic_vlv.fgi( 846, 751):���������� ���������� �������,CIL_PKS2_N4
      I_el=I_(80)
C FDA_10boats.fgi( 180, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_apubi,INT(I_el,4),I_umubi
     &)
C FDA_mechanic_vlv.fgi( 826, 751):���������� ���������� �������,CIL_PKS2_N3
      I_al=I_(81)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_opubi,INT(I_al,4),I_ipubi
     &)
C FDA_mechanic_vlv.fgi( 806, 751):���������� ���������� �������,CIL_PKS2_N2
      I_uk=I_(82)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_erubi,INT(I_uk,4),I_arubi
     &)
C FDA_mechanic_vlv.fgi( 786, 751):���������� ���������� �������,CIL_PKS2_N1
      I_ok=I_(83)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_urubi,INT(I_ok,4),I_orubi
     &)
C FDA_mechanic_vlv.fgi( 766, 751):���������� ���������� �������,CIL_PKS1_N5
      I_ik=I_(84)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_isubi,INT(I_ik,4),I_esubi
     &)
C FDA_mechanic_vlv.fgi( 746, 751):���������� ���������� �������,CIL_PKS1_N4
      I_ek=I_(85)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_atubi,INT(I_ek,4),I_usubi
     &)
C FDA_mechanic_vlv.fgi( 726, 751):���������� ���������� �������,CIL_PKS1_N3
      I_ak=I_(86)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_otubi,INT(I_ak,4),I_itubi
     &)
C FDA_mechanic_vlv.fgi( 706, 751):���������� ���������� �������,CIL_PKS1_N2
      I_uf=I_(87)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_evubi,INT(I_uf,4),I_avubi
     &)
C FDA_mechanic_vlv.fgi( 686, 751):���������� ���������� �������,CIL_PKS1_N1
      I_ofe=I_(28)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_uvebi,INT(I_ofe,4),I_ovebi
     &)
C FDA_mechanic_vlv.fgi( 666, 834):���������� ���������� �������,CIL_KO6_N5
      I_ife=I_(29)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ixebi,INT(I_ife,4),I_exebi
     &)
C FDA_mechanic_vlv.fgi( 646, 834):���������� ���������� �������,CIL_KO6_N4
      I_efe=I_(30)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_abibi,INT(I_efe,4),I_uxebi
     &)
C FDA_mechanic_vlv.fgi( 626, 834):���������� ���������� �������,CIL_KO6_N3
      I_afe=I_(31)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_obibi,INT(I_afe,4),I_ibibi
     &)
C FDA_mechanic_vlv.fgi( 606, 834):���������� ���������� �������,CIL_KO6_N2
      I_ude=I_(32)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_edibi,INT(I_ude,4),I_adibi
     &)
C FDA_mechanic_vlv.fgi( 586, 834):���������� ���������� �������,CIL_KO6_N1
      I_ode=I_(33)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_udibi,INT(I_ode,4),I_odibi
     &)
C FDA_mechanic_vlv.fgi( 566, 834):���������� ���������� �������,CIL_KO5_N5
      I_ide=I_(34)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ifibi,INT(I_ide,4),I_efibi
     &)
C FDA_mechanic_vlv.fgi( 546, 834):���������� ���������� �������,CIL_KO5_N4
      I_ede=I_(35)
C FDA_10boats.fgi( 160, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_akibi,INT(I_ede,4),I_ufibi
     &)
C FDA_mechanic_vlv.fgi( 526, 834):���������� ���������� �������,CIL_KO5_N3
      I_ade=I_(36)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_okibi,INT(I_ade,4),I_ikibi
     &)
C FDA_mechanic_vlv.fgi( 506, 834):���������� ���������� �������,CIL_KO5_N2
      I_ube=I_(37)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_elibi,INT(I_ube,4),I_alibi
     &)
C FDA_mechanic_vlv.fgi( 486, 834):���������� ���������� �������,CIL_KO5_N1
      I_obe=I_(38)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ulibi,INT(I_obe,4),I_olibi
     &)
C FDA_mechanic_vlv.fgi( 866, 808):���������� ���������� �������,CIL_KO4_N5
      I_ibe=I_(39)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_imibi,INT(I_ibe,4),I_emibi
     &)
C FDA_mechanic_vlv.fgi( 846, 808):���������� ���������� �������,CIL_KO4_N4
      I_ebe=I_(40)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_apibi,INT(I_ebe,4),I_umibi
     &)
C FDA_mechanic_vlv.fgi( 826, 808):���������� ���������� �������,CIL_KO4_N3
      I_abe=I_(41)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_opibi,INT(I_abe,4),I_ipibi
     &)
C FDA_mechanic_vlv.fgi( 806, 808):���������� ���������� �������,CIL_KO4_N2
      I_ux=I_(42)
C FDA_10boats.fgi( 160, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_eribi,INT(I_ux,4),I_aribi
     &)
C FDA_mechanic_vlv.fgi( 786, 808):���������� ���������� �������,CIL_KO4_N1
      I_ox=I_(43)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_uribi,INT(I_ox,4),I_oribi
     &)
C FDA_mechanic_vlv.fgi( 766, 808):���������� ���������� �������,CIL_KO3_N5
      I_ix=I_(44)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_isibi,INT(I_ix,4),I_esibi
     &)
C FDA_mechanic_vlv.fgi( 746, 808):���������� ���������� �������,CIL_KO3_N4
      I_ex=I_(45)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_atibi,INT(I_ex,4),I_usibi
     &)
C FDA_mechanic_vlv.fgi( 726, 808):���������� ���������� �������,CIL_KO3_N3
      I_ax=I_(46)
C FDA_10boats.fgi( 160, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_otibi,INT(I_ax,4),I_itibi
     &)
C FDA_mechanic_vlv.fgi( 706, 808):���������� ���������� �������,CIL_KO3_N2
      I_uv=I_(47)
C FDA_10boats.fgi( 180, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_evibi,INT(I_uv,4),I_avibi
     &)
C FDA_mechanic_vlv.fgi( 686, 808):���������� ���������� �������,CIL_KO3_N1
      I_ov=I_(48)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_uvibi,INT(I_ov,4),I_ovibi
     &)
C FDA_mechanic_vlv.fgi( 666, 808):���������� ���������� �������,CIL_KO2_N5
      I_iv=I_(49)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ixibi,INT(I_iv,4),I_exibi
     &)
C FDA_mechanic_vlv.fgi( 646, 808):���������� ���������� �������,CIL_KO2_N4
      I_ev=I_(50)
C FDA_10boats.fgi( 180, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_abobi,INT(I_ev,4),I_uxibi
     &)
C FDA_mechanic_vlv.fgi( 626, 808):���������� ���������� �������,CIL_KO2_N3
      I_av=I_(51)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_obobi,INT(I_av,4),I_ibobi
     &)
C FDA_mechanic_vlv.fgi( 606, 808):���������� ���������� �������,CIL_KO2_N2
      I_ut=I_(52)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_edobi,INT(I_ut,4),I_adobi
     &)
C FDA_mechanic_vlv.fgi( 586, 808):���������� ���������� �������,CIL_KO2_N1
      I_ot=I_(53)
C FDA_10boats.fgi( 160, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_udobi,INT(I_ot,4),I_odobi
     &)
C FDA_mechanic_vlv.fgi( 566, 808):���������� ���������� �������,CIL_KO1_N5
      I_it=I_(54)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ifobi,INT(I_it,4),I_efobi
     &)
C FDA_mechanic_vlv.fgi( 546, 808):���������� ���������� �������,CIL_KO1_N4
      I_et=I_(55)
C FDA_10boats.fgi( 180, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_akobi,INT(I_et,4),I_ufobi
     &)
C FDA_mechanic_vlv.fgi( 526, 808):���������� ���������� �������,CIL_KO1_N3
      I_at=I_(56)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_okobi,INT(I_at,4),I_ikobi
     &)
C FDA_mechanic_vlv.fgi( 506, 808):���������� ���������� �������,CIL_KO1_N2
      I_us=I_(57)
C FDA_10boats.fgi( 200, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_elobi,INT(I_us,4),I_alobi
     &)
C FDA_mechanic_vlv.fgi( 486, 808):���������� ���������� �������,CIL_KO1_N1
      I_ape=I_(11)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_osaxe,INT(I_ape,4),I_isaxe
     &)
C FDA_mechanic_vlv.fgi( 606, 717):���������� ���������� �������,CIL_20FDA60AE515_N
      I_ume=I_(12)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_etaxe,INT(I_ume,4),I_ataxe
     &)
C FDA_mechanic_vlv.fgi( 586, 717):���������� ���������� �������,CIL_20FDA60AE509_N
      I_ome=I_(13)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_utaxe,INT(I_ome,4),I_otaxe
     &)
C FDA_mechanic_vlv.fgi( 566, 717):���������� ���������� �������,CIL_20FDA60AE502_N7
      I_ime=I_(14)
C FDA_10boats.fgi( 180, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ivaxe,INT(I_ime,4),I_evaxe
     &)
C FDA_mechanic_vlv.fgi( 546, 717):���������� ���������� �������,CIL_20FDA60AE502_N6
      I_eme=I_(15)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_axaxe,INT(I_eme,4),I_uvaxe
     &)
C FDA_mechanic_vlv.fgi( 526, 717):���������� ���������� �������,CIL_20FDA60AE502_N5
      I_ame=I_(16)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_oxaxe,INT(I_ame,4),I_ixaxe
     &)
C FDA_mechanic_vlv.fgi( 506, 717):���������� ���������� �������,CIL_20FDA60AE502_N4
      I_ule=I_(17)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ebexe,INT(I_ule,4),I_abexe
     &)
C FDA_mechanic_vlv.fgi( 486, 717):���������� ���������� �������,CIL_20FDA60AE502_N3
      I_ole=I_(18)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ubexe,INT(I_ole,4),I_obexe
     &)
C FDA_mechanic_vlv.fgi( 666, 677):���������� ���������� �������,CIL_20FDA60AE502_N2
      I_ile=I_(19)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_idexe,INT(I_ile,4),I_edexe
     &)
C FDA_mechanic_vlv.fgi( 646, 677):���������� ���������� �������,CIL_20FDA60AE502_N1
      I_ale=I_(21)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ofexe,INT(I_ale,4),I_ifexe
     &)
C FDA_mechanic_vlv.fgi( 606, 677):���������� ���������� �������,CIL_20FDA60AE500_N2
      I_uke=I_(22)
C FDA_10boats.fgi( 140, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ekexe,INT(I_uke,4),I_akexe
     &)
C FDA_mechanic_vlv.fgi( 586, 677):���������� ���������� �������,CIL_20FDA60AE500_N1
      I_of=I_(88)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_uvubi,INT(I_of,4),I_ovubi
     &)
C FDA_mechanic_vlv.fgi( 666, 751):���������� ���������� �������,CIL_20FDA60AE413_N5
      I_if=I_(89)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ixubi,INT(I_if,4),I_exubi
     &)
C FDA_mechanic_vlv.fgi( 646, 751):���������� ���������� �������,CIL_20FDA60AE413_N4
      I_ef=I_(90)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_abadi,INT(I_ef,4),I_uxubi
     &)
C FDA_mechanic_vlv.fgi( 626, 751):���������� ���������� �������,CIL_20FDA60AE413_N3
      I_af=I_(91)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_obadi,INT(I_af,4),I_ibadi
     &)
C FDA_mechanic_vlv.fgi( 606, 751):���������� ���������� �������,CIL_20FDA60AE413_N2
      I_ud=I_(92)
C FDA_10boats.fgi( 200, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_edadi,INT(I_ud,4),I_adadi
     &)
C FDA_mechanic_vlv.fgi( 586, 751):���������� ���������� �������,CIL_20FDA60AE413_N1
      I_od=I_(93)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_udadi,INT(I_od,4),I_odadi
     &)
C FDA_mechanic_vlv.fgi( 566, 751):���������� ���������� �������,CIL_20FDA60AE408_N5
      I_id=I_(94)
C FDA_10boats.fgi(  40, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ifadi,INT(I_id,4),I_efadi
     &)
C FDA_mechanic_vlv.fgi( 546, 751):���������� ���������� �������,CIL_20FDA60AE408_N4
      I_ed=I_(95)
C FDA_10boats.fgi( 120, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_akadi,INT(I_ed,4),I_ufadi
     &)
C FDA_mechanic_vlv.fgi( 526, 751):���������� ���������� �������,CIL_20FDA60AE408_N3
      I_ad=I_(96)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_okadi,INT(I_ad,4),I_ikadi
     &)
C FDA_mechanic_vlv.fgi( 506, 751):���������� ���������� �������,CIL_20FDA60AE408_N2
      I_u=I_(97)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_eladi,INT(I_u,4),I_aladi
     &)
C FDA_mechanic_vlv.fgi( 486, 751):���������� ���������� �������,CIL_20FDA60AE408_N1
      I_ele=I_(20)
C FDA_10boats.fgi( 100, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_afexe,INT(I_ele,4),I_udexe
     &)
C FDA_mechanic_vlv.fgi( 626, 677):���������� ���������� �������,CIL_20FDA60AE403_N
      I_oke=I_(23)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ukexe,INT(I_oke,4),I_okexe
     &)
C FDA_mechanic_vlv.fgi( 566, 677):���������� ���������� �������,CIL_20FDA60AE402_N
      I_eke=I_(25)
C FDA_10boats.fgi(  80, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_amexe,INT(I_eke,4),I_ulexe
     &)
C FDA_mechanic_vlv.fgi( 526, 677):���������� ���������� �������,CIL_20FDA60AE401_N
      I_ufe=I_(27)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_epexe,INT(I_ufe,4),I_apexe
     &)
C FDA_mechanic_vlv.fgi( 486, 677):���������� ���������� �������,CIL_20FDA60AE400_N
      I_ike=I_(24)
C FDA_10boats.fgi(  60, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_ilexe,INT(I_ike,4),I_elexe
     &)
C FDA_mechanic_vlv.fgi( 546, 677):���������� ���������� �������,CIL_20FDA60AE201_N
      I_ake=I_(26)
C FDA_10boats.fgi( 220, 230):������-�������: ���������� ��� �������������� ������
      Call IND_LOD_MNEMO_2(deltat,I_omexe,INT(I_ake,4),I_imexe
     &)
C FDA_mechanic_vlv.fgi( 506, 677):���������� ���������� �������,CIL_20FDA60AE200_N
      I_erube = 1
C FDA_video.fgi( 149, 271):��������� ������������� IN (�������)
      I_irube = 1
C FDA_video.fgi( 149, 276):��������� ������������� IN (�������)
      I_orube = 1
C FDA_video.fgi( 149, 266):��������� ������������� IN (�������)
      I_urube = 1
C FDA_video.fgi( 149, 281):��������� ������������� IN (�������)
      I_asube = 1
C FDA_video.fgi( 149, 286):��������� ������������� IN (�������)
      I_esube=I_isube
C FDA_video.fgi(  70, 290):������,CAMERA1_IN
      I_osube=I_usube
C FDA_video.fgi(  70, 269):������,CAMERA5_IN
      I_atube=I_etube
C FDA_video.fgi(  70, 274):������,CAMERA4_IN
      I_itube=I_otube
C FDA_video.fgi(  70, 279):������,CAMERA3_IN
      I_utube=I_avube
C FDA_video.fgi(  70, 285):������,CAMERA2_IN
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_uxave,4),R8_usave
     &,R_epave,R_apave,C30_ovave,
     & R_asave,REAL(R_isave,4),R_irave,
     & REAL(R_urave,4),R_ipave,REAL(R_upave,4),R_axave,
     & R_uvave,L_osave,L_ideve,L_ifeve,L_esave,
     & REAL(R_otave,4),L_etave,L_atave,L_udeve,L_orave,
     & L_opave,L_avave,L_akeve,L_odeve,L_ixave,L_oxave,
     & REAL(R8_axorad,8),REAL(1.0,4),R8_udepad,L_emave,L_omave
     &,L_imave,L_umave,
     & L_ofeve,R_edeve,REAL(R_itave,4),L_ufeve,L_ulave,L_ekeve
     &,L_amave,L_abeve,
     & L_ebeve,L_ibeve,L_ubeve,L_adeve,L_obeve)
      !}
C FDA_mechanic_vlv.fgi( 910, 973):���������� ������ ������������ �������,20FDA32AB001-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_iveve,4),R8_ireve
     &,R_uleve,R_oleve,C30_eteve,
     & R_opeve,REAL(R_areve,4),R_apeve,
     & REAL(R_ipeve,4),R_ameve,REAL(R_imeve,4),R_oteve,
     & R_iteve,L_ereve,L_abive,L_adive,L_upeve,
     & REAL(R_eseve,4),L_ureve,L_oreve,L_ibive,L_epeve,
     & L_emeve,L_oseve,L_odive,L_ebive,L_aveve,L_eveve,
     & REAL(R8_axorad,8),REAL(1.0,4),R8_udepad,L_ukeve,L_eleve
     &,L_aleve,L_ileve,
     & L_edive,R_uxeve,REAL(R_aseve,4),L_idive,L_ikeve,L_udive
     &,L_okeve,L_oveve,
     & L_uveve,L_axeve,L_ixeve,L_oxeve,L_exeve)
      !}
C FDA_mechanic_vlv.fgi( 875, 973):���������� ������ ������������ �������,20FDA91AB004-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_ative,4),R8_apive
     &,R_ikive,R_ekive,C30_urive,
     & R_emive,REAL(R_omive,4),R_olive,
     & REAL(R_amive,4),R_okive,REAL(R_alive,4),R_esive,
     & R_asive,L_umive,L_ovive,L_oxive,L_imive,
     & REAL(R_upive,4),L_ipive,L_epive,L_axive,L_ulive,
     & L_ukive,L_erive,L_ebove,L_uvive,L_osive,L_usive,
     & REAL(R8_axorad,8),REAL(1.0,4),R8_udepad,L_ifive,L_ufive
     &,L_ofive,L_akive,
     & L_uxive,R_ivive,REAL(R_opive,4),L_above,L_afive,L_ibove
     &,L_efive,L_etive,
     & L_itive,L_otive,L_avive,L_evive,L_utive)
      !}
C FDA_mechanic_vlv.fgi( 858, 973):���������� ������ ������������ �������,20FDA91AB003-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_orove,4),R8_olove
     &,R_afove,R_udove,C30_ipove,
     & R_ukove,REAL(R_elove,4),R_ekove,
     & REAL(R_okove,4),R_efove,REAL(R_ofove,4),R_upove,
     & R_opove,L_ilove,L_etove,L_evove,L_alove,
     & REAL(R_imove,4),L_amove,L_ulove,L_otove,L_ikove,
     & L_ifove,L_umove,L_uvove,L_itove,L_erove,L_irove,
     & REAL(R8_axorad,8),REAL(1.0,4),R8_udepad,L_adove,L_idove
     &,L_edove,L_odove,
     & L_ivove,R_atove,REAL(R_emove,4),L_ovove,L_obove,L_axove
     &,L_ubove,L_urove,
     & L_asove,L_esove,L_osove,L_usove,L_isove)
      !}
C FDA_mechanic_vlv.fgi( 842, 973):���������� ������ ������������ �������,20FDA91AB002-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_epuve,4),R8_ekuve
     &,R_obuve,R_ibuve,C30_amuve,
     & R_ifuve,REAL(R_ufuve,4),R_uduve,
     & REAL(R_efuve,4),R_ubuve,REAL(R_eduve,4),R_imuve,
     & R_emuve,L_akuve,L_uruve,L_usuve,L_ofuve,
     & REAL(R_aluve,4),L_okuve,L_ikuve,L_esuve,L_afuve,
     & L_aduve,L_iluve,L_ituve,L_asuve,L_umuve,L_apuve,
     & REAL(R8_axorad,8),REAL(1.0,4),R8_udepad,L_oxove,L_abuve
     &,L_uxove,L_ebuve,
     & L_atuve,R_oruve,REAL(R_ukuve,4),L_etuve,L_exove,L_otuve
     &,L_ixove,L_ipuve,
     & L_opuve,L_upuve,L_eruve,L_iruve,L_aruve)
      !}
C FDA_mechanic_vlv.fgi( 826, 973):���������� ������ ������������ �������,20FDA91AB001-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_ulaxe,4),R8_udaxe
     &,R_exuve,R_axuve,C30_okaxe,
     & R_adaxe,REAL(R_idaxe,4),R_ibaxe,
     & REAL(R_ubaxe,4),R_ixuve,REAL(R_uxuve,4),R_alaxe,
     & R_ukaxe,L_odaxe,L_ipaxe,L_iraxe,L_edaxe,
     & REAL(R_ofaxe,4),L_efaxe,L_afaxe,L_upaxe,L_obaxe,
     & L_oxuve,L_akaxe,L_asaxe,L_opaxe,L_ilaxe,L_olaxe,
     & REAL(R8_axorad,8),REAL(1.0,4),R8_udepad,L_evuve,L_ovuve
     &,L_ivuve,L_uvuve,
     & L_oraxe,R_epaxe,REAL(R_ifaxe,4),L_uraxe,L_utuve,L_esaxe
     &,L_avuve,L_amaxe,
     & L_emaxe,L_imaxe,L_umaxe,L_apaxe,L_omaxe)
      !}
C FDA_mechanic_vlv.fgi( 892, 973):���������� ������ ������������ �������,20FDA91AB005-M01
      call c_clock(I_ivebi,I_evebi,I_avebi)
C FDA_mechanic_vlv.fgi( 554, 946):��������� �����
      !{
      Call BASE_STORE_HANDLER(deltat,REAL(R_utiri,4),R8_ariri
     &,I_umori,
     & I_apori,L_ilori,INT(I_etiri,4),I_epori,I_ipori,I_ebori
     &,
     & I_obori,I_ubori,I_adori,I_edori,I_idori,I_odori,I_udori
     &,
     & I_okori,I_ofori,R_epiri,REAL(R_opiri,4),
     & R_iliri,REAL(R_uliri,4),I_oxiri,I_efori,C20_asiri,
     & L_olori,C20_esiri,C8_isiri,R_amiri,
     & REAL(R_imiri,4),L_umiri,R_omiri,
     & REAL(R_apiri,4),I_afori,R_atiri,R_usiri,I_ifori,I_uxiri
     &,I_ixiri,
     & L_upiri,L_akori,L_ulori,L_ekori,L_ipiri,
     & L_oliri,REAL(R_uriri,4),L_iriri,L_eriri,L_ukori,
     & L_emiri,L_osiri,L_imori,L_itiri,L_otiri,REAL(R8_axorad
     &,8),
     & REAL(1.0,4),R8_udepad,L_ukiri,L_okiri,L_ikori,I_ufori
     &,L_amori,R_exiri,
     & REAL(R_oriri,4),L_emori,L_eliri,L_omori,L_aliri,L_aviri
     &,L_eviri,L_iviri,L_uviri,
     & L_axiri,L_oviri)
      !}
C FDA_mechanic_vlv.fgi( 180, 941):���������� �������� �������,20FDA60AE600
      !{
      Call LIFT_HANDLER(deltat,REAL(R_afuri,4),R8_ivori,I_iburi
     &,I_iluri,I_omuri,
     & L_iduri,I_oluri,R_otori,REAL(R_avori,4),
     & R_urori,REAL(R_esori,4),I_oburi,I_eburi,I_ukuri,I_emuri
     &,
     & C20_ixori,C20_oxori,C8_uxori,R_isori,
     & REAL(R_usori,4),L_etori,R_atori,
     & REAL(R_itori,4),I_amuri,R_aduri,R_uburi,I_imuri,I_aluri
     &,I_okuri,
     & L_evori,L_apuri,L_aruri,L_utori,L_asori,
     & L_ovori,L_uvori,REAL(R_exori,4),L_ipuri,L_osori,L_aburi
     &,
     & L_oruri,L_oduri,L_uduri,REAL(R8_axorad,8),REAL(1.0
     &,4),R8_udepad,L_erori,
     & L_arori,L_epuri,I_umuri,L_eruri,R_ikuri,REAL(R_axori
     &,4),L_iruri,L_orori,L_ururi,
     & L_irori,L_efuri,L_ifuri,L_ofuri,L_akuri,L_ekuri,L_ufuri
     &)
      !}
C FDA_mechanic_vlv.fgi( 113, 915):���������� �����,20FDA60AE200
      L_(181)=R8_exevi.gt.R0_axevi
C FDA_lamp.fgi( 826,   5):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omovi,L_epovi,R_apovi
     &,
     & REAL(R_ipovi,4),L_(181),L_imovi,I_umovi)
      !}
C FDA_lamp.fgi( 848,   4):���������� ������� ���������,20FDA14CL012
      L_(182)=R8_oxevi.lt.R0_ixevi
C FDA_lamp.fgi( 826,  16):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_erovi,L_urovi,R_orovi
     &,
     & REAL(R_asovi,4),L_(182),L_arovi,I_irovi)
      !}
C FDA_lamp.fgi( 848,  14):���������� ������� ���������,20FDA14CL011
      L_(170)=R8_abivi.gt.R0_uxevi
C FDA_lamp.fgi( 826,  27):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_erivi,L_urivi,R_orivi
     &,
     & REAL(R_asivi,4),L_(170),L_arivi,I_irivi)
      !}
C FDA_lamp.fgi( 848,  26):���������� ������� ���������,20FDA14CL010
      L_(171)=R8_ibivi.lt.R0_ebivi
C FDA_lamp.fgi( 826,  38):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usivi,L_itivi,R_etivi
     &,
     & REAL(R_otivi,4),L_(171),L_osivi,I_ativi)
      !}
C FDA_lamp.fgi( 848,  36):���������� ������� ���������,20FDA14CL009
      L_(168)=R8_ubivi.gt.R0_obivi
C FDA_lamp.fgi( 826,  49):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alivi,L_olivi,R_ilivi
     &,
     & REAL(R_ulivi,4),L_(168),L_ukivi,I_elivi)
      !}
C FDA_lamp.fgi( 848,  48):���������� ������� ���������,20FDA14CL008
      L_(172)=R8_odivi.lt.R0_adivi
C FDA_lamp.fgi( 826,  71):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivivi,L_axivi,R_uvivi
     &,
     & REAL(R_exivi,4),L_(172),L_evivi,I_ovivi)
      !}
C FDA_lamp.fgi( 848,  70):���������� ������� ���������,20FDA14CL006
      L_(167)=R8_idivi.gt.R0_edivi
C FDA_lamp.fgi( 807,  61):���������� >
      L_(169) = L_(167).AND.L_uvevi
C FDA_lamp.fgi( 829,  60):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omivi,L_epivi,R_apivi
     &,
     & REAL(R_ipivi,4),L_(169),L_imivi,I_umivi)
      !}
C FDA_lamp.fgi( 848,  58):���������� ������� ���������,20FDA14CL007
      L_(173)=R8_afivi.gt.R0_udivi
C FDA_lamp.fgi( 826,  82):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_abovi,L_obovi,R_ibovi
     &,
     & REAL(R_ubovi,4),L_(173),L_uxivi,I_ebovi)
      !}
C FDA_lamp.fgi( 848,  80):���������� ������� ���������,20FDA14CL005
      L_(174)=R8_ifivi.lt.R0_efivi
C FDA_lamp.fgi( 826,  93):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_odovi,L_efovi,R_afovi
     &,
     & REAL(R_ifovi,4),L_(174),L_idovi,I_udovi)
      !}
C FDA_lamp.fgi( 848,  92):���������� ������� ���������,20FDA14CL004
      L_(183)=R8_ufivi.gt.R0_ofivi
C FDA_lamp.fgi( 826, 104):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usovi,L_itovi,R_etovi
     &,
     & REAL(R_otovi,4),L_(183),L_osovi,I_atovi)
      !}
C FDA_lamp.fgi( 848, 102):���������� ������� ���������,20FDA14CL003
      L_(184)=R8_ekivi.gt.R0_akivi
C FDA_lamp.fgi( 826, 115):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivovi,L_axovi,R_uvovi
     &,
     & REAL(R_exovi,4),L_(184),L_evovi,I_ovovi)
      !}
C FDA_lamp.fgi( 848, 114):���������� ������� ���������,20FDA14CL002
      L_(185)=R8_okivi.lt.R0_ikivi
C FDA_lamp.fgi( 826, 126):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_abuvi,L_obuvi,R_ibuvi
     &,
     & REAL(R_ubuvi,4),L_(185),L_uxovi,I_ebuvi)
      !}
C FDA_lamp.fgi( 848, 124):���������� ������� ���������,20FDA14CL001
      L_(175)=R_ukovi.lt.R0_akovi
C FDA_lamp.fgi( 574, 621):���������� <
      L_(176)=R_ukovi.gt.R0_ekovi
C FDA_lamp.fgi( 574, 627):���������� >
      L_(216) = L_(176).AND.L_(175)
C FDA_lamp.fgi( 583, 626):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aluxi,L_oluxi,R_iluxi
     &,
     & REAL(R_uluxi,4),L_(216),L_ukuxi,I_eluxi)
      !}
C FDA_lamp.fgi( 604, 624):���������� ������� ���������,20FDA52AE401QH07
      L_(217)=R_ukovi.lt.R0_ikovi
C FDA_lamp.fgi( 574, 652):���������� <
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omuxi,L_epuxi,R_apuxi
     &,
     & REAL(R_ipuxi,4),L_(217),L_imuxi,I_umuxi)
      !}
C FDA_lamp.fgi( 604, 650):���������� ������� ���������,20FDA52AE401QH06
      L_(215)=R_ukovi.gt.R0_okovi
C FDA_lamp.fgi( 574, 639):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ifuxi,L_akuxi,R_ufuxi
     &,
     & REAL(R_ekuxi,4),L_(215),L_efuxi,I_ofuxi)
      !}
C FDA_lamp.fgi( 604, 638):���������� ������� ���������,20FDA52AE401QH08
      C30_alovi = '����� �������'
C FDA_lamp.fgi( 755, 680):��������� ���������� CH20 (CH30) (�������)
      C30_olovi = '����� �������'
C FDA_lamp.fgi( 773, 679):��������� ���������� CH20 (CH30) (�������)
      C30_elovi = ''
C FDA_lamp.fgi( 755, 682):��������� ���������� CH20 (CH30) (�������)
      L_(179)=I_amovi.ne.0
C FDA_lamp.fgi( 755, 656):��������� 1->LO
      L_(178)=I_emovi.ne.0
C FDA_lamp.fgi( 755, 668):��������� 1->LO
      L_otuvi=(L_(178).or.L_otuvi).and..not.(L_(179))
      L_(180)=.not.L_otuvi
C FDA_lamp.fgi( 784, 666):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osuvi,L_etuvi,R_atuvi
     &,
     & REAL(R_ituvi,4),L_otuvi,L_isuvi,I_usuvi)
      !}
C FDA_lamp.fgi( 804, 666):���������� ������� ���������,20FDA52QB001
      if(L_otuvi) then
         C30_ulovi=C30_alovi
      else
         C30_ulovi=C30_elovi
      endif
C FDA_lamp.fgi( 759, 680):���� RE IN LO CH20
      L_uruvi=(L_(179).or.L_uruvi).and..not.(L_(178))
      L_(177)=.not.L_uruvi
C FDA_lamp.fgi( 784, 654):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upuvi,L_iruvi,R_eruvi
     &,
     & REAL(R_oruvi,4),L_uruvi,L_opuvi,I_aruvi)
      !}
C FDA_lamp.fgi( 804, 654):���������� ������� ���������,20FDA52QB002
      if(L_uruvi) then
         C30_ilovi=C30_olovi
      else
         C30_ilovi=C30_ulovi
      endif
C FDA_lamp.fgi( 777, 679):���� RE IN LO CH20
      L_(186)=.true.
C FDA_lamp.fgi( 963, 194):��������� ���������� (�������)
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oduvi,L_efuvi,R_afuvi
     &,
     & REAL(R_ifuvi,4),L_(186),L_iduvi,I_uduvi)
      !}
C FDA_lamp.fgi( 980, 192):���������� ������� ���������,20FDA71AJ002_LREADY
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekuvi,L_ukuvi,R_okuvi
     &,
     & REAL(R_aluvi,4),L_eluvi,L_akuvi,I_ikuvi)
      !}
C FDA_lamp.fgi( 938, 242):���������� ������� ���������,20FDA52AE401QH10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amuvi,L_omuvi,R_imuvi
     &,
     & REAL(R_umuvi,4),L_apuvi,L_uluvi,I_emuvi)
      !}
C FDA_lamp.fgi( 938, 254):���������� ������� ���������,20FDA52AE401QH09
      !{
      Call DAT_DISCR_EMERG_HANDLER(deltat,L_oxuvi,R_ixuvi
     &,
     & REAL(R_uxuvi,4),R_uvuvi,REAL(R_exuvi,4),L_adaxi,
     & L_axuvi,L_edaxi,L_abaxi,I_evuvi)
      !}
C FDA_lamp.fgi(1032, 247):���������� ������� ����. �������� ������������,20FDA52AE501QB04
      !{
      Call DAT_DISCR_EMERG_HANDLER(deltat,L_ikaxi,R_ekaxi
     &,
     & REAL(R_okaxi,4),R_ofaxi,REAL(R_akaxi,4),L_ulaxi,
     & L_ufaxi,L_amaxi,L_ukaxi,I_afaxi)
      !}
C FDA_lamp.fgi(1032, 263):���������� ������� ����. �������� ������������,20FDA52AE501QB03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_apaxi,L_opaxi,R_ipaxi
     &,
     & REAL(R_upaxi,4),L_araxi,L_umaxi,I_epaxi)
      !}
C FDA_lamp.fgi( 938, 314):���������� ������� ���������,20FDA52AE501QB05
      if(.not.L_ovaxi) then
         R0_itaxi=0.0
      elseif(.not.L0_otaxi) then
         R0_itaxi=R0_etaxi
      else
         R0_itaxi=max(R_(1)-deltat,0.0)
      endif
      L_(187)=L_ovaxi.and.R0_itaxi.le.0.0
      L0_otaxi=L_ovaxi
C FDA_lamp.fgi(  69, 635):�������� ��������� ������
      L_(190) = L_(187).AND.(.NOT.L_ivaxi)
C FDA_lamp.fgi(  76, 634):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axaxi,L_oxaxi,R_ixaxi
     &,
     & REAL(R_uxaxi,4),L_(190),L_uvaxi,I_exaxi)
      !}
C FDA_lamp.fgi(  88, 632):���������� ������� ���������,20FDA71AE701XU01
      if(.not.L_ivaxi) then
         R0_avaxi=0.0
      elseif(.not.L0_evaxi) then
         R0_avaxi=R0_utaxi
      else
         R0_avaxi=max(R_(2)-deltat,0.0)
      endif
      L_(189)=L_ivaxi.and.R0_avaxi.le.0.0
      L0_evaxi=L_ivaxi
C FDA_lamp.fgi(  69, 624):�������� ��������� ������
      L_(188) = L_(189).AND.(.NOT.L_ovaxi)
C FDA_lamp.fgi(  76, 623):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uraxi,L_isaxi,R_esaxi
     &,
     & REAL(R_osaxi,4),L_(188),L_oraxi,I_asaxi)
      !}
C FDA_lamp.fgi(  88, 622):���������� ������� ���������,20FDA71AE701XU02
      if(.not.L_erexi) then
         R0_obexi=0.0
      elseif(.not.L0_ubexi) then
         R0_obexi=R0_ibexi
      else
         R0_obexi=max(R_(3)-deltat,0.0)
      endif
      L_(191)=L_erexi.and.R0_obexi.le.0.0
      L0_ubexi=L_erexi
C FDA_lamp.fgi(  69, 673):�������� ��������� ������
      L_(196) = L_(191).AND.(.NOT.L_arexi).AND.(.NOT.L_erexi
     &)
C FDA_lamp.fgi(  76, 671):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omexi,L_epexi,R_apexi
     &,
     & REAL(R_ipexi,4),L_(196),L_imexi,I_umexi)
      !}
C FDA_lamp.fgi(  88, 670):���������� ������� ���������,20FDA71AE202QH06
      if(.not.L_arexi) then
         R0_edexi=0.0
      elseif(.not.L0_idexi) then
         R0_edexi=R0_adexi
      else
         R0_edexi=max(R_(4)-deltat,0.0)
      endif
      L_(193)=L_arexi.and.R0_edexi.le.0.0
      L0_idexi=L_arexi
C FDA_lamp.fgi(  69, 662):�������� ��������� ������
      L_(195) = L_(193).AND.(.NOT.L_erexi).AND.(.NOT.L_erexi
     &)
C FDA_lamp.fgi(  76, 660):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alexi,L_olexi,R_ilexi
     &,
     & REAL(R_ulexi,4),L_(195),L_ukexi,I_elexi)
      !}
C FDA_lamp.fgi(  88, 658):���������� ������� ���������,20FDA71AE202QH05
      if(.not.L_erexi) then
         R0_udexi=0.0
      elseif(.not.L0_afexi) then
         R0_udexi=R0_odexi
      else
         R0_udexi=max(R_(5)-deltat,0.0)
      endif
      L_(192)=L_erexi.and.R0_udexi.le.0.0
      L0_afexi=L_erexi
C FDA_lamp.fgi(  69, 651):�������� ��������� ������
      L_(194) = L_(192).AND.(.NOT.L_arexi).AND.(.NOT.L_erexi
     &)
C FDA_lamp.fgi(  76, 649):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ifexi,L_akexi,R_ufexi
     &,
     & REAL(R_ekexi,4),L_(194),L_efexi,I_ofexi)
      !}
C FDA_lamp.fgi(  88, 648):���������� ������� ���������,20FDA71AE801XU02
      L_atexi=(L_irexi.or.L_atexi).and..not.(L_orexi)
      L_(197)=.not.L_atexi
C FDA_lamp.fgi(  72, 683):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asexi,L_osexi,R_isexi
     &,
     & REAL(R_usexi,4),L_atexi,L_urexi,I_esexi)
      !}
C FDA_lamp.fgi(  88, 684):���������� ������� ���������,20FDA72AL101_LON
      if(.not.L_oloxi) then
         R0_utexi=0.0
      elseif(.not.L0_avexi) then
         R0_utexi=R0_otexi
      else
         R0_utexi=max(R_(6)-deltat,0.0)
      endif
      L_(198)=L_oloxi.and.R0_utexi.le.0.0
      L0_avexi=L_oloxi
C FDA_lamp.fgi(  67, 789):�������� ��������� ������
      L_(212) = L_(198).AND.(.NOT.L_iloxi).AND.(.NOT.L_eloxi
     &)
C FDA_lamp.fgi(  74, 787):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ufoxi,L_ikoxi,R_ekoxi
     &,
     & REAL(R_okoxi,4),L_(212),L_ofoxi,I_akoxi)
      !}
C FDA_lamp.fgi(  86, 786):���������� ������� ���������,20FDA73AE403XU16
      if(.not.L_iloxi) then
         R0_ivexi=0.0
      elseif(.not.L0_ovexi) then
         R0_ivexi=R0_evexi
      else
         R0_ivexi=max(R_(7)-deltat,0.0)
      endif
      L_(206)=L_iloxi.and.R0_ivexi.le.0.0
      L0_ovexi=L_iloxi
C FDA_lamp.fgi(  67, 778):�������� ��������� ������
      L_(211) = L_(206).AND.(.NOT.L_oloxi).AND.(.NOT.L_eloxi
     &)
C FDA_lamp.fgi(  74, 776):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_edoxi,L_udoxi,R_odoxi
     &,
     & REAL(R_afoxi,4),L_(211),L_adoxi,I_idoxi)
      !}
C FDA_lamp.fgi(  86, 774):���������� ������� ���������,20FDA73AE403XU01
      if(.not.L_eloxi) then
         R0_axexi=0.0
      elseif(.not.L0_exexi) then
         R0_axexi=R0_uvexi
      else
         R0_axexi=max(R_(8)-deltat,0.0)
      endif
      L_(205)=L_eloxi.and.R0_axexi.le.0.0
      L0_exexi=L_eloxi
C FDA_lamp.fgi(  67, 767):�������� ��������� ������
      L_(210) = L_(205).AND.(.NOT.L_iloxi).AND.(.NOT.L_oloxi
     &)
C FDA_lamp.fgi(  74, 765):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxixi,L_eboxi,R_aboxi
     &,
     & REAL(R_iboxi,4),L_(210),L_ixixi,I_uxixi)
      !}
C FDA_lamp.fgi(  86, 764):���������� ������� ���������,20FDA73AE403XU03
      if(.not.L_otixi) then
         R0_oxexi=0.0
      elseif(.not.L0_uxexi) then
         R0_oxexi=R0_ixexi
      else
         R0_oxexi=max(R_(9)-deltat,0.0)
      endif
      L_(209)=L_otixi.and.R0_oxexi.le.0.0
      L0_uxexi=L_otixi
C FDA_lamp.fgi(  73, 750):�������� ��������� ������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_avixi,L_ovixi,R_ivixi
     &,
     & REAL(R_uvixi,4),L_(209),L_utixi,I_evixi)
      !}
C FDA_lamp.fgi(  86, 748):���������� ������� ���������,20FDA73AE002XU02
      if(.not.L_omixi) then
         R0_ebixi=0.0
      elseif(.not.L0_ibixi) then
         R0_ebixi=R0_abixi
      else
         R0_ebixi=max(R_(10)-deltat,0.0)
      endif
      L_(199)=L_omixi.and.R0_ebixi.le.0.0
      L0_ibixi=L_omixi
C FDA_lamp.fgi(  68, 695):�������� ��������� ������
      L_(201) = L_(199).AND.(.NOT.L_umixi).AND.(.NOT.L_apixi
     &).AND.(.NOT.L_urixi)
C FDA_lamp.fgi(  77, 698):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofixi,L_ekixi,R_akixi
     &,
     & REAL(R_ikixi,4),L_(201),L_ifixi,I_ufixi)
      !}
C FDA_lamp.fgi(  88, 696):���������� ������� ���������,20FDA71AE801QH12
      if(.not.L_umixi) then
         R0_ubixi=0.0
      elseif(.not.L0_adixi) then
         R0_ubixi=R0_obixi
      else
         R0_ubixi=max(R_(11)-deltat,0.0)
      endif
      L_(200)=L_umixi.and.R0_ubixi.le.0.0
      L0_adixi=L_umixi
C FDA_lamp.fgi(  68, 706):�������� ��������� ������
      L_(202) = L_(200).AND.(.NOT.L_omixi).AND.(.NOT.L_apixi
     &).AND.(.NOT.L_urixi)
C FDA_lamp.fgi(  77, 709):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_elixi,L_ulixi,R_olixi
     &,
     & REAL(R_amixi,4),L_(202),L_alixi,I_ilixi)
      !}
C FDA_lamp.fgi(  88, 708):���������� ������� ���������,20FDA71AE801QH11
      if(.not.L_apixi) then
         R0_idixi=0.0
      elseif(.not.L0_odixi) then
         R0_idixi=R0_edixi
      else
         R0_idixi=max(R_(12)-deltat,0.0)
      endif
      L_(203)=L_apixi.and.R0_idixi.le.0.0
      L0_odixi=L_apixi
C FDA_lamp.fgi(  68, 717):�������� ��������� ������
      L_(207) = L_(203).AND.(.NOT.L_omixi).AND.(.NOT.L_umixi
     &).AND.(.NOT.L_urixi)
C FDA_lamp.fgi(  77, 720):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ipixi,L_arixi,R_upixi
     &,
     & REAL(R_erixi,4),L_(207),L_epixi,I_opixi)
      !}
C FDA_lamp.fgi(  88, 718):���������� ������� ���������,20FDA71AE202XU04
      if(.not.L_urixi) then
         R0_afixi=0.0
      elseif(.not.L0_efixi) then
         R0_afixi=R0_udixi
      else
         R0_afixi=max(R_(13)-deltat,0.0)
      endif
      L_(204)=L_urixi.and.R0_afixi.le.0.0
      L0_efixi=L_urixi
C FDA_lamp.fgi(  68, 728):�������� ��������� ������
      L_(208) = L_(204).AND.(.NOT.L_omixi).AND.(.NOT.L_umixi
     &).AND.(.NOT.L_apixi)
C FDA_lamp.fgi(  77, 731):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_esixi,L_usixi,R_osixi
     &,
     & REAL(R_atixi,4),L_(208),L_asixi,I_isixi)
      !}
C FDA_lamp.fgi(  88, 730):���������� ������� ���������,20FDA71AE202XU01
      L_uroxi=(L_ipoxi.or.L_uroxi).and..not.(.NOT.L_ipoxi
     &)
      L_(213)=.not.L_uroxi
C FDA_lamp.fgi( 588, 879):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upoxi,L_iroxi,R_eroxi
     &,
     & REAL(R_oroxi,4),L_uroxi,L_opoxi,I_aroxi)
      !}
C FDA_lamp.fgi( 604, 880):���������� ������� ���������,20FDA71AJ002_LWORK
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amoxi,L_omoxi,R_imoxi
     &,
     & REAL(R_umoxi,4),L_(213),L_uloxi,I_emoxi)
      !}
C FDA_lamp.fgi( 604, 868):���������� ������� ���������,20FDA71AJ002_LSTOP
      L_avoxi=(L_osoxi.or.L_avoxi).and..not.(L_isoxi)
      L_(214)=.not.L_avoxi
C FDA_lamp.fgi( 588, 894):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atoxi,L_otoxi,R_itoxi
     &,
     & REAL(R_utoxi,4),L_avoxi,L_usoxi,I_etoxi)
      !}
C FDA_lamp.fgi( 604, 894):���������� ������� ���������,20FDA71AM001_LON
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uvoxi,L_ixoxi,R_exoxi
     &,
     & REAL(R_oxoxi,4),L_ebuxi,L_ovoxi,I_axoxi)
      !}
C FDA_lamp.fgi( 604, 914):���������� ������� ���������,20FDA71AJ002_LAVT
      !{
      Call DAT_DISCR_HANDLER(deltat,I_obuxi,L_eduxi,R_aduxi
     &,
     & REAL(R_iduxi,4),L_oduxi,L_ibuxi,I_ubuxi)
      !}
C FDA_lamp.fgi( 604, 926):���������� ������� ���������,20FDA71AJ002_LRUCH
      R_(14) = -500
C FDA_doz_logic.fgi(  29, 164):��������� (RE4) (�������)
      R_(15) = -500
C FDA_doz_logic.fgi( 175, 163):��������� (RE4) (�������)
      L_(218) = L_exuxi.AND.L_idebo
C FDA_doz_logic.fgi( 139, 564):�
      if(L_(218).and..not.L0_evuxi) then
         R0_utuxi=R0_avuxi
      else
         R0_utuxi=max(R_(18)-deltat,0.0)
      endif
      L_(219)=R0_utuxi.gt.0.0
      L0_evuxi=L_(218)
C FDA_doz_logic.fgi( 150, 564):������������  �� T
      !��������� R_(19) = FDA_doz_logicCytel /500/
      R_(19)=R0_ivuxi
C FDA_doz_logic.fgi( 165, 566):���������,ytel
      if(L_(219)) then
         R_(197)=R_(19)
      endif
C FDA_doz_logic.fgi( 169, 564):���� � ������������� �������
      L_(220) = L_exuxi.AND.L_idebo
C FDA_doz_logic.fgi(  46, 564):�
      if(L_(220).and..not.L0_axuxi) then
         R0_ovuxi=R0_uvuxi
      else
         R0_ovuxi=max(R_(20)-deltat,0.0)
      endif
      L_(221)=R0_ovuxi.gt.0.0
      L0_axuxi=L_(220)
C FDA_doz_logic.fgi(  57, 564):������������  �� T
      !��������� R_(21) = FDA_doz_logicCxtel /0/
      R_(21)=R0_ixuxi
C FDA_doz_logic.fgi(  72, 566):���������,xtel
      if(L_(221)) then
         R_(198)=R_(21)
      endif
C FDA_doz_logic.fgi(  76, 564):���� � ������������� �������
      L_(222) = L_esabo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 558):�
      if(L_(222).and..not.L0_ababo) then
         R0_oxuxi=R0_uxuxi
      else
         R0_oxuxi=max(R_(22)-deltat,0.0)
      endif
      L_(223)=R0_oxuxi.gt.0.0
      L0_ababo=L_(222)
C FDA_doz_logic.fgi( 150, 558):������������  �� T
      !��������� R_(23) = FDA_doz_logicCymvp /400/
      R_(23)=R0_ebabo
C FDA_doz_logic.fgi( 165, 560):���������,ymvp
      if(L_(223)) then
         R_(197)=R_(23)
      endif
C FDA_doz_logic.fgi( 169, 558):���� � ������������� �������
      L_(224) = L_etabo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 552):�
      if(L_(224).and..not.L0_ubabo) then
         R0_ibabo=R0_obabo
      else
         R0_ibabo=max(R_(24)-deltat,0.0)
      endif
      L_(225)=R0_ibabo.gt.0.0
      L0_ubabo=L_(224)
C FDA_doz_logic.fgi( 150, 552):������������  �� T
      !��������� R_(25) = FDA_doz_logicCybvp /700/
      R_(25)=R0_adabo
C FDA_doz_logic.fgi( 165, 554):���������,ybvp
      if(L_(225)) then
         R_(197)=R_(25)
      endif
C FDA_doz_logic.fgi( 169, 552):���� � ������������� �������
      !��������� R_(26) = FDA_doz_logicCycb0 /0/
      R_(26)=R0_edabo
C FDA_doz_logic.fgi( 165, 502):���������,ycb0
      if(L_exabo.and..not.L0_udabo) then
         R0_idabo=R0_odabo
      else
         R0_idabo=max(R_(27)-deltat,0.0)
      endif
      L_(226)=R0_idabo.gt.0.0
      L0_udabo=L_exabo
C FDA_doz_logic.fgi( 150, 500):������������  �� T
      if(L_(226)) then
         R_(197)=R_(26)
      endif
C FDA_doz_logic.fgi( 169, 500):���� � ������������� �������
      L_(227) = L_umebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 510):�
      if(L_(227).and..not.L0_ifabo) then
         R0_afabo=R0_efabo
      else
         R0_afabo=max(R_(28)-deltat,0.0)
      endif
      L_(234)=R0_afabo.gt.0.0
      L0_ifabo=L_(227)
C FDA_doz_logic.fgi( 150, 510):������������  �� T
      L_(228) = L_apebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 516):�
      if(L_(228).and..not.L0_akabo) then
         R0_ofabo=R0_ufabo
      else
         R0_ofabo=max(R_(29)-deltat,0.0)
      endif
      L_(235)=R0_ofabo.gt.0.0
      L0_akabo=L_(228)
C FDA_doz_logic.fgi( 150, 516):������������  �� T
      L_(229) = L_epebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 522):�
      if(L_(229).and..not.L0_okabo) then
         R0_ekabo=R0_ikabo
      else
         R0_ekabo=max(R_(30)-deltat,0.0)
      endif
      L_(236)=R0_ekabo.gt.0.0
      L0_okabo=L_(229)
C FDA_doz_logic.fgi( 150, 522):������������  �� T
      L_(230) = L_ipebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 528):�
      if(L_(230).and..not.L0_elabo) then
         R0_ukabo=R0_alabo
      else
         R0_ukabo=max(R_(31)-deltat,0.0)
      endif
      L_(237)=R0_ukabo.gt.0.0
      L0_elabo=L_(230)
C FDA_doz_logic.fgi( 150, 528):������������  �� T
      L_(231) = L_opebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 534):�
      if(L_(231).and..not.L0_ulabo) then
         R0_ilabo=R0_olabo
      else
         R0_ilabo=max(R_(32)-deltat,0.0)
      endif
      L_(238)=R0_ilabo.gt.0.0
      L0_ulabo=L_(231)
C FDA_doz_logic.fgi( 150, 534):������������  �� T
      L_(232) = L_upebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 540):�
      if(L_(232).and..not.L0_imabo) then
         R0_amabo=R0_emabo
      else
         R0_amabo=max(R_(33)-deltat,0.0)
      endif
      L_(239)=R0_amabo.gt.0.0
      L0_imabo=L_(232)
C FDA_doz_logic.fgi( 150, 540):������������  �� T
      L_(233) = L_arebo.AND.L_idebo
C FDA_doz_logic.fgi( 139, 546):�
      if(L_(233).and..not.L0_apabo) then
         R0_omabo=R0_umabo
      else
         R0_omabo=max(R_(34)-deltat,0.0)
      endif
      L_(240)=R0_omabo.gt.0.0
      L0_apabo=L_(233)
C FDA_doz_logic.fgi( 150, 546):������������  �� T
      !��������� R_(35) = FDA_doz_logicCycb1 /500/
      R_(35)=R0_epabo
C FDA_doz_logic.fgi( 165, 548):���������,ycb1
      if(L_(240)) then
         R_(197)=R_(35)
      endif
C FDA_doz_logic.fgi( 169, 546):���� � ������������� �������
      !��������� R_(36) = FDA_doz_logicCycb2 /300/
      R_(36)=R0_ipabo
C FDA_doz_logic.fgi( 165, 542):���������,ycb2
      if(L_(239)) then
         R_(197)=R_(36)
      endif
C FDA_doz_logic.fgi( 169, 540):���� � ������������� �������
      !��������� R_(37) = FDA_doz_logicCycb3 /300/
      R_(37)=R0_opabo
C FDA_doz_logic.fgi( 165, 536):���������,ycb3
      if(L_(238)) then
         R_(197)=R_(37)
      endif
C FDA_doz_logic.fgi( 169, 534):���� � ������������� �������
      !��������� R_(38) = FDA_doz_logicCycb4 /300/
      R_(38)=R0_upabo
C FDA_doz_logic.fgi( 165, 530):���������,ycb4
      if(L_(237)) then
         R_(197)=R_(38)
      endif
C FDA_doz_logic.fgi( 169, 528):���� � ������������� �������
      !��������� R_(39) = FDA_doz_logicCycb5 /300/
      R_(39)=R0_arabo
C FDA_doz_logic.fgi( 165, 524):���������,ycb5
      if(L_(236)) then
         R_(197)=R_(39)
      endif
C FDA_doz_logic.fgi( 169, 522):���� � ������������� �������
      !��������� R_(40) = FDA_doz_logicCycb6 /500/
      R_(40)=R0_erabo
C FDA_doz_logic.fgi( 165, 518):���������,ycb6
      if(L_(235)) then
         R_(197)=R_(40)
      endif
C FDA_doz_logic.fgi( 169, 516):���� � ������������� �������
      !��������� R_(41) = FDA_doz_logicCycb7 /690/
      R_(41)=R0_irabo
C FDA_doz_logic.fgi( 165, 512):���������,ycb7
      if(L_(234)) then
         R_(197)=R_(41)
      endif
C FDA_doz_logic.fgi( 169, 510):���� � ������������� �������
      R_i=R_(197)
C FDA_doz_logic.fgi( 169, 558):������-�������: ���������� ��� �������������� ������
      L_(241) = L_esabo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 558):�
      if(L_(241).and..not.L0_asabo) then
         R0_orabo=R0_urabo
      else
         R0_orabo=max(R_(42)-deltat,0.0)
      endif
      L_(242)=R0_orabo.gt.0.0
      L0_asabo=L_(241)
C FDA_doz_logic.fgi(  57, 558):������������  �� T
      !��������� R_(43) = FDA_doz_logicCxmvp /7760/
      R_(43)=R0_isabo
C FDA_doz_logic.fgi(  72, 560):���������,xmvp
      if(L_(242)) then
         R_(198)=R_(43)
      endif
C FDA_doz_logic.fgi(  76, 558):���� � ������������� �������
      L_(243) = L_etabo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 552):�
      if(L_(243).and..not.L0_atabo) then
         R0_osabo=R0_usabo
      else
         R0_osabo=max(R_(44)-deltat,0.0)
      endif
      L_(244)=R0_osabo.gt.0.0
      L0_atabo=L_(243)
C FDA_doz_logic.fgi(  57, 552):������������  �� T
      !��������� R_(45) = FDA_doz_logicCxbvp /1670/
      R_(45)=R0_itabo
C FDA_doz_logic.fgi(  72, 554):���������,xbvp
      if(L_(244)) then
         R_(198)=R_(45)
      endif
C FDA_doz_logic.fgi(  76, 552):���� � ������������� �������
      !��������� R_(46) = FDA_doz_logicCxcb0 /0/
      R_(46)=R0_utabo
C FDA_doz_logic.fgi(  72, 502):���������,xcb0
      if(L_exabo.and..not.L0_ivabo) then
         R0_avabo=R0_evabo
      else
         R0_avabo=max(R_(47)-deltat,0.0)
      endif
      L_(245)=R0_avabo.gt.0.0
      L0_ivabo=L_exabo
C FDA_doz_logic.fgi(  57, 500):������������  �� T
      if(L_(245)) then
         R_(198)=R_(46)
      endif
C FDA_doz_logic.fgi(  76, 500):���� � ������������� �������
      if(L_exabo.and..not.L0_axabo) then
         R0_ovabo=R0_uvabo
      else
         R0_ovabo=max(R_(48)-deltat,0.0)
      endif
      L_(246)=R0_ovabo.gt.0.0
      L0_axabo=L_exabo
C FDA_doz_logic.fgi(  57, 485):������������  �� T
      !��������� R_(51) = FDA_doz_logicCzminpos /0/
      R_(51)=R0_ixabo
C FDA_doz_logic.fgi(  72, 481):���������,zminpos
      !��������� R_(52) = FDA_doz_logicCzmaxpos /2300/
      R_(52)=R0_oxabo
C FDA_doz_logic.fgi(  72, 492):���������,zmaxpos
      L_(253) = L_umebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 510):�
      if(L_(253).and..not.L0_afebo) then
         R0_odebo=R0_udebo
      else
         R0_odebo=max(R_(53)-deltat,0.0)
      endif
      L_(260)=R0_odebo.gt.0.0
      L0_afebo=L_(253)
C FDA_doz_logic.fgi(  57, 510):������������  �� T
      L_(254) = L_apebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 516):�
      if(L_(254).and..not.L0_ofebo) then
         R0_efebo=R0_ifebo
      else
         R0_efebo=max(R_(54)-deltat,0.0)
      endif
      L_(261)=R0_efebo.gt.0.0
      L0_ofebo=L_(254)
C FDA_doz_logic.fgi(  57, 516):������������  �� T
      L_(255) = L_epebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 522):�
      if(L_(255).and..not.L0_ekebo) then
         R0_ufebo=R0_akebo
      else
         R0_ufebo=max(R_(55)-deltat,0.0)
      endif
      L_(262)=R0_ufebo.gt.0.0
      L0_ekebo=L_(255)
C FDA_doz_logic.fgi(  57, 522):������������  �� T
      L_(256) = L_ipebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 528):�
      if(L_(256).and..not.L0_ukebo) then
         R0_ikebo=R0_okebo
      else
         R0_ikebo=max(R_(56)-deltat,0.0)
      endif
      L_(263)=R0_ikebo.gt.0.0
      L0_ukebo=L_(256)
C FDA_doz_logic.fgi(  57, 528):������������  �� T
      L_(257) = L_opebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 534):�
      if(L_(257).and..not.L0_ilebo) then
         R0_alebo=R0_elebo
      else
         R0_alebo=max(R_(57)-deltat,0.0)
      endif
      L_(264)=R0_alebo.gt.0.0
      L0_ilebo=L_(257)
C FDA_doz_logic.fgi(  57, 534):������������  �� T
      L_(258) = L_upebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 540):�
      if(L_(258).and..not.L0_amebo) then
         R0_olebo=R0_ulebo
      else
         R0_olebo=max(R_(58)-deltat,0.0)
      endif
      L_(265)=R0_olebo.gt.0.0
      L0_amebo=L_(258)
C FDA_doz_logic.fgi(  57, 540):������������  �� T
      L_(259) = L_arebo.AND.L_idebo
C FDA_doz_logic.fgi(  46, 546):�
      if(L_(259).and..not.L0_omebo) then
         R0_emebo=R0_imebo
      else
         R0_emebo=max(R_(59)-deltat,0.0)
      endif
      L_(266)=R0_emebo.gt.0.0
      L0_omebo=L_(259)
C FDA_doz_logic.fgi(  57, 546):������������  �� T
      !��������� R_(60) = FDA_doz_logicCxcb1 /950/
      R_(60)=R0_erebo
C FDA_doz_logic.fgi(  72, 548):���������,xcb1
      if(L_(266)) then
         R_(198)=R_(60)
      endif
C FDA_doz_logic.fgi(  76, 546):���� � ������������� �������
      !��������� R_(61) = FDA_doz_logicCxcb2 /2190/
      R_(61)=R0_irebo
C FDA_doz_logic.fgi(  72, 542):���������,xcb2
      if(L_(265)) then
         R_(198)=R_(61)
      endif
C FDA_doz_logic.fgi(  76, 540):���� � ������������� �������
      !��������� R_(62) = FDA_doz_logicCxcb3 /3490/
      R_(62)=R0_orebo
C FDA_doz_logic.fgi(  72, 536):���������,xcb3
      if(L_(264)) then
         R_(198)=R_(62)
      endif
C FDA_doz_logic.fgi(  76, 534):���� � ������������� �������
      !��������� R_(63) = FDA_doz_logicCxcb4 /4720/
      R_(63)=R0_urebo
C FDA_doz_logic.fgi(  72, 530):���������,xcb4
      if(L_(263)) then
         R_(198)=R_(63)
      endif
C FDA_doz_logic.fgi(  76, 528):���� � ������������� �������
      !��������� R_(64) = FDA_doz_logicCxcb5 /6000/
      R_(64)=R0_asebo
C FDA_doz_logic.fgi(  72, 524):���������,xcb5
      if(L_(262)) then
         R_(198)=R_(64)
      endif
C FDA_doz_logic.fgi(  76, 522):���� � ������������� �������
      !��������� R_(65) = FDA_doz_logicCxcb6 /7280/
      R_(65)=R0_esebo
C FDA_doz_logic.fgi(  72, 518):���������,xcb6
      if(L_(261)) then
         R_(198)=R_(65)
      endif
C FDA_doz_logic.fgi(  76, 516):���� � ������������� �������
      !��������� R_(66) = FDA_doz_logicCxcb7 /8210/
      R_(66)=R0_isebo
C FDA_doz_logic.fgi(  72, 512):���������,xcb7
      if(L_(260)) then
         R_(198)=R_(66)
      endif
C FDA_doz_logic.fgi(  76, 510):���� � ������������� �������
      R_e=R_(198)
C FDA_doz_logic.fgi(  76, 546):������-�������: ���������� ��� �������������� ������
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_apako,4),L_itako,L_otako
     &,R8_uxako,C30_amako,
     & L_adepad,L_afepad,L_upepad,I_isako,I_osako,R_imako
     &,R_emako,
     & R_udako,REAL(R_efako,4),R_akako,
     & REAL(R_ikako,4),R_ifako,REAL(R_ufako,4),I_orako,
     & I_usako,I_esako,I_asako,L_okako,L_atako,L_ivako,L_ekako
     &,
     & L_ofako,L_ilako,L_elako,L_utako,L_afako,L_ulako,
     & L_axako,L_etako,L_omako,L_umako,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_alako,L_ukako,L_ovako,R_irako,REAL(R_olako,4),L_uvako
     &,L_exako,
     & L_epako,L_ipako,L_opako,L_arako,L_erako,L_upako)
      !}

      if(L_erako.or.L_arako.or.L_upako.or.L_opako.or.L_ipako.or.L_epako
     &) then      
                  I_urako = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urako = z'40000000'
      endif
C FDA_doz_logic.fgi(1097, 483):���� ���������� �������� ��������,20FDA21AL004C21
      L_(267)=R_oxako.gt.R0_ixako
C FDA_doz_logic.fgi(1139, 464):���������� >
      R_(68) = -0.02
C FDA_doz_logic.fgi(1134, 471):��������� (RE4) (�������)
      R_(69) = R_(68) * R8_uxako
C FDA_doz_logic.fgi(1139, 471):����������
      R_(70) = 0.0
C FDA_doz_logic.fgi(1140, 474):��������� (RE4) (�������)
      if(L_(267)) then
         R_adeko=R_(69)
      else
         R_adeko=R_(70)
      endif
C FDA_doz_logic.fgi(1146, 473):���� RE IN LO CH7
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ereko,4),L_oveko,L_uveko
     &,R8_uleko,C30_epeko,
     & L_adepad,L_afepad,L_upepad,I_oteko,I_uteko,R_opeko
     &,R_ipeko,
     & R_ufeko,REAL(R_ekeko,4),R_aleko,
     & REAL(R_ileko,4),R_ikeko,REAL(R_ukeko,4),I_useko,
     & I_aveko,I_iteko,I_eteko,L_oleko,L_eveko,L_oxeko,L_eleko
     &,
     & L_okeko,L_omeko,L_imeko,L_axeko,L_akeko,L_apeko,
     & L_ebiko,L_iveko,L_upeko,L_areko,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_emeko,L_ameko,L_uxeko,R_oseko,REAL(R_umeko,4),L_abiko
     &,L_ibiko,
     & L_ireko,L_oreko,L_ureko,L_eseko,L_iseko,L_aseko)
      !}

      if(L_iseko.or.L_eseko.or.L_aseko.or.L_ureko.or.L_oreko.or.L_ireko
     &) then      
                  I_ateko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ateko = z'40000000'
      endif
C FDA_doz_logic.fgi(1093, 646):���� ���������� �������� ��������,20FDA21AL004C19
      R_(72) = -0.02
C FDA_doz_logic.fgi(1133, 632):��������� (RE4) (�������)
      R_(73) = R_(72) * R8_uleko
C FDA_doz_logic.fgi(1138, 632):����������
      R_(74) = 0.0
C FDA_doz_logic.fgi(1139, 635):��������� (RE4) (�������)
      L_(277)=R_efiko.gt.R0_afiko
C FDA_doz_logic.fgi(1019, 462):���������� >
      R_(76) = -0.02
C FDA_doz_logic.fgi(1014, 469):��������� (RE4) (�������)
      R_(78) = 0.0
C FDA_doz_logic.fgi(1020, 472):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_asiko,4),L_ixiko,L_oxiko
     &,R8_omiko,C30_ariko,
     & L_adepad,L_afepad,L_upepad,I_iviko,I_oviko,R_iriko
     &,R_eriko,
     & R_okiko,REAL(R_aliko,4),R_uliko,
     & REAL(R_emiko,4),R_eliko,REAL(R_oliko,4),I_otiko,
     & I_uviko,I_eviko,I_aviko,L_imiko,L_axiko,L_iboko,L_amiko
     &,
     & L_iliko,L_ipiko,L_epiko,L_uxiko,L_ukiko,L_upiko,
     & L_adoko,L_exiko,L_oriko,L_uriko,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_apiko,L_umiko,L_oboko,R_itiko,REAL(R_opiko,4),L_uboko
     &,L_edoko,
     & L_esiko,L_isiko,L_osiko,L_atiko,L_etiko,L_usiko)
      !}

      if(L_etiko.or.L_atiko.or.L_usiko.or.L_osiko.or.L_isiko.or.L_esiko
     &) then      
                  I_utiko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utiko = z'40000000'
      endif
C FDA_doz_logic.fgi( 977, 476):���� ���������� �������� ��������,20FDA21AL001C17
      R_(77) = R_(76) * R8_omiko
C FDA_doz_logic.fgi(1019, 469):����������
      if(L_(277)) then
         R_ikiko=R_(77)
      else
         R_ikiko=R_(78)
      endif
C FDA_doz_logic.fgi(1026, 471):���� RE IN LO CH7
      L_(278)=R_odoko.gt.R0_idoko
C FDA_doz_logic.fgi( 887, 463):���������� >
      R_(80) = -0.02
C FDA_doz_logic.fgi( 882, 470):��������� (RE4) (�������)
      R_(82) = 0.0
C FDA_doz_logic.fgi( 888, 473):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_iroko,4),L_uvoko,L_axoko
     &,R8_amoko,C30_ipoko,
     & L_adepad,L_afepad,L_upepad,I_utoko,I_avoko,R_upoko
     &,R_opoko,
     & R_akoko,REAL(R_ikoko,4),R_eloko,
     & REAL(R_oloko,4),R_okoko,REAL(R_aloko,4),I_atoko,
     & I_evoko,I_otoko,I_itoko,L_uloko,L_ivoko,L_uxoko,L_iloko
     &,
     & L_ukoko,L_umoko,L_omoko,L_exoko,L_ekoko,L_epoko,
     & L_ibuko,L_ovoko,L_aroko,L_eroko,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_imoko,L_emoko,L_abuko,R_usoko,REAL(R_apoko,4),L_ebuko
     &,L_obuko,
     & L_oroko,L_uroko,L_asoko,L_isoko,L_osoko,L_esoko)
      !}

      if(L_osoko.or.L_isoko.or.L_esoko.or.L_asoko.or.L_uroko.or.L_oroko
     &) then      
                  I_etoko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etoko = z'40000000'
      endif
C FDA_doz_logic.fgi( 845, 477):���� ���������� �������� ��������,20FDA21AL001C30
      R_(81) = R_(80) * R8_amoko
C FDA_doz_logic.fgi( 887, 470):����������
      if(L_(278)) then
         R_ufoko=R_(81)
      else
         R_ufoko=R_(82)
      endif
C FDA_doz_logic.fgi( 894, 472):���� RE IN LO CH7
      L_(279)=R_aduko.gt.R0_ubuko
C FDA_doz_logic.fgi( 743, 463):���������� >
      R_(84) = -0.02
C FDA_doz_logic.fgi( 738, 470):��������� (RE4) (�������)
      R_(86) = 0.0
C FDA_doz_logic.fgi( 744, 473):��������� (RE4) (�������)
      R_(87) = -0.02
C FDA_doz_logic.fgi(1024, 545):��������� (RE4) (�������)
      R_(89) = 0.0
C FDA_doz_logic.fgi(1030, 548):��������� (RE4) (�������)
      R_(90) = -0.02
C FDA_doz_logic.fgi(1024, 584):��������� (RE4) (�������)
      R_(92) = 0.0
C FDA_doz_logic.fgi(1030, 587):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uruko,4),L_exuko,L_ixuko
     &,R8_epilo,C30_upuko,
     & L_adepad,L_afepad,L_upepad,I_evuko,I_ivuko,R_eruko
     &,R_aruko,
     & R_okuko,REAL(R_aluko,4),R_uluko,
     & REAL(R_emuko,4),R_eluko,REAL(R_oluko,4),I_ituko,
     & I_ovuko,I_avuko,I_utuko,L_imuko,L_uvuko,L_ebalo,L_amuko
     &,
     & L_iluko,L_epuko,L_apuko,L_oxuko,L_ukuko,L_opuko,
     & L_ubalo,L_axuko,L_iruko,L_oruko,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_umuko,L_omuko,L_ibalo,R_etuko,REAL(R_ipuko,4),L_obalo
     &,L_adalo,
     & L_asuko,L_esuko,L_isuko,L_usuko,L_atuko,L_osuko)
      !}

      if(L_atuko.or.L_usuko.or.L_osuko.or.L_isuko.or.L_esuko.or.L_asuko
     &) then      
                  I_otuko = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_otuko = z'40000000'
      endif
C FDA_doz_logic.fgi( 959, 640):���� ���������� �������� ��������,20FDA21AL001C41
      R_(88) = R_(87) * R8_epilo
C FDA_doz_logic.fgi(1029, 545):����������
      R_(93) = -0.02
C FDA_doz_logic.fgi(1024, 623):��������� (RE4) (�������)
      R_(95) = 0.0
C FDA_doz_logic.fgi(1030, 626):��������� (RE4) (�������)
      R_(97) = -0.02
C FDA_doz_logic.fgi( 892, 546):��������� (RE4) (�������)
      R_(98) = R_(97) * R8_epilo
C FDA_doz_logic.fgi( 897, 546):����������
      R_(99) = 0.0
C FDA_doz_logic.fgi( 898, 549):��������� (RE4) (�������)
      R_(100) = -0.02
C FDA_doz_logic.fgi( 892, 585):��������� (RE4) (�������)
      R_(102) = 0.0
C FDA_doz_logic.fgi( 898, 588):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_evalo,4),L_odelo,L_udelo
     &,R8_erilo,C30_etalo,
     & L_adepad,L_afepad,L_upepad,I_obelo,I_ubelo,R_otalo
     &,R_italo,
     & R_apalo,REAL(R_ipalo,4),R_eralo,
     & REAL(R_oralo,4),R_opalo,REAL(R_aralo,4),I_uxalo,
     & I_adelo,I_ibelo,I_ebelo,L_uralo,L_edelo,L_ofelo,L_iralo
     &,
     & L_upalo,L_osalo,L_isalo,L_afelo,L_epalo,L_atalo,
     & L_ekelo,L_idelo,L_utalo,L_avalo,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_esalo,L_asalo,L_ufelo,R_oxalo,REAL(R_usalo,4),L_akelo
     &,L_ikelo,
     & L_ivalo,L_ovalo,L_uvalo,L_exalo,L_ixalo,L_axalo)
      !}

      if(L_ixalo.or.L_exalo.or.L_axalo.or.L_uvalo.or.L_ovalo.or.L_ivalo
     &) then      
                  I_abelo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_abelo = z'40000000'
      endif
C FDA_doz_logic.fgi( 826, 641):���� ���������� �������� ��������,20FDA21AL001C43
      R_(101) = R_(100) * R8_erilo
C FDA_doz_logic.fgi( 897, 585):����������
      R_(91) = R_(90) * R8_erilo
C FDA_doz_logic.fgi(1029, 584):����������
      R_(103) = -0.02
C FDA_doz_logic.fgi( 892, 624):��������� (RE4) (�������)
      R_(105) = 0.0
C FDA_doz_logic.fgi( 898, 627):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ubilo,4),L_elilo,L_ililo
     &,R8_ivelo,C30_uxelo,
     & L_adepad,L_afepad,L_upepad,I_ekilo,I_ikilo,R_ebilo
     &,R_abilo,
     & R_iselo,REAL(R_uselo,4),R_otelo,
     & REAL(R_avelo,4),R_atelo,REAL(R_itelo,4),I_ifilo,
     & I_okilo,I_akilo,I_ufilo,L_evelo,L_ukilo,L_emilo,L_utelo
     &,
     & L_etelo,L_exelo,L_axelo,L_olilo,L_oselo,L_oxelo,
     & L_umilo,L_alilo,L_ibilo,L_obilo,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_uvelo,L_ovelo,L_imilo,R_efilo,REAL(R_ixelo,4),L_omilo
     &,L_apilo,
     & L_adilo,L_edilo,L_idilo,L_udilo,L_afilo,L_odilo)
      !}

      if(L_afilo.or.L_udilo.or.L_odilo.or.L_idilo.or.L_edilo.or.L_adilo
     &) then      
                  I_ofilo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofilo = z'40000000'
      endif
C FDA_doz_logic.fgi( 701, 477):���� ���������� �������� ��������,20FDA21AL001C26
      R_(85) = R_(84) * R8_ivelo
C FDA_doz_logic.fgi( 743, 470):����������
      if(L_(279)) then
         R_urelo=R_(85)
      else
         R_urelo=R_(86)
      endif
C FDA_doz_logic.fgi( 750, 472):���� RE IN LO CH7
      R_(108) = -0.02
C FDA_doz_logic.fgi( 752, 548):��������� (RE4) (�������)
      R_(109) = R_(108) * R8_epilo
C FDA_doz_logic.fgi( 757, 548):����������
      R_(110) = 0.0
C FDA_doz_logic.fgi( 758, 551):��������� (RE4) (�������)
      R_(111) = -0.02
C FDA_doz_logic.fgi( 752, 587):��������� (RE4) (�������)
      R_(112) = R_(111) * R8_erilo
C FDA_doz_logic.fgi( 757, 587):����������
      R_(113) = 0.0
C FDA_doz_logic.fgi( 758, 590):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_obolo,4),L_alolo,L_elolo
     &,R8_evilo,C30_oxilo,
     & L_adepad,L_afepad,L_upepad,I_akolo,I_ekolo,R_abolo
     &,R_uxilo,
     & R_esilo,REAL(R_osilo,4),R_itilo,
     & REAL(R_utilo,4),R_usilo,REAL(R_etilo,4),I_efolo,
     & I_ikolo,I_ufolo,I_ofolo,L_avilo,L_okolo,L_amolo,L_otilo
     &,
     & L_atilo,L_axilo,L_uvilo,L_ilolo,L_isilo,L_ixilo,
     & L_omolo,L_ukolo,L_ebolo,L_ibolo,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_ovilo,L_ivilo,L_emolo,R_afolo,REAL(R_exilo,4),L_imolo
     &,L_umolo,
     & L_ubolo,L_adolo,L_edolo,L_odolo,L_udolo,L_idolo)
      !}

      if(L_udolo.or.L_odolo.or.L_idolo.or.L_edolo.or.L_adolo.or.L_ubolo
     &) then      
                  I_ifolo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifolo = z'40000000'
      endif
C FDA_doz_logic.fgi( 676, 642):���� ���������� �������� ��������,20FDA21AL001C42
      R_(94) = R_(93) * R8_evilo
C FDA_doz_logic.fgi(1029, 623):����������
      R_(104) = R_(103) * R8_evilo
C FDA_doz_logic.fgi( 897, 624):����������
      R_(114) = -0.02
C FDA_doz_logic.fgi( 752, 626):��������� (RE4) (�������)
      R_(115) = R_(114) * R8_evilo
C FDA_doz_logic.fgi( 757, 626):����������
      R_(116) = 0.0
C FDA_doz_logic.fgi( 758, 629):��������� (RE4) (�������)
      I_(98) = 0
C FDA_doz_logic.fgi( 607, 389):��������� ������������� IN (�������)
      I_(99) = 1
C FDA_doz_logic.fgi( 607, 392):��������� ������������� IN (�������)
      I_(100) = 0
C FDA_doz_logic.fgi( 607, 424):��������� ������������� IN (�������)
      I_(101) = 1
C FDA_doz_logic.fgi( 607, 427):��������� ������������� IN (�������)
      R_(118) = 880
C FDA_doz_logic.fgi( 328, 715):��������� (RE4) (�������)
      R_atolo = R_(118) + R_ipapo
C FDA_doz_logic.fgi( 331, 715):��������
      L_(346)=R_ukemo.gt.R0_efemo
C FDA_doz_logic.fgi( 577, 365):���������� >
      L_(347)=R_ikemo.lt.R0_ifemo
C FDA_doz_logic.fgi( 586, 393):���������� <
      L_(351)=R_ukemo.lt.R0_ofemo
C FDA_doz_logic.fgi( 577, 372):���������� <
      L_(353)=R_ikemo.gt.R0_ekemo
C FDA_doz_logic.fgi( 559, 383):���������� >
      L_(355)=R_ukemo.lt.R0_okemo
C FDA_doz_logic.fgi( 559, 376):���������� <
      L_(356)=R_imemo.lt.R0_elemo
C FDA_doz_logic.fgi( 577, 401):���������� <
      L_(361)=R_imemo.gt.R0_ilemo
C FDA_doz_logic.fgi( 577, 408):���������� >
      L_(360)=R_apemo.gt.R0_ulemo
C FDA_doz_logic.fgi( 586, 429):���������� >
      L_(363)=R_imemo.gt.R0_ememo
C FDA_doz_logic.fgi( 559, 406):���������� >
      L_(365)=R_apemo.gt.R0_omemo
C FDA_doz_logic.fgi( 559, 413):���������� >
      L_(366)=R_apemo.lt.R0_umemo
C FDA_doz_logic.fgi( 559, 419):���������� <
      R_(119) = 1050
C FDA_doz_logic.fgi( 328, 687):��������� (RE4) (�������)
      R_oremo = R_(119) + R_edomo
C FDA_doz_logic.fgi( 331, 687):��������
      R_(120) = 880
C FDA_doz_logic.fgi( 583, 702):��������� (RE4) (�������)
      R_asemo = R_(120) + R_osemo
C FDA_doz_logic.fgi( 586, 702):��������
      L_akoto=.true.
C FDA_doz_logic.fgi( 467,  92):��������� ���������� (�������)
      L_ilexo=.true.
C FDA_doz_logic.fgi( 319,  92):��������� ���������� (�������)
      L_umubu=.true.
C FDA_doz_logic.fgi( 174,  92):��������� ���������� (�������)
      L_eraku=.true.
C FDA_doz_logic.fgi(  28,  93):��������� ���������� (�������)
      R_edefu = 0
C FDA_doz_logic.fgi(  29, 168):��������� (RE4) (�������)
      R_idefu = 0
C FDA_doz_logic.fgi( 175, 167):��������� (RE4) (�������)
      R_(121) = R_ilumad + R_elumad
C FDA_doz_logic.fgi( 471, 447):��������
      R_(122) = R_emumad + R_amumad
C FDA_doz_logic.fgi( 471, 475):��������
      R_(123) = R_ulumad + R_olumad
C FDA_doz_logic.fgi( 470, 461):��������
      R_(127) = 0.0
C FDA_doz_logic.fgi( 467, 449):��������� (RE4) (�������)
      R_(128) = 0.0
C FDA_doz_logic.fgi( 467, 463):��������� (RE4) (�������)
      R_(129) = 0.0
C FDA_doz_logic.fgi( 467, 477):��������� (RE4) (�������)
      R_(130) = 0.0
C FDA_doz_logic.fgi( 464, 431):��������� (RE4) (�������)
      R_(131) = R_ilumad + R_elumad
C FDA_doz_logic.fgi( 394, 446):��������
      R_(132) = R_emumad + R_amumad
C FDA_doz_logic.fgi( 394, 474):��������
      R_(133) = R_ulumad + R_olumad
C FDA_doz_logic.fgi( 393, 460):��������
      R_(137) = 0.0
C FDA_doz_logic.fgi( 390, 448):��������� (RE4) (�������)
      R_(138) = 0.0
C FDA_doz_logic.fgi( 390, 462):��������� (RE4) (�������)
      R_(139) = 0.0
C FDA_doz_logic.fgi( 390, 476):��������� (RE4) (�������)
      R_(140) = 0.0
C FDA_doz_logic.fgi( 386, 430):��������� (RE4) (�������)
      R_(141) = R_ilumad + R_elumad
C FDA_doz_logic.fgi( 314, 447):��������
      R_(142) = R_emumad + R_amumad
C FDA_doz_logic.fgi( 314, 475):��������
      R_(143) = R_ulumad + R_olumad
C FDA_doz_logic.fgi( 313, 461):��������
      R_(147) = 0.0
C FDA_doz_logic.fgi( 310, 449):��������� (RE4) (�������)
      R_(148) = 0.0
C FDA_doz_logic.fgi( 310, 463):��������� (RE4) (�������)
      R_(149) = 0.0
C FDA_doz_logic.fgi( 310, 477):��������� (RE4) (�������)
      R_(150) = 0.0
C FDA_doz_logic.fgi( 306, 429):��������� (RE4) (�������)
      R_(151) = R_ilumad + R_elumad
C FDA_doz_logic.fgi( 232, 446):��������
      R_(152) = R_emumad + R_amumad
C FDA_doz_logic.fgi( 232, 474):��������
      R_(153) = R_ulumad + R_olumad
C FDA_doz_logic.fgi( 231, 460):��������
      R_(157) = 0.0
C FDA_doz_logic.fgi( 228, 448):��������� (RE4) (�������)
      R_(158) = 0.0
C FDA_doz_logic.fgi( 228, 462):��������� (RE4) (�������)
      R_(159) = 0.0
C FDA_doz_logic.fgi( 228, 476):��������� (RE4) (�������)
      R_(160) = 0.0
C FDA_doz_logic.fgi( 225, 429):��������� (RE4) (�������)
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_akepad,4),L_epepad,L_ipepad
     &,R8_abepad,C30_odepad,
     & L_adepad,L_afepad,L_upepad,I_emepad,I_imepad,R_ifepad
     &,R_efepad,
     & R_avapad,REAL(R_ivapad,4),R_exapad,
     & REAL(R_oxapad,4),R_ovapad,REAL(R_axapad,4),I_ilepad
     &,
     & I_omepad,I_amepad,I_ulepad,L_uxapad,L_umepad,L_irepad
     &,L_ixapad,
     & L_uvapad,L_ubepad,L_obepad,L_opepad,L_evapad,L_idepad
     &,
     & L_asepad,L_apepad,L_ofepad,L_ufepad,REAL(R8_axorad
     &,8),REAL(1.0,4),R8_udepad,
     & L_ibepad,L_ebepad,L_orepad,R_isepad,REAL(R_edepad,4
     &),L_urepad,L_esepad,
     & L_ekepad,L_ikepad,L_okepad,L_alepad,L_elepad,L_ukepad
     &)
      !}

      if(L_elepad.or.L_alepad.or.L_ukepad.or.L_okepad.or.L_ikepad.or.L_e
     &kepad) then      
                  I_olepad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_olepad = z'40000000'
      endif
C FDA_doz_logic.fgi( 146, 743):���� ���������� �������� ��������,20FDA21AE704
      R_utapad=R_isepad
C FDA_doz_logic.fgi( 176, 722):������,20FDA21AE704VS01
      L_(597)=R_utapad.lt.R0_evomad
C FDA_doz_logic.fgi( 239, 487):���������� <
      L_(559)=R_utapad.lt.R0_idamad
C FDA_doz_logic.fgi( 400, 485):���������� <
      L_(578)=R_utapad.lt.R0_uxemad
C FDA_doz_logic.fgi( 320, 485):���������� <
      L_(540)=R_utapad.lt.R0_ikolad
C FDA_doz_logic.fgi( 477, 487):���������� <
      R_(164) = 1.0
C FDA_doz_logic.fgi(  51, 352):��������� (RE4) (�������)
      R_(165) = 0.0
C FDA_doz_logic.fgi(  51, 354):��������� (RE4) (�������)
      R_(166) = 1.0
C FDA_doz_logic.fgi(  51, 363):��������� (RE4) (�������)
      R_(167) = 0.0
C FDA_doz_logic.fgi(  51, 365):��������� (RE4) (�������)
      R_(168) = 1.0
C FDA_doz_logic.fgi(  51, 374):��������� (RE4) (�������)
      R_(169) = 0.0
C FDA_doz_logic.fgi(  51, 376):��������� (RE4) (�������)
      R_(170) = 1.0
C FDA_doz_logic.fgi(  51, 385):��������� (RE4) (�������)
      R_(171) = 0.0
C FDA_doz_logic.fgi(  51, 387):��������� (RE4) (�������)
      R_(172) = 1.0
C FDA_doz_logic.fgi(  51, 396):��������� (RE4) (�������)
      R_(173) = 0.0
C FDA_doz_logic.fgi(  51, 398):��������� (RE4) (�������)
      R_(174) = 1.0
C FDA_doz_logic.fgi(  51, 408):��������� (RE4) (�������)
      R_(175) = 0.0
C FDA_doz_logic.fgi(  51, 410):��������� (RE4) (�������)
      R_(176) = 1.0
C FDA_doz_logic.fgi(  51, 419):��������� (RE4) (�������)
      R_(177) = 0.0
C FDA_doz_logic.fgi(  51, 421):��������� (RE4) (�������)
      R_(178) = 1.0
C FDA_doz_logic.fgi(  51, 431):��������� (RE4) (�������)
      R_(179) = 0.0
C FDA_doz_logic.fgi(  51, 433):��������� (RE4) (�������)
      R_(180) = 0.0
C FDA_doz_logic.fgi(  50, 593):��������� (RE4) (�������)
      R_(181) = 0.0
C FDA_doz_logic.fgi(  50, 621):��������� (RE4) (�������)
      R_(182) = 0.0
C FDA_doz_logic.fgi(  50, 650):��������� (RE4) (�������)
      L_iverad=.false.
      if(L_edasad) then
         if(I0_everad.ge.I0_overad)  L_iverad=.true.
         I0_everad=I_(102)+1
      else
         I0_everad=0
      endif
C FDA_doz_logic.fgi(  48, 348):�������� ������ �� N �����
      if(L_iverad) then
         R_irerad=R_(164)
      else
         R_irerad=R_(165)
      endif
C FDA_doz_logic.fgi(  55, 353):���� RE IN LO CH7
      L_axerad=.false.
      if(L_idasad) then
         if(I0_uverad.ge.I0_exerad)  L_axerad=.true.
         I0_uverad=I_(103)+1
      else
         I0_uverad=0
      endif
C FDA_doz_logic.fgi(  48, 359):�������� ������ �� N �����
      if(L_axerad) then
         R_orerad=R_(166)
      else
         R_orerad=R_(167)
      endif
C FDA_doz_logic.fgi(  55, 364):���� RE IN LO CH7
      L_oxerad=.false.
      if(L_odasad) then
         if(I0_ixerad.ge.I0_uxerad)  L_oxerad=.true.
         I0_ixerad=I_(104)+1
      else
         I0_ixerad=0
      endif
C FDA_doz_logic.fgi(  48, 370):�������� ������ �� N �����
      if(L_oxerad) then
         R_urerad=R_(168)
      else
         R_urerad=R_(169)
      endif
C FDA_doz_logic.fgi(  55, 375):���� RE IN LO CH7
      L_ebirad=.false.
      if(L_udasad) then
         if(I0_abirad.ge.I0_ibirad)  L_ebirad=.true.
         I0_abirad=I_(105)+1
      else
         I0_abirad=0
      endif
C FDA_doz_logic.fgi(  48, 381):�������� ������ �� N �����
      if(L_ebirad) then
         R_aserad=R_(170)
      else
         R_aserad=R_(171)
      endif
C FDA_doz_logic.fgi(  55, 386):���� RE IN LO CH7
      L_ubirad=.false.
      if(L_afasad) then
         if(I0_obirad.ge.I0_adirad)  L_ubirad=.true.
         I0_obirad=I_(106)+1
      else
         I0_obirad=0
      endif
C FDA_doz_logic.fgi(  48, 392):�������� ������ �� N �����
      if(L_ubirad) then
         R_eserad=R_(172)
      else
         R_eserad=R_(173)
      endif
C FDA_doz_logic.fgi(  55, 397):���� RE IN LO CH7
      L_idirad=.false.
      if(L_efasad) then
         if(I0_edirad.ge.I0_odirad)  L_idirad=.true.
         I0_edirad=I_(107)+1
      else
         I0_edirad=0
      endif
C FDA_doz_logic.fgi(  48, 404):�������� ������ �� N �����
      if(L_idirad) then
         R_iserad=R_(174)
      else
         R_iserad=R_(175)
      endif
C FDA_doz_logic.fgi(  55, 409):���� RE IN LO CH7
      L_afirad=.false.
      if(L_ifasad) then
         if(I0_udirad.ge.I0_efirad)  L_afirad=.true.
         I0_udirad=I_(108)+1
      else
         I0_udirad=0
      endif
C FDA_doz_logic.fgi(  48, 415):�������� ������ �� N �����
      if(L_afirad) then
         R_oserad=R_(176)
      else
         R_oserad=R_(177)
      endif
C FDA_doz_logic.fgi(  55, 420):���� RE IN LO CH7
      L_ofirad=.false.
      if(L_ofasad) then
         if(I0_ifirad.ge.I0_ufirad)  L_ofirad=.true.
         I0_ifirad=I_(109)+1
      else
         I0_ifirad=0
      endif
C FDA_doz_logic.fgi(  48, 427):�������� ������ �� N �����
      if(L_ofirad) then
         R_userad=R_(178)
      else
         R_userad=R_(179)
      endif
C FDA_doz_logic.fgi(  55, 432):���� RE IN LO CH7
      if(L_akasad.and..not.L0_ikirad) then
         R0_akirad=R0_ekirad
      else
         R0_akirad=max(R_(189)-deltat,0.0)
      endif
      L_(629)=R0_akirad.gt.0.0
      L0_ikirad=L_akasad
C FDA_doz_logic.fgi(  46, 571):������������  �� T
      if(L_ekasad.and..not.L0_alirad) then
         R0_okirad=R0_ukirad
      else
         R0_okirad=max(R_(190)-deltat,0.0)
      endif
      L_(630)=R0_okirad.gt.0.0
      L0_alirad=L_ekasad
C FDA_doz_logic.fgi(  46, 577):������������  �� T
      L0_oterad=(L_(630).or.L0_oterad).and..not.(L_(629))
      L_(626)=.not.L0_oterad
C FDA_doz_logic.fgi(  55, 575):RS �������
      if(L_ikasad.and..not.L0_olirad) then
         R0_elirad=R0_ilirad
      else
         R0_elirad=max(R_(191)-deltat,0.0)
      endif
      L_(631)=R0_elirad.gt.0.0
      L0_olirad=L_ikasad
C FDA_doz_logic.fgi(  46, 599):������������  �� T
      if(L_okasad.and..not.L0_emirad) then
         R0_ulirad=R0_amirad
      else
         R0_ulirad=max(R_(192)-deltat,0.0)
      endif
      L_(632)=R0_ulirad.gt.0.0
      L0_emirad=L_okasad
C FDA_doz_logic.fgi(  46, 605):������������  �� T
      L0_uterad=(L_(632).or.L0_uterad).and..not.(L_(631))
      L_(627)=.not.L0_uterad
C FDA_doz_logic.fgi(  55, 603):RS �������
      if(L_ukasad.and..not.L0_umirad) then
         R0_imirad=R0_omirad
      else
         R0_imirad=max(R_(193)-deltat,0.0)
      endif
      L_(633)=R0_imirad.gt.0.0
      L0_umirad=L_ukasad
C FDA_doz_logic.fgi(  46, 628):������������  �� T
      if(L_alasad.and..not.L0_ipirad) then
         R0_apirad=R0_epirad
      else
         R0_apirad=max(R_(194)-deltat,0.0)
      endif
      L_(634)=R0_apirad.gt.0.0
      L0_ipirad=L_alasad
C FDA_doz_logic.fgi(  46, 634):������������  �� T
      L0_averad=(L_(634).or.L0_averad).and..not.(L_(633))
      L_(628)=.not.L0_averad
C FDA_doz_logic.fgi(  55, 632):RS �������
      R_(195) = 330
C FDA_doz_logic.fgi(  37, 657):��������� (RE4) (�������)
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ivorad,REAL(R_efurad
     &,4),
     & REAL(R_uturad,4),REAL(R_avurad,4),REAL(R_irurad,4)
     &,R_uvorad,REAL(R_erurad,4),
     & L_(640),L_(639),R_ivurad,R_ovurad,R_oxurad,I_asurad
     &,R_ovorad,
     & R_ixurad,R_ubasad,I_esurad,I_aturad,R_adurad,
     & REAL(R_idurad,4),R_exorad,REAL(R_oxorad,4),L_itorad
     &,
     & L_etorad,I_ikurad,I_ekurad,I_ururad,I_isurad,C20_ifurad
     &,C20_ofurad,C8_ufurad,
     & L_otorad,R_uxorad,REAL(R_eburad,4),L_oburad,
     & R_iburad,REAL(R_uburad,4),I_usurad,L_aburad,
     & L_(638),L_akurad,L_utorad,I_osurad,I_orurad,L_odurad
     &,L_iturad,L_uvurad,
     & L_edurad,L_ixorad,L_udurad,L_afurad,L_evurad,L_ukurad
     &,
     & L_imurad,L_omurad,L_amurad,L_emurad,L_elurad,L_ilurad
     &,L_umurad,L_apurad,L_epurad,L_ipurad,
     & L_opurad,L_alurad,REAL(R8_axorad,8),REAL(1.0,4),R8_okurad
     &,L_oturad,I_eturad,
     & L_evorad,L_avorad,L_axurad,L_exurad,L_olurad,L_upurad
     &,L_arurad,L_ulurad)
      !}
C FDA_doz_logic.fgi(  23, 749):���������� ��������� ������� ����������� ���������,20FDA21AE704M3
C label 1574  try1574=try1574-1
      R_elasad=R_uvorad
C FDA_doz_logic.fgi( 176, 718):������,20FDA21AE704VX01
      R_(185) = R_e + (-R_elasad)
C FDA_doz_logic.fgi(  49, 644):��������
      if(L0_averad) then
         R_(163)=R_(185)
      else
         R_(163)=R_(182)
      endif
C FDA_doz_logic.fgi(  59, 644):���� RE IN LO CH7
      L_etorad=R_(163).gt.R0_ularad
C FDA_doz_logic.fgi(  73, 650):���������� >
      L_itorad=R_(163).lt.R0_olarad
C FDA_doz_logic.fgi(  73, 638):���������� <
      L_akurad =.NOT.(L_etorad.OR.L_itorad)
C FDA_doz_logic.fgi(  79, 644):���
      R_(188) = R_ubasad + R_obasad
C FDA_doz_logic.fgi(  48, 712):��������
      R_iterad = R_adasad * R_(188)
C FDA_doz_logic.fgi(  55, 713):����������,1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_usiso,REAL(R_eboso
     &,4),
     & REAL(R_oroso,4),REAL(R_uroso,4),REAL(R_emoso,4),R_exato
     &,REAL(R_amoso,4),
     & L_(393),L_(392),R_esoso,R_isoso,R_itoso,I_umoso,R_atiso
     &,
     & R_etoso,R_axato,I_aposo,I_uposo,R_axiso,
     & REAL(R_ixiso,4),R_etiso,REAL(R_otiso,4),L_ivato,
     & L_evato,I_edoso,I_adoso,I_omoso,I_eposo,C20_iboso,C20_oboso
     &,C8_uboso,
     & L_ovato,R_utiso,REAL(R_eviso,4),L_oviso,
     & R_iviso,REAL(R_uviso,4),I_oposo,L_aviso,
     & L_(391),L_ixato,L_uvato,I_iposo,I_imoso,L_oxiso,L_eroso
     &,L_ososo,
     & L_exiso,L_itiso,L_uxiso,L_aboso,L_asoso,L_odoso,
     & L_ekoso,L_ikoso,L_ufoso,L_akoso,L_afoso,L_efoso,L_okoso
     &,L_ukoso,L_aloso,L_eloso,
     & L_iloso,L_udoso,REAL(R8_axorad,8),REAL(1.0,4),R8_idoso
     &,L_iroso,I_aroso,
     & L_osiso,L_isiso,L_usoso,L_atoso,L_ifoso,L_oloso,L_uloso
     &,L_ofoso)
      !}
C FDA_doz_logic.fgi( 468, 210):���������� ��������� ������� ����������� ���������,20FDA20KANT01_C3
C label 1613  try1613=try1613-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_aburo,4),L_iketo,L_oketo
     &,R8_avoro,C30_axoro,
     & L_adepad,L_afepad,L_upepad,I_efuro,I_ifuro,R_ixoro
     &,R_exoro,
     & R_asoro,REAL(R_isoro,4),R_etoro,
     & REAL(R_otoro,4),R_osoro,REAL(R_atoro,4),I_iduro,
     & I_ofuro,I_afuro,I_uduro,L_utoro,L_ufuro,L_ukuro,L_itoro
     &,
     & L_usoro,L_ivoro,L_evoro,L_ekuro,L_esoro,L_uvoro,
     & L_iluro,L_akuro,L_oxoro,L_uxoro,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_eketo,L_aketo,L_aluro,R_upupu,REAL(R_ovoro,4),L_eluro
     &,L_oluro,
     & L_eburo,L_iburo,L_oburo,L_aduro,L_eduro,L_uburo)
      !}

      if(L_eduro.or.L_aduro.or.L_uburo.or.L_oburo.or.L_iburo.or.L_eburo
     &) then      
                  I_oduro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oduro = z'40000000'
      endif
C FDA_doz_logic.fgi( 563, 209):���� ���������� �������� ��������,20FDA20KANT01_C1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_orepo,REAL(R_ixepo
     &,4),
     & REAL(R_aripo,4),REAL(R_eripo,4),REAL(R_olipo,4),R_esepo
     &,REAL(R_ilipo,4),
     & L_(381),L_(380),R_oripo,R_uripo,R_usipo,I_emipo,R_asepo
     &,
     & R_osipo,R_urepo,I_imipo,I_epipo,R_evepo,
     & REAL(R_ovepo,4),R_isepo,REAL(R_usepo,4),L_efeto,
     & L_afeto,I_obipo,I_ibipo,I_amipo,I_omipo,C20_oxepo,C20_uxepo
     &,C8_abipo,
     & L_ipupu,R_atepo,REAL(R_itepo,4),L_utepo,
     & R_otepo,REAL(R_avepo,4),I_apipo,L_etepo,
     & L_(379),L_ebipo,L_ifeto,I_umipo,I_ulipo,L_uvepo,L_opipo
     &,L_asipo,
     & L_ivepo,L_osepo,L_axepo,L_exepo,L_iripo,L_adipo,
     & L_ofipo,L_ufipo,L_efipo,L_ifipo,L_idipo,L_odipo,L_akipo
     &,L_ekipo,L_ikipo,L_okipo,
     & L_ukipo,L_edipo,REAL(R8_axorad,8),REAL(1.0,4),R8_ubipo
     &,L_upipo,I_ipipo,
     & L_irepo,L_erepo,L_esipo,L_isipo,L_udipo,L_alipo,L_elipo
     &,L_afipo)
      !}
C FDA_doz_logic.fgi( 598, 210):���������� ��������� ������� ����������� ���������,20FDA20KANT01_C10
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_epiro,4),L_ideto,L_epupu
     &,R8_eliro,C30_emiro,
     & L_adepad,L_afepad,L_upepad,I_osiro,I_usiro,R_omiro
     &,R_imiro,
     & R_efiro,REAL(R_ofiro,4),R_ikiro,
     & REAL(R_ukiro,4),R_ufiro,REAL(R_ekiro,4),I_uriro,
     & I_atiro,I_isiro,I_esiro,L_aliro,L_etiro,L_eviro,L_okiro
     &,
     & L_akiro,L_oliro,L_iliro,L_otiro,L_ifiro,L_amiro,
     & L_uviro,L_itiro,L_umiro,L_apiro,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_edeto,L_adeto,L_iviro,R_oriro,REAL(R_uliro,4),L_oviro
     &,L_axiro,
     & L_ipiro,L_opiro,L_upiro,L_eriro,L_iriro,L_ariro)
      !}

      if(L_iriro.or.L_eriro.or.L_ariro.or.L_upiro.or.L_opiro.or.L_ipiro
     &) then      
                  I_asiro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_asiro = z'40000000'
      endif
C FDA_doz_logic.fgi( 615, 209):���� ���������� �������� ��������,20FDA20KANT01_C11
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ekoro,4),L_ibeto,L_apupu
     &,R8_edoro,C30_eforo,
     & L_adepad,L_afepad,L_upepad,I_imoro,I_omoro,R_oforo
     &,R_iforo,
     & R_exiro,REAL(R_oxiro,4),R_iboro,
     & REAL(R_uboro,4),R_uxiro,REAL(R_eboro,4),I_oloro,
     & I_umoro,I_emoro,I_amoro,L_adoro,L_aporo,L_aroro,L_oboro
     &,
     & L_aboro,L_odoro,L_idoro,L_iporo,L_ixiro,L_aforo,
     & L_ororo,L_eporo,L_uforo,L_akoro,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_abeto,L_uxato,L_eroro,R_ebeto,REAL(R_udoro,4),L_iroro
     &,L_uroro,
     & L_ikoro,L_okoro,L_ukoro,L_eloro,L_iloro,L_aloro)
      !}

      if(L_iloro.or.L_eloro.or.L_aloro.or.L_ukoro.or.L_okoro.or.L_ikoro
     &) then      
                  I_uloro = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uloro = z'40000000'
      endif
C FDA_doz_logic.fgi( 580, 209):���� ���������� �������� ��������,20FDA20KANT01_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_oreso,REAL(R_axeso
     &,4),
     & REAL(R_ipiso,4),REAL(R_opiso,4),REAL(R_aliso,4),R_itato
     &,REAL(R_ukiso,4),
     & L_(390),L_(389),R_ariso,R_eriso,R_esiso,I_oliso,R_ureso
     &,
     & R_asiso,R_etato,I_uliso,I_omiso,R_uteso,
     & REAL(R_eveso,4),R_aseso,REAL(R_iseso,4),L_osato,
     & L_isato,I_abiso,I_uxeso,I_iliso,I_amiso,C20_exeso,C20_ixeso
     &,C8_oxeso,
     & L_usato,R_oseso,REAL(R_ateso,4),L_iteso,
     & R_eteso,REAL(R_oteso,4),I_imiso,L_useso,
     & L_(388),L_otato,L_atato,I_emiso,I_eliso,L_iveso,L_apiso
     &,L_iriso,
     & L_aveso,L_eseso,L_oveso,L_uveso,L_upiso,L_ibiso,
     & L_afiso,L_efiso,L_odiso,L_udiso,L_ubiso,L_adiso,L_ifiso
     &,L_ofiso,L_ufiso,L_akiso,
     & L_ekiso,L_obiso,REAL(R8_axorad,8),REAL(1.0,4),R8_ebiso
     &,L_episo,I_umiso,
     & L_ireso,L_ereso,L_oriso,L_uriso,L_ediso,L_ikiso,L_okiso
     &,L_idiso)
      !}
C FDA_doz_logic.fgi( 488, 210):���������� ��������� ������� ����������� ���������,20FDA20KANT01_C4
      R_orato=R_itato
C FDA_doz_logic.fgi( 507, 169):������,20FDA20KANT01_C5_VX01
      L_erato=L_atato
C FDA_doz_logic.fgi( 507, 165):������,20FDA20KANT01_C5_XH53
      L_arato=L_usato
C FDA_doz_logic.fgi( 507, 161):������,20FDA20KANT01_C5_XH54
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ipaso,REAL(R_utaso
     &,4),
     & REAL(R_emeso,4),REAL(R_imeso,4),REAL(R_ufeso,4),R_umato
     &,REAL(R_ofeso,4),
     & L_(387),L_(386),R_umeso,R_apeso,R_areso,I_ikeso,R_opaso
     &,
     & R_upeso,R_omato,I_okeso,I_ileso,R_osaso,
     & REAL(R_ataso,4),R_upaso,REAL(R_eraso,4),L_amato,
     & L_ulato,I_uvaso,I_ovaso,I_ekeso,I_ukeso,C20_avaso,C20_evaso
     &,C8_ivaso,
     & L_emato,R_iraso,REAL(R_uraso,4),L_esaso,
     & R_asaso,REAL(R_isaso,4),I_eleso,L_oraso,
     & L_(385),L_apato,L_imato,I_aleso,I_akeso,L_etaso,L_uleso
     &,L_epeso,
     & L_usaso,L_araso,L_itaso,L_otaso,L_omeso,L_exaso,
     & L_ubeso,L_adeso,L_ibeso,L_obeso,L_oxaso,L_uxaso,L_edeso
     &,L_ideso,L_odeso,L_udeso,
     & L_afeso,L_ixaso,REAL(R8_axorad,8),REAL(1.0,4),R8_axaso
     &,L_ameso,I_oleso,
     & L_epaso,L_apaso,L_ipeso,L_opeso,L_abeso,L_efeso,L_ifeso
     &,L_ebeso)
      !}
C FDA_doz_logic.fgi( 525, 210):���������� ��������� ������� ����������� ���������,20FDA20KANT01_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_emuro,REAL(R_osuro
     &,4),
     & REAL(R_alaso,4),REAL(R_elaso,4),REAL(R_odaso,4),R_alato
     &,REAL(R_idaso,4),
     & L_(384),L_(383),R_olaso,R_ulaso,R_umaso,I_efaso,R_imuro
     &,
     & R_omaso,R_ukato,I_ifaso,I_ekaso,R_iruro,
     & REAL(R_ururo,4),R_omuro,REAL(R_apuro,4),L_ekato,
     & L_akato,I_oturo,I_ituro,I_afaso,I_ofaso,C20_usuro,C20_aturo
     &,C8_eturo,
     & L_ikato,R_epuro,REAL(R_opuro,4),L_aruro,
     & R_upuro,REAL(R_eruro,4),I_akaso,L_ipuro,
     & L_(382),L_elato,L_okato,I_ufaso,I_udaso,L_asuro,L_okaso
     &,L_amaso,
     & L_oruro,L_umuro,L_esuro,L_isuro,L_ilaso,L_avuro,
     & L_oxuro,L_uxuro,L_exuro,L_ixuro,L_ivuro,L_ovuro,L_abaso
     &,L_ebaso,L_ibaso,L_obaso,
     & L_ubaso,L_evuro,REAL(R8_axorad,8),REAL(1.0,4),R8_uturo
     &,L_ukaso,I_ikaso,
     & L_amuro,L_uluro,L_emaso,L_imaso,L_uvuro,L_adaso,L_edaso
     &,L_axuro)
      !}
C FDA_doz_logic.fgi( 544, 210):���������� ��������� ������� ����������� ���������,20FDA20KANT01_C7
      !{
      Call TRANSPORTER_DOZ(deltat,R_imeto,R_uketo,L_ireto
     &,
     & R_ometo,R_aleto,L_oreto,R_umeto,
     & R_eleto,L_ureto,R_apeto,R_ileto,
     & L_aseto,L_elato,L_ifato,L_apato,L_areto,
     & L_upuso,R_ibato,REAL(0,4),REAL(1000,4),L_aruso,L_ubato
     &,
     & R_epeto,R_oleto,L_eseto,R_ipeto,
     & R_uleto,L_iseto,R_opeto,R_ameto,
     & L_oseto,R_upeto,R_emeto,L_useto,
     & L_urato,L_otato,L_ixato,L_ereto,L_abato,R_obato,
     & REAL(0,4),REAL(2000,4),L_ebato,L_ekato,L_amato,L_odato
     &,L_ufato,
     & L_olato,L_edato,L_iruso,L_ulato,L_akato,L_idato,L_ofato
     &,L_ilato,
     & L_adato,L_upato,L_osato,L_ivato,L_ipato,L_esato,L_avato
     &,
     & L_oruso,L_omuso,L_umuso,L_axuso,L_uvuso,L_odeto,L_adeto
     &,L_edeto,L_epupu,
     & L_ubeto,L_ideto,R_umupu,R_opuso,R_omupu,REAL(R_ufeto
     &,4),
     & REAL(R_efato,4),REAL(R_alato,4),REAL(R_umato,4),REAL
     &(R_orato,4),
     & REAL(R_itato,4),REAL(R_exato,4),L_esuso,L_ixuso,L_isuso
     &,L_osuso,L_ususo,L_utuso,L_ituso,
     & L_evuso,L_oxuso,L_ovuso,L_exuso,L_asuso,L_atuso,L_etuso
     &,L_uxuso,L_otuso,
     & L_avuso,L_ivuso,L_ikato,L_emato,L_usato,L_ovato,L_arato
     &,
     & L_udato,L_okato,L_imato,L_erato,L_atato,L_uvato,L_uxato
     &,
     & L_abeto,L_opato,L_isato,L_evato,L_aketo,L_eketo,L_afeto
     &,
     & L_efeto,L_udeto,L_epato,L_asato,L_uruso,L_utato,L_apupu
     &,L_oxato,
     & L_ibeto,L_oketo,L_obeto,L_iketo,L_ifeto,L_ipupu)
      !}
C FDA_doz_logic.fgi( 468, 182):���������� ������������ ��� ������� ����������� ���������,20FDA20KANT01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utoso,L_ivoso,R_evoso
     &,
     & REAL(R_ovoso,4),L_etuso,L_otoso,I_avoso)
      !}
C FDA_doz_logic.fgi( 538, 172):���������� ������� ���������,20FDA20KANT01_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_eluso,L_uluso,R_oluso
     &,
     & REAL(R_amuso,4),L_otuso,L_aluso,I_iluso)
      !}
C FDA_doz_logic.fgi( 538, 186):���������� ������� ���������,20FDA20KANT01_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixoso,L_abuso,R_uxoso
     &,
     & REAL(R_ebuso,4),L_avuso,L_exoso,I_oxoso)
      !}
C FDA_doz_logic.fgi( 562, 186):���������� ������� ���������,20FDA20KANT01_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofuso,L_ekuso,R_akuso
     &,
     & REAL(R_ikuso,4),L_ivuso,L_ifuso,I_ufuso)
      !}
C FDA_doz_logic.fgi( 588, 184):���������� ������� ���������,20FDA20KANT01_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aduso,L_oduso,R_iduso
     &,
     & REAL(R_uduso,4),L_oxuso,L_ubuso,I_eduso)
      !}
C FDA_doz_logic.fgi( 588, 170):���������� ������� ���������,20FDA20KANT01_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_odilad,REAL(R_imilad
     &,4),
     & REAL(R_adolad,4),REAL(R_edolad,4),REAL(R_ovilad,4)
     &,R_efilad,REAL(R_ivilad,4),
     & L_(537),L_(536),R_odolad,R_udolad,R_ufolad,I_exilad
     &,R_afilad,
     & R_ofolad,R_udilad,I_ixilad,I_ebolad,R_elilad,
     & REAL(R_olilad,4),R_ifilad,REAL(R_ufilad,4),L_obilad
     &,
     & L_ibilad,I_opilad,I_ipilad,I_axilad,I_oxilad,C20_omilad
     &,C20_umilad,C8_apilad,
     & L_ubilad,R_akilad,REAL(R_ikilad,4),L_ukilad,
     & R_okilad,REAL(R_alilad,4),I_abolad,L_ekilad,
     & L_(535),L_epilad,L_adilad,I_uxilad,I_uvilad,L_ulilad
     &,L_obolad,L_afolad,
     & L_ililad,L_ofilad,L_amilad,L_emilad,L_idolad,L_arilad
     &,
     & L_osilad,L_usilad,L_esilad,L_isilad,L_irilad,L_orilad
     &,L_atilad,L_etilad,L_itilad,L_otilad,
     & L_utilad,L_erilad,REAL(R8_axorad,8),REAL(1.0,4),R8_upilad
     &,L_ubolad,I_ibolad,
     & L_idilad,L_edilad,L_efolad,L_ifolad,L_urilad,L_avilad
     &,L_evilad,L_asilad)
      !}
C FDA_doz_logic.fgi(  25, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN01_C3
C label 1636  try1636=try1636-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_osekad,4),L_abikad,L_ebikad
     &,R8_epekad,C30_orekad,
     & L_adepad,L_afepad,L_upepad,I_axekad,I_exekad,R_asekad
     &,R_urekad,
     & R_elekad,REAL(R_olekad,4),R_imekad,
     & REAL(R_umekad,4),R_ulekad,REAL(R_emekad,4),I_evekad
     &,
     & I_ixekad,I_uvekad,I_ovekad,L_apekad,L_oxekad,L_adikad
     &,L_omekad,
     & L_amekad,L_arekad,L_upekad,L_ibikad,L_ilekad,L_irekad
     &,
     & L_odikad,L_uxekad,L_esekad,L_isekad,REAL(R8_axorad
     &,8),REAL(1.0,4),R8_udepad,
     & L_opekad,L_ipekad,L_edikad,R_avekad,REAL(R_erekad,4
     &),L_idikad,L_udikad,
     & L_usekad,L_atekad,L_etekad,L_otekad,L_utekad,L_itekad
     &)
      !}

      if(L_utekad.or.L_otekad.or.L_itekad.or.L_etekad.or.L_atekad.or.L_u
     &sekad) then      
                  I_ivekad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_ivekad = z'40000000'
      endif
C FDA_doz_logic.fgi( 128, 292):���� ���������� �������� ��������,20FDA20TRAN01_C1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uvakad,4),L_efekad,L_ifekad
     &,R8_isakad,C30_utakad,
     & L_adepad,L_afepad,L_upepad,I_edekad,I_idekad,R_evakad
     &,R_avakad,
     & R_ipakad,REAL(R_upakad,4),R_orakad,
     & REAL(R_asakad,4),R_arakad,REAL(R_irakad,4),I_ibekad
     &,
     & I_odekad,I_adekad,I_ubekad,L_esakad,L_udekad,L_ekekad
     &,L_urakad,
     & L_erakad,L_etakad,L_atakad,L_ofekad,L_opakad,L_otakad
     &,
     & L_ukekad,L_afekad,L_ivakad,L_ovakad,REAL(R8_axorad
     &,8),REAL(1.0,4),R8_udepad,
     & L_usakad,L_osakad,L_ikekad,R_ebekad,REAL(R_itakad,4
     &),L_okekad,L_alekad,
     & L_axakad,L_exakad,L_ixakad,L_uxakad,L_abekad,L_oxakad
     &)
      !}

      if(L_abekad.or.L_uxakad.or.L_oxakad.or.L_ixakad.or.L_exakad.or.L_a
     &xakad) then      
                  I_obekad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_obekad = z'40000000'
      endif
C FDA_doz_logic.fgi( 148, 292):���� ���������� �������� ��������,20FDA20TRAN01_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_axalad,REAL(R_ufelad
     &,4),
     & REAL(R_ivelad,4),REAL(R_ovelad,4),REAL(R_aselad,4)
     &,R_oxalad,REAL(R_urelad,4),
     & L_(534),L_(533),R_axelad,R_exelad,R_ebilad,I_oselad
     &,R_ixalad,
     & R_abilad,R_exalad,I_uselad,I_otelad,R_odelad,
     & REAL(R_afelad,4),R_uxalad,REAL(R_ebelad,4),L_avalad
     &,
     & L_utalad,I_alelad,I_ukelad,I_iselad,I_atelad,C20_akelad
     &,C20_ekelad,C8_ikelad,
     & L_evalad,R_ibelad,REAL(R_ubelad,4),L_edelad,
     & R_adelad,REAL(R_idelad,4),I_itelad,L_obelad,
     & L_(532),L_okelad,L_ivalad,I_etelad,I_eselad,L_efelad
     &,L_avelad,L_ixelad,
     & L_udelad,L_abelad,L_ifelad,L_ofelad,L_uvelad,L_ilelad
     &,
     & L_apelad,L_epelad,L_omelad,L_umelad,L_ulelad,L_amelad
     &,L_ipelad,L_opelad,L_upelad,L_arelad,
     & L_erelad,L_olelad,REAL(R8_axorad,8),REAL(1.0,4),R8_elelad
     &,L_evelad,I_utelad,
     & L_uvalad,L_ovalad,L_oxelad,L_uxelad,L_emelad,L_irelad
     &,L_orelad,L_imelad)
      !}
C FDA_doz_logic.fgi(  48, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN01_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_isukad,REAL(R_ebalad
     &,4),
     & REAL(R_uralad,4),REAL(R_asalad,4),REAL(R_imalad,4)
     &,R_atukad,REAL(R_emalad,4),
     & L_(531),L_(530),R_isalad,R_osalad,R_otalad,I_apalad
     &,R_usukad,
     & R_italad,R_osukad,I_epalad,I_aralad,R_axukad,
     & REAL(R_ixukad,4),R_etukad,REAL(R_otukad,4),L_irukad
     &,
     & L_erukad,I_idalad,I_edalad,I_umalad,I_ipalad,C20_ibalad
     &,C20_obalad,C8_ubalad,
     & L_orukad,R_utukad,REAL(R_evukad,4),L_ovukad,
     & R_ivukad,REAL(R_uvukad,4),I_upalad,L_avukad,
     & L_(529),L_adalad,L_urukad,I_opalad,I_omalad,L_oxukad
     &,L_iralad,L_usalad,
     & L_exukad,L_itukad,L_uxukad,L_abalad,L_esalad,L_udalad
     &,
     & L_ikalad,L_okalad,L_akalad,L_ekalad,L_efalad,L_ifalad
     &,L_ukalad,L_alalad,L_elalad,L_ilalad,
     & L_olalad,L_afalad,REAL(R8_axorad,8),REAL(1.0,4),R8_odalad
     &,L_oralad,I_eralad,
     & L_esukad,L_asukad,L_atalad,L_etalad,L_ofalad,L_ulalad
     &,L_amalad,L_ufalad)
      !}
C FDA_doz_logic.fgi(  66, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN01_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_umokad,REAL(R_otokad
     &,4),
     & REAL(R_emukad,4),REAL(R_imukad,4),REAL(R_ufukad,4)
     &,R_ipokad,REAL(R_ofukad,4),
     & L_(528),L_(527),R_umukad,R_apukad,R_arukad,I_ikukad
     &,R_epokad,
     & R_upukad,R_apokad,I_okukad,I_ilukad,R_isokad,
     & REAL(R_usokad,4),R_opokad,REAL(R_arokad,4),L_ulokad
     &,
     & L_olokad,I_uvokad,I_ovokad,I_ekukad,I_ukukad,C20_utokad
     &,C20_avokad,C8_evokad,
     & L_amokad,R_erokad,REAL(R_orokad,4),L_asokad,
     & R_urokad,REAL(R_esokad,4),I_elukad,L_irokad,
     & L_(526),L_ivokad,L_emokad,I_alukad,I_akukad,L_atokad
     &,L_ulukad,L_epukad,
     & L_osokad,L_upokad,L_etokad,L_itokad,L_omukad,L_exokad
     &,
     & L_ubukad,L_adukad,L_ibukad,L_obukad,L_oxokad,L_uxokad
     &,L_edukad,L_idukad,L_odukad,L_udukad,
     & L_afukad,L_ixokad,REAL(R8_axorad,8),REAL(1.0,4),R8_axokad
     &,L_amukad,I_olukad,
     & L_omokad,L_imokad,L_ipukad,L_opukad,L_abukad,L_efukad
     &,L_ifukad,L_ebukad)
      !}
C FDA_doz_logic.fgi(  85, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN01_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ekikad,REAL(R_arikad
     &,4),
     & REAL(R_ofokad,4),REAL(R_ufokad,4),REAL(R_ebokad,4)
     &,R_ukikad,REAL(R_abokad,4),
     & L_(525),L_(524),R_ekokad,R_ikokad,R_ilokad,I_ubokad
     &,R_okikad,
     & R_elokad,R_ikikad,I_adokad,I_udokad,R_umikad,
     & REAL(R_epikad,4),R_alikad,REAL(R_ilikad,4),L_efikad
     &,
     & L_afikad,I_esikad,I_asikad,I_obokad,I_edokad,C20_erikad
     &,C20_irikad,C8_orikad,
     & L_ifikad,R_olikad,REAL(R_amikad,4),L_imikad,
     & R_emikad,REAL(R_omikad,4),I_odokad,L_ulikad,
     & L_(523),L_urikad,L_ofikad,I_idokad,I_ibokad,L_ipikad
     &,L_efokad,L_okokad,
     & L_apikad,L_elikad,L_opikad,L_upikad,L_akokad,L_osikad
     &,
     & L_evikad,L_ivikad,L_utikad,L_avikad,L_atikad,L_etikad
     &,L_ovikad,L_uvikad,L_axikad,L_exikad,
     & L_ixikad,L_usikad,REAL(R8_axorad,8),REAL(1.0,4),R8_isikad
     &,L_ifokad,I_afokad,
     & L_akikad,L_ufikad,L_ukokad,L_alokad,L_itikad,L_oxikad
     &,L_uxikad,L_otikad)
      !}
C FDA_doz_logic.fgi( 104, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN01_C7
      !{
      Call TRANSPORTER_DOZ(deltat,R_ufakad,R_edakad,L_ulakad
     &,
     & R_akakad,R_idakad,L_amakad,R_ekakad,
     & R_odakad,L_emakad,R_ikakad,R_udakad,
     & L_imakad,L_urikad,L_asufad,L_ivokad,L_ilakad,
     & L_ibufad,R_apufad,REAL(0,4),REAL(1000,4),L_obufad,L_ipufad
     &,
     & R_okakad,R_afakad,L_omakad,R_ukakad,
     & R_efakad,L_umakad,R_alakad,R_ifakad,
     & L_apakad,R_elakad,R_ofakad,L_epakad,
     & L_adalad,L_okelad,L_epilad,L_olakad,L_omufad,R_epufad
     &,
     & REAL(0,4),REAL(2000,4),L_umufad,L_efikad,L_ulokad,L_erufad
     &,L_isufad,
     & L_usufad,L_upufad,L_adufad,L_olokad,L_afikad,L_arufad
     &,L_esufad,L_osufad,
     & L_opufad,L_irukad,L_avalad,L_obilad,L_etufad,L_otufad
     &,L_avufad,
     & L_edufad,L_uvofad,L_axofad,L_olufad,L_ilufad,L_oxufad
     &,L_uvufad,L_axufad,L_ixufad,
     & L_ovufad,L_exufad,R_uxofad,R_ebufad,R_ixofad,REAL(R_adakad
     &,4),
     & REAL(R_urufad,4),REAL(R_ukikad,4),REAL(R_ipokad,4)
     &,REAL(R_atukad,4),
     & REAL(R_oxalad,4),REAL(R_efilad,4),L_udufad,L_amufad
     &,L_afufad,L_efufad,L_ifufad,L_ikufad,L_akufad,
     & L_ukufad,L_emufad,L_elufad,L_ulufad,L_odufad,L_ofufad
     &,L_ufufad,L_imufad,L_ekufad,
     & L_okufad,L_alufad,L_ifikad,L_amokad,L_evalad,L_ubilad
     &,L_orukad,
     & L_irufad,L_ofikad,L_emokad,L_urukad,L_ivalad,L_adilad
     &,L_osakad,
     & L_usakad,L_erukad,L_utalad,L_ibilad,L_ipekad,L_opekad
     &,L_abakad,
     & L_ebakad,L_uxufad,L_atufad,L_itufad,L_idufad,L_utufad
     &,L_ifekad,L_evufad,
     & L_efekad,L_ebikad,L_ivufad,L_abikad,L_obakad,L_ibakad
     &)
      !}
C FDA_doz_logic.fgi(  25, 263):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovafad,L_exafad,R_axafad
     &,
     & REAL(R_ixafad,4),L_ifufad,L_ivafad,I_uvafad)
      !}
C FDA_doz_logic.fgi(  76, 268):���������� ������� ���������,20FDA20TRAN01_L6
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atafad,L_otafad,R_itafad
     &,
     & REAL(R_utafad,4),L_ufufad,L_usafad,I_etafad)
      !}
C FDA_doz_logic.fgi(  94, 254):���������� ������� ���������,20FDA20TRAN01_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amefad,L_omefad,R_imefad
     &,
     & REAL(R_umefad,4),L_ekufad,L_ulefad,I_emefad)
      !}
C FDA_doz_logic.fgi(  94, 268):���������� ������� ���������,20FDA20TRAN01_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebefad,L_ubefad,R_obefad
     &,
     & REAL(R_adefad,4),L_okufad,L_abefad,I_ibefad)
      !}
C FDA_doz_logic.fgi( 118, 268):���������� ������� ���������,20FDA20TRAN01_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikefad,L_alefad,R_ukefad
     &,
     & REAL(R_elefad,4),L_alufad,L_ekefad,I_okefad)
      !}
C FDA_doz_logic.fgi( 144, 266):���������� ������� ���������,20FDA20TRAN01_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_udefad,L_ifefad,R_efefad
     &,
     & REAL(R_ofefad,4),L_emufad,L_odefad,I_afefad)
      !}
C FDA_doz_logic.fgi( 144, 252):���������� ������� ���������,20FDA20TRAN01_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ivodad,REAL(R_ududad
     &,4),
     & REAL(R_etudad,4),REAL(R_itudad,4),REAL(R_upudad,4)
     &,R_ibofad,REAL(R_opudad,4),
     & L_(522),L_(521),R_utudad,R_avudad,R_axudad,I_irudad
     &,R_ovodad,
     & R_uvudad,R_ebofad,I_orudad,I_isudad,R_obudad,
     & REAL(R_adudad,4),R_uvodad,REAL(R_exodad,4),L_oxifad
     &,
     & L_ixifad,I_ufudad,I_ofudad,I_erudad,I_urudad,C20_afudad
     &,C20_efudad,C8_ifudad,
     & L_uxifad,R_ixodad,REAL(R_uxodad,4),L_ebudad,
     & R_abudad,REAL(R_ibudad,4),I_esudad,L_oxodad,
     & L_(520),L_obofad,L_abofad,I_asudad,I_arudad,L_edudad
     &,L_usudad,L_evudad,
     & L_ubudad,L_axodad,L_idudad,L_odudad,L_otudad,L_ekudad
     &,
     & L_uludad,L_amudad,L_iludad,L_oludad,L_okudad,L_ukudad
     &,L_emudad,L_imudad,L_omudad,L_umudad,
     & L_apudad,L_ikudad,REAL(R8_axorad,8),REAL(1.0,4),R8_akudad
     &,L_atudad,I_osudad,
     & L_evodad,L_avodad,L_ivudad,L_ovudad,L_aludad,L_epudad
     &,L_ipudad,L_eludad)
      !}
C FDA_doz_logic.fgi( 171, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN02_C3
C label 1650  try1650=try1650-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ebubad,4),L_emofad,L_imofad
     &,R8_evobad,C30_exobad,
     & L_adepad,L_afepad,L_upepad,I_ofubad,I_ufubad,R_oxobad
     &,R_ixobad,
     & R_esobad,REAL(R_osobad,4),R_itobad,
     & REAL(R_utobad,4),R_usobad,REAL(R_etobad,4),I_udubad
     &,
     & I_akubad,I_ifubad,I_efubad,L_avobad,L_ekubad,L_elubad
     &,L_otobad,
     & L_atobad,L_ovobad,L_ivobad,L_okubad,L_isobad,L_axobad
     &,
     & L_ulubad,L_ikubad,L_uxobad,L_abubad,REAL(R8_axorad
     &,8),REAL(1.0,4),R8_udepad,
     & L_amofad,L_ulofad,L_ilubad,R_odubad,REAL(R_uvobad,4
     &),L_olubad,L_amubad,
     & L_ibubad,L_obubad,L_ububad,L_edubad,L_idubad,L_adubad
     &)
      !}

      if(L_idubad.or.L_edubad.or.L_adubad.or.L_ububad.or.L_obubad.or.L_i
     &bubad) then      
                  I_afubad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_afubad = z'40000000'
      endif
C FDA_doz_logic.fgi( 271, 292):���� ���������� �������� ��������,20FDA20TRAN02_C1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ikobad,4),L_odofad,L_udofad
     &,R8_idobad,C30_ifobad,
     & L_adepad,L_afepad,L_upepad,I_omobad,I_umobad,R_ufobad
     &,R_ofobad,
     & R_ixibad,REAL(R_uxibad,4),R_obobad,
     & REAL(R_adobad,4),R_abobad,REAL(R_ibobad,4),I_ulobad
     &,
     & I_apobad,I_imobad,I_emobad,L_edobad,L_epobad,L_erobad
     &,L_ubobad,
     & L_ebobad,L_udobad,L_odobad,L_opobad,L_oxibad,L_efobad
     &,
     & L_urobad,L_ipobad,L_akobad,L_ekobad,REAL(R8_axorad
     &,8),REAL(1.0,4),R8_udepad,
     & L_edofad,L_adofad,L_irobad,R_idofad,REAL(R_afobad,4
     &),L_orobad,L_asobad,
     & L_okobad,L_ukobad,L_alobad,L_ilobad,L_olobad,L_elobad
     &)
      !}

      if(L_olobad.or.L_ilobad.or.L_elobad.or.L_alobad.or.L_ukobad.or.L_o
     &kobad) then      
                  I_amobad = ior(z'000000FF',z'04000000')
     &                            
                else
                   I_amobad = z'40000000'
      endif
C FDA_doz_logic.fgi( 291, 292):���� ���������� �������� ��������,20FDA20TRAN02_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_etidad,REAL(R_obodad
     &,4),
     & REAL(R_asodad,4),REAL(R_esodad,4),REAL(R_omodad,4)
     &,R_ovifad,REAL(R_imodad,4),
     & L_(519),L_(518),R_osodad,R_usodad,R_utodad,I_epodad
     &,R_itidad,
     & R_otodad,R_ivifad,I_ipodad,I_erodad,R_ixidad,
     & REAL(R_uxidad,4),R_otidad,REAL(R_avidad,4),L_utifad
     &,
     & L_otifad,I_ododad,I_idodad,I_apodad,I_opodad,C20_ubodad
     &,C20_adodad,C8_edodad,
     & L_avifad,R_evidad,REAL(R_ovidad,4),L_axidad,
     & R_uvidad,REAL(R_exidad,4),I_arodad,L_ividad,
     & L_(517),L_uvifad,L_evifad,I_upodad,I_umodad,L_abodad
     &,L_orodad,L_atodad,
     & L_oxidad,L_utidad,L_ebodad,L_ibodad,L_isodad,L_afodad
     &,
     & L_okodad,L_ukodad,L_ekodad,L_ikodad,L_ifodad,L_ofodad
     &,L_alodad,L_elodad,L_ilodad,L_olodad,
     & L_ulodad,L_efodad,REAL(R8_axorad,8),REAL(1.0,4),R8_udodad
     &,L_urodad,I_irodad,
     & L_atidad,L_usidad,L_etodad,L_itodad,L_ufodad,L_amodad
     &,L_emodad,L_akodad)
      !}
C FDA_doz_logic.fgi( 191, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN02_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_asedad,REAL(R_ixedad
     &,4),
     & REAL(R_upidad,4),REAL(R_aridad,4),REAL(R_ilidad,4)
     &,R_usifad,REAL(R_elidad,4),
     & L_(516),L_(515),R_iridad,R_oridad,R_osidad,I_amidad
     &,R_esedad,
     & R_isidad,R_osifad,I_emidad,I_apidad,R_evedad,
     & REAL(R_ovedad,4),R_isedad,REAL(R_usedad,4),L_asifad
     &,
     & L_urifad,I_ibidad,I_ebidad,I_ulidad,I_imidad,C20_oxedad
     &,C20_uxedad,C8_abidad,
     & L_esifad,R_atedad,REAL(R_itedad,4),L_utedad,
     & R_otedad,REAL(R_avedad,4),I_umidad,L_etedad,
     & L_(514),L_atifad,L_isifad,I_omidad,I_olidad,L_uvedad
     &,L_ipidad,L_uridad,
     & L_ivedad,L_osedad,L_axedad,L_exedad,L_eridad,L_ubidad
     &,
     & L_ifidad,L_ofidad,L_afidad,L_efidad,L_edidad,L_ididad
     &,L_ufidad,L_akidad,L_ekidad,L_ikidad,
     & L_okidad,L_adidad,REAL(R8_axorad,8),REAL(1.0,4),R8_obidad
     &,L_opidad,I_epidad,
     & L_uredad,L_oredad,L_asidad,L_esidad,L_odidad,L_ukidad
     &,L_alidad,L_udidad)
      !}
C FDA_doz_logic.fgi( 209, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN02_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_upadad,REAL(R_evadad
     &,4),
     & REAL(R_omedad,4),REAL(R_umedad,4),REAL(R_ekedad,4)
     &,R_arifad,REAL(R_akedad,4),
     & L_(513),L_(512),R_epedad,R_ipedad,R_iredad,I_ukedad
     &,R_aradad,
     & R_eredad,R_upifad,I_aledad,I_uledad,R_atadad,
     & REAL(R_itadad,4),R_eradad,REAL(R_oradad,4),L_epifad
     &,
     & L_apifad,I_exadad,I_axadad,I_okedad,I_eledad,C20_ivadad
     &,C20_ovadad,C8_uvadad,
     & L_ipifad,R_uradad,REAL(R_esadad,4),L_osadad,
     & R_isadad,REAL(R_usadad,4),I_oledad,L_asadad,
     & L_(511),L_erifad,L_opifad,I_iledad,I_ikedad,L_otadad
     &,L_emedad,L_opedad,
     & L_etadad,L_iradad,L_utadad,L_avadad,L_apedad,L_oxadad
     &,
     & L_ededad,L_idedad,L_ubedad,L_adedad,L_abedad,L_ebedad
     &,L_odedad,L_udedad,L_afedad,L_efedad,
     & L_ifedad,L_uxadad,REAL(R8_axorad,8),REAL(1.0,4),R8_ixadad
     &,L_imedad,I_amedad,
     & L_opadad,L_ipadad,L_upedad,L_aredad,L_ibedad,L_ofedad
     &,L_ufedad,L_obedad)
      !}
C FDA_doz_logic.fgi( 228, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN02_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_omubad,REAL(R_atubad
     &,4),
     & REAL(R_iladad,4),REAL(R_oladad,4),REAL(R_afadad,4)
     &,R_emifad,REAL(R_udadad,4),
     & L_(510),L_(509),R_amadad,R_emadad,R_epadad,I_ofadad
     &,R_umubad,
     & R_apadad,R_amifad,I_ufadad,I_okadad,R_urubad,
     & REAL(R_esubad,4),R_apubad,REAL(R_ipubad,4),L_ilifad
     &,
     & L_elifad,I_avubad,I_utubad,I_ifadad,I_akadad,C20_etubad
     &,C20_itubad,C8_otubad,
     & L_olifad,R_opubad,REAL(R_arubad,4),L_irubad,
     & R_erubad,REAL(R_orubad,4),I_ikadad,L_upubad,
     & L_(508),L_imifad,L_ulifad,I_ekadad,I_efadad,L_isubad
     &,L_aladad,L_imadad,
     & L_asubad,L_epubad,L_osubad,L_usubad,L_uladad,L_ivubad
     &,
     & L_abadad,L_ebadad,L_oxubad,L_uxubad,L_uvubad,L_axubad
     &,L_ibadad,L_obadad,L_ubadad,L_adadad,
     & L_edadad,L_ovubad,REAL(R8_axorad,8),REAL(1.0,4),R8_evubad
     &,L_eladad,I_ukadad,
     & L_imubad,L_emubad,L_omadad,L_umadad,L_exubad,L_idadad
     &,L_odadad,L_ixubad)
      !}
C FDA_doz_logic.fgi( 247, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN02_C7
      !{
      Call TRANSPORTER_DOZ(deltat,R_erofad,R_omofad,L_etofad
     &,
     & R_irofad,R_umofad,L_itofad,R_orofad,
     & R_apofad,L_otofad,R_urofad,R_epofad,
     & L_utofad,L_imifad,L_okifad,L_erifad,L_usofad,
     & L_asefad,R_odifad,REAL(0,4),REAL(1000,4),L_esefad,L_afifad
     &,
     & R_asofad,R_ipofad,L_avofad,R_esofad,
     & R_opofad,L_evofad,R_isofad,R_upofad,
     & L_ivofad,R_osofad,R_arofad,L_ovofad,
     & L_atifad,L_uvifad,L_obofad,L_atofad,L_edifad,R_udifad
     &,
     & REAL(2000,4),REAL(4000,4),L_idifad,L_ilifad,L_epifad
     &,L_ufifad,L_alifad,
     & L_umifad,L_ififad,L_osefad,L_apifad,L_elifad,L_ofifad
     &,L_ukifad,L_omifad,
     & L_efifad,L_asifad,L_utifad,L_oxifad,L_orifad,L_itifad
     &,L_exifad,
     & L_usefad,L_ipefad,L_opefad,L_ebifad,L_abifad,L_ekofad
     &,L_ifofad,L_ofofad,L_akofad,
     & L_efofad,L_ufofad,R_irefad,R_urefad,R_arefad,REAL(R_olofad
     &,4),
     & REAL(R_ikifad,4),REAL(R_emifad,4),REAL(R_arifad,4)
     &,REAL(R_usifad,4),
     & REAL(R_ovifad,4),REAL(R_ibofad,4),L_itefad,L_obifad
     &,L_otefad,L_utefad,L_avefad,L_axefad,L_ovefad,
     & L_ixefad,L_ubifad,L_uxefad,L_ibifad,L_etefad,L_evefad
     &,L_ivefad,L_adifad,L_uvefad,
     & L_exefad,L_oxefad,L_olifad,L_ipifad,L_avifad,L_uxifad
     &,L_esifad,
     & L_akifad,L_ulifad,L_opifad,L_isifad,L_evifad,L_abofad
     &,L_adofad,
     & L_edofad,L_urifad,L_otifad,L_ixifad,L_ulofad,L_amofad
     &,L_okofad,
     & L_ukofad,L_ikofad,L_irifad,L_etifad,L_atefad,L_axifad
     &,L_udofad,L_ubofad,
     & L_odofad,L_imofad,L_afofad,L_emofad,L_elofad,L_alofad
     &)
      !}
C FDA_doz_logic.fgi( 172, 263):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN02
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adafad,L_odafad,R_idafad
     &,
     & REAL(R_udafad,4),L_avefad,L_ubafad,I_edafad)
      !}
C FDA_doz_logic.fgi( 224, 268):���������� ������� ���������,20FDA20TRAN02_L6
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixudad,L_abafad,R_uxudad
     &,
     & REAL(R_ebafad,4),L_ivefad,L_exudad,I_oxudad)
      !}
C FDA_doz_logic.fgi( 240, 256):���������� ������� ���������,20FDA20TRAN02_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irafad,L_asafad,R_urafad
     &,
     & REAL(R_esafad,4),L_uvefad,L_erafad,I_orafad)
      !}
C FDA_doz_logic.fgi( 240, 268):���������� ������� ���������,20FDA20TRAN02_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofafad,L_ekafad,R_akafad
     &,
     & REAL(R_ikafad,4),L_exefad,L_ifafad,I_ufafad)
      !}
C FDA_doz_logic.fgi( 266, 268):���������� ������� ���������,20FDA20TRAN02_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_umafad,L_ipafad,R_epafad
     &,
     & REAL(R_opafad,4),L_oxefad,L_omafad,I_apafad)
      !}
C FDA_doz_logic.fgi( 290, 268):���������� ������� ���������,20FDA20TRAN02_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_elafad,L_ulafad,R_olafad
     &,
     & REAL(R_amafad,4),L_ubifad,L_alafad,I_ilafad)
      !}
C FDA_doz_logic.fgi( 290, 252):���������� ������� ���������,20FDA20TRAN02_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_upotu,REAL(R_evotu
     &,4),
     & REAL(R_omutu,4),REAL(R_umutu,4),REAL(R_ekutu,4),R_exevu
     &,REAL(R_akutu,4),
     & L_(492),L_(491),R_eputu,R_iputu,R_irutu,I_ukutu,R_arotu
     &,
     & R_erutu,R_axevu,I_alutu,I_ulutu,R_atotu,
     & REAL(R_itotu,4),R_erotu,REAL(R_orotu,4),L_ivevu,
     & L_evevu,I_exotu,I_axotu,I_okutu,I_elutu,C20_ivotu,C20_ovotu
     &,C8_uvotu,
     & L_ovevu,R_urotu,REAL(R_esotu,4),L_osotu,
     & R_isotu,REAL(R_usotu,4),I_olutu,L_asotu,
     & L_(490),L_ixevu,L_uvevu,I_ilutu,I_ikutu,L_ototu,L_emutu
     &,L_oputu,
     & L_etotu,L_irotu,L_utotu,L_avotu,L_aputu,L_oxotu,
     & L_edutu,L_idutu,L_ubutu,L_adutu,L_abutu,L_ebutu,L_odutu
     &,L_udutu,L_afutu,L_efutu,
     & L_ifutu,L_uxotu,REAL(R8_axorad,8),REAL(1.0,4),R8_ixotu
     &,L_imutu,I_amutu,
     & L_opotu,L_ipotu,L_uputu,L_arutu,L_ibutu,L_ofutu,L_ufutu
     &,L_obutu)
      !}
C FDA_doz_logic.fgi( 315, 292):���������� ��������� ������� ����������� ���������,20FDA20TRAN03_C3
C label 1664  try1664=try1664-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ososu,4),L_alivu,L_elivu
     &,R8_oposu,C30_orosu,
     & L_adepad,L_afepad,L_upepad,I_axosu,I_exosu,R_asosu
     &,R_urosu,
     & R_olosu,REAL(R_amosu,4),R_umosu,
     & REAL(R_eposu,4),R_emosu,REAL(R_omosu,4),I_evosu,
     & I_ixosu,I_uvosu,I_ovosu,L_iposu,L_oxosu,L_obusu,L_aposu
     &,
     & L_imosu,L_arosu,L_uposu,L_abusu,L_ulosu,L_irosu,
     & L_edusu,L_uxosu,L_esosu,L_isosu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_ukivu,L_okivu,L_ubusu,R_avosu,REAL(R_erosu,4),L_adusu
     &,L_idusu,
     & L_usosu,L_atosu,L_etosu,L_otosu,L_utosu,L_itosu)
      !}

      if(L_utosu.or.L_otosu.or.L_itosu.or.L_etosu.or.L_atosu.or.L_usosu
     &) then      
                  I_ivosu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ivosu = z'40000000'
      endif
C FDA_doz_logic.fgi( 415, 291):���� ���������� �������� ��������,20FDA20TRAN03_C1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uxisu,4),L_ibivu,L_obivu
     &,R8_utisu,C30_uvisu,
     & L_adepad,L_afepad,L_upepad,I_afosu,I_efosu,R_exisu
     &,R_axisu,
     & R_urisu,REAL(R_esisu,4),R_atisu,
     & REAL(R_itisu,4),R_isisu,REAL(R_usisu,4),I_edosu,
     & I_ifosu,I_udosu,I_odosu,L_otisu,L_ofosu,L_okosu,L_etisu
     &,
     & L_osisu,L_evisu,L_avisu,L_akosu,L_asisu,L_ovisu,
     & L_elosu,L_ufosu,L_ixisu,L_oxisu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_abivu,L_uxevu,L_ukosu,R_ebivu,REAL(R_ivisu,4),L_alosu
     &,L_ilosu,
     & L_abosu,L_ebosu,L_ibosu,L_ubosu,L_adosu,L_obosu)
      !}

      if(L_adosu.or.L_ubosu.or.L_obosu.or.L_ibosu.or.L_ebosu.or.L_abosu
     &) then      
                  I_idosu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idosu = z'40000000'
      endif
C FDA_doz_logic.fgi( 435, 291):���� ���������� �������� ��������,20FDA20TRAN03_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_omitu,REAL(R_atitu
     &,4),
     & REAL(R_ilotu,4),REAL(R_olotu,4),REAL(R_afotu,4),R_itevu
     &,REAL(R_udotu,4),
     & L_(489),L_(488),R_amotu,R_emotu,R_epotu,I_ofotu,R_umitu
     &,
     & R_apotu,R_etevu,I_ufotu,I_okotu,R_uritu,
     & REAL(R_esitu,4),R_apitu,REAL(R_ipitu,4),L_osevu,
     & L_isevu,I_avitu,I_utitu,I_ifotu,I_akotu,C20_etitu,C20_ititu
     &,C8_otitu,
     & L_usevu,R_opitu,REAL(R_aritu,4),L_iritu,
     & R_eritu,REAL(R_oritu,4),I_ikotu,L_upitu,
     & L_(487),L_otevu,L_atevu,I_ekotu,I_efotu,L_isitu,L_alotu
     &,L_imotu,
     & L_asitu,L_epitu,L_ositu,L_usitu,L_ulotu,L_ivitu,
     & L_abotu,L_ebotu,L_oxitu,L_uxitu,L_uvitu,L_axitu,L_ibotu
     &,L_obotu,L_ubotu,L_adotu,
     & L_edotu,L_ovitu,REAL(R8_axorad,8),REAL(1.0,4),R8_evitu
     &,L_elotu,I_ukotu,
     & L_imitu,L_emitu,L_omotu,L_umotu,L_exitu,L_idotu,L_odotu
     &,L_ixitu)
      !}
C FDA_doz_logic.fgi( 335, 292):���������� ��������� ������� ����������� ���������,20FDA20TRAN03_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_iletu,REAL(R_uretu
     &,4),
     & REAL(R_ekitu,4),REAL(R_ikitu,4),REAL(R_ubitu,4),R_orevu
     &,REAL(R_obitu,4),
     & L_(486),L_(485),R_ukitu,R_alitu,R_amitu,I_iditu,R_oletu
     &,
     & R_ulitu,R_irevu,I_oditu,I_ifitu,R_opetu,
     & REAL(R_aretu,4),R_uletu,REAL(R_emetu,4),L_upevu,
     & L_opevu,I_usetu,I_osetu,I_editu,I_uditu,C20_asetu,C20_esetu
     &,C8_isetu,
     & L_arevu,R_imetu,REAL(R_umetu,4),L_epetu,
     & R_apetu,REAL(R_ipetu,4),I_efitu,L_ometu,
     & L_(484),L_urevu,L_erevu,I_afitu,I_aditu,L_eretu,L_ufitu
     &,L_elitu,
     & L_upetu,L_ametu,L_iretu,L_oretu,L_okitu,L_etetu,
     & L_uvetu,L_axetu,L_ivetu,L_ovetu,L_otetu,L_utetu,L_exetu
     &,L_ixetu,L_oxetu,L_uxetu,
     & L_abitu,L_itetu,REAL(R8_axorad,8),REAL(1.0,4),R8_atetu
     &,L_akitu,I_ofitu,
     & L_eletu,L_aletu,L_ilitu,L_olitu,L_avetu,L_ebitu,L_ibitu
     &,L_evetu)
      !}
C FDA_doz_logic.fgi( 353, 292):���������� ��������� ������� ����������� ���������,20FDA20TRAN03_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ekatu,REAL(R_opatu
     &,4),
     & REAL(R_afetu,4),REAL(R_efetu,4),REAL(R_oxatu,4),R_umevu
     &,REAL(R_ixatu,4),
     & L_(483),L_(482),R_ofetu,R_ufetu,R_uketu,I_ebetu,R_ikatu
     &,
     & R_oketu,R_omevu,I_ibetu,I_edetu,R_imatu,
     & REAL(R_umatu,4),R_okatu,REAL(R_alatu,4),L_amevu,
     & L_ulevu,I_oratu,I_iratu,I_abetu,I_obetu,C20_upatu,C20_aratu
     &,C8_eratu,
     & L_emevu,R_elatu,REAL(R_olatu,4),L_amatu,
     & R_ulatu,REAL(R_ematu,4),I_adetu,L_ilatu,
     & L_(481),L_apevu,L_imevu,I_ubetu,I_uxatu,L_apatu,L_odetu
     &,L_aketu,
     & L_omatu,L_ukatu,L_epatu,L_ipatu,L_ifetu,L_asatu,
     & L_otatu,L_utatu,L_etatu,L_itatu,L_isatu,L_osatu,L_avatu
     &,L_evatu,L_ivatu,L_ovatu,
     & L_uvatu,L_esatu,REAL(R8_axorad,8),REAL(1.0,4),R8_uratu
     &,L_udetu,I_idetu,
     & L_akatu,L_ufatu,L_eketu,L_iketu,L_usatu,L_axatu,L_exatu
     &,L_atatu)
      !}
C FDA_doz_logic.fgi( 372, 292):���������� ��������� ������� ����������� ���������,20FDA20TRAN03_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_afusu,REAL(R_imusu
     &,4),
     & REAL(R_ubatu,4),REAL(R_adatu,4),REAL(R_ivusu,4),R_alevu
     &,REAL(R_evusu,4),
     & L_(480),L_(479),R_idatu,R_odatu,R_ofatu,I_axusu,R_efusu
     &,
     & R_ifatu,R_ukevu,I_exusu,I_abatu,R_elusu,
     & REAL(R_olusu,4),R_ifusu,REAL(R_ufusu,4),L_ekevu,
     & L_akevu,I_ipusu,I_epusu,I_uvusu,I_ixusu,C20_omusu,C20_umusu
     &,C8_apusu,
     & L_ikevu,R_akusu,REAL(R_ikusu,4),L_ukusu,
     & R_okusu,REAL(R_alusu,4),I_uxusu,L_ekusu,
     & L_(478),L_elevu,L_okevu,I_oxusu,I_ovusu,L_ulusu,L_ibatu
     &,L_udatu,
     & L_ilusu,L_ofusu,L_amusu,L_emusu,L_edatu,L_upusu,
     & L_isusu,L_osusu,L_asusu,L_esusu,L_erusu,L_irusu,L_ususu
     &,L_atusu,L_etusu,L_itusu,
     & L_otusu,L_arusu,REAL(R8_axorad,8),REAL(1.0,4),R8_opusu
     &,L_obatu,I_ebatu,
     & L_udusu,L_odusu,L_afatu,L_efatu,L_orusu,L_utusu,L_avusu
     &,L_urusu)
      !}
C FDA_doz_logic.fgi( 391, 292):���������� ��������� ������� ����������� ���������,20FDA20TRAN03_C7
      !{
      Call TRANSPORTER_DOZ(deltat,R_apivu,R_ilivu,L_asivu
     &,
     & R_epivu,R_olivu,L_esivu,R_ipivu,
     & R_ulivu,L_isivu,R_opivu,R_amivu,
     & L_osivu,L_elevu,L_ifevu,L_apevu,L_orivu,
     & L_upavu,R_ibevu,REAL(0,4),REAL(1000,4),L_aravu,L_ubevu
     &,
     & R_upivu,R_emivu,L_usivu,R_arivu,
     & R_imivu,L_ativu,R_erivu,R_omivu,
     & L_etivu,R_irivu,R_umivu,L_itivu,
     & L_urevu,L_otevu,L_ixevu,L_urivu,L_abevu,R_obevu,
     & REAL(4000,4),REAL(6000,4),L_ebevu,L_ekevu,L_amevu,L_odevu
     &,L_ufevu,
     & L_olevu,L_edevu,L_iravu,L_ulevu,L_akevu,L_idevu,L_ofevu
     &,L_ilevu,
     & L_adevu,L_upevu,L_osevu,L_ivevu,L_ipevu,L_esevu,L_avevu
     &,
     & L_oravu,L_emavu,L_imavu,L_axavu,L_uvavu,L_afivu,L_edivu
     &,L_idivu,L_udivu,
     & L_adivu,L_odivu,R_epavu,R_opavu,R_umavu,REAL(R_ikivu
     &,4),
     & REAL(R_efevu,4),REAL(R_alevu,4),REAL(R_umevu,4),REAL
     &(R_orevu,4),
     & REAL(R_itevu,4),REAL(R_exevu,4),L_esavu,L_ixavu,L_isavu
     &,L_osavu,L_usavu,L_utavu,L_itavu,
     & L_evavu,L_oxavu,L_ovavu,L_exavu,L_asavu,L_atavu,L_etavu
     &,L_uxavu,L_otavu,
     & L_avavu,L_ivavu,L_ikevu,L_emevu,L_usevu,L_ovevu,L_arevu
     &,
     & L_udevu,L_okevu,L_imevu,L_erevu,L_atevu,L_uvevu,L_uxevu
     &,
     & L_abivu,L_opevu,L_isevu,L_evevu,L_okivu,L_ukivu,L_ifivu
     &,
     & L_ofivu,L_efivu,L_epevu,L_asevu,L_uravu,L_utevu,L_obivu
     &,L_oxevu,
     & L_ibivu,L_elivu,L_ubivu,L_alivu,L_akivu,L_ufivu)
      !}
C FDA_doz_logic.fgi( 316, 262):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itutu,L_avutu,R_ututu
     &,
     & REAL(R_evutu,4),L_usavu,L_etutu,I_otutu)
      !}
C FDA_doz_logic.fgi( 368, 268):���������� ������� ���������,20FDA20TRAN03_L6
      !{
      Call DAT_DISCR_HANDLER(deltat,I_urutu,L_isutu,R_esutu
     &,
     & REAL(R_osutu,4),L_etavu,L_orutu,I_asutu)
      !}
C FDA_doz_logic.fgi( 384, 254):���������� ������� ���������,20FDA20TRAN03_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukavu,L_ilavu,R_elavu
     &,
     & REAL(R_olavu,4),L_otavu,L_okavu,I_alavu)
      !}
C FDA_doz_logic.fgi( 384, 268):���������� ������� ���������,20FDA20TRAN03_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axutu,L_oxutu,R_ixutu
     &,
     & REAL(R_uxutu,4),L_avavu,L_uvutu,I_exutu)
      !}
C FDA_doz_logic.fgi( 410, 268):���������� ������� ���������,20FDA20TRAN03_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efavu,L_ufavu,R_ofavu
     &,
     & REAL(R_akavu,4),L_ivavu,L_afavu,I_ifavu)
      !}
C FDA_doz_logic.fgi( 434, 266):���������� ������� ���������,20FDA20TRAN03_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_obavu,L_edavu,R_adavu
     &,
     & REAL(R_idavu,4),L_oxavu,L_ibavu,I_ubavu)
      !}
C FDA_doz_logic.fgi( 434, 252):���������� ������� ���������,20FDA20TRAN03_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_osoxu,REAL(R_abuxu
     &,4),
     & REAL(R_iruxu,4),REAL(R_oruxu,4),REAL(R_amuxu,4),R_adibad
     &,REAL(R_uluxu,4),
     & L_(507),L_(506),R_asuxu,R_esuxu,R_etuxu,I_omuxu,R_usoxu
     &,
     & R_atuxu,R_ubibad,I_umuxu,I_opuxu,R_uvoxu,
     & REAL(R_exoxu,4),R_atoxu,REAL(R_itoxu,4),L_ebibad,
     & L_abibad,I_aduxu,I_ubuxu,I_imuxu,I_apuxu,C20_ebuxu
     &,C20_ibuxu,C8_obuxu,
     & L_ibibad,R_otoxu,REAL(R_avoxu,4),L_ivoxu,
     & R_evoxu,REAL(R_ovoxu,4),I_ipuxu,L_utoxu,
     & L_(505),L_edibad,L_obibad,I_epuxu,I_emuxu,L_ixoxu,L_aruxu
     &,L_isuxu,
     & L_axoxu,L_etoxu,L_oxoxu,L_uxoxu,L_uruxu,L_iduxu,
     & L_akuxu,L_ekuxu,L_ofuxu,L_ufuxu,L_uduxu,L_afuxu,L_ikuxu
     &,L_okuxu,L_ukuxu,L_aluxu,
     & L_eluxu,L_oduxu,REAL(R8_axorad,8),REAL(1.0,4),R8_eduxu
     &,L_eruxu,I_upuxu,
     & L_isoxu,L_esoxu,L_osuxu,L_usuxu,L_efuxu,L_iluxu,L_oluxu
     &,L_ifuxu)
      !}
C FDA_doz_logic.fgi( 459, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN04_C3
C label 1678  try1678=try1678-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ivovu,4),L_umibad,L_apibad
     &,R8_isovu,C30_itovu,
     & L_adepad,L_afepad,L_upepad,I_ubuvu,I_aduvu,R_utovu
     &,R_otovu,
     & R_ipovu,REAL(R_upovu,4),R_orovu,
     & REAL(R_asovu,4),R_arovu,REAL(R_irovu,4),I_abuvu,
     & I_eduvu,I_obuvu,I_ibuvu,L_esovu,L_iduvu,L_ifuvu,L_urovu
     &,
     & L_erovu,L_usovu,L_osovu,L_uduvu,L_opovu,L_etovu,
     & L_akuvu,L_oduvu,L_avovu,L_evovu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_omibad,L_imibad,L_ofuvu,R_uxovu,REAL(R_atovu,4),L_ufuvu
     &,L_ekuvu,
     & L_ovovu,L_uvovu,L_axovu,L_ixovu,L_oxovu,L_exovu)
      !}

      if(L_oxovu.or.L_ixovu.or.L_exovu.or.L_axovu.or.L_uvovu.or.L_ovovu
     &) then      
                  I_ebuvu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ebuvu = z'40000000'
      endif
C FDA_doz_logic.fgi( 559, 292):���� ���������� �������� ��������,20FDA20TRAN04_C1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_odovu,4),L_efibad,L_ifibad
     &,R8_oxivu,C30_obovu,
     & L_adepad,L_afepad,L_upepad,I_ukovu,I_alovu,R_adovu
     &,R_ubovu,
     & R_otivu,REAL(R_avivu,4),R_uvivu,
     & REAL(R_exivu,4),R_evivu,REAL(R_ovivu,4),I_akovu,
     & I_elovu,I_okovu,I_ikovu,L_ixivu,L_ilovu,L_imovu,L_axivu
     &,
     & L_ivivu,L_abovu,L_uxivu,L_ulovu,L_utivu,L_ibovu,
     & L_apovu,L_olovu,L_edovu,L_idovu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_udibad,L_odibad,L_omovu,R_afibad,REAL(R_ebovu,4)
     &,L_umovu,L_epovu,
     & L_udovu,L_afovu,L_efovu,L_ofovu,L_ufovu,L_ifovu)
      !}

      if(L_ufovu.or.L_ofovu.or.L_ifovu.or.L_efovu.or.L_afovu.or.L_udovu
     &) then      
                  I_ekovu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekovu = z'40000000'
      endif
C FDA_doz_logic.fgi( 579, 292):���� ���������� �������� ��������,20FDA20TRAN04_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_irixu,REAL(R_uvixu
     &,4),
     & REAL(R_epoxu,4),REAL(R_ipoxu,4),REAL(R_ukoxu,4),R_exebad
     &,REAL(R_okoxu,4),
     & L_(504),L_(503),R_upoxu,R_aroxu,R_asoxu,I_iloxu,R_orixu
     &,
     & R_uroxu,R_axebad,I_oloxu,I_imoxu,R_otixu,
     & REAL(R_avixu,4),R_urixu,REAL(R_esixu,4),L_ivebad,
     & L_evebad,I_uxixu,I_oxixu,I_eloxu,I_uloxu,C20_axixu
     &,C20_exixu,C8_ixixu,
     & L_ovebad,R_isixu,REAL(R_usixu,4),L_etixu,
     & R_atixu,REAL(R_itixu,4),I_emoxu,L_osixu,
     & L_(502),L_ixebad,L_uvebad,I_amoxu,I_aloxu,L_evixu,L_umoxu
     &,L_eroxu,
     & L_utixu,L_asixu,L_ivixu,L_ovixu,L_opoxu,L_eboxu,
     & L_udoxu,L_afoxu,L_idoxu,L_odoxu,L_oboxu,L_uboxu,L_efoxu
     &,L_ifoxu,L_ofoxu,L_ufoxu,
     & L_akoxu,L_iboxu,REAL(R8_axorad,8),REAL(1.0,4),R8_aboxu
     &,L_apoxu,I_omoxu,
     & L_erixu,L_arixu,L_iroxu,L_oroxu,L_adoxu,L_ekoxu,L_ikoxu
     &,L_edoxu)
      !}
C FDA_doz_logic.fgi( 479, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN04_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_epexu,REAL(R_otexu
     &,4),
     & REAL(R_amixu,4),REAL(R_emixu,4),REAL(R_ofixu,4),R_itebad
     &,REAL(R_ifixu,4),
     & L_(501),L_(500),R_omixu,R_umixu,R_upixu,I_ekixu,R_ipexu
     &,
     & R_opixu,R_etebad,I_ikixu,I_elixu,R_isexu,
     & REAL(R_usexu,4),R_opexu,REAL(R_arexu,4),L_osebad,
     & L_isebad,I_ovexu,I_ivexu,I_akixu,I_okixu,C20_utexu
     &,C20_avexu,C8_evexu,
     & L_usebad,R_erexu,REAL(R_orexu,4),L_asexu,
     & R_urexu,REAL(R_esexu,4),I_alixu,L_irexu,
     & L_(499),L_otebad,L_atebad,I_ukixu,I_ufixu,L_atexu,L_olixu
     &,L_apixu,
     & L_osexu,L_upexu,L_etexu,L_itexu,L_imixu,L_axexu,
     & L_obixu,L_ubixu,L_ebixu,L_ibixu,L_ixexu,L_oxexu,L_adixu
     &,L_edixu,L_idixu,L_odixu,
     & L_udixu,L_exexu,REAL(R8_axorad,8),REAL(1.0,4),R8_uvexu
     &,L_ulixu,I_ilixu,
     & L_apexu,L_umexu,L_epixu,L_ipixu,L_uxexu,L_afixu,L_efixu
     &,L_abixu)
      !}
C FDA_doz_logic.fgi( 497, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN04_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_amaxu,REAL(R_isaxu
     &,4),
     & REAL(R_ukexu,4),REAL(R_alexu,4),REAL(R_idexu,4),R_orebad
     &,REAL(R_edexu,4),
     & L_(498),L_(497),R_ilexu,R_olexu,R_omexu,I_afexu,R_emaxu
     &,
     & R_imexu,R_irebad,I_efexu,I_akexu,R_eraxu,
     & REAL(R_oraxu,4),R_imaxu,REAL(R_umaxu,4),L_upebad,
     & L_opebad,I_itaxu,I_etaxu,I_udexu,I_ifexu,C20_osaxu
     &,C20_usaxu,C8_ataxu,
     & L_arebad,R_apaxu,REAL(R_ipaxu,4),L_upaxu,
     & R_opaxu,REAL(R_araxu,4),I_ufexu,L_epaxu,
     & L_(496),L_urebad,L_erebad,I_ofexu,I_odexu,L_uraxu,L_ikexu
     &,L_ulexu,
     & L_iraxu,L_omaxu,L_asaxu,L_esaxu,L_elexu,L_utaxu,
     & L_ixaxu,L_oxaxu,L_axaxu,L_exaxu,L_evaxu,L_ivaxu,L_uxaxu
     &,L_abexu,L_ebexu,L_ibexu,
     & L_obexu,L_avaxu,REAL(R8_axorad,8),REAL(1.0,4),R8_otaxu
     &,L_okexu,I_ekexu,
     & L_ulaxu,L_olaxu,L_amexu,L_emexu,L_ovaxu,L_ubexu,L_adexu
     &,L_uvaxu)
      !}
C FDA_doz_logic.fgi( 516, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN04_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ukuvu,REAL(R_eruvu
     &,4),
     & REAL(R_ofaxu,4),REAL(R_ufaxu,4),REAL(R_ebaxu,4),R_umebad
     &,REAL(R_abaxu,4),
     & L_(495),L_(494),R_ekaxu,R_ikaxu,R_ilaxu,I_ubaxu,R_aluvu
     &,
     & R_elaxu,R_omebad,I_adaxu,I_udaxu,R_apuvu,
     & REAL(R_ipuvu,4),R_eluvu,REAL(R_oluvu,4),L_amebad,
     & L_ulebad,I_esuvu,I_asuvu,I_obaxu,I_edaxu,C20_iruvu
     &,C20_oruvu,C8_uruvu,
     & L_emebad,R_uluvu,REAL(R_emuvu,4),L_omuvu,
     & R_imuvu,REAL(R_umuvu,4),I_odaxu,L_amuvu,
     & L_(493),L_apebad,L_imebad,I_idaxu,I_ibaxu,L_opuvu,L_efaxu
     &,L_okaxu,
     & L_epuvu,L_iluvu,L_upuvu,L_aruvu,L_akaxu,L_osuvu,
     & L_evuvu,L_ivuvu,L_utuvu,L_avuvu,L_atuvu,L_etuvu,L_ovuvu
     &,L_uvuvu,L_axuvu,L_exuvu,
     & L_ixuvu,L_usuvu,REAL(R8_axorad,8),REAL(1.0,4),R8_isuvu
     &,L_ifaxu,I_afaxu,
     & L_okuvu,L_ikuvu,L_ukaxu,L_alaxu,L_ituvu,L_oxuvu,L_uxuvu
     &,L_otuvu)
      !}
C FDA_doz_logic.fgi( 535, 293):���������� ��������� ������� ����������� ���������,20FDA20TRAN04_C7
      !{
      Call TRANSPORTER_DOZ(deltat,R_uribad,R_epibad,L_utibad
     &,
     & R_asibad,R_ipibad,L_avibad,R_esibad,
     & R_opibad,L_evibad,R_isibad,R_upibad,
     & L_ivibad,L_apebad,L_elebad,L_urebad,L_itibad,
     & L_osabad,R_efebad,REAL(0,4),REAL(1000,4),L_usabad,L_ofebad
     &,
     & R_osibad,R_aribad,L_ovibad,R_usibad,
     & R_eribad,L_uvibad,R_atibad,R_iribad,
     & L_axibad,R_etibad,R_oribad,L_exibad,
     & L_otebad,L_ixebad,L_edibad,L_otibad,L_udebad,R_ifebad
     &,
     & REAL(6000,4),REAL(8000,4),L_afebad,L_amebad,L_upebad
     &,L_ikebad,L_olebad,
     & L_ipebad,L_akebad,L_etabad,L_opebad,L_ulebad,L_ekebad
     &,L_ilebad,L_epebad,
     & L_ufebad,L_osebad,L_ivebad,L_ebibad,L_esebad,L_avebad
     &,L_uxebad,
     & L_itabad,L_arabad,L_erabad,L_ubebad,L_obebad,L_ukibad
     &,L_akibad,L_ekibad,L_okibad,
     & L_ufibad,L_ikibad,R_asabad,R_isabad,R_orabad,REAL(R_emibad
     &,4),
     & REAL(R_alebad,4),REAL(R_umebad,4),REAL(R_orebad,4)
     &,REAL(R_itebad,4),
     & REAL(R_exebad,4),REAL(R_adibad,4),L_avabad,L_edebad
     &,L_evabad,L_ivabad,L_ovabad,L_oxabad,L_exabad,
     & L_abebad,L_idebad,L_ibebad,L_adebad,L_utabad,L_uvabad
     &,L_axabad,L_odebad,L_ixabad,
     & L_uxabad,L_ebebad,L_emebad,L_arebad,L_ovebad,L_ibibad
     &,L_usebad,
     & L_okebad,L_imebad,L_erebad,L_atebad,L_uvebad,L_obibad
     &,L_odibad,
     & L_udibad,L_isebad,L_evebad,L_abibad,L_imibad,L_omibad
     &,L_elibad,
     & L_ilibad,L_alibad,L_asebad,L_utebad,L_otabad,L_oxebad
     &,L_ifibad,L_idibad,
     & L_efibad,L_apibad,L_ofibad,L_umibad,L_ulibad,L_olibad
     &)
      !}
C FDA_doz_logic.fgi( 460, 263):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN04
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exuxu,L_uxuxu,R_oxuxu
     &,
     & REAL(R_ababad,4),L_ovabad,L_axuxu,I_ixuxu)
      !}
C FDA_doz_logic.fgi( 512, 268):���������� ������� ���������,20FDA20TRAN04_L6
      !{
      Call DAT_DISCR_HANDLER(deltat,I_otuxu,L_evuxu,R_avuxu
     &,
     & REAL(R_ivuxu,4),L_axabad,L_ituxu,I_utuxu)
      !}
C FDA_doz_logic.fgi( 528, 256):���������� ������� ���������,20FDA20TRAN04_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omabad,L_epabad,R_apabad
     &,
     & REAL(R_ipabad,4),L_ixabad,L_imabad,I_umabad)
      !}
C FDA_doz_logic.fgi( 528, 268):���������� ������� ���������,20FDA20TRAN04_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubabad,L_idabad,R_edabad
     &,
     & REAL(R_odabad,4),L_uxabad,L_obabad,I_adabad)
      !}
C FDA_doz_logic.fgi( 554, 268):���������� ������� ���������,20FDA20TRAN04_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alabad,L_olabad,R_ilabad
     &,
     & REAL(R_ulabad,4),L_ebebad,L_ukabad,I_elabad)
      !}
C FDA_doz_logic.fgi( 578, 268):���������� ������� ���������,20FDA20TRAN04_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ifabad,L_akabad,R_ufabad
     &,
     & REAL(R_ekabad,4),L_idebad,L_efabad,I_ofabad)
      !}
C FDA_doz_logic.fgi( 578, 252):���������� ������� ���������,20FDA20TRAN04_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_itasu,REAL(R_edesu
     &,4),
     & REAL(R_usesu,4),REAL(R_atesu,4),REAL(R_ipesu,4),R_avasu
     &,REAL(R_epesu,4),
     & L_(477),L_(476),R_itesu,R_otesu,R_ovesu,I_aresu,R_utasu
     &,
     & R_ivesu,R_otasu,I_eresu,I_asesu,R_abesu,
     & REAL(R_ibesu,4),R_evasu,REAL(R_ovasu,4),L_isasu,
     & L_esasu,I_ifesu,I_efesu,I_upesu,I_iresu,C20_idesu,C20_odesu
     &,C8_udesu,
     & L_osasu,R_uvasu,REAL(R_exasu,4),L_oxasu,
     & R_ixasu,REAL(R_uxasu,4),I_uresu,L_axasu,
     & L_(475),L_afesu,L_usasu,I_oresu,I_opesu,L_obesu,L_isesu
     &,L_utesu,
     & L_ebesu,L_ivasu,L_ubesu,L_adesu,L_etesu,L_ufesu,
     & L_ilesu,L_olesu,L_alesu,L_elesu,L_ekesu,L_ikesu,L_ulesu
     &,L_amesu,L_emesu,L_imesu,
     & L_omesu,L_akesu,REAL(R8_axorad,8),REAL(1.0,4),R8_ofesu
     &,L_osesu,I_esesu,
     & L_etasu,L_atasu,L_avesu,L_evesu,L_okesu,L_umesu,L_apesu
     &,L_ukesu)
      !}
C FDA_doz_logic.fgi(  28, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN05_C3
C label 1692  try1692=try1692-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_areru,4),L_iveru,L_overu
     &,R8_oleru,C30_aperu,
     & L_adepad,L_afepad,L_upepad,I_iteru,I_oteru,R_iperu
     &,R_eperu,
     & R_oferu,REAL(R_akeru,4),R_ukeru,
     & REAL(R_eleru,4),R_ekeru,REAL(R_okeru,4),I_oseru,
     & I_uteru,I_eteru,I_ateru,L_ileru,L_averu,L_ixeru,L_aleru
     &,
     & L_ikeru,L_imeru,L_emeru,L_uveru,L_uferu,L_umeru,
     & L_abiru,L_everu,L_operu,L_uperu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_ameru,L_uleru,L_oxeru,R_iseru,REAL(R_omeru,4),L_uxeru
     &,L_ebiru,
     & L_ereru,L_ireru,L_oreru,L_aseru,L_eseru,L_ureru)
      !}

      if(L_eseru.or.L_aseru.or.L_ureru.or.L_oreru.or.L_ireru.or.L_ereru
     &) then      
                  I_useru = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_useru = z'40000000'
      endif
C FDA_doz_logic.fgi( 148, 211):���� ���������� �������� ��������,20FDA20TRAN05_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_upuru,REAL(R_ovuru
     &,4),
     & REAL(R_epasu,4),REAL(R_ipasu,4),REAL(R_ukasu,4),R_iruru
     &,REAL(R_okasu,4),
     & L_(474),L_(473),R_upasu,R_arasu,R_asasu,I_ilasu,R_eruru
     &,
     & R_urasu,R_aruru,I_olasu,I_imasu,R_ituru,
     & REAL(R_uturu,4),R_oruru,REAL(R_asuru,4),L_umuru,
     & L_omuru,I_uxuru,I_oxuru,I_elasu,I_ulasu,C20_uvuru,C20_axuru
     &,C8_exuru,
     & L_apuru,R_esuru,REAL(R_osuru,4),L_aturu,
     & R_usuru,REAL(R_eturu,4),I_emasu,L_isuru,
     & L_(472),L_ixuru,L_epuru,I_amasu,I_alasu,L_avuru,L_umasu
     &,L_erasu,
     & L_oturu,L_ururu,L_evuru,L_ivuru,L_opasu,L_ebasu,
     & L_udasu,L_afasu,L_idasu,L_odasu,L_obasu,L_ubasu,L_efasu
     &,L_ifasu,L_ofasu,L_ufasu,
     & L_akasu,L_ibasu,REAL(R8_axorad,8),REAL(1.0,4),R8_abasu
     &,L_apasu,I_omasu,
     & L_opuru,L_ipuru,L_irasu,L_orasu,L_adasu,L_ekasu,L_ikasu
     &,L_edasu)
      !}
C FDA_doz_logic.fgi(  48, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN05_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_eloru,REAL(R_asoru
     &,4),
     & REAL(R_okuru,4),REAL(R_ukuru,4),REAL(R_eduru,4),R_uloru
     &,REAL(R_aduru,4),
     & L_(471),L_(470),R_eluru,R_iluru,R_imuru,I_uduru,R_oloru
     &,
     & R_emuru,R_iloru,I_afuru,I_ufuru,R_uporu,
     & REAL(R_eroru,4),R_amoru,REAL(R_imoru,4),L_ekoru,
     & L_akoru,I_etoru,I_atoru,I_oduru,I_efuru,C20_esoru,C20_isoru
     &,C8_osoru,
     & L_ikoru,R_omoru,REAL(R_aporu,4),L_iporu,
     & R_eporu,REAL(R_oporu,4),I_ofuru,L_umoru,
     & L_(469),L_usoru,L_okoru,I_ifuru,I_iduru,L_iroru,L_ekuru
     &,L_oluru,
     & L_aroru,L_emoru,L_ororu,L_uroru,L_aluru,L_otoru,
     & L_exoru,L_ixoru,L_uvoru,L_axoru,L_avoru,L_evoru,L_oxoru
     &,L_uxoru,L_aburu,L_eburu,
     & L_iburu,L_utoru,REAL(R8_axorad,8),REAL(1.0,4),R8_itoru
     &,L_ikuru,I_akuru,
     & L_aloru,L_ukoru,L_uluru,L_amuru,L_ivoru,L_oburu,L_uburu
     &,L_ovoru)
      !}
C FDA_doz_logic.fgi(  66, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN05_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_odiru,REAL(R_imiru
     &,4),
     & REAL(R_adoru,4),REAL(R_edoru,4),REAL(R_oviru,4),R_efiru
     &,REAL(R_iviru,4),
     & L_(468),L_(467),R_odoru,R_udoru,R_uforu,I_exiru,R_afiru
     &,
     & R_oforu,R_udiru,I_ixiru,I_eboru,R_eliru,
     & REAL(R_oliru,4),R_ifiru,REAL(R_ufiru,4),L_obiru,
     & L_ibiru,I_opiru,I_ipiru,I_axiru,I_oxiru,C20_omiru,C20_umiru
     &,C8_apiru,
     & L_ubiru,R_akiru,REAL(R_ikiru,4),L_ukiru,
     & R_okiru,REAL(R_aliru,4),I_aboru,L_ekiru,
     & L_(466),L_epiru,L_adiru,I_uxiru,I_uviru,L_uliru,L_oboru
     &,L_aforu,
     & L_iliru,L_ofiru,L_amiru,L_emiru,L_idoru,L_ariru,
     & L_osiru,L_usiru,L_esiru,L_isiru,L_iriru,L_oriru,L_atiru
     &,L_etiru,L_itiru,L_otiru,
     & L_utiru,L_eriru,REAL(R8_axorad,8),REAL(1.0,4),R8_upiru
     &,L_uboru,I_iboru,
     & L_idiru,L_ediru,L_eforu,L_iforu,L_uriru,L_aviru,L_eviru
     &,L_asiru)
      !}
C FDA_doz_logic.fgi(  85, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN05_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_oxamu,REAL(R_ikemu
     &,4),
     & REAL(R_axemu,4),REAL(R_exemu,4),REAL(R_osemu,4),R_ebemu
     &,REAL(R_isemu,4),
     & L_(453),L_(452),R_oxemu,R_uxemu,R_ubimu,I_etemu,R_abemu
     &,
     & R_obimu,R_uxamu,I_itemu,I_evemu,R_efemu,
     & REAL(R_ofemu,4),R_ibemu,REAL(R_ubemu,4),L_ovamu,
     & L_ivamu,I_olemu,I_ilemu,I_atemu,I_otemu,C20_okemu,C20_ukemu
     &,C8_alemu,
     & L_uvamu,R_ademu,REAL(R_idemu,4),L_udemu,
     & R_odemu,REAL(R_afemu,4),I_avemu,L_edemu,
     & L_(451),L_elemu,L_axamu,I_utemu,I_usemu,L_ufemu,L_ovemu
     &,L_abimu,
     & L_ifemu,L_obemu,L_akemu,L_ekemu,L_ixemu,L_amemu,
     & L_opemu,L_upemu,L_epemu,L_ipemu,L_imemu,L_omemu,L_aremu
     &,L_eremu,L_iremu,L_oremu,
     & L_uremu,L_ememu,REAL(R8_axorad,8),REAL(1.0,4),R8_ulemu
     &,L_uvemu,I_ivemu,
     & L_ixamu,L_examu,L_ebimu,L_ibimu,L_umemu,L_asemu,L_esemu
     &,L_apemu)
      !}
C FDA_doz_logic.fgi( 103, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN05_C7
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_atulu,REAL(R_ubamu
     &,4),
     & REAL(R_isamu,4),REAL(R_osamu,4),REAL(R_apamu,4),R_otulu
     &,REAL(R_umamu,4),
     & L_(450),L_(449),R_atamu,R_etamu,R_evamu,I_opamu,R_itulu
     &,
     & R_avamu,R_etulu,I_upamu,I_oramu,R_oxulu,
     & REAL(R_abamu,4),R_utulu,REAL(R_evulu,4),L_asulu,
     & L_urulu,I_afamu,I_udamu,I_ipamu,I_aramu,C20_adamu,C20_edamu
     &,C8_idamu,
     & L_esulu,R_ivulu,REAL(R_uvulu,4),L_exulu,
     & R_axulu,REAL(R_ixulu,4),I_iramu,L_ovulu,
     & L_(448),L_odamu,L_isulu,I_eramu,I_epamu,L_ebamu,L_asamu
     &,L_itamu,
     & L_uxulu,L_avulu,L_ibamu,L_obamu,L_usamu,L_ifamu,
     & L_alamu,L_elamu,L_okamu,L_ukamu,L_ufamu,L_akamu,L_ilamu
     &,L_olamu,L_ulamu,L_amamu,
     & L_emamu,L_ofamu,REAL(R8_axorad,8),REAL(1.0,4),R8_efamu
     &,L_esamu,I_uramu,
     & L_usulu,L_osulu,L_otamu,L_utamu,L_ekamu,L_imamu,L_omamu
     &,L_ikamu)
      !}
C FDA_doz_logic.fgi( 121, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN05_C8
      !{
      Call TRANSPORTER_DOZ(deltat,R_otofu,R_asofu,L_oxofu
     &,
     & R_utofu,R_esofu,L_uxofu,R_avofu,
     & R_isofu,L_abufu,R_evofu,R_osofu,
     & L_ebufu,L_elemu,L_odamu,L_epiru,L_exofu,
     & L_osifu,R_edofu,REAL(0,4),REAL(1000,4),L_usifu,L_odofu
     &,
     & R_ivofu,R_usofu,L_ibufu,R_ovofu,
     & R_atofu,L_obufu,R_uvofu,R_etofu,
     & L_ubufu,R_axofu,R_itofu,L_adufu,
     & L_usoru,L_ixuru,L_afesu,L_ixofu,L_ubofu,R_idofu,
     & REAL(8000,4),REAL(10000,4),L_adofu,L_ovamu,L_obiru
     &,L_asulu,L_ifofu,
     & L_ufofu,L_afofu,L_etifu,L_ibiru,L_ivamu,L_urulu,L_efofu
     &,L_ofofu,
     & L_udofu,L_ekoru,L_umuru,L_isasu,L_ekofu,L_okofu,L_alofu
     &,
     & L_itifu,L_irifu,L_orifu,L_abofu,L_uxifu,L_omofu,L_ulofu
     &,L_amofu,L_imofu,
     & L_olofu,L_emofu,R_emupu,R_isifu,R_amupu,REAL(R_arofu
     &,4),
     & REAL(R_otulu,4),REAL(R_ebemu,4),REAL(R_efiru,4),REAL
     &(R_uloru,4),
     & REAL(R_iruru,4),REAL(R_avasu,4),L_utifu,L_ibofu,L_avifu
     &,L_evifu,L_ivifu,L_exifu,L_axifu,
     & L_ixifu,L_okisu,L_oxifu,L_ebofu,L_abisu,L_ovifu,L_uvifu
     &,L_obofu,L_erisu,
     & L_udisu,L_imisu,L_uvamu,L_ubiru,L_apuru,L_osasu,L_ikoru
     &,
     & L_isulu,L_axamu,L_adiru,L_okoru,L_epuru,L_usasu,L_uleru
     &,
     & L_ameru,L_akoru,L_omuru,L_esasu,L_erofu,L_irofu,L_apofu
     &,
     & L_epofu,L_umofu,L_akofu,L_ikofu,L_otifu,L_ukofu,L_overu
     &,L_elofu,
     & L_iveru,L_urofu,L_ilofu,L_orofu,L_opofu,L_ipofu)
      !}
C FDA_doz_logic.fgi(  28, 183):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN05
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axesu,L_oxesu,R_ixesu
     &,
     & REAL(R_uxesu,4),L_abisu,L_uvesu,I_exesu)
      !}
C FDA_doz_logic.fgi(  98, 174):���������� ������� ���������,20FDA20TRAN05_L9
      !{
      Call DAT_DISCR_HANDLER(deltat,I_episu,L_upisu,R_opisu
     &,
     & REAL(R_arisu,4),L_erisu,L_apisu,I_ipisu)
      !}
C FDA_doz_logic.fgi(  98, 188):���������� ������� ���������,20FDA20TRAN05_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubisu,L_idisu,R_edisu
     &,
     & REAL(R_odisu,4),L_udisu,L_obisu,I_adisu)
      !}
C FDA_doz_logic.fgi( 122, 188):���������� ������� ���������,20FDA20TRAN05_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilisu,L_amisu,R_ulisu
     &,
     & REAL(R_emisu,4),L_imisu,L_elisu,I_olisu)
      !}
C FDA_doz_logic.fgi( 148, 186):���������� ������� ���������,20FDA20TRAN05_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofisu,L_ekisu,R_akisu
     &,
     & REAL(R_ikisu,4),L_okisu,L_ifisu,I_ufisu)
      !}
C FDA_doz_logic.fgi( 148, 172):���������� ������� ���������,20FDA20TRAN05_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_orepu,REAL(R_ixepu
     &,4),
     & REAL(R_aripu,4),REAL(R_eripu,4),REAL(R_olipu,4),R_esepu
     &,REAL(R_ilipu,4),
     & L_(465),L_(464),R_oripu,R_uripu,R_usipu,I_emipu,R_asepu
     &,
     & R_osipu,R_urepu,I_imipu,I_epipu,R_evepu,
     & REAL(R_ovepu,4),R_isepu,REAL(R_usepu,4),L_opepu,
     & L_ipepu,I_obipu,I_ibipu,I_amipu,I_omipu,C20_oxepu,C20_uxepu
     &,C8_abipu,
     & L_upepu,R_atepu,REAL(R_itepu,4),L_utepu,
     & R_otepu,REAL(R_avepu,4),I_apipu,L_etepu,
     & L_(463),L_ebipu,L_arepu,I_umipu,I_ulipu,L_uvepu,L_opipu
     &,L_asipu,
     & L_ivepu,L_osepu,L_axepu,L_exepu,L_iripu,L_adipu,
     & L_ofipu,L_ufipu,L_efipu,L_ifipu,L_idipu,L_odipu,L_akipu
     &,L_ekipu,L_ikipu,L_okipu,
     & L_ukipu,L_edipu,REAL(R8_axorad,8),REAL(1.0,4),R8_ubipu
     &,L_upipu,I_ipipu,
     & L_irepu,L_erepu,L_esipu,L_isipu,L_udipu,L_alipu,L_elipu
     &,L_afipu)
      !}
C FDA_doz_logic.fgi( 175, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN06_C3
C label 1705  try1705=try1705-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_imimu,4),L_usimu,L_olupu
     &,R8_akimu,C30_ilimu,
     & L_adepad,L_afepad,L_upepad,I_urimu,I_asimu,R_ulimu
     &,R_olimu,
     & R_adimu,REAL(R_idimu,4),R_efimu,
     & REAL(R_ofimu,4),R_odimu,REAL(R_afimu,4),I_arimu,
     & I_esimu,I_orimu,I_irimu,L_ufimu,L_isimu,L_otimu,L_ifimu
     &,
     & L_udimu,L_ukimu,L_okimu,L_atimu,L_edimu,L_elimu,
     & L_evimu,L_osimu,L_amimu,L_emimu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_ikimu,L_ekimu,L_utimu,R_upimu,REAL(R_alimu,4),L_avimu
     &,L_ivimu,
     & L_omimu,L_umimu,L_apimu,L_ipimu,L_opimu,L_epimu)
      !}

      if(L_opimu.or.L_ipimu.or.L_epimu.or.L_apimu.or.L_umimu.or.L_omimu
     &) then      
                  I_erimu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_erimu = z'40000000'
      endif
C FDA_doz_logic.fgi( 295, 211):���� ���������� �������� ��������,20FDA20TRAN06_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_amapu,REAL(R_usapu
     &,4),
     & REAL(R_ilepu,4),REAL(R_olepu,4),REAL(R_afepu,4),R_omapu
     &,REAL(R_udepu,4),
     & L_(462),L_(461),R_amepu,R_emepu,R_epepu,I_ofepu,R_imapu
     &,
     & R_apepu,R_emapu,I_ufepu,I_okepu,R_orapu,
     & REAL(R_asapu,4),R_umapu,REAL(R_epapu,4),L_alapu,
     & L_ukapu,I_avapu,I_utapu,I_ifepu,I_akepu,C20_atapu,C20_etapu
     &,C8_itapu,
     & L_elapu,R_ipapu,REAL(R_upapu,4),L_erapu,
     & R_arapu,REAL(R_irapu,4),I_ikepu,L_opapu,
     & L_(460),L_otapu,L_ilapu,I_ekepu,I_efepu,L_esapu,L_alepu
     &,L_imepu,
     & L_urapu,L_apapu,L_isapu,L_osapu,L_ulepu,L_ivapu,
     & L_abepu,L_ebepu,L_oxapu,L_uxapu,L_uvapu,L_axapu,L_ibepu
     &,L_obepu,L_ubepu,L_adepu,
     & L_edepu,L_ovapu,REAL(R8_axorad,8),REAL(1.0,4),R8_evapu
     &,L_elepu,I_ukepu,
     & L_ulapu,L_olapu,L_omepu,L_umepu,L_exapu,L_idepu,L_odepu
     &,L_ixapu)
      !}
C FDA_doz_logic.fgi( 195, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN06_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ifumu,REAL(R_epumu
     &,4),
     & REAL(R_udapu,4),REAL(R_afapu,4),REAL(R_ixumu,4),R_akumu
     &,REAL(R_exumu,4),
     & L_(459),L_(458),R_ifapu,R_ofapu,R_okapu,I_abapu,R_ufumu
     &,
     & R_ikapu,R_ofumu,I_ebapu,I_adapu,R_amumu,
     & REAL(R_imumu,4),R_ekumu,REAL(R_okumu,4),L_idumu,
     & L_edumu,I_irumu,I_erumu,I_uxumu,I_ibapu,C20_ipumu,C20_opumu
     &,C8_upumu,
     & L_odumu,R_ukumu,REAL(R_elumu,4),L_olumu,
     & R_ilumu,REAL(R_ulumu,4),I_ubapu,L_alumu,
     & L_(457),L_arumu,L_udumu,I_obapu,I_oxumu,L_omumu,L_idapu
     &,L_ufapu,
     & L_emumu,L_ikumu,L_umumu,L_apumu,L_efapu,L_urumu,
     & L_itumu,L_otumu,L_atumu,L_etumu,L_esumu,L_isumu,L_utumu
     &,L_avumu,L_evumu,L_ivumu,
     & L_ovumu,L_asumu,REAL(R8_axorad,8),REAL(1.0,4),R8_orumu
     &,L_odapu,I_edapu,
     & L_efumu,L_afumu,L_akapu,L_ekapu,L_osumu,L_uvumu,L_axumu
     &,L_usumu)
      !}
C FDA_doz_logic.fgi( 213, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN06_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_uximu,REAL(R_okomu
     &,4),
     & REAL(R_exomu,4),REAL(R_ixomu,4),REAL(R_usomu,4),R_ibomu
     &,REAL(R_osomu,4),
     & L_(456),L_(455),R_uxomu,R_abumu,R_adumu,I_itomu,R_ebomu
     &,
     & R_ubumu,R_abomu,I_otomu,I_ivomu,R_ifomu,
     & REAL(R_ufomu,4),R_obomu,REAL(R_adomu,4),L_uvimu,
     & L_ovimu,I_ulomu,I_olomu,I_etomu,I_utomu,C20_ukomu,C20_alomu
     &,C8_elomu,
     & L_aximu,R_edomu,REAL(R_odomu,4),L_afomu,
     & R_udomu,REAL(R_efomu,4),I_evomu,L_idomu,
     & L_(454),L_ilomu,L_eximu,I_avomu,I_atomu,L_akomu,L_uvomu
     &,L_ebumu,
     & L_ofomu,L_ubomu,L_ekomu,L_ikomu,L_oxomu,L_emomu,
     & L_upomu,L_aromu,L_ipomu,L_opomu,L_omomu,L_umomu,L_eromu
     &,L_iromu,L_oromu,L_uromu,
     & L_asomu,L_imomu,REAL(R8_axorad,8),REAL(1.0,4),R8_amomu
     &,L_axomu,I_ovomu,
     & L_oximu,L_iximu,L_ibumu,L_obumu,L_apomu,L_esomu,L_isomu
     &,L_epomu)
      !}
C FDA_doz_logic.fgi( 232, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN06_C6
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ipolu,REAL(R_evolu
     &,4),
     & REAL(R_umulu,4),REAL(R_apulu,4),REAL(R_ikulu,4),R_arolu
     &,REAL(R_ekulu,4),
     & L_(447),L_(446),R_ipulu,R_opulu,R_orulu,I_alulu,R_upolu
     &,
     & R_irulu,R_opolu,I_elulu,I_amulu,R_atolu,
     & REAL(R_itolu,4),R_erolu,REAL(R_orolu,4),L_imolu,
     & L_emolu,I_ixolu,I_exolu,I_ukulu,I_ilulu,C20_ivolu,C20_ovolu
     &,C8_uvolu,
     & L_omolu,R_urolu,REAL(R_esolu,4),L_osolu,
     & R_isolu,REAL(R_usolu,4),I_ululu,L_asolu,
     & L_(445),L_axolu,L_umolu,I_olulu,I_okulu,L_otolu,L_imulu
     &,L_upulu,
     & L_etolu,L_irolu,L_utolu,L_avolu,L_epulu,L_uxolu,
     & L_idulu,L_odulu,L_adulu,L_edulu,L_ebulu,L_ibulu,L_udulu
     &,L_afulu,L_efulu,L_ifulu,
     & L_ofulu,L_abulu,REAL(R8_axorad,8),REAL(1.0,4),R8_oxolu
     &,L_omulu,I_emulu,
     & L_epolu,L_apolu,L_arulu,L_erulu,L_obulu,L_ufulu,L_akulu
     &,L_ubulu)
      !}
C FDA_doz_logic.fgi( 249, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN06_C7
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ukilu,REAL(R_orilu
     &,4),
     & REAL(R_ekolu,4),REAL(R_ikolu,4),REAL(R_ubolu,4),R_ililu
     &,REAL(R_obolu,4),
     & L_(444),L_(443),R_ukolu,R_alolu,R_amolu,I_idolu,R_elilu
     &,
     & R_ulolu,R_alilu,I_odolu,I_ifolu,R_ipilu,
     & REAL(R_upilu,4),R_olilu,REAL(R_amilu,4),L_ufilu,
     & L_ofilu,I_usilu,I_osilu,I_edolu,I_udolu,C20_urilu,C20_asilu
     &,C8_esilu,
     & L_akilu,R_emilu,REAL(R_omilu,4),L_apilu,
     & R_umilu,REAL(R_epilu,4),I_efolu,L_imilu,
     & L_(442),L_isilu,L_ekilu,I_afolu,I_adolu,L_arilu,L_ufolu
     &,L_elolu,
     & L_opilu,L_ulilu,L_erilu,L_irilu,L_okolu,L_etilu,
     & L_uvilu,L_axilu,L_ivilu,L_ovilu,L_otilu,L_utilu,L_exilu
     &,L_ixilu,L_oxilu,L_uxilu,
     & L_abolu,L_itilu,REAL(R8_axorad,8),REAL(1.0,4),R8_atilu
     &,L_akolu,I_ofolu,
     & L_okilu,L_ikilu,L_ilolu,L_ololu,L_avilu,L_ebolu,L_ibolu
     &,L_evilu)
      !}
C FDA_doz_logic.fgi( 266, 212):���������� ��������� ������� ����������� ���������,20FDA20TRAN06_C8
      !{
      Call TRANSPORTER_DOZ(deltat,R_ukifu,R_efifu,L_umifu
     &,
     & R_alifu,R_ififu,L_apifu,R_elifu,
     & R_ofifu,L_epifu,R_ilifu,R_ufifu,
     & L_ipifu,L_axolu,L_isilu,L_ilomu,L_imifu,
     & L_ufefu,R_irefu,REAL(0,4),REAL(1000,4),L_akefu,L_urefu
     &,
     & R_olifu,R_akifu,L_opifu,R_ulifu,
     & R_ekifu,L_upifu,R_amifu,R_ikifu,
     & L_arifu,R_emifu,R_okifu,L_erifu,
     & L_arumu,L_otapu,L_ebipu,L_omifu,L_arefu,R_orefu,
     & REAL(10000,4),REAL(12000,4),L_erefu,L_imolu,L_uvimu
     &,L_ufilu,L_osefu,
     & L_atefu,L_esefu,L_ikefu,L_ovimu,L_emolu,L_ofilu,L_isefu
     &,L_usefu,
     & L_asefu,L_idumu,L_alapu,L_opepu,L_itefu,L_utefu,L_evefu
     &,
     & L_okefu,L_odefu,L_udefu,L_epefu,L_apefu,L_uxefu,L_axefu
     &,L_exefu,L_oxefu,
     & L_uvefu,L_ixefu,R_ilupu,R_ofefu,R_elupu,REAL(R_edifu
     &,4),
     & REAL(R_ililu,4),REAL(R_arolu,4),REAL(R_ibomu,4),REAL
     &(R_akumu,4),
     & REAL(R_omapu,4),REAL(R_esepu,4),L_elefu,L_ipefu,L_ilefu
     &,L_olefu,L_ulefu,L_imefu,L_emefu,
     & L_omefu,L_opefu,L_umefu,L_evipu,L_alefu,L_amefu,L_udopu
     &,L_upefu,L_imopu,
     & L_abopu,L_okopu,L_omolu,L_aximu,L_elapu,L_upepu,L_odumu
     &,
     & L_ekilu,L_umolu,L_eximu,L_udumu,L_ilapu,L_arepu,L_ekimu
     &,
     & L_ikimu,L_edumu,L_ukapu,L_ipepu,L_idifu,L_odifu,L_ebifu
     &,
     & L_ibifu,L_abifu,L_etefu,L_otefu,L_ukefu,L_avefu,L_olupu
     &,L_ivefu,
     & L_usimu,L_afifu,L_ovefu,L_udifu,L_ubifu,L_obifu)
      !}
C FDA_doz_logic.fgi( 175, 181):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN06
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubopu,L_idopu,R_edopu
     &,
     & REAL(R_odopu,4),L_udopu,L_obopu,I_adopu)
      !}
C FDA_doz_logic.fgi( 294, 172):���������� ������� ���������,20FDA20TRAN06_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilopu,L_amopu,R_ulopu
     &,
     & REAL(R_emopu,4),L_imopu,L_elopu,I_olopu)
      !}
C FDA_doz_logic.fgi( 244, 188):���������� ������� ���������,20FDA20TRAN06_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axipu,L_oxipu,R_ixipu
     &,
     & REAL(R_uxipu,4),L_abopu,L_uvipu,I_exipu)
      !}
C FDA_doz_logic.fgi( 270, 188):���������� ������� ���������,20FDA20TRAN06_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofopu,L_ekopu,R_akopu
     &,
     & REAL(R_ikopu,4),L_okopu,L_ifopu,I_ufopu)
      !}
C FDA_doz_logic.fgi( 294, 186):���������� ������� ���������,20FDA20TRAN06_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etipu,L_utipu,R_otipu
     &,
     & REAL(R_avipu,4),L_evipu,L_atipu,I_itipu)
      !}
C FDA_doz_logic.fgi( 244, 174):���������� ������� ���������,20FDA20TRAN06_L10
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_edelu,REAL(R_amelu
     &,4),
     & REAL(R_obilu,4),REAL(R_ubilu,4),REAL(R_evelu,4),R_udelu
     &,REAL(R_avelu,4),
     & L_(441),L_(440),R_edilu,R_idilu,R_ifilu,I_uvelu,R_odelu
     &,
     & R_efilu,R_idelu,I_axelu,I_uxelu,R_ukelu,
     & REAL(R_elelu,4),R_afelu,REAL(R_ifelu,4),L_ebelu,
     & L_abelu,I_epelu,I_apelu,I_ovelu,I_exelu,C20_emelu,C20_imelu
     &,C8_omelu,
     & L_ibelu,R_ofelu,REAL(R_akelu,4),L_ikelu,
     & R_ekelu,REAL(R_okelu,4),I_oxelu,L_ufelu,
     & L_(439),L_umelu,L_obelu,I_ixelu,I_ivelu,L_ilelu,L_ebilu
     &,L_odilu,
     & L_alelu,L_efelu,L_olelu,L_ulelu,L_adilu,L_opelu,
     & L_eselu,L_iselu,L_urelu,L_aselu,L_arelu,L_erelu,L_oselu
     &,L_uselu,L_atelu,L_etelu,
     & L_itelu,L_upelu,REAL(R8_axorad,8),REAL(1.0,4),R8_ipelu
     &,L_ibilu,I_abilu,
     & L_adelu,L_ubelu,L_udilu,L_afilu,L_irelu,L_otelu,L_utelu
     &,L_orelu)
      !}
C FDA_doz_logic.fgi(  28, 136):���������� ��������� ������� ����������� ���������,20FDA20TRAN07_C3
C label 1718  try1718=try1718-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_axeku,4),L_ifiku,L_ukupu
     &,R8_oseku,C30_aveku,
     & L_adepad,L_afepad,L_upepad,I_idiku,I_odiku,R_iveku
     &,R_eveku,
     & R_opeku,REAL(R_areku,4),R_ureku,
     & REAL(R_eseku,4),R_ereku,REAL(R_oreku,4),I_obiku,
     & I_udiku,I_ediku,I_adiku,L_iseku,L_afiku,L_ekiku,L_aseku
     &,
     & L_ireku,L_iteku,L_eteku,L_ofiku,L_upeku,L_uteku,
     & L_ukiku,L_efiku,L_oveku,L_uveku,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_ateku,L_useku,L_ikiku,R_ibiku,REAL(R_oteku,4),L_okiku
     &,L_aliku,
     & L_exeku,L_ixeku,L_oxeku,L_abiku,L_ebiku,L_uxeku)
      !}

      if(L_ebiku.or.L_abiku.or.L_uxeku.or.L_oxeku.or.L_ixeku.or.L_exeku
     &) then      
                  I_ubiku = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubiku = z'40000000'
      endif
C FDA_doz_logic.fgi( 148, 134):���� ���������� �������� ��������,20FDA20TRAN07_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ovuku,REAL(R_ifalu
     &,4),
     & REAL(R_avalu,4),REAL(R_evalu,4),REAL(R_oralu,4),R_exuku
     &,REAL(R_iralu,4),
     & L_(438),L_(437),R_ovalu,R_uvalu,R_uxalu,I_esalu,R_axuku
     &,
     & R_oxalu,R_uvuku,I_isalu,I_etalu,R_edalu,
     & REAL(R_odalu,4),R_ixuku,REAL(R_uxuku,4),L_otuku,
     & L_ituku,I_okalu,I_ikalu,I_asalu,I_osalu,C20_ofalu,C20_ufalu
     &,C8_akalu,
     & L_utuku,R_abalu,REAL(R_ibalu,4),L_ubalu,
     & R_obalu,REAL(R_adalu,4),I_atalu,L_ebalu,
     & L_(436),L_ekalu,L_avuku,I_usalu,I_uralu,L_udalu,L_otalu
     &,L_axalu,
     & L_idalu,L_oxuku,L_afalu,L_efalu,L_ivalu,L_alalu,
     & L_omalu,L_umalu,L_emalu,L_imalu,L_ilalu,L_olalu,L_apalu
     &,L_epalu,L_ipalu,L_opalu,
     & L_upalu,L_elalu,REAL(R8_axorad,8),REAL(1.0,4),R8_ukalu
     &,L_utalu,I_italu,
     & L_ivuku,L_evuku,L_exalu,L_ixalu,L_ulalu,L_aralu,L_eralu
     &,L_amalu)
      !}
C FDA_doz_logic.fgi(  48, 136):���������� ��������� ������� ����������� ���������,20FDA20TRAN07_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_asoku,REAL(R_uxoku
     &,4),
     & REAL(R_iruku,4),REAL(R_oruku,4),REAL(R_amuku,4),R_osoku
     &,REAL(R_uluku,4),
     & L_(435),L_(434),R_asuku,R_esuku,R_etuku,I_omuku,R_isoku
     &,
     & R_atuku,R_esoku,I_umuku,I_opuku,R_ovoku,
     & REAL(R_axoku,4),R_usoku,REAL(R_etoku,4),L_aroku,
     & L_upoku,I_aduku,I_ubuku,I_imuku,I_apuku,C20_abuku,C20_ebuku
     &,C8_ibuku,
     & L_eroku,R_itoku,REAL(R_utoku,4),L_evoku,
     & R_avoku,REAL(R_ivoku,4),I_ipuku,L_otoku,
     & L_(433),L_obuku,L_iroku,I_epuku,I_emuku,L_exoku,L_aruku
     &,L_isuku,
     & L_uvoku,L_atoku,L_ixoku,L_oxoku,L_uruku,L_iduku,
     & L_akuku,L_ekuku,L_ofuku,L_ufuku,L_uduku,L_afuku,L_ikuku
     &,L_okuku,L_ukuku,L_aluku,
     & L_eluku,L_oduku,REAL(R8_axorad,8),REAL(1.0,4),R8_eduku
     &,L_eruku,I_upuku,
     & L_uroku,L_oroku,L_osuku,L_usuku,L_efuku,L_iluku,L_oluku
     &,L_ifuku)
      !}
C FDA_doz_logic.fgi(  66, 136):���������� ��������� ������� ����������� ���������,20FDA20TRAN07_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_imiku,REAL(R_etiku
     &,4),
     & REAL(R_uloku,4),REAL(R_amoku,4),REAL(R_ifoku,4),R_apiku
     &,REAL(R_efoku,4),
     & L_(432),L_(431),R_imoku,R_omoku,R_opoku,I_akoku,R_umiku
     &,
     & R_ipoku,R_omiku,I_ekoku,I_aloku,R_asiku,
     & REAL(R_isiku,4),R_epiku,REAL(R_opiku,4),L_iliku,
     & L_eliku,I_iviku,I_eviku,I_ufoku,I_ikoku,C20_itiku,C20_otiku
     &,C8_utiku,
     & L_oliku,R_upiku,REAL(R_eriku,4),L_oriku,
     & R_iriku,REAL(R_uriku,4),I_ukoku,L_ariku,
     & L_(430),L_aviku,L_uliku,I_okoku,I_ofoku,L_osiku,L_iloku
     &,L_umoku,
     & L_esiku,L_ipiku,L_usiku,L_atiku,L_emoku,L_uviku,
     & L_iboku,L_oboku,L_aboku,L_eboku,L_exiku,L_ixiku,L_uboku
     &,L_adoku,L_edoku,L_idoku,
     & L_odoku,L_axiku,REAL(R8_axorad,8),REAL(1.0,4),R8_oviku
     &,L_oloku,I_eloku,
     & L_emiku,L_amiku,L_apoku,L_epoku,L_oxiku,L_udoku,L_afoku
     &,L_uxiku)
      !}
C FDA_doz_logic.fgi(  85, 136):���������� ��������� ������� ����������� ���������,20FDA20TRAN07_C6
      !{
      Call TRANSPORTER_DOZ(deltat,R_akeku,R_ideku,L_ameku
     &,
     & R_ekeku,R_odeku,L_emeku,R_ikeku,
     & R_udeku,L_imeku,R_okeku,R_afeku,
     & L_omeku,L_uraku,L_apaku,L_aviku,L_oleku,
     & L_ivufu,R_alaku,REAL(0,4),REAL(1000,4),L_ovufu,L_ilaku
     &,
     & R_ukeku,R_efeku,L_umeku,R_aleku,
     & R_ifeku,L_apeku,R_eleku,R_ofeku,
     & L_epeku,R_ileku,R_ufeku,L_ipeku,
     & L_obuku,L_ekalu,L_umelu,L_uleku,L_okaku,R_elaku,
     & REAL(12000,4),REAL(14000,4),L_ukaku,L_upaku,L_iliku
     &,L_emaku,L_ipaku,
     & L_esaku,L_ulaku,L_axufu,L_eliku,L_opaku,L_amaku,L_epaku
     &,L_asaku,
     & L_olaku,L_aroku,L_otuku,L_ebelu,L_osaku,L_ataku,L_itaku
     &,
     & L_exufu,L_etufu,L_itufu,L_ofaku,L_ifaku,L_axaku,L_evaku
     &,L_ivaku,L_uvaku,
     & L_avaku,L_ovaku,R_okupu,R_evufu,R_ikupu,REAL(R_ibeku
     &,4),
     & REAL(R_umaku,4),REAL(R_oraku,4),REAL(R_apiku,4),REAL
     &(R_osoku,4),
     & REAL(R_exuku,4),REAL(R_udelu,4),L_uxufu,L_akaku,L_abaku
     &,L_ebaku,L_ibaku,L_idaku,L_adaku,
     & L_udaku,L_ekaku,L_efaku,L_ufaku,L_oxufu,L_obaku,L_ubaku
     &,L_ikaku,L_edaku,
     & L_odaku,L_afaku,L_araku,L_oliku,L_utuku,L_ibelu,L_eroku
     &,
     & L_imaku,L_eraku,L_uliku,L_iroku,L_avuku,L_obelu,L_useku
     &,
     & L_ateku,L_upoku,L_ituku,L_abelu,L_obeku,L_ubeku,L_ixaku
     &,
     & L_oxaku,L_exaku,L_isaku,L_usaku,L_ixufu,L_etaku,L_ukupu
     &,L_otaku,
     & L_ifiku,L_edeku,L_utaku,L_adeku,L_abeku,L_uxaku)
      !}
C FDA_doz_logic.fgi(  29, 108):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN07
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idufu,L_afufu,R_udufu
     &,
     & REAL(R_efufu,4),L_ubaku,L_edufu,I_odufu)
      !}
C FDA_doz_logic.fgi(  94,  96):���������� ������� ���������,20FDA20TRAN07_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_urufu,L_isufu,R_esufu
     &,
     & REAL(R_osufu,4),L_edaku,L_orufu,I_asufu)
      !}
C FDA_doz_logic.fgi(  94, 110):���������� ������� ���������,20FDA20TRAN07_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akufu,L_okufu,R_ikufu
     &,
     & REAL(R_ukufu,4),L_odaku,L_ufufu,I_ekufu)
      !}
C FDA_doz_logic.fgi( 118, 110):���������� ������� ���������,20FDA20TRAN07_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epufu,L_upufu,R_opufu
     &,
     & REAL(R_arufu,4),L_afaku,L_apufu,I_ipufu)
      !}
C FDA_doz_logic.fgi( 144, 108):���������� ������� ���������,20FDA20TRAN07_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_olufu,L_emufu,R_amufu
     &,
     & REAL(R_imufu,4),L_ekaku,L_ilufu,I_ulufu)
      !}
C FDA_doz_logic.fgi( 144,  94):���������� ������� ���������,20FDA20TRAN07_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_uxudu,REAL(R_okafu
     &,4),
     & REAL(R_exafu,4),REAL(R_ixafu,4),REAL(R_usafu,4),R_ibafu
     &,REAL(R_osafu,4),
     & L_(429),L_(428),R_uxafu,R_abefu,R_adefu,I_itafu,R_ebafu
     &,
     & R_ubefu,R_abafu,I_otafu,I_ivafu,R_ifafu,
     & REAL(R_ufafu,4),R_obafu,REAL(R_adafu,4),L_uvudu,
     & L_ovudu,I_ulafu,I_olafu,I_etafu,I_utafu,C20_ukafu,C20_alafu
     &,C8_elafu,
     & L_axudu,R_edafu,REAL(R_odafu,4),L_afafu,
     & R_udafu,REAL(R_efafu,4),I_evafu,L_idafu,
     & L_(427),L_ilafu,L_exudu,I_avafu,I_atafu,L_akafu,L_uvafu
     &,L_ebefu,
     & L_ofafu,L_ubafu,L_ekafu,L_ikafu,L_oxafu,L_emafu,
     & L_upafu,L_arafu,L_ipafu,L_opafu,L_omafu,L_umafu,L_erafu
     &,L_irafu,L_orafu,L_urafu,
     & L_asafu,L_imafu,REAL(R8_axorad,8),REAL(1.0,4),R8_amafu
     &,L_axafu,I_ovafu,
     & L_oxudu,L_ixudu,L_ibefu,L_obefu,L_apafu,L_esafu,L_isafu
     &,L_epafu)
      !}
C FDA_doz_logic.fgi( 174, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN08_C3
C label 1729  try1729=try1729-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_otadu,4),L_adedu,L_akupu
     &,R8_eradu,C30_osadu,
     & L_adepad,L_afepad,L_upepad,I_abedu,I_ebedu,R_atadu
     &,R_usadu,
     & R_emadu,REAL(R_omadu,4),R_ipadu,
     & REAL(R_upadu,4),R_umadu,REAL(R_epadu,4),I_exadu,
     & I_ibedu,I_uxadu,I_oxadu,L_aradu,L_obedu,L_udedu,L_opadu
     &,
     & L_apadu,L_asadu,L_uradu,L_ededu,L_imadu,L_isadu,
     & L_ifedu,L_ubedu,L_etadu,L_itadu,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_oradu,L_iradu,L_afedu,R_axadu,REAL(R_esadu,4),L_efedu
     &,L_ofedu,
     & L_utadu,L_avadu,L_evadu,L_ovadu,L_uvadu,L_ivadu)
      !}

      if(L_uvadu.or.L_ovadu.or.L_ivadu.or.L_evadu.or.L_avadu.or.L_utadu
     &) then      
                  I_ixadu = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixadu = z'40000000'
      endif
C FDA_doz_logic.fgi( 294, 133):���� ���������� �������� ��������,20FDA20TRAN08_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_etodu,REAL(R_adudu
     &,4),
     & REAL(R_osudu,4),REAL(R_usudu,4),REAL(R_epudu,4),R_utodu
     &,REAL(R_apudu,4),
     & L_(426),L_(425),R_etudu,R_itudu,R_ivudu,I_upudu,R_otodu
     &,
     & R_evudu,R_itodu,I_arudu,I_urudu,R_uxodu,
     & REAL(R_ebudu,4),R_avodu,REAL(R_ivodu,4),L_esodu,
     & L_asodu,I_efudu,I_afudu,I_opudu,I_erudu,C20_edudu,C20_idudu
     &,C8_odudu,
     & L_isodu,R_ovodu,REAL(R_axodu,4),L_ixodu,
     & R_exodu,REAL(R_oxodu,4),I_orudu,L_uvodu,
     & L_(424),L_ududu,L_osodu,I_irudu,I_ipudu,L_ibudu,L_esudu
     &,L_otudu,
     & L_abudu,L_evodu,L_obudu,L_ubudu,L_atudu,L_ofudu,
     & L_eludu,L_iludu,L_ukudu,L_aludu,L_akudu,L_ekudu,L_oludu
     &,L_uludu,L_amudu,L_emudu,
     & L_imudu,L_ufudu,REAL(R8_axorad,8),REAL(1.0,4),R8_ifudu
     &,L_isudu,I_asudu,
     & L_atodu,L_usodu,L_utudu,L_avudu,L_ikudu,L_omudu,L_umudu
     &,L_okudu)
      !}
C FDA_doz_logic.fgi( 194, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN08_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_opidu,REAL(R_ividu
     &,4),
     & REAL(R_apodu,4),REAL(R_epodu,4),REAL(R_okodu,4),R_eridu
     &,REAL(R_ikodu,4),
     & L_(423),L_(422),R_opodu,R_upodu,R_urodu,I_elodu,R_aridu
     &,
     & R_orodu,R_upidu,I_ilodu,I_emodu,R_etidu,
     & REAL(R_otidu,4),R_iridu,REAL(R_uridu,4),L_omidu,
     & L_imidu,I_oxidu,I_ixidu,I_alodu,I_olodu,C20_ovidu,C20_uvidu
     &,C8_axidu,
     & L_umidu,R_asidu,REAL(R_isidu,4),L_usidu,
     & R_osidu,REAL(R_atidu,4),I_amodu,L_esidu,
     & L_(421),L_exidu,L_apidu,I_ulodu,I_ukodu,L_utidu,L_omodu
     &,L_arodu,
     & L_itidu,L_oridu,L_avidu,L_evidu,L_ipodu,L_abodu,
     & L_ododu,L_udodu,L_edodu,L_idodu,L_ibodu,L_obodu,L_afodu
     &,L_efodu,L_ifodu,L_ofodu,
     & L_ufodu,L_ebodu,REAL(R8_axorad,8),REAL(1.0,4),R8_uxidu
     &,L_umodu,I_imodu,
     & L_ipidu,L_epidu,L_erodu,L_irodu,L_ubodu,L_akodu,L_ekodu
     &,L_adodu)
      !}
C FDA_doz_logic.fgi( 212, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN08_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_aledu,REAL(R_uredu
     &,4),
     & REAL(R_ikidu,4),REAL(R_okidu,4),REAL(R_adidu,4),R_oledu
     &,REAL(R_ubidu,4),
     & L_(420),L_(419),R_alidu,R_elidu,R_emidu,I_odidu,R_iledu
     &,
     & R_amidu,R_eledu,I_udidu,I_ofidu,R_opedu,
     & REAL(R_aredu,4),R_uledu,REAL(R_emedu,4),L_akedu,
     & L_ufedu,I_atedu,I_usedu,I_ididu,I_afidu,C20_asedu,C20_esedu
     &,C8_isedu,
     & L_ekedu,R_imedu,REAL(R_umedu,4),L_epedu,
     & R_apedu,REAL(R_ipedu,4),I_ifidu,L_omedu,
     & L_(418),L_osedu,L_ikedu,I_efidu,I_edidu,L_eredu,L_akidu
     &,L_ilidu,
     & L_upedu,L_amedu,L_iredu,L_oredu,L_ukidu,L_itedu,
     & L_axedu,L_exedu,L_ovedu,L_uvedu,L_utedu,L_avedu,L_ixedu
     &,L_oxedu,L_uxedu,L_abidu,
     & L_ebidu,L_otedu,REAL(R8_axorad,8),REAL(1.0,4),R8_etedu
     &,L_ekidu,I_ufidu,
     & L_ukedu,L_okedu,L_olidu,L_ulidu,L_evedu,L_ibidu,L_obidu
     &,L_ivedu)
      !}
C FDA_doz_logic.fgi( 231, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN08_C6
      !{
      Call TRANSPORTER_DOZ(deltat,R_odadu,R_abadu,L_okadu
     &,
     & R_udadu,R_ebadu,L_ukadu,R_afadu,
     & R_ibadu,L_aladu,R_efadu,R_obadu,
     & L_eladu,L_ipubu,L_olubu,L_osedu,L_ekadu,
     & L_atobu,R_ofubu,REAL(0,4),REAL(1000,4),L_etobu,L_akubu
     &,
     & R_ifadu,R_ubadu,L_iladu,R_ofadu,
     & R_adadu,L_oladu,R_ufadu,R_edadu,
     & L_uladu,R_akadu,R_idadu,L_amadu,
     & L_exidu,L_ududu,L_ilafu,L_ikadu,L_efubu,R_ufubu,
     & REAL(14000,4),REAL(16000,4),L_ifubu,L_imubu,L_akedu
     &,L_ukubu,L_amubu,
     & L_upubu,L_ikubu,L_otobu,L_ufedu,L_emubu,L_okubu,L_ulubu
     &,L_opubu,
     & L_ekubu,L_omidu,L_esodu,L_uvudu,L_erubu,L_orubu,L_asubu
     &,
     & L_utobu,L_urobu,L_asobu,L_edubu,L_adubu,L_otubu,L_usubu
     &,L_atubu,L_itubu,
     & L_osubu,L_etubu,R_ufupu,R_usobu,R_ofupu,REAL(R_axubu
     &,4),
     & REAL(R_ilubu,4),REAL(R_epubu,4),REAL(R_oledu,4),REAL
     &(R_eridu,4),
     & REAL(R_utodu,4),REAL(R_ibafu,4),L_ivobu,L_odubu,L_ovobu
     &,L_uvobu,L_axobu,L_abubu,L_oxobu,
     & L_ibubu,L_udubu,L_ububu,L_idubu,L_evobu,L_exobu,L_ixobu
     &,L_afubu,L_uxobu,
     & L_ebubu,L_obubu,L_omubu,L_ekedu,L_isodu,L_axudu,L_umidu
     &,
     & L_alubu,L_umubu,L_ikedu,L_apidu,L_osodu,L_exudu,L_iradu
     &,
     & L_oradu,L_imidu,L_asodu,L_ovudu,L_exubu,L_ixubu,L_avubu
     &,
     & L_evubu,L_utubu,L_arubu,L_irubu,L_avobu,L_urubu,L_akupu
     &,L_esubu,
     & L_adedu,L_uxubu,L_isubu,L_oxubu,L_ovubu,L_ivubu)
      !}
C FDA_doz_logic.fgi( 175, 107):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN08
      !{
      Call DAT_DISCR_HANDLER(deltat,I_abobu,L_obobu,R_ibobu
     &,
     & REAL(R_ubobu,4),L_ixobu,L_uxibu,I_ebobu)
      !}
C FDA_doz_logic.fgi( 240,  96):���������� ������� ���������,20FDA20TRAN08_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ipobu,L_arobu,R_upobu
     &,
     & REAL(R_erobu,4),L_uxobu,L_epobu,I_opobu)
      !}
C FDA_doz_logic.fgi( 240, 108):���������� ������� ���������,20FDA20TRAN08_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_odobu,L_efobu,R_afobu
     &,
     & REAL(R_ifobu,4),L_ebubu,L_idobu,I_udobu)
      !}
C FDA_doz_logic.fgi( 264, 108):���������� ������� ���������,20FDA20TRAN08_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ulobu,L_imobu,R_emobu
     &,
     & REAL(R_omobu,4),L_obubu,L_olobu,I_amobu)
      !}
C FDA_doz_logic.fgi( 290, 108):���������� ������� ���������,20FDA20TRAN08_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekobu,L_ukobu,R_okobu
     &,
     & REAL(R_alobu,4),L_udubu,L_akobu,I_ikobu)
      !}
C FDA_doz_logic.fgi( 290,  92):���������� ������� ���������,20FDA20TRAN08_L1
      !{
      Call TRANSPORTER_DOZ(deltat,R_ebixo,R_ovexo,L_efixo
     &,
     & R_ibixo,R_uvexo,L_ifixo,R_obixo,
     & R_axexo,L_ofixo,R_ubixo,R_exexo,
     & L_ufixo,L_amexo,L_ekexo,L_eroxo,L_udixo,
     & L_oraxo,R_edexo,REAL(0,4),REAL(1000,4),L_uraxo,L_odexo
     &,
     & R_adixo,R_ixexo,L_akixo,R_edixo,
     & R_oxexo,L_ekixo,R_idixo,R_uxexo,
     & L_ikixo,R_odixo,R_abixo,L_okixo,
     & L_utuxo,L_ibebu,L_akibu,L_afixo,L_ubexo,R_idexo,
     & REAL(16000,4),REAL(18000,4),L_adexo,L_alexo,L_odoxo
     &,L_ifexo,L_okexo,
     & L_imexo,L_afexo,L_esaxo,L_idoxo,L_ukexo,L_efexo,L_ikexo
     &,L_emexo,
     & L_udexo,L_eluxo,L_upabu,L_itebu,L_umexo,L_epexo,L_opexo
     &,
     & L_isaxo,L_ipaxo,L_opaxo,L_uxaxo,L_oxaxo,L_esexo,L_irexo
     &,L_orexo,L_asexo,
     & L_erexo,L_urexo,R_afupu,R_iraxo,R_udupu,REAL(R_otexo
     &,4),
     & REAL(R_akexo,4),REAL(R_ulexo,4),REAL(R_ekoxo,4),REAL
     &(R_umuxo,4),
     & REAL(R_isabu,4),REAL(R_axebu,4),L_ataxo,L_ebexo,L_etaxo
     &,L_itaxo,L_otaxo,L_ovaxo,L_evaxo,
     & L_axaxo,L_ibexo,L_ixaxo,L_abexo,L_usaxo,L_utaxo,L_avaxo
     &,L_obexo,L_ivaxo,
     & L_uvaxo,L_exaxo,L_elexo,L_udoxo,L_arabu,L_otebu,L_iluxo
     &,
     & L_ofexo,L_ilexo,L_afoxo,L_oluxo,L_erabu,L_utebu,L_apixo
     &,
     & L_epixo,L_aluxo,L_opabu,L_etebu,L_utexo,L_avexo,L_osexo
     &,
     & L_usexo,L_isexo,L_omexo,L_apexo,L_osaxo,L_ipexo,L_efupu
     &,L_upexo,
     & L_oxixo,L_ivexo,L_arexo,L_evexo,L_etexo,L_atexo)
      !}
C FDA_doz_logic.fgi( 320, 107):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN09
C label 1740  try1740=try1740-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_esixo,4),L_oxixo,L_efupu
     &,R8_umixo,C30_erixo,
     & L_adepad,L_afepad,L_upepad,I_ovixo,I_uvixo,R_orixo
     &,R_irixo,
     & R_ukixo,REAL(R_elixo,4),R_amixo,
     & REAL(R_imixo,4),R_ilixo,REAL(R_ulixo,4),I_utixo,
     & I_axixo,I_ivixo,I_evixo,L_omixo,L_exixo,L_iboxo,L_emixo
     &,
     & L_olixo,L_opixo,L_ipixo,L_uxixo,L_alixo,L_arixo,
     & L_adoxo,L_ixixo,L_urixo,L_asixo,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_epixo,L_apixo,L_oboxo,R_otixo,REAL(R_upixo,4),L_uboxo
     &,L_edoxo,
     & L_isixo,L_osixo,L_usixo,L_etixo,L_itixo,L_atixo)
      !}

      if(L_itixo.or.L_etixo.or.L_atixo.or.L_usixo.or.L_osixo.or.L_isixo
     &) then      
                  I_avixo = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avixo = z'40000000'
      endif
C FDA_doz_logic.fgi( 439, 133):���� ���������� �������� ��������,20FDA20TRAN09_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ivebu,REAL(R_efibu
     &,4),
     & REAL(R_utibu,4),REAL(R_avibu,4),REAL(R_iribu,4),R_axebu
     &,REAL(R_eribu,4),
     & L_(417),L_(416),R_ivibu,R_ovibu,R_oxibu,I_asibu,R_uvebu
     &,
     & R_ixibu,R_ovebu,I_esibu,I_atibu,R_adibu,
     & REAL(R_idibu,4),R_exebu,REAL(R_oxebu,4),L_itebu,
     & L_etebu,I_ikibu,I_ekibu,I_uribu,I_isibu,C20_ifibu,C20_ofibu
     &,C8_ufibu,
     & L_otebu,R_uxebu,REAL(R_ebibu,4),L_obibu,
     & R_ibibu,REAL(R_ubibu,4),I_usibu,L_abibu,
     & L_(415),L_akibu,L_utebu,I_osibu,I_oribu,L_odibu,L_itibu
     &,L_uvibu,
     & L_edibu,L_ixebu,L_udibu,L_afibu,L_evibu,L_ukibu,
     & L_imibu,L_omibu,L_amibu,L_emibu,L_elibu,L_ilibu,L_umibu
     &,L_apibu,L_epibu,L_ipibu,
     & L_opibu,L_alibu,REAL(R8_axorad,8),REAL(1.0,4),R8_okibu
     &,L_otibu,I_etibu,
     & L_evebu,L_avebu,L_axibu,L_exibu,L_olibu,L_upibu,L_aribu
     &,L_ulibu)
      !}
C FDA_doz_logic.fgi( 325, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN09_C3
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_urabu,REAL(R_oxabu
     &,4),
     & REAL(R_erebu,4),REAL(R_irebu,4),REAL(R_ulebu,4),R_isabu
     &,REAL(R_olebu,4),
     & L_(414),L_(413),R_urebu,R_asebu,R_atebu,I_imebu,R_esabu
     &,
     & R_usebu,R_asabu,I_omebu,I_ipebu,R_ivabu,
     & REAL(R_uvabu,4),R_osabu,REAL(R_atabu,4),L_upabu,
     & L_opabu,I_ubebu,I_obebu,I_emebu,I_umebu,C20_uxabu,C20_abebu
     &,C8_ebebu,
     & L_arabu,R_etabu,REAL(R_otabu,4),L_avabu,
     & R_utabu,REAL(R_evabu,4),I_epebu,L_itabu,
     & L_(412),L_ibebu,L_erabu,I_apebu,I_amebu,L_axabu,L_upebu
     &,L_esebu,
     & L_ovabu,L_usabu,L_exabu,L_ixabu,L_orebu,L_edebu,
     & L_ufebu,L_akebu,L_ifebu,L_ofebu,L_odebu,L_udebu,L_ekebu
     &,L_ikebu,L_okebu,L_ukebu,
     & L_alebu,L_idebu,REAL(R8_axorad,8),REAL(1.0,4),R8_adebu
     &,L_arebu,I_opebu,
     & L_orabu,L_irabu,L_isebu,L_osebu,L_afebu,L_elebu,L_ilebu
     &,L_efebu)
      !}
C FDA_doz_logic.fgi( 344, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN09_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_emuxo,REAL(R_atuxo
     &,4),
     & REAL(R_olabu,4),REAL(R_ulabu,4),REAL(R_efabu,4),R_umuxo
     &,REAL(R_afabu,4),
     & L_(411),L_(410),R_emabu,R_imabu,R_ipabu,I_ufabu,R_omuxo
     &,
     & R_epabu,R_imuxo,I_akabu,I_ukabu,R_uruxo,
     & REAL(R_esuxo,4),R_apuxo,REAL(R_ipuxo,4),L_eluxo,
     & L_aluxo,I_evuxo,I_avuxo,I_ofabu,I_ekabu,C20_etuxo,C20_ituxo
     &,C8_otuxo,
     & L_iluxo,R_opuxo,REAL(R_aruxo,4),L_iruxo,
     & R_eruxo,REAL(R_oruxo,4),I_okabu,L_upuxo,
     & L_(409),L_utuxo,L_oluxo,I_ikabu,I_ifabu,L_isuxo,L_elabu
     &,L_omabu,
     & L_asuxo,L_epuxo,L_osuxo,L_usuxo,L_amabu,L_ovuxo,
     & L_ebabu,L_ibabu,L_uxuxo,L_ababu,L_axuxo,L_exuxo,L_obabu
     &,L_ubabu,L_adabu,L_edabu,
     & L_idabu,L_uvuxo,REAL(R8_axorad,8),REAL(1.0,4),R8_ivuxo
     &,L_ilabu,I_alabu,
     & L_amuxo,L_uluxo,L_umabu,L_apabu,L_ixuxo,L_odabu,L_udabu
     &,L_oxuxo)
      !}
C FDA_doz_logic.fgi( 362, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN09_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ofoxo,REAL(R_ipoxo
     &,4),
     & REAL(R_afuxo,4),REAL(R_efuxo,4),REAL(R_oxoxo,4),R_ekoxo
     &,REAL(R_ixoxo,4),
     & L_(408),L_(407),R_ofuxo,R_ufuxo,R_ukuxo,I_ebuxo,R_akoxo
     &,
     & R_okuxo,R_ufoxo,I_ibuxo,I_eduxo,R_emoxo,
     & REAL(R_omoxo,4),R_ikoxo,REAL(R_ukoxo,4),L_odoxo,
     & L_idoxo,I_oroxo,I_iroxo,I_abuxo,I_obuxo,C20_opoxo,C20_upoxo
     &,C8_aroxo,
     & L_udoxo,R_aloxo,REAL(R_iloxo,4),L_uloxo,
     & R_oloxo,REAL(R_amoxo,4),I_aduxo,L_eloxo,
     & L_(406),L_eroxo,L_afoxo,I_ubuxo,I_uxoxo,L_umoxo,L_oduxo
     &,L_akuxo,
     & L_imoxo,L_okoxo,L_apoxo,L_epoxo,L_ifuxo,L_asoxo,
     & L_otoxo,L_utoxo,L_etoxo,L_itoxo,L_isoxo,L_osoxo,L_avoxo
     &,L_evoxo,L_ivoxo,L_ovoxo,
     & L_uvoxo,L_esoxo,REAL(R8_axorad,8),REAL(1.0,4),R8_uroxo
     &,L_uduxo,I_iduxo,
     & L_ifoxo,L_efoxo,L_ekuxo,L_ikuxo,L_usoxo,L_axoxo,L_exoxo
     &,L_atoxo)
      !}
C FDA_doz_logic.fgi( 395, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN09_C6
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovuvo,L_exuvo,R_axuvo
     &,
     & REAL(R_ixuvo,4),L_avaxo,L_ivuvo,I_uvuvo)
      !}
C FDA_doz_logic.fgi( 384,  96):���������� ������� ���������,20FDA20TRAN09_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amaxo,L_omaxo,R_imaxo
     &,
     & REAL(R_umaxo,4),L_ivaxo,L_ulaxo,I_emaxo)
      !}
C FDA_doz_logic.fgi( 384, 108):���������� ������� ���������,20FDA20TRAN09_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebaxo,L_ubaxo,R_obaxo
     &,
     & REAL(R_adaxo,4),L_uvaxo,L_abaxo,I_ibaxo)
      !}
C FDA_doz_logic.fgi( 410, 108):���������� ������� ���������,20FDA20TRAN09_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ikaxo,L_alaxo,R_ukaxo
     &,
     & REAL(R_elaxo,4),L_exaxo,L_ekaxo,I_okaxo)
      !}
C FDA_doz_logic.fgi( 434, 108):���������� ������� ���������,20FDA20TRAN09_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_udaxo,L_ifaxo,R_efaxo
     &,
     & REAL(R_ofaxo,4),L_ibexo,L_odaxo,I_afaxo)
      !}
C FDA_doz_logic.fgi( 434,  92):���������� ������� ���������,20FDA20TRAN09_L1
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_atovo,REAL(R_ubuvo
     &,4),
     & REAL(R_isuvo,4),REAL(R_osuvo,4),REAL(R_apuvo,4),R_otovo
     &,REAL(R_umuvo,4),
     & L_(405),L_(404),R_atuvo,R_etuvo,R_evuvo,I_opuvo,R_itovo
     &,
     & R_avuvo,R_etovo,I_upuvo,I_oruvo,R_oxovo,
     & REAL(R_abuvo,4),R_utovo,REAL(R_evovo,4),L_asovo,
     & L_urovo,I_afuvo,I_uduvo,I_ipuvo,I_aruvo,C20_aduvo,C20_eduvo
     &,C8_iduvo,
     & L_esovo,R_ivovo,REAL(R_uvovo,4),L_exovo,
     & R_axovo,REAL(R_ixovo,4),I_iruvo,L_ovovo,
     & L_(403),L_oduvo,L_isovo,I_eruvo,I_epuvo,L_ebuvo,L_asuvo
     &,L_ituvo,
     & L_uxovo,L_avovo,L_ibuvo,L_obuvo,L_usuvo,L_ifuvo,
     & L_aluvo,L_eluvo,L_okuvo,L_ukuvo,L_ufuvo,L_akuvo,L_iluvo
     &,L_oluvo,L_uluvo,L_amuvo,
     & L_emuvo,L_ofuvo,REAL(R8_axorad,8),REAL(1.0,4),R8_efuvo
     &,L_esuvo,I_uruvo,
     & L_usovo,L_osovo,L_otuvo,L_utuvo,L_ekuvo,L_imuvo,L_omuvo
     &,L_ikuvo)
      !}
C FDA_doz_logic.fgi( 467, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN10_C3
C label 1751  try1751=try1751-1
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uputo,4),L_evuto,L_idupu
     &,R8_iluto,C30_umuto,
     & L_adepad,L_afepad,L_upepad,I_etuto,I_ituto,R_eputo
     &,R_aputo,
     & R_ifuto,REAL(R_ufuto,4),R_okuto,
     & REAL(R_aluto,4),R_akuto,REAL(R_ikuto,4),I_isuto,
     & I_otuto,I_atuto,I_usuto,L_eluto,L_ututo,L_axuto,L_ukuto
     &,
     & L_ekuto,L_emuto,L_amuto,L_ivuto,L_ofuto,L_omuto,
     & L_oxuto,L_avuto,L_iputo,L_oputo,REAL(R8_axorad,8),REAL
     &(1.0,4),R8_udepad,
     & L_uluto,L_oluto,L_exuto,R_esuto,REAL(R_imuto,4),L_ixuto
     &,L_uxuto,
     & L_aruto,L_eruto,L_iruto,L_uruto,L_asuto,L_oruto)
      !}

      if(L_asuto.or.L_uruto.or.L_oruto.or.L_iruto.or.L_eruto.or.L_aruto
     &) then      
                  I_osuto = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osuto = z'40000000'
      endif
C FDA_doz_logic.fgi( 587, 133):���� ���������� �������� ��������,20FDA20TRAN10_C2
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ipivo,REAL(R_evivo
     &,4),
     & REAL(R_umovo,4),REAL(R_apovo,4),REAL(R_ikovo,4),R_arivo
     &,REAL(R_ekovo,4),
     & L_(402),L_(401),R_ipovo,R_opovo,R_orovo,I_alovo,R_upivo
     &,
     & R_irovo,R_opivo,I_elovo,I_amovo,R_ativo,
     & REAL(R_itivo,4),R_erivo,REAL(R_orivo,4),L_imivo,
     & L_emivo,I_ixivo,I_exivo,I_ukovo,I_ilovo,C20_ivivo,C20_ovivo
     &,C8_uvivo,
     & L_omivo,R_urivo,REAL(R_esivo,4),L_osivo,
     & R_isivo,REAL(R_usivo,4),I_ulovo,L_asivo,
     & L_(400),L_axivo,L_umivo,I_olovo,I_okovo,L_otivo,L_imovo
     &,L_upovo,
     & L_etivo,L_irivo,L_utivo,L_avivo,L_epovo,L_uxivo,
     & L_idovo,L_odovo,L_adovo,L_edovo,L_ebovo,L_ibovo,L_udovo
     &,L_afovo,L_efovo,L_ifovo,
     & L_ofovo,L_abovo,REAL(R8_axorad,8),REAL(1.0,4),R8_oxivo
     &,L_omovo,I_emovo,
     & L_epivo,L_apivo,L_arovo,L_erovo,L_obovo,L_ufovo,L_akovo
     &,L_ubovo)
      !}
C FDA_doz_logic.fgi( 487, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN10_C4
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_ukevo,REAL(R_orevo
     &,4),
     & REAL(R_ekivo,4),REAL(R_ikivo,4),REAL(R_ubivo,4),R_ilevo
     &,REAL(R_obivo,4),
     & L_(399),L_(398),R_ukivo,R_alivo,R_amivo,I_idivo,R_elevo
     &,
     & R_ulivo,R_alevo,I_odivo,I_ifivo,R_ipevo,
     & REAL(R_upevo,4),R_olevo,REAL(R_amevo,4),L_ufevo,
     & L_ofevo,I_usevo,I_osevo,I_edivo,I_udivo,C20_urevo,C20_asevo
     &,C8_esevo,
     & L_akevo,R_emevo,REAL(R_omevo,4),L_apevo,
     & R_umevo,REAL(R_epevo,4),I_efivo,L_imevo,
     & L_(397),L_isevo,L_ekevo,I_afivo,I_adivo,L_arevo,L_ufivo
     &,L_elivo,
     & L_opevo,L_ulevo,L_erevo,L_irevo,L_okivo,L_etevo,
     & L_uvevo,L_axevo,L_ivevo,L_ovevo,L_otevo,L_utevo,L_exevo
     &,L_ixevo,L_oxevo,L_uxevo,
     & L_abivo,L_itevo,REAL(R8_axorad,8),REAL(1.0,4),R8_atevo
     &,L_akivo,I_ofivo,
     & L_okevo,L_ikevo,L_ilivo,L_olivo,L_avevo,L_ebivo,L_ibivo
     &,L_evevo)
      !}
C FDA_doz_logic.fgi( 505, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN10_C5
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_edavo,REAL(R_amavo
     &,4),
     & REAL(R_obevo,4),REAL(R_ubevo,4),REAL(R_evavo,4),R_udavo
     &,REAL(R_avavo,4),
     & L_(396),L_(395),R_edevo,R_idevo,R_ifevo,I_uvavo,R_odavo
     &,
     & R_efevo,R_idavo,I_axavo,I_uxavo,R_ukavo,
     & REAL(R_elavo,4),R_afavo,REAL(R_ifavo,4),L_ebavo,
     & L_abavo,I_epavo,I_apavo,I_ovavo,I_exavo,C20_emavo,C20_imavo
     &,C8_omavo,
     & L_ibavo,R_ofavo,REAL(R_akavo,4),L_ikavo,
     & R_ekavo,REAL(R_okavo,4),I_oxavo,L_ufavo,
     & L_(394),L_umavo,L_obavo,I_ixavo,I_ivavo,L_ilavo,L_ebevo
     &,L_odevo,
     & L_alavo,L_efavo,L_olavo,L_ulavo,L_adevo,L_opavo,
     & L_esavo,L_isavo,L_uravo,L_asavo,L_aravo,L_eravo,L_osavo
     &,L_usavo,L_atavo,L_etavo,
     & L_itavo,L_upavo,REAL(R8_axorad,8),REAL(1.0,4),R8_ipavo
     &,L_ibevo,I_abevo,
     & L_adavo,L_ubavo,L_udevo,L_afevo,L_iravo,L_otavo,L_utavo
     &,L_oravo)
      !}
C FDA_doz_logic.fgi( 524, 135):���������� ��������� ������� ����������� ���������,20FDA20TRAN10_C6
      !{
      Call TRANSPORTER_DOZ(deltat,R_uvoto,R_etoto,L_ubuto
     &,
     & R_axoto,R_itoto,L_aduto,R_exoto,
     & R_ototo,L_eduto,R_ixoto,R_utoto,
     & L_iduto,L_okoto,L_udoto,L_umavo,L_ibuto,
     & L_epito,R_uxito,REAL(0,4),REAL(1000,4),L_ipito,L_eboto
     &,
     & R_oxoto,R_avoto,L_oduto,R_uxoto,
     & R_evoto,L_uduto,R_abuto,R_ivoto,
     & L_afuto,R_ebuto,R_ovoto,L_efuto,
     & L_isevo,L_axivo,L_oduvo,L_obuto,L_ixito,R_aboto,
     & REAL(18000,4),REAL(20000,4),L_oxito,L_ofoto,L_ebavo
     &,L_adoto,L_efoto,
     & L_aloto,L_oboto,L_upito,L_abavo,L_ifoto,L_uboto,L_afoto
     &,L_ukoto,
     & L_iboto,L_ufevo,L_imivo,L_asovo,L_iloto,L_uloto,L_emoto
     &,
     & L_arito,L_amito,L_emito,L_ivito,L_evito,L_upoto,L_apoto
     &,L_epoto,L_opoto,
     & L_umoto,L_ipoto,R_edupu,R_apito,R_adupu,REAL(R_esoto
     &,4),
     & REAL(R_odoto,4),REAL(R_ikoto,4),REAL(R_udavo,4),REAL
     &(R_ilevo,4),
     & REAL(R_arivo,4),REAL(R_otovo,4),L_orito,L_uvito,L_urito
     &,L_asito,L_esito,L_etito,L_usito,
     & L_otito,L_axito,L_avito,L_ovito,L_irito,L_isito,L_osito
     &,L_exito,L_atito,
     & L_itito,L_utito,L_ufoto,L_ibavo,L_omivo,L_esovo,L_akevo
     &,
     & L_edoto,L_akoto,L_obavo,L_ekevo,L_umivo,L_isovo,L_oluto
     &,
     & L_uluto,L_ofevo,L_emivo,L_urovo,L_isoto,L_osoto,L_eroto
     &,
     & L_iroto,L_aroto,L_eloto,L_oloto,L_erito,L_amoto,L_idupu
     &,L_imoto,
     & L_evuto,L_atoto,L_omoto,L_usoto,L_uroto,L_oroto)
      !}
C FDA_doz_logic.fgi( 468, 107):���������� ������������ ��� ������� ����������� ���������,20FDA20TRAN10
      Call KONTEYNER_DOZ(deltat,R_oxopo,R_olopo,L_okupo,L_axopo
     &,
     & L_exopo,L_amopo,L_emopo,L_omopo,L_umopo,L_epopo,
     & L_ipopo,L_upopo,L_aropo,L_iropo,L_oropo,L_asopo,
     & L_esopo,L_osopo,L_usopo,L_etopo,L_itopo,L_utopo,
     & L_avopo,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_alopo,R_ilopo,R_ulopo,
     & R_ikupo,L_elopo,L_ixupo,L_ukopo,
     & L_epupu,R_otupo,R_utupo,R_avupo,L_ovopo,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_ixipo,L_edupo,L_apupu,
     & R_evupo,R_ivupo,R_ofupo,R_ufupo,R_ekupo,
     & REAL(60,4),R_akupo,R_ubopo,R_afupo,R_efupo,
     & R_ifupo,R_abopo,R_ebopo,R_ibopo,R_obopo,L_ufopo,L_adopo
     &,
     & L_edopo,L_idopo,L_odopo,L_udopo,L_afopo,L_efopo,L_ifopo
     &,
     & L_ofopo,R_exupo,R_etipo,L_ixopo,
     & REAL(R_odupu,4),L_oxipo,L_idupo,R_uvupo,R_axupo,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_imopo,
     & REAL(R_ifupu,4),L_utipo,L_uxopo,R_ukupo,R_alupo,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_apopo,
     & REAL(R_ekupu,4),L_avipo,L_abupo,R_elupo,R_ilupo,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_opopo,
     & REAL(R_alupu,4),L_evipo,L_ebupo,R_olupo,R_ulupo,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_eropo,
     & REAL(R_ulupu,4),L_ivipo,L_ibupo,R_amupo,R_emupo,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_uropo,
     & REAL(R_imupu,4),L_ovipo,L_obupo,R_imupo,R_omupo,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_isopo,
     & REAL(R_uxovu,4),L_uvipo,L_udupo,L_akopo,R_ipupo,R_opupo
     &,
     & R_umupo,R_apupo,R_epupo,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_atopo,REAL(R_avosu,4),L_axipo,
     & L_ubupo,L_ekopo,R_irupo,R_orupo,R_upupo,R_arupo,R_erupo
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_otopo,
     & REAL(R_odubad,4),L_exipo,L_adupo,L_ikopo,R_isupo,
     & R_osupo,R_urupo,R_asupo,R_esupo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_uvopo,
     & REAL(R_avekad,4),L_uxipo,L_odupo,L_okopo,R_itupo,
     & R_ovupo,R_usupo,R_atupo,R_etupo,R_otipo,
     & REAL(R_ixofad,4),R_atipo,REAL(R_uxofad,4),R_itipo)
C FDA_doz_logic.fgi(  63,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT03
      Call KONTEYNER_DOZ(deltat,R_ofaru,R_orupu,L_oparu,L_afaru
     &,
     & L_efaru,L_asupu,L_esupu,L_osupu,L_usupu,L_etupu,
     & L_itupu,L_utupu,L_avupu,L_ivupu,L_ovupu,L_axupu,
     & L_exupu,L_oxupu,L_uxupu,L_ebaru,L_ibaru,L_ubaru,
     & L_adaru,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_arupu,R_irupu,R_urupu,
     & R_iparu,L_erupu,L_iferu,L_opupu,
     & L_epupu,R_oberu,R_uberu,R_aderu,L_odaru,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_isopu,L_elaru,L_apupu,
     & R_ederu,R_ideru,R_omaru,R_umaru,R_eparu,
     & REAL(60,4),R_aparu,R_utopu,R_amaru,R_emaru,
     & R_imaru,R_atopu,R_etopu,R_itopu,R_otopu,L_uxopu,L_avopu
     &,
     & L_evopu,L_ivopu,L_ovopu,L_uvopu,L_axopu,L_exopu,L_ixopu
     &,
     & L_oxopu,R_eferu,R_epopu,L_ifaru,
     & REAL(R_odupu,4),L_osopu,L_ilaru,R_uderu,R_aferu,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_isupu,
     & REAL(R_ifupu,4),L_upopu,L_ufaru,R_uparu,R_araru,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_atupu,
     & REAL(R_ekupu,4),L_aropu,L_akaru,R_eraru,R_iraru,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_otupu,
     & REAL(R_alupu,4),L_eropu,L_ekaru,R_oraru,R_uraru,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_evupu,
     & REAL(R_ulupu,4),L_iropu,L_ikaru,R_asaru,R_esaru,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_uvupu,
     & REAL(R_imupu,4),L_oropu,L_okaru,R_isaru,R_osaru,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_ixupu,
     & REAL(R_uxovu,4),L_uropu,L_ularu,L_abupu,R_itaru,R_otaru
     &,
     & R_usaru,R_ataru,R_etaru,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_abaru,REAL(R_avosu,4),L_asopu,
     & L_ukaru,L_ebupu,R_ivaru,R_ovaru,R_utaru,R_avaru,R_evaru
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_obaru,
     & REAL(R_odubad,4),L_esopu,L_alaru,L_ibupu,R_ixaru,
     & R_oxaru,R_uvaru,R_axaru,R_exaru,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_udaru,
     & REAL(R_avekad,4),L_usopu,L_olaru,L_obupu,R_iberu,
     & R_oderu,R_uxaru,R_aberu,R_eberu,R_opopu,
     & REAL(R_ixofad,4),R_apopu,REAL(R_uxofad,4),R_ipopu)
C FDA_doz_logic.fgi(  25,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT01
      Call KONTEYNER_DOZ(deltat,R_osudo,R_odudo,L_obafo,L_asudo
     &,
     & L_esudo,L_afudo,L_efudo,L_ofudo,L_ufudo,L_ekudo,
     & L_ikudo,L_ukudo,L_aludo,L_iludo,L_oludo,L_amudo,
     & L_emudo,L_omudo,L_umudo,L_epudo,L_ipudo,L_upudo,
     & L_arudo,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_adudo,R_idudo,R_ududo,
     & R_ibafo,L_edudo,L_isafo,L_ubudo,
     & L_epupu,R_opafo,R_upafo,R_arafo,L_orudo,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_isodo,L_evudo,L_apupu,
     & R_erafo,R_irafo,R_oxudo,R_uxudo,R_ebafo,
     & REAL(60,4),R_abafo,R_utodo,R_axudo,R_exudo,
     & R_ixudo,R_atodo,R_etodo,R_itodo,R_otodo,L_uxodo,L_avodo
     &,
     & L_evodo,L_ivodo,L_ovodo,L_uvodo,L_axodo,L_exodo,L_ixodo
     &,
     & L_oxodo,R_esafo,R_epodo,L_isudo,
     & REAL(R_odupu,4),L_osodo,L_ivudo,R_urafo,R_asafo,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_ifudo,
     & REAL(R_ifupu,4),L_upodo,L_usudo,R_ubafo,R_adafo,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_akudo,
     & REAL(R_ekupu,4),L_arodo,L_atudo,R_edafo,R_idafo,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_okudo,
     & REAL(R_alupu,4),L_erodo,L_etudo,R_odafo,R_udafo,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_eludo,
     & REAL(R_ulupu,4),L_irodo,L_itudo,R_afafo,R_efafo,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_uludo,
     & REAL(R_imupu,4),L_orodo,L_otudo,R_ifafo,R_ofafo,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_imudo,
     & REAL(R_uxovu,4),L_urodo,L_uvudo,L_abudo,R_ikafo,R_okafo
     &,
     & R_ufafo,R_akafo,R_ekafo,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_apudo,REAL(R_avosu,4),L_asodo,
     & L_utudo,L_ebudo,R_ilafo,R_olafo,R_ukafo,R_alafo,R_elafo
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_opudo,
     & REAL(R_odubad,4),L_esodo,L_avudo,L_ibudo,R_imafo,
     & R_omafo,R_ulafo,R_amafo,R_emafo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_urudo,
     & REAL(R_avekad,4),L_usodo,L_ovudo,L_obudo,R_ipafo,
     & R_orafo,R_umafo,R_apafo,R_epafo,R_opodo,
     & REAL(R_ixofad,4),R_apodo,REAL(R_uxofad,4),R_ipodo)
C FDA_doz_logic.fgi( 115,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT06
      Call KONTEYNER_DOZ(deltat,R_efero,R_eraro,L_epero,L_odero
     &,
     & L_udero,L_oraro,L_uraro,L_esaro,L_isaro,L_usaro,
     & L_ataro,L_itaro,L_otaro,L_avaro,L_evaro,L_ovaro,
     & L_uvaro,L_exaro,L_ixaro,L_uxaro,L_abero,L_ibero,
     & L_obero,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_oparo,R_araro,R_iraro,
     & R_apero,L_uparo,L_afiro,L_iparo,
     & L_epupu,R_ebiro,R_ibiro,R_obiro,L_edero,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_afaro,L_ukero,L_apupu,
     & R_ubiro,R_adiro,R_emero,R_imero,R_umero,
     & REAL(60,4),R_omero,R_ikaro,R_olero,R_ulero,
     & R_amero,R_ofaro,R_ufaro,R_akaro,R_ekaro,L_imaro,L_okaro
     &,
     & L_ukaro,L_alaro,L_elaro,L_ilaro,L_olaro,L_ularo,L_amaro
     &,
     & L_emaro,R_udiro,R_uxupo,L_afero,
     & REAL(R_odupu,4),L_efaro,L_alero,R_idiro,R_odiro,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_asaro,
     & REAL(R_ifupu,4),L_ibaro,L_ifero,R_ipero,R_opero,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_osaro,
     & REAL(R_ekupu,4),L_obaro,L_ofero,R_upero,R_arero,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_etaro,
     & REAL(R_alupu,4),L_ubaro,L_ufero,R_erero,R_irero,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_utaro,
     & REAL(R_ulupu,4),L_adaro,L_akero,R_orero,R_urero,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_ivaro,
     & REAL(R_imupu,4),L_edaro,L_ekero,R_asero,R_esero,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_axaro,
     & REAL(R_uxovu,4),L_idaro,L_ilero,L_omaro,R_atero,R_etero
     &,
     & R_isero,R_osero,R_usero,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_oxaro,REAL(R_avosu,4),L_odaro,
     & L_ikero,L_umaro,R_avero,R_evero,R_itero,R_otero,R_utero
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_ebero,
     & REAL(R_odubad,4),L_udaro,L_okero,L_aparo,R_axero,
     & R_exero,R_ivero,R_overo,R_uvero,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_idero,
     & REAL(R_avekad,4),L_ifaro,L_elero,L_eparo,R_abiro,
     & R_ediro,R_ixero,R_oxero,R_uxero,R_ebaro,
     & REAL(R_ixofad,4),R_oxupo,REAL(R_uxofad,4),R_abaro)
C FDA_doz_logic.fgi(  44,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT02
      Call KONTEYNER_DOZ(deltat,R_ubobo,R_umibo,L_ulobo,L_ebobo
     &,
     & L_ibobo,L_epibo,L_ipibo,L_upibo,L_aribo,L_iribo,
     & L_oribo,L_asibo,L_esibo,L_osibo,L_usibo,L_etibo,
     & L_itibo,L_utibo,L_avibo,L_ivibo,L_ovibo,L_axibo,
     & L_exibo,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_emibo,R_omibo,R_apibo,
     & R_olobo,L_imibo,L_obubo,L_amibo,
     & L_epupu,R_uvobo,R_axobo,R_exobo,L_uxibo,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_obibo,L_ifobo,L_apupu,
     & R_ixobo,R_oxobo,R_ukobo,R_alobo,R_ilobo,
     & REAL(60,4),R_elobo,R_afibo,R_ekobo,R_ikobo,
     & R_okobo,R_edibo,R_idibo,R_odibo,R_udibo,L_alibo,L_efibo
     &,
     & L_ifibo,L_ofibo,L_ufibo,L_akibo,L_ekibo,L_ikibo,L_okibo
     &,
     & L_ukibo,R_ibubo,R_ivebo,L_obobo,
     & REAL(R_odupu,4),L_ubibo,L_ofobo,R_abubo,R_ebubo,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_opibo,
     & REAL(R_ifupu,4),L_axebo,L_adobo,R_amobo,R_emobo,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_eribo,
     & REAL(R_ekupu,4),L_exebo,L_edobo,R_imobo,R_omobo,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_uribo,
     & REAL(R_alupu,4),L_ixebo,L_idobo,R_umobo,R_apobo,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_isibo,
     & REAL(R_ulupu,4),L_oxebo,L_odobo,R_epobo,R_ipobo,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_atibo,
     & REAL(R_imupu,4),L_uxebo,L_udobo,R_opobo,R_upobo,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_otibo,
     & REAL(R_uxovu,4),L_abibo,L_akobo,L_elibo,R_orobo,R_urobo
     &,
     & R_arobo,R_erobo,R_irobo,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_evibo,REAL(R_avosu,4),L_ebibo,
     & L_afobo,L_ilibo,R_osobo,R_usobo,R_asobo,R_esobo,R_isobo
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_uvibo,
     & REAL(R_odubad,4),L_ibibo,L_efobo,L_olibo,R_otobo,
     & R_utobo,R_atobo,R_etobo,R_itobo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_abobo,
     & REAL(R_avekad,4),L_adibo,L_ufobo,L_ulibo,R_ovobo,
     & R_uxobo,R_avobo,R_evobo,R_ivobo,R_uvebo,
     & REAL(R_ixofad,4),R_evebo,REAL(R_uxofad,4),R_ovebo)
C FDA_doz_logic.fgi( 167,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT09
      Call KONTEYNER_DOZ(deltat,R_udufo,R_upofo,L_umufo,L_edufo
     &,
     & L_idufo,L_erofo,L_irofo,L_urofo,L_asofo,L_isofo,
     & L_osofo,L_atofo,L_etofo,L_otofo,L_utofo,L_evofo,
     & L_ivofo,L_uvofo,L_axofo,L_ixofo,L_oxofo,L_abufo,
     & L_ebufo,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_epofo,R_opofo,R_arofo,
     & R_omufo,L_ipofo,L_odako,L_apofo,
     & L_epupu,R_uxufo,R_abako,R_ebako,L_ubufo,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_odofo,L_ikufo,L_apupu,
     & R_ibako,R_obako,R_ulufo,R_amufo,R_imufo,
     & REAL(60,4),R_emufo,R_akofo,R_elufo,R_ilufo,
     & R_olufo,R_efofo,R_ifofo,R_ofofo,R_ufofo,L_amofo,L_ekofo
     &,
     & L_ikofo,L_okofo,L_ukofo,L_alofo,L_elofo,L_ilofo,L_olofo
     &,
     & L_ulofo,R_idako,R_ixifo,L_odufo,
     & REAL(R_odupu,4),L_udofo,L_okufo,R_adako,R_edako,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_orofo,
     & REAL(R_ifupu,4),L_abofo,L_afufo,R_apufo,R_epufo,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_esofo,
     & REAL(R_ekupu,4),L_ebofo,L_efufo,R_ipufo,R_opufo,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_usofo,
     & REAL(R_alupu,4),L_ibofo,L_ifufo,R_upufo,R_arufo,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_itofo,
     & REAL(R_ulupu,4),L_obofo,L_ofufo,R_erufo,R_irufo,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_avofo,
     & REAL(R_imupu,4),L_ubofo,L_ufufo,R_orufo,R_urufo,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_ovofo,
     & REAL(R_uxovu,4),L_adofo,L_alufo,L_emofo,R_osufo,R_usufo
     &,
     & R_asufo,R_esufo,R_isufo,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_exofo,REAL(R_avosu,4),L_edofo,
     & L_akufo,L_imofo,R_otufo,R_utufo,R_atufo,R_etufo,R_itufo
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_uxofo,
     & REAL(R_odubad,4),L_idofo,L_ekufo,L_omofo,R_ovufo,
     & R_uvufo,R_avufo,R_evufo,R_ivufo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_adufo,
     & REAL(R_avekad,4),L_afofo,L_ukufo,L_umofo,R_oxufo,
     & R_ubako,R_axufo,R_exufo,R_ixufo,R_uxifo,
     & REAL(R_ixofad,4),R_exifo,REAL(R_uxofad,4),R_oxifo)
C FDA_doz_logic.fgi(  80,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT04
      Call KONTEYNER_DOZ(deltat,R_ikado,R_isubo,L_irado,L_ufado
     &,
     & L_akado,L_usubo,L_atubo,L_itubo,L_otubo,L_avubo,
     & L_evubo,L_ovubo,L_uvubo,L_exubo,L_ixubo,L_uxubo,
     & L_abado,L_ibado,L_obado,L_adado,L_edado,L_odado,
     & L_udado,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_urubo,R_esubo,R_osubo,
     & R_erado,L_asubo,L_ekedo,L_orubo,
     & L_epupu,R_idedo,R_odedo,R_udedo,L_ifado,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_ekubo,L_amado,L_apupu,
     & R_afedo,R_efedo,R_ipado,R_opado,R_arado,
     & REAL(60,4),R_upado,R_olubo,R_umado,R_apado,
     & R_epado,R_ukubo,R_alubo,R_elubo,R_ilubo,L_opubo,L_ulubo
     &,
     & L_amubo,L_emubo,L_imubo,L_omubo,L_umubo,L_apubo,L_epubo
     &,
     & L_ipubo,R_akedo,R_adubo,L_ekado,
     & REAL(R_odupu,4),L_ikubo,L_emado,R_ofedo,R_ufedo,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_etubo,
     & REAL(R_ifupu,4),L_odubo,L_okado,R_orado,R_urado,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_utubo,
     & REAL(R_ekupu,4),L_udubo,L_ukado,R_asado,R_esado,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_ivubo,
     & REAL(R_alupu,4),L_afubo,L_alado,R_isado,R_osado,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_axubo,
     & REAL(R_ulupu,4),L_efubo,L_elado,R_usado,R_atado,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_oxubo,
     & REAL(R_imupu,4),L_ifubo,L_ilado,R_etado,R_itado,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_ebado,
     & REAL(R_uxovu,4),L_ofubo,L_omado,L_upubo,R_evado,R_ivado
     &,
     & R_otado,R_utado,R_avado,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_ubado,REAL(R_avosu,4),L_ufubo,
     & L_olado,L_arubo,R_exado,R_ixado,R_ovado,R_uvado,R_axado
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_idado,
     & REAL(R_odubad,4),L_akubo,L_ulado,L_erubo,R_ebedo,
     & R_ibedo,R_oxado,R_uxado,R_abedo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_ofado,
     & REAL(R_avekad,4),L_okubo,L_imado,L_irubo,R_ededo,
     & R_ifedo,R_obedo,R_ubedo,R_adedo,R_idubo,
     & REAL(R_ixofad,4),R_ububo,REAL(R_uxofad,4),R_edubo)
C FDA_doz_logic.fgi( 151,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT08
      Call KONTEYNER_DOZ(deltat,R_apido,R_axedo,L_avido,L_imido
     &,
     & L_omido,L_ixedo,L_oxedo,L_abido,L_ebido,L_obido,
     & L_ubido,L_edido,L_idido,L_udido,L_afido,L_ifido,
     & L_ofido,L_akido,L_ekido,L_okido,L_ukido,L_elido,
     & L_ilido,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_ivedo,R_uvedo,R_exedo,
     & R_utido,L_ovedo,L_umodo,L_evedo,
     & L_epupu,R_alodo,R_elodo,R_ilodo,L_amido,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_umedo,L_orido,L_apupu,
     & R_olodo,R_ulodo,R_atido,R_etido,R_otido,
     & REAL(60,4),R_itido,R_eredo,R_isido,R_osido,
     & R_usido,R_ipedo,R_opedo,R_upedo,R_aredo,L_etedo,L_iredo
     &,
     & L_oredo,L_uredo,L_asedo,L_esedo,L_isedo,L_osedo,L_usedo
     &,
     & L_atedo,R_omodo,R_okedo,L_umido,
     & REAL(R_odupu,4),L_apedo,L_urido,R_emodo,R_imodo,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_uxedo,
     & REAL(R_ifupu,4),L_eledo,L_epido,R_evido,R_ivido,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_ibido,
     & REAL(R_ekupu,4),L_iledo,L_ipido,R_ovido,R_uvido,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_adido,
     & REAL(R_alupu,4),L_oledo,L_opido,R_axido,R_exido,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_odido,
     & REAL(R_ulupu,4),L_uledo,L_upido,R_ixido,R_oxido,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_efido,
     & REAL(R_imupu,4),L_amedo,L_arido,R_uxido,R_abodo,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_ufido,
     & REAL(R_uxovu,4),L_emedo,L_esido,L_itedo,R_ubodo,R_adodo
     &,
     & R_ebodo,R_ibodo,R_obodo,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_ikido,REAL(R_avosu,4),L_imedo,
     & L_erido,L_otedo,R_udodo,R_afodo,R_edodo,R_idodo,R_ododo
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_alido,
     & REAL(R_odubad,4),L_omedo,L_irido,L_utedo,R_ufodo,
     & R_akodo,R_efodo,R_ifodo,R_ofodo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_emido,
     & REAL(R_avekad,4),L_epedo,L_asido,L_avedo,R_ukodo,
     & R_amodo,R_ekodo,R_ikodo,R_okodo,R_aledo,
     & REAL(R_ixofad,4),R_ikedo,REAL(R_uxofad,4),R_ukedo)
C FDA_doz_logic.fgi( 134,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT07
      Call KONTEYNER_DOZ(deltat,R_exefo,R_elefo,L_ekifo,L_ovefo
     &,
     & L_uvefo,L_olefo,L_ulefo,L_emefo,L_imefo,L_umefo,
     & L_apefo,L_ipefo,L_opefo,L_arefo,L_erefo,L_orefo,
     & L_urefo,L_esefo,L_isefo,L_usefo,L_atefo,L_itefo,
     & L_otefo,L_ifekad,L_udofad,L_obivu,
     & L_ifibad,L_overu,L_olupu,
     & L_ukupu,L_akupu,L_efupu,
     & L_idupu,L_ubupu,R_okefo,R_alefo,R_ilefo,
     & R_akifo,L_ukefo,L_axifo,L_ikefo,
     & L_epupu,R_etifo,R_itifo,R_otifo,L_evefo,
     & REAL(R_upupu,4),REAL(R_omupu,4),
     & REAL(R_umupu,4),L_axafo,L_ubifo,L_apupu,
     & R_utifo,R_avifo,R_efifo,R_ififo,R_ufifo,
     & REAL(60,4),R_ofifo,R_ibefo,R_odifo,R_udifo,
     & R_afifo,R_oxafo,R_uxafo,R_abefo,R_ebefo,L_ifefo,L_obefo
     &,
     & L_ubefo,L_adefo,L_edefo,L_idefo,L_odefo,L_udefo,L_afefo
     &,
     & L_efefo,R_uvifo,R_usafo,L_axefo,
     & REAL(R_odupu,4),L_exafo,L_adifo,R_ivifo,R_ovifo,
     & REAL(R_adupu,4),REAL(R_edupu,4),L_amefo,
     & REAL(R_ifupu,4),L_itafo,L_ixefo,R_ikifo,R_okifo,
     & REAL(R_udupu,4),REAL(R_afupu,4),L_omefo,
     & REAL(R_ekupu,4),L_otafo,L_oxefo,R_ukifo,R_alifo,
     & REAL(R_ofupu,4),REAL(R_ufupu,4),L_epefo,
     & REAL(R_alupu,4),L_utafo,L_uxefo,R_elifo,R_ilifo,
     & REAL(R_ikupu,4),REAL(R_okupu,4),L_upefo,
     & REAL(R_ulupu,4),L_avafo,L_abifo,R_olifo,R_ulifo,
     & REAL(R_elupu,4),REAL(R_ilupu,4),L_irefo,
     & REAL(R_imupu,4),L_evafo,L_ebifo,R_amifo,R_emifo,
     & REAL(R_amupu,4),REAL(R_emupu,4),L_asefo,
     & REAL(R_uxovu,4),L_ivafo,L_idifo,L_ofefo,R_apifo,R_epifo
     &,
     & R_imifo,R_omifo,R_umifo,REAL(R_orabad,4),
     & REAL(R_asabad,4),L_osefo,REAL(R_avosu,4),L_ovafo,
     & L_ibifo,L_ufefo,R_arifo,R_erifo,R_ipifo,R_opifo,R_upifo
     &,
     & REAL(R_umavu,4),REAL(R_epavu,4),L_etefo,
     & REAL(R_odubad,4),L_uvafo,L_obifo,L_akefo,R_asifo,
     & R_esifo,R_irifo,R_orifo,R_urifo,
     & REAL(R_arefad,4),REAL(R_irefad,4),L_ivefo,
     & REAL(R_avekad,4),L_ixafo,L_edifo,L_ekefo,R_atifo,
     & R_evifo,R_isifo,R_osifo,R_usifo,R_etafo,
     & REAL(R_ixofad,4),R_osafo,REAL(R_uxofad,4),R_atafo)
C FDA_doz_logic.fgi(  96,  68):���������� ���������� ������� ����������� ���������,20FDA20CONT05
      !{
      Call DAT_DISCR_HANDLER(deltat,I_eteto,L_uteto,R_oteto
     &,
     & REAL(R_aveto,4),L_osito,L_ateto,I_iteto)
      !}
C FDA_doz_logic.fgi( 532,  96):���������� ������� ���������,20FDA20TRAN10_L5
      !{
      Call DAT_DISCR_HANDLER(deltat,I_okito,L_elito,R_alito
     &,
     & REAL(R_ilito,4),L_atito,L_ikito,I_ukito)
      !}
C FDA_doz_logic.fgi( 532, 108):���������� ������� ���������,20FDA20TRAN10_L4
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uveto,L_ixeto,R_exeto
     &,
     & REAL(R_oxeto,4),L_itito,L_oveto,I_axeto)
      !}
C FDA_doz_logic.fgi( 558, 108):���������� ������� ���������,20FDA20TRAN10_L3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afito,L_ofito,R_ifito
     &,
     & REAL(R_ufito,4),L_utito,L_udito,I_efito)
      !}
C FDA_doz_logic.fgi( 582, 108):���������� ������� ���������,20FDA20TRAN10_L2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibito,L_adito,R_ubito
     &,
     & REAL(R_edito,4),L_axito,L_ebito,I_obito)
      !}
C FDA_doz_logic.fgi( 582,  92):���������� ������� ���������,20FDA20TRAN10_L1
      R_(184) = R_i + (-R_ufasad)
C FDA_doz_logic.fgi(  49, 615):��������
C label 1771  try1771=try1771-1
      if(L0_uterad) then
         R_(162)=R_(184)
      else
         R_(162)=R_(181)
      endif
C FDA_doz_logic.fgi(  59, 615):���� RE IN LO CH7
      L_amarad=R_(162).gt.R0_ilarad
C FDA_doz_logic.fgi(  73, 621):���������� >
      L_emarad=R_(162).lt.R0_elarad
C FDA_doz_logic.fgi(  73, 609):���������� <
      L_ovarad =.NOT.(L_amarad.OR.L_emarad)
C FDA_doz_logic.fgi(  79, 615):���
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_eparad,REAL(R_utarad
     &,4),
     & REAL(R_imerad,4),REAL(R_omerad,4),REAL(R_akerad,4)
     &,R_oparad,REAL(R_uferad,4),
     & L_(625),L_(624),R_aperad,R_eperad,R_ererad,I_okerad
     &,R_iparad,
     & R_arerad,R_ibasad,I_ukerad,I_olerad,R_osarad,
     & REAL(R_atarad,4),R_uparad,REAL(R_erarad,4),L_emarad
     &,
     & L_amarad,I_axarad,I_uvarad,I_ikerad,I_alerad,C20_avarad
     &,C20_evarad,C8_ivarad,
     & L_imarad,R_irarad,REAL(R_urarad,4),L_esarad,
     & R_asarad,REAL(R_isarad,4),I_ilerad,L_orarad,
     & L_(623),L_ovarad,L_omarad,I_elerad,I_ekerad,L_etarad
     &,L_amerad,L_iperad,
     & L_usarad,L_ararad,L_itarad,L_otarad,L_umerad,L_ixarad
     &,
     & L_aderad,L_ederad,L_oberad,L_uberad,L_uxarad,L_aberad
     &,L_iderad,L_oderad,L_uderad,L_aferad,
     & L_eferad,L_oxarad,REAL(R8_axorad,8),REAL(1.0,4),R8_exarad
     &,L_emerad,I_ulerad,
     & L_aparad,L_umarad,L_operad,L_uperad,L_eberad,L_iferad
     &,L_oferad,L_iberad)
      !}
C FDA_doz_logic.fgi(  63, 749):���������� ��������� ������� ����������� ���������,20FDA21AE704M7
      R_ufasad=R_oparad
C FDA_doz_logic.fgi( 176, 714):������,20FDA21AE704VY01
C sav1=R_(184)
      R_(184) = R_i + (-R_ufasad)
C FDA_doz_logic.fgi(  49, 615):recalc:��������
C if(sav1.ne.R_(184) .and. try1771.gt.0) goto 1771
      R_(187) = R_ibasad + R_ebasad
C FDA_doz_logic.fgi(  48, 696):��������
      R_eterad = R_adasad * R_(187)
C FDA_doz_logic.fgi(  55, 697):����������,1
      L_(247) = L_akurad.AND.L_ovarad
C FDA_doz_logic.fgi(  43, 473):�
      L_(250) = L_abebo.AND.L_(247)
C FDA_doz_logic.fgi(  50, 491):�
      if(L_(250).and..not.L0_edebo) then
         R0_ubebo=R0_adebo
      else
         R0_ubebo=max(R_(50)-deltat,0.0)
      endif
      L_(249)=R0_ubebo.gt.0.0
      L0_edebo=L_(250)
C FDA_doz_logic.fgi(  57, 491):������������  �� T
      L_(252) = L_(249).OR.L_(246)
C FDA_doz_logic.fgi(  66, 490):���
      if(L_(252)) then
         R_(196)=R_(52)
      endif
C FDA_doz_logic.fgi(  76, 490):���� � ������������� �������
      L_(248) = L_uxabo.AND.L_(247)
C FDA_doz_logic.fgi(  50, 479):�
      if(L_(248).and..not.L0_obebo) then
         R0_ebebo=R0_ibebo
      else
         R0_ebebo=max(R_(49)-deltat,0.0)
      endif
      L_(251)=R0_ebebo.gt.0.0
      L0_obebo=L_(248)
C FDA_doz_logic.fgi(  57, 479):������������  �� T
      if(L_(251)) then
         R_(196)=R_(51)
      endif
C FDA_doz_logic.fgi(  76, 479):���� � ������������� �������
      R_o=R_(196)
C FDA_doz_logic.fgi(  76, 479):������-�������: ���������� ��� �������������� ������
      if(L_(247).and..not.L0_otuxi) then
         R0_atuxi=R0_etuxi
      else
         R0_atuxi=max(R_(17)-deltat,0.0)
      endif
      L_ituxi=R0_atuxi.gt.0.0
      L0_otuxi=L_(247)
C FDA_doz_logic.fgi(  57, 473):������������  �� T
      R_(183) = R_o + (-R_ilasad)
C FDA_doz_logic.fgi(  49, 587):��������
C label 1836  try1836=try1836-1
      if(L0_oterad) then
         R_(161)=R_(183)
      else
         R_(161)=R_(180)
      endif
C FDA_doz_logic.fgi(  59, 587):���� RE IN LO CH7
      L_ukarad=R_(161).gt.R0_alarad
C FDA_doz_logic.fgi(  73, 593):���������� >
      L_ikarad=R_(161).lt.R0_okarad
C FDA_doz_logic.fgi(  73, 581):���������� <
      L_ubupad =.NOT.(L_ukarad.OR.L_ikarad)
C FDA_doz_logic.fgi(  79, 587):���
      !{
      Call TOLKATEL_HANDLER_DOZ(deltat,R_uxipad,REAL(R_ikopad
     &,4),
     & REAL(R_uvopad,4),REAL(R_axopad,4),REAL(R_isopad,4)
     &,R_ebopad,REAL(R_esopad,4),
     & L_(619),L_(618),R_ixopad,R_oxopad,R_obupad,I_atopad
     &,R_abopad,
     & R_ibupad,R_abasad,I_etopad,I_avopad,R_efopad,
     & REAL(R_ofopad,4),R_ibopad,REAL(R_ubopad,4),L_ikarad
     &,
     & L_ukarad,I_ilopad,I_elopad,I_usopad,I_itopad,C20_okopad
     &,C20_ukopad,C8_alopad,
     & L_axipad,R_adopad,REAL(R_idopad,4),L_udopad,
     & R_odopad,REAL(R_afopad,4),I_utopad,L_edopad,
     & L_(617),L_ubupad,L_exipad,I_otopad,I_osopad,L_ufopad
     &,L_ivopad,L_uxopad,
     & L_ifopad,L_obopad,L_akopad,L_ekopad,L_exopad,L_ulopad
     &,
     & L_ipopad,L_opopad,L_apopad,L_epopad,L_emopad,L_imopad
     &,L_upopad,L_aropad,L_eropad,L_iropad,
     & L_oropad,L_amopad,REAL(R8_axorad,8),REAL(1.0,4),R8_olopad
     &,L_ovopad,I_evopad,
     & L_oxipad,L_ixipad,L_abupad,L_ebupad,L_omopad,L_uropad
     &,L_asopad,L_umopad)
      !}
C FDA_doz_logic.fgi( 102, 749):���������� ��������� ������� ����������� ���������,20FDA21AE704M5
      R_ilasad=R_ebopad
C FDA_doz_logic.fgi( 176, 710):������,20FDA21AE704VZ01
C sav1=R_(183)
      R_(183) = R_o + (-R_ilasad)
C FDA_doz_logic.fgi(  49, 587):recalc:��������
C if(sav1.ne.R_(183) .and. try1836.gt.0) goto 1836
      R_opirad = (-R_(195)) + R_ilasad
C FDA_doz_logic.fgi(  44, 656):��������
      R_(186) = R_abasad + R_uxurad
C FDA_doz_logic.fgi(  48, 681):��������
      R_aterad = R_adasad * R_(186)
C FDA_doz_logic.fgi(  55, 682):����������,1
      R_(156) = R_elasad + (-R_epumad)
C FDA_doz_logic.fgi( 231, 525):��������
C label 1878  try1878=try1878-1
      L_(603)=R_(156).lt.R0_ixomad
C FDA_doz_logic.fgi( 239, 525):���������� <
      L_(602)=R_(156).gt.R0_exomad
C FDA_doz_logic.fgi( 239, 519):���������� >
      if(L0_imumad) then
         R_etapad=R_(153)
      else
         R_etapad=R_(158)
      endif
C FDA_doz_logic.fgi( 238, 460):���� RE IN LO CH7
      R_ufumad=R_ufumad+deltat/R0_ofumad*R_etapad
      if(R_ufumad.gt.R0_ifumad) then
         R_ufumad=R0_ifumad
      elseif(R_ufumad.lt.R0_akumad) then
         R_ufumad=R0_akumad
      endif
C FDA_doz_logic.fgi( 237, 561):����������
      R_apumad=R_ufumad
C FDA_doz_logic.fgi( 260, 563):������,20FDA60CONTB01VY01
      R_(155) = R_ufasad + (-R_apumad)
C FDA_doz_logic.fgi( 231, 513):��������
      L_(601)=R_(155).lt.R0_axomad
C FDA_doz_logic.fgi( 239, 513):���������� <
      L_(600)=R_(155).gt.R0_uvomad
C FDA_doz_logic.fgi( 239, 507):���������� >
      if(L0_imumad) then
         R_atapad=R_(151)
      else
         R_atapad=R_(157)
      endif
C FDA_doz_logic.fgi( 238, 446):���� RE IN LO CH7
      R_afumad=R_afumad+deltat/R0_udumad*R_atapad
      if(R_afumad.gt.R0_odumad) then
         R_afumad=R0_odumad
      elseif(R_afumad.lt.R0_efumad) then
         R_afumad=R0_efumad
      endif
C FDA_doz_logic.fgi( 237, 549):����������
      R_umumad=R_afumad
C FDA_doz_logic.fgi( 260, 551):������,20FDA60CONTB01VZ01
      R_(154) = R_ilasad + (-R_umumad)
C FDA_doz_logic.fgi( 231, 500):��������
      L_(599)=R_(154).lt.R0_ovomad
C FDA_doz_logic.fgi( 239, 500):���������� <
      L_(598)=R_(154).gt.R0_ivomad
C FDA_doz_logic.fgi( 239, 494):���������� >
      L_(596) = L_(603).AND.L_(602).AND.L_(601).AND.L_(600
     &).AND.L_(599).AND.L_(598).AND.L_(597)
C FDA_doz_logic.fgi( 248, 511):�
      L0_imumad=L_(596).or.(L0_imumad.and..not.(.NOT.L_(597
     &)))
      L_(595)=.not.L0_imumad
C FDA_doz_logic.fgi( 257, 509):RS �������
      if(L0_imumad) then
         R_itapad=R_(152)
      else
         R_itapad=R_(159)
      endif
C FDA_doz_logic.fgi( 238, 474):���� RE IN LO CH7
      R_okumad=R_okumad+deltat/R0_ikumad*R_itapad
      if(R_okumad.gt.R0_ekumad) then
         R_okumad=R0_ekumad
      elseif(R_okumad.lt.R0_ukumad) then
         R_okumad=R0_ukumad
      endif
C FDA_doz_logic.fgi( 237, 574):����������
      R_epumad=R_okumad
C FDA_doz_logic.fgi( 260, 576):������,20FDA60CONTB01VX01
C sav1=R_(156)
      R_(156) = R_elasad + (-R_epumad)
C FDA_doz_logic.fgi( 231, 525):recalc:��������
C if(sav1.ne.R_(156) .and. try1878.gt.0) goto 1878
      L_(610)=R_epumad.lt.R0_obumad
C FDA_doz_logic.fgi( 235, 416):���������� <
      L_(609)=R_epumad.gt.R0_ibumad
C FDA_doz_logic.fgi( 235, 410):���������� >
      L_(338)=R_epumad.lt.R0_opolo
C FDA_doz_logic.fgi( 750, 616):���������� <
      L_(337)=R_epumad.gt.R0_ipolo
C FDA_doz_logic.fgi( 750, 610):���������� >
      L_(331)=R_epumad.gt.R0_urilo
C FDA_doz_logic.fgi( 750, 571):���������� >
      L_(332)=R_epumad.lt.R0_asilo
C FDA_doz_logic.fgi( 750, 577):���������� <
      L_(327)=R_epumad.lt.R0_arilo
C FDA_doz_logic.fgi( 750, 538):���������� <
      L_(326)=R_epumad.gt.R0_upilo
C FDA_doz_logic.fgi( 750, 532):���������� >
      L_(330)=R_umumad.lt.R0_orilo
C FDA_doz_logic.fgi( 750, 565):���������� <
      L_(329)=R_umumad.gt.R0_irilo
C FDA_doz_logic.fgi( 750, 559):���������� >
      L_(335)=R_umumad.gt.R0_apolo
C FDA_doz_logic.fgi( 750, 598):���������� >
      L_(336)=R_umumad.lt.R0_epolo
C FDA_doz_logic.fgi( 750, 604):���������� <
      L_(324)=R_umumad.gt.R0_ipilo
C FDA_doz_logic.fgi( 750, 520):���������� >
      L_(325)=R_umumad.lt.R0_opilo
C FDA_doz_logic.fgi( 750, 526):���������� <
      L_(606)=R_umumad.lt.R0_uxomad
C FDA_doz_logic.fgi( 235, 391):���������� <
      L_(605)=R_umumad.gt.R0_oxomad
C FDA_doz_logic.fgi( 235, 385):���������� >
      L_(608)=R_apumad.lt.R0_ebumad
C FDA_doz_logic.fgi( 235, 404):���������� <
      L_(607)=R_apumad.gt.R0_abumad
C FDA_doz_logic.fgi( 235, 398):���������� >
      L_(604) = L_(610).AND.L_(609).AND.L_(608).AND.L_(607
     &).AND.L_(606).AND.L_(605)
C FDA_doz_logic.fgi( 244, 403):�
      if(L_(604)) then
         R_usapad=R_omumad
      else
         R_usapad=R_(160)
      endif
C FDA_doz_logic.fgi( 235, 427):���� RE IN LO CH7
      R_edumad=R_edumad+deltat/R0_adumad*R_usapad
      if(R_edumad.gt.R0_ubumad) then
         R_edumad=R0_ubumad
      elseif(R_edumad.lt.R0_idumad) then
         R_edumad=R0_idumad
      endif
C FDA_doz_logic.fgi( 237, 537):����������
      R_avomad=R_edumad
C FDA_doz_logic.fgi( 260, 539):������,20FDA60CONTB01VS01
      L_(320)=R_avomad.gt.R0_apelo
C FDA_doz_logic.fgi( 703, 594):���������� >
      L_(322)=R_apumad.gt.R0_ipelo
C FDA_doz_logic.fgi( 703, 606):���������� >
      L_(323)=R_apumad.lt.R0_opelo
C FDA_doz_logic.fgi( 703, 612):���������� <
      R_(146) = R_elasad + (-R_usomad)
C FDA_doz_logic.fgi( 312, 525):��������
C label 2029  try2029=try2029-1
      L_(584)=R_(146).lt.R0_adimad
C FDA_doz_logic.fgi( 320, 525):���������� <
      L_(583)=R_(146).gt.R0_ubimad
C FDA_doz_logic.fgi( 320, 519):���������� >
      if(L0_esomad) then
         R_otomad=R_(143)
      else
         R_otomad=R_(148)
      endif
C FDA_doz_logic.fgi( 320, 461):���� RE IN LO CH7
      R_ilimad=R_ilimad+deltat/R0_elimad*R_otomad
      if(R_ilimad.gt.R0_alimad) then
         R_ilimad=R0_alimad
      elseif(R_ilimad.lt.R0_olimad) then
         R_ilimad=R0_olimad
      endif
C FDA_doz_logic.fgi( 317, 561):����������
      R_osomad=R_ilimad
C FDA_doz_logic.fgi( 338, 563):������,20FDA60CONTB02VY01
      R_(145) = R_ufasad + (-R_osomad)
C FDA_doz_logic.fgi( 312, 513):��������
      L_(582)=R_(145).lt.R0_obimad
C FDA_doz_logic.fgi( 320, 513):���������� <
      L_(581)=R_(145).gt.R0_ibimad
C FDA_doz_logic.fgi( 320, 507):���������� >
      if(L0_esomad) then
         R_itomad=R_(141)
      else
         R_itomad=R_(147)
      endif
C FDA_doz_logic.fgi( 320, 447):���� RE IN LO CH7
      R_okimad=R_okimad+deltat/R0_ikimad*R_itomad
      if(R_okimad.gt.R0_ekimad) then
         R_okimad=R0_ekimad
      elseif(R_okimad.lt.R0_ukimad) then
         R_okimad=R0_ukimad
      endif
C FDA_doz_logic.fgi( 317, 549):����������
      R_isomad=R_okimad
C FDA_doz_logic.fgi( 338, 551):������,20FDA60CONTB02VZ01
      R_(144) = R_ilasad + (-R_isomad)
C FDA_doz_logic.fgi( 312, 500):��������
      L_(580)=R_(144).lt.R0_ebimad
C FDA_doz_logic.fgi( 320, 500):���������� <
      L_(579)=R_(144).gt.R0_abimad
C FDA_doz_logic.fgi( 320, 494):���������� >
      L_(577) = L_(584).AND.L_(583).AND.L_(582).AND.L_(581
     &).AND.L_(580).AND.L_(579).AND.L_(578)
C FDA_doz_logic.fgi( 329, 511):�
      L0_esomad=L_(577).or.(L0_esomad.and..not.(.NOT.L_(578
     &)))
      L_(576)=.not.L0_esomad
C FDA_doz_logic.fgi( 338, 509):RS �������
      if(L0_esomad) then
         R_utomad=R_(142)
      else
         R_utomad=R_(149)
      endif
C FDA_doz_logic.fgi( 320, 475):���� RE IN LO CH7
      R_emimad=R_emimad+deltat/R0_amimad*R_utomad
      if(R_emimad.gt.R0_ulimad) then
         R_emimad=R0_ulimad
      elseif(R_emimad.lt.R0_imimad) then
         R_emimad=R0_imimad
      endif
C FDA_doz_logic.fgi( 317, 574):����������
      R_usomad=R_emimad
C FDA_doz_logic.fgi( 338, 576):������,20FDA60CONTB02VX01
C sav1=R_(146)
      R_(146) = R_elasad + (-R_usomad)
C FDA_doz_logic.fgi( 312, 525):recalc:��������
C if(sav1.ne.R_(146) .and. try2029.gt.0) goto 2029
      L_(591)=R_usomad.lt.R0_efimad
C FDA_doz_logic.fgi( 316, 416):���������� <
      L_(590)=R_usomad.gt.R0_afimad
C FDA_doz_logic.fgi( 316, 410):���������� >
      L_(312)=R_usomad.lt.R0_umalo
C FDA_doz_logic.fgi( 890, 575):���������� <
      L_(311)=R_usomad.gt.R0_omalo
C FDA_doz_logic.fgi( 890, 569):���������� >
      L_(306)=R_usomad.gt.R0_ulalo
C FDA_doz_logic.fgi( 890, 530):���������� >
      L_(307)=R_usomad.lt.R0_amalo
C FDA_doz_logic.fgi( 890, 536):���������� <
      L_(317)=R_usomad.gt.R0_alelo
C FDA_doz_logic.fgi( 890, 608):���������� >
      L_(318)=R_usomad.lt.R0_elelo
C FDA_doz_logic.fgi( 890, 614):���������� <
      L_(316)=R_isomad.lt.R0_ukelo
C FDA_doz_logic.fgi( 890, 602):���������� <
      L_(315)=R_isomad.gt.R0_okelo
C FDA_doz_logic.fgi( 890, 596):���������� >
      L_(305)=R_isomad.lt.R0_olalo
C FDA_doz_logic.fgi( 890, 524):���������� <
      L_(304)=R_isomad.gt.R0_ilalo
C FDA_doz_logic.fgi( 890, 518):���������� >
      L_(309)=R_isomad.gt.R0_emalo
C FDA_doz_logic.fgi( 890, 557):���������� >
      L_(310)=R_isomad.lt.R0_imalo
C FDA_doz_logic.fgi( 890, 563):���������� <
      L_(587)=R_isomad.lt.R0_idimad
C FDA_doz_logic.fgi( 316, 391):���������� <
      L_(586)=R_isomad.gt.R0_edimad
C FDA_doz_logic.fgi( 316, 385):���������� >
      L_(589)=R_osomad.lt.R0_udimad
C FDA_doz_logic.fgi( 316, 404):���������� <
      L_(588)=R_osomad.gt.R0_odimad
C FDA_doz_logic.fgi( 316, 398):���������� >
      L_(585) = L_(591).AND.L_(590).AND.L_(589).AND.L_(588
     &).AND.L_(587).AND.L_(586)
C FDA_doz_logic.fgi( 325, 403):�
      if(L_(585)) then
         R_etomad=R_omumad
      else
         R_etomad=R_(150)
      endif
C FDA_doz_logic.fgi( 316, 427):���� RE IN LO CH7
      R_ufimad=R_ufimad+deltat/R0_ofimad*R_etomad
      if(R_ufimad.gt.R0_ifimad) then
         R_ufimad=R0_ifimad
      elseif(R_ufimad.lt.R0_akimad) then
         R_ufimad=R0_akimad
      endif
C FDA_doz_logic.fgi( 317, 537):����������
      R_oxemad=R_ufimad
C FDA_doz_logic.fgi( 338, 539):������,20FDA60CONTB02VS01
      L_(300)=R_oxemad.gt.R0_okalo
C FDA_doz_logic.fgi( 844, 596):���������� >
      L_(303)=R_osomad.lt.R0_elalo
C FDA_doz_logic.fgi( 844, 614):���������� <
      L_(302)=R_osomad.gt.R0_alalo
C FDA_doz_logic.fgi( 844, 608):���������� >
      R_(136) = R_elasad + (-R_ivemad)
C FDA_doz_logic.fgi( 392, 524):��������
C label 2180  try2180=try2180-1
      L_(565)=R_(136).lt.R0_ofamad
C FDA_doz_logic.fgi( 400, 524):���������� <
      L_(564)=R_(136).gt.R0_ifamad
C FDA_doz_logic.fgi( 400, 518):���������� >
      if(L0_utemad) then
         R_axemad=R_(133)
      else
         R_axemad=R_(138)
      endif
C FDA_doz_logic.fgi( 400, 460):���� RE IN LO CH7
      R_apamad=R_apamad+deltat/R0_umamad*R_axemad
      if(R_apamad.gt.R0_omamad) then
         R_apamad=R0_omamad
      elseif(R_apamad.lt.R0_epamad) then
         R_apamad=R0_epamad
      endif
C FDA_doz_logic.fgi( 396, 561):����������
      R_evemad=R_apamad
C FDA_doz_logic.fgi( 418, 563):������,20FDA60CONTB03VY01
      R_(135) = R_ufasad + (-R_evemad)
C FDA_doz_logic.fgi( 392, 512):��������
      L_(563)=R_(135).lt.R0_efamad
C FDA_doz_logic.fgi( 400, 512):���������� <
      L_(562)=R_(135).gt.R0_afamad
C FDA_doz_logic.fgi( 400, 506):���������� >
      if(L0_utemad) then
         R_uvemad=R_(131)
      else
         R_uvemad=R_(137)
      endif
C FDA_doz_logic.fgi( 400, 446):���� RE IN LO CH7
      R_emamad=R_emamad+deltat/R0_amamad*R_uvemad
      if(R_emamad.gt.R0_ulamad) then
         R_emamad=R0_ulamad
      elseif(R_emamad.lt.R0_imamad) then
         R_emamad=R0_imamad
      endif
C FDA_doz_logic.fgi( 396, 549):����������
      R_avemad=R_emamad
C FDA_doz_logic.fgi( 418, 551):������,20FDA60CONTB03VZ01
      R_(134) = R_ilasad + (-R_avemad)
C FDA_doz_logic.fgi( 392, 499):��������
      L_(561)=R_(134).lt.R0_udamad
C FDA_doz_logic.fgi( 400, 499):���������� <
      L_(560)=R_(134).gt.R0_odamad
C FDA_doz_logic.fgi( 400, 493):���������� >
      L_(558) = L_(565).AND.L_(564).AND.L_(563).AND.L_(562
     &).AND.L_(561).AND.L_(560).AND.L_(559)
C FDA_doz_logic.fgi( 409, 510):�
      L0_utemad=L_(558).or.(L0_utemad.and..not.(.NOT.L_(559
     &)))
      L_(557)=.not.L0_utemad
C FDA_doz_logic.fgi( 419, 508):RS �������
      if(L0_utemad) then
         R_exemad=R_(132)
      else
         R_exemad=R_(139)
      endif
C FDA_doz_logic.fgi( 400, 474):���� RE IN LO CH7
      R_upamad=R_upamad+deltat/R0_opamad*R_exemad
      if(R_upamad.gt.R0_ipamad) then
         R_upamad=R0_ipamad
      elseif(R_upamad.lt.R0_aramad) then
         R_upamad=R0_aramad
      endif
C FDA_doz_logic.fgi( 396, 574):����������
      R_ivemad=R_upamad
C FDA_doz_logic.fgi( 418, 576):������,20FDA60CONTB03VX01
C sav1=R_(136)
      R_(136) = R_elasad + (-R_ivemad)
C FDA_doz_logic.fgi( 392, 524):recalc:��������
C if(sav1.ne.R_(136) .and. try2180.gt.0) goto 2180
      L_(291)=R_ivemad.gt.R0_ekuko
C FDA_doz_logic.fgi(1022, 568):���������� >
      L_(292)=R_ivemad.lt.R0_ikuko
C FDA_doz_logic.fgi(1022, 574):���������� <
      L_(287)=R_ivemad.lt.R0_ofuko
C FDA_doz_logic.fgi(1022, 535):���������� <
      L_(286)=R_ivemad.gt.R0_ifuko
C FDA_doz_logic.fgi(1022, 529):���������� >
      L_(572)=R_ivemad.lt.R0_ukamad
C FDA_doz_logic.fgi( 396, 417):���������� <
      L_(571)=R_ivemad.gt.R0_okamad
C FDA_doz_logic.fgi( 396, 411):���������� >
      L_(298)=R_ivemad.lt.R0_udalo
C FDA_doz_logic.fgi(1022, 613):���������� <
      L_(297)=R_ivemad.gt.R0_odalo
C FDA_doz_logic.fgi(1022, 607):���������� >
      L_(284)=R_avemad.gt.R0_afuko
C FDA_doz_logic.fgi(1022, 517):���������� >
      L_(285)=R_avemad.lt.R0_efuko
C FDA_doz_logic.fgi(1022, 523):���������� <
      L_(290)=R_avemad.lt.R0_akuko
C FDA_doz_logic.fgi(1022, 562):���������� <
      L_(289)=R_avemad.gt.R0_ufuko
C FDA_doz_logic.fgi(1022, 556):���������� >
      L_(568)=R_avemad.lt.R0_akamad
C FDA_doz_logic.fgi( 396, 392):���������� <
      L_(567)=R_avemad.gt.R0_ufamad
C FDA_doz_logic.fgi( 396, 386):���������� >
      L_(295)=R_avemad.gt.R0_edalo
C FDA_doz_logic.fgi(1022, 595):���������� >
      L_(296)=R_avemad.lt.R0_idalo
C FDA_doz_logic.fgi(1022, 601):���������� <
      L_(282)=R_evemad.gt.R0_oduko
C FDA_doz_logic.fgi( 975, 607):���������� >
      L_(283)=R_evemad.lt.R0_uduko
C FDA_doz_logic.fgi( 975, 613):���������� <
      L_(570)=R_evemad.lt.R0_ikamad
C FDA_doz_logic.fgi( 396, 405):���������� <
      L_(569)=R_evemad.gt.R0_ekamad
C FDA_doz_logic.fgi( 396, 399):���������� >
      L_(566) = L_(572).AND.L_(571).AND.L_(570).AND.L_(569
     &).AND.L_(568).AND.L_(567)
C FDA_doz_logic.fgi( 405, 404):�
      if(L_(566)) then
         R_ovemad=R_omumad
      else
         R_ovemad=R_(140)
      endif
C FDA_doz_logic.fgi( 396, 428):���� RE IN LO CH7
      R_ilamad=R_ilamad+deltat/R0_elamad*R_ovemad
      if(R_ilamad.gt.R0_alamad) then
         R_ilamad=R0_alamad
      elseif(R_ilamad.lt.R0_olamad) then
         R_ilamad=R0_olamad
      endif
C FDA_doz_logic.fgi( 396, 537):����������
      R_edamad=R_ilamad
C FDA_doz_logic.fgi( 418, 539):������,20FDA60CONTB03VS01
      L_(280)=R_edamad.gt.R0_eduko
C FDA_doz_logic.fgi( 975, 595):���������� >
      R_(126) = R_elasad + (-R_otolad)
C FDA_doz_logic.fgi( 469, 525):��������
C label 2331  try2331=try2331-1
      L_(546)=R_(126).lt.R0_ololad
C FDA_doz_logic.fgi( 477, 525):���������� <
      L_(545)=R_(126).gt.R0_ilolad
C FDA_doz_logic.fgi( 477, 519):���������� >
      if(L0_usolad) then
         R_evolad=R_(123)
      else
         R_evolad=R_(128)
      endif
C FDA_doz_logic.fgi( 477, 461):���� RE IN LO CH7
      R_upolad=R_upolad+deltat/R0_opolad*R_evolad
      if(R_upolad.gt.R0_ipolad) then
         R_upolad=R0_ipolad
      elseif(R_upolad.lt.R0_arolad) then
         R_upolad=R0_arolad
      endif
C FDA_doz_logic.fgi( 476, 561):����������
      R_itolad=R_upolad
C FDA_doz_logic.fgi( 498, 563):������,20FDA60CONTS01VY01
      R_(125) = R_ufasad + (-R_itolad)
C FDA_doz_logic.fgi( 469, 513):��������
      L_(544)=R_(125).lt.R0_elolad
C FDA_doz_logic.fgi( 477, 513):���������� <
      L_(543)=R_(125).gt.R0_alolad
C FDA_doz_logic.fgi( 477, 507):���������� >
      if(L0_usolad) then
         R_avolad=R_(121)
      else
         R_avolad=R_(127)
      endif
C FDA_doz_logic.fgi( 477, 447):���� RE IN LO CH7
      R_apolad=R_apolad+deltat/R0_umolad*R_avolad
      if(R_apolad.gt.R0_omolad) then
         R_apolad=R0_omolad
      elseif(R_apolad.lt.R0_epolad) then
         R_apolad=R0_epolad
      endif
C FDA_doz_logic.fgi( 476, 549):����������
      R_etolad=R_apolad
C FDA_doz_logic.fgi( 498, 551):������,20FDA60CONTS01VZ01
      R_(124) = R_ilasad + (-R_etolad)
C FDA_doz_logic.fgi( 469, 500):��������
      L_(542)=R_(124).lt.R0_ukolad
C FDA_doz_logic.fgi( 477, 500):���������� <
      L_(541)=R_(124).gt.R0_okolad
C FDA_doz_logic.fgi( 477, 494):���������� >
      L_(539) = L_(546).AND.L_(545).AND.L_(544).AND.L_(543
     &).AND.L_(542).AND.L_(541).AND.L_(540)
C FDA_doz_logic.fgi( 486, 511):�
      L0_usolad=L_(539).or.(L0_usolad.and..not.(.NOT.L_(540
     &)))
      L_(538)=.not.L0_usolad
C FDA_doz_logic.fgi( 495, 509):RS �������
      if(L0_usolad) then
         R_ivolad=R_(122)
      else
         R_ivolad=R_(129)
      endif
C FDA_doz_logic.fgi( 477, 475):���� RE IN LO CH7
      R_orolad=R_orolad+deltat/R0_irolad*R_ivolad
      if(R_orolad.gt.R0_erolad) then
         R_orolad=R0_erolad
      elseif(R_orolad.lt.R0_urolad) then
         R_orolad=R0_urolad
      endif
C FDA_doz_logic.fgi( 476, 573):����������
      R_otolad=R_orolad
C FDA_doz_logic.fgi( 498, 575):������,20FDA60CONTS01VX01
C sav1=R_(126)
      R_(126) = R_elasad + (-R_otolad)
C FDA_doz_logic.fgi( 469, 525):recalc:��������
C if(sav1.ne.R_(126) .and. try2331.gt.0) goto 2331
      L_(269)=R_otolad.lt.R0_ideko
C FDA_doz_logic.fgi(1126, 621):���������� <
      L_(268)=R_otolad.gt.R0_edeko
C FDA_doz_logic.fgi(1126, 615):���������� >
      L_(548)=R_otolad.lt.R0_afemo
C FDA_doz_logic.fgi( 474, 418):���������� <
      L_(547)=R_otolad.gt.R0_udemo
C FDA_doz_logic.fgi( 474, 412):���������� >
      L_(271)=R_etolad.lt.R0_udeko
C FDA_doz_logic.fgi(1126, 596):���������� <
      L_(270)=R_etolad.gt.R0_odeko
C FDA_doz_logic.fgi(1126, 590):���������� >
      L_(551)=R_etolad.lt.R0_esolad
C FDA_doz_logic.fgi( 474, 393):���������� <
      L_(550)=R_etolad.gt.R0_asolad
C FDA_doz_logic.fgi( 474, 387):���������� >
      L_(553)=R_itolad.lt.R0_osolad
C FDA_doz_logic.fgi( 474, 406):���������� <
      L_(552)=R_itolad.gt.R0_isolad
C FDA_doz_logic.fgi( 474, 400):���������� >
      L_(549) = L_(548).AND.L_(547).AND.L_(553).AND.L_(552
     &).AND.L_(551).AND.L_(550)
C FDA_doz_logic.fgi( 483, 405):�
      if(L_(549)) then
         R_utolad=R_atolad
      else
         R_utolad=R_(130)
      endif
C FDA_doz_logic.fgi( 474, 429):���� RE IN LO CH7
      R_emolad=R_emolad+deltat/R0_amolad*R_utolad
      if(R_emolad.gt.R0_ulolad) then
         R_emolad=R0_ulolad
      elseif(R_emolad.lt.R0_imolad) then
         R_emolad=R0_imolad
      endif
C FDA_doz_logic.fgi( 476, 537):����������
      R_akolad=R_emolad
C FDA_doz_logic.fgi( 498, 539):������,20FDA60CONTS01VS01
      L_(274)=R_akolad.gt.R0_ifeko
C FDA_doz_logic.fgi(1126, 578):���������� >
      L_(364) = L_(549).AND.L_(366).AND.L_(365).AND.L_(363
     &)
C FDA_doz_logic.fgi( 574, 418):�
      L0_amemo=L_(364).or.(L0_amemo.and..not.(.NOT.L_(361
     &)))
      L_(362)=.not.L0_amemo
C FDA_doz_logic.fgi( 586, 416):RS �������
      L_(357) = L_(360).AND.L0_amemo
C FDA_doz_logic.fgi( 596, 419):�
      L_(358) =.NOT.(L0_amemo.AND.L_(356))
C FDA_doz_logic.fgi( 596, 414):�
      L0_olemo=L_(357).or.(L0_olemo.and..not.(.NOT.L_(358
     &)))
      L_(359)=.not.L0_olemo
C FDA_doz_logic.fgi( 606, 417):RS �������
      if(L0_olemo) then
         I_epemo=I_(100)
      else
         I_epemo=I_(101)
      endif
C FDA_doz_logic.fgi( 613, 424):���� RE IN LO CH7
      L_(354) = L_(549).AND.L_(353).AND.L_(355)
C FDA_doz_logic.fgi( 574, 383):�
      L0_akemo=L_(354).or.(L0_akemo.and..not.(.NOT.L_(351
     &)))
      L_(352)=.not.L0_akemo
C FDA_doz_logic.fgi( 586, 381):RS �������
      L_(348) = L_(347).AND.L0_akemo
C FDA_doz_logic.fgi( 596, 384):�
      L_(349) =.NOT.(L0_akemo.AND.L_(346))
C FDA_doz_logic.fgi( 596, 379):�
      L0_ufemo=L_(348).or.(L0_ufemo.and..not.(.NOT.L_(349
     &)))
      L_(350)=.not.L0_ufemo
C FDA_doz_logic.fgi( 606, 382):RS �������
      if(L0_ufemo) then
         I_alemo=I_(98)
      else
         I_alemo=I_(99)
      endif
C FDA_doz_logic.fgi( 613, 389):���� RE IN LO CH7
      L_(273)=R_itolad.lt.R0_efeko
C FDA_doz_logic.fgi(1126, 609):���������� <
      L_(272)=R_itolad.gt.R0_afeko
C FDA_doz_logic.fgi(1126, 603):���������� >
      L_(321)=R_esolo.gt.R0_epelo
C FDA_doz_logic.fgi( 703, 600):���������� >
C label 2480  try2480=try2480-1
      L_(334) = L_(323).AND.L_(322).AND.L_(321).AND.L_(320
     &)
C FDA_doz_logic.fgi( 711, 603):�
      L_(339) = L_(338).AND.L_(337).AND.L_(336).AND.L_(335
     &).AND.L_(334)
C FDA_doz_logic.fgi( 759, 604):�
      if(L_(339)) then
         R_irolo=R_(115)
      else
         R_irolo=R_(116)
      endif
C FDA_doz_logic.fgi( 764, 628):���� RE IN LO CH7
      L_(333) = L_(332).AND.L_(331).AND.L_(330).AND.L_(329
     &).AND.L_(334)
C FDA_doz_logic.fgi( 759, 565):�
      if(L_(333)) then
         R_arolo=R_(112)
      else
         R_arolo=R_(113)
      endif
C FDA_doz_logic.fgi( 764, 589):���� RE IN LO CH7
      L_(328) = L_(327).AND.L_(326).AND.L_(325).AND.L_(324
     &).AND.L_(334)
C FDA_doz_logic.fgi( 759, 526):�
      if(L_(328)) then
         R_upolo=R_(109)
      else
         R_upolo=R_(110)
      endif
C FDA_doz_logic.fgi( 764, 550):���� RE IN LO CH7
      R_(117) = R_erolo + R_irolo + R_arolo + R_upolo
C FDA_doz_logic.fgi( 754, 643):��������
      R_isolo=R_isolo+deltat/R0_urolo*R_(117)
      if(R_isolo.gt.R0_orolo) then
         R_isolo=R0_orolo
      elseif(R_isolo.lt.R0_asolo) then
         R_isolo=R0_asolo
      endif
C FDA_doz_logic.fgi( 764, 641):����������
      R_esolo=R_isolo
C FDA_doz_logic.fgi( 782, 643):������,20FDA60CONTB01M
      L_(301)=R_omelo.gt.R0_ukalo
C FDA_doz_logic.fgi( 844, 602):���������� >
C label 2520  try2520=try2520-1
      L_(314) = L_(303).AND.L_(302).AND.L_(301).AND.L_(300
     &)
C FDA_doz_logic.fgi( 852, 605):�
      L_(319) = L_(318).AND.L_(317).AND.L_(316).AND.L_(315
     &).AND.L_(314)
C FDA_doz_logic.fgi( 899, 602):�
      if(L_(319)) then
         R_eselo=R_(104)
      else
         R_eselo=R_(105)
      endif
C FDA_doz_logic.fgi( 904, 626):���� RE IN LO CH7
      L_(313) = L_(312).AND.L_(311).AND.L_(310).AND.L_(309
     &).AND.L_(314)
C FDA_doz_logic.fgi( 899, 563):�
      if(L_(313)) then
         R_olelo=R_(101)
      else
         R_olelo=R_(102)
      endif
C FDA_doz_logic.fgi( 904, 587):���� RE IN LO CH7
      L_(308) = L_(307).AND.L_(306).AND.L_(305).AND.L_(304
     &).AND.L_(314)
C FDA_doz_logic.fgi( 899, 524):�
      if(L_(308)) then
         R_ilelo=R_(98)
      else
         R_ilelo=R_(99)
      endif
C FDA_doz_logic.fgi( 904, 548):���� RE IN LO CH7
      R_(106) = R_ulelo + R_eselo + R_olelo + R_ilelo
C FDA_doz_logic.fgi( 884, 642):��������
      R_umelo=R_umelo+deltat/R0_emelo*R_(106)
      if(R_umelo.gt.R0_amelo) then
         R_umelo=R0_amelo
      elseif(R_umelo.lt.R0_imelo) then
         R_umelo=R0_imelo
      endif
C FDA_doz_logic.fgi( 894, 640):����������
      R_omelo=R_umelo
C FDA_doz_logic.fgi( 912, 642):������,20FDA60CONTB02M
      L_(281)=R_ekalo.gt.R0_iduko
C FDA_doz_logic.fgi( 975, 601):���������� >
C label 2560  try2560=try2560-1
      L_(294) = L_(283).AND.L_(282).AND.L_(281).AND.L_(280
     &)
C FDA_doz_logic.fgi( 983, 604):�
      L_(299) = L_(298).AND.L_(297).AND.L_(296).AND.L_(295
     &).AND.L_(294)
C FDA_doz_logic.fgi(1031, 601):�
      if(L_(299)) then
         R_aselo=R_(94)
      else
         R_aselo=R_(95)
      endif
C FDA_doz_logic.fgi(1036, 625):���� RE IN LO CH7
      L_(293) = L_(292).AND.L_(291).AND.L_(290).AND.L_(289
     &).AND.L_(294)
C FDA_doz_logic.fgi(1031, 562):�
      if(L_(293)) then
         R_efalo=R_(91)
      else
         R_efalo=R_(92)
      endif
C FDA_doz_logic.fgi(1036, 586):���� RE IN LO CH7
      L_(288) = L_(287).AND.L_(286).AND.L_(285).AND.L_(284
     &).AND.L_(294)
C FDA_doz_logic.fgi(1031, 523):�
      if(L_(288)) then
         R_afalo=R_(88)
      else
         R_afalo=R_(89)
      endif
C FDA_doz_logic.fgi(1036, 547):���� RE IN LO CH7
      R_(96) = R_ifalo + R_aselo + R_efalo + R_afalo
C FDA_doz_logic.fgi(1015, 641):��������
      R_ikalo=R_ikalo+deltat/R0_ufalo*R_(96)
      if(R_ikalo.gt.R0_ofalo) then
         R_ikalo=R0_ofalo
      elseif(R_ikalo.lt.R0_akalo) then
         R_ikalo=R0_akalo
      endif
C FDA_doz_logic.fgi(1025, 639):����������
      R_ekalo=R_ikalo
C FDA_doz_logic.fgi(1042, 641):������,20FDA60CONTB03M
      R_(79) = (-R_upolo) + (-R_ilelo) + (-R_afalo) + R_ikiko
C FDA_doz_logic.fgi(1020, 485):��������
      R_ekiko=R_ekiko+deltat/R0_ofiko*R_(79)
      if(R_ekiko.gt.R0_ifiko) then
         R_ekiko=R0_ifiko
      elseif(R_ekiko.lt.R0_ufiko) then
         R_ekiko=R0_ufiko
      endif
C FDA_doz_logic.fgi(1029, 483):����������
      R_akiko=R_ekiko
C FDA_doz_logic.fgi(1048, 485):������,20FDA21CW005XQ01_input_
      R_(83) = (-R_arolo) + (-R_olelo) + (-R_efalo) + R_ufoko
C FDA_doz_logic.fgi( 888, 486):��������
      R_ofoko=R_ofoko+deltat/R0_afoko*R_(83)
      if(R_ofoko.gt.R0_udoko) then
         R_ofoko=R0_udoko
      elseif(R_ofoko.lt.R0_efoko) then
         R_ofoko=R0_efoko
      endif
C FDA_doz_logic.fgi( 897, 484):����������
      R_ifoko=R_ofoko
C FDA_doz_logic.fgi( 916, 486):������,20FDA21CW003XQ01_input_
      R_(107) = (-R_irolo) + (-R_eselo) + (-R_aselo) + R_urelo
C FDA_doz_logic.fgi( 744, 486):��������
      R_orelo=R_orelo+deltat/R0_arelo*R_(107)
      if(R_orelo.gt.R0_upelo) then
         R_orelo=R0_upelo
      elseif(R_orelo.lt.R0_erelo) then
         R_orelo=R0_erelo
      endif
C FDA_doz_logic.fgi( 753, 484):����������
      R_irelo=R_orelo
C FDA_doz_logic.fgi( 772, 486):������,20FDA21CW001XQ01_input_
      L_(275)=R_odiko.gt.R0_ofeko
C FDA_doz_logic.fgi(1126, 584):���������� >
C label 2615  try2615=try2615-1
      L_(276) = L_(269).AND.L_(268).AND.L_(273).AND.L_(272
     &).AND.L_(271).AND.L_(270).AND.L_(275).AND.L_(274)
C FDA_doz_logic.fgi(1137, 606):�
      if(L_(276)) then
         R_ubiko=R_(73)
      else
         R_ubiko=R_(74)
      endif
C FDA_doz_logic.fgi(1145, 634):���� RE IN LO CH7
      R_(75) = R_obiko + R_ubiko
C FDA_doz_logic.fgi(1138, 648):��������
      R_udiko=R_udiko+deltat/R0_ediko*R_(75)
      if(R_udiko.gt.R0_adiko) then
         R_udiko=R0_adiko
      elseif(R_udiko.lt.R0_idiko) then
         R_udiko=R0_idiko
      endif
C FDA_doz_logic.fgi(1148, 646):����������
      R_odiko=R_udiko
C FDA_doz_logic.fgi(1166, 648):������,20FDA60CONTS01M
      R_(71) = (-R_ubiko) + R_adeko
C FDA_doz_logic.fgi(1140, 489):��������
      R_ubeko=R_ubeko+deltat/R0_ebeko*R_(71)
      if(R_ubeko.gt.R0_abeko) then
         R_ubeko=R0_abeko
      elseif(R_ubeko.lt.R0_ibeko) then
         R_ubeko=R0_ibeko
      endif
C FDA_doz_logic.fgi(1149, 487):����������
      R_obeko=R_ubeko
C FDA_doz_logic.fgi(1168, 489):������,20FDA21CW007XQ01_input_
      End
