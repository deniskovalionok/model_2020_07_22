      Subroutine FDA20(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA20_ConIn
      ! ��������� ������ ����� FDA20 ����� FDA20_SP1
      L_(322)=.true.
C FDA20_SP1.fgi( 498,  15):pre: ����� 0
      R_(68)=R0_apu
C FDA20_SP2_UVR.fgi( 160,  42):pre: ������������  �� T
      R_(66)=R0_olu
C FDA20_SP2_UVR.fgi( 160,  22):pre: ������������  �� T
      R_(67)=R0_omu
C FDA20_SP2_UVR.fgi(  45,  37):pre: �������� ��������� ������
      R_(83)=R0_epad
C FDA20_SP2_UVR.fgi( 150, 215):pre: ������������  �� T
      R_(170)=R0_aduf
C FDA20_SP2_UVR.fgi( 174, 740):pre: ������������  �� T
      R_(155)=R0_asef
C FDA20_SP2_UVR.fgi( 162, 740):pre: �������� ��������� ������
      R_(158)=R0_evef
C FDA20_SP2_UVR.fgi( 241, 744):pre: ������������  �� T
      R_(41)=R0_afi
C FDA20_SP2_UVR.fgi( 170, 713):pre: ������������  �� T
      R_(40)=R0_idi
C FDA20_SP2_UVR.fgi( 174, 709):pre: ������������  �� T
      R_(37)=R0_oxe
C FDA20_SP2_UVR.fgi( 170, 697):pre: ������������  �� T
      R_(36)=R0_axe
C FDA20_SP2_UVR.fgi( 174, 693):pre: ������������  �� T
      R_(35)=R0_ive
C FDA20_SP2_UVR.fgi( 170, 689):pre: ������������  �� T
      R_(34)=R0_ute
C FDA20_SP2_UVR.fgi( 174, 685):pre: ������������  �� T
      R_(33)=R0_ete
C FDA20_SP2_UVR.fgi( 170, 681):pre: ������������  �� T
      R_(38)=R0_ebi
C FDA20_SP2_UVR.fgi( 174, 701):pre: ������������  �� T
      R_(39)=R0_ubi
C FDA20_SP2_UVR.fgi( 170, 705):pre: ������������  �� T
      R_(42)=R0_ofi
C FDA20_SP2_UVR.fgi( 174, 717):pre: ������������  �� T
      R_(157)=R0_otef
C FDA20_SP2_UVR.fgi( 241, 738):pre: ������������  �� T
      R_(168)=R0_ebof
C FDA20_SP2_UVR.fgi( 170, 655):pre: ������������  �� T
      R_(167)=R0_ixif
C FDA20_SP2_UVR.fgi( 174, 651):pre: ������������  �� T
      R_(164)=R0_atif
C FDA20_SP2_UVR.fgi( 170, 639):pre: ������������  �� T
      R_(163)=R0_esif
C FDA20_SP2_UVR.fgi( 174, 635):pre: ������������  �� T
      R_(162)=R0_irif
C FDA20_SP2_UVR.fgi( 170, 631):pre: ������������  �� T
      R_(161)=R0_opif
C FDA20_SP2_UVR.fgi( 174, 627):pre: ������������  �� T
      R_(160)=R0_umif
C FDA20_SP2_UVR.fgi( 170, 623):pre: ������������  �� T
      R_(165)=R0_utif
C FDA20_SP2_UVR.fgi( 174, 643):pre: ������������  �� T
      R_(166)=R0_ovif
C FDA20_SP2_UVR.fgi( 170, 647):pre: ������������  �� T
      R_(169)=R0_adof
C FDA20_SP2_UVR.fgi( 174, 659):pre: ������������  �� T
      R_(159)=R0_ubif
C FDA20_SP2_UVR.fgi( 119, 616):pre: ������������  �� T
      R_(30)=R0_ere
C FDA20_SP2_UVR.fgi( 167, 594):pre: ������������  �� T
      R_(156)=R0_osef
C FDA20_SP2_UVR.fgi( 167, 600):pre: ������������  �� T
      R_(29)=R0_ope
C FDA20_SP2_UVR.fgi( 167, 582):pre: ������������  �� T
      R_(122)=R0_ixod
C FDA20_SP2_UVR.fgi( 167, 588):pre: ������������  �� T
      R_(28)=R0_ape
C FDA20_SP2_UVR.fgi( 167, 570):pre: ������������  �� T
      R_(121)=R0_evod
C FDA20_SP2_UVR.fgi( 167, 576):pre: ������������  �� T
      R_(27)=R0_ime
C FDA20_SP2_UVR.fgi( 167, 558):pre: ������������  �� T
      R_(120)=R0_atod
C FDA20_SP2_UVR.fgi( 167, 564):pre: ������������  �� T
      R_(192)=R0_etak
C FDA20_SP1.fgi( 415, 713):pre: ������������  �� T
      R_(193)=R0_axak
C FDA20_SP1.fgi( 415, 742):pre: ������������  �� T
      R_(194)=R0_adek
C FDA20_SP1.fgi( 415, 771):pre: ������������  �� T
      R_(195)=R0_akek
C FDA20_SP1.fgi( 415, 800):pre: ������������  �� T
      R_(5)=R0_ek
C FDA20_SP2_UVR.fgi( 119, 536):pre: ������������  �� T
      R_(117)=R0_ifod
C FDA20_SP2_UVR.fgi( 174, 517):pre: ������������  �� T
      R_(116)=R0_udod
C FDA20_SP2_UVR.fgi( 170, 513):pre: ������������  �� T
      R_(115)=R0_edod
C FDA20_SP2_UVR.fgi( 174, 509):pre: ������������  �� T
      R_(114)=R0_obod
C FDA20_SP2_UVR.fgi( 170, 505):pre: ������������  �� T
      R_(118)=R0_akod
C FDA20_SP2_UVR.fgi( 170, 521):pre: ������������  �� T
      R_(119)=R0_okod
C FDA20_SP2_UVR.fgi( 174, 525):pre: ������������  �� T
      R_(22)=R0_ife
C FDA20_SP2_UVR.fgi( 169, 464):pre: ������������  �� T
      R_(100)=R0_exed
C FDA20_SP2_UVR.fgi( 169, 470):pre: ������������  �� T
      R_(111)=R0_ivid
C FDA20_SP2_UVR.fgi( 169, 476):pre: ������������  �� T
      R_(112)=R0_exid
C FDA20_SP2_UVR.fgi( 169, 485):pre: ������������  �� T
      R_(26)=R0_ame
C FDA20_SP2_UVR.fgi( 143, 485):pre: �������� ��������� ������
      R_(113)=R0_uxid
C FDA20_SP2_UVR.fgi( 169, 491):pre: ������������  �� T
      R_(18)=R0_ebe
C FDA20_SP2_UVR.fgi( 123, 491):pre: �������� ������� ������
      R_(109)=R0_atid
C FDA20_SP2_UVR.fgi( 169, 450):pre: ������������  �� T
      R_(25)=R0_ile
C FDA20_SP2_UVR.fgi( 143, 450):pre: �������� ��������� ������
      R_(21)=R0_ude
C FDA20_SP2_UVR.fgi( 169, 430):pre: ������������  �� T
      R_(99)=R0_ived
C FDA20_SP2_UVR.fgi( 169, 436):pre: ������������  �� T
      R_(108)=R0_esid
C FDA20_SP2_UVR.fgi( 169, 442):pre: ������������  �� T
      R_(110)=R0_otid
C FDA20_SP2_UVR.fgi( 169, 456):pre: ������������  �� T
      R_(17)=R0_ox
C FDA20_SP2_UVR.fgi( 123, 456):pre: �������� ������� ������
      R_(106)=R0_upid
C FDA20_SP2_UVR.fgi( 169, 416):pre: ������������  �� T
      R_(24)=R0_uke
C FDA20_SP2_UVR.fgi( 143, 416):pre: �������� ��������� ������
      R_(20)=R0_ede
C FDA20_SP2_UVR.fgi( 169, 396):pre: ������������  �� T
      R_(98)=R0_oted
C FDA20_SP2_UVR.fgi( 169, 402):pre: ������������  �� T
      R_(105)=R0_apid
C FDA20_SP2_UVR.fgi( 169, 408):pre: ������������  �� T
      R_(107)=R0_irid
C FDA20_SP2_UVR.fgi( 169, 422):pre: ������������  �� T
      R_(16)=R0_ax
C FDA20_SP2_UVR.fgi( 123, 422):pre: �������� ������� ������
      R_(103)=R0_olid
C FDA20_SP2_UVR.fgi( 169, 382):pre: ������������  �� T
      R_(23)=R0_eke
C FDA20_SP2_UVR.fgi( 143, 382):pre: �������� ��������� ������
      R_(19)=R0_obe
C FDA20_SP2_UVR.fgi( 169, 362):pre: ������������  �� T
      R_(97)=R0_used
C FDA20_SP2_UVR.fgi( 169, 368):pre: ������������  �� T
      R_(102)=R0_ukid
C FDA20_SP2_UVR.fgi( 169, 374):pre: ������������  �� T
      R_(104)=R0_emid
C FDA20_SP2_UVR.fgi( 169, 389):pre: ������������  �� T
      R_(15)=R0_iv
C FDA20_SP2_UVR.fgi( 123, 389):pre: �������� ������� ������
      R_(87)=R0_ofed
C FDA20_SP2_UVR.fgi( 174, 286):pre: ������������  �� T
      R_(88)=R0_eked
C FDA20_SP2_UVR.fgi( 174, 292):pre: ������������  �� T
      R_(89)=R0_uked
C FDA20_SP2_UVR.fgi( 174, 298):pre: ������������  �� T
      R_(90)=R0_iled
C FDA20_SP2_UVR.fgi( 174, 304):pre: ������������  �� T
      R_(91)=R0_amed
C FDA20_SP2_UVR.fgi( 174, 310):pre: ������������  �� T
      R_(92)=R0_omed
C FDA20_SP2_UVR.fgi( 174, 316):pre: ������������  �� T
      R_(93)=R0_eped
C FDA20_SP2_UVR.fgi( 174, 322):pre: ������������  �� T
      R_(94)=R0_uped
C FDA20_SP2_UVR.fgi( 174, 328):pre: ������������  �� T
      R_(95)=R0_ired
C FDA20_SP2_UVR.fgi( 174, 334):pre: ������������  �� T
      R_(96)=R0_ased
C FDA20_SP2_UVR.fgi( 174, 340):pre: ������������  �� T
      R_(86)=R0_utad
C FDA20_SP2_UVR.fgi( 144, 277):pre: ������������  �� T
      R_(31)=R0_ure
C FDA20_SP2_UVR.fgi( 144, 250):pre: ������������  �� T
      R_(80)=R0_ilad
C FDA20_SP2_UVR.fgi( 167, 205):pre: ������������  �� T
      R_(79)=R0_ukad
C FDA20_SP2_UVR.fgi( 167, 199):pre: ������������  �� T
      R_(82)=R0_umad
C FDA20_SP2_UVR.fgi( 166, 211):pre: �������� ��������� ������
      R_(84)=R0_upad
C FDA20_SP2_UVR.fgi( 137, 223):pre: ������������  �� T
      R_(77)=R0_efad
C FDA20_SP2_UVR.fgi( 167, 183):pre: ������������  �� T
      R_(78)=R0_ufad
C FDA20_SP2_UVR.fgi( 167, 189):pre: ������������  �� T
      R_(65)=R0_uku
C FDA20_SP2_UVR.fgi( 167, 172):pre: ������������  �� T
      R_(74)=R0_exu
C FDA20_SP2_UVR.fgi( 160, 124):pre: ������������  �� T
      R_(73)=R0_ivu
C FDA20_SP2_UVR.fgi( 160, 113):pre: ������������  �� T
      R_(72)=R0_otu
C FDA20_SP2_UVR.fgi( 160, 102):pre: ������������  �� T
      R_(71)=R0_osu
C FDA20_SP2_UVR.fgi( 160,  91):pre: ������������  �� T
      R_(70)=R0_uru
C FDA20_SP2_UVR.fgi( 160,  80):pre: ������������  �� T
      R_(13)=R0_us
C FDA20_SP2_UVR.fgi( 160,  69):pre: ������������  �� T
      R_(69)=R0_upu
C FDA20_SP2_UVR.fgi( 160,  58):pre: ������������  �� T
      R_(1)=R0_i
C FDA20_SP2_UVR.fgi( 420, 789):pre: ������������  �� T
      R_(2)=R0_ad
C FDA20_SP2_UVR.fgi( 420, 795):pre: ������������  �� T
      R_(62)=R0_oso
C FDA20_SP2_UVR.fgi( 403, 777):pre: ������������  �� T
      R_(61)=R0_oro
C FDA20_SP2_UVR.fgi( 403, 766):pre: ������������  �� T
      R_(60)=R0_opo
C FDA20_SP2_UVR.fgi( 403, 751):pre: ������������  �� T
      R_(59)=R0_umo
C FDA20_SP2_UVR.fgi( 403, 740):pre: ������������  �� T
      R_(58)=R0_amo
C FDA20_SP2_UVR.fgi( 403, 729):pre: ������������  �� T
      R_(57)=R0_elo
C FDA20_SP2_UVR.fgi( 403, 718):pre: ������������  �� T
      R_(56)=R0_iko
C FDA20_SP2_UVR.fgi( 403, 707):pre: ������������  �� T
      R_(55)=R0_ifo
C FDA20_SP2_UVR.fgi( 403, 696):pre: ������������  �� T
      R_(54)=R0_odo
C FDA20_SP2_UVR.fgi( 403, 685):pre: ������������  �� T
      R_(53)=R0_ubo
C FDA20_SP2_UVR.fgi( 403, 674):pre: ������������  �� T
      R_(51)=R0_ixi
C FDA20_SP2_UVR.fgi( 403, 659):pre: ������������  �� T
      R_(50)=R0_ovi
C FDA20_SP2_UVR.fgi( 403, 648):pre: ������������  �� T
      R_(49)=R0_uti
C FDA20_SP2_UVR.fgi( 403, 637):pre: ������������  �� T
      R_(48)=R0_ati
C FDA20_SP2_UVR.fgi( 403, 626):pre: ������������  �� T
      R_(47)=R0_esi
C FDA20_SP2_UVR.fgi( 403, 615):pre: ������������  �� T
      R_(46)=R0_iri
C FDA20_SP2_UVR.fgi( 403, 604):pre: ������������  �� T
      R_(14)=R0_it
C FDA20_SP2_UVR.fgi( 403, 593):pre: ������������  �� T
      R_(45)=R0_opi
C FDA20_SP2_UVR.fgi( 403, 582):pre: ������������  �� T
      R_(12)=R8_e !CopyBack
C FDA20_SP2_UVR.fgi( 430, 543):pre: ������-�������: ���������� ��� �������������� ������
      R_(9)=R0_ep
C FDA20_SP2_UVR.fgi( 422, 538):pre: ������������  �� T
      R_(8)=R0_im
C FDA20_SP2_UVR.fgi( 403, 520):pre: ������������  �� T
      R_(64)=R0_efu
C FDA20_SP2_UVR.fgi( 353, 822):pre: ������������  �� T
      R_(75)=R0_ebad
C FDA20_SP2_UVR.fgi( 122, 153):pre: ������������  �� T
      R_(43)=R0_iki
C FDA20_SP2_UVR.fgi(  45, 769):pre: �������� ��������� ������
      R_(32)=R0_use
C FDA20_SP2_UVR.fgi(  45, 653):pre: �������� ��������� ������
      R_(6)=R0_al
C FDA20_SP2_UVR.fgi(  69, 550):pre: �������� ��������� ������
      R_(101)=R0_akid
C FDA20_SP2_UVR.fgi(  69, 351):pre: �������� ��������� ������
      R_(85)=R0_itad
C FDA20_SP2_UVR.fgi(  44, 264):pre: �������� ��������� ������
      R_(81)=R0_emad
C FDA20_SP2_UVR.fgi( 138, 215):pre: �������� ��������� ������
      R_(76)=R0_udad
C FDA20_SP2_UVR.fgi(  44, 185):pre: �������� ��������� ������
      R_(44)=R0_umi
C FDA20_SP2_UVR.fgi( 404, 500):pre: �������� ��������� ������
      R_(4)=R0_if
C FDA20_SP2_UVR.fgi( 402, 798):pre: �������� ��������� ������
      R_(52)=R0_ibo
C FDA20_SP2_UVR.fgi( 404, 668):pre: �������� ��������� ������
      R_(3)=R0_ud
C FDA20_SP2_UVR.fgi( 402, 566):pre: �������� ��������� ������
      R_(11)=R0_ur
C FDA20_SP2_UVR.fgi( 404, 548):pre: �������� ��������� ������
      R_(10)=R0_ar
C FDA20_SP2_UVR.fgi( 404, 531):pre: �������� ��������� ������
      R_(216)=R0_evik
C FDA20_SP1.fgi( 445, 272):pre: ������������  �� T
      R_(214)=R0_osik
C FDA20_SP1.fgi( 445, 250):pre: ������������  �� T
      R_(213)=R0_urik
C FDA20_SP1.fgi( 445, 239):pre: ������������  �� T
      R_(212)=R0_arik
C FDA20_SP1.fgi( 445, 228):pre: ������������  �� T
      R_(210)=R0_imik
C FDA20_SP1.fgi( 445, 206):pre: ������������  �� T
      R_(228)=R0_osok
C FDA20_SP1.fgi( 445, 393):pre: ������������  �� T
      R_(227)=R0_orok
C FDA20_SP1.fgi( 445, 382):pre: ������������  �� T
      R_(226)=R0_ipok
C FDA20_SP1.fgi( 445, 371):pre: ������������  �� T
      R_(205)=R0_udik
C FDA20_SP1.fgi( 445, 169):pre: ������������  �� T
      R_(223)=R0_alok
C FDA20_SP1.fgi( 445, 338):pre: ������������  �� T
      R_(222)=R0_ekok
C FDA20_SP1.fgi( 445, 327):pre: ������������  �� T
      R_(221)=R0_efok
C FDA20_SP1.fgi( 445, 316):pre: ������������  �� T
      R_(219)=R0_ibok
C FDA20_SP1.fgi( 445, 294):pre: ������������  �� T
      R_(268)=R0_abil
C FDA20_SP1.fgi( 541, 776):pre: ������������  �� T
      R_(269)=R0_obil
C FDA20_SP1.fgi( 534, 786):pre: ������������  �� T
      R_(200)=R0_etek
C FDA20_SP1.fgi( 125, 194):pre: ������������  �� T
      R_(263)=R0_usel
C FDA20_SP1.fgi( 534, 780):pre: ������������  �� T
      R_(267)=R0_exel
C FDA20_SP1.fgi( 541, 760):pre: ������������  �� T
      R_(188)=R0_amak
C FDA20_SP1.fgi( 465, 561):pre: ������������  �� T
      R_(180)=R0_uvuf
C FDA20_SP1.fgi( 456, 561):pre: �������� ��������� ������
      R_(176)=R0_esuf
C FDA20_SP1.fgi( 465, 532):pre: ������������  �� T
      R_(189)=R0_omak
C FDA20_SP1.fgi( 465, 567):pre: ������������  �� T
      R_(181)=R0_ixuf
C FDA20_SP1.fgi( 456, 567):pre: �������� ��������� ������
      R_(177)=R0_usuf
C FDA20_SP1.fgi( 465, 538):pre: ������������  �� T
      R_(190)=R0_epak
C FDA20_SP1.fgi( 465, 573):pre: ������������  �� T
      R_(182)=R0_abak
C FDA20_SP1.fgi( 456, 573):pre: �������� ��������� ������
      R_(178)=R0_ituf
C FDA20_SP1.fgi( 465, 544):pre: ������������  �� T
      R_(191)=R0_upak
C FDA20_SP1.fgi( 465, 579):pre: ������������  �� T
      R_(183)=R0_obak
C FDA20_SP1.fgi( 456, 579):pre: �������� ��������� ������
      R_(179)=R0_avuf
C FDA20_SP1.fgi( 465, 550):pre: ������������  �� T
      R_(201)=R0_ovek
C FDA20_SP1.fgi( 158, 139):pre: ������������  �� T
      R_(264)=R0_itel
C FDA20_SP1.fgi( 541, 748):pre: ������������  �� T
      R_(204)=R0_edik
C FDA20_SP1.fgi( 445, 287):pre: ������������  �� T
      R_(203)=R0_obik
C FDA20_SP1.fgi( 445, 162):pre: ������������  �� T
      R_(232)=R0_ixok
C FDA20_SP1.fgi( 551, 441):pre: �������� ��������� ������
      R_(224)=R0_ulok
C FDA20_SP1.fgi( 445, 349):pre: ������������  �� T
      R_(233)=R0_efuk
C FDA20_SP1.fgi( 445, 436):pre: ������������  �� T
      R_(231)=R0_uvok
C FDA20_SP1.fgi( 551, 426):pre: �������� ��������� ������
      R_(207)=R0_ikik
C FDA20_SP1.fgi( 445, 199):pre: ������������  �� T
      R_(211)=R0_epik
C FDA20_SP1.fgi( 445, 217):pre: ������������  �� T
      R_(209)=R0_olik
C FDA20_SP1.fgi( 462, 191):pre: ������������  �� T
      R_(208)=R0_elik
C FDA20_SP1.fgi( 380, 191):pre: �������� ��������� ������
      R_(225)=R0_omok
C FDA20_SP1.fgi( 445, 360):pre: ������������  �� T
      R_(230)=R0_evok
C FDA20_SP1.fgi( 551, 410):pre: �������� ��������� ������
      R_(220)=R0_idok
C FDA20_SP1.fgi( 445, 305):pre: ������������  �� T
      R_(215)=R0_itik
C FDA20_SP1.fgi( 445, 261):pre: ������������  �� T
      R_(218)=R0_oxik
C FDA20_SP1.fgi( 392, 497):pre: ������������  �� T
      R_(217)=R0_axik
C FDA20_SP1.fgi( 392, 503):pre: ������������  �� T
      R_(234)=R0_akuk
C FDA20_SP1.fgi( 392, 486):pre: ������������  �� T
      R_(229)=R0_itok
C FDA20_SP1.fgi( 445, 404):pre: ������������  �� T
      R_(206)=R0_ofik
C FDA20_SP1.fgi( 445, 180):pre: ������������  �� T
      R_(202)=R0_oxek
C FDA20_SP1.fgi(  57, 145):pre: �������� ��������� ������
      R_(249)=R0_umal
C FDA20_SP1.fgi( 118, 327):pre: ������������  �� T
      R_(250)=R0_iral
C FDA20_SP1.fgi( 118, 316):pre: ������������  �� T
      R_(248)=R0_ilal
C FDA20_SP1.fgi( 118, 305):pre: ������������  �� T
      R_(241)=R0_asuk
C FDA20_SP1.fgi( 118, 283):pre: ������������  �� T
      R_(186)=R0_ekak
C FDA20_SP1.fgi( 118, 253):pre: ������������  �� T
      R_(243)=R0_otuk
C FDA20_SP1.fgi( 118, 340):pre: ������������  �� T
      R_(242)=R0_etuk
C FDA20_SP1.fgi(  68, 340):pre: �������� ��������� ������
      R_(244)=R0_ivuk
C FDA20_SP1.fgi( 118, 355):pre: ������������  �� T
      R_(251)=R0_usal
C FDA20_SP1.fgi( 125, 426):pre: ������������  �� T
      R_(173)=R0_ipuf
C FDA20_SP1.fgi( 422, 560):pre: ������������  �� T
      R_(236)=R0_amuk
C FDA20_SP1.fgi( 225, 360):pre: ������������  �� T
      R_(254)=R0_axal
C FDA20_SP1.fgi( 158, 388):pre: ������������  �� T
      R_(255)=R0_abel
C FDA20_SP1.fgi(  57, 394):pre: �������� ��������� ������
      R_(174)=R0_aruf
C FDA20_SP1.fgi( 422, 566):pre: ������������  �� T
      R_(175)=R0_uruf
C FDA20_SP1.fgi( 252, 711):pre: �������� ������� ������
      R_(238)=R0_epuk
C FDA20_SP1.fgi( 215, 764):pre: ������������  �� T
      R_(276)=R0_ofol
C FDA20_SP1.fgi( 118, 735):pre: ������������  �� T
      R_(246)=R0_adal
C FDA20_SP1.fgi( 118, 724):pre: ������������  �� T
      R_(245)=R0_oxuk
C FDA20_SP1.fgi( 118, 691):pre: ������������  �� T
      R_(275)=R0_afol
C FDA20_SP1.fgi( 118, 680):pre: ������������  �� T
      R_(187)=R0_alak
C FDA20_SP1.fgi( 118, 653):pre: ������������  �� T
      R_(272)=R0_ilil
C FDA20_SP1.fgi( 118, 614):pre: ������������  �� T
      R_(271)=R0_okil
C FDA20_SP1.fgi( 118, 603):pre: ������������  �� T
      R_(270)=R0_ufil
C FDA20_SP1.fgi( 118, 592):pre: ������������  �� T
      R_(253)=R0_ival
C FDA20_SP1.fgi( 125, 521):pre: ������������  �� T
      R_(252)=R0_otal
C FDA20_SP1.fgi( 125, 473):pre: ������������  �� T
      R_(256)=R0_afel
C FDA20_SP1.fgi( 158, 435):pre: ������������  �� T
      R_(257)=R0_akel
C FDA20_SP1.fgi(  57, 441):pre: �������� ��������� ������
      R_(247)=R0_ifal
C FDA20_SP1.fgi(  65, 746):pre: �������� ��������� ������
      R_(240)=R0_iruk
C FDA20_SP1.fgi( 118, 746):pre: ������������  �� T
      R_(259)=R0_olel
C FDA20_SP1.fgi( 158, 483):pre: ������������  �� T
      R_(260)=R0_ipel
C FDA20_SP1.fgi(  57, 489):pre: �������� ��������� ������
      R_(330)=R8_ekes
C FDA20_vent_log.fgi( 101, 271):pre: �������������� �����  
      R_(326)=R8_efes
C FDA20_vent_log.fgi( 177, 271):pre: �������������� �����  
      R_(291)=R8_asas
C FDA20_vent_log.fgi( 105, 212):pre: �������������� �����  
      R_(278)=R0_uvup
C FDA20_lamp.fgi( 248, 613):pre: ������������  �� T
      R_(277)=R0_evup
C FDA20_lamp.fgi( 248, 621):pre: ������������  �� T
      R_(273)=R0_ibol
C FDA20_SP1.fgi(  74, 815):pre: ������������  �� T
      R_(258)=R0_alel
C FDA20_SP1.fgi( 118, 586):pre: ������������  �� T
      R_(266)=R0_ovel
C FDA20_SP1.fgi( 541, 792):pre: ������������  �� T
      R_(265)=R0_avel
C FDA20_SP1.fgi( 541, 754):pre: ������������  �� T
      R_(262)=R0_esel
C FDA20_SP1.fgi( 534, 770):pre: ������������  �� T
      R_(261)=R0_orel
C FDA20_SP1.fgi( 534, 764):pre: ������������  �� T
      R_(239)=R0_upuk
C FDA20_SP1.fgi( 126, 685):pre: ������������  �� T
      R_(237)=R0_omuk
C FDA20_SP1.fgi( 222, 707):pre: ������������  �� T
      R_(235)=R0_iluk
C FDA20_SP1.fgi( 236, 355):pre: ������������  �� T
      R_(274)=R0_adol
C FDA20_SP1.fgi(  74, 809):pre: ������������  �� T
      R_(172)=R0_oluf
C FDA20_SP2_UVR.fgi(  54, 826):pre: ������������  �� T
      R_(171)=R0_aluf
C FDA20_SP2_UVR.fgi(  54, 832):pre: ������������  �� T
      R_(145)=R0_epud
C FDA20_SP2_UVR.fgi( 521, 179):pre: �������� ��������� ������
      R_(63)=R0_odu
C FDA20_SP2_UVR.fgi( 353, 828):pre: ������������  �� T
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_olefe,4),R8_ofefe
     &,I_arefe,I_orefe,I_upefe,
     & C8_ekefe,I_irefe,R_alefe,R_ukefe,R_obefe,
     & REAL(R_adefe,4),R_udefe,REAL(R_efefe,4),
     & R_edefe,REAL(R_odefe,4),I_ipefe,I_urefe,I_erefe,I_epefe
     &,L_ifefe,
     & L_esefe,L_etefe,L_afefe,L_idefe,
     & L_akefe,L_ufefe,L_osefe,L_ubefe,L_okefe,L_utefe,L_elefe
     &,
     & L_ilefe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(552
     &),L_uxafe,L_(553),
     & L_abefe,L_isefe,I_asefe,L_itefe,R_apefe,REAL(R_ikefe
     &,4),L_otefe,L_ebefe,
     & L_avefe,L_ibefe,L_ulefe,L_amefe,L_emefe,L_omefe,L_umefe
     &,L_imefe)
      !}

      if(L_umefe.or.L_omefe.or.L_imefe.or.L_emefe.or.L_amefe.or.L_ulefe
     &) then      
                  I_opefe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opefe = z'40000000'
      endif
C FDA20_vlv.fgi( 157, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR003KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ukade,4),R8_udade
     &,I_epade,I_upade,I_apade,
     & C8_ifade,I_opade,R_ekade,R_akade,R_uxube,
     & REAL(R_ebade,4),R_adade,REAL(R_idade,4),
     & R_ibade,REAL(R_ubade,4),I_omade,I_arade,I_ipade,I_imade
     &,L_odade,
     & L_irade,L_isade,L_edade,L_obade,
     & L_efade,L_afade,L_urade,L_abade,L_ufade,L_atade,L_ikade
     &,
     & L_okade,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(538
     &),L_axube,L_(539),
     & L_exube,L_orade,I_erade,L_osade,R_emade,REAL(R_ofade
     &,4),L_usade,L_ixube,
     & L_etade,L_oxube,L_alade,L_elade,L_ilade,L_ulade,L_amade
     &,L_olade)
      !}

      if(L_amade.or.L_ulade.or.L_olade.or.L_ilade.or.L_elade.or.L_alade
     &) then      
                  I_umade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_umade = z'40000000'
      endif
C FDA20_vlv.fgi( 212, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR110KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_axide,4),R8_atide
     &,I_idode,I_afode,I_edode,
     & C8_otide,I_udode,R_ivide,R_evide,R_aride,
     & REAL(R_iride,4),R_eside,REAL(R_oside,4),
     & R_oride,REAL(R_aside,4),I_ubode,I_efode,I_odode,I_obode
     &,L_uside,
     & L_ofode,L_okode,L_iside,L_uride,
     & L_itide,L_etide,L_akode,L_eride,L_avide,L_elode,L_ovide
     &,
     & L_uvide,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(544
     &),L_epide,L_(545),
     & L_ipide,L_ufode,I_ifode,L_ukode,R_ibode,REAL(R_utide
     &,4),L_alode,L_opide,
     & L_ilode,L_upide,L_exide,L_ixide,L_oxide,L_abode,L_ebode
     &,L_uxide)
      !}

      if(L_ebode.or.L_abode.or.L_uxide.or.L_oxide.or.L_ixide.or.L_exide
     &) then      
                  I_adode = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_adode = z'40000000'
      endif
C FDA20_vlv.fgi( 157, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR107KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_epafe,4),R8_elafe
     &,I_osafe,I_etafe,I_isafe,
     & C8_ulafe,I_atafe,R_omafe,R_imafe,R_efafe,
     & REAL(R_ofafe,4),R_ikafe,REAL(R_ukafe,4),
     & R_ufafe,REAL(R_ekafe,4),I_asafe,I_itafe,I_usafe,I_urafe
     &,L_alafe,
     & L_utafe,L_uvafe,L_okafe,L_akafe,
     & L_olafe,L_ilafe,L_evafe,L_ifafe,L_emafe,L_ixafe,L_umafe
     &,
     & L_apafe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(550
     &),L_idafe,L_(551),
     & L_odafe,L_avafe,I_otafe,L_axafe,R_orafe,REAL(R_amafe
     &,4),L_exafe,L_udafe,
     & L_oxafe,L_afafe,L_ipafe,L_opafe,L_upafe,L_erafe,L_irafe
     &,L_arafe)
      !}

      if(L_irafe.or.L_erafe.or.L_arafe.or.L_upafe.or.L_opafe.or.L_ipafe
     &) then      
                  I_esafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esafe = z'40000000'
      endif
C FDA20_vlv.fgi( 175, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR004KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_obide,4),R8_ovede
     &,I_akide,I_okide,I_ufide,
     & C8_exede,I_ikide,R_abide,R_uxede,R_osede,
     & REAL(R_atede,4),R_utede,REAL(R_evede,4),
     & R_etede,REAL(R_otede,4),I_ifide,I_ukide,I_ekide,I_efide
     &,L_ivede,
     & L_elide,L_emide,L_avede,L_itede,
     & L_axede,L_uvede,L_olide,L_usede,L_oxede,L_umide,L_ebide
     &,
     & L_ibide,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(542
     &),L_urede,L_(543),
     & L_asede,L_ilide,I_alide,L_imide,R_afide,REAL(R_ixede
     &,4),L_omide,L_esede,
     & L_apide,L_isede,L_ubide,L_adide,L_edide,L_odide,L_udide
     &,L_idide)
      !}

      if(L_udide.or.L_odide.or.L_idide.or.L_edide.or.L_adide.or.L_ubide
     &) then      
                  I_ofide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ofide = z'40000000'
      endif
C FDA20_vlv.fgi( 175, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR108KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_urude,4),R8_umude
     &,I_evude,I_uvude,I_avude,
     & C8_ipude,I_ovude,R_erude,R_arude,R_ukude,
     & REAL(R_elude,4),R_amude,REAL(R_imude,4),
     & R_ilude,REAL(R_ulude,4),I_otude,I_axude,I_ivude,I_itude
     &,L_omude,
     & L_ixude,L_ibafe,L_emude,L_olude,
     & L_epude,L_apude,L_uxude,L_alude,L_upude,L_adafe,L_irude
     &,
     & L_orude,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(548
     &),L_akude,L_(549),
     & L_ekude,L_oxude,I_exude,L_obafe,R_etude,REAL(R_opude
     &,4),L_ubafe,L_ikude,
     & L_edafe,L_okude,L_asude,L_esude,L_isude,L_usude,L_atude
     &,L_osude)
      !}

      if(L_atude.or.L_usude.or.L_osude.or.L_isude.or.L_esude.or.L_asude
     &) then      
                  I_utude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utude = z'40000000'
      endif
C FDA20_vlv.fgi( 194, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR005KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_efede,4),R8_ebede
     &,I_olede,I_emede,I_ilede,
     & C8_ubede,I_amede,R_odede,R_idede,R_evade,
     & REAL(R_ovade,4),R_ixade,REAL(R_uxade,4),
     & R_uvade,REAL(R_exade,4),I_alede,I_imede,I_ulede,I_ukede
     &,L_abede,
     & L_umede,L_upede,L_oxade,L_axade,
     & L_obede,L_ibede,L_epede,L_ivade,L_edede,L_irede,L_udede
     &,
     & L_afede,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(540
     &),L_itade,L_(541),
     & L_otade,L_apede,I_omede,L_arede,R_okede,REAL(R_adede
     &,4),L_erede,L_utade,
     & L_orede,L_avade,L_ifede,L_ofede,L_ufede,L_ekede,L_ikede
     &,L_akede)
      !}

      if(L_ikede.or.L_ekede.or.L_akede.or.L_ufede.or.L_ofede.or.L_ifede
     &) then      
                  I_elede = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_elede = z'40000000'
      endif
C FDA20_vlv.fgi( 194, 150):���� ���������� �������� � ���������������� ��������,20FDA21BR109KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_itode,4),R8_irode
     &,I_uxode,I_ibude,I_oxode,
     & C8_asode,I_ebude,R_usode,R_osode,R_imode,
     & REAL(R_umode,4),R_opode,REAL(R_arode,4),
     & R_apode,REAL(R_ipode,4),I_exode,I_obude,I_abude,I_axode
     &,L_erode,
     & L_adude,L_afude,L_upode,L_epode,
     & L_urode,L_orode,L_idude,L_omode,L_isode,L_ofude,L_atode
     &,
     & L_etode,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_(546
     &),L_olode,L_(547),
     & L_ulode,L_edude,I_ubude,L_efude,R_uvode,REAL(R_esode
     &,4),L_ifude,L_amode,
     & L_ufude,L_emode,L_otode,L_utode,L_avode,L_ivode,L_ovode
     &,L_evode)
      !}

      if(L_ovode.or.L_ivode.or.L_evode.or.L_avode.or.L_utode.or.L_otode
     &) then      
                  I_ixode = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixode = z'40000000'
      endif
C FDA20_vlv.fgi( 212, 176):���� ���������� �������� � ���������������� ��������,20FDA23BR006KA12
      L_(66) = L_eko.AND.L_exo
C FDA20_SP2_UVR.fgi( 342, 707):�
      L_(82) = L_ito.AND.L_uf
C FDA20_SP2_UVR.fgi( 339, 802):�
      L_(202) = L_osom.AND.L_erip.AND.L_okip.AND.L_ak
C FDA20_SP2_UVR.fgi(  58, 536):�
      !��������� R_(7) = FDA20_SP2_UVRC?? /4.0/
      R_(7)=R0_am
C FDA20_SP2_UVR.fgi( 424, 544):���������
      L_(130)=.false.
C FDA20_SP2_UVR.fgi( 186,  46):��������� ���������� (�������)
      L_(103)=.true.
C FDA20_SP2_UVR.fgi(  52,  42):��������� ���������� (�������)
      L_(131)=.false.
C FDA20_SP2_UVR.fgi( 163, 275):��������� ���������� (�������)
      L_(29) = L_imif.AND.L_emif.AND.L_amif.AND.L_ulif.AND.L_olif.AND.L_
     &ilif.AND.L_elif.AND.L_alif.AND.L_ukif.AND.L_okif
C FDA20_SP2_UVR.fgi(  57, 250):�
      if(L_ufu.and..not.L0_afu) then
         R0_odu=R0_udu
      else
         R0_odu=max(R_(63)-deltat,0.0)
      endif
      L_oku=R0_odu.gt.0.0
      L0_afu=L_ufu
C FDA20_SP2_UVR.fgi( 353, 828):������������  �� T
      L_upaf=.false.
C FDA20_SP2_UVR.fgi( 170, 149):��������� ���������� (�������)
      L_araf=.true.
C FDA20_SP2_UVR.fgi( 170, 145):��������� ���������� (�������)
      L_eraf=.false.
C FDA20_SP2_UVR.fgi( 170, 141):��������� ���������� (�������)
      L_iraf=.true.
C FDA20_SP2_UVR.fgi( 170, 137):��������� ���������� (�������)
      L_irad=.false.
C FDA20_SP2_UVR.fgi( 170, 232):��������� ���������� (�������)
      L_isad=.false.
C FDA20_SP2_UVR.fgi( 142, 232):��������� ���������� (�������)
      L_(142) = L_uxof.AND.L_ulod.AND.L_oxof.AND.L_olod.AND.L_ixof.AND.L
     &_ilod.AND.L_exof.AND.L_elod.AND.L_axof.AND.L_udof
C FDA20_SP2_UVR.fgi(  55, 340):�
      L_(168) = L_ited.AND.L_ilid.AND.L_ebuf
C FDA20_SP2_UVR.fgi(  55, 389):�
      L_(176) = L_eved.AND.L_opid.AND.L_ibuf
C FDA20_SP2_UVR.fgi(  55, 422):�
      L_(184) = L_axed.AND.L_usid.AND.L_obuf
C FDA20_SP2_UVR.fgi(  55, 456):�
      L_(193) = L_uxed.AND.L_axid.AND.L_ubuf
C FDA20_SP2_UVR.fgi(  55, 491):�
      !��������� R_(123) = FDA20_SP2_UVRCmarginweight /0.5
C /
      R_(123)=R0_ibud
C FDA20_SP2_UVR.fgi( 534, 297):���������,marginweight
      L_uraf=.false.
C FDA20_SP2_UVR.fgi( 556, 317):��������� ���������� (�������)
      L_usud = L_esaf.OR.L_isaf.OR.L_osaf.OR.L_usaf.OR.L_ataf.OR.L_etaf.
     &OR.L_itaf.OR.L_otaf.OR.L_utaf
C FDA20_SP2_UVR.fgi( 545, 340):���
      !��������� R_(126) = FDA20_SP2_UVRCfweight /10.5/
      R_(126)=R0_obud
C FDA20_SP2_UVR.fgi( 527, 106):���������,fweight
      R_(135) = 0.0
C FDA20_SP2_UVR.fgi( 544,  61):��������� (RE4) (�������)
      if(L_ovud) then
         R_(134)=R_ifud
      else
         R_(134)=R_(135)
      endif
C FDA20_SP2_UVR.fgi( 543,  58):���� RE IN LO CH7
      if(L_ivud) then
         R_(133)=R_efud
      else
         R_(133)=R_(134)
      endif
C FDA20_SP2_UVR.fgi( 539,  54):���� RE IN LO CH7
      if(L_evud) then
         R_(132)=R_afud
      else
         R_(132)=R_(133)
      endif
C FDA20_SP2_UVR.fgi( 535,  50):���� RE IN LO CH7
      if(L_avud) then
         R_(131)=R_udud
      else
         R_(131)=R_(132)
      endif
C FDA20_SP2_UVR.fgi( 531,  46):���� RE IN LO CH7
      if(L_utud) then
         R_(130)=R_odud
      else
         R_(130)=R_(131)
      endif
C FDA20_SP2_UVR.fgi( 527,  42):���� RE IN LO CH7
      if(L_otud) then
         R_(129)=R_idud
      else
         R_(129)=R_(130)
      endif
C FDA20_SP2_UVR.fgi( 523,  38):���� RE IN LO CH7
      if(L_itud) then
         R_(128)=R_edud
      else
         R_(128)=R_(129)
      endif
C FDA20_SP2_UVR.fgi( 519,  34):���� RE IN LO CH7
      if(L_etud) then
         R_(127)=R_adud
      else
         R_(127)=R_(128)
      endif
C FDA20_SP2_UVR.fgi( 515,  30):���� RE IN LO CH7
      if(L_atud) then
         R_ivis=R_ubud
      else
         R_ivis=R_(127)
      endif
C FDA20_SP2_UVR.fgi( 511,  26):���� RE IN LO CH7
      !{
      Call DAT_ANA_HANDLER(deltat,R_etis,R_abos,REAL(1,4)
     &,
     & REAL(R_utis,4),REAL(R_avis,4),
     & REAL(R_atis,4),REAL(R_usis,4),I_uxis,
     & REAL(R_ovis,4),L_uvis,REAL(R_axis,4),L_exis,L_ixis
     &,R_evis,
     & REAL(R_otis,4),REAL(R_itis,4),L_oxis,REAL(R_ivis,4
     &))
      !}
C FDA20_vlv.fgi( 316,  71):���������� �������,20FDA22CT001XQ01
      R_(144) = 0.0
C FDA20_SP2_UVR.fgi( 544, 139):��������� (RE4) (�������)
      L_umud=.false.
C FDA20_SP2_UVR.fgi( 530, 175):��������� ���������� (�������)
      L_apaf = L_isud.OR.L_esud.OR.L_asud.OR.L_urud.OR.L_orud.OR.L_irud.
     &OR.L_erud.OR.L_arud.OR.L_upud
C FDA20_SP2_UVR.fgi( 504, 186):���
      if(.not.L_apaf) then
         R0_epud=0.0
      elseif(.not.L0_opud) then
         R0_epud=R0_apud
      else
         R0_epud=max(R_(145)-deltat,0.0)
      endif
      L_ipud=L_apaf.and.R0_epud.le.0.0
      L0_opud=L_apaf
C FDA20_SP2_UVR.fgi( 521, 179):�������� ��������� ������
      L_elaf = L_ovud.OR.L_ivud.OR.L_evud.OR.L_avud.OR.L_utud.OR.L_otud.
     &OR.L_itud.OR.L_etud.OR.L_atud
C FDA20_SP2_UVR.fgi( 504, 207):���
      L_ilaf = L_ibaf.OR.L_ebaf.OR.L_abaf.OR.L_uxud.OR.L_oxud.OR.L_ixud.
     &OR.L_exud.OR.L_axud.OR.L_uvud
C FDA20_SP2_UVR.fgi( 504, 227):���
      L_olaf = L_efaf.OR.L_afaf.OR.L_udaf.OR.L_odaf.OR.L_idaf.OR.L_edaf.
     &OR.L_adaf.OR.L_ubaf.OR.L_obaf
C FDA20_SP2_UVR.fgi( 504, 247):���
      L_ulaf = L_alaf.OR.L_ukaf.OR.L_okaf.OR.L_ikaf.OR.L_ekaf.OR.L_akaf.
     &OR.L_ufaf.OR.L_ofaf.OR.L_ifaf
C FDA20_SP2_UVR.fgi( 504, 267):���
      L_omaf=L_umaf
C FDA20_SP2_UVR.fgi( 482, 643):������,unloading_pos
      R_(154) = 0.0
C FDA20_SP2_UVR.fgi( 540, 330):��������� (RE4) (�������)
      if(L_utaf) then
         R_(153)=R_oxaf
      else
         R_(153)=R_(154)
      endif
C FDA20_SP2_UVR.fgi( 539, 327):���� RE IN LO CH7
      if(L_otaf) then
         R_(152)=R_ixaf
      else
         R_(152)=R_(153)
      endif
C FDA20_SP2_UVR.fgi( 535, 323):���� RE IN LO CH7
      if(L_itaf) then
         R_(151)=R_exaf
      else
         R_(151)=R_(152)
      endif
C FDA20_SP2_UVR.fgi( 531, 319):���� RE IN LO CH7
      if(L_etaf) then
         R_(150)=R_axaf
      else
         R_(150)=R_(151)
      endif
C FDA20_SP2_UVR.fgi( 527, 315):���� RE IN LO CH7
      if(L_ataf) then
         R_(149)=R_uvaf
      else
         R_(149)=R_(150)
      endif
C FDA20_SP2_UVR.fgi( 523, 311):���� RE IN LO CH7
      if(L_usaf) then
         R_(148)=R_ovaf
      else
         R_(148)=R_(149)
      endif
C FDA20_SP2_UVR.fgi( 519, 307):���� RE IN LO CH7
      if(L_osaf) then
         R_(147)=R_ivaf
      else
         R_(147)=R_(148)
      endif
C FDA20_SP2_UVR.fgi( 515, 303):���� RE IN LO CH7
      if(L_isaf) then
         R_(146)=R_evaf
      else
         R_(146)=R_(147)
      endif
C FDA20_SP2_UVR.fgi( 511, 299):���� RE IN LO CH7
      if(L_esaf) then
         R_ukof=R_avaf
      else
         R_ukof=R_(146)
      endif
C FDA20_SP2_UVR.fgi( 507, 295):���� RE IN LO CH7
      R_asaf=R_ukof
C FDA20_SP2_UVR.fgi( 544, 293):������,fda_uvr_w1_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_akud,R_omud,REAL(1,4)
     &,
     & REAL(R_okud,4),REAL(R_ukud,4),
     & REAL(R_ufud,4),REAL(R_ofud,4),I_imud,
     & REAL(R_elud,4),L_ilud,REAL(R_olud,4),L_ulud,L_amud
     &,R_alud,
     & REAL(R_ikud,4),REAL(R_ekud,4),L_emud,REAL(R_asaf,4
     &))
      !}
C FDA20_SP2_UVR.fgi( 574, 279):���������� �������,fda_uvr_w1
      !{
      Call DAT_ANA_HANDLER(deltat,R_ofof,R_imof,REAL(1,4)
     &,
     & REAL(R_ekof,4),REAL(R_ikof,4),
     & REAL(R_ifof,4),REAL(R_efof,4),I_emof,
     & REAL(R_alof,4),L_elof,REAL(R_ilof,4),L_olof,L_ulof
     &,R_okof,
     & REAL(R_akof,4),REAL(R_ufof,4),L_amof,REAL(R_ukof,4
     &))
      !}
C FDA20_SP2_UVR.fgi( 574, 293):���������� �������,fda_uvr_w3
      R_(125) = (-R_ukof) + R_ukof
C FDA20_SP2_UVR.fgi( 527, 303):��������
      R_(124)=abs(R_(125))
C FDA20_SP2_UVR.fgi( 533, 303):���������� ��������
      L_(236)=R_(124).lt.R_(123)
C FDA20_SP2_UVR.fgi( 545, 302):���������� <
      L_osud = L_usud.AND.L_(236)
C FDA20_SP2_UVR.fgi( 554, 313):�
      L_oraf = L_usud.AND.(.NOT.L_(236))
C FDA20_SP2_UVR.fgi( 554, 309):�
      L_uxaf = L_odef.OR.L_idef.OR.L_edef.OR.L_adef.OR.L_ubef.OR.L_obef.
     &OR.L_ibef.OR.L_ebef.OR.L_abef
C FDA20_SP2_UVR.fgi( 504, 362):���
      L_udef = L_okef.OR.L_ikef.OR.L_ekef.OR.L_akef.OR.L_ufef.OR.L_ofef.
     &OR.L_ifef.OR.L_efef.OR.L_afef
C FDA20_SP2_UVR.fgi( 504, 382):���
      L_ukef = L_omef.OR.L_imef.OR.L_emef.OR.L_amef.OR.L_ulef.OR.L_olef.
     &OR.L_ilef.OR.L_elef.OR.L_alef
C FDA20_SP2_UVR.fgi( 504, 402):���
      L_umef = L_oref.OR.L_iref.OR.L_eref.OR.L_aref.OR.L_upef.OR.L_opef.
     &OR.L_ipef.OR.L_epef.OR.L_apef
C FDA20_SP2_UVR.fgi( 504, 422):���
      L_(249) = L_ubuf.AND.L_obuf.AND.L_ibuf.AND.L_ebuf.AND.L_abuf.AND.L
     &_uxof.AND.L_oxof.AND.L_ixof.AND.L_exof.AND.L_axof
C FDA20_SP2_UVR.fgi(  57, 730):�
      L_(255) = L_obif.AND.L_ibif.AND.L_ebif.AND.L_abif.AND.L_uxef.AND.L
     &_oxef.AND.L_ixef.AND.L_exef.AND.L_axef.AND.L_uvef
C FDA20_SP2_UVR.fgi(  57, 616):�
      L_(258) = L_imif.AND.L_emif.AND.L_amif.AND.L_ulif.AND.L_olif.AND.L
     &_ilif.AND.L_elif.AND.L_alif.AND.L_ukif.AND.L_okif
C FDA20_SP2_UVR.fgi(  57, 672):�
      L_(280) = L_uvof.AND.L_ovof.AND.L_ivof.AND.L_evof.AND.L_avof.AND.L
     &_utof.AND.L_otof.AND.L_itof.AND.L_etof.AND.L_atof
C FDA20_SP2_UVR.fgi(  57, 750):�
      L_(284) = L_(280).AND.L_(249)
C FDA20_SP2_UVR.fgi(  68, 740):�
      if(L_emuf.and..not.L0_iluf) then
         R0_aluf=R0_eluf
      else
         R0_aluf=max(R_(171)-deltat,0.0)
      endif
      L_epuf=R0_aluf.gt.0.0
      L0_iluf=L_emuf
C FDA20_SP2_UVR.fgi(  54, 832):������������  �� T
      if(L_imuf.and..not.L0_amuf) then
         R0_oluf=R0_uluf
      else
         R0_oluf=max(R_(172)-deltat,0.0)
      endif
      L_(287)=R0_oluf.gt.0.0
      L0_amuf=L_imuf
C FDA20_SP2_UVR.fgi(  54, 826):������������  �� T
      R_(184) = 0.5
C FDA20_SP1.fgi( 339, 655):��������� (RE4) (�������)
      R_(185) = 0.9
C FDA20_SP1.fgi( 128,  79):��������� (RE4) (�������)
      L_ulak = L_asek.OR.L_orek.OR.L_urek
C FDA20_SP1.fgi( 428, 642):���
      L_atak =.NOT.(L_asek.OR.L_urek.OR.L_orek.OR.L_esek)
C FDA20_SP1.fgi( 428, 623):���
      I_(1) = 1
C FDA20_SP1.fgi( 427, 704):��������� ������������� IN (�������)
      L_(310)=.false.
C FDA20_SP1.fgi( 418, 709):��������� ���������� (�������)
      I_(2) = 1
C FDA20_SP1.fgi( 427, 733):��������� ������������� IN (�������)
      L_(313)=.false.
C FDA20_SP1.fgi( 418, 738):��������� ���������� (�������)
      I_(3) = 1
C FDA20_SP1.fgi( 427, 762):��������� ������������� IN (�������)
      L_(316)=.false.
C FDA20_SP1.fgi( 418, 767):��������� ���������� (�������)
      I_(4) = 1
C FDA20_SP1.fgi( 427, 791):��������� ������������� IN (�������)
      L_(319)=.false.
C FDA20_SP1.fgi( 418, 796):��������� ���������� (�������)
      L_umek = L_urek.AND.L_orek.AND.L_asek.AND.L_esek
C FDA20_SP1.fgi( 428, 635):�
      L_(494) = L_udol.AND.L_umek
C FDA20_SP1.fgi(  45, 809):�
      if(L_(494).and..not.L0_idol) then
         R0_adol=R0_edol
      else
         R0_adol=max(R_(274)-deltat,0.0)
      endif
      L_(500)=R0_adol.gt.0.0
      L0_idol=L_(494)
C FDA20_SP1.fgi(  74, 809):������������  �� T
      R_(196) = 1000000
C FDA20_SP1.fgi(  38, 331):��������� (RE4) (�������)
      R_(199) = -12
C FDA20_SP1.fgi( 242, 243):��������� (RE4) (�������)
      R_(198) = 1200000
C FDA20_SP1.fgi( 240, 243):��������� (RE4) (�������)
      if(L_oxir.and..not.L0_uluk) then
         R0_iluk=R0_oluk
      else
         R0_iluk=max(R_(235)-deltat,0.0)
      endif
      L_(409)=R0_iluk.gt.0.0
      L0_uluk=L_oxir
C FDA20_SP1.fgi( 236, 355):������������  �� T
      if(L_oxir.and..not.L0_apuk) then
         R0_omuk=R0_umuk
      else
         R0_omuk=max(R_(237)-deltat,0.0)
      endif
      L_(412)=R0_omuk.gt.0.0
      L0_apuk=L_oxir
C FDA20_SP1.fgi( 222, 707):������������  �� T
      if(L_apur.and..not.L0_eruk) then
         R0_upuk=R0_aruk
      else
         R0_upuk=max(R_(239)-deltat,0.0)
      endif
      L_(417)=R0_upuk.gt.0.0
      L0_eruk=L_apur
C FDA20_SP1.fgi( 126, 685):������������  �� T
      L_(441)=.true.
C FDA20_SP1.fgi(  83, 540):��������� ���������� (�������)
      L_(443)=.true.
C FDA20_SP1.fgi(  79, 554):��������� ���������� (�������)
      L_(456)=.true.
C FDA20_SP1.fgi(  75, 570):��������� ���������� (�������)
      if(L_idor.and..not.L0_asel) then
         R0_orel=R0_urel
      else
         R0_orel=max(R_(261)-deltat,0.0)
      endif
      L_(465)=R0_orel.gt.0.0
      L0_asel=L_idor
C FDA20_SP1.fgi( 534, 764):������������  �� T
      if(L_oxir.and..not.L0_osel) then
         R0_esel=R0_isel
      else
         R0_esel=max(R_(262)-deltat,0.0)
      endif
      L_(466)=R0_esel.gt.0.0
      L0_osel=L_oxir
C FDA20_SP1.fgi( 534, 770):������������  �� T
      L_odil = L_(466).OR.L_(465)
C FDA20_SP1.fgi( 541, 769):���
      L_(463) = L_idor.AND.L_ixil
C FDA20_SP1.fgi( 534, 754):�
      if(L_(463).and..not.L0_ivel) then
         R0_avel=R0_evel
      else
         R0_avel=max(R_(265)-deltat,0.0)
      endif
      L_uril=R0_avel.gt.0.0
      L0_ivel=L_(463)
C FDA20_SP1.fgi( 541, 754):������������  �� T
      L_(464) = L_ixil.AND.L_oxir
C FDA20_SP1.fgi( 534, 792):�
      if(L_(464).and..not.L0_axel) then
         R0_ovel=R0_uvel
      else
         R0_ovel=max(R_(266)-deltat,0.0)
      endif
      L_evil=R0_ovel.gt.0.0
      L0_axel=L_(464)
C FDA20_SP1.fgi( 541, 792):������������  �� T
      if(L_evil.and..not.L0_ilel) then
         R0_alel=R0_elel
      else
         R0_alel=max(R_(258)-deltat,0.0)
      endif
      L_imil=R0_alel.gt.0.0
      L0_ilel=L_evil
C FDA20_SP1.fgi( 118, 586):������������  �� T
      L_(483) = L_osep.AND.L_emep.AND.L_udep.AND.L_okap.AND.L_arap.AND.L
     &_ivap.AND.L_ebap.AND.L_usum
C FDA20_SP1.fgi(  61, 614):�
      L_(477) = (.NOT.L_(483))
C FDA20_SP1.fgi(  69, 592):���
      L_(493) = L_oxil.AND.L_ixil.AND.L_exil
C FDA20_SP1.fgi(  70, 770):�
      L_ekor=.false.
C FDA20_SP1.fgi( 431, 673):��������� ���������� (�������)
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efor,L_ufor,R_ofor,
     & REAL(R_akor,4),L_ekor,L_afor,I_ifor)
      !}
C FDA20_lamp.fgi(  82, 738):���������� ������� ���������,20FDA21CG95
      L_osor =.NOT.(L_ebol)
C FDA20_SP1.fgi( 426, 677):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oror,L_esor,R_asor,
     & REAL(R_isor,4),L_osor,L_iror,I_uror)
      !}
C FDA20_lamp.fgi(  82, 760):���������� ������� ���������,20FDA21CG89
      L_upor=L_ebol
C FDA20_SP1.fgi( 448, 681):������,20FDA21CG90YU10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_umor,L_ipor,R_epor,
     & REAL(R_opor,4),L_upor,L_omor,I_apor)
      !}
C FDA20_lamp.fgi(  82, 748):���������� ������� ���������,20FDA21CG90
      L_amor=L_ebol
C FDA20_SP1.fgi( 448, 685):������,20FDA21CG91YU10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alor,L_olor,R_ilor,
     & REAL(R_ulor,4),L_amor,L_ukor,I_elor)
      !}
C FDA20_lamp.fgi(  82, 770):���������� ������� ���������,20FDA21CG91
      if(L_odol.and..not.L0_ubol) then
         R0_ibol=R0_obol
      else
         R0_ibol=max(R_(273)-deltat,0.0)
      endif
      L_elol=R0_ibol.gt.0.0
      L0_ubol=L_odol
C FDA20_SP1.fgi(  74, 815):������������  �� T
      if (.not.L_ilol.and.(L_alol.or.L_(500))) then
          I_olol = 0
          L_ilol = .true.
          L_(499) = .true.
      endif
      if (I_olol .gt. 0) then
         L_(499) = .false.
      endif
      if(L_elol)then
          I_olol = 0
          L_ilol = .false.
          L_(499) = .false.
      endif
C FDA20_SP1.fgi(  96, 801):���������� ������� ���������,cset
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_utol,R_otol,
     & REAL(R_avol,4),R_atol,REAL(R_itol,4),L_exol,
     & L_etol,L_ixol,L_evol,I_isol)
      !}
C FDA20_lamp.fgi( 106, 116):���������� ��������� �����������,20FDA21AE703QB03
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_edul,R_adul,
     & REAL(R_idul,4),R_ibul,REAL(R_ubul,4),L_oful,
     & L_obul,L_uful,L_odul,I_uxol)
      !}
C FDA20_lamp.fgi( 106,  70):���������� ��������� �����������,20FDA21AE703QB06
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_olul,R_ilul,
     & REAL(R_ulul,4),R_ukul,REAL(R_elul,4),L_apul,
     & L_alul,L_epul,L_amul,I_ekul)
      !}
C FDA20_lamp.fgi( 106,  85):���������� ��������� �����������,20FDA21AE703QB05
      !{
      Call LIMIT_SWITCH_HANDLER(deltat,L_asul,R_urul,
     & REAL(R_esul,4),R_erul,REAL(R_orul,4),L_itul,
     & L_irul,L_otul,L_isul,I_opul)
      !}
C FDA20_lamp.fgi( 106, 100):���������� ��������� �����������,20FDA21AE703QB04
      !{
      Call DAT_DISCR_HANDLER(deltat,I_evul,L_uvul,R_ovul,
     & REAL(R_axul,4),L_exul,L_avul,I_ivul)
      !}
C FDA20_lamp.fgi(  84, 102):���������� ������� ���������,20FDA21AB001BQ83
      !{
      Call DAT_DISCR_HANDLER(deltat,I_abam,L_obam,R_ibam,
     & REAL(R_ubam,4),L_adam,L_uxul,I_ebam)
      !}
C FDA20_lamp.fgi(  84, 116):���������� ������� ���������,20FDA21AB001BQ82
      !{
      Call DAT_DISCR_HANDLER(deltat,I_udam,L_ifam,R_efam,
     & REAL(R_ofam,4),L_ufam,L_odam,I_afam)
      !}
C FDA20_lamp.fgi(  62, 116):���������� ������� ���������,20FDA21AE701BQ70
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovam,L_exam,R_axam,
     & REAL(R_ixam,4),L_oxam,L_ivam,I_uvam)
      !}
C FDA20_lamp.fgi( 236, 180):���������� ������� ���������,20FDA21AL003BQ56
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibem,L_adem,R_ubem,
     & REAL(R_edem,4),L_idem,L_ebem,I_obem)
      !}
C FDA20_lamp.fgi( 236, 192):���������� ������� ���������,20FDA21AL003BQ55
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibim,L_adim,R_ubim,
     & REAL(R_edim,4),L_idim,L_ebim,I_obim)
      !}
C FDA20_lamp.fgi( 238, 278):���������� ������� ���������,20FDA21AL002BQ26
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efim,L_ufim,R_ofim,
     & REAL(R_akim,4),L_ekim,L_afim,I_ifim)
      !}
C FDA20_lamp.fgi( 238, 290):���������� ������� ���������,20FDA21AL002BQ25
      !{
      Call DAT_DISCR_HANDLER(deltat,I_orom,L_esom,R_asom,
     & REAL(R_isom,4),L_osom,L_irom,I_urom)
      !}
C FDA20_lamp.fgi(  70, 222):���������� ������� ���������,20FDA21AL001BQ40
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itom,L_avom,R_utom,
     & REAL(R_evom,4),L_ivom,L_etom,I_otom)
      !}
C FDA20_lamp.fgi(  70, 234):���������� ������� ���������,20FDA21AL001BQ39
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exom,L_uxom,R_oxom,
     & REAL(R_abum,4),L_ebum,L_axom,I_ixom)
      !}
C FDA20_lamp.fgi(  70, 264):���������� ������� ���������,20FDA21AL001BQ38
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adum,L_odum,R_idum,
     & REAL(R_udum,4),L_afum,L_ubum,I_edum)
      !}
C FDA20_lamp.fgi(  70, 250):���������� ������� ���������,20FDA21AL001BQ37
      L_(513) = (.NOT.L_okap)
C FDA20_lamp.fgi( 269, 412):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilap,L_amap,R_ulap,
     & REAL(R_emap,4),L_(513),L_elap,I_olap)
      !}
C FDA20_lamp.fgi( 282, 410):���������� ������� ���������,20FDA21AE702QB11
      L_(512) = (.NOT.L_ebap)
C FDA20_lamp.fgi( 269, 438):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adap,L_odap,R_idap,
     & REAL(R_udap,4),L_(512),L_ubap,I_edap)
      !}
C FDA20_lamp.fgi( 282, 436):���������� ������� ���������,20FDA21AE702QB19
      L_(511) = (.NOT.L_usum)
C FDA20_lamp.fgi( 269, 464):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_otum,L_evum,R_avum,
     & REAL(R_ivum,4),L_(511),L_itum,I_utum)
      !}
C FDA20_lamp.fgi( 282, 462):���������� ������� ���������,20FDA21AE702QB84
      L_(518) = (.NOT.L_osep)
C FDA20_lamp.fgi( 269, 490):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itep,L_avep,R_utep,
     & REAL(R_evep,4),L_(518),L_etep,I_otep)
      !}
C FDA20_lamp.fgi( 282, 488):���������� ������� ���������,20FDA21AE702QB13
      L_(515) = (.NOT.L_ivap)
C FDA20_lamp.fgi( 215, 412):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exap,L_uxap,R_oxap,
     & REAL(R_abep,4),L_(515),L_axap,I_ixap)
      !}
C FDA20_lamp.fgi( 228, 410):���������� ������� ���������,20FDA21AE702QB07
      L_(514) = (.NOT.L_arap)
C FDA20_lamp.fgi( 215, 438):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_urap,L_isap,R_esap,
     & REAL(R_osap,4),L_(514),L_orap,I_asap)
      !}
C FDA20_lamp.fgi( 228, 436):���������� ������� ���������,20FDA21AE702QB09
      L_(517) = (.NOT.L_emep)
C FDA20_lamp.fgi( 215, 464):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_apep,L_opep,R_ipep,
     & REAL(R_upep,4),L_(517),L_umep,I_epep)
      !}
C FDA20_lamp.fgi( 228, 462):���������� ������� ���������,20FDA21AE702QB15
      L_(516) = (.NOT.L_udep)
C FDA20_lamp.fgi( 215, 490):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofep,L_ekep,R_akep,
     & REAL(R_ikep,4),L_(516),L_ifep,I_ufep)
      !}
C FDA20_lamp.fgi( 228, 488):���������� ������� ���������,20FDA21AE702QB17
      !{
      Call DAT_DISCR_HANDLER(deltat,I_urum,L_isum,R_esum,
     & REAL(R_osum,4),L_usum,L_orum,I_asum)
      !}
C FDA20_lamp.fgi( 282, 476):���������� ������� ���������,20FDA21AE702QB85
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exum,L_uxum,R_oxum,
     & REAL(R_abap,4),L_ebap,L_axum,I_ixum)
      !}
C FDA20_lamp.fgi( 282, 450):���������� ������� ���������,20FDA21AE702QB20
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofap,L_ekap,R_akap,
     & REAL(R_ikap,4),L_okap,L_ifap,I_ufap)
      !}
C FDA20_lamp.fgi( 282, 424):���������� ������� ���������,20FDA21AE702QB12
      !{
      Call DAT_DISCR_HANDLER(deltat,I_apap,L_opap,R_ipap,
     & REAL(R_upap,4),L_arap,L_umap,I_epap)
      !}
C FDA20_lamp.fgi( 228, 450):���������� ������� ���������,20FDA21AE702QB10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itap,L_avap,R_utap,
     & REAL(R_evap,4),L_ivap,L_etap,I_otap)
      !}
C FDA20_lamp.fgi( 228, 424):���������� ������� ���������,20FDA21AE702QB08
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubep,L_idep,R_edep,
     & REAL(R_odep,4),L_udep,L_obep,I_adep)
      !}
C FDA20_lamp.fgi( 228, 502):���������� ������� ���������,20FDA21AE702QB18
      !{
      Call DAT_DISCR_HANDLER(deltat,I_elep,L_ulep,R_olep,
     & REAL(R_amep,4),L_emep,L_alep,I_ilep)
      !}
C FDA20_lamp.fgi( 228, 476):���������� ������� ���������,20FDA21AE702QB16
      !{
      Call DAT_DISCR_HANDLER(deltat,I_orep,L_esep,R_asep,
     & REAL(R_isep,4),L_osep,L_irep,I_urep)
      !}
C FDA20_lamp.fgi( 282, 502):���������� ������� ���������,20FDA21AE702QB14
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axep,L_oxep,R_ixep,
     & REAL(R_uxep,4),L_abip,L_uvep,I_exep)
      !}
C FDA20_lamp.fgi( 228, 398):���������� ������� ���������,20FDA21AE702QB87
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofip,L_ekip,R_akip,
     & REAL(R_ikip,4),L_okip,L_ifip,I_ufip)
      !}
C FDA20_lamp.fgi(  90, 344):���������� ������� ���������,20FDA21AL003BQ58
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilip,L_amip,R_ulip,
     & REAL(R_emip,4),L_imip,L_elip,I_olip)
      !}
C FDA20_lamp.fgi(  90, 356):���������� ������� ���������,20FDA21AL003BQ57
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epip,L_upip,R_opip,
     & REAL(R_arip,4),L_erip,L_apip,I_ipip)
      !}
C FDA20_lamp.fgi(  90, 370):���������� ������� ���������,20FDA21AL002BQ30
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asip,L_osip,R_isip,
     & REAL(R_usip,4),L_atip,L_urip,I_esip)
      !}
C FDA20_lamp.fgi(  90, 382):���������� ������� ���������,20FDA21AL002BQ29
      !{
      Call DAT_DISCR_HANDLER(deltat,I_esop,L_usop,R_osop,
     & REAL(R_atop,4),L_etop,L_asop,I_isop)
      !}
C FDA20_lamp.fgi(  74, 466):���������� ������� ���������,20FDA21AL004BQ46
      !{
      Call DAT_DISCR_HANDLER(deltat,I_avop,L_ovop,R_ivop,
     & REAL(R_uvop,4),L_axop,L_utop,I_evop)
      !}
C FDA20_lamp.fgi(  74, 454):���������� ������� ���������,20FDA21AL004BQ45
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekup,L_ukup,R_okup,
     & REAL(R_alup,4),L_elup,L_akup,I_ikup)
      !}
C FDA20_lamp.fgi(  74, 610):���������� ������� ���������,20FDA21AB001BQ81
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amup,L_omup,R_imup,
     & REAL(R_umup,4),L_apup,L_ulup,I_emup)
      !}
C FDA20_lamp.fgi(  74, 622):���������� ������� ���������,20FDA21AB001BQ80
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upup,L_irup,R_erup,
     & REAL(R_orup,4),L_urup,L_opup,I_arup)
      !}
C FDA20_lamp.fgi(  74, 650):���������� ������� ���������,20FDA21AB001BQ79
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osup,L_etup,R_atup,
     & REAL(R_itup,4),L_otup,L_isup,I_usup)
      !}
C FDA20_lamp.fgi(  74, 636):���������� ������� ���������,20FDA21AB001BQ78
      if(.NOT.L_iler.and..not.L0_ovup) then
         R0_evup=R0_ivup
      else
         R0_evup=max(R_(277)-deltat,0.0)
      endif
      L_(522)=R0_evup.gt.0.0
      L0_ovup=.NOT.L_iler
C FDA20_lamp.fgi( 248, 621):������������  �� T
      if(.NOT.L_exar.and..not.L0_exup) then
         R0_uvup=R0_axup
      else
         R0_uvup=max(R_(278)-deltat,0.0)
      endif
      L_(521)=R0_uvup.gt.0.0
      L0_exup=.NOT.L_exar
C FDA20_lamp.fgi( 248, 613):������������  �� T
      L_ofer=(L_(521).or.L_ofer).and..not.(L_(522))
      L_(523)=.not.L_ofer
C FDA20_lamp.fgi( 258, 617):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oder,L_efer,R_afer,
     & REAL(R_ifer,4),L_ofer,L_ider,I_uder)
      !}
C FDA20_lamp.fgi( 274, 618):���������� ������� ���������,20FDA21AE701BQ66
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aber,L_ober,R_iber,
     & REAL(R_uber,4),L_ofer,L_uxar,I_eber)
      !}
C FDA20_lamp.fgi( 274, 604):���������� ������� ���������,20FDA21AE701BQ67
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxup,L_ebar,R_abar,
     & REAL(R_ibar,4),L_obar,L_ixup,I_uxup)
      !}
C FDA20_lamp.fgi( 154, 548):���������� ������� ���������,20FDA21AE701BQ75
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idar,L_afar,R_udar,
     & REAL(R_efar,4),L_ifar,L_edar,I_odar)
      !}
C FDA20_lamp.fgi( 154, 562):���������� ������� ���������,20FDA21AE701BQ74
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekar,L_ukar,R_okar,
     & REAL(R_alar,4),L_elar,L_akar,I_ikar)
      !}
C FDA20_lamp.fgi( 154, 574):���������� ������� ���������,20FDA21AE701BQ73
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amar,L_omar,R_imar,
     & REAL(R_umar,4),L_apar,L_ular,I_emar)
      !}
C FDA20_lamp.fgi( 154, 588):���������� ������� ���������,20FDA21AE701BQ72
      !{
      Call DAT_DISCR_HANDLER(deltat,I_evar,L_uvar,R_ovar,
     & REAL(R_axar,4),L_exar,L_avar,I_ivar)
      !}
C FDA20_lamp.fgi( 274, 592):���������� ������� ���������,20FDA21AE701BQ68
      !{
      Call DAT_DISCR_HANDLER(deltat,I_iker,L_aler,R_uker,
     & REAL(R_eler,4),L_iler,L_eker,I_oker)
      !}
C FDA20_lamp.fgi( 274, 630):���������� ������� ���������,20FDA21AE701BQ65
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oser,L_eter,R_ater,
     & REAL(R_iter,4),L_oter,L_iser,I_user)
      !}
C FDA20_lamp.fgi( 154, 618):���������� ������� ���������,20FDA21AE701BQ77
      !{
      Call DAT_DISCR_HANDLER(deltat,I_iver,L_axer,R_uver,
     & REAL(R_exer,4),L_ixer,L_ever,I_over)
      !}
C FDA20_lamp.fgi( 154, 630):���������� ������� ���������,20FDA21AE701BQ76
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebir,L_ubir,R_obir,
     & REAL(R_adir,4),L_edir,L_abir,I_ibir)
      !}
C FDA20_lamp.fgi( 210, 618):���������� ������� ���������,20FDA21AE701BQ62
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afir,L_ofir,R_ifir,
     & REAL(R_ufir,4),L_akir,L_udir,I_efir)
      !}
C FDA20_lamp.fgi( 210, 630):���������� ������� ���������,20FDA21AE701BQ61
      L_(527) = (.NOT.L_oxir).AND.(.NOT.L_idor)
C FDA20_lamp.fgi(  67, 698):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_erir,L_urir,R_orir,
     & REAL(R_asir,4),L_(527),L_arir,I_irir)
      !}
C FDA20_lamp.fgi(  82, 696):���������� ������� ���������,20FDA21AE704BQ93
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovir,L_exir,R_axir,
     & REAL(R_ixir,4),L_oxir,L_ivir,I_uvir)
      !}
C FDA20_lamp.fgi(  82, 710):���������� ������� ���������,20FDA21AE704BQ94
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ibor,L_ador,R_ubor,
     & REAL(R_edor,4),L_idor,L_ebor,I_obor)
      !}
C FDA20_lamp.fgi(  82, 722):���������� ������� ���������,20FDA21AE704BQ92
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amur,L_omur,R_imur,
     & REAL(R_umur,4),L_apur,L_ulur,I_emur)
      !}
C FDA20_lamp.fgi(  82, 808):���������� ������� ���������,20FDA21AB002BQ06
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upur,L_irur,R_erur,
     & REAL(R_orur,4),L_urur,L_opur,I_arur)
      !}
C FDA20_lamp.fgi(  82, 820):���������� ������� ���������,20FDA21AB002BQ05
      R_(279) = 0.001
C FDA20_vent_log.fgi(  93,  78):��������� (RE4) (�������)
      R_eriv = R8_akas
C FDA20_vent_log.fgi(  96,  71):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_apiv,R_usiv,REAL(1,4)
     &,
     & REAL(R_opiv,4),REAL(R_upiv,4),
     & REAL(R_umiv,4),REAL(R_omiv,4),I_osiv,
     & REAL(R_iriv,4),L_oriv,REAL(R_uriv,4),L_asiv,L_esiv
     &,R_ariv,
     & REAL(R_ipiv,4),REAL(R_epiv,4),L_isiv,REAL(R_eriv,4
     &))
      !}
C FDA20_vlv.fgi( 289, 110):���������� �������,20FDA21CU001XQ01
      R_ukiv = R8_ekas
C FDA20_vent_log.fgi(  96,  75):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ofiv,R_imiv,REAL(1,4)
     &,
     & REAL(R_ekiv,4),REAL(R_ikiv,4),
     & REAL(R_ifiv,4),REAL(R_efiv,4),I_emiv,
     & REAL(R_aliv,4),L_eliv,REAL(R_iliv,4),L_oliv,L_uliv
     &,R_okiv,
     & REAL(R_akiv,4),REAL(R_ufiv,4),L_amiv,REAL(R_ukiv,4
     &))
      !}
C FDA20_vlv.fgi( 316, 110):���������� �������,20FDA21CM001XQ01
      R_isix = R8_okas
C FDA20_vent_log.fgi( 171,  29):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erix,R_avix,REAL(1,4)
     &,
     & REAL(R_urix,4),REAL(R_asix,4),
     & REAL(R_arix,4),REAL(R_upix,4),I_utix,
     & REAL(R_osix,4),L_usix,REAL(R_atix,4),L_etix,L_itix
     &,R_esix,
     & REAL(R_orix,4),REAL(R_irix,4),L_otix,REAL(R_isix,4
     &))
      !}
C FDA20_vlv.fgi( 346, 170):���������� �������,20FDA23CM001XQ01
      R_uxix = R8_ukas
C FDA20_vent_log.fgi( 171,  33):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovix,R_idox,REAL(1,4)
     &,
     & REAL(R_exix,4),REAL(R_ixix,4),
     & REAL(R_ivix,4),REAL(R_evix,4),I_edox,
     & REAL(R_abox,4),L_ebox,REAL(R_ibox,4),L_obox,L_ubox
     &,R_oxix,
     & REAL(R_axix,4),REAL(R_uvix,4),L_adox,REAL(R_uxix,4
     &))
      !}
C FDA20_vlv.fgi( 316, 170):���������� �������,20FDA23CU001XQ01
      R_etut = R8_alas
C FDA20_vent_log.fgi(  96, 109):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_asut,R_uvut,REAL(1,4)
     &,
     & REAL(R_osut,4),REAL(R_usut,4),
     & REAL(R_urut,4),REAL(R_orut,4),I_ovut,
     & REAL(R_itut,4),L_otut,REAL(R_utut,4),L_avut,L_evut
     &,R_atut,
     & REAL(R_isut,4),REAL(R_esut,4),L_ivut,REAL(R_etut,4
     &))
      !}
C FDA20_vlv.fgi( 465, 155):���������� �������,20FDA21CF102XQ01
      R_(280) = 60000
C FDA20_vent_log.fgi( 169,  59):��������� (RE4) (�������)
      if(R8_elas.ge.0.0) then
         R_(281)=R8_ilas/max(R8_elas,1.0e-10)
      else
         R_(281)=R8_ilas/min(R8_elas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159,  62):�������� ����������
      R_udax = R_(281) * R_(280)
C FDA20_vent_log.fgi( 172,  61):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_obax,R_ikax,REAL(1,4)
     &,
     & REAL(R_edax,4),REAL(R_idax,4),
     & REAL(R_ibax,4),REAL(R_ebax,4),I_ekax,
     & REAL(R_afax,4),L_efax,REAL(R_ifax,4),L_ofax,L_ufax
     &,R_odax,
     & REAL(R_adax,4),REAL(R_ubax,4),L_akax,REAL(R_udax,4
     &))
      !}
C FDA20_vlv.fgi( 316, 140):���������� �������,20FDA23CF106XQ01
      R_(282) = 60000
C FDA20_vent_log.fgi( 169,  75):��������� (RE4) (�������)
      if(R8_olas.ge.0.0) then
         R_(283)=R8_ulas/max(R8_olas,1.0e-10)
      else
         R_(283)=R8_ulas/min(R8_olas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159,  78):�������� ����������
      R_emax = R_(283) * R_(282)
C FDA20_vent_log.fgi( 172,  77):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alax,R_upax,REAL(1,4)
     &,
     & REAL(R_olax,4),REAL(R_ulax,4),
     & REAL(R_ukax,4),REAL(R_okax,4),I_opax,
     & REAL(R_imax,4),L_omax,REAL(R_umax,4),L_apax,L_epax
     &,R_amax,
     & REAL(R_ilax,4),REAL(R_elax,4),L_ipax,REAL(R_emax,4
     &))
      !}
C FDA20_vlv.fgi( 289, 140):���������� �������,20FDA23CF105XQ01
      R_(284) = 60000
C FDA20_vent_log.fgi( 169,  90):��������� (RE4) (�������)
      if(R8_amas.ge.0.0) then
         R_(285)=R8_emas/max(R8_amas,1.0e-10)
      else
         R_(285)=R8_emas/min(R8_amas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159,  93):�������� ����������
      R_osax = R_(285) * R_(284)
C FDA20_vent_log.fgi( 172,  92):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_irax,R_evax,REAL(1,4)
     &,
     & REAL(R_asax,4),REAL(R_esax,4),
     & REAL(R_erax,4),REAL(R_arax,4),I_avax,
     & REAL(R_usax,4),L_atax,REAL(R_etax,4),L_itax,L_otax
     &,R_isax,
     & REAL(R_urax,4),REAL(R_orax,4),L_utax,REAL(R_osax,4
     &))
      !}
C FDA20_vlv.fgi( 408, 155):���������� �������,20FDA23CF104XQ01
      R_(286) = 60000
C FDA20_vent_log.fgi( 169, 102):��������� (RE4) (�������)
      if(R8_imas.ge.0.0) then
         R_(287)=R8_omas/max(R8_imas,1.0e-10)
      else
         R_(287)=R8_omas/min(R8_imas,-1.0e-10)
      endif
C FDA20_vent_log.fgi( 159, 105):�������� ����������
      R_abex = R_(287) * R_(286)
C FDA20_vent_log.fgi( 172, 104):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uvax,R_odex,REAL(1,4)
     &,
     & REAL(R_ixax,4),REAL(R_oxax,4),
     & REAL(R_ovax,4),REAL(R_ivax,4),I_idex,
     & REAL(R_ebex,4),L_ibex,REAL(R_obex,4),L_ubex,L_adex
     &,R_uxax,
     & REAL(R_exax,4),REAL(R_axax,4),L_edex,REAL(R_abex,4
     &))
      !}
C FDA20_vlv.fgi( 377, 155):���������� �������,20FDA23CF103XQ01
      !��������� R_(288) = FDA20_vent_logC?? /0.0005/
      R_(288)=R0_apas
C FDA20_vent_log.fgi( 169, 209):���������
      L_(528)=R8_epas.gt.R_(288)
C FDA20_vent_log.fgi( 173, 210):���������� >
      !��������� R_(290) = FDA20_vent_logC?? /0.0/
      R_(290)=R0_ipas
C FDA20_vent_log.fgi( 181, 216):���������
      !��������� R_(289) = FDA20_vent_logC?? /500/
      R_(289)=R0_opas
C FDA20_vent_log.fgi( 181, 214):���������
      if(L_(528)) then
         R_(298)=R_(289)
      else
         R_(298)=R_(290)
      endif
C FDA20_vent_log.fgi( 184, 214):���� RE IN LO CH7
      R_(294) = 1.0
C FDA20_vent_log.fgi(  92, 210):��������� (RE4) (�������)
      R_(295) = 0.0
C FDA20_vent_log.fgi(  92, 212):��������� (RE4) (�������)
      !��������� R_(292) = FDA20_vent_logC?? /2.6/
      R_(292)=R0_aras
C FDA20_vent_log.fgi(  88, 233):���������
      !��������� R_(296) = FDA20_vent_logC?? /1/
      R_(296)=R0_esas
C FDA20_vent_log.fgi( 183, 232):���������
      L_(532)=R8_ivas.lt.R_(296)
C FDA20_vent_log.fgi( 187, 233):���������� <
      L_osas=L_isas.or.(L_osas.and..not.(L_(532)))
      L_(533)=.not.L_osas
C FDA20_vent_log.fgi( 217, 235):RS �������
      L_atas=L_osas
C FDA20_vent_log.fgi( 232, 237):������,20FDA23CW001_OUT
      !��������� R_(297) = FDA20_vent_logC?? /-50000/
      R_(297)=R0_usas
C FDA20_vent_log.fgi( 212, 213):���������
      if(L_atas) then
         R8_etas=R_(297)
      else
         R8_etas=R_(298)
      endif
C FDA20_vent_log.fgi( 215, 213):���� RE IN LO CH7
      if(R8_ivas.le.R0_avas) then
         R8_evas=R0_utas
      elseif(R8_ivas.gt.R0_otas) then
         R8_evas=R0_itas
      else
         R8_evas=R0_utas+(R8_ivas-(R0_avas))*(R0_itas-(R0_utas
     &))/(R0_otas-(R0_avas))
      endif
C FDA20_vent_log.fgi( 169,  48):��������������� ���������
      L_(529)=R8_evas.gt.R_(292)
C FDA20_vent_log.fgi(  92, 234):���������� >
      L_(530) = L_(529).OR.L_umas
C FDA20_vent_log.fgi( 106, 233):���
      L_iras=L_eras.or.(L_iras.and..not.(L_(530)))
      L_(531)=.not.L_iras
C FDA20_vent_log.fgi( 122, 236):RS �������
      L_oras=L_iras
C FDA20_vent_log.fgi( 136, 238):������,FDA23dust_start
      if(L_oras) then
         R_(293)=R_(294)
      else
         R_(293)=R_(295)
      endif
C FDA20_vent_log.fgi(  95, 211):���� RE IN LO CH7
      R8_asas=(R0_upas*R_(291)+deltat*R_(293))/(R0_upas+deltat
     &)
C FDA20_vent_log.fgi( 105, 212):�������������� �����  
      R8_uras=R8_asas
C FDA20_vent_log.fgi( 123, 212):������,20FDA23AN002KA11
      R_(299) = 60
C FDA20_vent_log.fgi(  94,  91):��������� (RE4) (�������)
      R_(300) = 1000
C FDA20_vent_log.fgi(  87,  94):��������� (RE4) (�������)
      R_(301) = R8_ovas * R_(300)
C FDA20_vent_log.fgi(  89,  96):����������
      if(R_(299).ge.0.0) then
         R_ibiv=R_(301)/max(R_(299),1.0e-10)
      else
         R_ibiv=R_(301)/min(R_(299),-1.0e-10)
      endif
C FDA20_vent_log.fgi(  98,  94):�������� ����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_exev,R_afiv,REAL(1,4)
     &,
     & REAL(R_uxev,4),REAL(R_abiv,4),
     & REAL(R_axev,4),REAL(R_uvev,4),I_udiv,
     & REAL(R_obiv,4),L_ubiv,REAL(R_adiv,4),L_ediv,L_idiv
     &,R_ebiv,
     & REAL(R_oxev,4),REAL(R_ixev,4),L_odiv,REAL(R_ibiv,4
     &))
      !}
C FDA20_vlv.fgi( 346, 110):���������� �������,20FDA21CF001XQ01
      R_evex = R8_uvas
C FDA20_vent_log.fgi( 169, 115):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atex,R_uxex,REAL(1,4)
     &,
     & REAL(R_otex,4),REAL(R_utex,4),
     & REAL(R_usex,4),REAL(R_osex,4),I_oxex,
     & REAL(R_ivex,4),L_ovex,REAL(R_uvex,4),L_axex,L_exex
     &,R_avex,
     & REAL(R_itex,4),REAL(R_etex,4),L_ixex,REAL(R_evex,4
     &))
      !}
C FDA20_vlv.fgi( 289, 155):���������� �������,20FDA23CF001XQ01
      R_obav = R8_axas
C FDA20_vent_log.fgi(  96, 115):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixut,R_efav,REAL(1,4)
     &,
     & REAL(R_abav,4),REAL(R_ebav,4),
     & REAL(R_exut,4),REAL(R_axut,4),I_afav,
     & REAL(R_ubav,4),L_adav,REAL(R_edav,4),L_idav,L_odav
     &,R_ibav,
     & REAL(R_uxut,4),REAL(R_oxut,4),L_udav,REAL(R_obav,4
     &))
      !}
C FDA20_vlv.fgi( 465, 169):���������� �������,20FDA21CF101XQ01
      R_amix = R8_exas
C FDA20_vent_log.fgi( 176, 165):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukix,R_opix,REAL(1,4)
     &,
     & REAL(R_ilix,4),REAL(R_olix,4),
     & REAL(R_okix,4),REAL(R_ikix,4),I_ipix,
     & REAL(R_emix,4),L_imix,REAL(R_omix,4),L_umix,L_apix
     &,R_ulix,
     & REAL(R_elix,4),REAL(R_alix,4),L_epix,REAL(R_amix,4
     &))
      !}
C FDA20_vlv.fgi( 377, 170):���������� �������,20FDA23CP101XQ01
      R_imov = R8_ixas
C FDA20_vent_log.fgi( 102, 165):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_elov,R_arov,REAL(1,4)
     &,
     & REAL(R_ulov,4),REAL(R_amov,4),
     & REAL(R_alov,4),REAL(R_ukov,4),I_upov,
     & REAL(R_omov,4),L_umov,REAL(R_apov,4),L_epov,L_ipov
     &,R_emov,
     & REAL(R_olov,4),REAL(R_ilov,4),L_opov,REAL(R_imov,4
     &))
      !}
C FDA20_vlv.fgi( 346, 125):���������� �������,20FDA21CP002XQ01
      R_(303) = R8_oxas + (-R8_odes)
C FDA20_vent_log.fgi( 161, 130):��������
      R_(302) = 1e-3
C FDA20_vent_log.fgi( 169, 127):��������� (RE4) (�������)
      R_(304) = R_(303) * R_(302)
C FDA20_vent_log.fgi( 172, 129):����������
      R_ebuv = R_(304)
C FDA20_vent_log.fgi( 176, 129):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axov,R_uduv,REAL(1,4)
     &,
     & REAL(R_oxov,4),REAL(R_uxov,4),
     & REAL(R_uvov,4),REAL(R_ovov,4),I_oduv,
     & REAL(R_ibuv,4),L_obuv,REAL(R_ubuv,4),L_aduv,L_eduv
     &,R_abuv,
     & REAL(R_ixov,4),REAL(R_exov,4),L_iduv,REAL(R_ebuv,4
     &))
      !}
C FDA20_vlv.fgi( 289, 125):���������� �������,20FDA23CP105XQ01
      R_(306) = R8_uxas + (-R8_odes)
C FDA20_vent_log.fgi( 161, 140):��������
      R_(305) = 1e-3
C FDA20_vent_log.fgi( 169, 137):��������� (RE4) (�������)
      R_(307) = R_(306) * R_(305)
C FDA20_vent_log.fgi( 172, 139):����������
      R_okuv = R_(307)
C FDA20_vent_log.fgi( 176, 139):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifuv,R_emuv,REAL(1,4)
     &,
     & REAL(R_akuv,4),REAL(R_ekuv,4),
     & REAL(R_efuv,4),REAL(R_afuv,4),I_amuv,
     & REAL(R_ukuv,4),L_aluv,REAL(R_eluv,4),L_iluv,L_oluv
     &,R_ikuv,
     & REAL(R_ufuv,4),REAL(R_ofuv,4),L_uluv,REAL(R_okuv,4
     &))
      !}
C FDA20_vlv.fgi( 408, 140):���������� �������,20FDA23CP104XQ01
      R_(309) = R8_abes + (-R8_odes)
C FDA20_vent_log.fgi( 161, 148):��������
      R_(308) = 1e-3
C FDA20_vent_log.fgi( 169, 145):��������� (RE4) (�������)
      R_(310) = R_(309) * R_(308)
C FDA20_vent_log.fgi( 172, 147):����������
      R_aruv = R_(310)
C FDA20_vent_log.fgi( 176, 147):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_umuv,R_osuv,REAL(1,4)
     &,
     & REAL(R_ipuv,4),REAL(R_opuv,4),
     & REAL(R_omuv,4),REAL(R_imuv,4),I_isuv,
     & REAL(R_eruv,4),L_iruv,REAL(R_oruv,4),L_uruv,L_asuv
     &,R_upuv,
     & REAL(R_epuv,4),REAL(R_apuv,4),L_esuv,REAL(R_aruv,4
     &))
      !}
C FDA20_vlv.fgi( 377, 140):���������� �������,20FDA23CP103XQ01
      R_(312) = R8_ebes + (-R8_odes)
C FDA20_vent_log.fgi( 161, 157):��������
      R_(311) = 1e-3
C FDA20_vent_log.fgi( 169, 154):��������� (RE4) (�������)
      R_(313) = R_(312) * R_(311)
C FDA20_vent_log.fgi( 172, 156):����������
      R_ivuv = R_(313)
C FDA20_vent_log.fgi( 176, 156):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_etuv,R_abax,REAL(1,4)
     &,
     & REAL(R_utuv,4),REAL(R_avuv,4),
     & REAL(R_atuv,4),REAL(R_usuv,4),I_uxuv,
     & REAL(R_ovuv,4),L_uvuv,REAL(R_axuv,4),L_exuv,L_ixuv
     &,R_evuv,
     & REAL(R_otuv,4),REAL(R_ituv,4),L_oxuv,REAL(R_ivuv,4
     &))
      !}
C FDA20_vlv.fgi( 346, 140):���������� �������,20FDA23CP102XQ01
      R_(315) = R8_ibes + (-R8_odes)
C FDA20_vent_log.fgi(  87, 130):��������
      R_(314) = 1e-3
C FDA20_vent_log.fgi(  95, 127):��������� (RE4) (�������)
      R_(316) = R_(315) * R_(314)
C FDA20_vent_log.fgi(  98, 129):����������
      R_uvav = R_(316)
C FDA20_vent_log.fgi( 102, 129):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_otav,R_ibev,REAL(1,4)
     &,
     & REAL(R_evav,4),REAL(R_ivav,4),
     & REAL(R_itav,4),REAL(R_etav,4),I_ebev,
     & REAL(R_axav,4),L_exav,REAL(R_ixav,4),L_oxav,L_uxav
     &,R_ovav,
     & REAL(R_avav,4),REAL(R_utav,4),L_abev,REAL(R_uvav,4
     &))
      !}
C FDA20_vlv.fgi( 437, 110):���������� �������,20FDA21CP110XQ01
      R_(318) = R8_obes + (-R8_odes)
C FDA20_vent_log.fgi(  87, 140):��������
      R_(317) = 1e-3
C FDA20_vent_log.fgi(  95, 137):��������� (RE4) (�������)
      R_(319) = R_(318) * R_(317)
C FDA20_vent_log.fgi(  98, 139):����������
      R_efev = R_(319)
C FDA20_vent_log.fgi( 102, 139):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_adev,R_ukev,REAL(1,4)
     &,
     & REAL(R_odev,4),REAL(R_udev,4),
     & REAL(R_ubev,4),REAL(R_obev,4),I_okev,
     & REAL(R_ifev,4),L_ofev,REAL(R_ufev,4),L_akev,L_ekev
     &,R_afev,
     & REAL(R_idev,4),REAL(R_edev,4),L_ikev,REAL(R_efev,4
     &))
      !}
C FDA20_vlv.fgi( 437, 125):���������� �������,20FDA21CP109XQ01
      R_(321) = R8_ubes + (-R8_odes)
C FDA20_vent_log.fgi(  87, 148):��������
      R_(320) = 1e-3
C FDA20_vent_log.fgi(  95, 145):��������� (RE4) (�������)
      R_(322) = R_(321) * R_(320)
C FDA20_vent_log.fgi(  98, 147):����������
      R_omev = R_(322)
C FDA20_vent_log.fgi( 102, 147):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ilev,R_erev,REAL(1,4)
     &,
     & REAL(R_amev,4),REAL(R_emev,4),
     & REAL(R_elev,4),REAL(R_alev,4),I_arev,
     & REAL(R_umev,4),L_apev,REAL(R_epev,4),L_ipev,L_opev
     &,R_imev,
     & REAL(R_ulev,4),REAL(R_olev,4),L_upev,REAL(R_omev,4
     &))
      !}
C FDA20_vlv.fgi( 408, 110):���������� �������,20FDA21CP108XQ01
      R_(324) = R8_ades + (-R8_odes)
C FDA20_vent_log.fgi(  87, 157):��������
      R_(323) = 1e-3
C FDA20_vent_log.fgi(  95, 154):��������� (RE4) (�������)
      R_(325) = R_(324) * R_(323)
C FDA20_vent_log.fgi(  98, 156):����������
      R_atev = R_(325)
C FDA20_vent_log.fgi( 102, 156):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_urev,R_ovev,REAL(1,4)
     &,
     & REAL(R_isev,4),REAL(R_osev,4),
     & REAL(R_orev,4),REAL(R_irev,4),I_ivev,
     & REAL(R_etev,4),L_itev,REAL(R_otev,4),L_utev,L_avev
     &,R_usev,
     & REAL(R_esev,4),REAL(R_asev,4),L_evev,REAL(R_atev,4
     &))
      !}
C FDA20_vlv.fgi( 377, 110):���������� �������,20FDA21CP107XQ01
      R_odix = R8_odes + (-R8_edes)
C FDA20_vent_log.fgi( 161, 181):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibix,R_ekix,REAL(1,4)
     &,
     & REAL(R_adix,4),REAL(R_edix,4),
     & REAL(R_ebix,4),REAL(R_abix,4),I_akix,
     & REAL(R_udix,4),L_afix,REAL(R_efix,4),L_ifix,L_ofix
     &,R_idix,
     & REAL(R_ubix,4),REAL(R_obix,4),L_ufix,REAL(R_odix,4
     &))
      !}
C FDA20_vlv.fgi( 408, 170):���������� �������,20FDA23CP001XQ01
      R_ifut = R8_odes + (-R8_ides)
C FDA20_vent_log.fgi(  87, 181):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_edut,R_alut,REAL(1,4)
     &,
     & REAL(R_udut,4),REAL(R_afut,4),
     & REAL(R_adut,4),REAL(R_ubut,4),I_ukut,
     & REAL(R_ofut,4),L_ufut,REAL(R_akut,4),L_ekut,L_ikut
     &,R_efut,
     & REAL(R_odut,4),REAL(R_idut,4),L_okut,REAL(R_ifut,4
     &))
      !}
C FDA20_vlv.fgi( 465, 110):���������� �������,20FDA21CP001XQ01
      !��������� R_(329) = FDA20_vent_logC?? /0.0/
      R_(329)=R0_ifes
C FDA20_vent_log.fgi( 164, 272):���������
      !��������� R_(328) = FDA20_vent_logC?? /0.001/
      R_(328)=R0_ofes
C FDA20_vent_log.fgi( 164, 270):���������
      !��������� R_(333) = FDA20_vent_logC?? /0.0/
      R_(333)=R0_ikes
C FDA20_vent_log.fgi(  86, 272):���������
      !��������� R_(332) = FDA20_vent_logC?? /0.001/
      R_(332)=R0_okes
C FDA20_vent_log.fgi(  86, 270):���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axes,R_udis,REAL(1,4)
     &,
     & REAL(R_oxes,4),REAL(R_uxes,4),
     & REAL(R_uves,4),REAL(R_oves,4),I_odis,
     & REAL(R_ibis,4),L_obis,REAL(R_ubis,4),L_adis,L_edis
     &,R_abis,
     & REAL(R_ixes,4),REAL(R_exes,4),L_idis,REAL(R_ebis,4
     &))
      !}
C FDA20_vlv.fgi( 289,  56):���������� �������,20FDA22CF002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifis,R_emis,REAL(1,4)
     &,
     & REAL(R_akis,4),REAL(R_ekis,4),
     & REAL(R_efis,4),REAL(R_afis,4),I_amis,
     & REAL(R_ukis,4),L_alis,REAL(R_elis,4),L_ilis,L_olis
     &,R_ikis,
     & REAL(R_ufis,4),REAL(R_ofis,4),L_ulis,REAL(R_okis,4
     &))
      !}
C FDA20_vlv.fgi( 377,  71):���������� �������,20FDA22CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_umis,R_osis,REAL(1,4)
     &,
     & REAL(R_ipis,4),REAL(R_opis,4),
     & REAL(R_omis,4),REAL(R_imis,4),I_isis,
     & REAL(R_eris,4),L_iris,REAL(R_oris,4),L_uris,L_asis
     &,R_upis,
     & REAL(R_epis,4),REAL(R_apis,4),L_esis,REAL(R_aris,4
     &))
      !}
C FDA20_vlv.fgi( 345,  71):���������� �������,20FDA22CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_obos,R_ikos,REAL(1,4)
     &,
     & REAL(R_edos,4),REAL(R_idos,4),
     & REAL(R_ibos,4),REAL(R_ebos,4),I_ekos,
     & REAL(R_afos,4),L_efos,REAL(R_ifos,4),L_ofos,L_ufos
     &,R_odos,
     & REAL(R_ados,4),REAL(R_ubos,4),L_akos,REAL(R_udos,4
     &))
      !}
C FDA20_vlv.fgi( 289,  71):���������� �������,20FDA22CF001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_alos,R_upos,REAL(1,4)
     &,
     & REAL(R_olos,4),REAL(R_ulos,4),
     & REAL(R_ukos,4),REAL(R_okos,4),I_opos,
     & REAL(R_imos,4),L_omos,REAL(R_umos,4),L_apos,L_epos
     &,R_amos,
     & REAL(R_ilos,4),REAL(R_elos,4),L_ipos,REAL(R_emos,4
     &))
      !}
C FDA20_vlv.fgi( 377,  86):���������� �������,20FDA22CU001XQ01
      !{
      Call BVALVE2_MAN(deltat,REAL(R_abus,4),R8_utos,I_ifus
     &,I_akus,I_efus,C8_ovos,
     & I_ufus,R_ixos,R_exos,R_uros,REAL(R_esos,4),
     & R_atos,REAL(R_itos,4),R_isos,
     & REAL(R_usos,4),I_udus,I_ekus,I_ofus,I_odus,L_otos,L_okus
     &,
     & L_olus,L_etos,L_ivos,L_evos,L_alus,
     & L_osos,L_asos,L_axos,L_emus,L_oxos,L_uxos,
     & REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_aros,L_eros
     &,L_ukus,I_ikus,
     & L_ulus,R8_avos,R_idus,REAL(R_uvos,4),L_amus,L_iros
     &,L_imus,L_oros,
     & L_ebus,L_ibus,L_obus,L_adus,L_edus,L_ubus)
      !}

      if(L_edus.or.L_adus.or.L_ubus.or.L_obus.or.L_ibus.or.L_ebus
     &) then      
                  I_afus = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_afus = z'40000000'
      endif
C FDA20_vlv.fgi( 161, 124):���� ���������� �������� �������� ��� 2,20FDA22AA004
      !{
      Call BVALVE2_MAN(deltat,REAL(R_ovus,4),R8_isus,I_adat
     &,I_odat,I_ubat,C8_etus,
     & I_idat,R_avus,R_utus,R_ipus,REAL(R_upus,4),
     & R_orus,REAL(R_asus,4),R_arus,
     & REAL(R_irus,4),I_ibat,I_udat,I_edat,I_ebat,L_esus,L_efat
     &,
     & L_ekat,L_urus,L_atus,L_usus,L_ofat,
     & L_erus,L_opus,L_otus,L_ukat,L_evus,L_ivus,
     & REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_omus,L_umus
     &,L_ifat,I_afat,
     & L_ikat,R8_osus,R_abat,REAL(R_itus,4),L_okat,L_apus
     &,L_alat,L_epus,
     & L_uvus,L_axus,L_exus,L_oxus,L_uxus,L_ixus)
      !}

      if(L_uxus.or.L_oxus.or.L_ixus.or.L_exus.or.L_axus.or.L_uvus
     &) then      
                  I_obat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obat = z'40000000'
      endif
C FDA20_vlv.fgi( 145, 124):���� ���������� �������� �������� ��� 2,20FDA22AA003
      !{
      Call BVALVE2_MAN(deltat,REAL(R_etat,4),R8_arat,I_oxat
     &,I_ebet,I_ixat,C8_urat,
     & I_abet,R_osat,R_isat,R_amat,REAL(R_imat,4),
     & R_epat,REAL(R_opat,4),R_omat,
     & REAL(R_apat,4),I_axat,I_ibet,I_uxat,I_uvat,L_upat,L_ubet
     &,
     & L_udet,L_ipat,L_orat,L_irat,L_edet,
     & L_umat,L_emat,L_esat,L_ifet,L_usat,L_atat,
     & REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_elat,L_ilat
     &,L_adet,I_obet,
     & L_afet,R8_erat,R_ovat,REAL(R_asat,4),L_efet,L_olat
     &,L_ofet,L_ulat,
     & L_itat,L_otat,L_utat,L_evat,L_ivat,L_avat)
      !}

      if(L_ivat.or.L_evat.or.L_avat.or.L_utat.or.L_otat.or.L_itat
     &) then      
                  I_exat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exat = z'40000000'
      endif
C FDA20_vlv.fgi( 129, 124):���� ���������� �������� �������� ��� 2,20FDA22AA002
      !{
      Call BVALVE2_MAN(deltat,REAL(R_uret,4),R8_omet,I_evet
     &,I_uvet,I_avet,C8_ipet,
     & I_ovet,R_eret,R_aret,R_oket,REAL(R_alet,4),
     & R_ulet,REAL(R_emet,4),R_elet,
     & REAL(R_olet,4),I_otet,I_axet,I_ivet,I_itet,L_imet,L_ixet
     &,
     & L_ibit,L_amet,L_epet,L_apet,L_uxet,
     & L_ilet,L_uket,L_upet,L_adit,L_iret,L_oret,
     & REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_ufet,L_aket
     &,L_oxet,I_exet,
     & L_obit,R8_umet,R_etet,REAL(R_opet,4),L_ubit,L_eket
     &,L_edit,L_iket,
     & L_aset,L_eset,L_iset,L_uset,L_atet,L_oset)
      !}

      if(L_atet.or.L_uset.or.L_oset.or.L_iset.or.L_eset.or.L_aset
     &) then      
                  I_utet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_utet = z'40000000'
      endif
C FDA20_vlv.fgi( 113, 124):���� ���������� �������� �������� ��� 2,20FDA22AA001
      !{
      Call DAT_ANA_HANDLER(deltat,R_oxit,R_ifot,REAL(1,4)
     &,
     & REAL(R_ebot,4),REAL(R_ibot,4),
     & REAL(R_ixit,4),REAL(R_exit,4),I_efot,
     & REAL(R_adot,4),L_edot,REAL(R_idot,4),L_odot,L_udot
     &,R_obot,
     & REAL(R_abot,4),REAL(R_uxit,4),L_afot,REAL(R_ubot,4
     &))
      !}
C FDA20_vlv.fgi( 345,  86):���������� �������,20FDA22CM001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_akot,R_umot,REAL(1,4)
     &,
     & REAL(R_okot,4),REAL(R_ukot,4),
     & REAL(R_ufot,4),REAL(R_ofot,4),I_omot,
     & REAL(R_ilot,4),L_olot,REAL(R_ulot,4),L_amot,L_emot
     &,R_alot,
     & REAL(R_ikot,4),REAL(R_ekot,4),L_imot,REAL(R_elot,4
     &))
      !}
C FDA20_vlv.fgi( 316,  86):���������� �������,20FDA22CP002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ipot,R_etot,REAL(1,4)
     &,
     & REAL(R_arot,4),REAL(R_erot,4),
     & REAL(R_epot,4),REAL(R_apot,4),I_atot,
     & REAL(R_urot,4),L_asot,REAL(R_esot,4),L_isot,L_osot
     &,R_irot,
     & REAL(R_upot,4),REAL(R_opot,4),L_usot,REAL(R_orot,4
     &))
      !}
C FDA20_vlv.fgi( 289,  86):���������� �������,20FDA22CP001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_utot,R_obut,REAL(1,4)
     &,
     & REAL(R_ivot,4),REAL(R_ovot,4),
     & REAL(R_otot,4),REAL(R_itot,4),I_ibut,
     & REAL(R_exot,4),L_ixot,REAL(R_oxot,4),L_uxot,L_abut
     &,R_uvot,
     & REAL(R_evot,4),REAL(R_avot,4),L_ebut,REAL(R_axot,4
     &))
      !}
C FDA20_vlv.fgi( 465, 140):���������� �������,20FDA23CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_olut,R_irut,REAL(1,4)
     &,
     & REAL(R_emut,4),REAL(R_imut,4),
     & REAL(R_ilut,4),REAL(R_elut,4),I_erut,
     & REAL(R_aput,4),L_eput,REAL(R_iput,4),L_oput,L_uput
     &,R_omut,
     & REAL(R_amut,4),REAL(R_ulut,4),L_arut,REAL(R_umut,4
     &))
      !}
C FDA20_vlv.fgi( 437, 170):���������� �������,20FDA21CT001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_itiv,R_ebov,REAL(1,4)
     &,
     & REAL(R_aviv,4),REAL(R_eviv,4),
     & REAL(R_etiv,4),REAL(R_ativ,4),I_abov,
     & REAL(R_uviv,4),L_axiv,REAL(R_exiv,4),L_ixiv,L_oxiv
     &,R_iviv,
     & REAL(R_utiv,4),REAL(R_otiv,4),L_uxiv,REAL(R_oviv,4
     &))
      !}
C FDA20_vlv.fgi( 408, 125):���������� �������,20FDA21CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_efex,R_amex,REAL(1,4)
     &,
     & REAL(R_ufex,4),REAL(R_akex,4),
     & REAL(R_afex,4),REAL(R_udex,4),I_ulex,
     & REAL(R_okex,4),L_ukex,REAL(R_alex,4),L_elex,L_ilex
     &,R_ekex,
     & REAL(R_ofex,4),REAL(R_ifex,4),L_olex,REAL(R_ikex,4
     &))
      !}
C FDA20_vlv.fgi( 346, 155):���������� �������,20FDA23CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_omex,R_isex,REAL(1,4)
     &,
     & REAL(R_epex,4),REAL(R_ipex,4),
     & REAL(R_imex,4),REAL(R_emex,4),I_esex,
     & REAL(R_arex,4),L_erex,REAL(R_irex,4),L_orex,L_urex
     &,R_opex,
     & REAL(R_apex,4),REAL(R_umex,4),L_asex,REAL(R_upex,4
     &))
      !}
C FDA20_vlv.fgi( 316, 155):���������� �������,20FDA23CT001XQ01
      C8_imike = 'regim5'
C FDA20_logic.fgi(  36, 144):��������� ���������� CH8 (�������)
      call chcomp(C8_osike,C8_imike,L_(556))
C FDA20_logic.fgi(  41, 145):���������� ���������
      C8_omike = 'regim4'
C FDA20_logic.fgi(  36, 150):��������� ���������� CH8 (�������)
      call chcomp(C8_osike,C8_omike,L_(557))
C FDA20_logic.fgi(  41, 151):���������� ���������
      C8_umike = 'regim3'
C FDA20_logic.fgi(  36, 156):��������� ���������� CH8 (�������)
      call chcomp(C8_osike,C8_umike,L_(558))
C FDA20_logic.fgi(  41, 157):���������� ���������
      C8_apike = 'regim2'
C FDA20_logic.fgi(  36, 162):��������� ���������� CH8 (�������)
      call chcomp(C8_osike,C8_apike,L_(559))
C FDA20_logic.fgi(  41, 163):���������� ���������
      C30_epike = '�������������� �����'
C FDA20_logic.fgi(  89, 153):��������� ���������� CH20 (CH30) (�������)
      C30_opike = '������������������ �����'
C FDA20_logic.fgi(  61, 154):��������� ���������� CH20 (CH30) (�������)
      C30_arike = '������ �����'
C FDA20_logic.fgi( 101, 171):��������� ���������� CH20 (CH30) (�������)
      C30_irike = '����� �������'
C FDA20_logic.fgi(  80, 172):��������� ���������� CH20 (CH30) (�������)
      C30_urike = '��������� ���������'
C FDA20_logic.fgi(  61, 173):��������� ���������� CH20 (CH30) (�������)
      C30_asike = ''
C FDA20_logic.fgi(  61, 175):��������� ���������� CH20 (CH30) (�������)
      C8_isike = 'regim1'
C FDA20_logic.fgi(  36, 168):��������� ���������� CH8 (�������)
      call chcomp(C8_osike,C8_isike,L_(560))
C FDA20_logic.fgi(  41, 169):���������� ���������
      if(L_(560)) then
         C30_orike=C30_urike
      else
         C30_orike=C30_asike
      endif
C FDA20_logic.fgi(  65, 173):���� RE IN LO CH20
      if(L_(559)) then
         C30_erike=C30_irike
      else
         C30_erike=C30_orike
      endif
C FDA20_logic.fgi(  85, 172):���� RE IN LO CH20
      if(L_(558)) then
         C30_upike=C30_arike
      else
         C30_upike=C30_erike
      endif
C FDA20_logic.fgi( 106, 171):���� RE IN LO CH20
      if(L_(557)) then
         C30_ipike=C30_opike
      else
         C30_ipike=C30_upike
      endif
C FDA20_logic.fgi(  66, 154):���� RE IN LO CH20
      if(L_(556)) then
         C30_esike=C30_epike
      else
         C30_esike=C30_ipike
      endif
C FDA20_logic.fgi(  94, 153):���� RE IN LO CH20
      L_(570)=.true.
C FDA20_logic.fgi(  52,  63):��������� ���������� (�������)
      L0_etike=R0_otike.ne.R0_itike
      R0_itike=R0_otike
C FDA20_logic.fgi(  39,  56):���������� ������������� ������
      if(L0_etike) then
         L_(567)=L_(570)
      else
         L_(567)=.false.
      endif
C FDA20_logic.fgi(  56,  62):���� � ������������� �������
      L_(3)=L_(567)
C FDA20_logic.fgi(  56,  62):������-�������: ���������� ��� �������������� ������
      L_(571)=.true.
C FDA20_logic.fgi(  52,  80):��������� ���������� (�������)
      L0_evike=R0_ovike.ne.R0_ivike
      R0_ivike=R0_ovike
C FDA20_logic.fgi(  39,  73):���������� ������������� ������
      if(L0_evike) then
         L_(568)=L_(571)
      else
         L_(568)=.false.
      endif
C FDA20_logic.fgi(  56,  79):���� � ������������� �������
      L_(2)=L_(568)
C FDA20_logic.fgi(  56,  79):������-�������: ���������� ��� �������������� ������
      L_(565) = L_(2).OR.L_(3)
C FDA20_logic.fgi(  66,  92):���
      L_(572)=.true.
C FDA20_logic.fgi(  52,  96):��������� ���������� (�������)
      L0_exike=R0_oxike.ne.R0_ixike
      R0_ixike=R0_oxike
C FDA20_logic.fgi(  39,  89):���������� ������������� ������
      if(L0_exike) then
         L_(569)=L_(572)
      else
         L_(569)=.false.
      endif
C FDA20_logic.fgi(  56,  95):���� � ������������� �������
      L_(1)=L_(569)
C FDA20_logic.fgi(  56,  95):������-�������: ���������� ��� �������������� ������
      L_(561) = L_(1).OR.L_(2)
C FDA20_logic.fgi(  66,  59):���
      L_atike=(L_(3).or.L_atike).and..not.(L_(561))
      L_(562)=.not.L_atike
C FDA20_logic.fgi(  74,  61):RS �������
      L_usike=L_atike
C FDA20_logic.fgi(  90,  63):������,FDA2_tech_mode
      L_(563) = L_(1).OR.L_(3)
C FDA20_logic.fgi(  66,  76):���
      L_avike=(L_(2).or.L_avike).and..not.(L_(563))
      L_(564)=.not.L_avike
C FDA20_logic.fgi(  74,  78):RS �������
      L_utike=L_avike
C FDA20_logic.fgi(  90,  80):������,FDA2_ruch_mode
      L_axike=(L_(1).or.L_axike).and..not.(L_(565))
      L_(566)=.not.L_axike
C FDA20_logic.fgi(  74,  94):RS �������
      L_uvike=L_axike
C FDA20_logic.fgi(  90,  96):������,FDA2_avt_mode
      !{
      Call BVALVE_MAN(deltat,REAL(R_atox,4),L_ibux,L_obux
     &,R8_arox,L_usike,
     & L_utike,L_uvike,I_ixox,I_oxox,R_amox,
     & REAL(R_imox,4),R_epox,REAL(R_opox,4),
     & R_omox,REAL(R_apox,4),I_ovox,I_uxox,I_exox,I_axox,L_upox
     &,
     & L_abux,L_idux,L_ipox,L_umox,
     & L_urox,L_orox,L_ubux,L_emox,L_esox,L_afux,L_ebux,
     & L_osox,L_usox,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_irox,
     & L_erox,L_odux,R_ivox,REAL(R_asox,4),L_udux,L_efux,L_etox
     &,
     & L_itox,L_otox,L_avox,L_evox,L_utox)
      !}

      if(L_evox.or.L_avox.or.L_utox.or.L_otox.or.L_itox.or.L_etox
     &) then      
                  I_uvox = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uvox = z'40000000'
      endif
C FDA20_vlv.fgi(  88, 150):���� ���������� ������ �������,20FDA21AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_udeke,4),L_emeke,L_imeke
     &,R8_uxake,L_usike,
     & L_utike,L_uvike,I_eleke,I_ileke,R_utake,
     & REAL(R_evake,4),R_axake,REAL(R_ixake,4),
     & R_ivake,REAL(R_uvake,4),I_ikeke,I_oleke,I_aleke,I_ukeke
     &,L_oxake,
     & L_uleke,L_epeke,L_exake,L_ovake,
     & L_obeke,L_ibeke,L_omeke,L_avake,L_adeke,L_upeke,L_ameke
     &,
     & L_ideke,L_odeke,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_ebeke,
     & L_abeke,L_ipeke,R_ekeke,REAL(R_ubeke,4),L_opeke,L_areke
     &,L_afeke,
     & L_efeke,L_ifeke,L_ufeke,L_akeke,L_ofeke)
      !}

      if(L_akeke.or.L_ufeke.or.L_ofeke.or.L_ifeke.or.L_efeke.or.L_afeke
     &) then      
                  I_okeke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okeke = z'40000000'
      endif
C FDA20_vlv.fgi(  88, 176):���� ���������� ������ �������,20FDA23AA101
      !{
      Call BVALVE_MAN(deltat,REAL(R_esobe,4),L_oxobe,L_uxobe
     &,R8_epobe,L_usike,
     & L_utike,L_uvike,I_ovobe,I_uvobe,R_elobe,
     & REAL(R_olobe,4),R_imobe,REAL(R_umobe,4),
     & R_ulobe,REAL(R_emobe,4),I_utobe,I_axobe,I_ivobe,I_evobe
     &,L_apobe,
     & L_exobe,L_obube,L_omobe,L_amobe,
     & L_arobe,L_upobe,L_abube,L_ilobe,L_irobe,L_edube,L_ixobe
     &,
     & L_urobe,L_asobe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_opobe,
     & L_ipobe,L_ubube,R_otobe,REAL(R_erobe,4),L_adube,L_idube
     &,L_isobe,
     & L_osobe,L_usobe,L_etobe,L_itobe,L_atobe)
      !}

      if(L_itobe.or.L_etobe.or.L_atobe.or.L_usobe.or.L_osobe.or.L_isobe
     &) then      
                  I_avobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avobe = z'40000000'
      endif
C FDA20_vlv.fgi( 123, 150):���� ���������� ������ �������,20FDA21AA101
      !{
      Call BVALVE_MAN(deltat,REAL(R_atofe,4),L_ibufe,L_obufe
     &,R8_arofe,L_usike,
     & L_utike,L_uvike,I_ixofe,I_oxofe,R_amofe,
     & REAL(R_imofe,4),R_epofe,REAL(R_opofe,4),
     & R_omofe,REAL(R_apofe,4),I_ovofe,I_uxofe,I_exofe,I_axofe
     &,L_upofe,
     & L_abufe,L_idufe,L_ipofe,L_umofe,
     & L_urofe,L_orofe,L_ubufe,L_emofe,L_esofe,L_afufe,L_ebufe
     &,
     & L_osofe,L_usofe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_irofe,
     & L_erofe,L_odufe,R_ivofe,REAL(R_asofe,4),L_udufe,L_efufe
     &,L_etofe,
     & L_itofe,L_otofe,L_avofe,L_evofe,L_utofe)
      !}

      if(L_evofe.or.L_avofe.or.L_utofe.or.L_otofe.or.L_itofe.or.L_etofe
     &) then      
                  I_uvofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uvofe = z'40000000'
      endif
C FDA20_vlv.fgi(  73, 150):���� ���������� ������ �������,20FDA23AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_abike,4),L_ikike,L_okike
     &,R8_iteke,C30_uveke,
     & L_usike,L_utike,L_uvike,I_ifike,I_ofike,R_ixeke,R_exeke
     &,
     & R_ireke,REAL(R_ureke,4),R_oseke,
     & REAL(R_ateke,4),R_aseke,REAL(R_iseke,4),I_odike,
     & I_ufike,I_efike,I_afike,L_eteke,L_akike,L_ilike,L_useke
     &,
     & L_eseke,L_eveke,L_aveke,L_ukike,L_oreke,L_oveke,
     & L_amike,L_ekike,L_oxeke,L_uxeke,REAL(R8_ereke,8),REAL
     &(1.0,4),R8_axeke,
     & L_uteke,L_oteke,L_olike,R_idike,REAL(R_iveke,4),L_ulike
     &,L_emike,
     & L_ebike,L_ibike,L_obike,L_adike,L_edike,L_ubike)
      !}

      if(L_edike.or.L_adike.or.L_ubike.or.L_obike.or.L_ibike.or.L_ebike
     &) then      
                  I_udike = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_udike = z'40000000'
      endif
C FDA20_vlv.fgi(  73, 176):���� ���������� �������� ��������,20FDA23AA201
      R_ekox=R_exeke
C FDA20_vent_log.fgi( 195,  37):������,20FDA23AA201XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_afox,R_ulox,REAL(1,4)
     &,
     & REAL(R_ofox,4),REAL(R_ufox,4),
     & REAL(R_udox,4),REAL(R_odox,4),I_olox,
     & REAL(R_ikox,4),L_okox,REAL(R_ukox,4),L_alox,L_elox
     &,R_akox,
     & REAL(R_ifox,4),REAL(R_efox,4),L_ilox,REAL(R_ekox,4
     &))
      !}
C FDA20_vlv.fgi( 465, 183):���������� �������,20FDA23AA201XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ipux,4),L_utux,L_avux
     &,R8_ilux,L_usike,
     & L_utike,L_uvike,I_usux,I_atux,R_ifux,
     & REAL(R_ufux,4),R_okux,REAL(R_alux,4),
     & R_akux,REAL(R_ikux,4),I_asux,I_etux,I_osux,I_isux,L_elux
     &,
     & L_itux,L_uvux,L_ukux,L_ekux,
     & L_emux,L_amux,L_evux,L_ofux,L_omux,L_ixux,L_otux,
     & L_apux,L_epux,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_ulux,
     & L_olux,L_axux,R_urux,REAL(R_imux,4),L_exux,L_oxux,L_opux
     &,
     & L_upux,L_arux,L_irux,L_orux,L_erux)
      !}

      if(L_orux.or.L_irux.or.L_erux.or.L_arux.or.L_upux.or.L_opux
     &) then      
                  I_esux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_esux = z'40000000'
      endif
C FDA20_vlv.fgi(  56, 150):���� ���������� ������ �������,20FDA21AA102
      !{
      Call BVALVE_MAN(deltat,REAL(R_omube,4),L_atube,L_etube
     &,R8_okube,L_usike,
     & L_utike,L_uvike,I_asube,I_esube,R_odube,
     & REAL(R_afube,4),R_ufube,REAL(R_ekube,4),
     & R_efube,REAL(R_ofube,4),I_erube,I_isube,I_urube,I_orube
     &,L_ikube,
     & L_osube,L_avube,L_akube,L_ifube,
     & L_ilube,L_elube,L_itube,L_udube,L_ulube,L_ovube,L_usube
     &,
     & L_emube,L_imube,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_alube,
     & L_ukube,L_evube,R_arube,REAL(R_olube,4),L_ivube,L_uvube
     &,L_umube,
     & L_apube,L_epube,L_opube,L_upube,L_ipube)
      !}

      if(L_upube.or.L_opube.or.L_ipube.or.L_epube.or.L_apube.or.L_umube
     &) then      
                  I_irube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_irube = z'40000000'
      endif
C FDA20_vlv.fgi( 140, 150):���� ���������� ������ �������,20FDA21AA104
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uvibe,4),L_efobe,L_ifobe
     &,R8_isibe,C30_utibe,
     & L_usike,L_utike,L_uvike,I_edobe,I_idobe,R_evibe,R_avibe
     &,
     & R_ipibe,REAL(R_upibe,4),R_oribe,
     & REAL(R_asibe,4),R_aribe,REAL(R_iribe,4),I_ibobe,
     & I_odobe,I_adobe,I_ubobe,L_esibe,L_udobe,L_ekobe,L_uribe
     &,
     & L_eribe,L_etibe,L_atibe,L_ofobe,L_opibe,L_otibe,
     & L_ukobe,L_afobe,L_ivibe,L_ovibe,REAL(R8_ereke,8),REAL
     &(1.0,4),R8_axeke,
     & L_usibe,L_osibe,L_ikobe,R_ebobe,REAL(R_itibe,4),L_okobe
     &,L_alobe,
     & L_axibe,L_exibe,L_ixibe,L_uxibe,L_abobe,L_oxibe)
      !}

      if(L_abobe.or.L_uxibe.or.L_oxibe.or.L_ixibe.or.L_exibe.or.L_axibe
     &) then      
                  I_obobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_obobe = z'40000000'
      endif
C FDA20_vlv.fgi( 105, 150):���� ���������� �������� ��������,20FDA21AB003
      R_afov=R_avibe
C FDA20_vent_log.fgi( 115,  58):������,20FDA21AB003XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_ubov,R_okov,REAL(1,4)
     &,
     & REAL(R_idov,4),REAL(R_odov,4),
     & REAL(R_obov,4),REAL(R_ibov,4),I_ikov,
     & REAL(R_efov,4),L_ifov,REAL(R_ofov,4),L_ufov,L_akov
     &,R_udov,
     & REAL(R_edov,4),REAL(R_adov,4),L_ekov,REAL(R_afov,4
     &))
      !}
C FDA20_vlv.fgi( 377, 125):���������� �������,20FDA21AB003XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ipufe,4),L_utufe,L_avufe
     &,R8_ilufe,L_usike,
     & L_utike,L_uvike,I_usufe,I_atufe,R_ifufe,
     & REAL(R_ufufe,4),R_okufe,REAL(R_alufe,4),
     & R_akufe,REAL(R_ikufe,4),I_asufe,I_etufe,I_osufe,I_isufe
     &,L_elufe,
     & L_itufe,L_uvufe,L_ukufe,L_ekufe,
     & L_emufe,L_amufe,L_evufe,L_ofufe,L_omufe,L_ixufe,L_otufe
     &,
     & L_apufe,L_epufe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke
     &,L_ulufe,
     & L_olufe,L_axufe,R_urufe,REAL(R_imufe,4),L_exufe,L_oxufe
     &,L_opufe,
     & L_upufe,L_arufe,L_irufe,L_orufe,L_erufe)
      !}

      if(L_orufe.or.L_irufe.or.L_erufe.or.L_arufe.or.L_upufe.or.L_opufe
     &) then      
                  I_esufe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esufe = z'40000000'
      endif
C FDA20_vlv.fgi( 105, 176):���� ���������� ������ �������,20FDA23AA102
      Call PUMP_HANDLER(deltat,C30_ifake,I_alake,L_usike,L_utike
     &,L_uvike,
     & I_elake,I_ukake,R_edake,REAL(R_odake,4),
     & R_obake,REAL(R_adake,4),I_ilake,L_arake,L_apake,L_usake
     &,
     & L_atake,L_ekake,L_emake,L_omake,L_imake,L_umake,L_opake
     &,L_ibake,
     & L_idake,L_ebake,L_ubake,L_esake,L_(555),
     & L_isake,L_(554),L_abake,L_uxufe,L_ikake,I_olake,R_irake
     &,R_orake,
     & L_otake,L_upake,L_osake,REAL(R8_ereke,8),L_ipake,
     & REAL(R8_epake,8),R_etake,REAL(R_ulake,4),R_amake,REAL
     &(R8_akake,8),R_ufake,
     & R8_axeke,R_itake,R8_epake,REAL(R_udake,4),REAL(R_afake
     &,4))
C FDA20_vlv.fgi(  55, 124):���������� ���������� �������,20FDA23CU001KN01
C label 766  try766=try766-1
C sav1=R_itake
C sav2=R_etake
C sav3=L_arake
C sav4=L_opake
C sav5=L_apake
C sav6=R8_epake
C sav7=R_amake
C sav8=I_olake
C sav9=I_ilake
C sav10=I_elake
C sav11=I_alake
C sav12=I_ukake
C sav13=I_okake
C sav14=L_ikake
C sav15=L_ekake
C sav16=R_ufake
C sav17=C30_ifake
C sav18=L_idake
C sav19=L_ubake
      Call PUMP_HANDLER(deltat,C30_ifake,I_alake,L_usike,L_utike
     &,L_uvike,
     & I_elake,I_ukake,R_edake,REAL(R_odake,4),
     & R_obake,REAL(R_adake,4),I_ilake,L_arake,L_apake,L_usake
     &,
     & L_atake,L_ekake,L_emake,L_omake,L_imake,L_umake,L_opake
     &,L_ibake,
     & L_idake,L_ebake,L_ubake,L_esake,L_(555),
     & L_isake,L_(554),L_abake,L_uxufe,L_ikake,I_olake,R_irake
     &,R_orake,
     & L_otake,L_upake,L_osake,REAL(R8_ereke,8),L_ipake,
     & REAL(R8_epake,8),R_etake,REAL(R_ulake,4),R_amake,REAL
     &(R8_akake,8),R_ufake,
     & R8_axeke,R_itake,R8_epake,REAL(R_udake,4),REAL(R_afake
     &,4))
C FDA20_vlv.fgi(  55, 124):recalc:���������� ���������� �������,20FDA23CU001KN01
C if(sav1.ne.R_itake .and. try766.gt.0) goto 766
C if(sav2.ne.R_etake .and. try766.gt.0) goto 766
C if(sav3.ne.L_arake .and. try766.gt.0) goto 766
C if(sav4.ne.L_opake .and. try766.gt.0) goto 766
C if(sav5.ne.L_apake .and. try766.gt.0) goto 766
C if(sav6.ne.R8_epake .and. try766.gt.0) goto 766
C if(sav7.ne.R_amake .and. try766.gt.0) goto 766
C if(sav8.ne.I_olake .and. try766.gt.0) goto 766
C if(sav9.ne.I_ilake .and. try766.gt.0) goto 766
C if(sav10.ne.I_elake .and. try766.gt.0) goto 766
C if(sav11.ne.I_alake .and. try766.gt.0) goto 766
C if(sav12.ne.I_ukake .and. try766.gt.0) goto 766
C if(sav13.ne.I_okake .and. try766.gt.0) goto 766
C if(sav14.ne.L_ikake .and. try766.gt.0) goto 766
C if(sav15.ne.L_ekake .and. try766.gt.0) goto 766
C if(sav16.ne.R_ufake .and. try766.gt.0) goto 766
C if(sav17.ne.C30_ifake .and. try766.gt.0) goto 766
C if(sav18.ne.L_idake .and. try766.gt.0) goto 766
C if(sav19.ne.L_ubake .and. try766.gt.0) goto 766
      if(L_opake) then
         R_(327)=R_(328)
      else
         R_(327)=R_(329)
      endif
C FDA20_vent_log.fgi( 167, 270):���� RE IN LO CH7
      R8_efes=(R0_udes*R_(326)+deltat*R_(327))/(R0_udes+deltat
     &)
C FDA20_vent_log.fgi( 177, 271):�������������� �����  
      R8_afes=R8_efes
C FDA20_vent_log.fgi( 196, 271):������,F_FDA23CU001_G
      Call PUMP_HANDLER(deltat,C30_exebe,I_ubibe,L_usike,L_utike
     &,L_uvike,
     & I_adibe,I_obibe,R_avebe,REAL(R_ivebe,4),
     & R_itebe,REAL(R_utebe,4),I_edibe,L_okibe,L_ufibe,L_imibe
     &,
     & L_omibe,L_abibe,L_afibe,L_ifibe,L_efibe,L_ofibe,L_ikibe
     &,L_etebe,
     & L_evebe,L_atebe,L_otebe,L_ulibe,L_(537),
     & L_amibe,L_(536),L_usebe,L_osebe,L_ebibe,I_idibe,R_alibe
     &,R_elibe,
     & L_epibe,L_upake,L_emibe,REAL(R8_ereke,8),L_ekibe,
     & REAL(R8_akibe,8),R_umibe,REAL(R_odibe,4),R_udibe,REAL
     &(R8_uxebe,8),R_oxebe,
     & R8_axeke,R_apibe,R8_akibe,REAL(R_ovebe,4),REAL(R_uvebe
     &,4))
C FDA20_vlv.fgi(  73, 124):���������� ���������� �������,20FDA21AX001KN01
C label 774  try774=try774-1
C sav1=R_apibe
C sav2=R_umibe
C sav3=L_okibe
C sav4=L_ikibe
C sav5=L_ufibe
C sav6=R8_akibe
C sav7=R_udibe
C sav8=I_idibe
C sav9=I_edibe
C sav10=I_adibe
C sav11=I_ubibe
C sav12=I_obibe
C sav13=I_ibibe
C sav14=L_ebibe
C sav15=L_abibe
C sav16=R_oxebe
C sav17=C30_exebe
C sav18=L_evebe
C sav19=L_otebe
      Call PUMP_HANDLER(deltat,C30_exebe,I_ubibe,L_usike,L_utike
     &,L_uvike,
     & I_adibe,I_obibe,R_avebe,REAL(R_ivebe,4),
     & R_itebe,REAL(R_utebe,4),I_edibe,L_okibe,L_ufibe,L_imibe
     &,
     & L_omibe,L_abibe,L_afibe,L_ifibe,L_efibe,L_ofibe,L_ikibe
     &,L_etebe,
     & L_evebe,L_atebe,L_otebe,L_ulibe,L_(537),
     & L_amibe,L_(536),L_usebe,L_osebe,L_ebibe,I_idibe,R_alibe
     &,R_elibe,
     & L_epibe,L_upake,L_emibe,REAL(R8_ereke,8),L_ekibe,
     & REAL(R8_akibe,8),R_umibe,REAL(R_odibe,4),R_udibe,REAL
     &(R8_uxebe,8),R_oxebe,
     & R8_axeke,R_apibe,R8_akibe,REAL(R_ovebe,4),REAL(R_uvebe
     &,4))
C FDA20_vlv.fgi(  73, 124):recalc:���������� ���������� �������,20FDA21AX001KN01
C if(sav1.ne.R_apibe .and. try774.gt.0) goto 774
C if(sav2.ne.R_umibe .and. try774.gt.0) goto 774
C if(sav3.ne.L_okibe .and. try774.gt.0) goto 774
C if(sav4.ne.L_ikibe .and. try774.gt.0) goto 774
C if(sav5.ne.L_ufibe .and. try774.gt.0) goto 774
C if(sav6.ne.R8_akibe .and. try774.gt.0) goto 774
C if(sav7.ne.R_udibe .and. try774.gt.0) goto 774
C if(sav8.ne.I_idibe .and. try774.gt.0) goto 774
C if(sav9.ne.I_edibe .and. try774.gt.0) goto 774
C if(sav10.ne.I_adibe .and. try774.gt.0) goto 774
C if(sav11.ne.I_ubibe .and. try774.gt.0) goto 774
C if(sav12.ne.I_obibe .and. try774.gt.0) goto 774
C if(sav13.ne.I_ibibe .and. try774.gt.0) goto 774
C if(sav14.ne.L_ebibe .and. try774.gt.0) goto 774
C if(sav15.ne.L_abibe .and. try774.gt.0) goto 774
C if(sav16.ne.R_oxebe .and. try774.gt.0) goto 774
C if(sav17.ne.C30_exebe .and. try774.gt.0) goto 774
C if(sav18.ne.L_evebe .and. try774.gt.0) goto 774
C if(sav19.ne.L_otebe .and. try774.gt.0) goto 774
      if(L_ikibe) then
         R_(331)=R_(332)
      else
         R_(331)=R_(333)
      endif
C FDA20_vent_log.fgi(  89, 270):���� RE IN LO CH7
      R8_ekes=(R0_ufes*R_(330)+deltat*R_(331))/(R0_ufes+deltat
     &)
C FDA20_vent_log.fgi( 101, 271):�������������� �����  
      R8_akes=R8_ekes
C FDA20_vent_log.fgi( 118, 271):������,F_FDA21AX001_G
      Call PUMP_HANDLER(deltat,C30_alit,I_omit,L_usike,L_utike
     &,L_uvike,
     & I_umit,I_imit,R_ufit,REAL(R_ekit,4),
     & R_efit,REAL(R_ofit,4),I_apit,L_isit,L_orit,L_evit,
     & L_ivit,L_ulit,L_upit,L_erit,L_arit,L_irit,L_esit,L_afit
     &,
     & L_akit,L_udit,L_ifit,L_otit,L_(535),
     & L_utit,L_(534),L_odit,L_idit,L_amit,I_epit,R_usit,R_atit
     &,
     & L_axit,L_upake,L_avit,REAL(R8_ereke,8),L_asit,
     & REAL(R8_urit,8),R_ovit,REAL(R_ipit,4),R_opit,REAL(R8_olit
     &,8),R_ilit,
     & R8_axeke,R_uvit,R8_urit,REAL(R_ikit,4),REAL(R_okit
     &,4))
C FDA20_vlv.fgi(  91, 124):���������� ���������� �������,20FDA22AX001KN01
C label 782  try782=try782-1
C sav1=R_uvit
C sav2=R_ovit
C sav3=L_isit
C sav4=L_esit
C sav5=L_orit
C sav6=R8_urit
C sav7=R_opit
C sav8=I_epit
C sav9=I_apit
C sav10=I_umit
C sav11=I_omit
C sav12=I_imit
C sav13=I_emit
C sav14=L_amit
C sav15=L_ulit
C sav16=R_ilit
C sav17=C30_alit
C sav18=L_akit
C sav19=L_ifit
      Call PUMP_HANDLER(deltat,C30_alit,I_omit,L_usike,L_utike
     &,L_uvike,
     & I_umit,I_imit,R_ufit,REAL(R_ekit,4),
     & R_efit,REAL(R_ofit,4),I_apit,L_isit,L_orit,L_evit,
     & L_ivit,L_ulit,L_upit,L_erit,L_arit,L_irit,L_esit,L_afit
     &,
     & L_akit,L_udit,L_ifit,L_otit,L_(535),
     & L_utit,L_(534),L_odit,L_idit,L_amit,I_epit,R_usit,R_atit
     &,
     & L_axit,L_upake,L_avit,REAL(R8_ereke,8),L_asit,
     & REAL(R8_urit,8),R_ovit,REAL(R_ipit,4),R_opit,REAL(R8_olit
     &,8),R_ilit,
     & R8_axeke,R_uvit,R8_urit,REAL(R_ikit,4),REAL(R_okit
     &,4))
C FDA20_vlv.fgi(  91, 124):recalc:���������� ���������� �������,20FDA22AX001KN01
C if(sav1.ne.R_uvit .and. try782.gt.0) goto 782
C if(sav2.ne.R_ovit .and. try782.gt.0) goto 782
C if(sav3.ne.L_isit .and. try782.gt.0) goto 782
C if(sav4.ne.L_esit .and. try782.gt.0) goto 782
C if(sav5.ne.L_orit .and. try782.gt.0) goto 782
C if(sav6.ne.R8_urit .and. try782.gt.0) goto 782
C if(sav7.ne.R_opit .and. try782.gt.0) goto 782
C if(sav8.ne.I_epit .and. try782.gt.0) goto 782
C if(sav9.ne.I_apit .and. try782.gt.0) goto 782
C if(sav10.ne.I_umit .and. try782.gt.0) goto 782
C if(sav11.ne.I_omit .and. try782.gt.0) goto 782
C if(sav12.ne.I_imit .and. try782.gt.0) goto 782
C if(sav13.ne.I_emit .and. try782.gt.0) goto 782
C if(sav14.ne.L_amit .and. try782.gt.0) goto 782
C if(sav15.ne.L_ulit .and. try782.gt.0) goto 782
C if(sav16.ne.R_ilit .and. try782.gt.0) goto 782
C if(sav17.ne.C30_alit .and. try782.gt.0) goto 782
C if(sav18.ne.L_akit .and. try782.gt.0) goto 782
C if(sav19.ne.L_ifit .and. try782.gt.0) goto 782
      if(.not.L_erel) then
         R0_ipel=0.0
      elseif(.not.L0_opel) then
         R0_ipel=R0_epel
      else
         R0_ipel=max(R_(260)-deltat,0.0)
      endif
      L_(457)=L_erel.and.R0_ipel.le.0.0
      L0_opel=L_erel
C FDA20_SP1.fgi(  57, 489):�������� ��������� ������
C label 783  try783=try783-1
      if(L_erel.and..not.L0_amel) then
         R0_olel=R0_ulel
      else
         R0_olel=max(R_(259)-deltat,0.0)
      endif
      L_(450)=R0_olel.gt.0.0
      L0_amel=L_erel
C FDA20_SP1.fgi( 158, 483):������������  �� T
      L_emel=(L_(450).or.L_emel).and..not.(L_elol)
      L_(451)=.not.L_emel
C FDA20_SP1.fgi( 170, 481):RS �������
      L_umel=L_emel
C FDA20_SP1.fgi( 193, 483):������,containerY1set
      if(L_ufal.and..not.L0_uruk) then
         R0_iruk=R0_oruk
      else
         R0_iruk=max(R_(240)-deltat,0.0)
      endif
      L_(416)=R0_iruk.gt.0.0
      L0_uruk=L_ufal
C FDA20_SP1.fgi( 118, 746):������������  �� T
      L0_akal=(L_(416).or.L0_akal).and..not.(L_(417))
      L_ifas=.not.L0_akal
C FDA20_SP1.fgi( 240, 744):RS �������
      if(.not.L0_akal) then
         R0_ifal=0.0
      elseif(.not.L0_ofal) then
         R0_ifal=R0_efal
      else
         R0_ifal=max(R_(247)-deltat,0.0)
      endif
      L_(415)=L0_akal.and.R0_ifal.le.0.0
      L0_ofal=L0_akal
C FDA20_SP1.fgi(  65, 746):�������� ��������� ������
      if(.not.L_ikel) then
         R0_akel=0.0
      elseif(.not.L0_ekel) then
         R0_akel=R0_ufel
      else
         R0_akel=max(R_(257)-deltat,0.0)
      endif
      L_(446)=L_ikel.and.R0_akel.le.0.0
      L0_ekel=L_ikel
C FDA20_SP1.fgi(  57, 441):�������� ��������� ������
      if(L_ikel.and..not.L0_ifel) then
         R0_afel=R0_efel
      else
         R0_afel=max(R_(256)-deltat,0.0)
      endif
      L_(444)=R0_afel.gt.0.0
      L0_ifel=L_ikel
C FDA20_SP1.fgi( 158, 435):������������  �� T
      L_ofel=(L_(444).or.L_ofel).and..not.(L_elol)
      L_(445)=.not.L_ofel
C FDA20_SP1.fgi( 170, 433):RS �������
      L_udel=L_ofel
C FDA20_SP1.fgi( 193, 435):������,containerY2set
      L_(454) = L_(499).OR.L_omel.OR.L_arel
C FDA20_SP1.fgi( 102, 780):���
      if (L_(454)) then
          I_olol = 1
          L_uxil = .true.
      endif
      if (L_atak) then
          L_(308) = .true.
          L_uxil = .false.
      elseif (L_(493)) then
          L_(414) = .true.
          L_uxil = .false.
      endif
      if (I_olol.ne.1) then
          L_uxil = .false.
          L_(414) = .false.
          L_(308) = .false.
      endif
C FDA20_SP1.fgi(  94, 768):��� ������� ��������� � ����������,cset
      if(L_ilol) then
          if (L_(414)) then
              I_olol = 2
              L_ufal = .true.
              L_(497) = .false.
          endif
          if (L_(415)) then
              L_(497) = .true.
              L_ufal = .false.
          endif
          if (I_olol.ne.2) then
              L_ufal = .false.
              L_(497) = .false.
          endif
      else
          L_(497) = .false.
      endif
C FDA20_SP1.fgi(  94, 746):��� ������� ���������,cset
C sav1=L_(416)
      if(L_ufal.and..not.L0_uruk) then
         R0_iruk=R0_oruk
      else
         R0_iruk=max(R_(240)-deltat,0.0)
      endif
      L_(416)=R0_iruk.gt.0.0
      L0_uruk=L_ufal
C FDA20_SP1.fgi( 118, 746):recalc:������������  �� T
C if(sav1.ne.L_(416) .and. try799.gt.0) goto 799
      if(L_ilol) then
          if (L_(497)) then
              I_olol = 3
              L_ukol = .true.
              L_(498) = .false.
          endif
          if (L_urur) then
              L_(498) = .true.
              L_ukol = .false.
          endif
          if (I_olol.ne.3) then
              L_ukol = .false.
              L_(498) = .false.
          endif
      else
          L_(498) = .false.
      endif
C FDA20_SP1.fgi(  94, 735):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(498)) then
              I_olol = 4
              L_afal = .true.
              L_(491) = .false.
          endif
          if (L_udal) then
              L_(491) = .true.
              L_afal = .false.
          endif
          if (I_olol.ne.4) then
              L_afal = .false.
              L_(491) = .false.
          endif
      else
          L_(491) = .false.
      endif
C FDA20_SP1.fgi(  94, 724):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(491)) then
              I_olol = 5
              L_axil = .true.
              L_(492) = .false.
          endif
          if (L_uvil) then
              L_(492) = .true.
              L_axil = .false.
          endif
          if (I_olol.ne.5) then
              L_axil = .false.
              L_(492) = .false.
          endif
      else
          L_(492) = .false.
      endif
C FDA20_SP1.fgi(  94, 713):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(492)) then
              I_olol = 6
              L_ivil = .true.
              L_(490) = .false.
          endif
          if (L_evil) then
              L_(490) = .true.
              L_ivil = .false.
          endif
          if (I_olol.ne.6) then
              L_ivil = .false.
              L_(490) = .false.
          endif
      else
          L_(490) = .false.
      endif
C FDA20_SP1.fgi(  94, 702):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(490)) then
              I_olol = 7
              L_obal = .true.
              L_(495) = .false.
          endif
          if (L_ibal) then
              L_(495) = .true.
              L_obal = .false.
          endif
          if (I_olol.ne.7) then
              L_obal = .false.
              L_(495) = .false.
          endif
      else
          L_(495) = .false.
      endif
C FDA20_SP1.fgi(  94, 691):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(495)) then
              I_olol = 8
              L_ikol = .true.
              L_(496) = .false.
          endif
          if (L_apur) then
              L_(496) = .true.
              L_ikol = .false.
          endif
          if (I_olol.ne.8) then
              L_ikol = .false.
              L_(496) = .false.
          endif
      else
          L_(496) = .false.
      endif
C FDA20_SP1.fgi(  94, 680):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(496)) then
              I_olol = 9
              L_esil = .true.
              L_(487) = .false.
          endif
          if (L_uvil) then
              L_(487) = .true.
              L_esil = .false.
          endif
          if (I_olol.ne.9) then
              L_esil = .false.
              L_(487) = .false.
          endif
      else
          L_(487) = .false.
      endif
C FDA20_SP1.fgi(  94, 669):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(487)) then
              I_olol = 10
              L_etil = .true.
              L_(486) = .false.
          endif
          if (L_uril) then
              L_(486) = .true.
              L_etil = .false.
          endif
          if (I_olol.ne.10) then
              L_etil = .false.
              L_(486) = .false.
          endif
      else
          L_(486) = .false.
      endif
C FDA20_SP1.fgi(  94, 658):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(486)) then
              I_olol = 11
              L_util = .true.
              L_(484) = .false.
          endif
          if (L_evil) then
              L_(484) = .true.
              L_util = .false.
          endif
          if (I_olol.ne.11) then
              L_util = .false.
              L_(484) = .false.
          endif
      else
          L_(484) = .false.
      endif
C FDA20_SP1.fgi(  94, 647):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(484)) then
              I_olol = 12
              L_oril = .true.
              L_(485) = .false.
          endif
          if (L_uvil) then
              L_(485) = .true.
              L_oril = .false.
          endif
          if (I_olol.ne.12) then
              L_oril = .false.
              L_(485) = .false.
          endif
      else
          L_(485) = .false.
      endif
C FDA20_SP1.fgi(  94, 636):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(485)) then
              I_olol = 13
              L_atil = .true.
              L_(481) = .false.
          endif
          if (L_uril) then
              L_(481) = .true.
              L_atil = .false.
          endif
          if (I_olol.ne.13) then
              L_atil = .false.
              L_(481) = .false.
          endif
      else
          L_(481) = .false.
      endif
C FDA20_SP1.fgi(  94, 625):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(481)) then
              I_olol = 14
              L_amil = .true.
              L_(482) = .false.
          endif
          if (L_(483)) then
              L_(482) = .true.
              L_amil = .false.
          endif
          if (I_olol.ne.14) then
              L_amil = .false.
              L_(482) = .false.
          endif
      else
          L_(482) = .false.
      endif
C FDA20_SP1.fgi(  94, 614):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(482)) then
              I_olol = 15
              L_elil = .true.
              L_(478) = .false.
          endif
          if (L_abip) then
              L_(478) = .true.
              L_elil = .false.
          endif
          if (I_olol.ne.15) then
              L_elil = .false.
              L_(478) = .false.
          endif
      else
          L_(478) = .false.
      endif
C FDA20_SP1.fgi(  94, 603):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(478)) then
              I_olol = 16
              L_ikil = .true.
              L_(476) = .false.
          endif
          if (L_(477)) then
              L_(476) = .true.
              L_ikil = .false.
          endif
          if (I_olol.ne.16) then
              L_ikil = .false.
              L_(476) = .false.
          endif
      else
          L_(476) = .false.
      endif
C FDA20_SP1.fgi(  94, 592):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(476)) then
              I_olol = 17
              L_otil = .true.
              L_(461) = .false.
          endif
          if (L_evil) then
              L_(461) = .true.
              L_otil = .false.
          endif
          if (I_olol.ne.17) then
              L_otil = .false.
              L_(461) = .false.
          endif
      else
          L_(461) = .false.
      endif
C FDA20_SP1.fgi(  94, 581):��� ������� ���������,cset
      if (L_(461)) then
          I_olol = 18
          L_apel = .true.
      endif
      if (L_umel) then
          L_(455) = .true.
          L_apel = .false.
      elseif (L_(456)) then
          L_(474) = .true.
          L_apel = .false.
      endif
      if (I_olol.ne.18) then
          L_apel = .false.
          L_(474) = .false.
          L_(455) = .false.
      endif
C FDA20_SP1.fgi(  94, 568):��� ������� ��������� � ����������,cset
      if(L_ilol) then
          if (L_(474)) then
              I_olol = 19
              L_ofil = .true.
              L_(475) = .false.
          endif
          if (L_uvil) then
              L_(475) = .true.
              L_ofil = .false.
          endif
          if (I_olol.ne.19) then
              L_ofil = .false.
              L_(475) = .false.
          endif
      else
          L_(475) = .false.
      endif
C FDA20_SP1.fgi(  94, 517):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(475)) then
              I_olol = 20
              L_usil = .true.
              L_(460) = .false.
          endif
          if (L_uril) then
              L_(460) = .true.
              L_usil = .false.
          endif
          if (I_olol.ne.20) then
              L_usil = .false.
              L_(460) = .false.
          endif
      else
          L_(460) = .false.
      endif
C FDA20_SP1.fgi(  94, 506):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(460)) then
              I_olol = 21
              L_erel = .true.
              L_arel = .false.
          endif
          if (L_(457)) then
              L_arel = .true.
              L_erel = .false.
          endif
          if (I_olol.ne.21) then
              L_erel = .false.
              L_arel = .false.
          endif
      else
          L_arel = .false.
      endif
C FDA20_SP1.fgi(  94, 489):��� ������� ���������,cset
C sav1=L_(457)
      if(.not.L_erel) then
         R0_ipel=0.0
      elseif(.not.L0_opel) then
         R0_ipel=R0_epel
      else
         R0_ipel=max(R_(260)-deltat,0.0)
      endif
      L_(457)=L_erel.and.R0_ipel.le.0.0
      L0_opel=L_erel
C FDA20_SP1.fgi(  57, 489):recalc:�������� ��������� ������
C if(sav1.ne.L_(457) .and. try783.gt.0) goto 783
C sav1=L_(450)
      if(L_erel.and..not.L0_amel) then
         R0_olel=R0_ulel
      else
         R0_olel=max(R_(259)-deltat,0.0)
      endif
      L_(450)=R0_olel.gt.0.0
      L0_amel=L_erel
C FDA20_SP1.fgi( 158, 483):recalc:������������  �� T
C if(sav1.ne.L_(450) .and. try785.gt.0) goto 785
      if (L_(455)) then
          I_olol = 22
          L_odel = .true.
      endif
      if (L_udel) then
          L_(442) = .true.
          L_odel = .false.
      elseif (L_(443)) then
          L_(452) = .true.
          L_odel = .false.
      endif
      if (I_olol.ne.22) then
          L_odel = .false.
          L_(452) = .false.
          L_(442) = .false.
      endif
C FDA20_SP1.fgi(  98, 552):��� ������� ��������� � ����������,cset
      if(L_ilol) then
          if (L_(452)) then
              I_olol = 23
              L_imel = .true.
              L_(453) = .false.
          endif
          if (L_uvil) then
              L_(453) = .true.
              L_imel = .false.
          endif
          if (I_olol.ne.23) then
              L_imel = .false.
              L_(453) = .false.
          endif
      else
          L_(453) = .false.
      endif
C FDA20_SP1.fgi(  94, 469):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(453)) then
              I_olol = 24
              L_osil = .true.
              L_(449) = .false.
          endif
          if (L_uril) then
              L_(449) = .true.
              L_osil = .false.
          endif
          if (I_olol.ne.24) then
              L_osil = .false.
              L_(449) = .false.
          endif
      else
          L_(449) = .false.
      endif
C FDA20_SP1.fgi(  94, 458):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(449)) then
              I_olol = 25
              L_ikel = .true.
              L_omel = .false.
          endif
          if (L_(446)) then
              L_omel = .true.
              L_ikel = .false.
          endif
          if (I_olol.ne.25) then
              L_ikel = .false.
              L_omel = .false.
          endif
      else
          L_omel = .false.
      endif
C FDA20_SP1.fgi(  94, 441):��� ������� ���������,cset
C sav1=L_(444)
      if(L_ikel.and..not.L0_ifel) then
         R0_afel=R0_efel
      else
         R0_afel=max(R_(256)-deltat,0.0)
      endif
      L_(444)=R0_afel.gt.0.0
      L0_ifel=L_ikel
C FDA20_SP1.fgi( 158, 435):recalc:������������  �� T
C if(sav1.ne.L_(444) .and. try814.gt.0) goto 814
C sav1=L_(446)
      if(.not.L_ikel) then
         R0_akel=0.0
      elseif(.not.L0_ekel) then
         R0_akel=R0_ufel
      else
         R0_akel=max(R_(257)-deltat,0.0)
      endif
      L_(446)=L_ikel.and.R0_akel.le.0.0
      L0_ekel=L_ikel
C FDA20_SP1.fgi(  57, 441):recalc:�������� ��������� ������
C if(sav1.ne.L_(446) .and. try812.gt.0) goto 812
C sav1=L_(454)
      L_(454) = L_(499).OR.L_omel.OR.L_arel
C FDA20_SP1.fgi( 102, 780):recalc:���
C if(sav1.ne.L_(454) .and. try828.gt.0) goto 828
      L_ipam=L_ikel
C FDA20_SP1.fgi( 173, 441):������,vint_p1
      !{
      Call DAT_DISCR_HANDLER(deltat,I_imam,L_apam,R_umam,
     & REAL(R_epam,4),L_ipam,L_emam,I_omam)
      !}
C FDA20_lamp.fgi( 290, 258):���������� ������� ���������,20FDA21AL002KF02BQ28
      L_(447) = L_osil.AND.L_efil
C FDA20_SP1.fgi( 165, 452):�
      L_okel=(L_(447).or.L_okel).and..not.(L_elol)
      L_(448)=.not.L_okel
C FDA20_SP1.fgi( 181, 450):RS �������
      L_amim=L_okel
C FDA20_SP1.fgi( 197, 460):������,cyl27
      L_(506) = (.NOT.L_amim)
C FDA20_lamp.fgi( 224, 311):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_umim,L_ipim,R_epim,
     & REAL(R_opim,4),L_(506),L_omim,I_apim)
      !}
C FDA20_lamp.fgi( 238, 310):���������� ������� ���������,20FDA21AL002BQ23
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alim,L_olim,R_ilim,
     & REAL(R_ulim,4),L_amim,L_ukim,I_elim)
      !}
C FDA20_lamp.fgi( 238, 322):���������� ������� ���������,20FDA21AL002BQ24
      L_isim=L_okel
C FDA20_SP1.fgi( 197, 456):������,cyl28
      L_(507) = (.NOT.L_isim)
C FDA20_lamp.fgi( 277, 311):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etim,L_utim,R_otim,
     & REAL(R_avim,4),L_(507),L_atim,I_itim)
      !}
C FDA20_lamp.fgi( 290, 310):���������� ������� ���������,20FDA21AL002BQ21
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irim,L_asim,R_urim,
     & REAL(R_esim,4),L_isim,L_erim,I_orim)
      !}
C FDA20_lamp.fgi( 290, 322):���������� ������� ���������,20FDA21AL002BQ22
      L_oxem=L_okel
C FDA20_SP1.fgi( 197, 452):������,cyl29
      L_(505) = (.NOT.L_oxem)
C FDA20_lamp.fgi( 277, 279):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atem,L_otem,R_item,
     & REAL(R_utem,4),L_(505),L_usem,I_etem)
      !}
C FDA20_lamp.fgi( 290, 278):���������� ������� ���������,20FDA21AL002BQ28
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovem,L_exem,R_axem,
     & REAL(R_ixem,4),L_oxem,L_ivem,I_uvem)
      !}
C FDA20_lamp.fgi( 290, 290):���������� ������� ���������,20FDA21AL002BQ27
      L_ukel=L_imel
C FDA20_SP1.fgi( 140, 469):������,move_to_y2
      if(L_imel.and..not.L0_aval) then
         R0_otal=R0_utal
      else
         R0_otal=max(R_(252)-deltat,0.0)
      endif
      L_eval=R0_otal.gt.0.0
      L0_aval=L_imel
C FDA20_SP1.fgi( 125, 473):������������  �� T
      L_odom=L_erel
C FDA20_SP1.fgi( 173, 489):������,cyl25
      !{
      Call DAT_DISCR_HANDLER(deltat,I_obom,L_edom,R_adom,
     & REAL(R_idom,4),L_odom,L_ibom,I_ubom)
      !}
C FDA20_lamp.fgi( 122, 234):���������� ������� ���������,20FDA21AL001KE25BQ31
      L_(458) = L_usil.AND.L_efil
C FDA20_SP1.fgi( 165, 500):�
      L_irel=(L_(458).or.L_irel).and..not.(L_elol)
      L_(459)=.not.L_irel
C FDA20_SP1.fgi( 181, 498):RS �������
      L_emom=L_irel
C FDA20_SP1.fgi( 197, 508):������,cyl24
      L_(508) = (.NOT.L_emom)
C FDA20_lamp.fgi( 109, 253):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_apom,L_opom,R_ipom,
     & REAL(R_upom,4),L_(508),L_umom,I_epom)
      !}
C FDA20_lamp.fgi( 122, 252):���������� ������� ���������,20FDA21AL001BQ31
      !{
      Call DAT_DISCR_HANDLER(deltat,I_elom,L_ulom,R_olom,
     & REAL(R_amom,4),L_emom,L_alom,I_ilom)
      !}
C FDA20_lamp.fgi( 122, 264):���������� ������� ���������,20FDA21AL001BQ32
      L_ikom=L_irel
C FDA20_SP1.fgi( 197, 504):������,cyl23
      L_(510) = (.NOT.L_ikom)
C FDA20_lamp.fgi( 109, 283):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epum,L_upum,R_opum,
     & REAL(R_arum,4),L_(510),L_apum,I_ipum)
      !}
C FDA20_lamp.fgi( 122, 282):���������� ������� ���������,20FDA21AL001BQ33
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ifom,L_akom,R_ufom,
     & REAL(R_ekom,4),L_ikom,L_efom,I_ofom)
      !}
C FDA20_lamp.fgi( 122, 294):���������� ������� ���������,20FDA21AL001BQ34
      L_ukum=L_irel
C FDA20_SP1.fgi( 197, 500):������,cyl22
      L_(509) = (.NOT.L_ukum)
C FDA20_lamp.fgi(  56, 283):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_olum,L_emum,R_amum,
     & REAL(R_imum,4),L_(509),L_ilum,I_ulum)
      !}
C FDA20_lamp.fgi(  70, 282):���������� ������� ���������,20FDA21AL001BQ35
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ufum,L_ikum,R_ekum,
     & REAL(R_okum,4),L_ukum,L_ofum,I_akum)
      !}
C FDA20_lamp.fgi(  70, 294):���������� ������� ���������,20FDA21AL001BQ36
      L_ifil=L_ofil
C FDA20_SP1.fgi( 140, 517):������,move_to_y1
      if(L_ofil.and..not.L0_uval) then
         R0_ival=R0_oval
      else
         R0_ival=max(R_(253)-deltat,0.0)
      endif
      L_upel=R0_ival.gt.0.0
      L0_uval=L_ofil
C FDA20_SP1.fgi( 125, 521):������������  �� T
      if(L_ikil.and..not.L0_ekil) then
         R0_ufil=R0_akil
      else
         R0_ufil=max(R_(270)-deltat,0.0)
      endif
      L_(480)=R0_ufil.gt.0.0
      L0_ekil=L_ikil
C FDA20_SP1.fgi( 118, 592):������������  �� T
      if(L_elil.and..not.L0_alil) then
         R0_okil=R0_ukil
      else
         R0_okil=max(R_(271)-deltat,0.0)
      endif
      L_emil=R0_okil.gt.0.0
      L0_alil=L_elil
C FDA20_SP1.fgi( 118, 603):������������  �� T
      if(L_amil.and..not.L0_ulil) then
         R0_ilil=R0_olil
      else
         R0_ilil=max(R_(272)-deltat,0.0)
      endif
      L_(479)=R0_ilil.gt.0.0
      L0_ulil=L_amil
C FDA20_SP1.fgi( 118, 614):������������  �� T
      L_eril=(L_(479).or.L_eril).and..not.(L_(480))
      L_udip=.not.L_eril
C FDA20_SP1.fgi( 181, 612):RS �������
      L_aril=L_eril
C FDA20_SP1.fgi( 197, 642):������,20FDA21AE702VP01_B
      L_upil=L_eril
C FDA20_SP1.fgi( 197, 638):������,20FDA21AE702VP02_B
      L_opil=L_eril
C FDA20_SP1.fgi( 197, 634):������,20FDA21AE702VP03_B
      L_ipil=L_eril
C FDA20_SP1.fgi( 197, 630):������,20FDA21AE702VP04_B
      L_epil=L_eril
C FDA20_SP1.fgi( 197, 626):������,20FDA21AE702VP05_B
      L_apil=L_eril
C FDA20_SP1.fgi( 197, 622):������,20FDA21AE702VP06_B
      L_umil=L_eril
C FDA20_SP1.fgi( 197, 618):������,20FDA21AE702VP07_B
      L_omil=L_eril
C FDA20_SP1.fgi( 197, 614):������,20FDA21AE702VP08_B
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubip,L_idip,R_edip,
     & REAL(R_odip,4),L_udip,L_obip,I_adip)
      !}
C FDA20_lamp.fgi( 228, 384):���������� ������� ���������,20FDA21AE702QB86
      L_iril=L_oril
C FDA20_SP1.fgi( 140, 636):������,move_to_kt2
      if(L_(486).and..not.L0_ilak) then
         R0_alak=R0_elak
      else
         R0_alak=max(R_(187)-deltat,0.0)
      endif
      L_olak=R0_alak.gt.0.0
      L0_ilak=L_(486)
C FDA20_SP1.fgi( 118, 653):������������  �� T
      L_asil=L_esil
C FDA20_SP1.fgi( 140, 669):������,move_to_bvp
      if(L_ikol.and..not.L0_ifol) then
         R0_afol=R0_efol
      else
         R0_afol=max(R_(275)-deltat,0.0)
      endif
      L_ekol=R0_afol.gt.0.0
      L0_ifol=L_ikol
C FDA20_SP1.fgi( 118, 680):������������  �� T
      if(L_obal.and..not.L0_abal) then
         R0_oxuk=R0_uxuk
      else
         R0_oxuk=max(R_(245)-deltat,0.0)
      endif
      L_ebal=R0_oxuk.gt.0.0
      L0_abal=L_obal
C FDA20_SP1.fgi( 118, 691):������������  �� T
      L_ovil=L_axil
C FDA20_SP1.fgi( 197, 713):������,move_to_tel
      if(L_afal.and..not.L0_idal) then
         R0_adal=R0_edal
      else
         R0_adal=max(R_(246)-deltat,0.0)
      endif
      L_odal=R0_adal.gt.0.0
      L0_idal=L_afal
C FDA20_SP1.fgi( 118, 724):������������  �� T
      if(L_ukol.and..not.L0_akol) then
         R0_ofol=R0_ufol
      else
         R0_ofol=max(R_(276)-deltat,0.0)
      endif
      L_okol=R0_ofol.gt.0.0
      L0_akol=L_ukol
C FDA20_SP1.fgi( 118, 735):������������  �� T
      if(L_uxil.and..not.L0_opuk) then
         R0_epuk=R0_ipuk
      else
         R0_epuk=max(R_(238)-deltat,0.0)
      endif
      L_(401)=R0_epuk.gt.0.0
      L0_opuk=L_uxil
C FDA20_SP1.fgi( 215, 764):������������  �� T
      L_(411) = L_(401).AND.L_ulak
C FDA20_SP1.fgi( 227, 763):�
      L0_ubal=(L_(411).or.L0_ubal).and..not.(L_(412))
      L_(413)=.not.L0_ubal
C FDA20_SP1.fgi( 240, 709):RS �������
      if(L0_ubal) then
         R0_uruf=0.0
      elseif(L0_asuf) then
         R0_uruf=R0_oruf
      else
         R0_uruf=max(R_(175)-deltat,0.0)
      endif
      L_utir=L0_ubal.or.R0_uruf.gt.0.0
      L0_asuf=L0_ubal
C FDA20_SP1.fgi( 252, 711):�������� ������� ������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usir,L_itir,R_etir,
     & REAL(R_otir,4),L_utir,L_osir,I_atir)
      !}
C FDA20_lamp.fgi(  82, 782):���������� ������� ���������,20FDA21CG88
      if(.NOT.L_utir.and..not.L0_iruf) then
         R0_aruf=R0_eruf
      else
         R0_aruf=max(R_(174)-deltat,0.0)
      endif
      L_(306)=R0_aruf.gt.0.0
      L0_iruf=.NOT.L_utir
C FDA20_SP1.fgi( 422, 566):������������  �� T
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osur,L_etur,R_atur,
     & REAL(R_itur,4),L_ifas,L_isur,I_usur)
      !}
C FDA20_lamp.fgi(  83, 843):���������� ������� ���������,20FDA21AB002BQ04
      !{
      Call DAT_DISCR_HANDLER(deltat,I_evur,L_uvur,R_ovur,
     & REAL(R_axur,4),L_ifas,L_avur,I_ivur)
      !}
C FDA20_lamp.fgi(  83, 854):���������� ������� ���������,20FDA21AB002BQ03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxur,L_ibas,R_ebas,
     & REAL(R_obas,4),L_ifas,L_oxur,I_abas)
      !}
C FDA20_lamp.fgi(  83, 865):���������� ������� ���������,20FDA21AB002BQ02
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idas,L_afas,R_udas,
     & REAL(R_efas,4),L_ifas,L_edas,I_odas)
      !}
C FDA20_lamp.fgi(  83, 876):���������� ������� ���������,20FDA21AB002BQ01
      if(.not.L_ibel) then
         R0_abel=0.0
      elseif(.not.L0_ebel) then
         R0_abel=R0_uxal
      else
         R0_abel=max(R_(255)-deltat,0.0)
      endif
      L_(434)=L_ibel.and.R0_abel.le.0.0
      L0_ebel=L_ibel
C FDA20_SP1.fgi(  57, 394):�������� ��������� ������
C label 1040  try1040=try1040-1
      if(L_ibel.and..not.L0_ixal) then
         R0_axal=R0_exal
      else
         R0_axal=max(R_(254)-deltat,0.0)
      endif
      L_(431)=R0_axal.gt.0.0
      L0_ixal=L_ibel
C FDA20_SP1.fgi( 158, 388):������������  �� T
      L_oxal=(L_(431).or.L_oxal).and..not.(L_elol)
      L_(432)=.not.L_oxal
C FDA20_SP1.fgi( 170, 386):RS �������
      L_edel=L_oxal
C FDA20_SP1.fgi( 193, 388):������,containerY3set
      if (L_(442)) then
          I_olol = 26
          L_idel = .true.
      endif
      if (L_edel) then
          L_(439) = .true.
          L_idel = .false.
      elseif (L_(441)) then
          L_(440) = .true.
          L_idel = .false.
      endif
      if (I_olol.ne.26) then
          L_idel = .false.
          L_(440) = .false.
          L_(439) = .false.
      endif
C FDA20_SP1.fgi( 102, 538):��� ������� ��������� � ����������,cset
      if(L_ilol) then
          if (L_(440)) then
              I_olol = 27
              L_adel = .true.
              L_(438) = .false.
          endif
          if (L_uvil) then
              L_(438) = .true.
              L_adel = .false.
          endif
          if (I_olol.ne.27) then
              L_adel = .false.
              L_(438) = .false.
          endif
      else
          L_(438) = .false.
      endif
C FDA20_SP1.fgi(  94, 422):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(438)) then
              I_olol = 28
              L_isil = .true.
              L_(437) = .false.
          endif
          if (L_uril) then
              L_(437) = .true.
              L_isil = .false.
          endif
          if (I_olol.ne.28) then
              L_isil = .false.
              L_(437) = .false.
          endif
      else
          L_(437) = .false.
      endif
C FDA20_SP1.fgi(  94, 411):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(437)) then
              I_olol = 29
              L_ibel = .true.
              L_(433) = .false.
          endif
          if (L_(434)) then
              L_(433) = .true.
              L_ibel = .false.
          endif
          if (I_olol.ne.29) then
              L_ibel = .false.
              L_(433) = .false.
          endif
      else
          L_(433) = .false.
      endif
C FDA20_SP1.fgi(  94, 394):��� ������� ���������,cset
C sav1=L_(434)
      if(.not.L_ibel) then
         R0_abel=0.0
      elseif(.not.L0_ebel) then
         R0_abel=R0_uxal
      else
         R0_abel=max(R_(255)-deltat,0.0)
      endif
      L_(434)=L_ibel.and.R0_abel.le.0.0
      L0_ebel=L_ibel
C FDA20_SP1.fgi(  57, 394):recalc:�������� ��������� ������
C if(sav1.ne.L_(434) .and. try1040.gt.0) goto 1040
C sav1=L_(431)
      if(L_ibel.and..not.L0_ixal) then
         R0_axal=R0_exal
      else
         R0_axal=max(R_(254)-deltat,0.0)
      endif
      L_(431)=R0_axal.gt.0.0
      L0_ixal=L_ibel
C FDA20_SP1.fgi( 158, 388):recalc:������������  �� T
C if(sav1.ne.L_(431) .and. try1042.gt.0) goto 1042
      if(L_ilol) then
          if (L_(433)) then
              I_olol = 30
              L_osal = .true.
              L_(430) = .false.
          endif
          if (L_uvil) then
              L_(430) = .true.
              L_osal = .false.
          endif
          if (I_olol.ne.30) then
              L_osal = .false.
              L_(430) = .false.
          endif
      else
          L_(430) = .false.
      endif
C FDA20_SP1.fgi(  94, 366):��� ������� ���������,cset
      L_isal=L_osal
C FDA20_SP1.fgi( 140, 366):������,move_to_ush1
      if(L_osal.and..not.L0_imuk) then
         R0_amuk=R0_emuk
      else
         R0_amuk=max(R_(236)-deltat,0.0)
      endif
      L_(400)=R0_amuk.gt.0.0
      L0_imuk=L_osal
C FDA20_SP1.fgi( 225, 360):������������  �� T
      L_(408) = L_esek.AND.L_(400)
C FDA20_SP1.fgi( 235, 361):�
      L_ixuk=(L_(408).or.L_ixuk).and..not.(L_(409))
      L_(410)=.not.L_ixuk
C FDA20_SP1.fgi( 246, 359):RS �������
      L_exuk=L_ixuk
C FDA20_SP1.fgi( 277, 361):������,pu_cont_pres
      if(.NOT.L_exuk.and..not.L0_upuf) then
         R0_ipuf=R0_opuf
      else
         R0_ipuf=max(R_(173)-deltat,0.0)
      endif
      L_(299)=R0_ipuf.gt.0.0
      L0_upuf=.NOT.L_exuk
C FDA20_SP1.fgi( 422, 560):������������  �� T
      L_olam=L_ibel
C FDA20_SP1.fgi( 173, 394):������,cyl16
      !{
      Call DAT_DISCR_HANDLER(deltat,I_okam,L_elam,R_alam,
     & REAL(R_ilam,4),L_olam,L_ikam,I_ukam)
      !}
C FDA20_lamp.fgi( 290, 158):���������� ������� ���������,20FDA21AL003KE16BQ31
      L_(435) = L_isil.AND.L_efil
C FDA20_SP1.fgi( 165, 405):�
      L_obel=(L_(435).or.L_obel).and..not.(L_elol)
      L_(436)=.not.L_obel
C FDA20_SP1.fgi( 181, 403):RS �������
      L_ekem=L_obel
C FDA20_SP1.fgi( 197, 413):������,cyl13
      L_(503) = (.NOT.L_ekem)
C FDA20_lamp.fgi( 223, 210):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alem,L_olem,R_ilem,
     & REAL(R_ulem,4),L_(503),L_ukem,I_elem)
      !}
C FDA20_lamp.fgi( 236, 208):���������� ������� ���������,20FDA21AL003BQ53
      !{
      Call DAT_DISCR_HANDLER(deltat,I_efem,L_ufem,R_ofem,
     & REAL(R_akem,4),L_ekem,L_afem,I_ifem)
      !}
C FDA20_lamp.fgi( 236, 222):���������� ������� ���������,20FDA21AL003BQ54
      L_opem=L_obel
C FDA20_SP1.fgi( 197, 409):������,cyl14
      L_(504) = (.NOT.L_opem)
C FDA20_lamp.fgi( 276, 210):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irem,L_asem,R_urem,
     & REAL(R_esem,4),L_(504),L_erem,I_orem)
      !}
C FDA20_lamp.fgi( 290, 208):���������� ������� ���������,20FDA21AL003BQ51
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omem,L_epem,R_apem,
     & REAL(R_ipem,4),L_opem,L_imem,I_umem)
      !}
C FDA20_lamp.fgi( 290, 222):���������� ������� ���������,20FDA21AL003BQ52
      L_esam=L_obel
C FDA20_SP1.fgi( 197, 405):������,cyl15
      L_(502) = (.NOT.L_esam)
C FDA20_lamp.fgi( 276, 178):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_atam,L_otam,R_itam,
     & REAL(R_utam,4),L_(502),L_usam,I_etam)
      !}
C FDA20_lamp.fgi( 290, 176):���������� ������� ���������,20FDA21AL003BQ49
      !{
      Call DAT_DISCR_HANDLER(deltat,I_eram,L_uram,R_oram,
     & REAL(R_asam,4),L_esam,L_aram,I_iram)
      !}
C FDA20_lamp.fgi( 290, 190):���������� ������� ���������,20FDA21AL003BQ50
      L_ubel=L_adel
C FDA20_SP1.fgi( 140, 422):������,move_to_y3
      if(L_adel.and..not.L0_etal) then
         R0_usal=R0_atal
      else
         R0_usal=max(R_(251)-deltat,0.0)
      endif
      L_ital=R0_usal.gt.0.0
      L0_etal=L_adel
C FDA20_SP1.fgi( 125, 426):������������  �� T
      L_(405)=R8_ikas.gt.R_(196)
C FDA20_SP1.fgi(  53, 348):���������� >
C label 1124  try1124=try1124-1
      L_(407) = L_ovabe.AND.L_ibabe.AND.L_(405)
C FDA20_SP1.fgi(  69, 355):�
      if(L_ilol) then
          if (L_(430)) then
              I_olol = 31
              L_axuk = .true.
              L_(406) = .false.
          endif
          if (L_(407)) then
              L_(406) = .true.
              L_axuk = .false.
          endif
          if (I_olol.ne.31) then
              L_axuk = .false.
              L_(406) = .false.
          endif
      else
          L_(406) = .false.
      endif
C FDA20_SP1.fgi(  94, 355):��� ������� ���������,cset
      if(L_axuk.and..not.L0_uvuk) then
         R0_ivuk=R0_ovuk
      else
         R0_ivuk=max(R_(244)-deltat,0.0)
      endif
      L_evabe=R0_ivuk.gt.0.0
      L0_uvuk=L_axuk
C FDA20_SP1.fgi( 118, 355):������������  �� T
      L_ababe=L_evabe
C FDA20_SP1.fgi( 197, 351):������,20FDA21AA202YA21
      L_uxux=L_ivabe
C FDA20_SP1.fgi( 197, 336):������,20FDA21AA202YA22
      !{
      Call KLAPAN_VNB_MAN(deltat,REAL(R_olabe,4),R8_ofabe
     &,I_arabe,I_orabe,I_upabe,
     & C8_ekabe,I_irabe,R_alabe,R_ukabe,R_obabe,
     & REAL(R_adabe,4),R_udabe,REAL(R_efabe,4),
     & R_edabe,REAL(R_odabe,4),I_ipabe,I_urabe,I_erabe,I_epabe
     &,L_ifabe,
     & L_esabe,L_etabe,L_afabe,L_idabe,
     & L_akabe,L_ufabe,L_osabe,L_ubabe,L_okabe,L_utabe,L_elabe
     &,
     & L_ilabe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_uxux
     &,L_ababe,L_isabe,
     & I_asabe,L_itabe,R_apabe,REAL(R_ikabe,4),L_otabe,L_ebabe
     &,L_avabe,L_ibabe,
     & L_ulabe,L_amabe,L_emabe,L_omabe,L_umabe,L_imabe)
      !}

      if(L_umabe.or.L_omabe.or.L_imabe.or.L_emabe.or.L_amabe.or.L_ulabe
     &) then      
                  I_opabe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opabe = z'40000000'
      endif
C FDA20_vlv.fgi( 229, 149):���� ���������� �������� VNB,20FDA21AA202
      L_(402)=R8_ikas.gt.R_(196)
C FDA20_SP1.fgi(  53, 333):���������� >
      L_(403) = L_uvabe.AND.L_ebabe.AND.L_(402)
C FDA20_SP1.fgi(  60, 340):�
      if(.not.L_(403)) then
         R0_etuk=0.0
      elseif(.not.L0_ituk) then
         R0_etuk=R0_atuk
      else
         R0_etuk=max(R_(242)-deltat,0.0)
      endif
      L_(404)=L_(403).and.R0_etuk.le.0.0
      L0_ituk=L_(403)
C FDA20_SP1.fgi(  68, 340):�������� ��������� ������
      if(L_ilol) then
          if (L_(406)) then
              I_olol = 32
              L_evuk = .true.
              L_(423) = .false.
          endif
          if (L_(404)) then
              L_(423) = .true.
              L_evuk = .false.
          endif
          if (I_olol.ne.32) then
              L_evuk = .false.
              L_(423) = .false.
          endif
      else
          L_(423) = .false.
      endif
C FDA20_SP1.fgi(  94, 340):��� ������� ���������,cset
      if(L_evuk.and..not.L0_avuk) then
         R0_otuk=R0_utuk
      else
         R0_otuk=max(R_(243)-deltat,0.0)
      endif
      L_ivabe=R0_otuk.gt.0.0
      L0_avuk=L_evuk
C FDA20_SP1.fgi( 118, 340):������������  �� T
C sav1=L_uxux
C FDA20_SP1.fgi( 197, 336):recalc:������,20FDA21AA202YA22
C if(sav1.ne.L_uxux .and. try1136.gt.0) goto 1136
      !{
      Call KLAPAN_VNB_MAN(deltat,REAL(R_akebe,4),R8_adebe
     &,I_imebe,I_apebe,I_emebe,
     & C8_odebe,I_umebe,R_ifebe,R_efebe,R_axabe,
     & REAL(R_ixabe,4),R_ebebe,REAL(R_obebe,4),
     & R_oxabe,REAL(R_abebe,4),I_ulebe,I_epebe,I_omebe,I_olebe
     &,L_ubebe,
     & L_opebe,L_orebe,L_ibebe,L_uxabe,
     & L_idebe,L_edebe,L_arebe,L_exabe,L_afebe,L_esebe,L_ofebe
     &,
     & L_ufebe,REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_evabe
     &,L_ivabe,L_upebe,
     & I_ipebe,L_urebe,R_ilebe,REAL(R_udebe,4),L_asebe,L_ovabe
     &,L_isebe,L_uvabe,
     & L_ekebe,L_ikebe,L_okebe,L_alebe,L_elebe,L_ukebe)
      !}

      if(L_elebe.or.L_alebe.or.L_ukebe.or.L_okebe.or.L_ikebe.or.L_ekebe
     &) then      
                  I_amebe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amebe = z'40000000'
      endif
C FDA20_vlv.fgi( 229, 176):���� ���������� �������� VNB,20FDA21AA201
C sav1=L_(403)
      L_(403) = L_uvabe.AND.L_ebabe.AND.L_(402)
C FDA20_SP1.fgi(  60, 340):recalc:�
C if(sav1.ne.L_(403) .and. try1146.gt.0) goto 1146
C sav1=L_(407)
      L_(407) = L_ovabe.AND.L_ibabe.AND.L_(405)
C FDA20_SP1.fgi(  69, 355):recalc:�
C if(sav1.ne.L_(407) .and. try1126.gt.0) goto 1126
      L_(321) = L_ovabe.AND.L_ibabe
C FDA20_SP1.fgi( 236, 240):�
      if(L_(321)) then
         R_(197)=R_(198)
      else
         R_(197)=R_(199)
      endif
C FDA20_SP1.fgi( 241, 240):���� RE IN LO CH7
      R8_usek=R8_usek+deltat/R0_osek*R_(197)
      if(R8_usek.gt.R0_isek) then
         R8_usek=R0_isek
      elseif(R8_usek.lt.R0_atek) then
         R8_usek=R0_atek
      endif
C FDA20_SP1.fgi( 252, 234):����������
      R8_ikas=R8_usek
C FDA20_SP1.fgi( 272, 236):������,20FDA21CP003QP01_P
      R_usov = R8_ikas * R_(279)
C FDA20_vent_log.fgi(  96,  80):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_orov,R_ivov,REAL(1,4)
     &,
     & REAL(R_esov,4),REAL(R_isov,4),
     & REAL(R_irov,4),REAL(R_erov,4),I_evov,
     & REAL(R_atov,4),L_etov,REAL(R_itov,4),L_otov,L_utov
     &,R_osov,
     & REAL(R_asov,4),REAL(R_urov,4),L_avov,REAL(R_usov,4
     &))
      !}
C FDA20_vlv.fgi( 316, 125):���������� �������,20FDA21CP003XQ01
      if(L_ilol) then
          if (L_(423)) then
              I_olol = 33
              L_opal = .true.
              L_(428) = .false.
          endif
          if (L_urup) then
              L_(428) = .true.
              L_opal = .false.
          endif
          if (I_olol.ne.33) then
              L_opal = .false.
              L_(428) = .false.
          endif
      else
          L_(428) = .false.
      endif
C FDA20_SP1.fgi(  94, 327):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(428)) then
              I_olol = 34
              L_esal = .true.
              L_(429) = .false.
          endif
          if (L_apup) then
              L_(429) = .true.
              L_esal = .false.
          endif
          if (I_olol.ne.34) then
              L_esal = .false.
              L_(429) = .false.
          endif
      else
          L_(429) = .false.
      endif
C FDA20_SP1.fgi(  94, 316):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(429)) then
              I_olol = 35
              L_emal = .true.
              L_(426) = .false.
          endif
          if (L_otup) then
              L_(426) = .true.
              L_emal = .false.
          endif
          if (I_olol.ne.35) then
              L_emal = .false.
              L_(426) = .false.
          endif
      else
          L_(426) = .false.
      endif
C FDA20_SP1.fgi(  94, 305):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(426)) then
              I_olol = 36
              L_eral = .true.
              L_(427) = .false.
          endif
          if (L_evil) then
              L_(427) = .true.
              L_eral = .false.
          endif
          if (I_olol.ne.36) then
              L_eral = .false.
              L_(427) = .false.
          endif
      else
          L_(427) = .false.
      endif
C FDA20_SP1.fgi(  94, 294):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(427)) then
              I_olol = 37
              L_usuk = .true.
              L_(424) = .false.
          endif
          if (L_elup) then
              L_(424) = .true.
              L_usuk = .false.
          endif
          if (I_olol.ne.37) then
              L_usuk = .false.
              L_(424) = .false.
          endif
      else
          L_(424) = .false.
      endif
C FDA20_SP1.fgi(  94, 283):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(424)) then
              I_olol = 38
              L_aral = .true.
              L_(425) = .false.
          endif
          if (L_uvil) then
              L_(425) = .true.
              L_aral = .false.
          endif
          if (I_olol.ne.38) then
              L_aral = .false.
              L_(425) = .false.
          endif
      else
          L_(425) = .false.
      endif
C FDA20_SP1.fgi(  94, 272):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(425)) then
              I_olol = 39
              L_elal = .true.
              L_(422) = .false.
          endif
          if (L_uril) then
              L_(422) = .true.
              L_elal = .false.
          endif
          if (I_olol.ne.39) then
              L_elal = .false.
              L_(422) = .false.
          endif
      else
          L_(422) = .false.
      endif
C FDA20_SP1.fgi(  94, 261):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(422)) then
              I_olol = 40
              L_omal = .true.
              L_(421) = .false.
          endif
          if (L_evil) then
              L_(421) = .true.
              L_omal = .false.
          endif
          if (I_olol.ne.40) then
              L_omal = .false.
              L_(421) = .false.
          endif
      else
          L_(421) = .false.
      endif
C FDA20_SP1.fgi(  94, 245):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(421)) then
              I_olol = 41
              L_okal = .true.
              L_(420) = .false.
          endif
          if (L_uvil) then
              L_(420) = .true.
              L_okal = .false.
          endif
          if (I_olol.ne.41) then
              L_okal = .false.
              L_(420) = .false.
          endif
      else
          L_(420) = .false.
      endif
C FDA20_SP1.fgi(  94, 234):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(420)) then
              I_olol = 42
              L_alal = .true.
              L_(419) = .false.
          endif
          if (L_uril) then
              L_(419) = .true.
              L_alal = .false.
          endif
          if (I_olol.ne.42) then
              L_alal = .false.
              L_(419) = .false.
          endif
      else
          L_(419) = .false.
      endif
C FDA20_SP1.fgi(  94, 223):��� ������� ���������,cset
      L_ikal=L_okal
C FDA20_SP1.fgi( 140, 234):������,move_to_kt1
      if(L_(422).and..not.L0_okak) then
         R0_ekak=R0_ikak
      else
         R0_ekak=max(R_(186)-deltat,0.0)
      endif
      L_ukak=R0_ekak.gt.0.0
      L0_okak=L_(422)
C FDA20_SP1.fgi( 118, 253):������������  �� T
      L_upal=L_aral
C FDA20_SP1.fgi( 140, 272):������,move_to_mvp
      if(L_usuk.and..not.L0_osuk) then
         R0_asuk=R0_esuk
      else
         R0_asuk=max(R_(241)-deltat,0.0)
      endif
      L_isuk=R0_asuk.gt.0.0
      L0_osuk=L_usuk
C FDA20_SP1.fgi( 118, 283):������������  �� T
      if(L_emal.and..not.L0_ulal) then
         R0_ilal=R0_olal
      else
         R0_ilal=max(R_(248)-deltat,0.0)
      endif
      L_amal=R0_ilal.gt.0.0
      L0_ulal=L_emal
C FDA20_SP1.fgi( 118, 305):������������  �� T
      if(L_esal.and..not.L0_ural) then
         R0_iral=R0_oral
      else
         R0_iral=max(R_(250)-deltat,0.0)
      endif
      L_asal=R0_iral.gt.0.0
      L0_ural=L_esal
C FDA20_SP1.fgi( 118, 316):������������  �� T
      if(L_opal.and..not.L0_epal) then
         R0_umal=R0_apal
      else
         R0_umal=max(R_(249)-deltat,0.0)
      endif
      L_ipal=R0_umal.gt.0.0
      L0_epal=L_opal
C FDA20_SP1.fgi( 118, 327):������������  �� T
      if(.not.L_abik) then
         R0_oxek=0.0
      elseif(.not.L0_uxek) then
         R0_oxek=R0_ixek
      else
         R0_oxek=max(R_(202)-deltat,0.0)
      endif
      L_(329)=L_abik.and.R0_oxek.le.0.0
      L0_uxek=L_abik
C FDA20_SP1.fgi(  57, 145):�������� ��������� ������
C label 1219  try1219=try1219-1
      if(L_ekik.and..not.L0_akik) then
         R0_ofik=R0_ufik
      else
         R0_ofik=max(R_(206)-deltat,0.0)
      endif
      L_(335)=R0_ofik.gt.0.0
      L0_akik=L_ekik
C FDA20_SP1.fgi( 445, 180):������������  �� T
      L_(374) =.NOT.(L_urer)
C FDA20_SP1.fgi( 393, 360):���
      if(L_uxok.and..not.L0_utok) then
         R0_itok=R0_otok
      else
         R0_itok=max(R_(229)-deltat,0.0)
      endif
      L_(380)=R0_itok.gt.0.0
      L0_utok=L_uxok
C FDA20_SP1.fgi( 445, 404):������������  �� T
      L_(355) =.NOT.(L_itar)
C FDA20_SP1.fgi( 393, 261):���
      L_okuk=L_ibik
C FDA20_SP1.fgi( 140, 212):������,KT1_start
      if(L_okuk.and..not.L0_ikuk) then
         R0_akuk=R0_ekuk
      else
         R0_akuk=max(R_(234)-deltat,0.0)
      endif
      L_(399)=R0_akuk.gt.0.0
      L0_ikuk=L_okuk
C FDA20_SP1.fgi( 392, 486):������������  �� T
      if(L_ebuk.and..not.L0_ixik) then
         R0_axik=R0_exik
      else
         R0_axik=max(R_(217)-deltat,0.0)
      endif
      L_(358)=R0_axik.gt.0.0
      L0_ixik=L_ebuk
C FDA20_SP1.fgi( 392, 503):������������  �� T
      if(L_abuk.and..not.L0_abok) then
         R0_oxik=R0_uxik
      else
         R0_oxik=max(R_(218)-deltat,0.0)
      endif
      L_(359)=R0_oxik.gt.0.0
      L0_abok=L_abuk
C FDA20_SP1.fgi( 392, 497):������������  �� T
      L_(398) = L_(358).OR.L_(359)
C FDA20_SP1.fgi( 401, 502):���
      if (.not.L_ukuk.and.(L_aluk.or.L_(399))) then
          I_eluk = 0
          L_ukuk = .true.
          L_(397) = .true.
      endif
      if (I_eluk .gt. 0) then
         L_(397) = .false.
      endif
      if(L_(398))then
          I_eluk = 0
          L_ukuk = .false.
          L_(397) = .false.
      endif
C FDA20_SP1.fgi( 414, 478):���������� ������� ���������,kt1sp
      if (L_(397)) then
          I_eluk = 13
          L_ibuk = .true.
      endif
      if (L_abuk) then
          L_(383) = .true.
          L_ibuk = .false.
      elseif (L_ebuk) then
          L_(394) = .true.
          L_ibuk = .false.
      endif
      if (I_eluk.ne.13) then
          L_ibuk = .false.
          L_(394) = .false.
          L_(383) = .false.
      endif
C FDA20_SP1.fgi( 414, 448):��� ������� ��������� � ����������,kt1sp
      if(L_ukuk) then
          if (L_(383)) then
              I_eluk = 14
              L_uvik = .true.
              L_(356) = .false.
          endif
          if (L_exar) then
              L_(356) = .true.
              L_uvik = .false.
          endif
          if (I_eluk.ne.14) then
              L_uvik = .false.
              L_(356) = .false.
          endif
      else
          L_(356) = .false.
      endif
C FDA20_SP1.fgi( 418, 272):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(356)) then
              I_eluk = 15
              L_avik = .true.
              L_(354) = .false.
          endif
          if (L_(355)) then
              L_(354) = .true.
              L_avik = .false.
          endif
          if (I_eluk.ne.15) then
              L_avik = .false.
              L_(354) = .false.
          endif
      else
          L_(354) = .false.
      endif
C FDA20_SP1.fgi( 418, 261):��� ������� ���������,kt1sp
      if(L_avik.and..not.L0_utik) then
         R0_itik=R0_otik
      else
         R0_itik=max(R_(215)-deltat,0.0)
      endif
      L_(353)=R0_itik.gt.0.0
      L0_utik=L_avik
C FDA20_SP1.fgi( 445, 261):������������  �� T
      L_(385) = L_(380).OR.L_(353)
C FDA20_SP1.fgi( 489, 403):���
      if(L_afok.and..not.L0_udok) then
         R0_idok=R0_odok
      else
         R0_idok=max(R_(220)-deltat,0.0)
      endif
      L_(364)=R0_idok.gt.0.0
      L0_udok=L_afok
C FDA20_SP1.fgi( 445, 305):������������  �� T
      if(.not.L0_aduk) then
         R0_evok=0.0
      elseif(.not.L0_ivok) then
         R0_evok=R0_avok
      else
         R0_evok=max(R_(230)-deltat,0.0)
      endif
      L_itar=L0_aduk.and.R0_evok.le.0.0
      L0_ivok=L0_aduk
C FDA20_SP1.fgi( 551, 410):�������� ��������� ������
C sav1=L_(355)
      L_(355) =.NOT.(L_itar)
C FDA20_SP1.fgi( 393, 261):recalc:���
C if(sav1.ne.L_(355) .and. try1227.gt.0) goto 1227
      L_(382) =.NOT.(L_itar)
C FDA20_SP1.fgi( 393, 404):���
      if(L_epok.and..not.L0_apok) then
         R0_omok=R0_umok
      else
         R0_omok=max(R_(225)-deltat,0.0)
      endif
      L_(372)=R0_omok.gt.0.0
      L0_apok=L_epok
C FDA20_SP1.fgi( 445, 360):������������  �� T
      if(.not..NOT.L_urer) then
         R0_elik=0.0
      elseif(.not.L0_ilik) then
         R0_elik=R0_alik
      else
         R0_elik=max(R_(208)-deltat,0.0)
      endif
      L_(341)=.NOT.L_urer.and.R0_elik.le.0.0
      L0_ilik=.NOT.L_urer
C FDA20_SP1.fgi( 380, 191):�������� ��������� ������
      if(L_ukuk) then
          if (L_(354)) then
              I_eluk = 16
              L_etik = .true.
              L_(352) = .false.
          endif
          if (L_ixer) then
              L_(352) = .true.
              L_etik = .false.
          endif
          if (I_eluk.ne.16) then
              L_etik = .false.
              L_(352) = .false.
          endif
      else
          L_(352) = .false.
      endif
C FDA20_SP1.fgi( 418, 250):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(352)) then
              I_eluk = 17
              L_isik = .true.
              L_(350) = .false.
          endif
          if (L_obar) then
              L_(350) = .true.
              L_isik = .false.
          endif
          if (I_eluk.ne.17) then
              L_isik = .false.
              L_(350) = .false.
          endif
      else
          L_(350) = .false.
      endif
C FDA20_SP1.fgi( 418, 239):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(350)) then
              I_eluk = 18
              L_orik = .true.
              L_(348) = .false.
          endif
          if (L_elar) then
              L_(348) = .true.
              L_orik = .false.
          endif
          if (I_eluk.ne.18) then
              L_orik = .false.
              L_(348) = .false.
          endif
      else
          L_(348) = .false.
      endif
C FDA20_SP1.fgi( 418, 228):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(348)) then
              I_eluk = 19
              L_upik = .true.
              L_(345) = .false.
          endif
          if (L_urer) then
              L_(345) = .true.
              L_upik = .false.
          endif
          if (I_eluk.ne.19) then
              L_upik = .false.
              L_(345) = .false.
          endif
      else
          L_(345) = .false.
      endif
C FDA20_SP1.fgi( 418, 217):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(345)) then
              I_eluk = 20
              L_apik = .true.
              L_(342) = .false.
          endif
          if (L_akir) then
              L_(342) = .true.
              L_apik = .false.
          endif
          if (I_eluk.ne.20) then
              L_apik = .false.
              L_(342) = .false.
          endif
      else
          L_(342) = .false.
      endif
C FDA20_SP1.fgi( 418, 206):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(342)) then
              I_eluk = 21
              L_emik = .true.
              L_(340) = .false.
          endif
          if (L_(341)) then
              L_(340) = .true.
              L_emik = .false.
          endif
          if (I_eluk.ne.21) then
              L_emik = .false.
              L_(340) = .false.
          endif
      else
          L_(340) = .false.
      endif
C FDA20_SP1.fgi( 418, 191):��� ������� ���������,kt1sp
      if(L_emik.and..not.L0_amik) then
         R0_olik=R0_ulik
      else
         R0_olik=max(R_(209)-deltat,0.0)
      endif
      L_(346)=R0_olik.gt.0.0
      L0_amik=L_emik
C FDA20_SP1.fgi( 462, 191):������������  �� T
      L_(388) = L_(372).OR.L_(346)
C FDA20_SP1.fgi( 489, 359):���
      if(L_upik.and..not.L0_opik) then
         R0_epik=R0_ipik
      else
         R0_epik=max(R_(211)-deltat,0.0)
      endif
      L_(344)=R0_epik.gt.0.0
      L0_opik=L_upik
C FDA20_SP1.fgi( 445, 217):������������  �� T
      if(L_(341).and..not.L0_ukik) then
         R0_ikik=R0_okik
      else
         R0_ikik=max(R_(207)-deltat,0.0)
      endif
      L_(338)=R0_ikik.gt.0.0
      L0_ukik=L_(341)
C FDA20_SP1.fgi( 445, 199):������������  �� T
      L_(339) = L_(338).AND.L_emik
C FDA20_SP1.fgi( 456, 198):�
      if(.not.L0_eduk) then
         R0_uvok=0.0
      elseif(.not.L0_axok) then
         R0_uvok=R0_ovok
      else
         R0_uvok=max(R_(231)-deltat,0.0)
      endif
      L_urer=L0_eduk.and.R0_uvok.le.0.0
      L0_axok=L0_eduk
C FDA20_SP1.fgi( 551, 426):�������� ��������� ������
C sav1=L_(341)
      if(.not..NOT.L_urer) then
         R0_elik=0.0
      elseif(.not.L0_ilik) then
         R0_elik=R0_alik
      else
         R0_elik=max(R_(208)-deltat,0.0)
      endif
      L_(341)=.NOT.L_urer.and.R0_elik.le.0.0
      L0_ilik=.NOT.L_urer
C FDA20_SP1.fgi( 380, 191):recalc:�������� ��������� ������
C if(sav1.ne.L_(341) .and. try1272.gt.0) goto 1272
C sav1=L_upik
C sav2=L_(345)
      if(L_ukuk) then
          if (L_(348)) then
              I_eluk = 19
              L_upik = .true.
              L_(345) = .false.
          endif
          if (L_urer) then
              L_(345) = .true.
              L_upik = .false.
          endif
          if (I_eluk.ne.19) then
              L_upik = .false.
              L_(345) = .false.
          endif
      else
          L_(345) = .false.
      endif
C FDA20_SP1.fgi( 418, 217):recalc:��� ������� ���������,kt1sp
C if(sav1.ne.L_upik .and. try1281.gt.0) goto 1281
C if(sav2.ne.L_(345) .and. try1281.gt.0) goto 1281
C sav1=L_(374)
      L_(374) =.NOT.(L_urer)
C FDA20_SP1.fgi( 393, 360):recalc:���
C if(sav1.ne.L_(374) .and. try1223.gt.0) goto 1223
      L_(396) = L_ifar.AND.L_apar.AND.(.NOT.L_ipir).AND.L_akir.AND.L_ure
     &r.AND.L_ixer.AND.L_exar.AND.L_itar
C FDA20_SP1.fgi( 393, 431):�
      if(L_ukuk) then
          if (L_(394)) then
              I_eluk = 1
              L_ufuk = .true.
              L_(395) = .false.
          endif
          if (L_(396)) then
              L_(395) = .true.
              L_ufuk = .false.
          endif
          if (I_eluk.ne.1) then
              L_ufuk = .false.
              L_(395) = .false.
          endif
      else
          L_(395) = .false.
      endif
C FDA20_SP1.fgi( 414, 436):��� ������� ���������,kt1sp
      if(L_ufuk.and..not.L0_ofuk) then
         R0_efuk=R0_ifuk
      else
         R0_efuk=max(R_(233)-deltat,0.0)
      endif
      L_(393)=R0_efuk.gt.0.0
      L0_ofuk=L_ufuk
C FDA20_SP1.fgi( 445, 436):������������  �� T
      L_(391) = L_(335).OR.L_(393)
C FDA20_SP1.fgi( 489, 437):���
      if(L_ukuk) then
          if (L_(395)) then
              I_eluk = 2
              L_uxok = .true.
              L_(381) = .false.
          endif
          if (L_(382)) then
              L_(381) = .true.
              L_uxok = .false.
          endif
          if (I_eluk.ne.2) then
              L_uxok = .false.
              L_(381) = .false.
          endif
      else
          L_(381) = .false.
      endif
C FDA20_SP1.fgi( 414, 404):��� ������� ���������,kt1sp
C sav1=L_(380)
      if(L_uxok.and..not.L0_utok) then
         R0_itok=R0_otok
      else
         R0_itok=max(R_(229)-deltat,0.0)
      endif
      L_(380)=R0_itok.gt.0.0
      L0_utok=L_uxok
C FDA20_SP1.fgi( 445, 404):recalc:������������  �� T
C if(sav1.ne.L_(380) .and. try1225.gt.0) goto 1225
      if(L_ukuk) then
          if (L_(381)) then
              I_eluk = 3
              L_etok = .true.
              L_(379) = .false.
          endif
          if (L_obar) then
              L_(379) = .true.
              L_etok = .false.
          endif
          if (I_eluk.ne.3) then
              L_etok = .false.
              L_(379) = .false.
          endif
      else
          L_(379) = .false.
      endif
C FDA20_SP1.fgi( 414, 393):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(379)) then
              I_eluk = 4
              L_esok = .true.
              L_(377) = .false.
          endif
          if (L_elar) then
              L_(377) = .true.
              L_esok = .false.
          endif
          if (I_eluk.ne.4) then
              L_esok = .false.
              L_(377) = .false.
          endif
      else
          L_(377) = .false.
      endif
C FDA20_SP1.fgi( 414, 382):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(377)) then
              I_eluk = 5
              L_erok = .true.
              L_(375) = .false.
          endif
          if (L_edir) then
              L_(375) = .true.
              L_erok = .false.
          endif
          if (I_eluk.ne.5) then
              L_erok = .false.
              L_(375) = .false.
          endif
      else
          L_(375) = .false.
      endif
C FDA20_SP1.fgi( 414, 371):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(375)) then
              I_eluk = 6
              L_epok = .true.
              L_(373) = .false.
          endif
          if (L_(374)) then
              L_(373) = .true.
              L_epok = .false.
          endif
          if (I_eluk.ne.6) then
              L_epok = .false.
              L_(373) = .false.
          endif
      else
          L_(373) = .false.
      endif
C FDA20_SP1.fgi( 414, 360):��� ������� ���������,kt1sp
C sav1=L_(372)
      if(L_epok.and..not.L0_apok) then
         R0_omok=R0_umok
      else
         R0_omok=max(R_(225)-deltat,0.0)
      endif
      L_(372)=R0_omok.gt.0.0
      L0_apok=L_epok
C FDA20_SP1.fgi( 445, 360):recalc:������������  �� T
C if(sav1.ne.L_(372) .and. try1270.gt.0) goto 1270
      if(L_ukuk) then
          if (L_(373)) then
              I_eluk = 7
              L_imok = .true.
              L_(371) = .false.
          endif
          if (L_ipir) then
              L_(371) = .true.
              L_imok = .false.
          endif
          if (I_eluk.ne.7) then
              L_imok = .false.
              L_(371) = .false.
          endif
      else
          L_(371) = .false.
      endif
C FDA20_SP1.fgi( 414, 349):��� ������� ���������,kt1sp
      if(L_imok.and..not.L0_emok) then
         R0_ulok=R0_amok
      else
         R0_ulok=max(R_(224)-deltat,0.0)
      endif
      L_(390)=R0_ulok.gt.0.0
      L0_emok=L_imok
C FDA20_SP1.fgi( 445, 349):������������  �� T
      L0_oduk=(L_(390).or.L0_oduk).and..not.(L_(391))
      L_(392)=.not.L0_oduk
C FDA20_SP1.fgi( 535, 439):RS �������
      if(.not.L0_oduk) then
         R0_ixok=0.0
      elseif(.not.L0_oxok) then
         R0_ixok=R0_exok
      else
         R0_ixok=max(R_(232)-deltat,0.0)
      endif
      L_ipir=L0_oduk.and.R0_ixok.le.0.0
      L0_oxok=L0_oduk
C FDA20_SP1.fgi( 551, 441):�������� ��������� ������
C sav1=L_imok
C sav2=L_(371)
      if(L_ukuk) then
          if (L_(373)) then
              I_eluk = 7
              L_imok = .true.
              L_(371) = .false.
          endif
          if (L_ipir) then
              L_(371) = .true.
              L_imok = .false.
          endif
          if (I_eluk.ne.7) then
              L_imok = .false.
              L_(371) = .false.
          endif
      else
          L_(371) = .false.
      endif
C FDA20_SP1.fgi( 414, 349):recalc:��� ������� ���������,kt1sp
C if(sav1.ne.L_imok .and. try1340.gt.0) goto 1340
C if(sav2.ne.L_(371) .and. try1340.gt.0) goto 1340
C sav1=L_(396)
      L_(396) = L_ifar.AND.L_apar.AND.(.NOT.L_ipir).AND.L_akir.AND.L_ure
     &r.AND.L_ixer.AND.L_exar.AND.L_itar
C FDA20_SP1.fgi( 393, 431):recalc:�
C if(sav1.ne.L_(396) .and. try1313.gt.0) goto 1313
      L_(337) =.NOT.(L_ipir)
C FDA20_SP1.fgi( 393, 180):���
      if(L_ukuk) then
          if (L_(340)) then
              I_eluk = 22
              L_ekik = .true.
              L_(336) = .false.
          endif
          if (L_(337)) then
              L_(336) = .true.
              L_ekik = .false.
          endif
          if (I_eluk.ne.22) then
              L_ekik = .false.
              L_(336) = .false.
          endif
      else
          L_(336) = .false.
      endif
C FDA20_SP1.fgi( 418, 180):��� ������� ���������,kt1sp
C sav1=L_(335)
      if(L_ekik.and..not.L0_akik) then
         R0_ofik=R0_ufik
      else
         R0_ofik=max(R_(206)-deltat,0.0)
      endif
      L_(335)=R0_ofik.gt.0.0
      L0_akik=L_ekik
C FDA20_SP1.fgi( 445, 180):recalc:������������  �� T
C if(sav1.ne.L_(335) .and. try1221.gt.0) goto 1221
      if(L_ukuk) then
          if (L_(336)) then
              I_eluk = 23
              L_ifik = .true.
              L_(334) = .false.
          endif
          if (L_ifar) then
              L_(334) = .true.
              L_ifik = .false.
          endif
          if (I_eluk.ne.23) then
              L_ifik = .false.
              L_(334) = .false.
          endif
      else
          L_(334) = .false.
      endif
C FDA20_SP1.fgi( 418, 169):��� ������� ���������,kt1sp
      if(L_(334).and..not.L0_adik) then
         R0_obik=R0_ubik
      else
         R0_obik=max(R_(203)-deltat,0.0)
      endif
      L_(361)=R0_obik.gt.0.0
      L0_adik=L_(334)
C FDA20_SP1.fgi( 445, 162):������������  �� T
      if(L_ukuk) then
          if (L_(371)) then
              I_eluk = 8
              L_olok = .true.
              L_(369) = .false.
          endif
          if (L_apar) then
              L_(369) = .true.
              L_olok = .false.
          endif
          if (I_eluk.ne.8) then
              L_olok = .false.
              L_(369) = .false.
          endif
      else
          L_(369) = .false.
      endif
C FDA20_SP1.fgi( 414, 338):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(369)) then
              I_eluk = 9
              L_ukok = .true.
              L_(366) = .false.
          endif
          if (L_ifar) then
              L_(366) = .true.
              L_ukok = .false.
          endif
          if (I_eluk.ne.9) then
              L_ukok = .false.
              L_(366) = .false.
          endif
      else
          L_(366) = .false.
      endif
C FDA20_SP1.fgi( 414, 327):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(366)) then
              I_eluk = 10
              L_akok = .true.
              L_(365) = .false.
          endif
          if (L_oter) then
              L_(365) = .true.
              L_akok = .false.
          endif
          if (I_eluk.ne.10) then
              L_akok = .false.
              L_(365) = .false.
          endif
      else
          L_(365) = .false.
      endif
C FDA20_SP1.fgi( 414, 316):��� ������� ���������,kt1sp
      if(L_ukuk) then
          if (L_(365)) then
              I_eluk = 11
              L_afok = .true.
              L_(363) = .false.
          endif
          if (L_itar) then
              L_(363) = .true.
              L_afok = .false.
          endif
          if (I_eluk.ne.11) then
              L_afok = .false.
              L_(363) = .false.
          endif
      else
          L_(363) = .false.
      endif
C FDA20_SP1.fgi( 414, 305):��� ������� ���������,kt1sp
C sav1=L_(364)
      if(L_afok.and..not.L0_udok) then
         R0_idok=R0_odok
      else
         R0_idok=max(R_(220)-deltat,0.0)
      endif
      L_(364)=R0_idok.gt.0.0
      L0_udok=L_afok
C FDA20_SP1.fgi( 445, 305):recalc:������������  �� T
C if(sav1.ne.L_(364) .and. try1252.gt.0) goto 1252
      if(L_ukuk) then
          if (L_(363)) then
              I_eluk = 12
              L_edok = .true.
              L_(362) = .false.
          endif
          if (L_iler) then
              L_(362) = .true.
              L_edok = .false.
          endif
          if (I_eluk.ne.12) then
              L_edok = .false.
              L_(362) = .false.
          endif
      else
          L_(362) = .false.
      endif
C FDA20_SP1.fgi( 414, 294):��� ������� ���������,kt1sp
      if(L_(362).and..not.L0_odik) then
         R0_edik=R0_idik
      else
         R0_edik=max(R_(204)-deltat,0.0)
      endif
      L_(360)=R0_edik.gt.0.0
      L0_odik=L_(362)
C FDA20_SP1.fgi( 445, 287):������������  �� T
      L_ebok=(L_(360).or.L_ebok).and..not.(L_(361))
      L_ebuk=.not.L_ebok
C FDA20_SP1.fgi( 528, 285):RS �������
C sav1=L_(358)
      if(L_ebuk.and..not.L0_ixik) then
         R0_axik=R0_exik
      else
         R0_axik=max(R_(217)-deltat,0.0)
      endif
      L_(358)=R0_axik.gt.0.0
      L0_ixik=L_ebuk
C FDA20_SP1.fgi( 392, 503):recalc:������������  �� T
C if(sav1.ne.L_(358) .and. try1234.gt.0) goto 1234
      L_abuk=L_ebok
C FDA20_SP1.fgi( 548, 287):������,from_y4
C sav1=L_ibuk
C sav2=L_(394)
C sav3=L_(383)
      if (L_(397)) then
          I_eluk = 13
          L_ibuk = .true.
      endif
      if (L_abuk) then
          L_(383) = .true.
          L_ibuk = .false.
      elseif (L_ebuk) then
          L_(394) = .true.
          L_ibuk = .false.
      endif
      if (I_eluk.ne.13) then
          L_ibuk = .false.
          L_(394) = .false.
          L_(383) = .false.
      endif
C FDA20_SP1.fgi( 414, 448):recalc:��� ������� ��������� � ����������,kt1sp
C if(sav1.ne.L_ibuk .and. try1242.gt.0) goto 1242
C if(sav2.ne.L_(394) .and. try1242.gt.0) goto 1242
C if(sav3.ne.L_(383) .and. try1242.gt.0) goto 1242
C sav1=L_(359)
      if(L_abuk.and..not.L0_abok) then
         R0_oxik=R0_uxik
      else
         R0_oxik=max(R_(218)-deltat,0.0)
      endif
      L_(359)=R0_oxik.gt.0.0
      L0_abok=L_abuk
C FDA20_SP1.fgi( 392, 497):recalc:������������  �� T
C if(sav1.ne.L_(359) .and. try1236.gt.0) goto 1236
      if(L_ilol) then
          if (L_(419)) then
              I_olol = 43
              L_ibik = .true.
              L_(332) = .false.
          endif
          if (L_abuk) then
              L_(332) = .true.
              L_ibik = .false.
          endif
          if (I_olol.ne.43) then
              L_ibik = .false.
              L_(332) = .false.
          endif
      else
          L_(332) = .false.
      endif
C FDA20_SP1.fgi(  94, 212):��� ������� ���������,cset
C sav1=L_okuk
C FDA20_SP1.fgi( 140, 212):recalc:������,KT1_start
C if(sav1.ne.L_okuk .and. try1229.gt.0) goto 1229
      if(L_ilol) then
          if (L_(332)) then
              I_olol = 44
              L_imal = .true.
              L_(324) = .false.
          endif
          if (L_evil) then
              L_(324) = .true.
              L_imal = .false.
          endif
          if (I_olol.ne.44) then
              L_imal = .false.
              L_(324) = .false.
          endif
      else
          L_(324) = .false.
      endif
C FDA20_SP1.fgi(  94, 201):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(324)) then
              I_olol = 45
              L_evek = .true.
              L_(323) = .false.
          endif
          if (L_uvil) then
              L_(323) = .true.
              L_evek = .false.
          endif
          if (I_olol.ne.45) then
              L_evek = .false.
              L_(323) = .false.
          endif
      else
          L_(323) = .false.
      endif
C FDA20_SP1.fgi(  94, 190):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(323)) then
              I_olol = 46
              L_ukal = .true.
              L_(327) = .false.
          endif
          if (L_uril) then
              L_(327) = .true.
              L_ukal = .false.
          endif
          if (I_olol.ne.46) then
              L_ukal = .false.
              L_(327) = .false.
          endif
      else
          L_(327) = .false.
      endif
C FDA20_SP1.fgi(  94, 179):��� ������� ���������,cset
      if(L_ilol) then
          if (L_(327)) then
              I_olol = 47
              L_abik = .true.
              L_(328) = .false.
          endif
          if (L_(329)) then
              L_(328) = .true.
              L_abik = .false.
          endif
          if (I_olol.ne.47) then
              L_abik = .false.
              L_(328) = .false.
          endif
      else
          L_(328) = .false.
      endif
C FDA20_SP1.fgi(  94, 145):��� ������� ���������,cset
C sav1=L_(329)
      if(.not.L_abik) then
         R0_oxek=0.0
      elseif(.not.L0_uxek) then
         R0_oxek=R0_ixek
      else
         R0_oxek=max(R_(202)-deltat,0.0)
      endif
      L_(329)=L_abik.and.R0_oxek.le.0.0
      L0_uxek=L_abik
C FDA20_SP1.fgi(  57, 145):recalc:�������� ��������� ������
C if(sav1.ne.L_(329) .and. try1219.gt.0) goto 1219
      L_(384) = L_(393).OR.L_(364)
C FDA20_SP1.fgi( 489, 410):���
      L0_aduk=(L_(384).or.L0_aduk).and..not.(L_(385))
      L_(386)=.not.L0_aduk
C FDA20_SP1.fgi( 535, 408):RS �������
C sav1=L_itar
      if(.not.L0_aduk) then
         R0_evok=0.0
      elseif(.not.L0_ivok) then
         R0_evok=R0_avok
      else
         R0_evok=max(R_(230)-deltat,0.0)
      endif
      L_itar=L0_aduk.and.R0_evok.le.0.0
      L0_ivok=L0_aduk
C FDA20_SP1.fgi( 551, 410):recalc:�������� ��������� ������
C if(sav1.ne.L_itar .and. try1254.gt.0) goto 1254
      L_(387) = L_(393).OR.L_(344).OR.L_(339)
C FDA20_SP1.fgi( 489, 426):���
      L0_eduk=(L_(387).or.L0_eduk).and..not.(L_(388))
      L_(389)=.not.L0_eduk
C FDA20_SP1.fgi( 535, 424):RS �������
C sav1=L_urer
      if(.not.L0_eduk) then
         R0_uvok=0.0
      elseif(.not.L0_axok) then
         R0_uvok=R0_ovok
      else
         R0_uvok=max(R_(231)-deltat,0.0)
      endif
      L_urer=L0_eduk.and.R0_uvok.le.0.0
      L0_axok=L0_eduk
C FDA20_SP1.fgi( 551, 426):recalc:�������� ��������� ������
C if(sav1.ne.L_urer .and. try1299.gt.0) goto 1299
      L_(418) = L_(308).OR.L_(328)
C FDA20_SP1.fgi( 103, 129):���
      if(L_ilol) then
          if (L_(418)) then
              I_olol = 48
              L_ekal = .true.
              L_(501) = .false.
          endif
          if (L_uvil) then
              L_(501) = .true.
              L_ekal = .false.
          endif
          if (I_olol.ne.48) then
              L_ekal = .false.
              L_(501) = .false.
          endif
      else
          L_(501) = .false.
      endif
C FDA20_SP1.fgi(  95, 120):��� ������� ���������,cset
      if(L_(501)) then
         I_olol=0
         L_ilol=.false.
      endif
C FDA20_SP1.fgi(  95, 109):����� ������� ���������,cset
      L_abol = L_ekal.OR.L_uxil
C FDA20_SP1.fgi( 181, 770):���
      if(L_abol.and..not.L0_utel) then
         R0_itel=R0_otel
      else
         R0_itel=max(R_(264)-deltat,0.0)
      endif
      L_(462)=R0_itel.gt.0.0
      L0_utel=L_abol
C FDA20_SP1.fgi( 541, 748):������������  �� T
      L_ibop=L_abik
C FDA20_SP1.fgi( 173, 145):������,vint_p2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ixip,L_abop,R_uxip,
     & REAL(R_ebop,4),L_ibop,L_exip,I_oxip)
      !}
C FDA20_lamp.fgi( 122, 434):���������� ������� ���������,20FDA21AL004KF02Q41
      if(L_abik.and..not.L0_axek) then
         R0_ovek=R0_uvek
      else
         R0_ovek=max(R_(201)-deltat,0.0)
      endif
      L_(325)=R0_ovek.gt.0.0
      L0_axek=L_abik
C FDA20_SP1.fgi( 158, 139):������������  �� T
      L_exek=(L_(325).or.L_exek).and..not.(L_elol)
      L_(326)=.not.L_exek
C FDA20_SP1.fgi( 170, 137):RS �������
      L_ivek=L_exek
C FDA20_SP1.fgi( 193, 139):������,containerY4set
      L_usak =.NOT.(L_umel.OR.L_udel.OR.L_edel.OR.L_ivek)
C FDA20_SP1.fgi( 428, 610):���
      L_(307) = L_usak.AND.L_(306)
C FDA20_SP1.fgi( 440, 579):�
      if(L_(307).and..not.L0_ivuf) then
         R0_avuf=R0_evuf
      else
         R0_avuf=max(R_(179)-deltat,0.0)
      endif
      L_irak=R0_avuf.gt.0.0
      L0_ivuf=L_(307)
C FDA20_SP1.fgi( 465, 550):������������  �� T
      L_(318) = L_irak.AND.L_usak
C FDA20_SP1.fgi( 414, 781):�
      if(.not.L_(307)) then
         R0_obak=0.0
      elseif(.not.L0_ubak) then
         R0_obak=R0_ibak
      else
         R0_obak=max(R_(183)-deltat,0.0)
      endif
      L_(305)=L_(307).and.R0_obak.le.0.0
      L0_ubak=L_(307)
C FDA20_SP1.fgi( 456, 579):�������� ��������� ������
      if(L_(305).and..not.L0_erak) then
         R0_upak=R0_arak
      else
         R0_upak=max(R_(191)-deltat,0.0)
      endif
      L_epek=R0_upak.gt.0.0
      L0_erak=L_(305)
C FDA20_SP1.fgi( 465, 579):������������  �� T
      L_osak = L_umel.AND.(.NOT.L_udel).AND.(.NOT.L_edel).AND.
     &(.NOT.L_ivek)
C FDA20_SP1.fgi( 428, 602):�
      L_(304) = L_osak.AND.L_(306)
C FDA20_SP1.fgi( 440, 573):�
      if(L_(304).and..not.L0_utuf) then
         R0_ituf=R0_otuf
      else
         R0_ituf=max(R_(178)-deltat,0.0)
      endif
      L_orak=R0_ituf.gt.0.0
      L0_utuf=L_(304)
C FDA20_SP1.fgi( 465, 544):������������  �� T
      L_(315) = L_orak.AND.L_osak
C FDA20_SP1.fgi( 414, 752):�
      if(.not.L_(304)) then
         R0_abak=0.0
      elseif(.not.L0_ebak) then
         R0_abak=R0_uxuf
      else
         R0_abak=max(R_(182)-deltat,0.0)
      endif
      L_(303)=L_(304).and.R0_abak.le.0.0
      L0_ebak=L_(304)
C FDA20_SP1.fgi( 456, 573):�������� ��������� ������
      if(L_(303).and..not.L0_opak) then
         R0_epak=R0_ipak
      else
         R0_epak=max(R_(190)-deltat,0.0)
      endif
      L_apek=R0_epak.gt.0.0
      L0_opak=L_(303)
C FDA20_SP1.fgi( 465, 573):������������  �� T
      L_isak = L_umel.AND.L_udel.AND.(.NOT.L_edel).AND.(.NOT.L_ivek
     &)
C FDA20_SP1.fgi( 428, 594):�
      L_(302) = L_isak.AND.L_(306)
C FDA20_SP1.fgi( 440, 567):�
      if(L_(302).and..not.L0_etuf) then
         R0_usuf=R0_atuf
      else
         R0_usuf=max(R_(177)-deltat,0.0)
      endif
      L_urak=R0_usuf.gt.0.0
      L0_etuf=L_(302)
C FDA20_SP1.fgi( 465, 538):������������  �� T
      L_(312) = L_urak.AND.L_isak
C FDA20_SP1.fgi( 414, 723):�
      if(.not.L_(302)) then
         R0_ixuf=0.0
      elseif(.not.L0_oxuf) then
         R0_ixuf=R0_exuf
      else
         R0_ixuf=max(R_(181)-deltat,0.0)
      endif
      L_(301)=L_(302).and.R0_ixuf.le.0.0
      L0_oxuf=L_(302)
C FDA20_SP1.fgi( 456, 567):�������� ��������� ������
      if(L_(301).and..not.L0_apak) then
         R0_omak=R0_umak
      else
         R0_omak=max(R_(189)-deltat,0.0)
      endif
      L_ipek=R0_omak.gt.0.0
      L0_apak=L_(301)
C FDA20_SP1.fgi( 465, 567):������������  �� T
      L_esak = L_umel.AND.L_udel.AND.L_edel.AND.(.NOT.L_ivek
     &)
C FDA20_SP1.fgi( 428, 586):�
      L_(300) = L_esak.AND.L_(299)
C FDA20_SP1.fgi( 440, 561):�
      if(L_(300).and..not.L0_osuf) then
         R0_esuf=R0_isuf
      else
         R0_esuf=max(R_(176)-deltat,0.0)
      endif
      L_asak=R0_esuf.gt.0.0
      L0_osuf=L_(300)
C FDA20_SP1.fgi( 465, 532):������������  �� T
      L_(309) = L_asak.AND.L_esak
C FDA20_SP1.fgi( 414, 694):�
      if(.not.L_(300)) then
         R0_uvuf=0.0
      elseif(.not.L0_axuf) then
         R0_uvuf=R0_ovuf
      else
         R0_uvuf=max(R_(180)-deltat,0.0)
      endif
      L_(298)=L_(300).and.R0_uvuf.le.0.0
      L0_axuf=L_(300)
C FDA20_SP1.fgi( 456, 561):�������� ��������� ������
      if(L_(298).and..not.L0_imak) then
         R0_amak=R0_emak
      else
         R0_amak=max(R_(188)-deltat,0.0)
      endif
      L_opek=R0_amak.gt.0.0
      L0_imak=L_(298)
C FDA20_SP1.fgi( 465, 561):������������  �� T
      L_(488) = L_elal.OR.L_alal.OR.L_ukal
C FDA20_SP1.fgi( 173, 259):���
      L_itil = L_etil.OR.L_atil.OR.L_usil.OR.L_osil.OR.L_isil.OR.L_
     &(488)
C FDA20_SP1.fgi( 183, 653):���
      L0_uxel=(L_itil.or.L0_uxel).and..not.(L_idor)
      L_(469)=.not.L0_uxel
C FDA20_SP1.fgi( 523, 759):RS �������
      L_(468) = L0_uxel.AND.L_efil
C FDA20_SP1.fgi( 534, 760):�
      if(L_(468).and..not.L0_oxel) then
         R0_exel=R0_ixel
      else
         R0_exel=max(R_(267)-deltat,0.0)
      endif
      L_(467)=R0_exel.gt.0.0
      L0_oxel=L_(468)
C FDA20_SP1.fgi( 541, 760):������������  �� T
      L_idil = L_(467).OR.L_(462)
C FDA20_SP1.fgi( 550, 759):���
      if(L0_uxel.and..not.L0_etel) then
         R0_usel=R0_atel
      else
         R0_usel=max(R_(263)-deltat,0.0)
      endif
      L_(470)=R0_usel.gt.0.0
      L0_etel=L0_uxel
C FDA20_SP1.fgi( 534, 780):������������  �� T
      L_(330) = L_ukal.AND.L_efil
C FDA20_SP1.fgi( 165, 156):�
      L_ebik=(L_(330).or.L_ebik).and..not.(L_elol)
      L_(331)=.not.L_ebik
C FDA20_SP1.fgi( 181, 154):RS �������
      L_ubup=L_ebik
C FDA20_SP1.fgi( 197, 160):������,cyl18
      L_(520) = (.NOT.L_ubup)
C FDA20_lamp.fgi(  60, 485):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_odup,L_efup,R_afup,
     & REAL(R_ifup,4),L_(520),L_idup,I_udup)
      !}
C FDA20_lamp.fgi(  74, 484):���������� ������� ���������,20FDA21AL004BQ43
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uxop,L_ibup,R_ebup,
     & REAL(R_obup,4),L_ubup,L_oxop,I_abup)
      !}
C FDA20_lamp.fgi(  74, 496):���������� ������� ���������,20FDA21AL004BQ44
      L_alop=L_ebik
C FDA20_SP1.fgi( 197, 156):������,cyl20
      L_(519) = (.NOT.L_alop)
C FDA20_lamp.fgi( 109, 455):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ulop,L_imop,R_emop,
     & REAL(R_omop,4),L_(519),L_olop,I_amop)
      !}
C FDA20_lamp.fgi( 122, 454):���������� ������� ���������,20FDA21AL004BQ41
      !{
      Call DAT_DISCR_HANDLER(deltat,I_akop,L_okop,R_ikop,
     & REAL(R_ukop,4),L_alop,L_ufop,I_ekop)
      !}
C FDA20_lamp.fgi( 122, 466):���������� ������� ���������,20FDA21AL004BQ42
      L_avek=L_evek
C FDA20_SP1.fgi( 140, 190):������,move_to_y4
      if(L_evek.and..not.L0_otek) then
         R0_etek=R0_itek
      else
         R0_etek=max(R_(200)-deltat,0.0)
      endif
      L_utek=R0_etek.gt.0.0
      L0_otek=L_evek
C FDA20_SP1.fgi( 125, 194):������������  �� T
      L_(489) = L_eral.OR.L_omal.OR.L_imal
C FDA20_SP1.fgi( 173, 292):���
      L_avil = L_ivil.OR.L_util.OR.L_otil.OR.L_(489)
C FDA20_SP1.fgi( 183, 699):���
      L0_edil=(L_avil.or.L0_edil).and..not.(L_oxir)
      L_(473)=.not.L0_edil
C FDA20_SP1.fgi( 523, 784):RS �������
      if(L0_edil.and..not.L0_adil) then
         R0_obil=R0_ubil
      else
         R0_obil=max(R_(269)-deltat,0.0)
      endif
      L_(472)=R0_obil.gt.0.0
      L0_adil=L0_edil
C FDA20_SP1.fgi( 534, 786):������������  �� T
      L_afil = L_(472).OR.L_(470)
C FDA20_SP1.fgi( 541, 785):���
      L_(471) = L0_edil.AND.L_efil
C FDA20_SP1.fgi( 534, 776):�
      if(L_(471).and..not.L0_ibil) then
         R0_abil=R0_ebil
      else
         R0_abil=max(R_(268)-deltat,0.0)
      endif
      L_udil=R0_abil.gt.0.0
      L0_ibil=L_(471)
C FDA20_SP1.fgi( 541, 776):������������  �� T
      L_(333) = L_(362).OR.L_(334)
C FDA20_SP1.fgi( 465, 152):���
      if(L_(333)) then
         I_eluk=0
         L_ukuk=.false.
      endif
C FDA20_SP1.fgi( 474, 144):����� ������� ���������,kt1sp
      if(L_edok.and..not.L0_adok) then
         R0_ibok=R0_obok
      else
         R0_ibok=max(R_(219)-deltat,0.0)
      endif
      L_ubok=R0_ibok.gt.0.0
      L0_adok=L_edok
C FDA20_SP1.fgi( 445, 294):������������  �� T
      if(L_akok.and..not.L0_ufok) then
         R0_efok=R0_ifok
      else
         R0_efok=max(R_(221)-deltat,0.0)
      endif
      L_ofok=R0_efok.gt.0.0
      L0_ufok=L_akok
C FDA20_SP1.fgi( 445, 316):������������  �� T
      if(L_ukok.and..not.L0_okok) then
         R0_ekok=R0_ikok
      else
         R0_ekok=max(R_(222)-deltat,0.0)
      endif
      L_(368)=R0_ekok.gt.0.0
      L0_okok=L_ukok
C FDA20_SP1.fgi( 445, 327):������������  �� T
      if(L_olok.and..not.L0_ilok) then
         R0_alok=R0_elok
      else
         R0_alok=max(R_(223)-deltat,0.0)
      endif
      L_(370)=R0_alok.gt.0.0
      L0_ilok=L_olok
C FDA20_SP1.fgi( 445, 338):������������  �� T
      L_uduk = L_(393).OR.L_(370)
C FDA20_SP1.fgi( 489, 443):���
      if(L_ifik.and..not.L0_efik) then
         R0_udik=R0_afik
      else
         R0_udik=max(R_(205)-deltat,0.0)
      endif
      L_(367)=R0_udik.gt.0.0
      L0_efik=L_ifik
C FDA20_SP1.fgi( 445, 169):������������  �� T
      L_afuk = L_(393).OR.L_(368).OR.L_(367)
C FDA20_SP1.fgi( 489, 448):���
      L_(526) = (.NOT.L_ipir)
C FDA20_lamp.fgi( 196, 649):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukir,L_ilir,R_elir,
     & REAL(R_olir,4),L_(526),L_okir,I_alir)
      !}
C FDA20_lamp.fgi( 210, 648):���������� ������� ���������,20FDA21AE701BQ60
      !{
      Call DAT_DISCR_HANDLER(deltat,I_imir,L_apir,R_umir,
     & REAL(R_epir,4),L_ipir,L_emir,I_omir)
      !}
C FDA20_lamp.fgi( 210, 660):���������� ������� ���������,20FDA21AE701BQ59
      if(L_erok.and..not.L0_arok) then
         R0_ipok=R0_opok
      else
         R0_ipok=max(R_(226)-deltat,0.0)
      endif
      L_upok=R0_ipok.gt.0.0
      L0_arok=L_erok
C FDA20_SP1.fgi( 445, 371):������������  �� T
      if(L_esok.and..not.L0_asok) then
         R0_orok=R0_urok
      else
         R0_orok=max(R_(227)-deltat,0.0)
      endif
      L_(376)=R0_orok.gt.0.0
      L0_asok=L_esok
C FDA20_SP1.fgi( 445, 382):������������  �� T
      if(L_etok.and..not.L0_atok) then
         R0_osok=R0_usok
      else
         R0_osok=max(R_(228)-deltat,0.0)
      endif
      L_(378)=R0_osok.gt.0.0
      L0_atok=L_etok
C FDA20_SP1.fgi( 445, 393):������������  �� T
      L_(525) = (.NOT.L_urer)
C FDA20_lamp.fgi( 196, 590):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_emer,L_umer,R_omer,
     & REAL(R_aper,4),L_(525),L_amer,I_imer)
      !}
C FDA20_lamp.fgi( 210, 588):���������� ������� ���������,20FDA21AE701BQ64
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uper,L_irer,R_erer,
     & REAL(R_orer,4),L_urer,L_oper,I_arer)
      !}
C FDA20_lamp.fgi( 210, 602):���������� ������� ���������,20FDA21AE701BQ63
      if(L_apik.and..not.L0_umik) then
         R0_imik=R0_omik
      else
         R0_imik=max(R_(210)-deltat,0.0)
      endif
      L_(343)=R0_imik.gt.0.0
      L0_umik=L_apik
C FDA20_SP1.fgi( 445, 206):������������  �� T
      L_iduk = L_(393).OR.L_(343)
C FDA20_SP1.fgi( 489, 433):���
      if(L_orik.and..not.L0_irik) then
         R0_arik=R0_erik
      else
         R0_arik=max(R_(212)-deltat,0.0)
      endif
      L_(347)=R0_arik.gt.0.0
      L0_irik=L_orik
C FDA20_SP1.fgi( 445, 228):������������  �� T
      L_irok = L_(376).OR.L_(347)
C FDA20_SP1.fgi( 489, 381):���
      if(L_isik.and..not.L0_esik) then
         R0_urik=R0_asik
      else
         R0_urik=max(R_(213)-deltat,0.0)
      endif
      L_(349)=R0_urik.gt.0.0
      L0_esik=L_isik
C FDA20_SP1.fgi( 445, 239):������������  �� T
      L_isok = L_(378).OR.L_(349)
C FDA20_SP1.fgi( 489, 392):���
      if(L_etik.and..not.L0_atik) then
         R0_osik=R0_usik
      else
         R0_osik=max(R_(214)-deltat,0.0)
      endif
      L_(351)=R0_osik.gt.0.0
      L0_atik=L_etik
C FDA20_SP1.fgi( 445, 250):������������  �� T
      L_ubuk = L_(393).OR.L_(351)
C FDA20_SP1.fgi( 489, 418):���
      L_(524) = (.NOT.L_itar)
C FDA20_lamp.fgi( 196, 561):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upar,L_irar,R_erar,
     & REAL(R_orar,4),L_(524),L_opar,I_arar)
      !}
C FDA20_lamp.fgi( 210, 560):���������� ������� ���������,20FDA21AE701BQ71
      !{
      Call DAT_DISCR_HANDLER(deltat,I_isar,L_atar,R_usar,
     & REAL(R_etar,4),L_itar,L_esar,I_osar)
      !}
C FDA20_lamp.fgi( 210, 572):���������� ������� ���������,20FDA21AE701BQ69
      if(L_uvik.and..not.L0_ovik) then
         R0_evik=R0_ivik
      else
         R0_evik=max(R_(216)-deltat,0.0)
      endif
      L_(357)=R0_evik.gt.0.0
      L0_ovik=L_uvik
C FDA20_SP1.fgi( 445, 272):������������  �� T
      L_obuk = L_(393).OR.L_(357)
C FDA20_SP1.fgi( 489, 414):���
      if(L_(318)) then
         R_emek=R_arek
      else
         R_emek=R_emek
      endif
C FDA20_SP1.fgi( 417, 788):���� RE IN LO CH7
C label 1669  try1669=try1669-1
      R_alav = R_emek
C FDA20_SP1.fgi( 438, 785):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ufav,R_omav,REAL(1,4)
     &,
     & REAL(R_ikav,4),REAL(R_okav,4),
     & REAL(R_ofav,4),REAL(R_ifav,4),I_imav,
     & REAL(R_elav,4),L_ilav,REAL(R_olav,4),L_ulav,L_amav
     &,R_ukav,
     & REAL(R_ekav,4),REAL(R_akav,4),L_emav,REAL(R_alav,4
     &))
      !}
C FDA20_vlv.fgi( 469,  79):���������� �������,20FDA21CW001XQ01
      L_(293)=R_emek.lt.R_(184)
C FDA20_SP1.fgi( 362, 672):���������� <
      L_(297)=R_emek.lt.R_(185)
C FDA20_SP1.fgi( 148,  95):���������� <
      if(L_(315)) then
         R_amek=R_upek
      else
         R_amek=R_amek
      endif
C FDA20_SP1.fgi( 417, 759):���� RE IN LO CH7
C label 1688  try1688=try1688-1
      R_irav = R_amek
C FDA20_SP1.fgi( 438, 756):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epav,R_atav,REAL(1,4)
     &,
     & REAL(R_upav,4),REAL(R_arav,4),
     & REAL(R_apav,4),REAL(R_umav,4),I_usav,
     & REAL(R_orav,4),L_urav,REAL(R_asav,4),L_esav,L_isav
     &,R_erav,
     & REAL(R_opav,4),REAL(R_ipav,4),L_osav,REAL(R_irav,4
     &))
      !}
C FDA20_vlv.fgi( 440,  79):���������� �������,20FDA21CW003XQ01
      L_(292)=R_amek.lt.R_(184)
C FDA20_SP1.fgi( 362, 668):���������� <
      L_(296)=R_amek.lt.R_(185)
C FDA20_SP1.fgi( 148,  91):���������� <
      if(L_(312)) then
         R_imek=R_erek
      else
         R_imek=R_imek
      endif
C FDA20_SP1.fgi( 417, 730):���� RE IN LO CH7
C label 1707  try1707=try1707-1
      R_uses = R_imek
C FDA20_SP1.fgi( 438, 727):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ores,R_ives,REAL(1,4)
     &,
     & REAL(R_eses,4),REAL(R_ises,4),
     & REAL(R_ires,4),REAL(R_eres,4),I_eves,
     & REAL(R_ates,4),L_etes,REAL(R_ites,4),L_otes,L_utes
     &,R_oses,
     & REAL(R_ases,4),REAL(R_ures,4),L_aves,REAL(R_uses,4
     &))
      !}
C FDA20_vlv.fgi( 441,  66):���������� �������,20FDA21CW005XQ01
      L_(291)=R_imek.lt.R_(184)
C FDA20_SP1.fgi( 362, 664):���������� <
      L_(295)=R_imek.lt.R_(185)
C FDA20_SP1.fgi( 148,  87):���������� <
      if(L_(309)) then
         R_omek=R_irek
      else
         R_omek=R_omek
      endif
C FDA20_SP1.fgi( 417, 701):���� RE IN LO CH7
C label 1726  try1726=try1726-1
      R_imes = R_omek
C FDA20_SP1.fgi( 438, 698):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eles,R_ares,REAL(1,4)
     &,
     & REAL(R_ules,4),REAL(R_ames,4),
     & REAL(R_ales,4),REAL(R_ukes,4),I_upes,
     & REAL(R_omes,4),L_umes,REAL(R_apes,4),L_epes,L_ipes
     &,R_emes,
     & REAL(R_oles,4),REAL(R_iles,4),L_opes,REAL(R_imes,4
     &))
      !}
C FDA20_vlv.fgi( 469,  66):���������� �������,20FDA21CW007XQ01
      L_(290)=R_omek.lt.R_(184)
C FDA20_SP1.fgi( 362, 660):���������� <
      L_adak = L_(293).AND.L_(292).AND.L_(291).AND.L_(290
     &)
C FDA20_SP1.fgi( 376, 669):�
      L_(294)=R_omek.lt.R_(185)
C FDA20_SP1.fgi( 148,  83):���������� <
      L_akak = L_(297).OR.L_(296).OR.L_(295).OR.L_(294).OR.
     &(.NOT.L_umel).OR.(.NOT.L_udel).OR.(.NOT.L_edel).OR.
     &(.NOT.L_ivek)
C FDA20_SP1.fgi( 180,  88):���
      L_(289) = L_(287).AND.(.NOT.L_akak)
C FDA20_SP2_UVR.fgi(  66, 825):�
      if (.not.L_omuf.and.(L_umuf.or.L_(289))) then
          I_apuf = 0
          L_omuf = .true.
          L_(288) = .true.
      endif
      if (I_apuf .gt. 0) then
         L_(288) = .false.
      endif
      if(L_epuf)then
          I_apuf = 0
          L_omuf = .false.
          L_(288) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 818):���������� ������� ���������,uvr
      !{Call KN_VLVR(deltat,REAL(R_efife,4),L_omife,L_umife
C ,R8_ebife,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_efife,4),L_omife
     &,L_umife,R8_ebife,C30_odife,
     & L_usike,L_utike,L_uvike,I_olife,I_ulife,R_evefe,
     & REAL(R_ovefe,4),R_ixefe,REAL(R_uxefe,4),
     & R_uvefe,REAL(R_exefe,4),I_ukife,I_amife,I_ilife,I_elife
     &,
     & L_oxefe,L_axefe,L_adife,L_ubife,L_apife,
     & L_ivefe,L_idife,L_erife,L_obife,L_ibife,L_arife,L_irife
     &,
     & L_abife,L_emife,L_opife,L_imife,L_udife,L_afife,
     & REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_upife,R_okife
     &,
     & REAL(R_edife,4),L_ifife,L_ofife,L_ufife,L_ekife,L_ikife
     &,L_akife)
      !}

      if(L_ikife.or.L_ekife.or.L_akife.or.L_ufife.or.L_ofife.or.L_ifife
     &) then      
                  I_alife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_alife = z'40000000'
      endif
C FDA20_vlv.fgi( 140, 176):���� ���������� ���������������� ��������,20FDA23AB002
C label 1757  try1757=try1757-1
      if(.not.L_ir) then
         R0_ar=0.0
      elseif(.not.L0_er) then
         R0_ar=R0_up
      else
         R0_ar=max(R_(10)-deltat,0.0)
      endif
      L_(10)=L_ir.and.R0_ar.le.0.0
      L0_er=L_ir
C FDA20_SP2_UVR.fgi( 404, 531):�������� ��������� ������
      L_(12) = L_omife.AND.L_(10)
C FDA20_SP2_UVR.fgi( 357, 537):�
      if(.not.L_is) then
         R0_ur=0.0
      elseif(.not.L0_as) then
         R0_ur=R0_or
      else
         R0_ur=max(R_(11)-deltat,0.0)
      endif
      L_(14)=L_is.and.R0_ur.le.0.0
      L0_as=L_is
C FDA20_SP2_UVR.fgi( 404, 548):�������� ��������� ������
      if(.not.L_ipi) then
         R0_ud=0.0
      elseif(.not.L0_af) then
         R0_ud=R0_od
      else
         R0_ud=max(R_(3)-deltat,0.0)
      endif
      L_(6)=L_ipi.and.R0_ud.le.0.0
      L0_af=L_ipi
C FDA20_SP2_UVR.fgi( 402, 566):�������� ��������� ������
      L_(45) = L_(82).AND.L_(6)
C FDA20_SP2_UVR.fgi( 354, 571):�
      if(.not.L_ido) then
         R0_ibo=0.0
      elseif(.not.L0_obo) then
         R0_ibo=R0_ebo
      else
         R0_ibo=max(R_(52)-deltat,0.0)
      endif
      L_(57)=L_ido.and.R0_ibo.le.0.0
      L0_obo=L_ido
C FDA20_SP2_UVR.fgi( 404, 668):�������� ��������� ������
      L_(60) = L_uvo.AND.L_(57)
C FDA20_SP2_UVR.fgi( 342, 674):�
      if(.not.L_idu) then
         R0_if=0.0
      elseif(.not.L0_of) then
         R0_if=R0_ef
      else
         R0_if=max(R_(4)-deltat,0.0)
      endif
      L_(7)=L_idu.and.R0_if.le.0.0
      L0_of=L_idu
C FDA20_SP2_UVR.fgi( 402, 798):�������� ��������� ������
      L_(86) = L_(7).AND.L_(82)
C FDA20_SP2_UVR.fgi( 354, 803):�
      if(.not.L_epi) then
         R0_umi=0.0
      elseif(.not.L0_api) then
         R0_umi=R0_omi
      else
         R0_umi=max(R_(44)-deltat,0.0)
      endif
      L_(42)=L_epi.and.R0_umi.le.0.0
      L0_api=L_epi
C FDA20_SP2_UVR.fgi( 404, 500):�������� ��������� ������
      L_edad=L_epi
C FDA20_SP2_UVR.fgi( 458, 506):������,tilter_complete
      if(.not.L_ikad) then
         R0_udad=0.0
      elseif(.not.L0_afad) then
         R0_udad=R0_odad
      else
         R0_udad=max(R_(76)-deltat,0.0)
      endif
      L_(109)=L_ikad.and.R0_udad.le.0.0
      L0_afad=L_ikad
C FDA20_SP2_UVR.fgi(  44, 185):�������� ��������� ������
      L_(112) = L_otif.AND.L_(109)
C FDA20_SP2_UVR.fgi(  57, 189):�
      if(.not.L_atad) then
         R0_emad=0.0
      elseif(.not.L0_imad) then
         R0_emad=R0_amad
      else
         R0_emad=max(R_(81)-deltat,0.0)
      endif
      L_(115)=L_atad.and.R0_emad.le.0.0
      L0_imad=L_atad
C FDA20_SP2_UVR.fgi( 138, 215):�������� ��������� ������
      L_(119) = L_amod.AND.L_(115)
C FDA20_SP2_UVR.fgi(  59, 223):�
      Call FDA20_1(ext_deltat)
      End
      Subroutine FDA20_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      if(.not.L_ovad) then
         R0_itad=0.0
      elseif(.not.L0_otad) then
         R0_itad=R0_etad
      else
         R0_itad=max(R_(85)-deltat,0.0)
      endif
      L_(120)=L_ovad.and.R0_itad.le.0.0
      L0_otad=L_ovad
C FDA20_SP2_UVR.fgi(  44, 264):�������� ��������� ������
      L_(122) = L_ubof.AND.L_abof.AND.L_exif.AND.L_ivif.AND.L_otif.AND.L
     &_usif.AND.L_asif.AND.L_erif.AND.L_ipif.AND.L_ivad.AND.L_
     &(120)
C FDA20_SP2_UVR.fgi(  57, 277):�
      if(.not.L_ikid) then
         R0_akid=0.0
      elseif(.not.L0_ekid) then
         R0_akid=R0_ufid
      else
         R0_akid=max(R_(101)-deltat,0.0)
      endif
      L_(160)=L_ikid.and.R0_akid.le.0.0
      L0_ekid=L_ikid
C FDA20_SP2_UVR.fgi(  69, 351):�������� ��������� ������
      if(.not.L_ul) then
         R0_al=0.0
      elseif(.not.L0_el) then
         R0_al=R0_uk
      else
         R0_al=max(R_(6)-deltat,0.0)
      endif
      L_(8)=L_ul.and.R0_al.le.0.0
      L0_el=L_ul
C FDA20_SP2_UVR.fgi(  69, 550):�������� ��������� ������
      if(.not.L_odof) then
         R0_use=0.0
      elseif(.not.L0_ate) then
         R0_use=R0_ose
      else
         R0_use=max(R_(32)-deltat,0.0)
      endif
      L_(30)=L_odof.and.R0_use.le.0.0
      L0_ate=L_odof
C FDA20_SP2_UVR.fgi(  45, 653):�������� ��������� ������
      L_(259) = L_ivad.AND.L_(30)
C FDA20_SP2_UVR.fgi(  57, 659):�
      if(.not.L_ukuf) then
         R0_iki=0.0
      elseif(.not.L0_oki) then
         R0_iki=R0_eki
      else
         R0_iki=max(R_(43)-deltat,0.0)
      endif
      L_(40)=L_ukuf.and.R0_iki.le.0.0
      L0_oki=L_ukuf
C FDA20_SP2_UVR.fgi(  45, 769):�������� ��������� ������
      L_(286) = L_imi.AND.L_emi.AND.L_ami.AND.L_uli.AND.L_oli.AND.L_ili.
     &AND.L_eli.AND.L_ali.AND.L_uki.AND.L_uxu.AND.L_ubof.AND.L_abof.AND.
     &L_exif.AND.L_ivif.AND.L_otif.AND.L_usif.AND.L_asif.AND.L_erif.AND.
     &L_ipif.AND.L_ivad.AND.L_(40)
C FDA20_SP2_UVR.fgi(  58, 792):�
      if(L_omuf) then
          if (L_(288)) then
              I_apuf = 1
              L_ukuf = .true.
              L_(285) = .false.
          endif
          if (L_(286)) then
              L_(285) = .true.
              L_ukuf = .false.
          endif
          if (I_apuf.ne.1) then
              L_ukuf = .false.
              L_(285) = .false.
          endif
      else
          L_(285) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 804):��� ������� ���������,uvr
C sav1=L_(40)
      if(.not.L_ukuf) then
         R0_iki=0.0
      elseif(.not.L0_oki) then
         R0_iki=R0_eki
      else
         R0_iki=max(R_(43)-deltat,0.0)
      endif
      L_(40)=L_ukuf.and.R0_iki.le.0.0
      L0_oki=L_ukuf
C FDA20_SP2_UVR.fgi(  45, 769):recalc:�������� ��������� ������
C if(sav1.ne.L_(40) .and. try1810.gt.0) goto 1810
      if(L_omuf) then
          if (L_(285)) then
              I_apuf = 2
              L_okuf = .true.
              L_(283) = .false.
          endif
          if (L_(284)) then
              L_(283) = .true.
              L_okuf = .false.
          endif
          if (I_apuf.ne.2) then
              L_okuf = .false.
              L_(283) = .false.
          endif
      else
          L_(283) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 740):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(283)) then
              I_apuf = 3
              L_afof = .true.
              L_(279) = .false.
          endif
          if (L_udof) then
              L_(279) = .true.
              L_afof = .false.
          endif
          if (I_apuf.ne.3) then
              L_afof = .false.
              L_(279) = .false.
          endif
      else
          L_(279) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 717):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(279)) then
              I_apuf = 4
              L_omif = .true.
              L_(257) = .false.
          endif
          if (L_(258)) then
              L_(257) = .true.
              L_omif = .false.
          endif
          if (I_apuf.ne.4) then
              L_omif = .false.
              L_(257) = .false.
          endif
      else
          L_(257) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 672):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(257)) then
              I_apuf = 5
              L_odof = .true.
              L_(256) = .false.
          endif
          if (L_(259)) then
              L_(256) = .true.
              L_odof = .false.
          endif
          if (I_apuf.ne.5) then
              L_odof = .false.
              L_(256) = .false.
          endif
      else
          L_(256) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 659):��� ������� ���������,uvr
C sav1=L_(30)
      if(.not.L_odof) then
         R0_use=0.0
      elseif(.not.L0_ate) then
         R0_use=R0_ose
      else
         R0_use=max(R_(32)-deltat,0.0)
      endif
      L_(30)=L_odof.and.R0_use.le.0.0
      L0_ate=L_odof
C FDA20_SP2_UVR.fgi(  45, 653):recalc:�������� ��������� ������
C if(sav1.ne.L_(30) .and. try1806.gt.0) goto 1806
      if(L_omuf) then
          if (L_(256)) then
              I_apuf = 6
              L_ikif = .true.
              L_(254) = .false.
          endif
          if (L_(255)) then
              L_(254) = .true.
              L_ikif = .false.
          endif
          if (I_apuf.ne.6) then
              L_ikif = .false.
              L_(254) = .false.
          endif
      else
          L_(254) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 616):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(254)) then
              I_apuf = 7
              L_itef = .true.
              L_(248) = .false.
          endif
          if (L_etef) then
              L_(248) = .true.
              L_itef = .false.
          endif
          if (I_apuf.ne.7) then
              L_itef = .false.
              L_(248) = .false.
          endif
      else
          L_(248) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 600):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(248)) then
              I_apuf = 8
              L_ebud = .true.
              L_(235) = .false.
          endif
          if (L_abud) then
              L_(235) = .true.
              L_ebud = .false.
          endif
          if (I_apuf.ne.8) then
              L_ebud = .false.
              L_(235) = .false.
          endif
      else
          L_(235) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 588):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(235)) then
              I_apuf = 9
              L_axod = .true.
              L_(233) = .false.
          endif
          if (L_uvod) then
              L_(233) = .true.
              L_axod = .false.
          endif
          if (I_apuf.ne.9) then
              L_axod = .false.
              L_(233) = .false.
          endif
      else
          L_(233) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 576):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(233)) then
              I_apuf = 10
              L_utod = .true.
              L_(231) = .false.
          endif
          if (L_otod) then
              L_(231) = .true.
              L_utod = .false.
          endif
          if (I_apuf.ne.10) then
              L_utod = .false.
              L_(231) = .false.
          endif
      else
          L_(231) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 564):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(231)) then
              I_apuf = 11
              L_ul = .true.
              L_(200) = .false.
          endif
          if (L_(8)) then
              L_(200) = .true.
              L_ul = .false.
          endif
          if (I_apuf.ne.11) then
              L_ul = .false.
              L_(200) = .false.
          endif
      else
          L_(200) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 550):��� ������� ���������,uvr
C sav1=L_(8)
      if(.not.L_ul) then
         R0_al=0.0
      elseif(.not.L0_el) then
         R0_al=R0_uk
      else
         R0_al=max(R_(6)-deltat,0.0)
      endif
      L_(8)=L_ul.and.R0_al.le.0.0
      L0_el=L_ul
C FDA20_SP2_UVR.fgi(  69, 550):recalc:�������� ��������� ������
C if(sav1.ne.L_(8) .and. try1804.gt.0) goto 1804
      if(L_omuf) then
          if (L_(200)) then
              I_apuf = 12
              L_imod = .true.
              L_(201) = .false.
          endif
          if (L_(202)) then
              L_(201) = .true.
              L_imod = .false.
          endif
          if (I_apuf.ne.12) then
              L_imod = .false.
              L_(201) = .false.
          endif
      else
          L_(201) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 536):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(201)) then
              I_apuf = 13
              L_emod = .true.
              L_(194) = .false.
          endif
          if (L_udof) then
              L_(194) = .true.
              L_emod = .false.
          endif
          if (I_apuf.ne.13) then
              L_emod = .false.
              L_(194) = .false.
          endif
      else
          L_(194) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 525):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(194)) then
              I_apuf = 14
              L_ibod = .true.
              L_(192) = .false.
          endif
          if (L_(193)) then
              L_(192) = .true.
              L_ibod = .false.
          endif
          if (I_apuf.ne.14) then
              L_ibod = .false.
              L_(192) = .false.
          endif
      else
          L_(192) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 491):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(192)) then
              I_apuf = 15
              L_evid = .true.
              L_(183) = .false.
          endif
          if (L_(184)) then
              L_(183) = .true.
              L_evid = .false.
          endif
          if (I_apuf.ne.15) then
              L_evid = .false.
              L_(183) = .false.
          endif
      else
          L_(183) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 456):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(183)) then
              I_apuf = 16
              L_asid = .true.
              L_(175) = .false.
          endif
          if (L_(176)) then
              L_(175) = .true.
              L_asid = .false.
          endif
          if (I_apuf.ne.16) then
              L_asid = .false.
              L_(175) = .false.
          endif
      else
          L_(175) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 422):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(175)) then
              I_apuf = 17
              L_umid = .true.
              L_(167) = .false.
          endif
          if (L_(168)) then
              L_(167) = .true.
              L_umid = .false.
          endif
          if (I_apuf.ne.17) then
              L_umid = .false.
              L_(167) = .false.
          endif
      else
          L_(167) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 389):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(167)) then
              I_apuf = 18
              L_ikid = .true.
              L_(159) = .false.
          endif
          if (L_(160)) then
              L_(159) = .true.
              L_ikid = .false.
          endif
          if (I_apuf.ne.18) then
              L_ikid = .false.
              L_(159) = .false.
          endif
      else
          L_(159) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 351):��� ������� ���������,uvr
C sav1=L_(160)
      if(.not.L_ikid) then
         R0_akid=0.0
      elseif(.not.L0_ekid) then
         R0_akid=R0_ufid
      else
         R0_akid=max(R_(101)-deltat,0.0)
      endif
      L_(160)=L_ikid.and.R0_akid.le.0.0
      L0_ekid=L_ikid
C FDA20_SP2_UVR.fgi(  69, 351):recalc:�������� ��������� ������
C if(sav1.ne.L_(160) .and. try1802.gt.0) goto 1802
      if(L_omuf) then
          if (L_(159)) then
              I_apuf = 19
              L_osed = .true.
              L_(141) = .false.
          endif
          if (L_(142)) then
              L_(141) = .true.
              L_osed = .false.
          endif
          if (I_apuf.ne.19) then
              L_osed = .false.
              L_(141) = .false.
          endif
      else
          L_(141) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 340):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(141)) then
              I_apuf = 20
              L_ovad = .true.
              L_(121) = .false.
          endif
          if (L_(122)) then
              L_(121) = .true.
              L_ovad = .false.
          endif
          if (I_apuf.ne.20) then
              L_ovad = .false.
              L_(121) = .false.
          endif
      else
          L_(121) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 277):��� ������� ���������,uvr
C sav1=L_(120)
      if(.not.L_ovad) then
         R0_itad=0.0
      elseif(.not.L0_otad) then
         R0_itad=R0_etad
      else
         R0_itad=max(R_(85)-deltat,0.0)
      endif
      L_(120)=L_ovad.and.R0_itad.le.0.0
      L0_otad=L_ovad
C FDA20_SP2_UVR.fgi(  44, 264):recalc:�������� ��������� ������
C if(sav1.ne.L_(120) .and. try1798.gt.0) goto 1798
      if(L_omuf) then
          if (L_(121)) then
              I_apuf = 21
              L_ise = .true.
              L_(117) = .false.
          endif
          if (L_(29)) then
              L_(117) = .true.
              L_ise = .false.
          endif
          if (I_apuf.ne.21) then
              L_ise = .false.
              L_(117) = .false.
          endif
      else
          L_(117) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 250):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(117)) then
              I_apuf = 22
              L_atad = .true.
              L_(118) = .false.
          endif
          if (L_(119)) then
              L_(118) = .true.
              L_atad = .false.
          endif
          if (I_apuf.ne.22) then
              L_atad = .false.
              L_(118) = .false.
          endif
      else
          L_(118) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 223):��� ������� ���������,uvr
C sav1=L_(115)
      if(.not.L_atad) then
         R0_emad=0.0
      elseif(.not.L0_imad) then
         R0_emad=R0_amad
      else
         R0_emad=max(R_(81)-deltat,0.0)
      endif
      L_(115)=L_atad.and.R0_emad.le.0.0
      L0_imad=L_atad
C FDA20_SP2_UVR.fgi( 138, 215):recalc:�������� ��������� ������
C if(sav1.ne.L_(115) .and. try1792.gt.0) goto 1792
      if(L_omuf) then
          if (L_(118)) then
              I_apuf = 23
              L_ikad = .true.
              L_(111) = .false.
          endif
          if (L_(112)) then
              L_(111) = .true.
              L_ikad = .false.
          endif
          if (I_apuf.ne.23) then
              L_ikad = .false.
              L_(111) = .false.
          endif
      else
          L_(111) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 189):��� ������� ���������,uvr
C sav1=L_(109)
      if(.not.L_ikad) then
         R0_udad=0.0
      elseif(.not.L0_afad) then
         R0_udad=R0_odad
      else
         R0_udad=max(R_(76)-deltat,0.0)
      endif
      L_(109)=L_ikad.and.R0_udad.le.0.0
      L0_afad=L_ikad
C FDA20_SP2_UVR.fgi(  44, 185):recalc:�������� ��������� ������
C if(sav1.ne.L_(109) .and. try1788.gt.0) goto 1788
      if(L_omuf) then
          if (L_(111)) then
              I_apuf = 24
              L_ilu = .true.
              L_(107) = .false.
          endif
          if (L_olif) then
              L_(107) = .true.
              L_ilu = .false.
          endif
          if (I_apuf.ne.24) then
              L_ilu = .false.
              L_(107) = .false.
          endif
      else
          L_(107) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 172):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(107)) then
              I_apuf = 25
              L_idad = .true.
              L_(108) = .false.
          endif
          if (L_edad) then
              L_(108) = .true.
              L_idad = .false.
          endif
          if (I_apuf.ne.25) then
              L_idad = .false.
              L_(108) = .false.
          endif
      else
          L_(108) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 153):��� ������� ���������,uvr
      if(L_idad.and..not.L0_obad) then
         R0_ebad=R0_ibad
      else
         R0_ebad=max(R_(75)-deltat,0.0)
      endif
      L_adad=R0_ebad.gt.0.0
      L0_obad=L_idad
C FDA20_SP2_UVR.fgi( 122, 153):������������  �� T
      if(L_adad.and..not.L0_ofu) then
         R0_efu=R0_ifu
      else
         R0_efu=max(R_(64)-deltat,0.0)
      endif
      L_(88)=R0_efu.gt.0.0
      L0_ofu=L_adad
C FDA20_SP2_UVR.fgi( 353, 822):������������  �� T
      if (.not.L_aku.and.(L_eku.or.L_(88))) then
          I_iku = 0
          L_aku = .true.
          L_(87) = .true.
      endif
      if (I_iku .gt. 0) then
         L_(87) = .false.
      endif
      if(L_oku)then
          I_iku = 0
          L_aku = .false.
          L_(87) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 814):���������� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(87)) then
              I_iku = 1
              L_idu = .true.
              L_(85) = .false.
          endif
          if (L_(86)) then
              L_(85) = .true.
              L_idu = .false.
          endif
          if (I_iku.ne.1) then
              L_idu = .false.
              L_(85) = .false.
          endif
      else
          L_(85) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 803):��� ������� ���������,uvrkant
C sav1=L_(7)
      if(.not.L_idu) then
         R0_if=0.0
      elseif(.not.L0_of) then
         R0_if=R0_ef
      else
         R0_if=max(R_(4)-deltat,0.0)
      endif
      L_(7)=L_idu.and.R0_if.le.0.0
      L0_of=L_idu
C FDA20_SP2_UVR.fgi( 402, 798):recalc:�������� ��������� ������
C if(sav1.ne.L_(7) .and. try1779.gt.0) goto 1779
      if(L_aku) then
          if (L_(85)) then
              I_iku = 2
              L_eto = .true.
              L_(84) = .false.
          endif
          if (L_uto) then
              L_(84) = .true.
              L_eto = .false.
          endif
          if (I_iku.ne.2) then
              L_eto = .false.
              L_(84) = .false.
          endif
      else
          L_(84) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 777):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(84)) then
              I_iku = 3
              L_iso = .true.
              L_(81) = .false.
          endif
          if (L_eso) then
              L_(81) = .true.
              L_iso = .false.
          endif
          if (I_iku.ne.3) then
              L_iso = .false.
              L_(81) = .false.
          endif
      else
          L_(81) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 766):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(81)) then
              I_iku = 4
              L_ero = .true.
              L_(79) = .false.
          endif
          if (L_axo) then
              L_(79) = .true.
              L_ero = .false.
          endif
          if (I_iku.ne.4) then
              L_ero = .false.
              L_(79) = .false.
          endif
      else
          L_(79) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 751):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(79)) then
              I_iku = 5
              L_ipo = .true.
              L_(77) = .false.
          endif
          if (L_iro) then
              L_(77) = .true.
              L_ipo = .false.
          endif
          if (I_iku.ne.5) then
              L_ipo = .false.
              L_(77) = .false.
          endif
      else
          L_(77) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 740):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(77)) then
              I_iku = 6
              L_omo = .true.
              L_(75) = .false.
          endif
          if (L_oto) then
              L_(75) = .true.
              L_omo = .false.
          endif
          if (I_iku.ne.6) then
              L_omo = .false.
              L_(75) = .false.
          endif
      else
          L_(75) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 729):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(75)) then
              I_iku = 7
              L_ulo = .true.
              L_(71) = .false.
          endif
          if (L_eso) then
              L_(71) = .true.
              L_ulo = .false.
          endif
          if (I_iku.ne.7) then
              L_ulo = .false.
              L_(71) = .false.
          endif
      else
          L_(71) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 718):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(71)) then
              I_iku = 8
              L_alo = .true.
              L_(65) = .false.
          endif
          if (L_(66)) then
              L_(65) = .true.
              L_alo = .false.
          endif
          if (I_iku.ne.8) then
              L_alo = .false.
              L_(65) = .false.
          endif
      else
          L_(65) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 707):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(65)) then
              I_iku = 9
              L_ako = .true.
              L_(62) = .false.
          endif
          if (L_iro) then
              L_(62) = .true.
              L_ako = .false.
          endif
          if (I_iku.ne.9) then
              L_ako = .false.
              L_(62) = .false.
          endif
      else
          L_(62) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 696):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(62)) then
              I_iku = 10
              L_efo = .true.
              L_(61) = .false.
          endif
          if (L_oxo) then
              L_(61) = .true.
              L_efo = .false.
          endif
          if (I_iku.ne.10) then
              L_efo = .false.
              L_(61) = .false.
          endif
      else
          L_(61) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 685):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(61)) then
              I_iku = 11
              L_ido = .true.
              L_(59) = .false.
          endif
          if (L_(60)) then
              L_(59) = .true.
              L_ido = .false.
          endif
          if (I_iku.ne.11) then
              L_ido = .false.
              L_(59) = .false.
          endif
      else
          L_(59) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 674):��� ������� ���������,uvrkant
C sav1=L_(57)
      if(.not.L_ido) then
         R0_ibo=0.0
      elseif(.not.L0_obo) then
         R0_ibo=R0_ebo
      else
         R0_ibo=max(R_(52)-deltat,0.0)
      endif
      L_(57)=L_ido.and.R0_ibo.le.0.0
      L0_obo=L_ido
C FDA20_SP2_UVR.fgi( 404, 668):recalc:�������� ��������� ������
C if(sav1.ne.L_(57) .and. try1775.gt.0) goto 1775
      if(L_aku) then
          if (L_(59)) then
              I_iku = 12
              L_abo = .true.
              L_(56) = .false.
          endif
          if (L_ovo) then
              L_(56) = .true.
              L_abo = .false.
          endif
          if (I_iku.ne.12) then
              L_abo = .false.
              L_(56) = .false.
          endif
      else
          L_(56) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 659):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(56)) then
              I_iku = 13
              L_exi = .true.
              L_(54) = .false.
          endif
          if (L_ixo) then
              L_(54) = .true.
              L_exi = .false.
          endif
          if (I_iku.ne.13) then
              L_exi = .false.
              L_(54) = .false.
          endif
      else
          L_(54) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 648):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(54)) then
              I_iku = 14
              L_ivi = .true.
              L_(53) = .false.
          endif
          if (L_eso) then
              L_(53) = .true.
              L_ivi = .false.
          endif
          if (I_iku.ne.14) then
              L_ivi = .false.
              L_(53) = .false.
          endif
      else
          L_(53) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 637):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(53)) then
              I_iku = 15
              L_oti = .true.
              L_(51) = .false.
          endif
          if (L_ivo) then
              L_(51) = .true.
              L_oti = .false.
          endif
          if (I_iku.ne.15) then
              L_oti = .false.
              L_(51) = .false.
          endif
      else
          L_(51) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 626):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(51)) then
              I_iku = 16
              L_usi = .true.
              L_(50) = .false.
          endif
          if (L_iro) then
              L_(50) = .true.
              L_usi = .false.
          endif
          if (I_iku.ne.16) then
              L_usi = .false.
              L_(50) = .false.
          endif
      else
          L_(50) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 615):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(50)) then
              I_iku = 17
              L_asi = .true.
              L_(49) = .false.
          endif
          if (L_uto) then
              L_(49) = .true.
              L_asi = .false.
          endif
          if (I_iku.ne.17) then
              L_asi = .false.
              L_(49) = .false.
          endif
      else
          L_(49) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 604):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(49)) then
              I_iku = 18
              L_av = .true.
              L_(46) = .false.
          endif
          if (L_eso) then
              L_(46) = .true.
              L_av = .false.
          endif
          if (I_iku.ne.18) then
              L_av = .false.
              L_(46) = .false.
          endif
      else
          L_(46) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 593):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(46)) then
              I_iku = 19
              L_eri = .true.
              L_(47) = .false.
          endif
          if (L_exo) then
              L_(47) = .true.
              L_eri = .false.
          endif
          if (I_iku.ne.19) then
              L_eri = .false.
              L_(47) = .false.
          endif
      else
          L_(47) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 582):��� ������� ���������,uvrkant
      if(L_aku) then
          if (L_(47)) then
              I_iku = 20
              L_ipi = .true.
              L_(44) = .false.
          endif
          if (L_(45)) then
              L_(44) = .true.
              L_ipi = .false.
          endif
          if (I_iku.ne.20) then
              L_ipi = .false.
              L_(44) = .false.
          endif
      else
          L_(44) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 571):��� ������� ���������,uvrkant
C sav1=L_(6)
      if(.not.L_ipi) then
         R0_ud=0.0
      elseif(.not.L0_af) then
         R0_ud=R0_od
      else
         R0_ud=max(R_(3)-deltat,0.0)
      endif
      L_(6)=L_ipi.and.R0_ud.le.0.0
      L0_af=L_ipi
C FDA20_SP2_UVR.fgi( 402, 566):recalc:�������� ��������� ������
C if(sav1.ne.L_(6) .and. try1771.gt.0) goto 1771
      if(L_aku) then
          if (L_(44)) then
              I_iku = 21
              L_is = .true.
              L_(13) = .false.
          endif
          if (L_(14)) then
              L_(13) = .true.
              L_is = .false.
          endif
          if (I_iku.ne.21) then
              L_is = .false.
              L_(13) = .false.
          endif
      else
          L_(13) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 554):��� ������� ���������,uvrkant
C sav1=L_(14)
      if(.not.L_is) then
         R0_ur=0.0
      elseif(.not.L0_as) then
         R0_ur=R0_or
      else
         R0_ur=max(R_(11)-deltat,0.0)
      endif
      L_(14)=L_is.and.R0_ur.le.0.0
      L0_as=L_is
C FDA20_SP2_UVR.fgi( 404, 548):recalc:�������� ��������� ������
C if(sav1.ne.L_(14) .and. try1769.gt.0) goto 1769
      if(L_aku) then
          if (L_(13)) then
              I_iku = 22
              L_ir = .true.
              L_(11) = .false.
          endif
          if (L_(12)) then
              L_(11) = .true.
              L_ir = .false.
          endif
          if (I_iku.ne.22) then
              L_ir = .false.
              L_(11) = .false.
          endif
      else
          L_(11) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 537):��� ������� ���������,uvrkant
C sav1=L_(10)
      if(.not.L_ir) then
         R0_ar=0.0
      elseif(.not.L0_er) then
         R0_ar=R0_up
      else
         R0_ar=max(R_(10)-deltat,0.0)
      endif
      L_(10)=L_ir.and.R0_ar.le.0.0
      L0_er=L_ir
C FDA20_SP2_UVR.fgi( 404, 531):recalc:�������� ��������� ������
C if(sav1.ne.L_(10) .and. try1766.gt.0) goto 1766
      if(L_aku) then
          if (L_(11)) then
              I_iku = 23
              L_ap = .true.
              L_(41) = .false.
          endif
          if (L_umife) then
              L_(41) = .true.
              L_ap = .false.
          endif
          if (I_iku.ne.23) then
              L_ap = .false.
              L_(41) = .false.
          endif
      else
          L_(41) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 520):��� ������� ���������,uvrkant
      if(L_ap.and..not.L0_um) then
         R0_im=R0_om
      else
         R0_im=max(R_(8)-deltat,0.0)
      endif
      L_os=R0_im.gt.0.0
      L0_um=L_ap
C FDA20_SP2_UVR.fgi( 403, 520):������������  �� T
      L_obife=L_os
C FDA20_SP2_UVR.fgi( 461, 520):������,20FDA23AB002_ulucl
      L_(9) = L_em.AND.L_ir
C FDA20_SP2_UVR.fgi( 401, 538):�
      if(L_(9).and..not.L0_op) then
         R0_ep=R0_ip
      else
         R0_ep=max(R_(9)-deltat,0.0)
      endif
      L_ibife=R0_ep.gt.0.0
      L0_op=L_(9)
C FDA20_SP2_UVR.fgi( 422, 538):������������  �� T
      if(L_ibife) then
         R_(12)=R_(7)
      endif
C FDA20_SP2_UVR.fgi( 430, 543):���� � ������������� �������
      R8_e=R_(12)
C FDA20_SP2_UVR.fgi( 430, 543):������-�������: ���������� ��� �������������� ������
      R_ol = R8_e
C FDA20_SP2_UVR.fgi( 439, 549):��������
      if(L_aku) then
          if (L_(41)) then
              I_iku = 24
              L_epi = .true.
              L_(43) = .false.
          endif
          if (L_(42)) then
              L_(43) = .true.
              L_epi = .false.
          endif
          if (I_iku.ne.24) then
              L_epi = .false.
              L_(43) = .false.
          endif
      else
          L_(43) = .false.
      endif
C FDA20_SP2_UVR.fgi( 376, 506):��� ������� ���������,uvrkant
C sav1=L_(42)
      if(.not.L_epi) then
         R0_umi=0.0
      elseif(.not.L0_api) then
         R0_umi=R0_omi
      else
         R0_umi=max(R_(44)-deltat,0.0)
      endif
      L_(42)=L_epi.and.R0_umi.le.0.0
      L0_api=L_epi
C FDA20_SP2_UVR.fgi( 404, 500):recalc:�������� ��������� ������
C if(sav1.ne.L_(42) .and. try1783.gt.0) goto 1783
C sav1=L_edad
C FDA20_SP2_UVR.fgi( 458, 506):recalc:������,tilter_complete
C if(sav1.ne.L_edad .and. try1785.gt.0) goto 1785
      if(L_(43)) then
         I_iku=0
         L_aku=.false.
      endif
C FDA20_SP2_UVR.fgi( 376, 490):����� ������� ���������,uvrkant
      L_es=L_is
C FDA20_SP2_UVR.fgi( 458, 554):������,g_proc
      L_evo = L_idu.OR.L_ipi
C FDA20_SP2_UVR.fgi( 442, 802):���
      L_avo=L_evo
C FDA20_SP2_UVR.fgi( 466, 802):������,20FDA20KANT01_INIT
      if(L_eri.and..not.L0_ari) then
         R0_opi=R0_upi
      else
         R0_opi=max(R_(45)-deltat,0.0)
      endif
      L_(69)=R0_opi.gt.0.0
      L0_ari=L_eri
C FDA20_SP2_UVR.fgi( 403, 582):������������  �� T
      if(L_av.and..not.L0_ut) then
         R0_it=R0_ot
      else
         R0_it=max(R_(14)-deltat,0.0)
      endif
      L_(72)=R0_it.gt.0.0
      L0_ut=L_av
C FDA20_SP2_UVR.fgi( 403, 593):������������  �� T
      if(L_asi.and..not.L0_uri) then
         R0_iri=R0_ori
      else
         R0_iri=max(R_(46)-deltat,0.0)
      endif
      L_(48)=R0_iri.gt.0.0
      L0_uri=L_asi
C FDA20_SP2_UVR.fgi( 403, 604):������������  �� T
      if(L_usi.and..not.L0_osi) then
         R0_esi=R0_isi
      else
         R0_esi=max(R_(47)-deltat,0.0)
      endif
      L_(63)=R0_esi.gt.0.0
      L0_osi=L_usi
C FDA20_SP2_UVR.fgi( 403, 615):������������  �� T
      if(L_oti.and..not.L0_iti) then
         R0_ati=R0_eti
      else
         R0_ati=max(R_(48)-deltat,0.0)
      endif
      L_(67)=R0_ati.gt.0.0
      L0_iti=L_oti
C FDA20_SP2_UVR.fgi( 403, 626):������������  �� T
      if(L_ivi.and..not.L0_evi) then
         R0_uti=R0_avi
      else
         R0_uti=max(R_(49)-deltat,0.0)
      endif
      L_(73)=R0_uti.gt.0.0
      L0_evi=L_ivi
C FDA20_SP2_UVR.fgi( 403, 637):������������  �� T
      L_emaf=(L_(73).or.L_emaf).and..not.(L_epi)
      L_(15)=.not.L_emaf
C FDA20_SP2_UVR.fgi( 451, 635):RS �������,tunl
      L_amaf=L_emaf
C FDA20_SP2_UVR.fgi( 482, 637):������,unloading_complete
      if(L_exi.and..not.L0_axi) then
         R0_ovi=R0_uvi
      else
         R0_ovi=max(R_(50)-deltat,0.0)
      endif
      L_avife=R0_ovi.gt.0.0
      L0_axi=L_exi
C FDA20_SP2_UVR.fgi( 403, 648):������������  �� T
      if(L_abo.and..not.L0_uxi) then
         R0_ixi=R0_oxi
      else
         R0_ixi=max(R_(51)-deltat,0.0)
      endif
      L_(55)=R0_ixi.gt.0.0
      L0_uxi=L_abo
C FDA20_SP2_UVR.fgi( 403, 659):������������  �� T
      if(L_ido.and..not.L0_edo) then
         R0_ubo=R0_ado
      else
         R0_ubo=max(R_(53)-deltat,0.0)
      endif
      L_(58)=R0_ubo.gt.0.0
      L0_edo=L_ido
C FDA20_SP2_UVR.fgi( 403, 674):������������  �� T
      L_adu = L_(58).OR.L_(55)
C FDA20_SP2_UVR.fgi( 446, 673):���
      if(L_efo.and..not.L0_afo) then
         R0_odo=R0_udo
      else
         R0_odo=max(R_(54)-deltat,0.0)
      endif
      L_utife=R0_odo.gt.0.0
      L0_afo=L_efo
C FDA20_SP2_UVR.fgi( 403, 685):������������  �� T
      L_ubu = L_utife.OR.L_avife
C FDA20_SP2_UVR.fgi( 446, 684):���
      !{Call KN_VLVR(deltat,REAL(R_oxife,4),L_akofe,L_ekofe
C ,R8_otife,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_oxife,4),L_akofe
     &,L_ekofe,R8_otife,C30_axife,
     & L_usike,L_utike,L_uvike,I_afofe,I_efofe,R_orife,
     & REAL(R_asife,4),R_usife,REAL(R_etife,4),
     & R_esife,REAL(R_osife,4),I_edofe,I_ifofe,I_udofe,I_odofe
     &,
     & L_atife,L_isife,L_ivife,L_evife,L_ikofe,
     & L_urife,L_uvife,L_olofe,L_avife,L_utife,L_ilofe,L_ulofe
     &,
     & L_itife,L_ofofe,L_alofe,L_ufofe,L_exife,L_ixife,
     & REAL(R8_ereke,8),REAL(1.0,4),R8_axeke,L_elofe,R_adofe
     &,
     & REAL(R_ovife,4),L_uxife,L_abofe,L_ebofe,L_obofe,L_ubofe
     &,L_ibofe)
      !}

      if(L_ubofe.or.L_obofe.or.L_ibofe.or.L_ebofe.or.L_abofe.or.L_uxife
     &) then      
                  I_idofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idofe = z'40000000'
      endif
C FDA20_vlv.fgi( 123, 176):���� ���������� ���������������� ��������,20FDA23AB001
      L_afur=L_akofe
C FDA20_lamp.fgi( 154, 893):������,20FDA23AB001YV10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adur,L_odur,R_idur,
     & REAL(R_udur,4),L_afur,L_ubur,I_edur)
      !}
C FDA20_lamp.fgi( 152, 878):���������� ������� ���������,20FDA23AB001QH01
      L_ivor=L_ekofe
C FDA20_lamp.fgi( 154, 889):������,20FDA23AB001YV11
      !{
      Call DAT_DISCR_HANDLER(deltat,I_itor,L_avor,R_utor,
     & REAL(R_evor,4),L_ivor,L_etor,I_otor)
      !}
C FDA20_lamp.fgi( 152, 864):���������� ������� ���������,20FDA23AB001QH01_1
      L_imaf=L_ekofe
C FDA20_SP2_UVR.fgi( 482, 653):������,gran_v_closed
      if(L_ako.and..not.L0_ufo) then
         R0_ifo=R0_ofo
      else
         R0_ifo=max(R_(55)-deltat,0.0)
      endif
      L_(64)=R0_ifo.gt.0.0
      L0_ufo=L_ako
C FDA20_SP2_UVR.fgi( 403, 696):������������  �� T
      if(L_alo.and..not.L0_uko) then
         R0_iko=R0_oko
      else
         R0_iko=max(R_(56)-deltat,0.0)
      endif
      L_(68)=R0_iko.gt.0.0
      L0_uko=L_alo
C FDA20_SP2_UVR.fgi( 403, 707):������������  �� T
      if(L_ulo.and..not.L0_olo) then
         R0_elo=R0_ilo
      else
         R0_elo=max(R_(57)-deltat,0.0)
      endif
      L_(74)=R0_elo.gt.0.0
      L0_olo=L_ulo
C FDA20_SP2_UVR.fgi( 403, 718):������������  �� T
      if(L_omo.and..not.L0_imo) then
         R0_amo=R0_emo
      else
         R0_amo=max(R_(58)-deltat,0.0)
      endif
      L_ibu=R0_amo.gt.0.0
      L0_imo=L_omo
C FDA20_SP2_UVR.fgi( 403, 729):������������  �� T
      if(L_ipo.and..not.L0_epo) then
         R0_umo=R0_apo
      else
         R0_umo=max(R_(59)-deltat,0.0)
      endif
      L_(76)=R0_umo.gt.0.0
      L0_epo=L_ipo
C FDA20_SP2_UVR.fgi( 403, 740):������������  �� T
      L_uxo = L_(76).OR.L_(64).OR.L_(63)
C FDA20_SP2_UVR.fgi( 442, 738):���
      if(L_ero.and..not.L0_aro) then
         R0_opo=R0_upo
      else
         R0_opo=max(R_(60)-deltat,0.0)
      endif
      L_(78)=R0_opo.gt.0.0
      L0_aro=L_ero
C FDA20_SP2_UVR.fgi( 403, 751):������������  �� T
      if(L_iso.and..not.L0_aso) then
         R0_oro=R0_uro
      else
         R0_oro=max(R_(61)-deltat,0.0)
      endif
      L_(80)=R0_oro.gt.0.0
      L0_aso=L_iso
C FDA20_SP2_UVR.fgi( 403, 766):������������  �� T
      L_abu = L_(80).OR.L_(74).OR.L_(73).OR.L_(72)
C FDA20_SP2_UVR.fgi( 442, 763):���
      if(L_eto.and..not.L0_ato) then
         R0_oso=R0_uso
      else
         R0_oso=max(R_(62)-deltat,0.0)
      endif
      L_(83)=R0_oso.gt.0.0
      L0_ato=L_eto
C FDA20_SP2_UVR.fgi( 403, 777):������������  �� T
      L_ebu = L_(83).OR.L_(48)
C FDA20_SP2_UVR.fgi( 442, 776):���
      L_(5) = L_idu.AND.L_axo
C FDA20_SP2_UVR.fgi( 414, 795):�
      if(L_(5).and..not.L0_id) then
         R0_ad=R0_ed
      else
         R0_ad=max(R_(2)-deltat,0.0)
      endif
      L_(70)=R0_ad.gt.0.0
      L0_id=L_(5)
C FDA20_SP2_UVR.fgi( 420, 795):������������  �� T
      L_edu = L_(78).OR.L_(70).OR.L_(69).OR.L_(68).OR.L_(67
     &)
C FDA20_SP2_UVR.fgi( 442, 747):���
      L_(4) = L_idu.AND.L_eko
C FDA20_SP2_UVR.fgi( 414, 789):�
      if(L_(4).and..not.L0_u) then
         R0_i=R0_o
      else
         R0_i=max(R_(1)-deltat,0.0)
      endif
      L_(52)=R0_i.gt.0.0
      L0_u=L_(4)
C FDA20_SP2_UVR.fgi( 420, 789):������������  �� T
      L_obu = L_(52).OR.L_(68).OR.L_(67)
C FDA20_SP2_UVR.fgi( 446, 707):���
      L_ubad=L_adad
C FDA20_SP2_UVR.fgi( 184, 153):������,tilter_run
      if(L_omuf) then
          if (L_(108)) then
              I_apuf = 26
              L_abad = .true.
              L_(106) = .false.
          endif
          if (L_atof) then
              L_(106) = .true.
              L_abad = .false.
          endif
          if (I_apuf.ne.26) then
              L_abad = .false.
              L_(106) = .false.
          endif
      else
          L_(106) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 124):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(106)) then
              I_apuf = 27
              L_axu = .true.
              L_(100) = .false.
          endif
          if (L_udof) then
              L_(100) = .true.
              L_axu = .false.
          endif
          if (I_apuf.ne.27) then
              L_axu = .false.
              L_(100) = .false.
          endif
      else
          L_(100) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 113):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(100)) then
              I_apuf = 28
              L_evu = .true.
              L_(98) = .false.
          endif
          if (L_okif) then
              L_(98) = .true.
              L_evu = .false.
          endif
          if (I_apuf.ne.28) then
              L_evu = .false.
              L_(98) = .false.
          endif
      else
          L_(98) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93, 102):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(98)) then
              I_apuf = 29
              L_itu = .true.
              L_(94) = .false.
          endif
          if (L_etu) then
              L_(94) = .true.
              L_itu = .false.
          endif
          if (I_apuf.ne.29) then
              L_itu = .false.
              L_(94) = .false.
          endif
      else
          L_(94) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  91):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(94)) then
              I_apuf = 30
              L_isu = .true.
              L_(93) = .false.
          endif
          if (L_uvef) then
              L_(93) = .true.
              L_isu = .false.
          endif
          if (I_apuf.ne.30) then
              L_isu = .false.
              L_(93) = .false.
          endif
      else
          L_(93) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  80):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(93)) then
              I_apuf = 31
              L_oru = .true.
              L_(92) = .false.
          endif
          if (L_udof) then
              L_(92) = .true.
              L_oru = .false.
          endif
          if (I_apuf.ne.31) then
              L_oru = .false.
              L_(92) = .false.
          endif
      else
          L_(92) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  69):��� ������� ���������,uvr
      if(L_omuf) then
          if (L_(92)) then
              I_apuf = 32
              L_iru = .true.
              L_(90) = .false.
          endif
          if (L_axof) then
              L_(90) = .true.
              L_iru = .false.
          endif
          if (I_apuf.ne.32) then
              L_iru = .false.
              L_(90) = .false.
          endif
      else
          L_(90) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  58):��� ������� ���������,uvr
      if(L_iru.and..not.L0_eru) then
         R0_upu=R0_aru
      else
         R0_upu=max(R_(69)-deltat,0.0)
      endif
      L_(96)=R0_upu.gt.0.0
      L0_eru=L_iru
C FDA20_SP2_UVR.fgi( 160,  58):������������  �� T
      if(L_oru.and..not.L0_et) then
         R0_us=R0_at
      else
         R0_us=max(R_(13)-deltat,0.0)
      endif
      L_(16)=R0_us.gt.0.0
      L0_et=L_oru
C FDA20_SP2_UVR.fgi( 160,  69):������������  �� T
      if(L_isu.and..not.L0_esu) then
         R0_uru=R0_asu
      else
         R0_uru=max(R_(70)-deltat,0.0)
      endif
      L_(105)=R0_uru.gt.0.0
      L0_esu=L_isu
C FDA20_SP2_UVR.fgi( 160,  80):������������  �� T
      if(L_itu.and..not.L0_atu) then
         R0_osu=R0_usu
      else
         R0_osu=max(R_(71)-deltat,0.0)
      endif
      L_orod=R0_osu.gt.0.0
      L0_atu=L_itu
C FDA20_SP2_UVR.fgi( 160,  91):������������  �� T
      if(L_evu.and..not.L0_avu) then
         R0_otu=R0_utu
      else
         R0_otu=max(R_(72)-deltat,0.0)
      endif
      L_(97)=R0_otu.gt.0.0
      L0_avu=L_evu
C FDA20_SP2_UVR.fgi( 160, 102):������������  �� T
      if(L_axu.and..not.L0_uvu) then
         R0_ivu=R0_ovu
      else
         R0_ivu=max(R_(73)-deltat,0.0)
      endif
      L_(99)=R0_ivu.gt.0.0
      L0_uvu=L_axu
C FDA20_SP2_UVR.fgi( 160, 113):������������  �� T
      L_(204) = L_(99).OR.L_(16)
C FDA20_SP2_UVR.fgi( 175, 112):���
      if(L_abad.and..not.L0_oxu) then
         R0_exu=R0_ixu
      else
         R0_exu=max(R_(74)-deltat,0.0)
      endif
      L_(101)=R0_exu.gt.0.0
      L0_oxu=L_abad
C FDA20_SP2_UVR.fgi( 160, 124):������������  �� T
      if(L_ilu.and..not.L0_elu) then
         R0_uku=R0_alu
      else
         R0_uku=max(R_(65)-deltat,0.0)
      endif
      L_(89)=R0_uku.gt.0.0
      L0_elu=L_ilu
C FDA20_SP2_UVR.fgi( 167, 172):������������  �� T
      if(L_ikad.and..not.L0_ekad) then
         R0_ufad=R0_akad
      else
         R0_ufad=max(R_(78)-deltat,0.0)
      endif
      L_(113)=R0_ufad.gt.0.0
      L0_ekad=L_ikad
C FDA20_SP2_UVR.fgi( 167, 189):������������  �� T
      L_(153) = L_(113).OR.L_(89)
C FDA20_SP2_UVR.fgi( 179, 188):���
      L_(110) = L_ikad.AND.L_abuf
C FDA20_SP2_UVR.fgi( 153, 183):�
      if(L_(110).and..not.L0_ofad) then
         R0_efad=R0_ifad
      else
         R0_efad=max(R_(77)-deltat,0.0)
      endif
      L_(125)=R0_efad.gt.0.0
      L0_ofad=L_(110)
C FDA20_SP2_UVR.fgi( 167, 183):������������  �� T
      L_orad=L_atad
C FDA20_SP2_UVR.fgi( 184, 228):������,mixing_requ
      if(L_atad.and..not.L0_erad) then
         R0_upad=R0_arad
      else
         R0_upad=max(R_(84)-deltat,0.0)
      endif
      L_(116)=R0_upad.gt.0.0
      L0_erad=L_atad
C FDA20_SP2_UVR.fgi( 137, 223):������������  �� T
      L_asad=(L_(116).or.L_asad).and..not.(L_(119))
      L_esad=.not.L_asad
C FDA20_SP2_UVR.fgi( 155, 221):RS �������,ABC
      L_urad=L_asad
C FDA20_SP2_UVR.fgi( 184, 223):������,abc_run
      if(.not.L_esad) then
         R0_umad=0.0
      elseif(.not.L0_apad) then
         R0_umad=R0_omad
      else
         R0_umad=max(R_(82)-deltat,0.0)
      endif
      L_usad=L_esad.and.R0_umad.le.0.0
      L0_apad=L_esad
C FDA20_SP2_UVR.fgi( 166, 211):�������� ��������� ������
      L_(114) = L_atad.AND.L_okad
C FDA20_SP2_UVR.fgi( 153, 199):�
      if(L_(114).and..not.L0_elad) then
         R0_ukad=R0_alad
      else
         R0_ukad=max(R_(79)-deltat,0.0)
      endif
      L_(220)=R0_ukad.gt.0.0
      L0_elad=L_(114)
C FDA20_SP2_UVR.fgi( 167, 199):������������  �� T
      if(L_atad.and..not.L0_ulad) then
         R0_ilad=R0_olad
      else
         R0_ilad=max(R_(80)-deltat,0.0)
      endif
      L_urod=R0_ilad.gt.0.0
      L0_ulad=L_atad
C FDA20_SP2_UVR.fgi( 167, 205):������������  �� T
      if(L_ise.and..not.L0_ese) then
         R0_ure=R0_ase
      else
         R0_ure=max(R_(31)-deltat,0.0)
      endif
      L_(157)=R0_ure.gt.0.0
      L0_ese=L_ise
C FDA20_SP2_UVR.fgi( 144, 250):������������  �� T
      if(L_ovad.and..not.L0_evad) then
         R0_utad=R0_avad
      else
         R0_utad=max(R_(86)-deltat,0.0)
      endif
      L_(123)=R0_utad.gt.0.0
      L0_evad=L_ovad
C FDA20_SP2_UVR.fgi( 144, 277):������������  �� T
      L_(129) = L_(123).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 538, 633):���
      if(L_osed.and..not.L0_ised) then
         R0_ased=R0_esed
      else
         R0_ased=max(R_(96)-deltat,0.0)
      endif
      L_(152)=R0_ased.gt.0.0
      L0_ised=L_osed
C FDA20_SP2_UVR.fgi( 174, 340):������������  �� T
      L_(140) = L_osed.AND.L_uxof
C FDA20_SP2_UVR.fgi( 160, 334):�
      if(L_(140).and..not.L0_ured) then
         R0_ired=R0_ored
      else
         R0_ired=max(R_(95)-deltat,0.0)
      endif
      L_(217)=R0_ired.gt.0.0
      L0_ured=L_(140)
C FDA20_SP2_UVR.fgi( 174, 334):������������  �� T
      L_(139) = L_osed.AND.L_ulod
C FDA20_SP2_UVR.fgi( 160, 328):�
      if(L_(139).and..not.L0_ered) then
         R0_uped=R0_ared
      else
         R0_uped=max(R_(94)-deltat,0.0)
      endif
      L_(151)=R0_uped.gt.0.0
      L0_ered=L_(139)
C FDA20_SP2_UVR.fgi( 174, 328):������������  �� T
      L_(138) = L_osed.AND.L_oxof
C FDA20_SP2_UVR.fgi( 160, 322):�
      if(L_(138).and..not.L0_oped) then
         R0_eped=R0_iped
      else
         R0_eped=max(R_(93)-deltat,0.0)
      endif
      L_(214)=R0_eped.gt.0.0
      L0_oped=L_(138)
C FDA20_SP2_UVR.fgi( 174, 322):������������  �� T
      L_(137) = L_osed.AND.L_olod
C FDA20_SP2_UVR.fgi( 160, 316):�
      if(L_(137).and..not.L0_aped) then
         R0_omed=R0_umed
      else
         R0_omed=max(R_(92)-deltat,0.0)
      endif
      L_(150)=R0_omed.gt.0.0
      L0_aped=L_(137)
C FDA20_SP2_UVR.fgi( 174, 316):������������  �� T
      L_(136) = L_osed.AND.L_ixof
C FDA20_SP2_UVR.fgi( 160, 310):�
      if(L_(136).and..not.L0_imed) then
         R0_amed=R0_emed
      else
         R0_amed=max(R_(91)-deltat,0.0)
      endif
      L_(211)=R0_amed.gt.0.0
      L0_imed=L_(136)
C FDA20_SP2_UVR.fgi( 174, 310):������������  �� T
      L_(135) = L_osed.AND.L_ilod
C FDA20_SP2_UVR.fgi( 160, 304):�
      if(L_(135).and..not.L0_uled) then
         R0_iled=R0_oled
      else
         R0_iled=max(R_(90)-deltat,0.0)
      endif
      L_(149)=R0_iled.gt.0.0
      L0_uled=L_(135)
C FDA20_SP2_UVR.fgi( 174, 304):������������  �� T
      L_(134) = L_osed.AND.L_exof
C FDA20_SP2_UVR.fgi( 160, 298):�
      if(L_(134).and..not.L0_eled) then
         R0_uked=R0_aled
      else
         R0_uked=max(R_(89)-deltat,0.0)
      endif
      L_(208)=R0_uked.gt.0.0
      L0_eled=L_(134)
C FDA20_SP2_UVR.fgi( 174, 298):������������  �� T
      L_(133) = L_osed.AND.L_elod
C FDA20_SP2_UVR.fgi( 160, 292):�
      if(L_(133).and..not.L0_oked) then
         R0_eked=R0_iked
      else
         R0_eked=max(R_(88)-deltat,0.0)
      endif
      L_(148)=R0_eked.gt.0.0
      L0_oked=L_(133)
C FDA20_SP2_UVR.fgi( 174, 292):������������  �� T
      L_(132) = L_osed.AND.L_axof
C FDA20_SP2_UVR.fgi( 160, 286):�
      if(L_(132).and..not.L0_aked) then
         R0_ofed=R0_ufed
      else
         R0_ofed=max(R_(87)-deltat,0.0)
      endif
      L_(205)=R0_ofed.gt.0.0
      L0_aked=L_(132)
C FDA20_SP2_UVR.fgi( 174, 286):������������  �� T
      L_okid=L_ikid
C FDA20_SP2_UVR.fgi( 194, 351):������,box5meas
      L_(239) = L_etud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 108):�
      L_(240) = L_itud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 112):�
      L_(241) = L_otud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 116):�
      L_(242) = L_utud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 120):�
      L_(243) = L_avud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 124):�
      L_(244) = L_evud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 128):�
      L_(245) = L_ivud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 132):�
      L_(246) = L_ovud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 136):�
      if(L_(246)) then
         R_(143)=R_oxaf
      else
         R_(143)=R_(144)
      endif
C FDA20_SP2_UVR.fgi( 543, 136):���� RE IN LO CH7
      if(L_(245)) then
         R_(142)=R_ixaf
      else
         R_(142)=R_(143)
      endif
C FDA20_SP2_UVR.fgi( 539, 132):���� RE IN LO CH7
      if(L_(244)) then
         R_(141)=R_exaf
      else
         R_(141)=R_(142)
      endif
C FDA20_SP2_UVR.fgi( 535, 128):���� RE IN LO CH7
      if(L_(243)) then
         R_(140)=R_axaf
      else
         R_(140)=R_(141)
      endif
C FDA20_SP2_UVR.fgi( 531, 124):���� RE IN LO CH7
      if(L_(242)) then
         R_(139)=R_uvaf
      else
         R_(139)=R_(140)
      endif
C FDA20_SP2_UVR.fgi( 527, 120):���� RE IN LO CH7
      if(L_(241)) then
         R_(138)=R_ovaf
      else
         R_(138)=R_(139)
      endif
C FDA20_SP2_UVR.fgi( 523, 116):���� RE IN LO CH7
      if(L_(240)) then
         R_(137)=R_ivaf
      else
         R_(137)=R_(138)
      endif
C FDA20_SP2_UVR.fgi( 519, 112):���� RE IN LO CH7
      if(L_(239)) then
         R_(136)=R_evaf
      else
         R_(136)=R_(137)
      endif
C FDA20_SP2_UVR.fgi( 515, 108):���� RE IN LO CH7
      L_(238) = L_atud.AND.L_okid
C FDA20_SP2_UVR.fgi( 503, 104):�
      if(L_(238)) then
         R_erof=R_avaf
      else
         R_erof=R_(136)
      endif
C FDA20_SP2_UVR.fgi( 511, 104):���� RE IN LO CH7
      L_(237)=R_erof.gt.R_(126)
C FDA20_SP2_UVR.fgi( 533, 107):���������� >
      !{
      Call DAT_ANA_HANDLER(deltat,R_apof,R_usof,REAL(1,4)
     &,
     & REAL(R_opof,4),REAL(R_upof,4),
     & REAL(R_umof,4),REAL(R_omof,4),I_osof,
     & REAL(R_irof,4),L_orof,REAL(R_urof,4),L_asof,L_esof
     &,R_arof,
     & REAL(R_ipof,4),REAL(R_epof,4),L_isof,REAL(R_erof,4
     &))
      !}
C FDA20_SP2_UVR.fgi( 564,  93):���������� �������,fda_uvr_w2
      L_opaf = L_(238).OR.L_(239).OR.L_(240).OR.L_(241).OR.L_
     &(242).OR.L_(243).OR.L_(244).OR.L_(245).OR.L_(246)
C FDA20_SP2_UVR.fgi( 549, 149):���
      L_ipaf = L_opaf.AND.L_(237)
C FDA20_SP2_UVR.fgi( 555, 122):�
      L_epaf = L_opaf.AND.(.NOT.L_(237))
C FDA20_SP2_UVR.fgi( 555, 118):�
      if(L_umid) then
         R0_iv=0.0
      elseif(L0_ov) then
         R0_iv=R0_ev
      else
         R0_iv=max(R_(15)-deltat,0.0)
      endif
      L_(166)=L_umid.or.R0_iv.gt.0.0
      L0_ov=L_umid
C FDA20_SP2_UVR.fgi( 123, 389):�������� ������� ������
      if(L_(166).and..not.L0_omid) then
         R0_emid=R0_imid
      else
         R0_emid=max(R_(104)-deltat,0.0)
      endif
      L_(165)=R0_emid.gt.0.0
      L0_omid=L_(166)
C FDA20_SP2_UVR.fgi( 169, 389):������������  �� T
      L_(161) = L_(166).AND.L_ilid
C FDA20_SP2_UVR.fgi( 160, 374):�
      if(L_(161).and..not.L0_elid) then
         R0_ukid=R0_alid
      else
         R0_ukid=max(R_(102)-deltat,0.0)
      endif
      L_(222)=R0_ukid.gt.0.0
      L0_elid=L_(161)
C FDA20_SP2_UVR.fgi( 169, 374):������������  �� T
      L_(143) = L_(166).AND.L_ited
C FDA20_SP2_UVR.fgi( 160, 368):�
      if(L_(143).and..not.L0_eted) then
         R0_used=R0_ated
      else
         R0_used=max(R_(97)-deltat,0.0)
      endif
      L_(154)=R0_used.gt.0.0
      L0_eted=L_(143)
C FDA20_SP2_UVR.fgi( 169, 368):������������  �� T
      L_(17) = L_(166).AND.L_ebuf
C FDA20_SP2_UVR.fgi( 160, 362):�
      if(L_(17).and..not.L0_ade) then
         R0_obe=R0_ube
      else
         R0_obe=max(R_(19)-deltat,0.0)
      endif
      L_(21)=R0_obe.gt.0.0
      L0_ade=L_(17)
C FDA20_SP2_UVR.fgi( 169, 362):������������  �� T
      L_(126) = L_(165).OR.L_(21)
C FDA20_SP2_UVR.fgi( 180, 388):���
      if(.not.L_(166)) then
         R0_eke=0.0
      elseif(.not.L0_ike) then
         R0_eke=R0_ake
      else
         R0_eke=max(R_(23)-deltat,0.0)
      endif
      L_(163)=L_(166).and.R0_eke.le.0.0
      L0_ike=L_(166)
C FDA20_SP2_UVR.fgi( 143, 382):�������� ��������� ������
      L_(164) = L_(166).AND.L_(163).AND.L_ivif
C FDA20_SP2_UVR.fgi( 160, 382):�
      if(L_(164).and..not.L0_amid) then
         R0_olid=R0_ulid
      else
         R0_olid=max(R_(103)-deltat,0.0)
      endif
      L_(162)=R0_olid.gt.0.0
      L0_amid=L_(164)
C FDA20_SP2_UVR.fgi( 169, 382):������������  �� T
      if(L_asid) then
         R0_ax=0.0
      elseif(L0_ex) then
         R0_ax=R0_uv
      else
         R0_ax=max(R_(16)-deltat,0.0)
      endif
      L_(174)=L_asid.or.R0_ax.gt.0.0
      L0_ex=L_asid
C FDA20_SP2_UVR.fgi( 123, 422):�������� ������� ������
      if(L_(174).and..not.L0_urid) then
         R0_irid=R0_orid
      else
         R0_irid=max(R_(107)-deltat,0.0)
      endif
      L_(173)=R0_irid.gt.0.0
      L0_urid=L_(174)
C FDA20_SP2_UVR.fgi( 169, 422):������������  �� T
      L_(169) = L_(174).AND.L_opid
C FDA20_SP2_UVR.fgi( 160, 408):�
      if(L_(169).and..not.L0_ipid) then
         R0_apid=R0_epid
      else
         R0_apid=max(R_(105)-deltat,0.0)
      endif
      L_(224)=R0_apid.gt.0.0
      L0_ipid=L_(169)
C FDA20_SP2_UVR.fgi( 169, 408):������������  �� T
      L_(144) = L_(174).AND.L_eved
C FDA20_SP2_UVR.fgi( 160, 402):�
      if(L_(144).and..not.L0_aved) then
         R0_oted=R0_uted
      else
         R0_oted=max(R_(98)-deltat,0.0)
      endif
      L_(155)=R0_oted.gt.0.0
      L0_aved=L_(144)
C FDA20_SP2_UVR.fgi( 169, 402):������������  �� T
      L_(18) = L_(174).AND.L_ibuf
C FDA20_SP2_UVR.fgi( 160, 396):�
      if(L_(18).and..not.L0_ode) then
         R0_ede=R0_ide
      else
         R0_ede=max(R_(20)-deltat,0.0)
      endif
      L_(22)=R0_ede.gt.0.0
      L0_ode=L_(18)
C FDA20_SP2_UVR.fgi( 169, 396):������������  �� T
      L_(127) = L_(173).OR.L_(22)
C FDA20_SP2_UVR.fgi( 180, 421):���
      if(.not.L_(174)) then
         R0_uke=0.0
      elseif(.not.L0_ale) then
         R0_uke=R0_oke
      else
         R0_uke=max(R_(24)-deltat,0.0)
      endif
      L_(171)=L_(174).and.R0_uke.le.0.0
      L0_ale=L_(174)
C FDA20_SP2_UVR.fgi( 143, 416):�������� ��������� ������
      L_(172) = L_(174).AND.L_(171).AND.L_exif
C FDA20_SP2_UVR.fgi( 160, 416):�
      if(L_(172).and..not.L0_erid) then
         R0_upid=R0_arid
      else
         R0_upid=max(R_(106)-deltat,0.0)
      endif
      L_(170)=R0_upid.gt.0.0
      L0_erid=L_(172)
C FDA20_SP2_UVR.fgi( 169, 416):������������  �� T
      if(L_evid) then
         R0_ox=0.0
      elseif(L0_ux) then
         R0_ox=R0_ix
      else
         R0_ox=max(R_(17)-deltat,0.0)
      endif
      L_(182)=L_evid.or.R0_ox.gt.0.0
      L0_ux=L_evid
C FDA20_SP2_UVR.fgi( 123, 456):�������� ������� ������
      if(L_(182).and..not.L0_avid) then
         R0_otid=R0_utid
      else
         R0_otid=max(R_(110)-deltat,0.0)
      endif
      L_(181)=R0_otid.gt.0.0
      L0_avid=L_(182)
C FDA20_SP2_UVR.fgi( 169, 456):������������  �� T
      L_(177) = L_(182).AND.L_usid
C FDA20_SP2_UVR.fgi( 160, 442):�
      if(L_(177).and..not.L0_osid) then
         R0_esid=R0_isid
      else
         R0_esid=max(R_(108)-deltat,0.0)
      endif
      L_(226)=R0_esid.gt.0.0
      L0_osid=L_(177)
C FDA20_SP2_UVR.fgi( 169, 442):������������  �� T
      L_(145) = L_(182).AND.L_axed
C FDA20_SP2_UVR.fgi( 160, 436):�
      if(L_(145).and..not.L0_uved) then
         R0_ived=R0_oved
      else
         R0_ived=max(R_(99)-deltat,0.0)
      endif
      L_(156)=R0_ived.gt.0.0
      L0_uved=L_(145)
C FDA20_SP2_UVR.fgi( 169, 436):������������  �� T
      L_(19) = L_(182).AND.L_obuf
C FDA20_SP2_UVR.fgi( 160, 430):�
      if(L_(19).and..not.L0_efe) then
         R0_ude=R0_afe
      else
         R0_ude=max(R_(21)-deltat,0.0)
      endif
      L_(23)=R0_ude.gt.0.0
      L0_efe=L_(19)
C FDA20_SP2_UVR.fgi( 169, 430):������������  �� T
      L_(128) = L_(181).OR.L_(23)
C FDA20_SP2_UVR.fgi( 180, 455):���
      if(.not.L_(182)) then
         R0_ile=0.0
      elseif(.not.L0_ole) then
         R0_ile=R0_ele
      else
         R0_ile=max(R_(25)-deltat,0.0)
      endif
      L_(179)=L_(182).and.R0_ile.le.0.0
      L0_ole=L_(182)
C FDA20_SP2_UVR.fgi( 143, 450):�������� ��������� ������
      L_(180) = L_(182).AND.L_(179).AND.L_abof
C FDA20_SP2_UVR.fgi( 160, 450):�
      if(L_(180).and..not.L0_itid) then
         R0_atid=R0_etid
      else
         R0_atid=max(R_(109)-deltat,0.0)
      endif
      L_(178)=R0_atid.gt.0.0
      L0_itid=L_(180)
C FDA20_SP2_UVR.fgi( 169, 450):������������  �� T
      if(L_ibod) then
         R0_ebe=0.0
      elseif(L0_ibe) then
         R0_ebe=R0_abe
      else
         R0_ebe=max(R_(18)-deltat,0.0)
      endif
      L_(191)=L_ibod.or.R0_ebe.gt.0.0
      L0_ibe=L_ibod
C FDA20_SP2_UVR.fgi( 123, 491):�������� ������� ������
      if(L_(191).and..not.L0_ebod) then
         R0_uxid=R0_abod
      else
         R0_uxid=max(R_(113)-deltat,0.0)
      endif
      L_(190)=R0_uxid.gt.0.0
      L0_ebod=L_(191)
C FDA20_SP2_UVR.fgi( 169, 491):������������  �� T
      if(.not.L_(191)) then
         R0_ame=0.0
      elseif(.not.L0_eme) then
         R0_ame=R0_ule
      else
         R0_ame=max(R_(26)-deltat,0.0)
      endif
      L_(187)=L_(191).and.R0_ame.le.0.0
      L0_eme=L_(191)
C FDA20_SP2_UVR.fgi( 143, 485):�������� ��������� ������
      L_(188) = L_(191).AND.L_(187).AND.L_ubof
C FDA20_SP2_UVR.fgi( 160, 485):�
      if(L_(188).and..not.L0_oxid) then
         R0_exid=R0_ixid
      else
         R0_exid=max(R_(112)-deltat,0.0)
      endif
      L_(186)=R0_exid.gt.0.0
      L0_oxid=L_(188)
C FDA20_SP2_UVR.fgi( 169, 485):������������  �� T
      L_(185) = L_(191).AND.L_axid
C FDA20_SP2_UVR.fgi( 160, 476):�
      if(L_(185).and..not.L0_uvid) then
         R0_ivid=R0_ovid
      else
         R0_ivid=max(R_(111)-deltat,0.0)
      endif
      L_(228)=R0_ivid.gt.0.0
      L0_uvid=L_(185)
C FDA20_SP2_UVR.fgi( 169, 476):������������  �� T
      L_(146) = L_(191).AND.L_uxed
C FDA20_SP2_UVR.fgi( 160, 470):�
      if(L_(146).and..not.L0_oxed) then
         R0_exed=R0_ixed
      else
         R0_exed=max(R_(100)-deltat,0.0)
      endif
      L_(158)=R0_exed.gt.0.0
      L0_oxed=L_(146)
C FDA20_SP2_UVR.fgi( 169, 470):������������  �� T
      L_(20) = L_(191).AND.L_ubuf
C FDA20_SP2_UVR.fgi( 160, 464):�
      if(L_(20).and..not.L0_ufe) then
         R0_ife=R0_ofe
      else
         R0_ife=max(R_(22)-deltat,0.0)
      endif
      L_(24)=R0_ife.gt.0.0
      L0_ufe=L_(20)
C FDA20_SP2_UVR.fgi( 169, 464):������������  �� T
      L_(189) = L_(190).OR.L_(24)
C FDA20_SP2_UVR.fgi( 180, 490):���
      if(L_emod.and..not.L0_alod) then
         R0_okod=R0_ukod
      else
         R0_okod=max(R_(119)-deltat,0.0)
      endif
      L_(219)=R0_okod.gt.0.0
      L0_alod=L_emod
C FDA20_SP2_UVR.fgi( 174, 525):������������  �� T
      L_(199) = L_emod.AND.L_amod
C FDA20_SP2_UVR.fgi( 160, 521):�
      if(L_(199).and..not.L0_ikod) then
         R0_akod=R0_ekod
      else
         R0_akod=max(R_(118)-deltat,0.0)
      endif
      L_(216)=R0_akod.gt.0.0
      L0_ikod=L_(199)
C FDA20_SP2_UVR.fgi( 170, 521):������������  �� T
      L_(195) = L_emod.AND.L_elod
C FDA20_SP2_UVR.fgi( 160, 505):�
      if(L_(195).and..not.L0_adod) then
         R0_obod=R0_ubod
      else
         R0_obod=max(R_(114)-deltat,0.0)
      endif
      L_(203)=R0_obod.gt.0.0
      L0_adod=L_(195)
C FDA20_SP2_UVR.fgi( 170, 505):������������  �� T
      L_(196) = L_emod.AND.L_ilod
C FDA20_SP2_UVR.fgi( 160, 509):�
      if(L_(196).and..not.L0_odod) then
         R0_edod=R0_idod
      else
         R0_edod=max(R_(115)-deltat,0.0)
      endif
      L_(207)=R0_edod.gt.0.0
      L0_odod=L_(196)
C FDA20_SP2_UVR.fgi( 174, 509):������������  �� T
      L_(197) = L_emod.AND.L_olod
C FDA20_SP2_UVR.fgi( 160, 513):�
      if(L_(197).and..not.L0_efod) then
         R0_udod=R0_afod
      else
         R0_udod=max(R_(116)-deltat,0.0)
      endif
      L_(210)=R0_udod.gt.0.0
      L0_efod=L_(197)
C FDA20_SP2_UVR.fgi( 170, 513):������������  �� T
      L_(198) = L_emod.AND.L_ulod
C FDA20_SP2_UVR.fgi( 160, 517):�
      if(L_(198).and..not.L0_ufod) then
         R0_ifod=R0_ofod
      else
         R0_ifod=max(R_(117)-deltat,0.0)
      endif
      L_(213)=R0_ifod.gt.0.0
      L0_ufod=L_(198)
C FDA20_SP2_UVR.fgi( 174, 517):������������  �� T
      if(L_imod.and..not.L0_ok) then
         R0_ek=R0_ik
      else
         R0_ek=max(R_(5)-deltat,0.0)
      endif
      L_efop=R0_ek.gt.0.0
      L0_ok=L_imod
C FDA20_SP2_UVR.fgi( 119, 536):������������  �� T
      !{
      Call DAT_DISCR_HANDLER(deltat,I_edop,L_udop,R_odop,
     & REAL(R_afop,4),L_efop,L_adop,I_idop)
      !}
C FDA20_lamp.fgi(  74, 432):���������� ������� ���������,20FDA21AL004BQ48
      L_ufak=L_efop
C FDA20_SP2_UVR.fgi( 214, 536):������,bd_u2_close
      L_edak=L_ufak
C FDA20_SP1.fgi( 358, 796):������,20FDA21AL001C26_ulucl
      L_ofak=L_efop
C FDA20_SP2_UVR.fgi( 214, 532):������,bd_carbon_close
      L_odak=L_ofak
C FDA20_SP1.fgi( 358, 767):������,20FDA21AL001C30_ulucl
      L_ifak=L_efop
C FDA20_SP2_UVR.fgi( 214, 528):������,bd_scrap_close
      L_afak=L_ifak
C FDA20_SP1.fgi( 358, 738):������,20FDA21AL001C17_ulucl
      L_okek=L_ul
C FDA20_SP2_UVR.fgi( 214, 566):������,bd_u2_open
      L_idak=L_okek
C FDA20_SP1.fgi( 358, 803):������,20FDA21AL001C26_uluop
      L_uxim=L_okek
C FDA20_SP1.fgi( 358, 807):������,vd1
      !{
      Call DAT_DISCR_HANDLER(deltat,I_uvim,L_ixim,R_exim,
     & REAL(R_oxim,4),L_uxim,L_ovim,I_axim)
      !}
C FDA20_lamp.fgi( 122, 218):���������� ������� ���������,20FDA21AL001KF01BQ31
      if(L_okek.and..not.L0_ikek) then
         R0_akek=R0_ekek
      else
         R0_akek=max(R_(195)-deltat,0.0)
      endif
      L_(320)=R0_akek.gt.0.0
      L0_ikek=L_okek
C FDA20_SP1.fgi( 415, 800):������������  �� T
      if(I_(4).ne.0) then
        iv1=I_(4)
        if(0) then
          if(L_(319).and..not.L0_olek) then
            R0_elek=R0_alek
            R_emek=R_emek+1
          elseif(L_(320).and..not.L0_ulek) then
            R0_elek=R0_alek
            R_emek=R_emek-1
          elseif(L_(319)) then
            R0_elek=R0_elek-deltat
            if(R0_elek.lt.0.0) then
              R0_elek=R0_alek
              R_emek=R_emek+1
            endif
          elseif(L_(320)) then
            R0_elek=R0_elek-deltat
            if(R0_elek.lt.0.0) then
              R0_elek=R0_alek
              R_emek=R_emek-1
            endif
          endif
          L0_olek=L_(319)
          L0_ulek=L_(320)
        else
            R0_elek=R0_ukek-R0_ilek
            if(L_(319).and..not.L_(320)) then
               R_emek=R_emek+R0_elek*deltat/max(deltat,R0_alek
     &)
            elseif(L_(320).and..not.L_(319)) then
               R_emek=R_emek-R0_elek*deltat/max(deltat,R0_alek
     &)
            endif   
         endif
         if(0) then
            if(R_emek.gt.R0_ukek) then
               R_emek=R0_ilek
            elseif(R_emek.lt.R0_ilek) then
               R_emek=R0_ukek
            endif
         else
            if(R_emek.gt.R0_ukek) then
               R_emek=R0_ukek
            elseif(R_emek.lt.R0_ilek) then
               R_emek=R0_ilek
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 799):��������� ���������
      L_il=L_ul
C FDA20_SP2_UVR.fgi( 214, 550):������,20FDA20UNL01
      L_odek=L_ul
C FDA20_SP2_UVR.fgi( 214, 562):������,bd_carbon_open
      L_udak=L_odek
C FDA20_SP1.fgi( 358, 774):������,20FDA21AL001C30_uluop
      L_apol=L_odek
C FDA20_SP1.fgi( 358, 778):������,vd3
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amol,L_omol,R_imol,
     & REAL(R_umol,4),L_apol,L_ulol,I_emol)
      !}
C FDA20_lamp.fgi(  90, 394):���������� ������� ���������,20FDA21AL004KF03Q41
      if(L_odek.and..not.L0_idek) then
         R0_adek=R0_edek
      else
         R0_adek=max(R_(194)-deltat,0.0)
      endif
      L_(317)=R0_adek.gt.0.0
      L0_idek=L_odek
C FDA20_SP1.fgi( 415, 771):������������  �� T
      if(I_(3).ne.0) then
        iv1=I_(3)
        if(0) then
          if(L_(316).and..not.L0_ofek) then
            R0_efek=R0_afek
            R_amek=R_amek+1
          elseif(L_(317).and..not.L0_ufek) then
            R0_efek=R0_afek
            R_amek=R_amek-1
          elseif(L_(316)) then
            R0_efek=R0_efek-deltat
            if(R0_efek.lt.0.0) then
              R0_efek=R0_afek
              R_amek=R_amek+1
            endif
          elseif(L_(317)) then
            R0_efek=R0_efek-deltat
            if(R0_efek.lt.0.0) then
              R0_efek=R0_afek
              R_amek=R_amek-1
            endif
          endif
          L0_ofek=L_(316)
          L0_ufek=L_(317)
        else
            R0_efek=R0_udek-R0_ifek
            if(L_(316).and..not.L_(317)) then
               R_amek=R_amek+R0_efek*deltat/max(deltat,R0_afek
     &)
            elseif(L_(317).and..not.L_(316)) then
               R_amek=R_amek-R0_efek*deltat/max(deltat,R0_afek
     &)
            endif   
         endif
         if(0) then
            if(R_amek.gt.R0_udek) then
               R_amek=R0_ifek
            elseif(R_amek.lt.R0_ifek) then
               R_amek=R0_udek
            endif
         else
            if(R_amek.gt.R0_udek) then
               R_amek=R0_udek
            elseif(R_amek.lt.R0_ifek) then
               R_amek=R0_ifek
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 770):��������� ���������
      L_oxak=L_ul
C FDA20_SP2_UVR.fgi( 214, 558):������,bd_scrap_open
      L_efak=L_oxak
C FDA20_SP1.fgi( 358, 745):������,20FDA21AL001C17_uluop
      L_urol=L_oxak
C FDA20_SP1.fgi( 358, 749):������,vd2
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upol,L_irol,R_erol,
     & REAL(R_orol,4),L_urol,L_opol,I_arol)
      !}
C FDA20_lamp.fgi(  90, 332):���������� ������� ���������,20FDA21AL001KF02BQ31
      if(L_oxak.and..not.L0_ixak) then
         R0_axak=R0_exak
      else
         R0_axak=max(R_(193)-deltat,0.0)
      endif
      L_(314)=R0_axak.gt.0.0
      L0_ixak=L_oxak
C FDA20_SP1.fgi( 415, 742):������������  �� T
      if(I_(2).ne.0) then
        iv1=I_(2)
        if(0) then
          if(L_(313).and..not.L0_obek) then
            R0_ebek=R0_abek
            R_imek=R_imek+1
          elseif(L_(314).and..not.L0_ubek) then
            R0_ebek=R0_abek
            R_imek=R_imek-1
          elseif(L_(313)) then
            R0_ebek=R0_ebek-deltat
            if(R0_ebek.lt.0.0) then
              R0_ebek=R0_abek
              R_imek=R_imek+1
            endif
          elseif(L_(314)) then
            R0_ebek=R0_ebek-deltat
            if(R0_ebek.lt.0.0) then
              R0_ebek=R0_abek
              R_imek=R_imek-1
            endif
          endif
          L0_obek=L_(313)
          L0_ubek=L_(314)
        else
            R0_ebek=R0_uxak-R0_ibek
            if(L_(313).and..not.L_(314)) then
               R_imek=R_imek+R0_ebek*deltat/max(deltat,R0_abek
     &)
            elseif(L_(314).and..not.L_(313)) then
               R_imek=R_imek-R0_ebek*deltat/max(deltat,R0_abek
     &)
            endif   
         endif
         if(0) then
            if(R_imek.gt.R0_uxak) then
               R_imek=R0_ibek
            elseif(R_imek.lt.R0_ibek) then
               R_imek=R0_uxak
            endif
         else
            if(R_imek.gt.R0_uxak) then
               R_imek=R0_uxak
            elseif(R_imek.lt.R0_ibek) then
               R_imek=R0_ibek
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 741):��������� ���������
      L_irop=L_ul
C FDA20_SP2_UVR.fgi( 214, 554):������,bd_pu_open
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ipop,L_arop,R_upop,
     & REAL(R_erop,4),L_irop,L_epop,I_opop)
      !}
C FDA20_lamp.fgi(  74, 420):���������� ������� ���������,20FDA21AL004BQ47
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utip,L_ivip,R_evip,
     & REAL(R_ovip,4),L_irop,L_otip,I_avip)
      !}
C FDA20_lamp.fgi( 122, 420):���������� ������� ���������,20FDA21AL004KF01Q41
      if(L_irop.and..not.L0_otak) then
         R0_etak=R0_itak
      else
         R0_etak=max(R_(192)-deltat,0.0)
      endif
      L_(311)=R0_etak.gt.0.0
      L0_otak=L_irop
C FDA20_SP1.fgi( 415, 713):������������  �� T
      if(I_(1).ne.0) then
        iv1=I_(1)
        if(0) then
          if(L_(310).and..not.L0_ovak) then
            R0_evak=R0_avak
            R_omek=R_omek+1
          elseif(L_(311).and..not.L0_uvak) then
            R0_evak=R0_avak
            R_omek=R_omek-1
          elseif(L_(310)) then
            R0_evak=R0_evak-deltat
            if(R0_evak.lt.0.0) then
              R0_evak=R0_avak
              R_omek=R_omek+1
            endif
          elseif(L_(311)) then
            R0_evak=R0_evak-deltat
            if(R0_evak.lt.0.0) then
              R0_evak=R0_avak
              R_omek=R_omek-1
            endif
          endif
          L0_ovak=L_(310)
          L0_uvak=L_(311)
        else
            R0_evak=R0_utak-R0_ivak
            if(L_(310).and..not.L_(311)) then
               R_omek=R_omek+R0_evak*deltat/max(deltat,R0_avak
     &)
            elseif(L_(311).and..not.L_(310)) then
               R_omek=R_omek-R0_evak*deltat/max(deltat,R0_avak
     &)
            endif   
         endif
         if(0) then
            if(R_omek.gt.R0_utak) then
               R_omek=R0_ivak
            elseif(R_omek.lt.R0_ivak) then
               R_omek=R0_utak
            endif
         else
            if(R_omek.gt.R0_utak) then
               R_omek=R0_utak
            elseif(R_omek.lt.R0_ivak) then
               R_omek=R0_ivak
            endif
         endif
      endif
C FDA20_SP1.fgi( 426, 712):��������� ���������
      if(L_utod.and..not.L0_itod) then
         R0_atod=R0_etod
      else
         R0_atod=max(R_(120)-deltat,0.0)
      endif
      L_(230)=R0_atod.gt.0.0
      L0_itod=L_utod
C FDA20_SP2_UVR.fgi( 167, 564):������������  �� T
      L_abid = L_(230).OR.L_(162)
C FDA20_SP2_UVR.fgi( 544, 683):���
      L_(25) = L_utod.AND.L_usod
C FDA20_SP2_UVR.fgi( 156, 558):�
      if(L_(25).and..not.L0_ume) then
         R0_ime=R0_ome
      else
         R0_ime=max(R_(27)-deltat,0.0)
      endif
      L_asod=R0_ime.gt.0.0
      L0_ume=L_(25)
C FDA20_SP2_UVR.fgi( 167, 558):������������  �� T
      if(L_axod.and..not.L0_ovod) then
         R0_evod=R0_ivod
      else
         R0_evod=max(R_(121)-deltat,0.0)
      endif
      L_(232)=R0_evod.gt.0.0
      L0_ovod=L_axod
C FDA20_SP2_UVR.fgi( 167, 576):������������  �� T
      L_ebid = L_(232).OR.L_(170)
C FDA20_SP2_UVR.fgi( 544, 687):���
      L_(26) = L_axod.AND.L_avod
C FDA20_SP2_UVR.fgi( 156, 570):�
      if(L_(26).and..not.L0_ipe) then
         R0_ape=R0_epe
      else
         R0_ape=max(R_(28)-deltat,0.0)
      endif
      L_esod=R0_ape.gt.0.0
      L0_ipe=L_(26)
C FDA20_SP2_UVR.fgi( 167, 570):������������  �� T
      if(L_ebud.and..not.L0_uxod) then
         R0_ixod=R0_oxod
      else
         R0_ixod=max(R_(122)-deltat,0.0)
      endif
      L_(234)=R0_ixod.gt.0.0
      L0_uxod=L_ebud
C FDA20_SP2_UVR.fgi( 167, 588):������������  �� T
      L_ibid = L_(234).OR.L_(178)
C FDA20_SP2_UVR.fgi( 544, 691):���
      L_(27) = L_ebud.AND.L_exod
C FDA20_SP2_UVR.fgi( 156, 582):�
      if(L_(27).and..not.L0_are) then
         R0_ope=R0_upe
      else
         R0_ope=max(R_(29)-deltat,0.0)
      endif
      L_isod=R0_ope.gt.0.0
      L0_are=L_(27)
C FDA20_SP2_UVR.fgi( 167, 582):������������  �� T
      if(L_itef.and..not.L0_atef) then
         R0_osef=R0_usef
      else
         R0_osef=max(R_(156)-deltat,0.0)
      endif
      L_(247)=R0_osef.gt.0.0
      L0_atef=L_itef
C FDA20_SP2_UVR.fgi( 167, 600):������������  �� T
      L_obid = L_(247).OR.L_(186)
C FDA20_SP2_UVR.fgi( 544, 695):���
      L_(28) = L_itef.AND.L_isef
C FDA20_SP2_UVR.fgi( 156, 594):�
      if(L_(28).and..not.L0_ore) then
         R0_ere=R0_ire
      else
         R0_ere=max(R_(30)-deltat,0.0)
      endif
      L_osod=R0_ere.gt.0.0
      L0_ore=L_(28)
C FDA20_SP2_UVR.fgi( 167, 594):������������  �� T
      if(L_ikif.and..not.L0_edif) then
         R0_ubif=R0_adif
      else
         R0_ubif=max(R_(159)-deltat,0.0)
      endif
      L_ekif=R0_ubif.gt.0.0
      L0_edif=L_ikif
C FDA20_SP2_UVR.fgi( 119, 616):������������  �� T
      L_akif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 616):������,20FDA20TRAN01_NEXT
      L_ufif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 614):������,20FDA20TRAN02_NEXT
      L_ofif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 612):������,20FDA20TRAN03_NEXT
      L_ifif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 610):������,20FDA20TRAN04_NEXT
      L_efif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 608):������,20FDA20TRAN05_NEXT
      L_afif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 606):������,20FDA20TRAN06_NEXT
      L_udif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 604):������,20FDA20TRAN07_NEXT
      L_odif=L_ekif
C FDA20_SP2_UVR.fgi( 252, 602):������,20FDA20TRAN08_NEXT
      L_idif = L_ekif.OR.L_(105)
C FDA20_SP2_UVR.fgi( 238, 597):���
      if(L_odof.and..not.L0_idof) then
         R0_adof=R0_edof
      else
         R0_adof=max(R_(169)-deltat,0.0)
      endif
      L_(278)=R0_adof.gt.0.0
      L0_idof=L_odof
C FDA20_SP2_UVR.fgi( 174, 659):������������  �� T
      L_obed = L_(278).OR.L_(189).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 626):���
      L_(273) = L_odof.AND.L_exif
C FDA20_SP2_UVR.fgi( 160, 647):�
      if(L_(273).and..not.L0_axif) then
         R0_ovif=R0_uvif
      else
         R0_ovif=max(R_(166)-deltat,0.0)
      endif
      L_(272)=R0_ovif.gt.0.0
      L0_axif=L_(273)
C FDA20_SP2_UVR.fgi( 170, 647):������������  �� T
      L_abed = L_(272).OR.L_(126).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 608):���
      L_(271) = L_odof.AND.L_ivif
C FDA20_SP2_UVR.fgi( 160, 643):�
      if(L_(271).and..not.L0_evif) then
         R0_utif=R0_avif
      else
         R0_utif=max(R_(165)-deltat,0.0)
      endif
      L_(270)=R0_utif.gt.0.0
      L0_evif=L_(271)
C FDA20_SP2_UVR.fgi( 174, 643):������������  �� T
      L_uxad = L_(125).OR.L_(270).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 602):���
      L_(261) = L_odof.AND.L_ipif
C FDA20_SP2_UVR.fgi( 160, 623):�
      if(L_(261).and..not.L0_epif) then
         R0_umif=R0_apif
      else
         R0_umif=max(R_(160)-deltat,0.0)
      endif
      L_(260)=R0_umif.gt.0.0
      L0_epif=L_(261)
C FDA20_SP2_UVR.fgi( 170, 623):������������  �� T
      L_(263) = L_odof.AND.L_erif
C FDA20_SP2_UVR.fgi( 160, 627):�
      if(L_(263).and..not.L0_arif) then
         R0_opif=R0_upif
      else
         R0_opif=max(R_(161)-deltat,0.0)
      endif
      L_(262)=R0_opif.gt.0.0
      L0_arif=L_(263)
C FDA20_SP2_UVR.fgi( 174, 627):������������  �� T
      L_axad = L_(262).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 585):���
      L_(265) = L_odof.AND.L_asif
C FDA20_SP2_UVR.fgi( 160, 631):�
      if(L_(265).and..not.L0_urif) then
         R0_irif=R0_orif
      else
         R0_irif=max(R_(162)-deltat,0.0)
      endif
      L_(264)=R0_irif.gt.0.0
      L0_urif=L_(265)
C FDA20_SP2_UVR.fgi( 170, 631):������������  �� T
      L_exad = L_(264).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 589):���
      L_(267) = L_odof.AND.L_usif
C FDA20_SP2_UVR.fgi( 160, 635):�
      if(L_(267).and..not.L0_osif) then
         R0_esif=R0_isif
      else
         R0_esif=max(R_(163)-deltat,0.0)
      endif
      L_(266)=R0_esif.gt.0.0
      L0_osif=L_(267)
C FDA20_SP2_UVR.fgi( 174, 635):������������  �� T
      L_ixad = L_(266).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 593):���
      L_(269) = L_odof.AND.L_otif
C FDA20_SP2_UVR.fgi( 160, 639):�
      if(L_(269).and..not.L0_itif) then
         R0_atif=R0_etif
      else
         R0_atif=max(R_(164)-deltat,0.0)
      endif
      L_(268)=R0_atif.gt.0.0
      L0_itif=L_(269)
C FDA20_SP2_UVR.fgi( 170, 639):������������  �� T
      L_oxad = L_(268).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 597):���
      L_(275) = L_odof.AND.L_abof
C FDA20_SP2_UVR.fgi( 160, 651):�
      if(L_(275).and..not.L0_uxif) then
         R0_ixif=R0_oxif
      else
         R0_ixif=max(R_(167)-deltat,0.0)
      endif
      L_(274)=R0_ixif.gt.0.0
      L0_uxif=L_(275)
C FDA20_SP2_UVR.fgi( 174, 651):������������  �� T
      L_ebed = L_(274).OR.L_(127).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 614):���
      L_(277) = L_odof.AND.L_ubof
C FDA20_SP2_UVR.fgi( 160, 655):�
      if(L_(277).and..not.L0_obof) then
         R0_ebof=R0_ibof
      else
         R0_ebof=max(R_(168)-deltat,0.0)
      endif
      L_(276)=R0_ebof.gt.0.0
      L0_obof=L_(277)
C FDA20_SP2_UVR.fgi( 170, 655):������������  �� T
      L_ibed = L_(276).OR.L_(128).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 620):���
      if(L_omif.and..not.L0_avef) then
         R0_otef=R0_utef
      else
         R0_otef=max(R_(157)-deltat,0.0)
      endif
      L_(250)=R0_otef.gt.0.0
      L0_avef=L_omif
C FDA20_SP2_UVR.fgi( 241, 738):������������  �� T
      if(L_afof.and..not.L0_aki) then
         R0_ofi=R0_ufi
      else
         R0_ofi=max(R_(42)-deltat,0.0)
      endif
      L_(229)=R0_ofi.gt.0.0
      L0_aki=L_afof
C FDA20_SP2_UVR.fgi( 174, 717):������������  �� T
      L_irod = L_(229).OR.L_(228)
C FDA20_SP2_UVR.fgi( 542, 817):���
      L_(37) = L_afof.AND.L_eved
C FDA20_SP2_UVR.fgi( 160, 705):�
      if(L_(37).and..not.L0_edi) then
         R0_ubi=R0_adi
      else
         R0_ubi=max(R_(39)-deltat,0.0)
      endif
      L_(223)=R0_ubi.gt.0.0
      L0_edi=L_(37)
C FDA20_SP2_UVR.fgi( 170, 705):������������  �� T
      L_upod = L_(223).OR.L_(222)
C FDA20_SP2_UVR.fgi( 542, 805):���
      L_(36) = L_afof.AND.L_ited
C FDA20_SP2_UVR.fgi( 160, 701):�
      if(L_(36).and..not.L0_obi) then
         R0_ebi=R0_ibi
      else
         R0_ebi=max(R_(38)-deltat,0.0)
      endif
      L_(221)=R0_ebi.gt.0.0
      L0_obi=L_(36)
C FDA20_SP2_UVR.fgi( 174, 701):������������  �� T
      L_opod = L_(221).OR.L_(220).OR.L_(219)
C FDA20_SP2_UVR.fgi( 542, 800):���
      L_(31) = L_afof.AND.L_elod
C FDA20_SP2_UVR.fgi( 160, 681):�
      if(L_(31).and..not.L0_ote) then
         R0_ete=R0_ite
      else
         R0_ete=max(R_(33)-deltat,0.0)
      endif
      L_(206)=R0_ete.gt.0.0
      L0_ote=L_(31)
C FDA20_SP2_UVR.fgi( 170, 681):������������  �� T
      L_omod = L_(206).OR.L_(205).OR.L_(204).OR.L_(203)
C FDA20_SP2_UVR.fgi( 542, 769):���
      L_(32) = L_afof.AND.L_ilod
C FDA20_SP2_UVR.fgi( 160, 685):�
      if(L_(32).and..not.L0_eve) then
         R0_ute=R0_ave
      else
         R0_ute=max(R_(34)-deltat,0.0)
      endif
      L_(209)=R0_ute.gt.0.0
      L0_eve=L_(32)
C FDA20_SP2_UVR.fgi( 174, 685):������������  �� T
      L_umod = L_(209).OR.L_(208).OR.L_(207)
C FDA20_SP2_UVR.fgi( 542, 776):���
      L_(33) = L_afof.AND.L_olod
C FDA20_SP2_UVR.fgi( 160, 689):�
      if(L_(33).and..not.L0_uve) then
         R0_ive=R0_ove
      else
         R0_ive=max(R_(35)-deltat,0.0)
      endif
      L_(212)=R0_ive.gt.0.0
      L0_uve=L_(33)
C FDA20_SP2_UVR.fgi( 170, 689):������������  �� T
      L_apod = L_(212).OR.L_(211).OR.L_(210)
C FDA20_SP2_UVR.fgi( 542, 782):���
      L_(34) = L_afof.AND.L_ulod
C FDA20_SP2_UVR.fgi( 160, 693):�
      if(L_(34).and..not.L0_ixe) then
         R0_axe=R0_exe
      else
         R0_axe=max(R_(36)-deltat,0.0)
      endif
      L_(215)=R0_axe.gt.0.0
      L0_ixe=L_(34)
C FDA20_SP2_UVR.fgi( 174, 693):������������  �� T
      L_epod = L_(215).OR.L_(214).OR.L_(213)
C FDA20_SP2_UVR.fgi( 542, 788):���
      L_(35) = L_afof.AND.L_amod
C FDA20_SP2_UVR.fgi( 160, 697):�
      if(L_(35).and..not.L0_abi) then
         R0_oxe=R0_uxe
      else
         R0_oxe=max(R_(37)-deltat,0.0)
      endif
      L_(218)=R0_oxe.gt.0.0
      L0_abi=L_(35)
C FDA20_SP2_UVR.fgi( 170, 697):������������  �� T
      L_ipod = L_(218).OR.L_(217).OR.L_(216)
C FDA20_SP2_UVR.fgi( 542, 794):���
      L_(38) = L_afof.AND.L_axed
C FDA20_SP2_UVR.fgi( 160, 709):�
      if(L_(38).and..not.L0_udi) then
         R0_idi=R0_odi
      else
         R0_idi=max(R_(40)-deltat,0.0)
      endif
      L_(225)=R0_idi.gt.0.0
      L0_udi=L_(38)
C FDA20_SP2_UVR.fgi( 174, 709):������������  �� T
      L_arod = L_(225).OR.L_(224)
C FDA20_SP2_UVR.fgi( 542, 809):���
      L_(39) = L_afof.AND.L_uxed
C FDA20_SP2_UVR.fgi( 160, 713):�
      if(L_(39).and..not.L0_ifi) then
         R0_afi=R0_efi
      else
         R0_afi=max(R_(41)-deltat,0.0)
      endif
      L_(227)=R0_afi.gt.0.0
      L0_ifi=L_(39)
C FDA20_SP2_UVR.fgi( 170, 713):������������  �� T
      L_erod = L_(227).OR.L_(226)
C FDA20_SP2_UVR.fgi( 542, 813):���
      L_(253) = L_okuf.AND.(.NOT.L_(249))
C FDA20_SP2_UVR.fgi( 152, 744):�
      if(L_(253).and..not.L0_ovef) then
         R0_evef=R0_ivef
      else
         R0_evef=max(R_(158)-deltat,0.0)
      endif
      L_(252)=R0_evef.gt.0.0
      L0_ovef=L_(253)
C FDA20_SP2_UVR.fgi( 241, 744):������������  �� T
      L_(251) = L_(252).OR.L_(250)
C FDA20_SP2_UVR.fgi( 251, 743):���
      L_ofid = L_(158).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 756):���
      L_ifid = L_(156).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 750):���
      L_efid = L_(155).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 744):���
      L_afid = L_(154).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 738):���
      L_udid = L_(153).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 733):���
      L_odid = L_(152).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 728):���
      L_idid = L_(151).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 722):���
      L_edid = L_(150).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 716):���
      L_adid = L_(149).OR.L_(157).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 710):���
      if(.not.L_okuf) then
         R0_asef=0.0
      elseif(.not.L0_esef) then
         R0_asef=R0_uref
      else
         R0_asef=max(R_(155)-deltat,0.0)
      endif
      L_(282)=L_okuf.and.R0_asef.le.0.0
      L0_esef=L_okuf
C FDA20_SP2_UVR.fgi( 162, 740):�������� ��������� ������
      if(L_(282).and..not.L0_iduf) then
         R0_aduf=R0_eduf
      else
         R0_aduf=max(R_(170)-deltat,0.0)
      endif
      L_ikuf=R0_aduf.gt.0.0
      L0_iduf=L_(282)
C FDA20_SP2_UVR.fgi( 174, 740):������������  �� T
      L_ekuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 740):������,20FDA20TRAN01_PREV
      L_akuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 738):������,20FDA20TRAN02_PREV
      L_ufuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 736):������,20FDA20TRAN03_PREV
      L_ofuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 734):������,20FDA20TRAN04_PREV
      L_ifuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 732):������,20FDA20TRAN05_PREV
      L_efuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 730):������,20FDA20TRAN06_PREV
      L_afuf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 728):������,20FDA20TRAN07_PREV
      L_uduf=L_ikuf
C FDA20_SP2_UVR.fgi( 194, 726):������,20FDA20TRAN08_PREV
      L_oduf = L_ikuf.OR.L_(101)
C FDA20_SP2_UVR.fgi( 240, 721):���
      L_ifed = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 673):���
      L_efed = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 669):���
      L_afed = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 665):���
      L_uded = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 661):���
      L_oded = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 657):���
      L_ided = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 653):���
      L_eded = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 649):���
      L_aded = L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 645):���
      L_ubed = L_(130).OR.L_(131).OR.L_ukuf
C FDA20_SP2_UVR.fgi( 544, 640):���
      if(L_(115).and..not.L0_opad) then
         R0_epad=R0_ipad
      else
         R0_epad=max(R_(83)-deltat,0.0)
      endif
      L_osad=R0_epad.gt.0.0
      L0_opad=L_(115)
C FDA20_SP2_UVR.fgi( 150, 215):������������  �� T
      L_ukur=L_omife
C FDA20_lamp.fgi( 154, 858):������,20FDA23AB002YV11
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ufur,L_ikur,R_ekur,
     & REAL(R_okur,4),L_ukur,L_ofur,I_akur)
      !}
C FDA20_lamp.fgi( 152, 844):���������� ������� ���������,20FDA23AB002QH01
      L_ebur=L_umife
C FDA20_lamp.fgi( 154, 854):������,20FDA23AB002YV10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exor,L_uxor,R_oxor,
     & REAL(R_abur,4),L_ebur,L_axor,I_ixor)
      !}
C FDA20_lamp.fgi( 152, 830):���������� ������� ���������,20FDA23AB002QH01_1
      if(.not.L_opu) then
         R0_omu=0.0
      elseif(.not.L0_umu) then
         R0_omu=R0_imu
      else
         R0_omu=max(R_(67)-deltat,0.0)
      endif
      L_(102)=L_opu.and.R0_omu.le.0.0
      L0_umu=L_opu
C FDA20_SP2_UVR.fgi(  45,  37):�������� ��������� ������
C label 3070  try3070=try3070-1
      L_(104) = L_ivad.AND.L_(103).AND.L_(102)
C FDA20_SP2_UVR.fgi(  56,  42):�
      if(L_omuf) then
          if (L_(90)) then
              I_apuf = 33
              L_opu = .true.
              L_(91) = .false.
          endif
          if (L_(104)) then
              L_(91) = .true.
              L_opu = .false.
          endif
          if (I_apuf.ne.33) then
              L_opu = .false.
              L_(91) = .false.
          endif
      else
          L_(91) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  42):��� ������� ���������,uvr
C sav1=L_(102)
      if(.not.L_opu) then
         R0_omu=0.0
      elseif(.not.L0_umu) then
         R0_omu=R0_imu
      else
         R0_omu=max(R_(67)-deltat,0.0)
      endif
      L_(102)=L_opu.and.R0_omu.le.0.0
      L0_umu=L_opu
C FDA20_SP2_UVR.fgi(  45,  37):recalc:�������� ��������� ������
C if(sav1.ne.L_(102) .and. try3070.gt.0) goto 3070
      if(L_omuf) then
          if (L_(91)) then
              I_apuf = 34
              L_emu = .true.
              L_(281) = .false.
          endif
          if (L_okif) then
              L_(281) = .true.
              L_emu = .false.
          endif
          if (I_apuf.ne.34) then
              L_emu = .false.
              L_(281) = .false.
          endif
      else
          L_(281) = .false.
      endif
C FDA20_SP2_UVR.fgi(  93,  22):��� ������� ���������,uvr
      if(L_(281)) then
         I_apuf=0
         L_omuf=.false.
      endif
C FDA20_SP2_UVR.fgi(  93,  11):����� ������� ���������,uvr
      if(L_emu.and..not.L0_amu) then
         R0_olu=R0_ulu
      else
         R0_olu=max(R_(66)-deltat,0.0)
      endif
      L_(95)=R0_olu.gt.0.0
      L0_amu=L_emu
C FDA20_SP2_UVR.fgi( 160,  22):������������  �� T
      L_(147) = L_adad.OR.L_(97).OR.L_(96).OR.L_(95)
C FDA20_SP2_UVR.fgi( 175, 101):���
      L_ubid = L_(148).OR.L_(157).OR.L_(147).OR.L_(251)
C FDA20_SP2_UVR.fgi( 544, 703):���
      if(L_opu.and..not.L0_ipu) then
         R0_apu=R0_epu
      else
         R0_apu=max(R_(68)-deltat,0.0)
      endif
      L_(124)=R0_apu.gt.0.0
      L0_ipu=L_opu
C FDA20_SP2_UVR.fgi( 160,  42):������������  �� T
      L_uvad = L_(124).OR.L_(260).OR.L_(129)
C FDA20_SP2_UVR.fgi( 544, 580):���
      End
