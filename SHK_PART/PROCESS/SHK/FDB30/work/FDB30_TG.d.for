      subroutine FDB30_TGData!
      implicit none
      include 'FDB30_TG.fh'

      REAL*8 FlwTnSqFrom(19)
      COMMON/FDB30_TG_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(19)
      COMMON/FDB30_TG_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(19)
      COMMON/FDB30_TG_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(19)
      COMMON/FDB30_TG_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(19)
      COMMON/FDB30_TG_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(19)
      COMMON/FDB30_TG_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(13)
      COMMON/FDB30_TG_nowrt/ INVPERe
      INTEGER*4 NZSUBe(96)
      COMMON/FDB30_TG_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(14)
      COMMON/FDB30_TG_nowrt/ INZSUBe
      INTEGER*4 ILNZe(14)
      COMMON/FDB30_TG_nowrt/ ILNZe
      INTEGER*4 INVPERp(13)
      COMMON/FDB30_TG_nowrt/ INVPERp
      INTEGER*4 NZSUBp(96)
      COMMON/FDB30_TG_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(14)
      COMMON/FDB30_TG_nowrt/ INZSUBp
      INTEGER*4 ILNZp(14)
      COMMON/FDB30_TG_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=19)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=19)
      INTEGER*4 Nent
      PARAMETER (Nent=13)
      INTEGER*4 Npc
      PARAMETER (Npc=26)
      INTEGER*4 Npres
      PARAMETER (Npres=13)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(13)
      COMMON/FDB30_TG_nowrt/ PIntIter
      LOGICAL*1 EIntIter(13)
      COMMON/FDB30_TG_nowrt/ EIntIter
      LOGICAL*1 EextIter(13)
      COMMON/FDB30_TG_nowrt/ EextIter
      LOGICAL*1 PExtIter(13)
      COMMON/FDB30_TG_nowrt/ PExtIter
      !DEC$PSECT/FDB30_TG_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:19)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:19)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:19)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:19)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:19)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:19)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA EextIter(1:13)/.true.,.false.,.true.,.true.,.true.,.true.,
     >.false.,.true.,.true.,.true.,.true.,.true.,.false./
      DATA PExtIter(1:13)/.true.,.false.,.true.,.true.,.true.,.true.,
     >.false.,.true.,.true.,.true.,.true.,.true.,.false./
      DATA EIntIter(1:13)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA PIntIter(1:13)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA INVPERe(1:13)/7,13,2,4,5,6,8,9,3,10,11,12,1/
      DATA NZSUBe(1:96)/12,9,7,8,7,13,8,13,9,13,10,13,11,13,12,13,0,0,0,
     >0,0,0,0,0,0,0,0,-1,0,0,0,0,1816225892,1685016172,0,0,0,1268643410,
     >71349390,0,0,-1,-3,412239416,412239496,1268643740,71244233,
     >424709396,2,424708300,424709384,8,1268643688,1065353216,0,
     >1268643520,183297456,183309088,1268643248,1268643136,1,0,0,0,
     >1086556160,0,1086556160,0,0,1268643360,19,12,1,13,1,1,13,
     >1268672776,420724676,0,0,0,1268672792,420724676,426138540,
     >426138536,0,61,417091124,426138876,98823232,1268643836,71297666,
     >71383748,1268643848,71297846/
      DATA INZSUBe(1:14)/1,2,2,3,5,5,7,9,11,13,15,16,16,16/
      DATA ILNZe(1:14)/1,2,3,4,6,8,10,12,14,16,18,20,21,21/
      DATA INVPERp(1:13)/7,13,2,4,5,6,8,9,3,10,11,12,1/
      DATA NZSUBp(1:96)/12,9,7,8,7,13,8,13,9,13,10,13,11,13,12,13,0,0,0,
     >0,0,0,0,0,0,0,0,-1,0,0,0,0,1816225892,1685016172,0,0,0,1268643410,
     >71349390,0,0,-1,-3,412239416,412239496,1268643740,71244233,
     >424709396,2,424708300,424709384,8,1268643688,1065353216,0,
     >1268643520,183297456,183309088,1268643248,1268643136,1,0,0,0,
     >1086556160,0,1086556160,0,0,1268643360,19,12,1,13,1,1,13,
     >1268672776,420724676,0,0,0,1268672792,420724676,426138540,
     >426138536,426138676,426138896,426138628,426138876,98823232,
     >98823344,98822736,98822848,183278224,183282128/
      DATA INZSUBp(1:14)/1,2,2,3,5,5,7,9,11,13,15,16,16,16/
      DATA ILNZp(1:14)/1,2,3,4,6,8,10,12,14,16,18,20,21,21/
      return
      end

