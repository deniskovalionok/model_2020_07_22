      subroutine FDB60_vent_hData!
      implicit none
      include 'FDB60_vent_h.fh'

      REAL*8 FlwTnSqFrom(13)
      COMMON/FDB60_vent_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(13)
      COMMON/FDB60_vent_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(12)
      COMMON/FDB60_vent_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(75)
      COMMON/FDB60_vent_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(13)
      COMMON/FDB60_vent_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(13)
      COMMON/FDB60_vent_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(12)
      COMMON/FDB60_vent_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(75)
      COMMON/FDB60_vent_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(13)
      COMMON/FDB60_vent_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(13)
      COMMON/FDB60_vent_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=13)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=13)
      INTEGER*4 Nent
      PARAMETER (Nent=12)
      INTEGER*4 Npc
      PARAMETER (Npc=24)
      INTEGER*4 Npres
      PARAMETER (Npres=12)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(12)
      COMMON/FDB60_vent_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(12)
      COMMON/FDB60_vent_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(12)
      COMMON/FDB60_vent_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(12)
      COMMON/FDB60_vent_h_nowrt/ PExtIter
      !DEC$PSECT/FDB60_vent_h_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA EextIter(1:12)/.false.,.true.,.true.,.false.,.true.,.false.,
     >.true.,.true.,.true.,.true.,.false.,.true./
      DATA PExtIter(1:12)/.false.,.true.,.true.,.false.,.true.,.false.,
     >.true.,.true.,.true.,.true.,.false.,.true./
      DATA EIntIter(1:12)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA PIntIter(1:12)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA INVPERe(1:12)/4,11,6,12,3,2,7,1,8,9,10,5/
      DATA NZSUBe(1:75)/2,3,4,12,10,8,12,9,12,10,12,11,12,12,71244233,
     >420515092,2,420513996,420515080,8,1268652264,1065353216,0,
     >1268652096,100180736,100194000,1268651888,1268651792,1,0,0,0,
     >1086556160,0,1086556160,0,0,1268651984,13,11,1,12,1,1,12,
     >1268672776,416530372,0,0,0,1268672792,416530372,421944236,
     >421944232,0,57,414591024,421944572,19654464,1268652412,71297666,
     >71383748,1268652424,71297846,71383748,1268652440,71299920,48,
     >421724320,1268652452,71299790,57,0,1268672352,419817464/
      DATA INZSUBe(1:13)/1,2,3,4,5,6,6,8,10,12,13,13,13/
      DATA ILNZe(1:13)/1,2,3,4,5,6,7,9,11,13,15,16,16/
      DATA INVPERp(1:12)/4,11,6,12,3,2,7,1,8,9,10,5/
      DATA NZSUBp(1:75)/2,3,4,12,10,8,12,9,12,10,12,11,12,12,71244233,
     >420515092,2,420513996,420515080,8,1268652264,1065353216,0,
     >1268652096,100180736,100194000,1268651888,1268651792,1,0,0,0,
     >1086556160,0,1086556160,0,0,1268651984,13,11,1,12,1,1,12,
     >1268672776,416530372,0,0,0,1268672792,416530372,421944236,
     >421944232,421944372,421944592,421944324,421944572,19654464,
     >19654544,19654304,19654864,100169808,100171568,100170896,100170752
     >,0,0,0,0,0,0,19655392,1268672344,419817464/
      DATA INZSUBp(1:13)/1,2,3,4,5,6,6,8,10,12,13,13,13/
      DATA ILNZp(1:13)/1,2,3,4,5,6,7,9,11,13,15,16,16/
      return
      end

