      Subroutine CONTROL_FDD(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'CONTROL_FDD.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(34)=R0_adix
C control_tg.fgi(  41, 113):pre: ���������������� ����� 
      R_(60)=R0_odube
C control_th.fgi(  85, 215):pre: ���������������� ����� 
      R_ifor=0
      R_idor=0
      R_abor=0
      R_evir=0
      R_usir=0
      R_urir=0
      R_upir=0
      R_ilir=0
      R_ekir=0
      R_ifir=0
      R_ibir=0
      R_uxer=0
      R_exer=0
      R_over=0
      R_aver=0
C FDD_logic.fgi(  49, 277):pre: ���������� ���,TVS1
      R_ifor=0
      R_idor=0
      R_abor=0
      R_evir=0
      R_usir=0
      R_urir=0
      R_upir=0
      R_ilir=0
      R_ekir=0
      R_ifir=0
      R_ibir=0
      R_uxer=0
      R_exer=0
      R_over=0
      R_aver=0
C FDD_logic.fgi(  55, 277):pre: ���������� ���,TVS2
      R_ifor=0
      R_idor=0
      R_abor=0
      R_evir=0
      R_usir=0
      R_urir=0
      R_upir=0
      R_ilir=0
      R_ekir=0
      R_ifir=0
      R_ibir=0
      R_uxer=0
      R_exer=0
      R_over=0
      R_aver=0
C FDD_logic.fgi(  61, 277):pre: ���������� ���,TVS3
      R_ifor=0
      R_idor=0
      R_abor=0
      R_evir=0
      R_usir=0
      R_urir=0
      R_upir=0
      R_ilir=0
      R_ekir=0
      R_ifir=0
      R_ibir=0
      R_uxer=0
      R_exer=0
      R_over=0
      R_aver=0
C FDD_logic.fgi(  67, 277):pre: ���������� ���,TVS4
      I_(1) = 1
C FDD_logic.fgi( 116, 199):��������� ������������� IN (�������)
      I_(2) = 0
C FDD_logic.fgi( 116, 201):��������� ������������� IN (�������)
      I_(3) = 1
C FDD_logic.fgi( 116, 216):��������� ������������� IN (�������)
      I_(4) = 0
C FDD_logic.fgi( 116, 218):��������� ������������� IN (�������)
      I_(5) = 1
C FDD_logic.fgi( 116, 233):��������� ������������� IN (�������)
      I_(6) = 0
C FDD_logic.fgi( 116, 235):��������� ������������� IN (�������)
      I_(7) = 1
C FDD_logic.fgi( 115, 248):��������� ������������� IN (�������)
      I_(8) = 0
C FDD_logic.fgi( 115, 250):��������� ������������� IN (�������)
      I_(14) = 1
C FDD_logic.fgi( 115, 265):��������� ������������� IN (�������)
      I_(15) = 0
C FDD_logic.fgi( 115, 267):��������� ������������� IN (�������)
      I_(16) = 0
C FDD_logic.fgi(  30, 188):��������� ������������� IN (�������)
      L_(1)=I_i.ne.I_(16)
C FDD_logic.fgi(  35, 189):���������� �������������
      if(L_(1)) then
         I_u=I0_o
      else
         I_u=I0_ad
      endif
C FDD_logic.fgi(  42, 196):���� RE IN LO CH7
      if(L_(1)) then
         I_(9)=I_(1)
      else
         I_(9)=I_(2)
      endif
C FDD_logic.fgi( 124, 199):���� RE IN LO CH7
      !�����. �������� I0_o = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 196):��������� �����
      !�����. �������� I0_ad = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 198):��������� �����
      I_(17) = 0
C FDD_logic.fgi(  30, 206):��������� ������������� IN (�������)
      L_(2)=I_ed.ne.I_(17)
C FDD_logic.fgi(  35, 207):���������� �������������
      if(L_(2)) then
         I_od=I0_id
      else
         I_od=I0_ud
      endif
C FDD_logic.fgi(  42, 214):���� RE IN LO CH7
      if(L_(2)) then
         I_(10)=I_(3)
      else
         I_(10)=I_(4)
      endif
C FDD_logic.fgi( 124, 216):���� RE IN LO CH7
      !�����. �������� I0_id = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 214):��������� �����
      !�����. �������� I0_ud = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 216):��������� �����
      I_(18) = 0
C FDD_logic.fgi(  30, 224):��������� ������������� IN (�������)
      L_(3)=I_af.ne.I_(18)
C FDD_logic.fgi(  35, 225):���������� �������������
      if(L_(3)) then
         I_if=I0_ef
      else
         I_if=I0_of
      endif
C FDD_logic.fgi(  42, 232):���� RE IN LO CH7
      if(L_(3)) then
         I_(11)=I_(5)
      else
         I_(11)=I_(6)
      endif
C FDD_logic.fgi( 124, 233):���� RE IN LO CH7
      !�����. �������� I0_ef = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 232):��������� �����
      !�����. �������� I0_of = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 234):��������� �����
      I_(19) = 0
C FDD_logic.fgi(  30, 241):��������� ������������� IN (�������)
      L_(4)=I_uf.ne.I_(19)
C FDD_logic.fgi(  35, 242):���������� �������������
      if(L_(4)) then
         I_ek=I0_ak
      else
         I_ek=I0_ik
      endif
C FDD_logic.fgi(  42, 249):���� RE IN LO CH7
      if(L_(4)) then
         I_(12)=I_(7)
      else
         I_(12)=I_(8)
      endif
C FDD_logic.fgi( 123, 248):���� RE IN LO CH7
      !�����. �������� I0_ak = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 249):��������� �����
      !�����. �������� I0_ik = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 251):��������� �����
      I_(20) = 0
C FDD_logic.fgi(  30, 256):��������� ������������� IN (�������)
      L_(5)=I_ok.ne.I_(20)
C FDD_logic.fgi(  35, 257):���������� �������������
      if(L_(5)) then
         I_al=I0_uk
      else
         I_al=I0_el
      endif
C FDD_logic.fgi(  42, 264):���� RE IN LO CH7
      if(L_(5)) then
         I_(13)=I_(14)
      else
         I_(13)=I_(15)
      endif
C FDD_logic.fgi( 123, 265):���� RE IN LO CH7
      I_il = I_(13) + I_(12) + I_(11) + I_(10) + I_(9)
C FDD_logic.fgi( 133, 234):��������
      !�����. �������� I0_uk = FDD_logicC?? /z'0100000A'/
C FDD_logic.fgi(  34, 264):��������� �����
      !�����. �������� I0_el = FDD_logicC?? /z'01000003'/
C FDD_logic.fgi(  34, 266):��������� �����
      Call TVS_HANDLER(deltat,I_asod,I_esod,R_eki,R_iki,
     & R_oki,R_uki,I_usod,I_ukir,R_ali,
     & R_eli,R_ili,R_oli,R_uli,R_ami,R_emi,
     & R_imi,R_omi,R_umi,R_api,R_epi,R_ipi,
     & R_opi,R_oti,R_uti,I_atod,R_avi,R_evi,
     & R_ivi,R_ovi,I_etod,I_itod,I_otod,I_adir,
     & R_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,
     & R_ibo,R_obo,R_ubo,R_ado,R_edo,R_ido,
     & I_utod,I_avod,I_evod,I_edir,I_ivod,I_ovod,I_uvod,
     & I_idir,I_axod,I_exod,I_ixod,I_odir,I_oxod,
     & I_uxod,I_abud,I_udir,I_ebud,I_ibud,I_obud,
     & I_afir,I_ubud,I_adud,I_edud,I_efir,
     & R_alo,R_elo,R_ilo,R_olo,R_ulo,R_amo,
     & R_emo,R_imo,I_idud,I_odud,I_udud,I_ador,
     & I_afud,I_efud,I_ofud,I_edor,R_omo,R_umo,
     & R_apo,R_epo,R_ipo,R_opo,R_upo,R_aro,
     & R_ero,R_iro,I_ufud,I_akud,I_ekud,I_erer,
     & I_ikud,I_okud,I_ukud,I_irer,R_eto,R_ito,
     & R_oto,R_uto,I_alud,I_elud,I_ilud,I_ober,
     & R_avo,R_evo,R_ivo,R_ovo,R_uvo,R_axo,
     & R_oxo,R_uxo,R_abu,R_ebu,I_olud,I_ulud,
     & I_amud,I_ider,R_ibu,R_obu,I_emud,I_imud,
     & I_omud,I_oder,R_ubu,R_adu,I_umud,I_apud,
     & I_epud,I_uder,I_ipud,I_upud,I_afer,
     & R_odu,R_udu,I_arud,I_erud,I_irud,I_efer,
     & R_afu,R_efu,R_ifu,R_ofu,I_orud,I_urud,
     & I_asud,I_ifer,I_esud,I_isud,R_ufu,R_aku,
     & R_eku,R_iku,I_osud,I_akir,R_alu,
     & R_elu,R_ilu,R_ulu,R_amu,R_emu,R_imu,
     & R_omu,R_umu,R_apu,R_epu,R_ipu,R_opu,
     & R_upu,R_iru,R_oru,R_uru,R_isu,R_osu,
     & R_usu,R_atu,I_usud,I_atud,I_etud,I_imir,
     & R_etu,R_itu,R_avu,R_evu,R_ivu,R_ovu,
     & I_itud,I_otud,I_utud,I_omir,R_uvu,R_axu,
     & R_oxu,R_uxu,R_abad,R_ebad,I_avud,I_evud,
     & I_ivud,I_umir,R_ibad,R_obad,R_ubad,
     & R_adad,R_edad,R_idad,R_afad,R_efad,R_ifad,
     & R_ofad,R_ufad,R_akad,R_ekad,R_ikad,R_okad,
     & R_ukad,R_alad,R_elad,R_olad,R_emad,R_imad,
     & R_omad,R_umad,R_apad,R_epad,R_ipad,R_opad,
     & I_ovud,I_uvud,I_axud,I_exud,I_ixud,I_oxud,I_apir,
     & I_epir,R_upad,R_arad,R_erad,R_irad,
     & R_urad,I_uxud,R_asad,R_esad,I_abaf,I_ebaf,I_ibaf,
     & R_osad,R_usad,R_etad,R_itad,R_otad,I_obaf,
     & I_ubaf,R_evad,R_ivad,R_uvad,R_axad,R_exad,
     & R_uxad,R_abed,R_ebed,R_obed,R_ubed,
     & I_ipir,I_opir,R_ided,R_oded,R_afed,
     & R_efed,R_ifed,R_ofed,R_aked,R_eked,R_iked,
     & R_oked,R_uked,R_aled,R_eled,R_iled,I_idaf,
     & I_odaf,R_oled,I_udaf,I_irir,R_uled,R_amed,
     & R_emed,R_imed,R_omed,I_afaf,R_umed,R_aped,
     & R_eped,R_iped,I_efaf,R_ared,R_ered,R_ired,
     & R_ored,R_ured,I_ifaf,I_isir,R_ased,
     & R_esed,R_ised,R_osed,R_used,R_ated,R_eted,
     & I_axir,R_ited,R_oted,I_ovir,
     & I_ivir,R_axed,R_exed,I_ofaf,I_ufaf,R_ixed,
     & R_oxed,R_uxed,R_abid,I_utir,R_amaf,R_emaf,
     & I_imaf,I_omaf,R_apaf,R_epaf,I_ipaf,L_aber,
     & R_okid,R_um,R_ex,R_ole,R_ave,
     & R_iber,R_uber,I_opaf,L_eber,R_ukid,
     & R_ap,R_ix,R_ule,R_eve,R_eder,
     & R_ofer,R_upaf,R_araf,I_eraf,I_iraf,R_oraf,R_uraf,
     & I_asaf,L_ader,R_elid,R_ep,R_ox,
     & R_ame,R_ive,R_aker,R_eker,I_esaf,
     & L_ufer,R_ilid,R_ip,R_ux,R_eme,
     & R_ove,R_oker,R_uker,R_isaf,R_osaf,I_usaf,
     & I_ataf,R_etaf,R_itaf,I_otaf,L_iker,R_olid,
     & R_up,R_ebe,R_ome,R_axe,R_oler,
     & R_uler,I_utaf,L_iler,R_ulid,R_ar,
     & R_ibe,R_ume,R_exe,R_emer,R_imer,
     & R_avaf,R_evaf,R_ivaf,R_ovaf,R_uvaf,R_axaf,I_exaf,I_ixaf
     &,
     & I_oxaf,I_uxaf,I_abef,I_ebef,R_ibef,R_obef,R_ubef,R_edef
     &,
     & R_idef,R_odef,I_udef,L_amer,R_amid,R_er,
     & R_obe,R_ape,R_ixe,R_umer,R_aper,
     & I_afef,L_omer,R_imid,R_ir,R_ube,
     & R_epe,R_oxe,R_iper,R_oper,I_ifef,
     & L_eper,R_omid,R_or,R_ade,R_ipe,
     & R_uxe,R_arer,R_orer,I_ofef,L_uper,
     & R_umid,R_ur,R_ede,R_ope,R_abi,
     & R_aser,R_eser,I_ufef,L_urer,R_apid,
     & R_es,R_ode,R_are,R_ibi,R_oser,
     & R_user,I_akef,L_iser,R_epid,R_is,
     & R_ude,R_ere,R_obi,R_eter,R_iter,
     & I_uvir,I_ekef,R_axid,R_exid,R_uxid,
     & R_abod,I_okef,I_ukef,I_alef,I_olef,I_ulef,R_ebod,
     & R_ibod,R_ukod,I_oxir,R_alod,R_amod,
     & R_emod,R_imod,I_afor,R_omod,R_emef,R_imef,
     & R_omef,R_umef,R_apef,R_epef,R_ipef,R_opef,R_upef,R_aref
     &,
     & R_eref,R_iref,R_oref,R_uref,R_asef,R_esef,R_isef,R_osef
     &,
     & I_usef,I_atef,I_etef,I_itef,I_otef,I_utef,I_avef,I_evef
     &,
     & I_ivef,I_ovef,I_uvef,I_axef,I_exef,I_ixef,R_uxef,R_abif
     &,
     & R_ebif,R_ibif,R_obif,R_ubif,R_adif,R_edif,R_idif,R_odif
     &,
     & R_udif,R_afif,I_ifif,L_ater,R_ipod,R_it,
     & R_ufe,R_ese,R_odi,R_uter,R_aver,
     & I_ofif,L_oter,R_opod,R_ot,R_ake,
     & R_ise,R_udi,R_iver,R_over,I_ufif,
     & L_ever,R_upod,R_ut,R_eke,R_ose,
     & R_afi,R_axer,R_exer,I_arod,L_uver,
     & R_efi,R_ol,R_av,R_ike,R_use,R_oxer,
     & R_uxer,I_erod,L_ixer,R_ifi,R_ul,R_ev,
     & R_oke,R_ate,R_ebir,R_ibir,I_irod,
     & L_abir,R_ofi,R_am,R_iv,R_uke,R_ete,
     & R_ubir,R_ifir,I_orod,L_obir,R_ufi,
     & R_em,R_ov,R_ale,R_ite,R_ufir,
     & R_ekir,I_urod,L_ofir,R_aki,R_im,R_uv,
     & R_ele,R_ote,R_okir,R_ilir,I_ulaf,
     & L_ikir,R_ikid,R_om,R_ax,R_ile,R_ute,
     & R_ulir,R_upir,I_umaf,L_olir,R_alid,
     & R_op,R_abe,R_ime,R_uve,R_erir,
     & R_urir,I_adef,L_arir,R_emid,R_as,R_ide,
     & R_upe,R_ebi,R_esir,R_usir,I_amef,
     & L_asir,R_umod,R_os,R_afe,R_ire,R_ubi,
     & R_etir,R_evir,I_oxef,L_atir,R_apod,
     & R_us,R_efe,R_ore,R_adi,R_ixir,
     & R_abor,I_efif,L_exir,R_epod,R_at,R_ife,
     & R_ure,R_edi,R_ibor,R_idor,I_efef,
     & L_ebor,R_uvid,R_et,R_ofe,R_ase,R_idi,
     & R_udor,I_ifud,I_uxar,R_ifor)
C FDD_logic.fgi(  67, 277):���������� ���,TVS4
      Call TVS_HANDLER(deltat,I_ipal,I_opal,R_oduf,R_uduf
     &,
     & R_afuf,R_efuf,I_eral,I_ukir,R_ifuf,
     & R_ofuf,R_ufuf,R_akuf,R_ekuf,R_ikuf,R_okuf,
     & R_ukuf,R_aluf,R_eluf,R_iluf,R_oluf,R_uluf,
     & R_amuf,R_asuf,R_esuf,I_iral,R_isuf,R_osuf,
     & R_usuf,R_atuf,I_oral,I_ural,I_asal,I_adir,
     & R_otuf,R_utuf,R_avuf,R_evuf,R_ivuf,R_ovuf,
     & R_uvuf,R_axuf,R_exuf,R_ixuf,R_oxuf,R_uxuf,
     & I_esal,I_isal,I_osal,I_edir,I_usal,I_atal,I_etal,
     & I_idir,I_ital,I_otal,I_utal,I_odir,I_aval,
     & I_eval,I_ival,I_udir,I_oval,I_uval,I_axal,
     & I_afir,I_exal,I_ixal,I_oxal,I_efir,
     & R_ifak,R_ofak,R_ufak,R_akak,R_ekak,R_ikak,
     & R_okak,R_ukak,I_uxal,I_abel,I_ebel,I_ador,
     & I_ibel,I_obel,I_adel,I_edor,R_alak,R_elak,
     & R_ilak,R_olak,R_ulak,R_amak,R_emak,R_imak,
     & R_omak,R_umak,I_edel,I_idel,I_odel,I_erer,
     & I_udel,I_afel,I_efel,I_irer,R_orak,R_urak,
     & R_asak,R_esak,I_ifel,I_ofel,I_ufel,I_ober,
     & R_isak,R_osak,R_usak,R_atak,R_etak,R_itak,
     & R_avak,R_evak,R_ivak,R_ovak,I_akel,I_ekel,
     & I_ikel,I_ider,R_uvak,R_axak,I_okel,I_ukel,
     & I_alel,I_oder,R_exak,R_ixak,I_elel,I_ilel,
     & I_olel,I_uder,I_ulel,I_emel,I_afer,
     & R_abek,R_ebek,I_imel,I_omel,I_umel,I_efer,
     & R_ibek,R_obek,R_ubek,R_adek,I_apel,I_epel,
     & I_ipel,I_ifer,I_opel,I_upel,R_edek,R_idek,
     & R_odek,R_udek,I_arel,I_akir,R_ifek,
     & R_ofek,R_ufek,R_ekek,R_ikek,R_okek,R_ukek,
     & R_alek,R_elek,R_ilek,R_olek,R_ulek,R_amek,
     & R_emek,R_umek,R_apek,R_epek,R_upek,R_arek,
     & R_erek,R_irek,I_erel,I_irel,I_orel,I_imir,
     & R_orek,R_urek,R_isek,R_osek,R_usek,R_atek,
     & I_urel,I_asel,I_esel,I_omir,R_etek,R_itek,
     & R_avek,R_evek,R_ivek,R_ovek,I_isel,I_osel,
     & I_usel,I_umir,R_uvek,R_axek,R_exek,
     & R_ixek,R_oxek,R_uxek,R_ibik,R_obik,R_ubik,
     & R_adik,R_edik,R_idik,R_odik,R_udik,R_afik,
     & R_efik,R_ifik,R_ofik,R_akik,R_okik,R_ukik,
     & R_alik,R_elik,R_ilik,R_olik,R_ulik,R_amik,
     & I_atel,I_etel,I_itel,I_otel,I_utel,I_avel,I_apir,
     & I_epir,R_emik,R_imik,R_omik,R_umik,
     & R_epik,I_evel,R_ipik,R_opik,I_ivel,I_ovel,I_uvel,
     & R_arik,R_erik,R_orik,R_urik,R_asik,I_axel,
     & I_exel,R_osik,R_usik,R_etik,R_itik,R_otik,
     & R_evik,R_ivik,R_ovik,R_axik,R_exik,
     & I_ipir,I_opir,R_uxik,R_abok,R_ibok,
     & R_obok,R_ubok,R_adok,R_idok,R_odok,R_udok,
     & R_afok,R_efok,R_ifok,R_ofok,R_ufok,I_uxel,
     & I_abil,R_akok,I_ebil,I_irir,R_ekok,R_ikok,
     & R_okok,R_ukok,R_alok,I_ibil,R_elok,R_ilok,
     & R_olok,R_ulok,I_obil,R_imok,R_omok,R_umok,
     & R_apok,R_epok,I_ubil,I_isir,R_ipok,
     & R_opok,R_upok,R_arok,R_erok,R_irok,R_orok,
     & I_axir,R_urok,R_asok,I_ovir,
     & I_ivir,R_itok,R_otok,I_adil,I_edil,R_utok,
     & R_avok,R_evok,R_ivok,I_utir,R_ikil,R_okil,
     & I_ukil,I_alil,R_ilil,R_olil,I_ulil,L_aber,
     & R_afuk,R_elif,R_otif,R_akof,R_isof,
     & R_iber,R_uber,I_amil,L_eber,R_efuk,
     & R_ilif,R_utif,R_ekof,R_osof,R_eder,
     & R_ofer,R_emil,R_imil,I_omil,I_umil,R_apil,R_epil,
     & I_ipil,L_ader,R_ofuk,R_olif,R_avif,
     & R_ikof,R_usof,R_aker,R_eker,I_opil,
     & L_ufer,R_ufuk,R_ulif,R_evif,R_okof,
     & R_atof,R_oker,R_uker,R_upil,R_aril,I_eril,
     & I_iril,R_oril,R_uril,I_asil,L_iker,R_akuk,
     & R_emif,R_ovif,R_alof,R_itof,R_oler,
     & R_uler,I_esil,L_iler,R_ekuk,R_imif,
     & R_uvif,R_elof,R_otof,R_emer,R_imer,
     & R_isil,R_osil,R_usil,R_atil,R_etil,R_itil,I_otil,I_util
     &,
     & I_avil,I_evil,I_ivil,I_ovil,R_uvil,R_axil,R_exil,R_oxil
     &,
     & R_uxil,R_abol,I_ebol,L_amer,R_ikuk,R_omif,
     & R_axif,R_ilof,R_utof,R_umer,R_aper,
     & I_ibol,L_omer,R_ukuk,R_umif,R_exif,
     & R_olof,R_avof,R_iper,R_oper,I_ubol,
     & L_eper,R_aluk,R_apif,R_ixif,R_ulof,
     & R_evof,R_arer,R_orer,I_adol,L_uper,
     & R_eluk,R_epif,R_oxif,R_amof,R_ivof,
     & R_aser,R_eser,I_edol,L_urer,R_iluk,
     & R_opif,R_abof,R_imof,R_uvof,R_oser,
     & R_user,I_idol,L_iser,R_oluk,R_upif,
     & R_ebof,R_omof,R_axof,R_eter,R_iter,
     & I_uvir,I_odol,R_ituk,R_otuk,R_evuk,
     & R_ivuk,I_afol,I_efol,I_ifol,I_akol,I_ekol,R_ovuk,
     & R_uvuk,R_efal,I_oxir,R_ifal,R_ikal,
     & R_okal,R_ukal,I_afor,R_alal,R_okol,R_ukol,
     & R_alol,R_elol,R_ilol,R_olol,R_ulol,R_amol,R_emol,R_imol
     &,
     & R_omol,R_umol,R_apol,R_epol,R_ipol,R_opol,R_upol,R_arol
     &,
     & I_erol,I_irol,I_orol,I_urol,I_asol,I_esol,I_isol,I_osol
     &,
     & I_usol,I_atol,I_etol,I_itol,I_otol,I_utol,R_evol,R_ivol
     &,
     & R_ovol,R_uvol,R_axol,R_exol,R_ixol,R_oxol,R_uxol,R_abul
     &,
     & R_ebul,R_ibul,I_ubul,L_ater,R_ulal,R_urif,
     & R_edof,R_opof,R_abuf,R_uter,R_aver,
     & I_adul,L_oter,R_amal,R_asif,R_idof,
     & R_upof,R_ebuf,R_iver,R_over,I_edul,
     & L_ever,R_emal,R_esif,R_odof,R_arof,
     & R_ibuf,R_axer,R_exer,I_imal,L_uver,
     & R_obuf,R_akif,R_isif,R_udof,R_erof,R_oxer,
     & R_uxer,I_omal,L_ixer,R_ubuf,R_ekif,R_osif,
     & R_afof,R_irof,R_ebir,R_ibir,I_umal,
     & L_abir,R_aduf,R_ikif,R_usif,R_efof,R_orof,
     & R_ubir,R_ifir,I_apal,L_obir,R_eduf,
     & R_okif,R_atif,R_ifof,R_urof,R_ufir,
     & R_ekir,I_epal,L_ofir,R_iduf,R_ukif,R_etif,
     & R_ofof,R_asof,R_okir,R_ilir,I_ekil,
     & L_ikir,R_uduk,R_alif,R_itif,R_ufof,R_esof,
     & R_ulir,R_upir,I_elil,L_olir,R_ifuk,
     & R_amif,R_ivif,R_ukof,R_etof,R_erir,
     & R_urir,I_ixil,L_arir,R_okuk,R_ipif,R_uxif,
     & R_emof,R_ovof,R_esir,R_usir,I_ikol,
     & L_asir,R_elal,R_arif,R_ibof,R_umof,R_exof,
     & R_etir,R_evir,I_avol,L_atir,R_ilal,
     & R_erif,R_obof,R_apof,R_ixof,R_ixir,
     & R_abor,I_obul,L_exir,R_olal,R_irif,R_ubof,
     & R_epof,R_oxof,R_ibor,R_idor,I_obol,
     & L_ebor,R_etuk,R_orif,R_adof,R_ipof,R_uxof,
     & R_udor,I_ubel,I_uxar,R_ifor)
C FDD_logic.fgi(  61, 277):���������� ���,TVS3
      Call TVS_HANDLER(deltat,I_ulip,I_amip,R_abem,R_ebem
     &,
     & R_ibem,R_obem,I_omip,I_ukir,R_ubem,
     & R_adem,R_edem,R_idem,R_odem,R_udem,R_afem,
     & R_efem,R_ifem,R_ofem,R_ufem,R_akem,R_ekem,
     & R_ikem,R_ipem,R_opem,I_umip,R_upem,R_arem,
     & R_erem,R_irem,I_apip,I_epip,I_ipip,I_adir,
     & R_asem,R_esem,R_isem,R_osem,R_usem,R_atem,
     & R_etem,R_item,R_otem,R_utem,R_avem,R_evem,
     & I_opip,I_upip,I_arip,I_edir,I_erip,I_irip,I_orip,
     & I_idir,I_urip,I_asip,I_esip,I_odir,I_isip,
     & I_osip,I_usip,I_udir,I_atip,I_etip,I_itip,
     & I_afir,I_otip,I_utip,I_avip,I_efir,
     & R_ubim,R_adim,R_edim,R_idim,R_odim,R_udim,
     & R_afim,R_efim,I_evip,I_ivip,I_ovip,I_ador,
     & I_uvip,I_axip,I_ixip,I_edor,R_ifim,R_ofim,
     & R_ufim,R_akim,R_ekim,R_ikim,R_okim,R_ukim,
     & R_alim,R_elim,I_oxip,I_uxip,I_abop,I_erer,
     & I_ebop,I_ibop,I_obop,I_irer,R_apim,R_epim,
     & R_ipim,R_opim,I_ubop,I_adop,I_edop,I_ober,
     & R_upim,R_arim,R_erim,R_irim,R_orim,R_urim,
     & R_isim,R_osim,R_usim,R_atim,I_idop,I_odop,
     & I_udop,I_ider,R_etim,R_itim,I_afop,I_efop,
     & I_ifop,I_oder,R_otim,R_utim,I_ofop,I_ufop,
     & I_akop,I_uder,I_ekop,I_okop,I_afer,
     & R_ivim,R_ovim,I_ukop,I_alop,I_elop,I_efer,
     & R_uvim,R_axim,R_exim,R_ixim,I_ilop,I_olop,
     & I_ulop,I_ifer,I_amop,I_emop,R_oxim,R_uxim,
     & R_abom,R_ebom,I_imop,I_akir,R_ubom,
     & R_adom,R_edom,R_odom,R_udom,R_afom,R_efom,
     & R_ifom,R_ofom,R_ufom,R_akom,R_ekom,R_ikom,
     & R_okom,R_elom,R_ilom,R_olom,R_emom,R_imom,
     & R_omom,R_umom,I_omop,I_umop,I_apop,I_imir,
     & R_apom,R_epom,R_upom,R_arom,R_erom,R_irom,
     & I_epop,I_ipop,I_opop,I_omir,R_orom,R_urom,
     & R_isom,R_osom,R_usom,R_atom,I_upop,I_arop,
     & I_erop,I_umir,R_etom,R_itom,R_otom,
     & R_utom,R_avom,R_evom,R_uvom,R_axom,R_exom,
     & R_ixom,R_oxom,R_uxom,R_abum,R_ebum,R_ibum,
     & R_obum,R_ubum,R_adum,R_idum,R_afum,R_efum,
     & R_ifum,R_ofum,R_ufum,R_akum,R_ekum,R_ikum,
     & I_irop,I_orop,I_urop,I_asop,I_esop,I_isop,I_apir,
     & I_epir,R_okum,R_ukum,R_alum,R_elum,
     & R_olum,I_osop,R_ulum,R_amum,I_usop,I_atop,I_etop,
     & R_imum,R_omum,R_apum,R_epum,R_ipum,I_itop,
     & I_otop,R_arum,R_erum,R_orum,R_urum,R_asum,
     & R_osum,R_usum,R_atum,R_itum,R_otum,
     & I_ipir,I_opir,R_evum,R_ivum,R_uvum,
     & R_axum,R_exum,R_ixum,R_uxum,R_abap,R_ebap,
     & R_ibap,R_obap,R_ubap,R_adap,R_edap,I_evop,
     & I_ivop,R_idap,I_ovop,I_irir,R_odap,R_udap,
     & R_afap,R_efap,R_ifap,I_uvop,R_ofap,R_ufap,
     & R_akap,R_ekap,I_axop,R_ukap,R_alap,R_elap,
     & R_ilap,R_olap,I_exop,I_isir,R_ulap,
     & R_amap,R_emap,R_imap,R_omap,R_umap,R_apap,
     & I_axir,R_epap,R_ipap,I_ovir,
     & I_ivir,R_urap,R_asap,I_ixop,I_oxop,R_esap,
     & R_isap,R_osap,R_usap,I_utir,R_udup,R_afup,
     & I_efup,I_ifup,R_ufup,R_akup,I_ekup,L_aber,
     & R_ibep,R_oful,R_asul,R_idam,R_upam,
     & R_iber,R_uber,I_ikup,L_eber,R_obep,
     & R_uful,R_esul,R_odam,R_aram,R_eder,
     & R_ofer,R_okup,R_ukup,I_alup,I_elup,R_ilup,R_olup,
     & I_ulup,L_ader,R_adep,R_akul,R_isul,
     & R_udam,R_eram,R_aker,R_eker,I_amup,
     & L_ufer,R_edep,R_ekul,R_osul,R_afam,
     & R_iram,R_oker,R_uker,R_emup,R_imup,I_omup,
     & I_umup,R_apup,R_epup,I_ipup,L_iker,R_idep,
     & R_okul,R_atul,R_ifam,R_uram,R_oler,
     & R_uler,I_opup,L_iler,R_odep,R_ukul,
     & R_etul,R_ofam,R_asam,R_emer,R_imer,
     & R_upup,R_arup,R_erup,R_irup,R_orup,R_urup,I_asup,I_esup
     &,
     & I_isup,I_osup,I_usup,I_atup,R_etup,R_itup,R_otup,R_avup
     &,
     & R_evup,R_ivup,I_ovup,L_amer,R_udep,R_alul,
     & R_itul,R_ufam,R_esam,R_umer,R_aper,
     & I_uvup,L_omer,R_efep,R_elul,R_otul,
     & R_akam,R_isam,R_iper,R_oper,I_exup,
     & L_eper,R_ifep,R_ilul,R_utul,R_ekam,
     & R_osam,R_arer,R_orer,I_ixup,L_uper,
     & R_ofep,R_olul,R_avul,R_ikam,R_usam,
     & R_aser,R_eser,I_oxup,L_urer,R_ufep,
     & R_amul,R_ivul,R_ukam,R_etam,R_oser,
     & R_user,I_uxup,L_iser,R_akep,R_emul,
     & R_ovul,R_alam,R_itam,R_eter,R_iter,
     & I_uvir,I_abar,R_urep,R_asep,R_osep,
     & R_usep,I_ibar,I_obar,I_ubar,I_idar,I_odar,R_atep,
     & R_etep,R_obip,I_oxir,R_ubip,R_udip,
     & R_afip,R_efip,I_afor,R_ifip,R_afar,R_efar,
     & R_ifar,R_ofar,R_ufar,R_akar,R_ekar,R_ikar,R_okar,R_ukar
     &,
     & R_alar,R_elar,R_ilar,R_olar,R_ular,R_amar,R_emar,R_imar
     &,
     & I_omar,I_umar,I_apar,I_epar,I_ipar,I_opar,I_upar,I_arar
     &,
     & I_erar,I_irar,I_orar,I_urar,I_asar,I_esar,R_osar,R_usar
     &,
     & R_atar,R_etar,R_itar,R_otar,R_utar,R_avar,R_evar,R_ivar
     &,
     & R_ovar,R_uvar,I_exar,L_ater,R_ekip,R_epul,
     & R_oxul,R_amam,R_ivam,R_uter,R_aver,
     & I_ixar,L_oter,R_ikip,R_ipul,R_uxul,
     & R_emam,R_ovam,R_iver,R_over,I_oxar,
     & L_ever,R_okip,R_opul,R_abam,R_imam,
     & R_uvam,R_axer,R_exer,I_ukip,L_uver,
     & R_axam,R_idul,R_upul,R_ebam,R_omam,R_oxer,
     & R_uxer,I_alip,L_ixer,R_exam,R_odul,R_arul,
     & R_ibam,R_umam,R_ebir,R_ibir,I_elip,
     & L_abir,R_ixam,R_udul,R_erul,R_obam,R_apam,
     & R_ubir,R_ifir,I_ilip,L_obir,R_oxam,
     & R_aful,R_irul,R_ubam,R_epam,R_ufir,
     & R_ekir,I_olip,L_ofir,R_uxam,R_eful,R_orul,
     & R_adam,R_ipam,R_okir,R_ilir,I_odup,
     & L_ikir,R_ebep,R_iful,R_urul,R_edam,R_opam,
     & R_ulir,R_upir,I_ofup,L_olir,R_ubep,
     & R_ikul,R_usul,R_efam,R_oram,R_erir,
     & R_urir,I_utup,L_arir,R_afep,R_ulul,R_evul,
     & R_okam,R_atam,R_esir,R_usir,I_udar,
     & L_asir,R_ofip,R_imul,R_uvul,R_elam,R_otam,
     & R_etir,R_evir,I_isar,L_atir,R_ufip,
     & R_omul,R_axul,R_ilam,R_utam,R_ixir,
     & R_abor,I_axar,L_exir,R_akip,R_umul,R_exul,
     & R_olam,R_avam,R_ibor,R_idor,I_axup,
     & L_ebor,R_orep,R_apul,R_ixul,R_ulam,R_evam,
     & R_udor,I_exip,I_uxar,R_ifor)
C FDD_logic.fgi(  55, 277):���������� ���,TVS2
      Call TVS_HANDLER(deltat,I_apet,I_epet,R_edas,R_idas
     &,
     & R_odas,R_udas,I_upet,I_ukir,R_afas,
     & R_efas,R_ifas,R_ofas,R_ufas,R_akas,R_ekas,
     & R_ikas,R_okas,R_ukas,R_alas,R_elas,R_ilas,
     & R_olas,R_oras,R_uras,I_aret,R_asas,R_esas,
     & R_isas,R_osas,I_eret,I_iret,I_oret,I_adir,
     & R_etas,R_itas,R_otas,R_utas,R_avas,R_evas,
     & R_ivas,R_ovas,R_uvas,R_axas,R_exas,R_ixas,
     & I_uret,I_aset,I_eset,I_edir,I_iset,I_oset,I_uset,
     & I_idir,I_atet,I_etet,I_itet,I_odir,I_otet,
     & I_utet,I_avet,I_udir,I_evet,I_ivet,I_ovet,
     & I_afir,I_uvet,I_axet,I_exet,I_efir,
     & R_afes,R_efes,R_ifes,R_ofes,R_ufes,R_akes,
     & R_ekes,R_ikes,I_ixet,I_oxet,I_uxet,I_ador,
     & I_abit,I_ebit,I_obit,I_edor,R_okes,R_ukes,
     & R_ales,R_eles,R_iles,R_oles,R_ules,R_ames,
     & R_emes,R_imes,I_ubit,I_adit,I_edit,I_erer,
     & I_idit,I_odit,I_udit,I_irer,R_eres,R_ires,
     & R_ores,R_ures,I_afit,I_efit,I_ifit,I_ober,
     & R_ases,R_eses,R_ises,R_oses,R_uses,R_ates,
     & R_otes,R_utes,R_aves,R_eves,I_ofit,I_ufit,
     & I_akit,I_ider,R_ives,R_oves,I_ekit,I_ikit,
     & I_okit,I_oder,R_uves,R_axes,I_ukit,I_alit,
     & I_elit,I_uder,I_ilit,I_ulit,I_afer,
     & R_oxes,R_uxes,I_amit,I_emit,I_imit,I_efer,
     & R_abis,R_ebis,R_ibis,R_obis,I_omit,I_umit,
     & I_apit,I_ifer,I_epit,I_ipit,R_ubis,R_adis,
     & R_edis,R_idis,I_opit,I_akir,R_afis,
     & R_efis,R_ifis,R_ufis,R_akis,R_ekis,R_ikis,
     & R_okis,R_ukis,R_alis,R_elis,R_ilis,R_olis,
     & R_ulis,R_imis,R_omis,R_umis,R_ipis,R_opis,
     & R_upis,R_aris,I_upit,I_arit,I_erit,I_imir,
     & R_eris,R_iris,R_asis,R_esis,R_isis,R_osis,
     & I_irit,I_orit,I_urit,I_omir,R_usis,R_atis,
     & R_otis,R_utis,R_avis,R_evis,I_asit,I_esit,
     & I_isit,I_umir,R_ivis,R_ovis,R_uvis,
     & R_axis,R_exis,R_ixis,R_abos,R_ebos,R_ibos,
     & R_obos,R_ubos,R_ados,R_edos,R_idos,R_odos,
     & R_udos,R_afos,R_efos,R_ofos,R_ekos,R_ikos,
     & R_okos,R_ukos,R_alos,R_elos,R_ilos,R_olos,
     & I_osit,I_usit,I_atit,I_etit,I_itit,I_otit,I_apir,
     & I_epir,R_ulos,R_amos,R_emos,R_imos,
     & R_umos,I_utit,R_apos,R_epos,I_avit,I_evit,I_ivit,
     & R_opos,R_upos,R_eros,R_iros,R_oros,I_ovit,
     & I_uvit,R_esos,R_isos,R_usos,R_atos,R_etos,
     & R_utos,R_avos,R_evos,R_ovos,R_uvos,
     & I_ipir,I_opir,R_ixos,R_oxos,R_abus,
     & R_ebus,R_ibus,R_obus,R_adus,R_edus,R_idus,
     & R_odus,R_udus,R_afus,R_efus,R_ifus,I_ixit,
     & I_oxit,R_ofus,I_uxit,I_irir,R_ufus,R_akus,
     & R_ekus,R_ikus,R_okus,I_abot,R_ukus,R_alus,
     & R_elus,R_ilus,I_ebot,R_amus,R_emus,R_imus,
     & R_omus,R_umus,I_ibot,I_isir,R_apus,
     & R_epus,R_ipus,R_opus,R_upus,R_arus,R_erus,
     & I_axir,R_irus,R_orus,I_ovir,
     & I_ivir,R_atus,R_etus,I_obot,I_ubot,R_itus,
     & R_otus,R_utus,R_avus,I_utir,R_akot,R_ekot,
     & I_ikot,I_okot,R_alot,R_elot,I_ilot,L_aber,
     & R_odat,R_ukor,R_etor,R_ofur,R_asur,
     & R_iber,R_uber,I_olot,L_eber,R_udat,
     & R_alor,R_itor,R_ufur,R_esur,R_eder,
     & R_ofer,R_ulot,R_amot,I_emot,I_imot,R_omot,R_umot,
     & I_apot,L_ader,R_efat,R_elor,R_otor,
     & R_akur,R_isur,R_aker,R_eker,I_epot,
     & L_ufer,R_ifat,R_ilor,R_utor,R_ekur,
     & R_osur,R_oker,R_uker,R_ipot,R_opot,I_upot,
     & I_arot,R_erot,R_irot,I_orot,L_iker,R_ofat,
     & R_ulor,R_evor,R_okur,R_atur,R_oler,
     & R_uler,I_urot,L_iler,R_ufat,R_amor,
     & R_ivor,R_ukur,R_etur,R_emer,R_imer,
     & R_asot,R_esot,R_isot,R_osot,R_usot,R_atot,I_etot,I_itot
     &,
     & I_otot,I_utot,I_avot,I_evot,R_ivot,R_ovot,R_uvot,R_exot
     &,
     & R_ixot,R_oxot,I_uxot,L_amer,R_akat,R_emor,
     & R_ovor,R_alur,R_itur,R_umer,R_aper,
     & I_abut,L_omer,R_ikat,R_imor,R_uvor,
     & R_elur,R_otur,R_iper,R_oper,I_ibut,
     & L_eper,R_okat,R_omor,R_axor,R_ilur,
     & R_utur,R_arer,R_orer,I_obut,L_uper,
     & R_ukat,R_umor,R_exor,R_olur,R_avur,
     & R_aser,R_eser,I_ubut,L_urer,R_alat,
     & R_epor,R_oxor,R_amur,R_ivur,R_oser,
     & R_user,I_adut,L_iser,R_elat,R_ipor,
     & R_uxor,R_emur,R_ovur,R_eter,R_iter,
     & I_uvir,I_edut,R_atat,R_etat,R_utat,
     & R_avat,I_odut,I_udut,I_afut,I_ofut,I_ufut,R_evat,
     & R_ivat,R_udet,I_oxir,R_afet,R_aket,
     & R_eket,R_iket,I_afor,R_oket,R_ekut,R_ikut,
     & R_okut,R_ukut,R_alut,R_elut,R_ilut,R_olut,R_ulut,R_amut
     &,
     & R_emut,R_imut,R_omut,R_umut,R_aput,R_eput,R_iput,R_oput
     &,
     & I_uput,I_arut,I_erut,I_irut,I_orut,I_urut,I_asut,I_esut
     &,
     & I_isut,I_osut,I_usut,I_atut,I_etut,I_itut,R_utut,R_avut
     &,
     & R_evut,R_ivut,R_ovut,R_uvut,R_axut,R_exut,R_ixut,R_oxut
     &,
     & R_uxut,R_abav,I_ibav,L_ater,R_ilet,R_iror,
     & R_ubur,R_epur,R_oxur,R_uter,R_aver,
     & I_obav,L_oter,R_olet,R_oror,R_adur,
     & R_ipur,R_uxur,R_iver,R_over,I_ubav,
     & L_ever,R_ulet,R_uror,R_edur,R_opur,
     & R_abas,R_axer,R_exer,I_amet,L_uver,
     & R_ebas,R_ofor,R_asor,R_idur,R_upur,R_oxer,
     & R_uxer,I_emet,L_ixer,R_ibas,R_ufor,R_esor,
     & R_odur,R_arur,R_ebir,R_ibir,I_imet,
     & L_abir,R_obas,R_akor,R_isor,R_udur,R_erur,
     & R_ubir,R_ifir,I_omet,L_obir,R_ubas,
     & R_ekor,R_osor,R_afur,R_irur,R_ufir,
     & R_ekir,I_umet,L_ofir,R_adas,R_ikor,R_usor,
     & R_efur,R_orur,R_okir,R_ilir,I_ufot,
     & L_ikir,R_idat,R_okor,R_ator,R_ifur,R_urur,
     & R_ulir,R_upir,I_ukot,L_olir,R_afat,
     & R_olor,R_avor,R_ikur,R_usur,R_erir,
     & R_urir,I_axot,L_arir,R_ekat,R_apor,R_ixor,
     & R_ulur,R_evur,R_esir,R_usir,I_akut,
     & L_asir,R_uket,R_opor,R_abur,R_imur,R_uvur,
     & R_etir,R_evir,I_otut,L_atir,R_alet,
     & R_upor,R_ebur,R_omur,R_axur,R_ixir,
     & R_abor,I_ebav,L_exir,R_elet,R_aror,R_ibur,
     & R_umur,R_exur,R_ibor,R_idor,I_ebut,
     & L_ebor,R_usat,R_eror,R_obur,R_apur,R_ixur,
     & R_udor,I_ibit,I_uxar,R_ifor)
C FDD_logic.fgi(  49, 277):���������� ���,TVS1
      !��������� I_(38) = control_tgC?? /65486/
      I_(38)=I0_omav
C control_tg.fgi( 117,  31):��������� ������������� IN
      !��������� I_(39) = control_tgC?? /65519/
      I_(39)=I0_umav
C control_tg.fgi( 117,  20):��������� ������������� IN
      !��������� I_(40) = control_tgC?? /65503/
      I_(40)=I0_apav
C control_tg.fgi( 117,  22):��������� ������������� IN
      !��������� I_(41) = control_tgC?? /65534/
      I_(41)=I0_epav
C control_tg.fgi( 117,  29):��������� ������������� IN
      I_(42) = 2
C control_tg.fgi( 120,  38):��������� ������������� IN (�������)
      I_(43) = 3
C control_tg.fgi( 120,  40):��������� ������������� IN (�������)
      L_(34)=.false.
C control_tg.fgi( 196,  53):��������� ���������� (�������)
      if(L_(34)) then
         I0_isav=0
      endif
      I_(48)=I0_isav
      L_(33)=I0_isav.ne.0
C control_tg.fgi( 204,  55):���������� ��������� 
      L_otuv=.false.
C control_tg.fgi( 142, 117):��������� ���������� (�������)
      L_(40)=.true.
C control_tg.fgi( 357, 290):��������� ���������� (�������)
      L_(42)=.false.
C control_tg.fgi(   7, 156):��������� ���������� (�������)
      if(L_(42)) then
         I0_asax=0
      endif
      I_(50)=I0_asax
      L_(41)=I0_asax.ne.0
C control_tg.fgi(  15, 158):���������� ��������� 
      !�����. �������� I0_itax = control_tgC?? /z'01000007'
C /
C control_tg.fgi( 253,  91):��������� �����
      !�����. �������� I0_utax = control_tgC?? /z'0100000A'
C /
C control_tg.fgi( 232,  92):��������� �����
      !�����. �������� I0_avax = control_tgC?? /z'01000003'
C /
C control_tg.fgi( 232,  94):��������� �����
      !�����. �������� I0_ovax = control_tgC?? /z'01000007'
C /
C control_tg.fgi( 253, 105):��������� �����
      !�����. �������� I0_axax = control_tgC?? /z'0100000A'
C /
C control_tg.fgi( 232, 106):��������� �����
      !�����. �������� I0_exax = control_tgC?? /z'01000003'
C /
C control_tg.fgi( 232, 108):��������� �����
      !��������� R_(7) = control_tgC?? /1/
      R_(7)=R0_oxax
C control_tg.fgi(  29,  74):���������
      R_(9) = 0
C control_tg.fgi(  46,  81):��������� (RE4) (�������)
      L_(43)=.false.
C control_tg.fgi(  38,  69):��������� ���������� (�������)
      R_(8) = 0.1
C control_tg.fgi(  40,  81):��������� (RE4) (�������)
      R_(19) = 750
C control_tg.fgi( 111, 180):��������� (RE4) (�������)
      L_amex=L_efex
C control_tg.fgi( 155, 189):������,m3m4_fidpos
      L_(45)=.false.
C control_tg.fgi(  98, 164):��������� ���������� (�������)
      !��������� R_(12) = control_tgC?? /3.3/
      R_(12)=R0_ubex
C control_tg.fgi(  95, 172):���������
      !��������� R_(15) = control_tgC?? /5.357/
      R_(15)=R0_adex
C control_tg.fgi( 118, 159):���������
      !��������� R_(13) = control_tgCvslow /1/
      R_(13)=R0_udex
C control_tg.fgi(  35, 180):���������,vslow
      !��������� R_(14) = control_tgCvnorm /0.1/
      R_(14)=R0_afex
C control_tg.fgi(  35, 183):���������,vnorm
      if(L_efex) then
         R_(20)=R_(13)
      else
         R_(20)=R_(14)
      endif
C control_tg.fgi(  44, 181):���� RE IN LO CH7
      R_(21) = 0
C control_tg.fgi( 109, 177):��������� (RE4) (�������)
      !��������� R_(23) = control_tgC497 /750/
      R_(23)=R0_ifex
C control_tg.fgi( 106, 177):���������
      L_(57) = L_ulex.OR.L_olex
C control_tg.fgi( 220, 134):���
      L_(64) = L_ulex.OR.L_olex
C control_tg.fgi( 220, 225):���
      !��������� R_(33) = control_tgC?? /20/
      R_(33)=R0_orex
C control_tg.fgi(  77, 284):���������
      !��������� R_(28) = control_tgC?? /-750/
      R_(28)=R0_utex
C control_tg.fgi(  91, 217):���������
      !��������� R_(27) = control_tgC?? /750/
      R_(27)=R0_avex
C control_tg.fgi( 106, 217):���������
      !��������� R_(30) = control_tgC?? /750/
      R_(30)=R0_evex
C control_tg.fgi( 104, 284):���������
      R_(26) = 0.0
C control_tg.fgi(  77, 216):��������� (RE4) (�������)
      !��������� R_(31) = control_tgC?? /730/
      R_(31)=R0_ivex
C control_tg.fgi(  90, 284):���������
      L0_udix=R0_efix.ne.R0_afix
      R0_afix=R0_efix
C control_tg.fgi( 176,  13):���������� ������������� ������
      L0_ilix=R0_ukix.ne.R0_okix
      R0_okix=R0_ukix
C control_th.fgi( 178,  35):���������� ������������� ������
      !�����. �������� I0_itix = control_thC?? /z'01000007'
C /
C control_th.fgi( 133,  33):��������� �����
      !�����. �������� I0_otix = control_thC?? /z'0100000A'
C /
C control_th.fgi( 112,  34):��������� �����
      !�����. �������� I0_utix = control_thC?? /z'01000003'
C /
C control_th.fgi( 112,  36):��������� �����
      !�����. �������� I0_ivix = control_thC?? /z'01000007'
C /
C control_th.fgi( 133,  49):��������� �����
      !�����. �������� I0_ovix = control_thC?? /z'0100000A'
C /
C control_th.fgi( 112,  50):��������� �����
      !�����. �������� I0_uvix = control_thC?? /z'01000003'
C /
C control_th.fgi( 112,  52):��������� �����
      !�����. �������� I0_adox = control_thC?? /z'01000085'
C /
C control_th.fgi( 116, 122):��������� �����
      !�����. �������� I0_edox = control_thC?? /z'01000007'
C /
C control_th.fgi( 116, 124):��������� �����
      R_(44) = 26000
C control_th.fgi( 115, 186):��������� (RE4) (�������)
      L_(119)=.false.
C control_th.fgi( 102, 173):��������� ���������� (�������)
      !��������� R_(38) = control_thC?? /33/
      R_(38)=R0_ifox
C control_th.fgi(  99, 181):���������
      !��������� R_(41) = control_thC?? /28.26/
      R_(41)=R0_ofox
C control_th.fgi( 130, 187):���������
      !��������� R_(39) = control_thCvslow /1/
      R_(39)=R0_okox
C control_th.fgi(  82, 193):���������,vslow
      !��������� R_(40) = control_thCvnorm /0.1/
      R_(40)=R0_ukox
C control_th.fgi(  82, 196):���������,vnorm
      R_(46) = 0.0
C control_th.fgi( 113, 186):��������� (RE4) (�������)
      !��������� R_(48) = control_thC497 /0.0/
      R_(48)=R0_alox
C control_th.fgi( 110, 186):���������
      L0_imux=R0_umux.ne.R0_omux
      R0_omux=R0_umux
C control_th.fgi( 203,  86):���������� ������������� ������
      L_(137)=.false.
C control_th.fgi( 241,  84):��������� ���������� (�������)
      L_(138)=.false.
C control_th.fgi( 241,  88):��������� ���������� (�������)
      L0_opux=R0_arux.ne.R0_upux
      R0_upux=R0_arux
C control_th.fgi( 203,  99):���������� ������������� ������
      L_(139)=.false.
C control_th.fgi( 241,  97):��������� ���������� (�������)
      L_(140)=.false.
C control_th.fgi( 241, 101):��������� ���������� (�������)
      L_(141)=.true.
C control_th.fgi( 362, 139):��������� ���������� (�������)
      L0_asebe=.false.
      I0_urebe=0

      if(L0_amade)then
        I0_urobe=1
         L0_irebe=.false.
         L0_ekebe=.false.
         L0_uvabe=.false.
         L0_avabe=.false.
         L0_otabe=.false.
         L0_etabe=.false.
         L0_usabe=.false.
         L0_isabe=.false.
         L0_asabe=.false.
         L0_arebe=.false.
         L0_opebe=.false.
         L0_epebe=.false.
         L0_umebe=.false.
         L0_imebe=.false.
         L0_amebe=.false.
         L0_olebe=.false.
         L0_elebe=.false.
         L0_ukebe=.false.
         L0_ikebe=.false.
         L0_ufebe=.false.
         L0_ifebe=.false.
         L0_afebe=.false.
         L0_odebe=.false.
         L0_edebe=.false.
         L0_ubebe=.false.
         L0_ibebe=.false.
         L0_abebe=.false.
         L0_oxabe=.false.
         L0_exabe=.false.
         L0_ivabe=.false.
      endif

      if(L0_irebe)I0_urebe=I0_urebe+1
      if(L0_ekebe)I0_urebe=I0_urebe+1
      if(L0_uvabe)I0_urebe=I0_urebe+1
      if(L0_avabe)I0_urebe=I0_urebe+1
      if(L0_otabe)I0_urebe=I0_urebe+1
      if(L0_etabe)I0_urebe=I0_urebe+1
      if(L0_usabe)I0_urebe=I0_urebe+1
      if(L0_isabe)I0_urebe=I0_urebe+1
      if(L0_asabe)I0_urebe=I0_urebe+1
      if(L0_arebe)I0_urebe=I0_urebe+1
      if(L0_opebe)I0_urebe=I0_urebe+1
      if(L0_epebe)I0_urebe=I0_urebe+1
      if(L0_umebe)I0_urebe=I0_urebe+1
      if(L0_imebe)I0_urebe=I0_urebe+1
      if(L0_amebe)I0_urebe=I0_urebe+1
      if(L0_olebe)I0_urebe=I0_urebe+1
      if(L0_elebe)I0_urebe=I0_urebe+1
      if(L0_ukebe)I0_urebe=I0_urebe+1
      if(L0_ikebe)I0_urebe=I0_urebe+1
      if(L0_ufebe)I0_urebe=I0_urebe+1
      if(L0_ifebe)I0_urebe=I0_urebe+1
      if(L0_afebe)I0_urebe=I0_urebe+1
      if(L0_odebe)I0_urebe=I0_urebe+1
      if(L0_edebe)I0_urebe=I0_urebe+1
      if(L0_ubebe)I0_urebe=I0_urebe+1
      if(L0_ibebe)I0_urebe=I0_urebe+1
      if(L0_abebe)I0_urebe=I0_urebe+1
      if(L0_oxabe)I0_urebe=I0_urebe+1
      if(L0_exabe)I0_urebe=I0_urebe+1
      if(L0_ivabe)I0_urebe=I0_urebe+1


      if(.not.L0_asebe.and.L0_orebe.and..not.L0_irebe)then
        I0_urobe=1
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L_(141).and..not.L0_ekebe)then
        I0_urobe=2
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_axabe.and..not.L0_uvabe)then
        I0_urobe=3
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_evabe.and..not.L0_avabe)then
        I0_urobe=4
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_utabe.and..not.L0_otabe)then
        I0_urobe=5
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_itabe.and..not.L0_etabe)then
        I0_urobe=6
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_atabe.and..not.L0_usabe)then
        I0_urobe=7
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_osabe.and..not.L0_isabe)then
        I0_urobe=8
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_esabe.and..not.L0_asabe)then
        I0_urobe=9
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_erebe.and..not.L0_arebe)then
        I0_urobe=10
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_upebe.and..not.L0_opebe)then
        I0_urobe=11
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ipebe.and..not.L0_epebe)then
        I0_urobe=12
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_apebe.and..not.L0_umebe)then
        I0_urobe=13
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_omebe.and..not.L0_imebe)then
        I0_urobe=14
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_emebe.and..not.L0_amebe)then
        I0_urobe=15
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ulebe.and..not.L0_olebe)then
        I0_urobe=16
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ilebe.and..not.L0_elebe)then
        I0_urobe=17
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_alebe.and..not.L0_ukebe)then
        I0_urobe=18
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_okebe.and..not.L0_ikebe)then
        I0_urobe=19
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_akebe.and..not.L0_ufebe)then
        I0_urobe=20
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ofebe.and..not.L0_ifebe)then
        I0_urobe=21
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_efebe.and..not.L0_afebe)then
        I0_urobe=22
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_udebe.and..not.L0_odebe)then
        I0_urobe=23
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_idebe.and..not.L0_edebe)then
        I0_urobe=24
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_adebe.and..not.L0_ubebe)then
        I0_urobe=25
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_obebe.and..not.L0_ibebe)then
        I0_urobe=26
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ebebe.and..not.L0_abebe)then
        I0_urobe=27
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_uxabe.and..not.L0_oxabe)then
        I0_urobe=28
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ixabe.and..not.L0_exabe)then
        I0_urobe=29
        L0_asebe=.true.
      endif

      if(.not.L0_asebe.and.L0_ovabe.and..not.L0_ivabe)then
        I0_urobe=30
        L0_asebe=.true.
      endif

      if(I0_urebe.eq.1)then
         L0_irebe=.false.
         L0_ekebe=.false.
         L0_uvabe=.false.
         L0_avabe=.false.
         L0_otabe=.false.
         L0_etabe=.false.
         L0_usabe=.false.
         L0_isabe=.false.
         L0_asabe=.false.
         L0_arebe=.false.
         L0_opebe=.false.
         L0_epebe=.false.
         L0_umebe=.false.
         L0_imebe=.false.
         L0_amebe=.false.
         L0_olebe=.false.
         L0_elebe=.false.
         L0_ukebe=.false.
         L0_ikebe=.false.
         L0_ufebe=.false.
         L0_ifebe=.false.
         L0_afebe=.false.
         L0_odebe=.false.
         L0_edebe=.false.
         L0_ubebe=.false.
         L0_ibebe=.false.
         L0_abebe=.false.
         L0_oxabe=.false.
         L0_exabe=.false.
         L0_ivabe=.false.
      else
         L0_irebe=L0_orebe
         L0_ekebe=L_(141)
         L0_uvabe=L0_axabe
         L0_avabe=L0_evabe
         L0_otabe=L0_utabe
         L0_etabe=L0_itabe
         L0_usabe=L0_atabe
         L0_isabe=L0_osabe
         L0_asabe=L0_esabe
         L0_arebe=L0_erebe
         L0_opebe=L0_upebe
         L0_epebe=L0_ipebe
         L0_umebe=L0_apebe
         L0_imebe=L0_omebe
         L0_amebe=L0_emebe
         L0_olebe=L0_ulebe
         L0_elebe=L0_ilebe
         L0_ukebe=L0_alebe
         L0_ikebe=L0_okebe
         L0_ufebe=L0_akebe
         L0_ifebe=L0_ofebe
         L0_afebe=L0_efebe
         L0_odebe=L0_udebe
         L0_edebe=L0_idebe
         L0_ubebe=L0_adebe
         L0_ibebe=L0_obebe
         L0_abebe=L0_ebebe
         L0_oxabe=L0_uxabe
         L0_exabe=L0_ixabe
         L0_ivabe=L0_ovabe
      endif
C control_th.fgi( 371, 112):��������� �������
      select case (I0_urobe)
      case (1)
        C255_orobe="����-� ��������� �� ����������"
      case (2)
        C255_orobe="��������� ������� ������ � ��������"
      case (3)
        C255_orobe="��������� �������, ������"
      case (4)
        C255_orobe="��������� ������� ������ � ��������"
      case (5)
        C255_orobe="��������� �������, ������"
      case (6)
        C255_orobe="������ � ����.�������� ���������"
      case (7)
        C255_orobe="������ �������"
      case (8)
        C255_orobe="������ �����������"
      case (9)
        C255_orobe="������ �����������"
      case (10)
        C255_orobe="������ �������"
      case (11)
        C255_orobe="������ � ����.�������� ���������"
      case (12)
        C255_orobe="???"
      case (13)
        C255_orobe="???"
      case (14)
        C255_orobe="???"
      case (15)
        C255_orobe="???"
      case (16)
        C255_orobe="???"
      case (17)
        C255_orobe="???"
      case (18)
        C255_orobe="???"
      case (19)
        C255_orobe="???"
      case (20)
        C255_orobe="???"
      case (21)
        C255_orobe="???"
      case (22)
        C255_orobe="???"
      case (23)
        C255_orobe="???"
      case (24)
        C255_orobe="???"
      case (25)
        C255_orobe="???"
      case (26)
        C255_orobe="???"
      case (27)
        C255_orobe="???"
      case (28)
        C255_orobe="???"
      case (29)
        C255_orobe="???"
      case (30)
        C255_orobe="???"
      end select
C control_th.fgi( 386, 141):MarkVI text out 30 variants,mo_text4
      !��������� R_(59) = control_thC?? /60/
      R_(59)=R0_irobe
C control_th.fgi(  81, 284):���������
      !��������� R_(53) = control_thC?? /26000/
      R_(53)=R0_utobe
C control_th.fgi(  95, 247):���������
      !��������� R_(52) = control_thC?? /-26000/
      R_(52)=R0_avobe
C control_th.fgi( 110, 247):���������
      !��������� R_(55) = control_thC?? /25150/
      R_(55)=R0_evobe
C control_th.fgi( 121, 284):���������
      !��������� R_(56) = control_thC?? /16956/
      R_(56)=R0_ivobe
C control_th.fgi( 108, 284):���������
      R_(51) = 0.0
C control_th.fgi(  81, 246):��������� (RE4) (�������)
      !��������� R_(57) = control_thC?? /8760/
      R_(57)=R0_ovobe
C control_th.fgi(  94, 284):���������
      I_(105) = 1
C common.fgi(  50,  18):��������� ������������� IN (�������)
      I_(106) = 0
C common.fgi(  50,  20):��������� ������������� IN (�������)
      L_(155)=I_apade.eq.I0_okube
C common.fgi(  86, 103):���������� �������������
      L_(156)=I_apade.eq.I0_ukube
C common.fgi(  86, 109):���������� �������������
      L_alube = L_(156).OR.L_(155)
C common.fgi(  97, 108):���
      if(L_alube) then
         I_ikube=I_(105)
      else
         I_ikube=I_(106)
      endif
C common.fgi(  54,  18):���� RE IN LO CH7
      if(L_alube) then
         L0_isix=L0_imux
      endif
      L_(135)=L0_isix
C control_th.fgi( 217,  85):�������-��������
      if(L_(138).or.L_(137)) then
         L_apux=(L_(138).or.L_apux).and..not.(L_(137))
      else
         if(L_(135).and..not.L0_olux) L_apux=.not.L_apux
      endif
      L0_olux=L_(135)
      L_ulux=.not.L_apux
C control_th.fgi( 247,  86):T �������
      L_emux=L_apux
C control_th.fgi( 272,  92):������,mainops_chkbox02_sw
      if(L_apux) then
         I_epux=1
      else
         I_epux=0
      endif
C control_th.fgi( 258,  88):��������� LO->1
      if(L_alube) then
         L0_esix=L0_opux
      endif
      L_(136)=L0_esix
C control_th.fgi( 217,  98):�������-��������
      if(L_(140).or.L_(139)) then
         L_erux=(L_(140).or.L_erux).and..not.(L_(139))
      else
         if(L_(136).and..not.L0_amux) L_erux=.not.L_erux
      endif
      L0_amux=L_(136)
      L_avux=.not.L_erux
C control_th.fgi( 247,  99):T �������
      L_ipux=L_erux
C control_th.fgi( 272, 105):������,mainops_chkbox01_sw
      L_(115) =.NOT.(L_ipux.OR.L_emux)
C control_th.fgi(  50, 165):���
      if(L_erux) then
         I_irux=1
      else
         I_irux=0
      endif
C control_th.fgi( 258, 101):��������� LO->1
      if(L_alube) then
         I0_amav=I_(50)
      endif
      I_(49)=I0_amav
C control_tg.fgi(  56, 157):�������-��������
      L_(39)=I_(49).eq.I0_itav
C control_tg.fgi(  75, 158):���������� �������������
      if(L_alube) then
         I0_ulav=I_(48)
      endif
      I_(37)=I0_ulav
C control_tg.fgi( 219,  54):�������-��������
      L_(32)=I_(37).eq.I0_olav
C control_tg.fgi( 228,  55):���������� �������������
      I_(107) = 1
C common.fgi(  50,  30):��������� ������������� IN (�������)
      if(L_alube) then
         I_(108)=I_ipade
      else
         I_(108)=I_(107)
      endif
C common.fgi(  54,  28):���� RE IN LO CH7
      select case (I_(108))
      case (1)
        C255_epade="����� ������������"
      case (2)
        C255_epade="�1.�����.����������� ������.������� 1"
      case (3)
        C255_epade="�2.�����.����������� ������.������� 2"
      case (4)
        C255_epade="�3.����.����������� ����.�������"
      case (5)
        C255_epade="�4.����.����������� ����.�������(�)"
      case (6)
        C255_epade="�5.���������� ����� ������.������� 1"
      case (7)
        C255_epade="�6.���������� ����� ������.������� 1"
      case (8)
        C255_epade="�7.����������� ������� ������"
      case (9)
        C255_epade="�8.����������� ������� ������"
      case (10)
        C255_epade="�9.������ ����� ������(�����)"
      case (11)
        C255_epade="�10.������ ����� �������(������)"
      case (12)
        C255_epade="�11.������ ������ �������(�����)"
      case (13)
        C255_epade="�12.������ ������ �������(������)"
      case (14)
        C255_epade="???"
      case (15)
        C255_epade="???"
      case (16)
        C255_epade="???"
      case (17)
        C255_epade="???"
      case (18)
        C255_epade="???"
      case (19)
        C255_epade="???"
      case (20)
        C255_epade="???"
      case (21)
        C255_epade="???"
      case (22)
        C255_epade="???"
      case (23)
        C255_epade="???"
      case (24)
        C255_epade="???"
      case (25)
        C255_epade="???"
      case (26)
        C255_epade="???"
      case (27)
        C255_epade="???"
      case (28)
        C255_epade="???"
      case (29)
        C255_epade="???"
      case (30)
        C255_epade="???"
      end select
C common.fgi( 153,  29):MarkVI text out 30 variants,sel_dev_menu
      L_(158)=I_(108).gt.I0_elube
C common.fgi(  86,  47):���������� �������������
      L_(159)=I_(108).eq.I0_ilube
C common.fgi(  86,  53):���������� �������������
      L_(162)=I_apade.eq.I0_olube
C common.fgi(  86,  59):���������� �������������
      L_(157)=I_apade.eq.I0_ulube
C common.fgi(  86,  65):���������� �������������
      L_(160)=.true.
C common.fgi( 127,  42):��������� ���������� (�������)
      select case (I_apade)
      case (1)
        C255_umade="����� ������"
      case (2)
        C255_umade="��������������"
      case (3)
        C255_umade="������"
      case (4)
        C255_umade="��������������� ������������"
      case (5)
        C255_umade="???"
      case (6)
        C255_umade="???"
      case (7)
        C255_umade="???"
      case (8)
        C255_umade="???"
      case (9)
        C255_umade="???"
      case (10)
        C255_umade="???"
      case (11)
        C255_umade="???"
      case (12)
        C255_umade="???"
      case (13)
        C255_umade="???"
      case (14)
        C255_umade="???"
      case (15)
        C255_umade="???"
      case (16)
        C255_umade="???"
      case (17)
        C255_umade="???"
      case (18)
        C255_umade="???"
      case (19)
        C255_umade="???"
      case (20)
        C255_umade="???"
      case (21)
        C255_umade="???"
      case (22)
        C255_umade="???"
      case (23)
        C255_umade="???"
      case (24)
        C255_umade="???"
      case (25)
        C255_umade="???"
      case (26)
        C255_umade="???"
      case (27)
        C255_umade="???"
      case (28)
        C255_umade="???"
      case (29)
        C255_umade="???"
      case (30)
        C255_umade="???"
      end select
C common.fgi( 152, 116):MarkVI text out 30 variants,sel_reg_menu
      if(C255_arade.ne.C255_opade) call spw_ShowFormat(C255_arade
     &,C255_upade,-1)
      C255_opade=C255_arade
C common.fgi( 154,   9):call_fmt
      L_(154) = (.NOT.L_elobe)
C control_th.fgi(  25, 265):���
C label 306  try306=try306-1
      if(L_(154)) then
         I0_ofube=0
      endif
      I_(104)=I0_ofube
      L_(153)=I0_ofube.ne.0
C control_th.fgi(  35, 267):���������� ��������� 
      if(L_alube) then
         I0_usix=I_(104)
      endif
      I_ubube=I0_usix
C control_th.fgi(  59, 266):�������-��������
      L_obube=I_ubube.eq.1
      L_ibube=I_ubube.eq.2
      L_ebube=I_ubube.eq.3
      L_abube=I_ubube.eq.4
C control_th.fgi(  80, 267):���������� �� 4
      if(L_obube) then
         R_(58)=R_(59)
      endif
C control_th.fgi(  84, 283):���� � ������������� �������
      if(L_ibube) then
         R_(58)=R_(57)
      endif
C control_th.fgi(  97, 283):���� � ������������� �������
      if(L_ebube) then
         R_(58)=R_(56)
      endif
C control_th.fgi( 111, 283):���� � ������������� �������
      if(L_abube) then
         R_(58)=R_(55)
      endif
C control_th.fgi( 124, 283):���� � ������������� �������
      R_(1)=R_(58)
C control_th.fgi(  84, 283):������-�������: ���������� ��� �������������� ������
      if(L_(154)) then
         I0_ifube=0
      endif
      I_(103)=I0_ifube
      L_(152)=I0_ifube.ne.0
C control_th.fgi(  35, 236):���������� ��������� 
      if(L_alube) then
         I0_osix=I_(103)
      endif
      I_(102)=I0_osix
C control_th.fgi(  59, 235):�������-��������
      L_(151)=I_(102).eq.1
      L_oxobe=I_(102).eq.2
      L_ixobe=I_(102).eq.3
      L_exobe=I_(102).eq.4
C control_th.fgi(  77, 236):���������� �� 4
      if(L_(151)) then
         R_(54)=R_(51)
      endif
C control_th.fgi(  84, 246):���� � ������������� �������
      if(L_oxobe) then
         R_(54)=R_(53)
      endif
C control_th.fgi(  98, 246):���� � ������������� �������
      if(L_ixobe) then
         R_(54)=R_(52)
      endif
C control_th.fgi( 113, 246):���� � ������������� �������
      R_(50) = (-R_(1)) + R_efube
C control_th.fgi( 134, 283):��������
      if(L_exobe) then
         R_(54)=R_(50)
      endif
C control_th.fgi( 123, 246):���� � ������������� �������
      R_(2)=R_(54)
C control_th.fgi(  84, 246):������-�������: ���������� ��� �������������� ������
      R_uxobe = R_(1) + R_(2)
C control_th.fgi( 134, 290):��������
      R_(43) = R_uxobe + (-R_elox)
C control_th.fgi(  58, 178):��������
      L_(118)=R_(43).gt.R0_ekox
C control_th.fgi(  67, 182):���������� >
      I_(96)=0
      if(L_(118))I_(96)=1
C control_th.fgi(  77, 182):lo2digit
      L_(117)=R_(43).lt.R0_akox
C control_th.fgi(  67, 176):���������� <
      I_(95)=0
      if(L_(117))I_(95)=-1
C control_th.fgi(  77, 176):lo2digit
      I_(97) = I_(96) + I_(95)
C control_th.fgi(  93, 179):��������
      R_(47) = R_(38) * I_(97)
C control_th.fgi( 102, 180):����������
      L_(95)=R0_irix.lt.R0_omix
C control_th.fgi( 271,  57):���������� <
      L_(96) = L_apix.OR.L_(95)
C control_th.fgi( 283,  62):���
      L_(93)=L_(96).and..not.L0_ulix
      L0_ulix=L_(96)
C control_th.fgi( 290,  62):������������  �� 1 ���
      L_(90) = L_(93).OR.L0_ilix.OR.L_olox
C control_th.fgi( 199,  56):���
      if(L_(90)) then
         I0_elix=0
      endif
      I_(86)=I0_elix
      L_(80)=I0_elix.ne.0
C control_th.fgi( 208,  58):���������� ��������� 
      if(L_alube) then
         I0_asix=I_(86)
      endif
      I_(88)=I0_asix
C control_th.fgi( 217,  57):�������-��������
      L_(104)=I_(88).eq.1
      L_(103)=I_(88).eq.2
      L_(102)=I_(88).eq.3
      L_(101)=I_(88).eq.4
C control_th.fgi( 223,  58):���������� �� 4
      I_(84)=0
      if(L_(104))I_(84)=1
C control_th.fgi( 233,  61):lo2digit
      I_(83)=0
      if(L_(103))I_(83)=-1
C control_th.fgi( 233,  59):lo2digit
      I_(80) = I_(84) + I_(83)
C control_th.fgi( 238,  60):��������
      R_(36)=I_(80)
C control_th.fgi( 245,  60):��������� IN->RE
      R0_irix=R0_irix+deltat/R0_erix*R_(36)
      if(R0_irix.gt.R0_arix) then
         R0_irix=R0_arix
      elseif(R0_irix.lt.R0_orix) then
         R0_irix=R0_orix
      endif
C control_th.fgi( 257,  58):����������
      L_apix=R0_irix.gt.R0_umix
C control_th.fgi( 271,  63):���������� >
C sav1=L_(96)
      L_(96) = L_apix.OR.L_(95)
C control_th.fgi( 283,  62):recalc:���
C if(sav1.ne.L_(96) .and. try424.gt.0) goto 424
      L_(94)=R0_opix.lt.R0_amix
C control_th.fgi( 271,  36):���������� <
      L_(92) = L_emix.OR.L_(94)
C control_th.fgi( 283,  41):���
      L_(91)=L_(92).and..not.L0_olix
      L0_olix=L_(92)
C control_th.fgi( 290,  41):������������  �� 1 ���
      L_(89) = L_(91).OR.L0_ilix.OR.L_olox
C control_th.fgi( 199,  35):���
      if(L_(89)) then
         I0_alix=0
      endif
      I_(85)=I0_alix
      L_(79)=I0_alix.ne.0
C control_th.fgi( 208,  37):���������� ��������� 
      if(L_alube) then
         I0_urix=I_(85)
      endif
      I_(87)=I0_urix
C control_th.fgi( 217,  36):�������-��������
      L_(100)=I_(87).eq.1
      L_(99)=I_(87).eq.2
      L_(98)=I_(87).eq.3
      L_(97)=I_(87).eq.4
C control_th.fgi( 223,  37):���������� �� 4
      I_(82)=0
      if(L_(100))I_(82)=1
C control_th.fgi( 233,  40):lo2digit
      I_(81)=0
      if(L_(99))I_(81)=-1
C control_th.fgi( 233,  38):lo2digit
      I_(79) = I_(82) + I_(81)
C control_th.fgi( 238,  39):��������
      R_(35)=I_(79)
C control_th.fgi( 245,  39):��������� IN->RE
      R0_opix=R0_opix+deltat/R0_ipix*R_(35)
      if(R0_opix.gt.R0_epix) then
         R0_opix=R0_epix
      elseif(R0_opix.lt.R0_upix) then
         R0_opix=R0_upix
      endif
C control_th.fgi( 257,  37):����������
      L_emix=R0_opix.gt.R0_imix
C control_th.fgi( 271,  42):���������� >
C sav1=L_(92)
      L_(92) = L_emix.OR.L_(94)
C control_th.fgi( 283,  41):recalc:���
C if(sav1.ne.L_(92) .and. try462.gt.0) goto 462
      L_(78) =.NOT.(L_apix.OR.L_emix)
C control_th.fgi(  50, 157):���
      L_(120) = L_(115).OR.L_(78)
C control_th.fgi(  58, 164):���
      if(R_(41).ge.0.0) then
         R_(42)=R_elox/max(R_(41),1.0e-10)
      else
         R_(42)=R_elox/min(R_(41),-1.0e-10)
      endif
C control_th.fgi( 135, 189):�������� ����������
      I_ufox=R_(42)
C control_th.fgi( 144, 189):��������� RE->IN
      R_(37)=I_ufox
C control_th.fgi(  70, 119):��������� IN->RE
      L_(108)=R_(37).gt.R0_ixix
C control_th.fgi(  83, 119):���������� >
      L_(107)=R_(37).lt.R0_exix
C control_th.fgi(  83, 113):���������� <
      L_umobe = L_(108).AND.L_(107)
C control_th.fgi(  99, 118):�
      L_(114)=R_(37).gt.R0_obox
C control_th.fgi(  83, 107):���������� >
      L_(113)=R_(37).lt.R0_ibox
C control_th.fgi(  83, 101):���������� <
      L_opobe = L_(114).AND.L_(113)
C control_th.fgi(  99, 106):�
      L_(112)=R_(37).gt.R0_ebox
C control_th.fgi(  83,  95):���������� >
      L_(111)=R_(37).lt.R0_abox
C control_th.fgi(  83,  89):���������� <
      L_isibe = L_(112).AND.L_(111)
C control_th.fgi(  99,  94):�
      L_(110)=R_(37).gt.R0_uxix
C control_th.fgi(  83,  83):���������� >
      L_(109)=R_(37).lt.R0_oxix
C control_th.fgi(  83,  77):���������� <
      L_etibe = L_(110).AND.L_(109)
C control_th.fgi(  99,  82):�
      L_ikox = L_umobe.OR.L_opobe.OR.L_isibe.OR.L_etibe
C control_th.fgi( 117,  72):���
      I_(94)=0
      if(L_umobe)I_(94)=1
C control_th.fgi( 126,  66):lo2digit
      I_(93)=0
      if(L_opobe)I_(93)=2
C control_th.fgi( 126,  64):lo2digit
      I_(92)=0
      if(L_isibe)I_(92)=3
C control_th.fgi( 126,  62):lo2digit
      I_(91)=0
      if(L_etibe)I_(91)=4
C control_th.fgi( 126,  60):lo2digit
      I_efox = I_(94) + I_(93) + I_(92) + I_(91)
C control_th.fgi( 132,  63):��������
      L_(116)=I_ubube.eq.I_efox
C control_th.fgi(  41, 186):���������� �������������
      L_ilox = L_ikox.AND.L_(116)
C control_th.fgi(  50, 190):�
      if(L_ilox) then
         R_(45)=R_(39)
      else
         R_(45)=R_(40)
      endif
C control_th.fgi(  91, 194):���� RE IN LO CH7
      if(L_(119)) then
         R_elox=R_(48)
      elseif(L_(120)) then
         R_elox=R_elox
      else
         R_elox=R_elox+deltat/R_(45)*R_(47)
      endif
      if(R_elox.gt.R_(44)) then
         R_elox=R_(44)
      elseif(R_elox.lt.R_(46)) then
         R_elox=R_(46)
      endif
C control_th.fgi( 113, 180):����������
C sav1=R_(43)
      R_(43) = R_uxobe + (-R_elox)
C control_th.fgi(  58, 178):recalc:��������
C if(sav1.ne.R_(43) .and. try406.gt.0) goto 406
      R_efube=R_elox
C control_th.fgi( 159, 180):������,truck_x_coord_mm
C sav1=R_(50)
      R_(50) = (-R_(1)) + R_efube
C control_th.fgi( 134, 283):recalc:��������
C if(sav1.ne.R_(50) .and. try393.gt.0) goto 393
      R0_udube=(R_efube-R_(60)+R0_idube*R0_udube)/(R0_idube
     &+deltat)
      R0_odube=R_efube
C control_th.fgi(  85, 215):���������������� ����� 
      if(R0_udube.gt.R0_uvobe) then
         R_(49)=R0_udube-R0_uvobe
      elseif(R0_udube.le.-R0_uvobe) then
         R_(49)=R0_udube+R0_uvobe
      else
         R_(49)=0.0
      endif
C control_th.fgi(  99, 215):���� ������������������
      R_edube=abs(R_(49))
C control_th.fgi( 112, 215):���������� ��������
      L_olox=R_edube.gt.R0_atix
C control_th.fgi( 126, 207):���������� >
C sav1=L_(90)
      L_(90) = L_(93).OR.L0_ilix.OR.L_olox
C control_th.fgi( 199,  56):recalc:���
C if(sav1.ne.L_(90) .and. try428.gt.0) goto 428
C sav1=L_(89)
      L_(89) = L_(91).OR.L0_ilix.OR.L_olox
C control_th.fgi( 199,  35):recalc:���
C if(sav1.ne.L_(89) .and. try466.gt.0) goto 466
      if(L_olox) then
         I0_akix=0
      endif
      I_(68)=I0_akix
      L_(77)=I0_akix.ne.0
C control_tg.fgi(  38, 267):���������� ��������� 
      if(L_alube) then
         I0_emav=I_(68)
      endif
      I_ibix=I0_emav
C control_tg.fgi(  56, 266):�������-��������
      L_ebix=I_ibix.eq.1
      L_(71)=I_ibix.eq.2
      L_(70)=I_ibix.eq.3
      L_esube=I_ibix.eq.4
C control_tg.fgi(  73, 267):���������� �� 4
      if(L_ebix) then
         R_(32)=R_(33)
      endif
C control_tg.fgi(  80, 283):���� � ������������� �������
      L_erube=I_ibix.eq.I0_urax
C control_tg.fgi(  74, 249):���������� �������������
      L_usuv=R_odix.gt.R0_irav
C control_tg.fgi( 118, 105):���������� >
      L_(29)=L_erube.and..not.L0_urav
      L0_urav=L_erube
C control_tg.fgi(  85, 244):������������  �� 1 ���
      L0_asav=(L_(29).or.L0_asav).and..not.(L_usuv)
      L_(30)=.not.L0_asav
C control_tg.fgi(  94, 242):RS �������
      L_(31) = L_erube.AND.L_(30)
C control_tg.fgi(  84, 259):�
      L_abix = L_(71).OR.L_esube.OR.L_(31)
C control_tg.fgi(  90, 264):���
      if(L_abix) then
         R_(32)=R_(31)
      endif
C control_tg.fgi(  93, 283):���� � ������������� �������
      L_uxex = L_(70).OR.L0_asav
C control_tg.fgi( 102, 265):���
      if(L_uxex) then
         R_(32)=R_(30)
      endif
C control_tg.fgi( 107, 283):���� � ������������� �������
      R_(3)=R_(32)
C control_tg.fgi(  80, 283):������-�������: ���������� ��� �������������� ������
      if(L_olox) then
         I0_ufix=0
      endif
      I_(67)=I0_ufix
      L_(76)=I0_ufix.ne.0
C control_tg.fgi(  38, 206):���������� ��������� 
      if(L_alube) then
         I0_imav=I_(67)
      endif
      I_(64)=I0_imav
C control_tg.fgi(  56, 205):�������-��������
      L_(69)=I_(64).eq.1
      L_ixex=I_(64).eq.2
      L_exex=I_(64).eq.3
      L_axex=I_(64).eq.4
C control_tg.fgi(  73, 206):���������� �� 4
      if(L_(69)) then
         R_(29)=R_(26)
      endif
C control_tg.fgi(  80, 216):���� � ������������� �������
      if(L_ixex) then
         R_(29)=R_(28)
      endif
C control_tg.fgi(  94, 216):���� � ������������� �������
      if(L_exex) then
         R_(29)=R_(27)
      endif
C control_tg.fgi( 109, 216):���� � ������������� �������
      R_(25) = (-R_(3)) + R_odix
C control_tg.fgi( 130, 283):��������
      if(L_axex) then
         R_(29)=R_(25)
      endif
C control_tg.fgi( 119, 216):���� � ������������� �������
      R_(4)=R_(29)
C control_tg.fgi(  80, 216):������-�������: ���������� ��� �������������� ������
      R_oxex = R_(3) + R_(4)
C control_tg.fgi( 130, 287):��������
      R_(18) = R_oxex + (-R_ofex)
C control_tg.fgi(  33, 169):��������
      L_orube=R_(18).gt.R0_odex
C control_tg.fgi(  44, 173):���������� >
      I_(56)=0
      if(L_orube)I_(56)=1
C control_tg.fgi(  73, 173):lo2digit
      L_(44)=R_(18).lt.R0_idex
C control_tg.fgi(  44, 167):���������� <
      I_(55)=0
      if(L_(44))I_(55)=-1
C control_tg.fgi(  73, 167):lo2digit
      I_(57) = I_(56) + I_(55)
C control_tg.fgi(  89, 170):��������
      R_(22) = R_(12) * I_(57)
C control_tg.fgi(  98, 171):����������
      L_(19)=R0_ukav.lt.R0_afav
C control_tg.fgi( 275,  35):���������� <
      L_(20) = L_ifav.OR.L_(19)
C control_tg.fgi( 287,  40):���
      L_(17)=L_(20).and..not.L0_edav
      L0_edav=L_(20)
C control_tg.fgi( 294,  40):������������  �� 1 ���
      R0_edix=(R_odix-R_(34)+R0_ubix*R0_edix)/(R0_ubix+deltat
     &)
      R0_adix=R_odix
C control_tg.fgi(  41, 113):���������������� ����� 
      if(R0_edix.gt.R0_ovex) then
         R_(24)=R0_edix-R0_ovex
      elseif(R0_edix.le.-R0_ovex) then
         R_(24)=R0_edix+R0_ovex
      else
         R_(24)=0.0
      endif
C control_tg.fgi(  55, 113):���� ������������������
      R_obix=abs(R_(24))
C control_tg.fgi(  68, 113):���������� ��������
      L_emex=R_obix.gt.R0_orav
C control_tg.fgi( 118,  99):���������� >
      L_(75) = L_(17).OR.L0_udix.OR.L_emex
C control_tg.fgi( 193,  34):���
      if(L_(75)) then
         I0_ofix=0
      endif
      I_(66)=I0_ofix
      L_(74)=I0_ofix.ne.0
C control_tg.fgi( 204,  36):���������� ��������� 
      if(L_alube) then
         I0_ilav=I_(66)
      endif
      I_(59)=I0_ilav
C control_tg.fgi( 219,  35):�������-��������
      L_(54)=I_(59).eq.1
      L_(53)=I_(59).eq.2
      L_(52)=I_(59).eq.3
      L_(51)=I_(59).eq.4
C control_tg.fgi( 227,  36):���������� �� 4
      I_(36)=0
      if(L_(54))I_(36)=1
C control_tg.fgi( 237,  39):lo2digit
      I_(35)=0
      if(L_(53))I_(35)=-1
C control_tg.fgi( 237,  37):lo2digit
      I_(32) = I_(36) + I_(35)
C control_tg.fgi( 242,  38):��������
      R_(6)=I_(32)
C control_tg.fgi( 249,  38):��������� IN->RE
      R0_ukav=R0_ukav+deltat/R0_okav*R_(6)
      if(R0_ukav.gt.R0_ikav) then
         R0_ukav=R0_ikav
      elseif(R0_ukav.lt.R0_alav) then
         R0_ukav=R0_alav
      endif
C control_tg.fgi( 261,  36):����������
      L_ifav=R0_ukav.gt.R0_efav
C control_tg.fgi( 275,  41):���������� >
C sav1=L_(20)
      L_(20) = L_ifav.OR.L_(19)
C control_tg.fgi( 287,  40):recalc:���
C if(sav1.ne.L_(20) .and. try783.gt.0) goto 783
      L_(18)=R0_akav.lt.R0_idav
C control_tg.fgi( 275,  14):���������� <
      L_(16) = L_odav.OR.L_(18)
C control_tg.fgi( 287,  19):���
      L_(15)=L_(16).and..not.L0_adav
      L0_adav=L_(16)
C control_tg.fgi( 294,  19):������������  �� 1 ���
      L_(73) = L_(15).OR.L0_udix.OR.L_emex
C control_tg.fgi( 193,  13):���
      if(L_(73)) then
         I0_ifix=0
      endif
      I_(65)=I0_ifix
      L_(72)=I0_ifix.ne.0
C control_tg.fgi( 204,  15):���������� ��������� 
      if(L_alube) then
         I0_elav=I_(65)
      endif
      I_(58)=I0_elav
C control_tg.fgi( 219,  14):�������-��������
      L_(50)=I_(58).eq.1
      L_(49)=I_(58).eq.2
      L_(48)=I_(58).eq.3
      L_(47)=I_(58).eq.4
C control_tg.fgi( 227,  15):���������� �� 4
      I_(34)=0
      if(L_(50))I_(34)=1
C control_tg.fgi( 237,  18):lo2digit
      I_(33)=0
      if(L_(49))I_(33)=-1
C control_tg.fgi( 237,  16):lo2digit
      I_(31) = I_(34) + I_(33)
C control_tg.fgi( 242,  17):��������
      R_(5)=I_(31)
C control_tg.fgi( 249,  17):��������� IN->RE
      R0_akav=R0_akav+deltat/R0_ufav*R_(5)
      if(R0_akav.gt.R0_ofav) then
         R0_akav=R0_ofav
      elseif(R0_akav.lt.R0_ekav) then
         R0_akav=R0_ekav
      endif
C control_tg.fgi( 261,  15):����������
      L_odav=R0_akav.gt.R0_udav
C control_tg.fgi( 275,  20):���������� >
C sav1=L_(16)
      L_(16) = L_odav.OR.L_(18)
C control_tg.fgi( 287,  19):recalc:���
C if(sav1.ne.L_(16) .and. try850.gt.0) goto 850
      L_(6) =.NOT.(L_ifav.OR.L_odav)
C control_tg.fgi(  73, 145):���
      L_(46) = L_(39).OR.L_(6)
C control_tg.fgi(  87, 157):���
      if(L_(45)) then
         R_ofex=R_(23)
      elseif(L_(46)) then
         R_ofex=R_ofex
      else
         R_ofex=R_ofex+deltat/R_(20)*R_(22)
      endif
      if(R_ofex.gt.R_(19)) then
         R_ofex=R_(19)
      elseif(R_ofex.lt.R_(21)) then
         R_ofex=R_(21)
      endif
C control_tg.fgi( 109, 171):����������
C sav1=R_(18)
      R_(18) = R_oxex + (-R_ofex)
C control_tg.fgi(  33, 169):recalc:��������
C if(sav1.ne.R_(18) .and. try761.gt.0) goto 761
      R_odix=R_ofex
C control_tg.fgi( 155, 171):������,travers_y_coord_mm
C sav1=R_(25)
      R_(25) = (-R_(3)) + R_odix
C control_tg.fgi( 130, 283):recalc:��������
C if(sav1.ne.R_(25) .and. try748.gt.0) goto 748
C sav1=R0_edix
      R0_edix=(R_odix-R_(34)+R0_ubix*R0_edix)/(R0_ubix+deltat
     &)
      R0_adix=R_odix
C control_tg.fgi(  41, 113):recalc:���������������� ����� 
C if(sav1.ne.R0_edix .and. try787.gt.0) goto 787
      L_(36)=R_odix.gt.R0_usav
C control_tg.fgi( 118, 129):���������� >
      L_(35)=R_odix.lt.R0_osav
C control_tg.fgi( 118, 123):���������� <
      L_elobe = L_(36).AND.L_(35)
C control_tg.fgi( 134, 128):�
C sav1=L_(154)
      L_(154) = (.NOT.L_elobe)
C control_th.fgi(  25, 265):recalc:���
C if(sav1.ne.L_(154) .and. try306.gt.0) goto 306
      L_(150) = (.NOT.L_elobe)
C control_th.fgi( 362, 258):���
      L_akax = (.NOT.L_(36))
C control_tg.fgi( 134, 134):���
      if(R_odix.gt.R0_uvex) then
         R_idix=R_odix-R0_uvex
      elseif(R_odix.le.-R0_uvex) then
         R_idix=R_odix+R0_uvex
      else
         R_idix=0.0
      endif
C control_tg.fgi(  55, 122):���� ������������������
      L_(38)=R_odix.gt.R0_etav
C control_tg.fgi( 118, 117):���������� >
      L_avuv = (.NOT.L_(35)).AND.(.NOT.L_(38))
C control_tg.fgi( 134, 122):�
      L_(37)=R_odix.lt.R0_atav
C control_tg.fgi( 118, 111):���������� <
      L_etuv = L_(38).AND.L_(37)
C control_tg.fgi( 134, 112):�
      L0_orax=.false.
      I0_irax=0

      if(L0_amade)then
        I0_ikix=1
         L0_erax=.false.
         L0_ufax=.false.
         L0_ovuv=.false.
         L0_utuv=.false.
         L0_ituv=.false.
         L0_atuv=.false.
         L0_osuv=.false.
         L0_esuv=.false.
         L0_uruv=.false.
         L0_upax=.false.
         L0_ipax=.false.
         L0_apax=.false.
         L0_omax=.false.
         L0_emax=.false.
         L0_ulax=.false.
         L0_ilax=.false.
         L0_alax=.false.
         L0_okax=.false.
         L0_ekax=.false.
         L0_ifax=.false.
         L0_afax=.false.
         L0_odax=.false.
         L0_edax=.false.
         L0_ubax=.false.
         L0_ibax=.false.
         L0_abax=.false.
         L0_oxuv=.false.
         L0_exuv=.false.
         L0_uvuv=.false.
         L0_evuv=.false.
      endif

      if(L0_erax)I0_irax=I0_irax+1
      if(L0_ufax)I0_irax=I0_irax+1
      if(L0_ovuv)I0_irax=I0_irax+1
      if(L0_utuv)I0_irax=I0_irax+1
      if(L0_ituv)I0_irax=I0_irax+1
      if(L0_atuv)I0_irax=I0_irax+1
      if(L0_osuv)I0_irax=I0_irax+1
      if(L0_esuv)I0_irax=I0_irax+1
      if(L0_uruv)I0_irax=I0_irax+1
      if(L0_upax)I0_irax=I0_irax+1
      if(L0_ipax)I0_irax=I0_irax+1
      if(L0_apax)I0_irax=I0_irax+1
      if(L0_omax)I0_irax=I0_irax+1
      if(L0_emax)I0_irax=I0_irax+1
      if(L0_ulax)I0_irax=I0_irax+1
      if(L0_ilax)I0_irax=I0_irax+1
      if(L0_alax)I0_irax=I0_irax+1
      if(L0_okax)I0_irax=I0_irax+1
      if(L0_ekax)I0_irax=I0_irax+1
      if(L0_ifax)I0_irax=I0_irax+1
      if(L0_afax)I0_irax=I0_irax+1
      if(L0_odax)I0_irax=I0_irax+1
      if(L0_edax)I0_irax=I0_irax+1
      if(L0_ubax)I0_irax=I0_irax+1
      if(L0_ibax)I0_irax=I0_irax+1
      if(L0_abax)I0_irax=I0_irax+1
      if(L0_oxuv)I0_irax=I0_irax+1
      if(L0_exuv)I0_irax=I0_irax+1
      if(L0_uvuv)I0_irax=I0_irax+1
      if(L0_evuv)I0_irax=I0_irax+1


      if(.not.L0_orax.and.L_(40).and..not.L0_erax)then
        I0_ikix=1
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L_akax.and..not.L0_ufax)then
        I0_ikix=2
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L_elobe.and..not.L0_ovuv)then
        I0_ikix=3
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L_avuv.and..not.L0_utuv)then
        I0_ikix=4
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L_otuv.and..not.L0_ituv)then
        I0_ikix=5
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L_etuv.and..not.L0_atuv)then
        I0_ikix=6
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L_usuv.and..not.L0_osuv)then
        I0_ikix=7
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_isuv.and..not.L0_esuv)then
        I0_ikix=8
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_asuv.and..not.L0_uruv)then
        I0_ikix=9
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_arax.and..not.L0_upax)then
        I0_ikix=10
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_opax.and..not.L0_ipax)then
        I0_ikix=11
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_epax.and..not.L0_apax)then
        I0_ikix=12
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_umax.and..not.L0_omax)then
        I0_ikix=13
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_imax.and..not.L0_emax)then
        I0_ikix=14
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_amax.and..not.L0_ulax)then
        I0_ikix=15
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_olax.and..not.L0_ilax)then
        I0_ikix=16
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_elax.and..not.L0_alax)then
        I0_ikix=17
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_ukax.and..not.L0_okax)then
        I0_ikix=18
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_ikax.and..not.L0_ekax)then
        I0_ikix=19
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_ofax.and..not.L0_ifax)then
        I0_ikix=20
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_efax.and..not.L0_afax)then
        I0_ikix=21
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_udax.and..not.L0_odax)then
        I0_ikix=22
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_idax.and..not.L0_edax)then
        I0_ikix=23
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_adax.and..not.L0_ubax)then
        I0_ikix=24
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_obax.and..not.L0_ibax)then
        I0_ikix=25
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_ebax.and..not.L0_abax)then
        I0_ikix=26
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_uxuv.and..not.L0_oxuv)then
        I0_ikix=27
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_ixuv.and..not.L0_exuv)then
        I0_ikix=28
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_axuv.and..not.L0_uvuv)then
        I0_ikix=29
        L0_orax=.true.
      endif

      if(.not.L0_orax.and.L0_ivuv.and..not.L0_evuv)then
        I0_ikix=30
        L0_orax=.true.
      endif

      if(I0_irax.eq.1)then
         L0_erax=.false.
         L0_ufax=.false.
         L0_ovuv=.false.
         L0_utuv=.false.
         L0_ituv=.false.
         L0_atuv=.false.
         L0_osuv=.false.
         L0_esuv=.false.
         L0_uruv=.false.
         L0_upax=.false.
         L0_ipax=.false.
         L0_apax=.false.
         L0_omax=.false.
         L0_emax=.false.
         L0_ulax=.false.
         L0_ilax=.false.
         L0_alax=.false.
         L0_okax=.false.
         L0_ekax=.false.
         L0_ifax=.false.
         L0_afax=.false.
         L0_odax=.false.
         L0_edax=.false.
         L0_ubax=.false.
         L0_ibax=.false.
         L0_abax=.false.
         L0_oxuv=.false.
         L0_exuv=.false.
         L0_uvuv=.false.
         L0_evuv=.false.
      else
         L0_erax=L_(40)
         L0_ufax=L_akax
         L0_ovuv=L_elobe
         L0_utuv=L_avuv
         L0_ituv=L_otuv
         L0_atuv=L_etuv
         L0_osuv=L_usuv
         L0_esuv=L0_isuv
         L0_uruv=L0_asuv
         L0_upax=L0_arax
         L0_ipax=L0_opax
         L0_apax=L0_epax
         L0_omax=L0_umax
         L0_emax=L0_imax
         L0_ulax=L0_amax
         L0_ilax=L0_olax
         L0_alax=L0_elax
         L0_okax=L0_ukax
         L0_ekax=L0_ikax
         L0_ifax=L0_ofax
         L0_afax=L0_efax
         L0_odax=L0_udax
         L0_edax=L0_idax
         L0_ubax=L0_adax
         L0_ibax=L0_obax
         L0_abax=L0_ebax
         L0_oxuv=L0_uxuv
         L0_exuv=L0_ixuv
         L0_uvuv=L0_axuv
         L0_evuv=L0_ivuv
      endif
C control_tg.fgi( 371, 261):��������� �������
      select case (I0_ikix)
      case (1)
        C255_otex="��������� ������������"
      case (2)
        C255_otex="������� ������������"
      case (3)
        C255_otex="������� ���������"
      case (4)
        C255_otex="� ������� ���������"
      case (5)
        C255_otex="������ ��������� �������"
      case (6)
        C255_otex="��������� �������"
      case (7)
        C255_otex="������ ������������"
      case (8)
        C255_otex="???"
      case (9)
        C255_otex="???"
      case (10)
        C255_otex="???"
      case (11)
        C255_otex="???"
      case (12)
        C255_otex="???"
      case (13)
        C255_otex="???"
      case (14)
        C255_otex="???"
      case (15)
        C255_otex="???"
      case (16)
        C255_otex="???"
      case (17)
        C255_otex="???"
      case (18)
        C255_otex="???"
      case (19)
        C255_otex="???"
      case (20)
        C255_otex="???"
      case (21)
        C255_otex="???"
      case (22)
        C255_otex="???"
      case (23)
        C255_otex="???"
      case (24)
        C255_otex="???"
      case (25)
        C255_otex="???"
      case (26)
        C255_otex="???"
      case (27)
        C255_otex="???"
      case (28)
        C255_otex="???"
      case (29)
        C255_otex="???"
      case (30)
        C255_otex="???"
      end select
C control_tg.fgi( 386, 290):MarkVI text out 30 variants,tg_text1
      L_(27) = L_esube.AND.L_etuv
C control_tg.fgi( 109, 235):�
      R_(16) = R_(19) + (-R_ofex)
C control_tg.fgi( 124, 163):��������
      if(R_(15).ge.0.0) then
         R_(17)=R_(16)/max(R_(15),1.0e-10)
      else
         R_(17)=R_(16)/min(R_(15),-1.0e-10)
      endif
C control_tg.fgi( 131, 161):�������� ����������
      I_edex=R_(17)
C control_tg.fgi( 140, 161):��������� RE->IN
      L_(13) =.NOT.(L_odav)
C control_tg.fgi( 335,  12):���
      I_(28)=0
      if(L_(13))I_(28)=1
C control_tg.fgi( 341,  12):lo2digit
      I_(27)=0
      if(L_odav)I_(27)=2
C control_tg.fgi( 341,  10):lo2digit
      I_(62) = I_(28) + I_(27)
C control_tg.fgi( 347,  11):��������
      select case (I_(62))
      case (1)
        C255_osex="�����"
      case (2)
        C255_osex="� ������"
      case (3)
        C255_osex="� ������"
      case (4)
        C255_osex="��������"
      case (5)
        C255_osex="�� ����������"
      case (6)
        C255_osex="����� ������ � ����������"
      case (7)
        C255_osex="����� ������ ����������"
      case (8)
        C255_osex="???"
      case (9)
        C255_osex="???"
      case (10)
        C255_osex="???"
      case (11)
        C255_osex="???"
      case (12)
        C255_osex="???"
      case (13)
        C255_osex="???"
      case (14)
        C255_osex="???"
      case (15)
        C255_osex="???"
      case (16)
        C255_osex="???"
      case (17)
        C255_osex="???"
      case (18)
        C255_osex="???"
      case (19)
        C255_osex="???"
      case (20)
        C255_osex="???"
      case (21)
        C255_osex="???"
      case (22)
        C255_osex="???"
      case (23)
        C255_osex="???"
      case (24)
        C255_osex="???"
      case (25)
        C255_osex="???"
      case (26)
        C255_osex="???"
      case (27)
        C255_osex="???"
      case (28)
        C255_osex="???"
      case (29)
        C255_osex="???"
      case (30)
        C255_osex="???"
      end select
C control_tg.fgi( 361,  11):MarkVI text out 30 variants,tg_text7
      L_(9) = L_(50).AND.(.NOT.L_odav)
C control_tg.fgi( 304,  23):�
      I_(23)=0
      if(L_(9))I_(23)=8
C control_tg.fgi( 315,  24):lo2digit
      L_(8) = (.NOT.L_(18)).AND.L_(49)
C control_tg.fgi( 304,  11):�
      I_(22)=0
      if(L_(8))I_(22)=10
C control_tg.fgi( 315,  11):lo2digit
      L_(7) = (.NOT.L_(9)).AND.(.NOT.L_(8))
C control_tg.fgi( 310,  20):�
      I_(21)=0
      if(L_(7))I_(21)=6
C control_tg.fgi( 315,  20):lo2digit
      I_(60) = I_(23) + I_(22) + I_(21)
C control_tg.fgi( 323,  22):��������
      select case (I_(60))
      case (1)
        C255_esex="��� �����"
      case (2)
        C255_esex="���������� � UZ"
      case (3)
        C255_esex="������������"
      case (4)
        C255_esex="��� 380�"
      case (5)
        C255_esex="�������������"
      case (6)
        C255_esex="����� � ������"
      case (7)
        C255_esex="����� ����"
      case (8)
        C255_esex="������ ������"
      case (9)
        C255_esex="������ �� ����"
      case (10)
        C255_esex="������ �����"
      case (11)
        C255_esex="����� �������"
      case (12)
        C255_esex="����������������"
      case (13)
        C255_esex="����� ����"
      case (14)
        C255_esex="����� ������� �����"
      case (15)
        C255_esex="������ �������"
      case (16)
        C255_esex="������ ����������"
      case (17)
        C255_esex="�� ����������"
      case (18)
        C255_esex="???"
      case (19)
        C255_esex="???"
      case (20)
        C255_esex="???"
      case (21)
        C255_esex="???"
      case (22)
        C255_esex="???"
      case (23)
        C255_esex="???"
      case (24)
        C255_esex="???"
      case (25)
        C255_esex="???"
      case (26)
        C255_esex="???"
      case (27)
        C255_esex="???"
      case (28)
        C255_esex="???"
      case (29)
        C255_esex="???"
      case (30)
        C255_esex="???"
      end select
C control_tg.fgi( 361,  22):MarkVI text out 30 variants,tg_text9
      L_(14) =.NOT.(L_ifav)
C control_tg.fgi( 335,  39):���
      I_(30)=0
      if(L_(14))I_(30)=1
C control_tg.fgi( 341,  39):lo2digit
      I_(29)=0
      if(L_ifav)I_(29)=2
C control_tg.fgi( 341,  37):lo2digit
      I_(63) = I_(30) + I_(29)
C control_tg.fgi( 347,  38):��������
      select case (I_(63))
      case (1)
        C255_usex="�����"
      case (2)
        C255_usex="� ������"
      case (3)
        C255_usex="� ������"
      case (4)
        C255_usex="��������"
      case (5)
        C255_usex="�� ����������"
      case (6)
        C255_usex="����� ������ � ����������"
      case (7)
        C255_usex="����� ������ ����������"
      case (8)
        C255_usex="???"
      case (9)
        C255_usex="???"
      case (10)
        C255_usex="???"
      case (11)
        C255_usex="???"
      case (12)
        C255_usex="???"
      case (13)
        C255_usex="???"
      case (14)
        C255_usex="???"
      case (15)
        C255_usex="???"
      case (16)
        C255_usex="???"
      case (17)
        C255_usex="???"
      case (18)
        C255_usex="???"
      case (19)
        C255_usex="???"
      case (20)
        C255_usex="???"
      case (21)
        C255_usex="???"
      case (22)
        C255_usex="???"
      case (23)
        C255_usex="???"
      case (24)
        C255_usex="???"
      case (25)
        C255_usex="???"
      case (26)
        C255_usex="???"
      case (27)
        C255_usex="???"
      case (28)
        C255_usex="???"
      case (29)
        C255_usex="???"
      case (30)
        C255_usex="???"
      end select
C control_tg.fgi( 361,  38):MarkVI text out 30 variants,tg_text6
      L_(12) = L_(54).AND.(.NOT.L_ifav)
C control_tg.fgi( 304,  44):�
      I_(26)=0
      if(L_(12))I_(26)=8
C control_tg.fgi( 315,  45):lo2digit
      L_(11) = (.NOT.L_(19)).AND.L_(53)
C control_tg.fgi( 304,  32):�
      I_(25)=0
      if(L_(11))I_(25)=10
C control_tg.fgi( 315,  32):lo2digit
      L_(10) = (.NOT.L_(12)).AND.(.NOT.L_(11))
C control_tg.fgi( 310,  41):�
      I_(24)=0
      if(L_(10))I_(24)=6
C control_tg.fgi( 315,  41):lo2digit
      I_(61) = I_(26) + I_(25) + I_(24)
C control_tg.fgi( 323,  43):��������
      select case (I_(61))
      case (1)
        C255_isex="��� �����"
      case (2)
        C255_isex="���������� � UZ"
      case (3)
        C255_isex="������������"
      case (4)
        C255_isex="��� 380�"
      case (5)
        C255_isex="�������������"
      case (6)
        C255_isex="����� � ������"
      case (7)
        C255_isex="����� ����"
      case (8)
        C255_isex="������ ������"
      case (9)
        C255_isex="������ �� ����"
      case (10)
        C255_isex="������ �����"
      case (11)
        C255_isex="����� �������"
      case (12)
        C255_isex="����������������"
      case (13)
        C255_isex="����� ����"
      case (14)
        C255_isex="����� ������� �����"
      case (15)
        C255_isex="������ �������"
      case (16)
        C255_isex="������ ����������"
      case (17)
        C255_isex="�� ����������"
      case (18)
        C255_isex="???"
      case (19)
        C255_isex="???"
      case (20)
        C255_isex="???"
      case (21)
        C255_isex="???"
      case (22)
        C255_isex="???"
      case (23)
        C255_isex="???"
      case (24)
        C255_isex="???"
      case (25)
        C255_isex="???"
      case (26)
        C255_isex="???"
      case (27)
        C255_isex="???"
      case (28)
        C255_isex="???"
      case (29)
        C255_isex="???"
      case (30)
        C255_isex="???"
      end select
C control_tg.fgi( 361,  49):MarkVI text out 30 variants,tg_text8
      L_(66) = L_emex.AND.L_amex.AND.(.NOT.L_exex).AND.(.NOT.L_ixex
     &)
C control_tg.fgi( 237, 242):�
      L_imex = (.NOT.L_upex).AND.L_(66)
C control_tg.fgi( 244, 243):�
      L_(65) = L_exex.AND.L_emex.AND.(.NOT.L_(64))
C control_tg.fgi( 237, 233):�
      L_umex = (.NOT.L_upex).AND.L_(65)
C control_tg.fgi( 244, 234):�
      L_(63) = L_emex.AND.L_ixex.AND.(.NOT.L_(64))
C control_tg.fgi( 237, 227):�
      L_epex = (.NOT.L_upex).AND.L_(63)
C control_tg.fgi( 244, 228):�
      L_(62) = L_(64).AND.L_emex
C control_tg.fgi( 237, 222):�
      L_apex = (.NOT.L_upex).AND.L_(62)
C control_tg.fgi( 244, 223):�
      L_(59) = L_emex.AND.L_amex.AND.(.NOT.L_exex).AND.(.NOT.L_ixex
     &)
C control_tg.fgi( 237, 151):�
      L_ufex = (.NOT.L_elex).AND.L_(59)
C control_tg.fgi( 244, 152):�
      L_(58) = L_exex.AND.L_emex.AND.(.NOT.L_(57))
C control_tg.fgi( 237, 142):�
      L_ekex = (.NOT.L_elex).AND.L_(58)
C control_tg.fgi( 244, 143):�
      L_(56) = L_emex.AND.L_ixex.AND.(.NOT.L_(57))
C control_tg.fgi( 237, 136):�
      L_okex = (.NOT.L_elex).AND.L_(56)
C control_tg.fgi( 244, 137):�
      L_(55) = L_(57).AND.L_emex
C control_tg.fgi( 237, 131):�
      L_ikex = (.NOT.L_elex).AND.L_(55)
C control_tg.fgi( 244, 132):�
      L_uvax=L_emex
C control_tg.fgi( 284,  65):������,M4run
      if(L_uvax) then
         I_(52)=I0_axax
      else
         I_(52)=I0_exax
      endif
C control_tg.fgi( 239, 106):���� RE IN LO CH7
      if(L_ivax) then
         I_ixax=I0_ovax
      else
         I_ixax=I_(52)
      endif
C control_tg.fgi( 257, 105):���� RE IN LO CH7
      L_otax = L_emex.AND.L_(32)
C control_tg.fgi( 237,  56):�
      L_ukiv = L_uvax.AND.L_otax
C control_tg.fgi( 225,  73):�
      L_ixev = (.NOT.L_uvax).AND.(.NOT.L_otax)
C control_tg.fgi( 225,  77):�
      L_ovev = L_uvax.AND.(.NOT.L_otax)
C control_tg.fgi( 225,  81):�
      L_esiv =.NOT.(L_ukiv.OR.L_ixev.OR.L_ovev)
C control_tg.fgi( 268,  85):���
      L0_osiv=.false.
      I0_isiv=0

      if(L0_amade)then
        I0_asex=1
         L0_asiv=.false.
         L0_okiv=.false.
         L0_exev=.false.
         L0_ivev=.false.
         L0_avev=.false.
         L0_otev=.false.
         L0_etev=.false.
         L0_usev=.false.
         L0_isev=.false.
         L0_oriv=.false.
         L0_eriv=.false.
         L0_upiv=.false.
         L0_ipiv=.false.
         L0_apiv=.false.
         L0_omiv=.false.
         L0_emiv=.false.
         L0_uliv=.false.
         L0_iliv=.false.
         L0_aliv=.false.
         L0_ekiv=.false.
         L0_ufiv=.false.
         L0_ifiv=.false.
         L0_afiv=.false.
         L0_odiv=.false.
         L0_ediv=.false.
         L0_ubiv=.false.
         L0_ibiv=.false.
         L0_abiv=.false.
         L0_oxev=.false.
         L0_uvev=.false.
      endif

      if(L0_asiv)I0_isiv=I0_isiv+1
      if(L0_okiv)I0_isiv=I0_isiv+1
      if(L0_exev)I0_isiv=I0_isiv+1
      if(L0_ivev)I0_isiv=I0_isiv+1
      if(L0_avev)I0_isiv=I0_isiv+1
      if(L0_otev)I0_isiv=I0_isiv+1
      if(L0_etev)I0_isiv=I0_isiv+1
      if(L0_usev)I0_isiv=I0_isiv+1
      if(L0_isev)I0_isiv=I0_isiv+1
      if(L0_oriv)I0_isiv=I0_isiv+1
      if(L0_eriv)I0_isiv=I0_isiv+1
      if(L0_upiv)I0_isiv=I0_isiv+1
      if(L0_ipiv)I0_isiv=I0_isiv+1
      if(L0_apiv)I0_isiv=I0_isiv+1
      if(L0_omiv)I0_isiv=I0_isiv+1
      if(L0_emiv)I0_isiv=I0_isiv+1
      if(L0_uliv)I0_isiv=I0_isiv+1
      if(L0_iliv)I0_isiv=I0_isiv+1
      if(L0_aliv)I0_isiv=I0_isiv+1
      if(L0_ekiv)I0_isiv=I0_isiv+1
      if(L0_ufiv)I0_isiv=I0_isiv+1
      if(L0_ifiv)I0_isiv=I0_isiv+1
      if(L0_afiv)I0_isiv=I0_isiv+1
      if(L0_odiv)I0_isiv=I0_isiv+1
      if(L0_ediv)I0_isiv=I0_isiv+1
      if(L0_ubiv)I0_isiv=I0_isiv+1
      if(L0_ibiv)I0_isiv=I0_isiv+1
      if(L0_abiv)I0_isiv=I0_isiv+1
      if(L0_oxev)I0_isiv=I0_isiv+1
      if(L0_uvev)I0_isiv=I0_isiv+1


      if(.not.L0_osiv.and.L_esiv.and..not.L0_asiv)then
        I0_asex=1
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L_ukiv.and..not.L0_okiv)then
        I0_asex=2
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L_ixev.and..not.L0_exev)then
        I0_asex=3
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L_ovev.and..not.L0_ivev)then
        I0_asex=4
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_evev.and..not.L0_avev)then
        I0_asex=5
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_utev.and..not.L0_otev)then
        I0_asex=6
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_itev.and..not.L0_etev)then
        I0_asex=7
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_atev.and..not.L0_usev)then
        I0_asex=8
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_osev.and..not.L0_isev)then
        I0_asex=9
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_uriv.and..not.L0_oriv)then
        I0_asex=10
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_iriv.and..not.L0_eriv)then
        I0_asex=11
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_ariv.and..not.L0_upiv)then
        I0_asex=12
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_opiv.and..not.L0_ipiv)then
        I0_asex=13
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_epiv.and..not.L0_apiv)then
        I0_asex=14
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_umiv.and..not.L0_omiv)then
        I0_asex=15
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_imiv.and..not.L0_emiv)then
        I0_asex=16
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_amiv.and..not.L0_uliv)then
        I0_asex=17
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_oliv.and..not.L0_iliv)then
        I0_asex=18
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_eliv.and..not.L0_aliv)then
        I0_asex=19
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_ikiv.and..not.L0_ekiv)then
        I0_asex=20
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_akiv.and..not.L0_ufiv)then
        I0_asex=21
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_ofiv.and..not.L0_ifiv)then
        I0_asex=22
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_efiv.and..not.L0_afiv)then
        I0_asex=23
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_udiv.and..not.L0_odiv)then
        I0_asex=24
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_idiv.and..not.L0_ediv)then
        I0_asex=25
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_adiv.and..not.L0_ubiv)then
        I0_asex=26
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_obiv.and..not.L0_ibiv)then
        I0_asex=27
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_ebiv.and..not.L0_abiv)then
        I0_asex=28
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_uxev.and..not.L0_oxev)then
        I0_asex=29
        L0_osiv=.true.
      endif

      if(.not.L0_osiv.and.L0_axev.and..not.L0_uvev)then
        I0_asex=30
        L0_osiv=.true.
      endif

      if(I0_isiv.eq.1)then
         L0_asiv=.false.
         L0_okiv=.false.
         L0_exev=.false.
         L0_ivev=.false.
         L0_avev=.false.
         L0_otev=.false.
         L0_etev=.false.
         L0_usev=.false.
         L0_isev=.false.
         L0_oriv=.false.
         L0_eriv=.false.
         L0_upiv=.false.
         L0_ipiv=.false.
         L0_apiv=.false.
         L0_omiv=.false.
         L0_emiv=.false.
         L0_uliv=.false.
         L0_iliv=.false.
         L0_aliv=.false.
         L0_ekiv=.false.
         L0_ufiv=.false.
         L0_ifiv=.false.
         L0_afiv=.false.
         L0_odiv=.false.
         L0_ediv=.false.
         L0_ubiv=.false.
         L0_ibiv=.false.
         L0_abiv=.false.
         L0_oxev=.false.
         L0_uvev=.false.
      else
         L0_asiv=L_esiv
         L0_okiv=L_ukiv
         L0_exev=L_ixev
         L0_ivev=L_ovev
         L0_avev=L0_evev
         L0_otev=L0_utev
         L0_etev=L0_itev
         L0_usev=L0_atev
         L0_isev=L0_osev
         L0_oriv=L0_uriv
         L0_eriv=L0_iriv
         L0_upiv=L0_ariv
         L0_ipiv=L0_opiv
         L0_apiv=L0_epiv
         L0_omiv=L0_umiv
         L0_emiv=L0_imiv
         L0_uliv=L0_amiv
         L0_iliv=L0_oliv
         L0_aliv=L0_eliv
         L0_ekiv=L0_ikiv
         L0_ufiv=L0_akiv
         L0_ifiv=L0_ofiv
         L0_afiv=L0_efiv
         L0_odiv=L0_udiv
         L0_ediv=L0_idiv
         L0_ubiv=L0_adiv
         L0_ibiv=L0_obiv
         L0_abiv=L0_ebiv
         L0_oxev=L0_uxev
         L0_uvev=L0_axev
      endif
C control_tg.fgi( 371, 130):��������� �������
      select case (I0_asex)
      case (1)
        C255_urex="��������� ����� �� ����������"
      case (2)
        C255_urex="���� �������"
      case (3)
        C255_urex="���� � �����"
      case (4)
        C255_urex="���� ��������"
      case (5)
        C255_urex="???"
      case (6)
        C255_urex="???"
      case (7)
        C255_urex="???"
      case (8)
        C255_urex="???"
      case (9)
        C255_urex="???"
      case (10)
        C255_urex="???"
      case (11)
        C255_urex="???"
      case (12)
        C255_urex="???"
      case (13)
        C255_urex="???"
      case (14)
        C255_urex="???"
      case (15)
        C255_urex="???"
      case (16)
        C255_urex="???"
      case (17)
        C255_urex="???"
      case (18)
        C255_urex="???"
      case (19)
        C255_urex="???"
      case (20)
        C255_urex="???"
      case (21)
        C255_urex="???"
      case (22)
        C255_urex="???"
      case (23)
        C255_urex="???"
      case (24)
        C255_urex="???"
      case (25)
        C255_urex="???"
      case (26)
        C255_urex="???"
      case (27)
        C255_urex="???"
      case (28)
        C255_urex="???"
      case (29)
        C255_urex="???"
      case (30)
        C255_urex="???"
      end select
C control_tg.fgi( 386, 159):MarkVI text out 30 variants,tg_text3
      if(L_otax) then
         I_(51)=I0_utax
      else
         I_(51)=I0_avax
      endif
C control_tg.fgi( 239,  92):���� RE IN LO CH7
      if(L_ilex) then
         I_evax=I0_itax
      else
         I_evax=I_(51)
      endif
C control_tg.fgi( 257,  91):���� RE IN LO CH7
      L_(28) = L_erube.AND.L_usuv
C control_tg.fgi( 109, 231):�
      L_esav=(L_(27).or.L_esav).and..not.(L_(28))
      L_oxiv=.not.L_esav
C control_tg.fgi( 117, 233):RS �������
      L_alov=L_esav
C control_tg.fgi( 155, 235):������,capt_on
      L_(22)=L_oxiv.and..not.L0_opav
      L0_opav=L_oxiv
C control_tg.fgi(  86,  41):������������  �� 1 ���
      L_irex = (.NOT.L_(77))
C control_tg.fgi(  74, 225):���
      L_(68) = (.NOT.L_irex)
C control_tg.fgi( 220, 251):���
      L_(67) = L_emex.AND.L_(68).AND.(.NOT.L_amex).AND.(.NOT.L_ixex
     &).AND.(.NOT.L_exex)
C control_tg.fgi( 237, 251):�
      L_omex = (.NOT.L_upex).AND.L_(67)
C control_tg.fgi( 244, 252):�
      L_ipex = (.NOT.L_omex).AND.(.NOT.L_imex).AND.(.NOT.L_umex
     &).AND.(.NOT.L_epex).AND.(.NOT.L_apex).AND.(.NOT.L_upex
     &).AND.(.NOT.L_ilex)
C control_tg.fgi( 259, 210):�
      L0_oruv=.false.
      I0_iruv=0

      if(L0_amade)then
        I0_erex=1
         L0_aruv=.false.
         L0_ekuv=.false.
         L0_uvov=.false.
         L0_evov=.false.
         L0_avov=.false.
         L0_utov=.false.
         L0_itov=.false.
         L0_etov=.false.
         L0_atov=.false.
         L0_upuv=.false.
         L0_opuv=.false.
         L0_ipuv=.false.
         L0_apuv=.false.
         L0_omuv=.false.
         L0_emuv=.false.
         L0_uluv=.false.
         L0_iluv=.false.
         L0_aluv=.false.
         L0_okuv=.false.
         L0_ufuv=.false.
         L0_ifuv=.false.
         L0_afuv=.false.
         L0_oduv=.false.
         L0_eduv=.false.
         L0_ubuv=.false.
         L0_ibuv=.false.
         L0_abuv=.false.
         L0_oxov=.false.
         L0_exov=.false.
         L0_ivov=.false.
      endif

      if(L0_aruv)I0_iruv=I0_iruv+1
      if(L0_ekuv)I0_iruv=I0_iruv+1
      if(L0_uvov)I0_iruv=I0_iruv+1
      if(L0_evov)I0_iruv=I0_iruv+1
      if(L0_avov)I0_iruv=I0_iruv+1
      if(L0_utov)I0_iruv=I0_iruv+1
      if(L0_itov)I0_iruv=I0_iruv+1
      if(L0_etov)I0_iruv=I0_iruv+1
      if(L0_atov)I0_iruv=I0_iruv+1
      if(L0_upuv)I0_iruv=I0_iruv+1
      if(L0_opuv)I0_iruv=I0_iruv+1
      if(L0_ipuv)I0_iruv=I0_iruv+1
      if(L0_apuv)I0_iruv=I0_iruv+1
      if(L0_omuv)I0_iruv=I0_iruv+1
      if(L0_emuv)I0_iruv=I0_iruv+1
      if(L0_uluv)I0_iruv=I0_iruv+1
      if(L0_iluv)I0_iruv=I0_iruv+1
      if(L0_aluv)I0_iruv=I0_iruv+1
      if(L0_okuv)I0_iruv=I0_iruv+1
      if(L0_ufuv)I0_iruv=I0_iruv+1
      if(L0_ifuv)I0_iruv=I0_iruv+1
      if(L0_afuv)I0_iruv=I0_iruv+1
      if(L0_oduv)I0_iruv=I0_iruv+1
      if(L0_eduv)I0_iruv=I0_iruv+1
      if(L0_ubuv)I0_iruv=I0_iruv+1
      if(L0_ibuv)I0_iruv=I0_iruv+1
      if(L0_abuv)I0_iruv=I0_iruv+1
      if(L0_oxov)I0_iruv=I0_iruv+1
      if(L0_exov)I0_iruv=I0_iruv+1
      if(L0_ivov)I0_iruv=I0_iruv+1


      if(.not.L0_oruv.and.L0_eruv.and..not.L0_aruv)then
        I0_erex=1
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_ikuv.and..not.L0_ekuv)then
        I0_erex=2
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_axov.and..not.L0_uvov)then
        I0_erex=3
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_upex.and..not.L0_evov)then
        I0_erex=4
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_opex.and..not.L0_avov)then
        I0_erex=5
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_ipex.and..not.L0_utov)then
        I0_erex=6
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_otov.and..not.L0_itov)then
        I0_erex=7
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_epex.and..not.L0_etov)then
        I0_erex=8
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_apex.and..not.L0_atov)then
        I0_erex=9
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_umex.and..not.L0_upuv)then
        I0_erex=10
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_omex.and..not.L0_opuv)then
        I0_erex=11
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L_imex.and..not.L0_ipuv)then
        I0_erex=12
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_epuv.and..not.L0_apuv)then
        I0_erex=13
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_umuv.and..not.L0_omuv)then
        I0_erex=14
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_imuv.and..not.L0_emuv)then
        I0_erex=15
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_amuv.and..not.L0_uluv)then
        I0_erex=16
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_oluv.and..not.L0_iluv)then
        I0_erex=17
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_eluv.and..not.L0_aluv)then
        I0_erex=18
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_ukuv.and..not.L0_okuv)then
        I0_erex=19
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_akuv.and..not.L0_ufuv)then
        I0_erex=20
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_ofuv.and..not.L0_ifuv)then
        I0_erex=21
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_efuv.and..not.L0_afuv)then
        I0_erex=22
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_uduv.and..not.L0_oduv)then
        I0_erex=23
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_iduv.and..not.L0_eduv)then
        I0_erex=24
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_aduv.and..not.L0_ubuv)then
        I0_erex=25
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_obuv.and..not.L0_ibuv)then
        I0_erex=26
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_ebuv.and..not.L0_abuv)then
        I0_erex=27
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_uxov.and..not.L0_oxov)then
        I0_erex=28
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_ixov.and..not.L0_exov)then
        I0_erex=29
        L0_oruv=.true.
      endif

      if(.not.L0_oruv.and.L0_ovov.and..not.L0_ivov)then
        I0_erex=30
        L0_oruv=.true.
      endif

      if(I0_iruv.eq.1)then
         L0_aruv=.false.
         L0_ekuv=.false.
         L0_uvov=.false.
         L0_evov=.false.
         L0_avov=.false.
         L0_utov=.false.
         L0_itov=.false.
         L0_etov=.false.
         L0_atov=.false.
         L0_upuv=.false.
         L0_opuv=.false.
         L0_ipuv=.false.
         L0_apuv=.false.
         L0_omuv=.false.
         L0_emuv=.false.
         L0_uluv=.false.
         L0_iluv=.false.
         L0_aluv=.false.
         L0_okuv=.false.
         L0_ufuv=.false.
         L0_ifuv=.false.
         L0_afuv=.false.
         L0_oduv=.false.
         L0_eduv=.false.
         L0_ubuv=.false.
         L0_ibuv=.false.
         L0_abuv=.false.
         L0_oxov=.false.
         L0_exov=.false.
         L0_ivov=.false.
      else
         L0_aruv=L0_eruv
         L0_ekuv=L0_ikuv
         L0_uvov=L0_axov
         L0_evov=L_upex
         L0_avov=L_opex
         L0_utov=L_ipex
         L0_itov=L0_otov
         L0_etov=L_epex
         L0_atov=L_apex
         L0_upuv=L_umex
         L0_opuv=L_omex
         L0_ipuv=L_imex
         L0_apuv=L0_epuv
         L0_omuv=L0_umuv
         L0_emuv=L0_imuv
         L0_uluv=L0_amuv
         L0_iluv=L0_oluv
         L0_aluv=L0_eluv
         L0_okuv=L0_ukuv
         L0_ufuv=L0_akuv
         L0_ifuv=L0_ofuv
         L0_afuv=L0_efuv
         L0_oduv=L0_uduv
         L0_eduv=L0_iduv
         L0_ubuv=L0_aduv
         L0_ibuv=L0_obuv
         L0_abuv=L0_ebuv
         L0_oxov=L0_uxov
         L0_exov=L0_ixov
         L0_ivov=L0_ovov
      endif
C control_tg.fgi( 289, 261):��������� �������
      select case (I0_erex)
      case (1)
        C255_arex="��� �����"
      case (2)
        C255_arex="���������� � UZ"
      case (3)
        C255_arex="������������"
      case (4)
        C255_arex="��� 380�"
      case (5)
        C255_arex="�������������"
      case (6)
        C255_arex="����� � ������"
      case (7)
        C255_arex="����� ����"
      case (8)
        C255_arex="������ ������"
      case (9)
        C255_arex="������ �� ����"
      case (10)
        C255_arex="������ �����"
      case (11)
        C255_arex="����� �������"
      case (12)
        C255_arex="����������������"
      case (13)
        C255_arex="����� ����"
      case (14)
        C255_arex="����� ������� �����"
      case (15)
        C255_arex="������ �������"
      case (16)
        C255_arex="������ ����������"
      case (17)
        C255_arex="�� ����������"
      case (18)
        C255_arex="???"
      case (19)
        C255_arex="???"
      case (20)
        C255_arex="???"
      case (21)
        C255_arex="???"
      case (22)
        C255_arex="???"
      case (23)
        C255_arex="???"
      case (24)
        C255_arex="???"
      case (25)
        C255_arex="???"
      case (26)
        C255_arex="???"
      case (27)
        C255_arex="???"
      case (28)
        C255_arex="???"
      case (29)
        C255_arex="???"
      case (30)
        C255_arex="???"
      end select
C control_tg.fgi( 306, 290):MarkVI text out 30 variants,tg_text4
      L_(61) = (.NOT.L_irex)
C control_tg.fgi( 220, 160):���
      L_(60) = L_emex.AND.L_(61).AND.(.NOT.L_amex).AND.(.NOT.L_ixex
     &).AND.(.NOT.L_exex)
C control_tg.fgi( 237, 160):�
      L_akex = (.NOT.L_elex).AND.L_(60)
C control_tg.fgi( 244, 161):�
      L_ukex = (.NOT.L_akex).AND.(.NOT.L_ufex).AND.(.NOT.L_ekex
     &).AND.(.NOT.L_okex).AND.(.NOT.L_ikex).AND.(.NOT.L_elex
     &).AND.(.NOT.L_ilex)
C control_tg.fgi( 259, 119):�
      L0_esev=.false.
      I0_asev=0

      if(L0_amade)then
        I0_etex=1
         L0_orev=.false.
         L0_ukev=.false.
         L0_ixav=.false.
         L0_uvav=.false.
         L0_ovav=.false.
         L0_ivav=.false.
         L0_avav=.false.
         L0_utav=.false.
         L0_otav=.false.
         L0_irev=.false.
         L0_erev=.false.
         L0_arev=.false.
         L0_opev=.false.
         L0_epev=.false.
         L0_umev=.false.
         L0_imev=.false.
         L0_amev=.false.
         L0_olev=.false.
         L0_elev=.false.
         L0_ikev=.false.
         L0_akev=.false.
         L0_ofev=.false.
         L0_efev=.false.
         L0_udev=.false.
         L0_idev=.false.
         L0_adev=.false.
         L0_obev=.false.
         L0_ebev=.false.
         L0_uxav=.false.
         L0_axav=.false.
      endif

      if(L0_orev)I0_asev=I0_asev+1
      if(L0_ukev)I0_asev=I0_asev+1
      if(L0_ixav)I0_asev=I0_asev+1
      if(L0_uvav)I0_asev=I0_asev+1
      if(L0_ovav)I0_asev=I0_asev+1
      if(L0_ivav)I0_asev=I0_asev+1
      if(L0_avav)I0_asev=I0_asev+1
      if(L0_utav)I0_asev=I0_asev+1
      if(L0_otav)I0_asev=I0_asev+1
      if(L0_irev)I0_asev=I0_asev+1
      if(L0_erev)I0_asev=I0_asev+1
      if(L0_arev)I0_asev=I0_asev+1
      if(L0_opev)I0_asev=I0_asev+1
      if(L0_epev)I0_asev=I0_asev+1
      if(L0_umev)I0_asev=I0_asev+1
      if(L0_imev)I0_asev=I0_asev+1
      if(L0_amev)I0_asev=I0_asev+1
      if(L0_olev)I0_asev=I0_asev+1
      if(L0_elev)I0_asev=I0_asev+1
      if(L0_ikev)I0_asev=I0_asev+1
      if(L0_akev)I0_asev=I0_asev+1
      if(L0_ofev)I0_asev=I0_asev+1
      if(L0_efev)I0_asev=I0_asev+1
      if(L0_udev)I0_asev=I0_asev+1
      if(L0_idev)I0_asev=I0_asev+1
      if(L0_adev)I0_asev=I0_asev+1
      if(L0_obev)I0_asev=I0_asev+1
      if(L0_ebev)I0_asev=I0_asev+1
      if(L0_uxav)I0_asev=I0_asev+1
      if(L0_axav)I0_asev=I0_asev+1


      if(.not.L0_esev.and.L0_urev.and..not.L0_orev)then
        I0_etex=1
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_alev.and..not.L0_ukev)then
        I0_etex=2
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_oxav.and..not.L0_ixav)then
        I0_etex=3
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_elex.and..not.L0_uvav)then
        I0_etex=4
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_alex.and..not.L0_ovav)then
        I0_etex=5
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_ukex.and..not.L0_ivav)then
        I0_etex=6
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_evav.and..not.L0_avav)then
        I0_etex=7
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_okex.and..not.L0_utav)then
        I0_etex=8
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_ikex.and..not.L0_otav)then
        I0_etex=9
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_ekex.and..not.L0_irev)then
        I0_etex=10
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_akex.and..not.L0_erev)then
        I0_etex=11
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L_ufex.and..not.L0_arev)then
        I0_etex=12
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_upev.and..not.L0_opev)then
        I0_etex=13
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ipev.and..not.L0_epev)then
        I0_etex=14
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_apev.and..not.L0_umev)then
        I0_etex=15
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_omev.and..not.L0_imev)then
        I0_etex=16
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_emev.and..not.L0_amev)then
        I0_etex=17
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ulev.and..not.L0_olev)then
        I0_etex=18
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ilev.and..not.L0_elev)then
        I0_etex=19
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_okev.and..not.L0_ikev)then
        I0_etex=20
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ekev.and..not.L0_akev)then
        I0_etex=21
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ufev.and..not.L0_ofev)then
        I0_etex=22
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ifev.and..not.L0_efev)then
        I0_etex=23
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_afev.and..not.L0_udev)then
        I0_etex=24
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_odev.and..not.L0_idev)then
        I0_etex=25
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_edev.and..not.L0_adev)then
        I0_etex=26
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ubev.and..not.L0_obev)then
        I0_etex=27
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_ibev.and..not.L0_ebev)then
        I0_etex=28
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_abev.and..not.L0_uxav)then
        I0_etex=29
        L0_esev=.true.
      endif

      if(.not.L0_esev.and.L0_exav.and..not.L0_axav)then
        I0_etex=30
        L0_esev=.true.
      endif

      if(I0_asev.eq.1)then
         L0_orev=.false.
         L0_ukev=.false.
         L0_ixav=.false.
         L0_uvav=.false.
         L0_ovav=.false.
         L0_ivav=.false.
         L0_avav=.false.
         L0_utav=.false.
         L0_otav=.false.
         L0_irev=.false.
         L0_erev=.false.
         L0_arev=.false.
         L0_opev=.false.
         L0_epev=.false.
         L0_umev=.false.
         L0_imev=.false.
         L0_amev=.false.
         L0_olev=.false.
         L0_elev=.false.
         L0_ikev=.false.
         L0_akev=.false.
         L0_ofev=.false.
         L0_efev=.false.
         L0_udev=.false.
         L0_idev=.false.
         L0_adev=.false.
         L0_obev=.false.
         L0_ebev=.false.
         L0_uxav=.false.
         L0_axav=.false.
      else
         L0_orev=L0_urev
         L0_ukev=L0_alev
         L0_ixav=L0_oxav
         L0_uvav=L_elex
         L0_ovav=L_alex
         L0_ivav=L_ukex
         L0_avav=L0_evav
         L0_utav=L_okex
         L0_otav=L_ikex
         L0_irev=L_ekex
         L0_erev=L_akex
         L0_arev=L_ufex
         L0_opev=L0_upev
         L0_epev=L0_ipev
         L0_umev=L0_apev
         L0_imev=L0_omev
         L0_amev=L0_emev
         L0_olev=L0_ulev
         L0_elev=L0_ilev
         L0_ikev=L0_okev
         L0_akev=L0_ekev
         L0_ofev=L0_ufev
         L0_efev=L0_ifev
         L0_udev=L0_afev
         L0_idev=L0_odev
         L0_adev=L0_edev
         L0_obev=L0_ubev
         L0_ebev=L0_ibev
         L0_uxav=L0_abev
         L0_axav=L0_exav
      endif
C control_tg.fgi( 289, 170):��������� �������
      select case (I0_etex)
      case (1)
        C255_atex="��� �����"
      case (2)
        C255_atex="���������� � UZ"
      case (3)
        C255_atex="������������"
      case (4)
        C255_atex="��� 380�"
      case (5)
        C255_atex="�������������"
      case (6)
        C255_atex="����� � ������"
      case (7)
        C255_atex="����� ����"
      case (8)
        C255_atex="������ ������"
      case (9)
        C255_atex="������ �� ����"
      case (10)
        C255_atex="������ �����"
      case (11)
        C255_atex="����� �������"
      case (12)
        C255_atex="����������������"
      case (13)
        C255_atex="����� ����"
      case (14)
        C255_atex="����� ������� �����"
      case (15)
        C255_atex="������ �������"
      case (16)
        C255_atex="������ ����������"
      case (17)
        C255_atex="�� ����������"
      case (18)
        C255_atex="???"
      case (19)
        C255_atex="???"
      case (20)
        C255_atex="???"
      case (21)
        C255_atex="???"
      case (22)
        C255_atex="???"
      case (23)
        C255_atex="???"
      case (24)
        C255_atex="???"
      case (25)
        C255_atex="???"
      case (26)
        C255_atex="???"
      case (27)
        C255_atex="???"
      case (28)
        C255_atex="???"
      case (29)
        C255_atex="???"
      case (30)
        C255_atex="???"
      end select
C control_tg.fgi( 306, 199):MarkVI text out 30 variants,tg_text5
      L_(132) = L_olox.AND.L_ilox.AND.(.NOT.L_ixobe).AND.
     &(.NOT.L_oxobe)
C control_th.fgi( 237, 242):�
      L_imube = (.NOT.L_avux).AND.L_(132)
C control_th.fgi( 244, 243):�
      L_(125) = L_olox.AND.L_ilox.AND.(.NOT.L_ixobe).AND.
     &(.NOT.L_oxobe)
C control_th.fgi( 237, 149):�
      L_amube = (.NOT.L_ulux).AND.L_(125)
C control_th.fgi( 244, 150):�
      L_(105) = L_olox.AND.L_emux
C control_th.fgi(  88,  30):�
      if(L_(105)) then
         I_(89)=I0_otix
      else
         I_(89)=I0_utix
      endif
C control_th.fgi( 119,  34):���� RE IN LO CH7
      if(L_etix) then
         I_avix=I0_itix
      else
         I_avix=I_(89)
      endif
C control_th.fgi( 137,  33):���� RE IN LO CH7
      L_(106) = L_olox.AND.L_ipux
C control_th.fgi(  88,  44):�
      if(L_(106)) then
         I_(90)=I0_ovix
      else
         I_(90)=I0_uvix
      endif
C control_th.fgi( 119,  50):���� RE IN LO CH7
      if(L_evix) then
         I_axix=I0_ivix
      else
         I_axix=I_(90)
      endif
C control_th.fgi( 137,  49):���� RE IN LO CH7
      R_adube=R_edube
C control_th.fgi( 159, 215):������,mo_digit1
      if(R_efube.gt.R0_axobe) then
         R_afube=R_efube-R0_axobe
      elseif(R_efube.le.-R0_axobe) then
         R_afube=R_efube+R0_axobe
      else
         R_afube=0.0
      endif
C control_th.fgi(  99, 224):���� ������������������
      if(L_ikox) then
         I_ubox=I0_adox
      else
         I_ubox=I0_edox
      endif
C control_th.fgi( 128,  76):���� RE IN LO CH7
      if(L_etibe) then
         I_idox=I0_adox
      else
         I_idox=I0_edox
      endif
C control_th.fgi( 128,  86):���� RE IN LO CH7
      L_otibe = (.NOT.L_(109))
C control_th.fgi( 132, 129):���
      L_usibe = (.NOT.L_(111)).AND.(.NOT.L_(110))
C control_th.fgi( 132, 137):�
      if(L_isibe) then
         I_odox=I0_adox
      else
         I_odox=I0_edox
      endif
C control_th.fgi( 128,  98):���� RE IN LO CH7
      L_asibe = (.NOT.L_(113)).AND.(.NOT.L_(112))
C control_th.fgi( 132, 145):�
      if(L_opobe) then
         I_udox=I0_adox
      else
         I_udox=I0_edox
      endif
C control_th.fgi( 128, 110):���� RE IN LO CH7
      L_epobe = (.NOT.L_(107)).AND.(.NOT.L_(114))
C control_th.fgi( 132, 153):�
      if(L_umobe) then
         I_afox=I0_adox
      else
         I_afox=I0_edox
      endif
C control_th.fgi( 128, 122):���� RE IN LO CH7
      L_okobe = (.NOT.L_(108))
C control_th.fgi( 132, 161):���
      L0_erobe=.false.
      I0_arobe=0

      if(L0_amade)then
        I0_ekube=1
         L0_upobe=.false.
         L0_ufobe=.false.
         L0_ovibe=.false.
         L0_utibe=.false.
         L0_itibe=.false.
         L0_atibe=.false.
         L0_osibe=.false.
         L0_esibe=.false.
         L0_uribe=.false.
         L0_ipobe=.false.
         L0_apobe=.false.
         L0_omobe=.false.
         L0_emobe=.false.
         L0_ulobe=.false.
         L0_ilobe=.false.
         L0_alobe=.false.
         L0_ukobe=.false.
         L0_ikobe=.false.
         L0_akobe=.false.
         L0_ifobe=.false.
         L0_afobe=.false.
         L0_odobe=.false.
         L0_edobe=.false.
         L0_ubobe=.false.
         L0_ibobe=.false.
         L0_abobe=.false.
         L0_oxibe=.false.
         L0_exibe=.false.
         L0_uvibe=.false.
         L0_evibe=.false.
      endif

      if(L0_upobe)I0_arobe=I0_arobe+1
      if(L0_ufobe)I0_arobe=I0_arobe+1
      if(L0_ovibe)I0_arobe=I0_arobe+1
      if(L0_utibe)I0_arobe=I0_arobe+1
      if(L0_itibe)I0_arobe=I0_arobe+1
      if(L0_atibe)I0_arobe=I0_arobe+1
      if(L0_osibe)I0_arobe=I0_arobe+1
      if(L0_esibe)I0_arobe=I0_arobe+1
      if(L0_uribe)I0_arobe=I0_arobe+1
      if(L0_ipobe)I0_arobe=I0_arobe+1
      if(L0_apobe)I0_arobe=I0_arobe+1
      if(L0_omobe)I0_arobe=I0_arobe+1
      if(L0_emobe)I0_arobe=I0_arobe+1
      if(L0_ulobe)I0_arobe=I0_arobe+1
      if(L0_ilobe)I0_arobe=I0_arobe+1
      if(L0_alobe)I0_arobe=I0_arobe+1
      if(L0_ukobe)I0_arobe=I0_arobe+1
      if(L0_ikobe)I0_arobe=I0_arobe+1
      if(L0_akobe)I0_arobe=I0_arobe+1
      if(L0_ifobe)I0_arobe=I0_arobe+1
      if(L0_afobe)I0_arobe=I0_arobe+1
      if(L0_odobe)I0_arobe=I0_arobe+1
      if(L0_edobe)I0_arobe=I0_arobe+1
      if(L0_ubobe)I0_arobe=I0_arobe+1
      if(L0_ibobe)I0_arobe=I0_arobe+1
      if(L0_abobe)I0_arobe=I0_arobe+1
      if(L0_oxibe)I0_arobe=I0_arobe+1
      if(L0_exibe)I0_arobe=I0_arobe+1
      if(L0_uvibe)I0_arobe=I0_arobe+1
      if(L0_evibe)I0_arobe=I0_arobe+1


      if(.not.L0_erobe.and.L_exobe.and..not.L0_upobe)then
        I0_ekube=1
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_ixobe.and..not.L0_ufobe)then
        I0_ekube=2
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_oxobe.and..not.L0_ovibe)then
        I0_ekube=3
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_avibe.and..not.L0_utibe)then
        I0_ekube=4
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_otibe.and..not.L0_itibe)then
        I0_ekube=5
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_etibe.and..not.L0_atibe)then
        I0_ekube=6
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_usibe.and..not.L0_osibe)then
        I0_ekube=7
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_isibe.and..not.L0_esibe)then
        I0_ekube=8
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_asibe.and..not.L0_uribe)then
        I0_ekube=9
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_opobe.and..not.L0_ipobe)then
        I0_ekube=10
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_epobe.and..not.L0_apobe)then
        I0_ekube=11
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_umobe.and..not.L0_omobe)then
        I0_ekube=12
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_imobe.and..not.L0_emobe)then
        I0_ekube=13
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_amobe.and..not.L0_ulobe)then
        I0_ekube=14
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_olobe.and..not.L0_ilobe)then
        I0_ekube=15
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_elobe.and..not.L0_alobe)then
        I0_ekube=16
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_(150).and..not.L0_ukobe)then
        I0_ekube=17
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L_okobe.and..not.L0_ikobe)then
        I0_ekube=18
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_ekobe.and..not.L0_akobe)then
        I0_ekube=19
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_ofobe.and..not.L0_ifobe)then
        I0_ekube=20
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_efobe.and..not.L0_afobe)then
        I0_ekube=21
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_udobe.and..not.L0_odobe)then
        I0_ekube=22
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_idobe.and..not.L0_edobe)then
        I0_ekube=23
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_adobe.and..not.L0_ubobe)then
        I0_ekube=24
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_obobe.and..not.L0_ibobe)then
        I0_ekube=25
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_ebobe.and..not.L0_abobe)then
        I0_ekube=26
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_uxibe.and..not.L0_oxibe)then
        I0_ekube=27
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_ixibe.and..not.L0_exibe)then
        I0_ekube=28
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_axibe.and..not.L0_uvibe)then
        I0_ekube=29
        L0_erobe=.true.
      endif

      if(.not.L0_erobe.and.L0_ivibe.and..not.L0_evibe)then
        I0_ekube=30
        L0_erobe=.true.
      endif

      if(I0_arobe.eq.1)then
         L0_upobe=.false.
         L0_ufobe=.false.
         L0_ovibe=.false.
         L0_utibe=.false.
         L0_itibe=.false.
         L0_atibe=.false.
         L0_osibe=.false.
         L0_esibe=.false.
         L0_uribe=.false.
         L0_ipobe=.false.
         L0_apobe=.false.
         L0_omobe=.false.
         L0_emobe=.false.
         L0_ulobe=.false.
         L0_ilobe=.false.
         L0_alobe=.false.
         L0_ukobe=.false.
         L0_ikobe=.false.
         L0_akobe=.false.
         L0_ifobe=.false.
         L0_afobe=.false.
         L0_odobe=.false.
         L0_edobe=.false.
         L0_ubobe=.false.
         L0_ibobe=.false.
         L0_abobe=.false.
         L0_oxibe=.false.
         L0_exibe=.false.
         L0_uvibe=.false.
         L0_evibe=.false.
      else
         L0_upobe=L_exobe
         L0_ufobe=L_ixobe
         L0_ovibe=L_oxobe
         L0_utibe=L0_avibe
         L0_itibe=L_otibe
         L0_atibe=L_etibe
         L0_osibe=L_usibe
         L0_esibe=L_isibe
         L0_uribe=L_asibe
         L0_ipobe=L_opobe
         L0_apobe=L_epobe
         L0_omobe=L_umobe
         L0_emobe=L0_imobe
         L0_ulobe=L0_amobe
         L0_ilobe=L0_olobe
         L0_alobe=L_elobe
         L0_ukobe=L_(150)
         L0_ikobe=L_okobe
         L0_akobe=L0_ekobe
         L0_ifobe=L0_ofobe
         L0_afobe=L0_efobe
         L0_odobe=L0_udobe
         L0_edobe=L0_idobe
         L0_ubobe=L0_adobe
         L0_ibobe=L0_obobe
         L0_abobe=L0_ebobe
         L0_oxibe=L0_uxibe
         L0_exibe=L0_ixibe
         L0_uvibe=L0_axibe
         L0_evibe=L0_ivibe
      endif
C control_th.fgi( 371, 261):��������� �������
      select case (I0_ekube)
      case (1)
        C255_otobe="�����������"
      case (2)
        C255_otobe="����������� �����"
      case (3)
        C255_otobe="����������� ������"
      case (4)
        C255_otobe="�����-� � ����-� �� ����������"
      case (5)
        C255_otobe="������ ������������"
      case (6)
        C255_otobe="������� ������"
      case (7)
        C255_otobe="����� ���.������ � ��������"
      case (8)
        C255_otobe="������� ��������"
      case (9)
        C255_otobe="����� ���.�������� � ������������"
      case (10)
        C255_otobe="������� �����������"
      case (11)
        C255_otobe="����� ���.����������� � ���������"
      case (12)
        C255_otobe="������� ���������"
      case (13)
        C255_otobe="???"
      case (14)
        C255_otobe="�� ����������"
      case (15)
        C255_otobe="����-� ������� �������� �� ���."
      case (16)
        C255_otobe="������� �������� ������ � �����������"
      case (17)
        C255_otobe="������� �������� �� ���.� �����������"
      case (18)
        C255_otobe="����� ������������"
      case (19)
        C255_otobe="???"
      case (20)
        C255_otobe="???"
      case (21)
        C255_otobe="???"
      case (22)
        C255_otobe="???"
      case (23)
        C255_otobe="???"
      case (24)
        C255_otobe="???"
      case (25)
        C255_otobe="???"
      case (26)
        C255_otobe="???"
      case (27)
        C255_otobe="???"
      case (28)
        C255_otobe="???"
      case (29)
        C255_otobe="???"
      case (30)
        C255_otobe="???"
      end select
C control_th.fgi( 386, 290):MarkVI text out 30 variants,mo_text1
      L_(130) = L_okobe.OR.L_otibe
C control_th.fgi( 220, 225):���
      L_(131) = L_ixobe.AND.L_olox.AND.(.NOT.L_(130))
C control_th.fgi( 237, 233):�
      L_upabe = (.NOT.L_avux).AND.L_(131)
C control_th.fgi( 244, 234):�
      L_(129) = L_olox.AND.L_oxobe.AND.(.NOT.L_(130))
C control_th.fgi( 237, 227):�
      L_isux = (.NOT.L_avux).AND.L_(129)
C control_th.fgi( 244, 228):�
      L_(128) = L_(130).AND.L_olox
C control_th.fgi( 237, 222):�
      L_asux = (.NOT.L_avux).AND.L_(128)
C control_th.fgi( 244, 223):�
      L_(123) = L_okobe.OR.L_otibe
C control_th.fgi( 220, 132):���
      L_(124) = L_ixobe.AND.L_olox.AND.(.NOT.L_(123))
C control_th.fgi( 237, 140):�
      L_okux = (.NOT.L_ulux).AND.L_(124)
C control_th.fgi( 244, 141):�
      L_(121) = L_(123).AND.L_olox
C control_th.fgi( 237, 129):�
      L_amox = (.NOT.L_ulux).AND.L_(121)
C control_th.fgi( 244, 130):�
      L_(122) = L_olox.AND.L_oxobe.AND.(.NOT.L_(123))
C control_th.fgi( 237, 134):�
      L_imox = (.NOT.L_ulux).AND.L_(122)
C control_th.fgi( 244, 135):�
      L_(87) =.NOT.(L_emix)
C control_th.fgi( 331,  34):���
      I_(76)=0
      if(L_(87))I_(76)=7
C control_th.fgi( 337,  34):lo2digit
      I_(75)=0
      if(L_emix)I_(75)=6
C control_th.fgi( 337,  32):lo2digit
      I_(100) = I_(76) + I_(75)
C control_th.fgi( 343,  33):��������
      select case (I_(100))
      case (1)
        C255_isobe="�����"
      case (2)
        C255_isobe="� ������"
      case (3)
        C255_isobe="� ������"
      case (4)
        C255_isobe="��������"
      case (5)
        C255_isobe="�� ����������"
      case (6)
        C255_isobe="����� ������ � ����������"
      case (7)
        C255_isobe="����� ������ ����������"
      case (8)
        C255_isobe="???"
      case (9)
        C255_isobe="???"
      case (10)
        C255_isobe="???"
      case (11)
        C255_isobe="???"
      case (12)
        C255_isobe="???"
      case (13)
        C255_isobe="???"
      case (14)
        C255_isobe="???"
      case (15)
        C255_isobe="???"
      case (16)
        C255_isobe="???"
      case (17)
        C255_isobe="???"
      case (18)
        C255_isobe="???"
      case (19)
        C255_isobe="???"
      case (20)
        C255_isobe="???"
      case (21)
        C255_isobe="???"
      case (22)
        C255_isobe="???"
      case (23)
        C255_isobe="???"
      case (24)
        C255_isobe="???"
      case (25)
        C255_isobe="???"
      case (26)
        C255_isobe="???"
      case (27)
        C255_isobe="???"
      case (28)
        C255_isobe="???"
      case (29)
        C255_isobe="???"
      case (30)
        C255_isobe="???"
      end select
C control_th.fgi( 357,  33):MarkVI text out 30 variants,mo_text8
      L_(83) = L_(100).AND.(.NOT.L_emix)
C control_th.fgi( 300,  45):�
      I_(71)=0
      if(L_(83))I_(71)=8
C control_th.fgi( 311,  46):lo2digit
      L_(82) = (.NOT.L_(94)).AND.L_(99)
C control_th.fgi( 300,  33):�
      I_(70)=0
      if(L_(82))I_(70)=10
C control_th.fgi( 311,  33):lo2digit
      L_(81) = (.NOT.L_(83)).AND.(.NOT.L_(82))
C control_th.fgi( 306,  42):�
      I_(69)=0
      if(L_(81))I_(69)=6
C control_th.fgi( 311,  42):lo2digit
      I_(98) = I_(71) + I_(70) + I_(69)
C control_th.fgi( 319,  44):��������
      select case (I_(98))
      case (1)
        C255_asobe="��� �����"
      case (2)
        C255_asobe="���������� � UZ"
      case (3)
        C255_asobe="������������"
      case (4)
        C255_asobe="��� 380�"
      case (5)
        C255_asobe="�������������"
      case (6)
        C255_asobe="����� � ������"
      case (7)
        C255_asobe="����� ����"
      case (8)
        C255_asobe="������ ������"
      case (9)
        C255_asobe="������ �� ����"
      case (10)
        C255_asobe="������ �����"
      case (11)
        C255_asobe="����� �������"
      case (12)
        C255_asobe="����������������"
      case (13)
        C255_asobe="����� ����"
      case (14)
        C255_asobe="����� ������� �����"
      case (15)
        C255_asobe="������ �������"
      case (16)
        C255_asobe="������ ����������"
      case (17)
        C255_asobe="�� ����������"
      case (18)
        C255_asobe="???"
      case (19)
        C255_asobe="???"
      case (20)
        C255_asobe="???"
      case (21)
        C255_asobe="???"
      case (22)
        C255_asobe="???"
      case (23)
        C255_asobe="???"
      case (24)
        C255_asobe="???"
      case (25)
        C255_asobe="???"
      case (26)
        C255_asobe="???"
      case (27)
        C255_asobe="???"
      case (28)
        C255_asobe="???"
      case (29)
        C255_asobe="???"
      case (30)
        C255_asobe="???"
      end select
C control_th.fgi( 357,  44):MarkVI text out 30 variants,mo_text10
      L_(88) =.NOT.(L_apix)
C control_th.fgi( 331,  61):���
      I_(78)=0
      if(L_(88))I_(78)=7
C control_th.fgi( 337,  61):lo2digit
      I_(77)=0
      if(L_apix)I_(77)=6
C control_th.fgi( 337,  59):lo2digit
      I_(101) = I_(78) + I_(77)
C control_th.fgi( 343,  60):��������
      select case (I_(101))
      case (1)
        C255_osobe="�����"
      case (2)
        C255_osobe="� ������"
      case (3)
        C255_osobe="� ������"
      case (4)
        C255_osobe="��������"
      case (5)
        C255_osobe="�� ����������"
      case (6)
        C255_osobe="����� ������ � ����������"
      case (7)
        C255_osobe="����� ������ ����������"
      case (8)
        C255_osobe="???"
      case (9)
        C255_osobe="???"
      case (10)
        C255_osobe="???"
      case (11)
        C255_osobe="???"
      case (12)
        C255_osobe="???"
      case (13)
        C255_osobe="???"
      case (14)
        C255_osobe="???"
      case (15)
        C255_osobe="???"
      case (16)
        C255_osobe="???"
      case (17)
        C255_osobe="???"
      case (18)
        C255_osobe="???"
      case (19)
        C255_osobe="???"
      case (20)
        C255_osobe="???"
      case (21)
        C255_osobe="???"
      case (22)
        C255_osobe="???"
      case (23)
        C255_osobe="???"
      case (24)
        C255_osobe="???"
      case (25)
        C255_osobe="???"
      case (26)
        C255_osobe="???"
      case (27)
        C255_osobe="???"
      case (28)
        C255_osobe="???"
      case (29)
        C255_osobe="???"
      case (30)
        C255_osobe="???"
      end select
C control_th.fgi( 357,  60):MarkVI text out 30 variants,mo_text7
      L_(86) = L_(104).AND.(.NOT.L_apix)
C control_th.fgi( 300,  66):�
      I_(74)=0
      if(L_(86))I_(74)=8
C control_th.fgi( 311,  67):lo2digit
      L_(85) = (.NOT.L_(95)).AND.L_(103)
C control_th.fgi( 300,  54):�
      I_(73)=0
      if(L_(85))I_(73)=10
C control_th.fgi( 311,  54):lo2digit
      L_(84) = (.NOT.L_(86)).AND.(.NOT.L_(85))
C control_th.fgi( 306,  63):�
      I_(72)=0
      if(L_(84))I_(72)=6
C control_th.fgi( 311,  63):lo2digit
      I_(99) = I_(74) + I_(73) + I_(72)
C control_th.fgi( 319,  65):��������
      select case (I_(99))
      case (1)
        C255_esobe="��� �����"
      case (2)
        C255_esobe="���������� � UZ"
      case (3)
        C255_esobe="������������"
      case (4)
        C255_esobe="��� 380�"
      case (5)
        C255_esobe="�������������"
      case (6)
        C255_esobe="����� � ������"
      case (7)
        C255_esobe="����� ����"
      case (8)
        C255_esobe="������ ������"
      case (9)
        C255_esobe="������ �� ����"
      case (10)
        C255_esobe="������ �����"
      case (11)
        C255_esobe="����� �������"
      case (12)
        C255_esobe="����������������"
      case (13)
        C255_esobe="����� ����"
      case (14)
        C255_esobe="����� ������� �����"
      case (15)
        C255_esobe="������ �������"
      case (16)
        C255_esobe="������ ����������"
      case (17)
        C255_esobe="�� ����������"
      case (18)
        C255_esobe="???"
      case (19)
        C255_esobe="???"
      case (20)
        C255_esobe="???"
      case (21)
        C255_esobe="???"
      case (22)
        C255_esobe="???"
      case (23)
        C255_esobe="???"
      case (24)
        C255_esobe="???"
      case (25)
        C255_esobe="???"
      case (26)
        C255_esobe="???"
      case (27)
        C255_esobe="???"
      case (28)
        C255_esobe="???"
      case (29)
        C255_esobe="???"
      case (30)
        C255_esobe="???"
      end select
C control_th.fgi( 357,  71):MarkVI text out 30 variants,mo_text9
      L_(149) = L_abube.AND.L_esebe
C control_th.fgi( 356, 223):�
      L_(145) = (.NOT.L_esebe).AND.L_abube
C control_th.fgi( 356, 219):�
      L_(148) = L_ebube.AND.L_isebe
C control_th.fgi( 356, 215):�
      L_(144) = (.NOT.L_isebe).AND.L_ebube
C control_th.fgi( 356, 211):�
      L_(147) = L_ibube.AND.L_osebe
C control_th.fgi( 356, 207):�
      L_(143) = (.NOT.L_osebe).AND.L_ibube
C control_th.fgi( 356, 203):�
      L_(146) = L_obube.AND.L_usebe
C control_th.fgi( 356, 199):�
      L_(142) = (.NOT.L_usebe).AND.L_obube
C control_th.fgi( 356, 195):�
      L_eribe = (.NOT.L_(153))
C control_th.fgi(  81, 256):���
      L_(127) = (.NOT.L_eribe)
C control_th.fgi( 220, 158):���
      L_(126) = L_olox.AND.L_(127).AND.(.NOT.L_ilox).AND.
     &(.NOT.L_oxobe).AND.(.NOT.L_ixobe)
C control_th.fgi( 237, 158):�
      L_emube = (.NOT.L_ulux).AND.L_(126)
C control_th.fgi( 244, 159):�
      L_epox = (.NOT.L_emube).AND.(.NOT.L_amube).AND.(.NOT.L_okux
     &).AND.(.NOT.L_imox).AND.(.NOT.L_amox).AND.(.NOT.L_ulux
     &)
C control_th.fgi( 259, 118):�
      Call CONTROL_FDD_1(ext_deltat)
      End
      Subroutine CONTROL_FDD_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'CONTROL_FDD.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_ilux=.false.
      I0_elux=0

      if(L0_amade)then
        I0_atobe=1
         L0_ukux=.false.
         L0_uxox=.false.
         L0_irox=.false.
         L0_upox=.false.
         L0_ipox=.false.
         L0_apox=.false.
         L0_omox=.false.
         L0_emox=.false.
         L0_ulox=.false.
         L0_ikux=.false.
         L0_ekux=.false.
         L0_akux=.false.
         L0_ofux=.false.
         L0_efux=.false.
         L0_udux=.false.
         L0_idux=.false.
         L0_adux=.false.
         L0_obux=.false.
         L0_ebux=.false.
         L0_ixox=.false.
         L0_axox=.false.
         L0_ovox=.false.
         L0_evox=.false.
         L0_utox=.false.
         L0_itox=.false.
         L0_atox=.false.
         L0_osox=.false.
         L0_esox=.false.
         L0_urox=.false.
         L0_arox=.false.
      endif

      if(L0_ukux)I0_elux=I0_elux+1
      if(L0_uxox)I0_elux=I0_elux+1
      if(L0_irox)I0_elux=I0_elux+1
      if(L0_upox)I0_elux=I0_elux+1
      if(L0_ipox)I0_elux=I0_elux+1
      if(L0_apox)I0_elux=I0_elux+1
      if(L0_omox)I0_elux=I0_elux+1
      if(L0_emox)I0_elux=I0_elux+1
      if(L0_ulox)I0_elux=I0_elux+1
      if(L0_ikux)I0_elux=I0_elux+1
      if(L0_ekux)I0_elux=I0_elux+1
      if(L0_akux)I0_elux=I0_elux+1
      if(L0_ofux)I0_elux=I0_elux+1
      if(L0_efux)I0_elux=I0_elux+1
      if(L0_udux)I0_elux=I0_elux+1
      if(L0_idux)I0_elux=I0_elux+1
      if(L0_adux)I0_elux=I0_elux+1
      if(L0_obux)I0_elux=I0_elux+1
      if(L0_ebux)I0_elux=I0_elux+1
      if(L0_ixox)I0_elux=I0_elux+1
      if(L0_axox)I0_elux=I0_elux+1
      if(L0_ovox)I0_elux=I0_elux+1
      if(L0_evox)I0_elux=I0_elux+1
      if(L0_utox)I0_elux=I0_elux+1
      if(L0_itox)I0_elux=I0_elux+1
      if(L0_atox)I0_elux=I0_elux+1
      if(L0_osox)I0_elux=I0_elux+1
      if(L0_esox)I0_elux=I0_elux+1
      if(L0_urox)I0_elux=I0_elux+1
      if(L0_arox)I0_elux=I0_elux+1


      if(.not.L0_ilux.and.L0_alux.and..not.L0_ukux)then
        I0_atobe=1
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_abux.and..not.L0_uxox)then
        I0_atobe=2
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_orox.and..not.L0_irox)then
        I0_atobe=3
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_ulux.and..not.L0_upox)then
        I0_atobe=4
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_opox.and..not.L0_ipox)then
        I0_atobe=5
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_epox.and..not.L0_apox)then
        I0_atobe=6
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_umox.and..not.L0_omox)then
        I0_atobe=7
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_imox.and..not.L0_emox)then
        I0_atobe=8
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_amox.and..not.L0_ulox)then
        I0_atobe=9
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_okux.and..not.L0_ikux)then
        I0_atobe=10
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_emube.and..not.L0_ekux)then
        I0_atobe=11
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L_amube.and..not.L0_akux)then
        I0_atobe=12
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_ufux.and..not.L0_ofux)then
        I0_atobe=13
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_ifux.and..not.L0_efux)then
        I0_atobe=14
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_afux.and..not.L0_udux)then
        I0_atobe=15
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_odux.and..not.L0_idux)then
        I0_atobe=16
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_edux.and..not.L0_adux)then
        I0_atobe=17
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_ubux.and..not.L0_obux)then
        I0_atobe=18
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_ibux.and..not.L0_ebux)then
        I0_atobe=19
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_oxox.and..not.L0_ixox)then
        I0_atobe=20
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_exox.and..not.L0_axox)then
        I0_atobe=21
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_uvox.and..not.L0_ovox)then
        I0_atobe=22
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_ivox.and..not.L0_evox)then
        I0_atobe=23
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_avox.and..not.L0_utox)then
        I0_atobe=24
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_otox.and..not.L0_itox)then
        I0_atobe=25
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_etox.and..not.L0_atox)then
        I0_atobe=26
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_usox.and..not.L0_osox)then
        I0_atobe=27
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_isox.and..not.L0_esox)then
        I0_atobe=28
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_asox.and..not.L0_urox)then
        I0_atobe=29
        L0_ilux=.true.
      endif

      if(.not.L0_ilux.and.L0_erox.and..not.L0_arox)then
        I0_atobe=30
        L0_ilux=.true.
      endif

      if(I0_elux.eq.1)then
         L0_ukux=.false.
         L0_uxox=.false.
         L0_irox=.false.
         L0_upox=.false.
         L0_ipox=.false.
         L0_apox=.false.
         L0_omox=.false.
         L0_emox=.false.
         L0_ulox=.false.
         L0_ikux=.false.
         L0_ekux=.false.
         L0_akux=.false.
         L0_ofux=.false.
         L0_efux=.false.
         L0_udux=.false.
         L0_idux=.false.
         L0_adux=.false.
         L0_obux=.false.
         L0_ebux=.false.
         L0_ixox=.false.
         L0_axox=.false.
         L0_ovox=.false.
         L0_evox=.false.
         L0_utox=.false.
         L0_itox=.false.
         L0_atox=.false.
         L0_osox=.false.
         L0_esox=.false.
         L0_urox=.false.
         L0_arox=.false.
      else
         L0_ukux=L0_alux
         L0_uxox=L0_abux
         L0_irox=L0_orox
         L0_upox=L_ulux
         L0_ipox=L_opox
         L0_apox=L_epox
         L0_omox=L0_umox
         L0_emox=L_imox
         L0_ulox=L_amox
         L0_ikux=L_okux
         L0_ekux=L_emube
         L0_akux=L_amube
         L0_ofux=L0_ufux
         L0_efux=L0_ifux
         L0_udux=L0_afux
         L0_idux=L0_odux
         L0_adux=L0_edux
         L0_obux=L0_ubux
         L0_ebux=L0_ibux
         L0_ixox=L0_oxox
         L0_axox=L0_exox
         L0_ovox=L0_uvox
         L0_evox=L0_ivox
         L0_utox=L0_avox
         L0_itox=L0_otox
         L0_atox=L0_etox
         L0_osox=L0_usox
         L0_esox=L0_isox
         L0_urox=L0_asox
         L0_arox=L0_erox
      endif
C control_th.fgi( 289, 168):��������� �������
      select case (I0_atobe)
      case (1)
        C255_usobe="��� �����"
      case (2)
        C255_usobe="���������� � UZ"
      case (3)
        C255_usobe="������������"
      case (4)
        C255_usobe="��� 380�"
      case (5)
        C255_usobe="�������������"
      case (6)
        C255_usobe="����� � ������"
      case (7)
        C255_usobe="����� ����"
      case (8)
        C255_usobe="������ ������"
      case (9)
        C255_usobe="������ �� ����"
      case (10)
        C255_usobe="������ �����"
      case (11)
        C255_usobe="����� �������"
      case (12)
        C255_usobe="����������������"
      case (13)
        C255_usobe="����� ����"
      case (14)
        C255_usobe="����� ������� �����"
      case (15)
        C255_usobe="������ �������"
      case (16)
        C255_usobe="������ ����������"
      case (17)
        C255_usobe="�� ����������"
      case (18)
        C255_usobe="???"
      case (19)
        C255_usobe="???"
      case (20)
        C255_usobe="???"
      case (21)
        C255_usobe="???"
      case (22)
        C255_usobe="???"
      case (23)
        C255_usobe="???"
      case (24)
        C255_usobe="???"
      case (25)
        C255_usobe="???"
      case (26)
        C255_usobe="???"
      case (27)
        C255_usobe="???"
      case (28)
        C255_usobe="???"
      case (29)
        C255_usobe="???"
      case (30)
        C255_usobe="???"
      end select
C control_th.fgi( 306, 197):MarkVI text out 30 variants,mo_text6
      L_(134) = (.NOT.L_eribe)
C control_th.fgi( 220, 251):���
      L_(133) = L_olox.AND.L_(134).AND.(.NOT.L_ilox).AND.
     &(.NOT.L_oxobe).AND.(.NOT.L_ixobe)
C control_th.fgi( 237, 251):�
      L_omube = (.NOT.L_avux).AND.L_(133)
C control_th.fgi( 244, 252):�
      L_(161) = L_omube.OR.L_imube.OR.L_emube.OR.L_amube
C common.fgi(  97,  93):���
      L0_emade=.false.
      I0_ulade=0

      if(L0_amade)then
        I0_omade=1
         L0_ilade=.false.
         L0_ibade=.false.
         L0_asube=.false.
         L0_irube=.false.
         L0_arube=.false.
         L0_opube=.false.
         L0_ipube=.false.
         L0_epube=.false.
         L0_apube=.false.
         L0_elade=.false.
         L0_ukade=.false.
         L0_ikade=.false.
         L0_akade=.false.
         L0_ofade=.false.
         L0_efade=.false.
         L0_udade=.false.
         L0_idade=.false.
         L0_adade=.false.
         L0_obade=.false.
         L0_abade=.false.
         L0_oxube=.false.
         L0_exube=.false.
         L0_uvube=.false.
         L0_ivube=.false.
         L0_avube=.false.
         L0_otube=.false.
         L0_etube=.false.
         L0_usube=.false.
         L0_isube=.false.
         L0_urube=.false.
      endif

      if(L0_ilade)I0_ulade=I0_ulade+1
      if(L0_ibade)I0_ulade=I0_ulade+1
      if(L0_asube)I0_ulade=I0_ulade+1
      if(L0_irube)I0_ulade=I0_ulade+1
      if(L0_arube)I0_ulade=I0_ulade+1
      if(L0_opube)I0_ulade=I0_ulade+1
      if(L0_ipube)I0_ulade=I0_ulade+1
      if(L0_epube)I0_ulade=I0_ulade+1
      if(L0_apube)I0_ulade=I0_ulade+1
      if(L0_elade)I0_ulade=I0_ulade+1
      if(L0_ukade)I0_ulade=I0_ulade+1
      if(L0_ikade)I0_ulade=I0_ulade+1
      if(L0_akade)I0_ulade=I0_ulade+1
      if(L0_ofade)I0_ulade=I0_ulade+1
      if(L0_efade)I0_ulade=I0_ulade+1
      if(L0_udade)I0_ulade=I0_ulade+1
      if(L0_idade)I0_ulade=I0_ulade+1
      if(L0_adade)I0_ulade=I0_ulade+1
      if(L0_obade)I0_ulade=I0_ulade+1
      if(L0_abade)I0_ulade=I0_ulade+1
      if(L0_oxube)I0_ulade=I0_ulade+1
      if(L0_exube)I0_ulade=I0_ulade+1
      if(L0_uvube)I0_ulade=I0_ulade+1
      if(L0_ivube)I0_ulade=I0_ulade+1
      if(L0_avube)I0_ulade=I0_ulade+1
      if(L0_otube)I0_ulade=I0_ulade+1
      if(L0_etube)I0_ulade=I0_ulade+1
      if(L0_usube)I0_ulade=I0_ulade+1
      if(L0_isube)I0_ulade=I0_ulade+1
      if(L0_urube)I0_ulade=I0_ulade+1


      if(.not.L0_emade.and.L0_olade.and..not.L0_ilade)then
        I0_omade=1
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_(161).and..not.L0_ibade)then
        I0_omade=2
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_esube.and..not.L0_asube)then
        I0_omade=3
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_orube.and..not.L0_irube)then
        I0_omade=4
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_erube.and..not.L0_arube)then
        I0_omade=5
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_upube.and..not.L0_opube)then
        I0_omade=6
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_(159).and..not.L0_ipube)then
        I0_omade=7
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_(158).and..not.L0_epube)then
        I0_omade=8
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_(157).and..not.L0_apube)then
        I0_omade=9
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_(162).and..not.L0_elade)then
        I0_omade=10
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_alade.and..not.L0_ukade)then
        I0_omade=11
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_okade.and..not.L0_ikade)then
        I0_omade=12
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ekade.and..not.L0_akade)then
        I0_omade=13
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ufade.and..not.L0_ofade)then
        I0_omade=14
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ifade.and..not.L0_efade)then
        I0_omade=15
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_afade.and..not.L0_udade)then
        I0_omade=16
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_odade.and..not.L0_idade)then
        I0_omade=17
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_edade.and..not.L0_adade)then
        I0_omade=18
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ubade.and..not.L0_obade)then
        I0_omade=19
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ebade.and..not.L0_abade)then
        I0_omade=20
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_uxube.and..not.L0_oxube)then
        I0_omade=21
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ixube.and..not.L0_exube)then
        I0_omade=22
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_axube.and..not.L0_uvube)then
        I0_omade=23
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_ovube.and..not.L0_ivube)then
        I0_omade=24
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_evube.and..not.L0_avube)then
        I0_omade=25
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_utube.and..not.L0_otube)then
        I0_omade=26
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_itube.and..not.L0_etube)then
        I0_omade=27
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_atube.and..not.L0_usube)then
        I0_omade=28
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L0_osube.and..not.L0_isube)then
        I0_omade=29
        L0_emade=.true.
      endif

      if(.not.L0_emade.and.L_(160).and..not.L0_urube)then
        I0_omade=30
        L0_emade=.true.
      endif

      if(I0_ulade.eq.1)then
         L0_ilade=.false.
         L0_ibade=.false.
         L0_asube=.false.
         L0_irube=.false.
         L0_arube=.false.
         L0_opube=.false.
         L0_ipube=.false.
         L0_epube=.false.
         L0_apube=.false.
         L0_elade=.false.
         L0_ukade=.false.
         L0_ikade=.false.
         L0_akade=.false.
         L0_ofade=.false.
         L0_efade=.false.
         L0_udade=.false.
         L0_idade=.false.
         L0_adade=.false.
         L0_obade=.false.
         L0_abade=.false.
         L0_oxube=.false.
         L0_exube=.false.
         L0_uvube=.false.
         L0_ivube=.false.
         L0_avube=.false.
         L0_otube=.false.
         L0_etube=.false.
         L0_usube=.false.
         L0_isube=.false.
         L0_urube=.false.
      else
         L0_ilade=L0_olade
         L0_ibade=L_(161)
         L0_asube=L_esube
         L0_irube=L_orube
         L0_arube=L_erube
         L0_opube=L0_upube
         L0_ipube=L_(159)
         L0_epube=L_(158)
         L0_apube=L_(157)
         L0_elade=L_(162)
         L0_ukade=L0_alade
         L0_ikade=L0_okade
         L0_akade=L0_ekade
         L0_ofade=L0_ufade
         L0_efade=L0_ifade
         L0_udade=L0_afade
         L0_idade=L0_odade
         L0_adade=L0_edade
         L0_obade=L0_ubade
         L0_abade=L0_ebade
         L0_oxube=L0_uxube
         L0_exube=L0_ixube
         L0_uvube=L0_axube
         L0_ivube=L0_ovube
         L0_avube=L0_evube
         L0_otube=L0_utube
         L0_etube=L0_itube
         L0_usube=L0_atube
         L0_isube=L0_osube
         L0_urube=L_(160)
      endif
C common.fgi( 134,  71):��������� �������
      select case (I0_omade)
      case (1)
        C255_imade="====================="
      case (2)
        C255_imade="����������� � �������"
      case (3)
        C255_imade="������ �� ������� �������"
      case (4)
        C255_imade="�����"
      case (5)
        C255_imade="�����������"
      case (6)
        C255_imade="���������� ������ � ��������"
      case (7)
        C255_imade="�������� ������ ������������"
      case (8)
        C255_imade="���������� �������������"
      case (9)
        C255_imade="�������� ������ �� ��� ��"
      case (10)
        C255_imade="�������� ������ �� ���������"
      case (11)
        C255_imade="�������� ������������� ����������"
      case (12)
        C255_imade="��������� �������"
      case (13)
        C255_imade="???"
      case (14)
        C255_imade="???"
      case (15)
        C255_imade="???"
      case (16)
        C255_imade="???"
      case (17)
        C255_imade="???"
      case (18)
        C255_imade="???"
      case (19)
        C255_imade="???"
      case (20)
        C255_imade="???"
      case (21)
        C255_imade="???"
      case (22)
        C255_imade="???"
      case (23)
        C255_imade="???"
      case (24)
        C255_imade="???"
      case (25)
        C255_imade="???"
      case (26)
        C255_imade="???"
      case (27)
        C255_imade="???"
      case (28)
        C255_imade="???"
      case (29)
        C255_imade="???"
      case (30)
        C255_imade="====================="
      end select
C common.fgi( 152, 100):MarkVI text out 30 variants,regime_state
      L_etux = (.NOT.L_omube).AND.(.NOT.L_imube).AND.(.NOT.L_upabe
     &).AND.(.NOT.L_isux).AND.(.NOT.L_asux).AND.(.NOT.L_avux
     &)
C control_th.fgi( 259, 211):�
      L0_urabe=.false.
      I0_irabe=0

      if(L0_amade)then
        I0_orabe=1
         L0_arabe=.false.
         L0_akabe=.false.
         L0_ovux=.false.
         L0_utux=.false.
         L0_itux=.false.
         L0_atux=.false.
         L0_osux=.false.
         L0_esux=.false.
         L0_urux=.false.
         L0_opabe=.false.
         L0_ipabe=.false.
         L0_epabe=.false.
         L0_umabe=.false.
         L0_imabe=.false.
         L0_amabe=.false.
         L0_olabe=.false.
         L0_elabe=.false.
         L0_ukabe=.false.
         L0_ikabe=.false.
         L0_ofabe=.false.
         L0_efabe=.false.
         L0_udabe=.false.
         L0_idabe=.false.
         L0_adabe=.false.
         L0_obabe=.false.
         L0_ebabe=.false.
         L0_uxux=.false.
         L0_ixux=.false.
         L0_axux=.false.
         L0_evux=.false.
      endif

      if(L0_arabe)I0_irabe=I0_irabe+1
      if(L0_akabe)I0_irabe=I0_irabe+1
      if(L0_ovux)I0_irabe=I0_irabe+1
      if(L0_utux)I0_irabe=I0_irabe+1
      if(L0_itux)I0_irabe=I0_irabe+1
      if(L0_atux)I0_irabe=I0_irabe+1
      if(L0_osux)I0_irabe=I0_irabe+1
      if(L0_esux)I0_irabe=I0_irabe+1
      if(L0_urux)I0_irabe=I0_irabe+1
      if(L0_opabe)I0_irabe=I0_irabe+1
      if(L0_ipabe)I0_irabe=I0_irabe+1
      if(L0_epabe)I0_irabe=I0_irabe+1
      if(L0_umabe)I0_irabe=I0_irabe+1
      if(L0_imabe)I0_irabe=I0_irabe+1
      if(L0_amabe)I0_irabe=I0_irabe+1
      if(L0_olabe)I0_irabe=I0_irabe+1
      if(L0_elabe)I0_irabe=I0_irabe+1
      if(L0_ukabe)I0_irabe=I0_irabe+1
      if(L0_ikabe)I0_irabe=I0_irabe+1
      if(L0_ofabe)I0_irabe=I0_irabe+1
      if(L0_efabe)I0_irabe=I0_irabe+1
      if(L0_udabe)I0_irabe=I0_irabe+1
      if(L0_idabe)I0_irabe=I0_irabe+1
      if(L0_adabe)I0_irabe=I0_irabe+1
      if(L0_obabe)I0_irabe=I0_irabe+1
      if(L0_ebabe)I0_irabe=I0_irabe+1
      if(L0_uxux)I0_irabe=I0_irabe+1
      if(L0_ixux)I0_irabe=I0_irabe+1
      if(L0_axux)I0_irabe=I0_irabe+1
      if(L0_evux)I0_irabe=I0_irabe+1


      if(.not.L0_urabe.and.L0_erabe.and..not.L0_arabe)then
        I0_orabe=1
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ekabe.and..not.L0_akabe)then
        I0_orabe=2
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_uvux.and..not.L0_ovux)then
        I0_orabe=3
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_avux.and..not.L0_utux)then
        I0_orabe=4
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_otux.and..not.L0_itux)then
        I0_orabe=5
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_etux.and..not.L0_atux)then
        I0_orabe=6
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_usux.and..not.L0_osux)then
        I0_orabe=7
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_isux.and..not.L0_esux)then
        I0_orabe=8
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_asux.and..not.L0_urux)then
        I0_orabe=9
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_upabe.and..not.L0_opabe)then
        I0_orabe=10
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_omube.and..not.L0_ipabe)then
        I0_orabe=11
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L_imube.and..not.L0_epabe)then
        I0_orabe=12
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_apabe.and..not.L0_umabe)then
        I0_orabe=13
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_omabe.and..not.L0_imabe)then
        I0_orabe=14
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_emabe.and..not.L0_amabe)then
        I0_orabe=15
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ulabe.and..not.L0_olabe)then
        I0_orabe=16
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ilabe.and..not.L0_elabe)then
        I0_orabe=17
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_alabe.and..not.L0_ukabe)then
        I0_orabe=18
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_okabe.and..not.L0_ikabe)then
        I0_orabe=19
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ufabe.and..not.L0_ofabe)then
        I0_orabe=20
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ifabe.and..not.L0_efabe)then
        I0_orabe=21
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_afabe.and..not.L0_udabe)then
        I0_orabe=22
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_odabe.and..not.L0_idabe)then
        I0_orabe=23
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_edabe.and..not.L0_adabe)then
        I0_orabe=24
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ubabe.and..not.L0_obabe)then
        I0_orabe=25
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ibabe.and..not.L0_ebabe)then
        I0_orabe=26
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ababe.and..not.L0_uxux)then
        I0_orabe=27
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_oxux.and..not.L0_ixux)then
        I0_orabe=28
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_exux.and..not.L0_axux)then
        I0_orabe=29
        L0_urabe=.true.
      endif

      if(.not.L0_urabe.and.L0_ivux.and..not.L0_evux)then
        I0_orabe=30
        L0_urabe=.true.
      endif

      if(I0_irabe.eq.1)then
         L0_arabe=.false.
         L0_akabe=.false.
         L0_ovux=.false.
         L0_utux=.false.
         L0_itux=.false.
         L0_atux=.false.
         L0_osux=.false.
         L0_esux=.false.
         L0_urux=.false.
         L0_opabe=.false.
         L0_ipabe=.false.
         L0_epabe=.false.
         L0_umabe=.false.
         L0_imabe=.false.
         L0_amabe=.false.
         L0_olabe=.false.
         L0_elabe=.false.
         L0_ukabe=.false.
         L0_ikabe=.false.
         L0_ofabe=.false.
         L0_efabe=.false.
         L0_udabe=.false.
         L0_idabe=.false.
         L0_adabe=.false.
         L0_obabe=.false.
         L0_ebabe=.false.
         L0_uxux=.false.
         L0_ixux=.false.
         L0_axux=.false.
         L0_evux=.false.
      else
         L0_arabe=L0_erabe
         L0_akabe=L0_ekabe
         L0_ovux=L0_uvux
         L0_utux=L_avux
         L0_itux=L_otux
         L0_atux=L_etux
         L0_osux=L0_usux
         L0_esux=L_isux
         L0_urux=L_asux
         L0_opabe=L_upabe
         L0_ipabe=L_omube
         L0_epabe=L_imube
         L0_umabe=L0_apabe
         L0_imabe=L0_omabe
         L0_amabe=L0_emabe
         L0_olabe=L0_ulabe
         L0_elabe=L0_ilabe
         L0_ukabe=L0_alabe
         L0_ikabe=L0_okabe
         L0_ofabe=L0_ufabe
         L0_efabe=L0_ifabe
         L0_udabe=L0_afabe
         L0_idabe=L0_odabe
         L0_adabe=L0_edabe
         L0_obabe=L0_ubabe
         L0_ebabe=L0_ibabe
         L0_uxux=L0_ababe
         L0_ixux=L0_oxux
         L0_axux=L0_exux
         L0_evux=L0_ivux
      endif
C control_th.fgi( 289, 261):��������� �������
      select case (I0_orabe)
      case (1)
        C255_orux="��� �����"
      case (2)
        C255_orux="���������� � UZ"
      case (3)
        C255_orux="������������"
      case (4)
        C255_orux="��� 380�"
      case (5)
        C255_orux="�������������"
      case (6)
        C255_orux="����� � ������"
      case (7)
        C255_orux="����� ����"
      case (8)
        C255_orux="������ ������"
      case (9)
        C255_orux="������ �� ����"
      case (10)
        C255_orux="������ �����"
      case (11)
        C255_orux="����� �������"
      case (12)
        C255_orux="����������������"
      case (13)
        C255_orux="����� ����"
      case (14)
        C255_orux="����� ������� �����"
      case (15)
        C255_orux="������ �������"
      case (16)
        C255_orux="������ ����������"
      case (17)
        C255_orux="�� ����������"
      case (18)
        C255_orux="???"
      case (19)
        C255_orux="???"
      case (20)
        C255_orux="???"
      case (21)
        C255_orux="???"
      case (22)
        C255_orux="???"
      case (23)
        C255_orux="???"
      case (24)
        C255_orux="???"
      case (25)
        C255_orux="???"
      case (26)
        C255_orux="???"
      case (27)
        C255_orux="???"
      case (28)
        C255_orux="???"
      case (29)
        C255_orux="???"
      case (30)
        C255_orux="???"
      end select
C control_th.fgi( 306, 290):MarkVI text out 30 variants,mo_text5
      L0_oribe=.false.
      I0_iribe=0

      if(L0_amade)then
        I0_akube=1
         L0_aribe=.false.
         L0_ufibe=.false.
         L0_ovebe=.false.
         L0_avebe=.false.
         L0_utebe=.false.
         L0_otebe=.false.
         L0_itebe=.false.
         L0_etebe=.false.
         L0_atebe=.false.
         L0_opibe=.false.
         L0_epibe=.false.
         L0_umibe=.false.
         L0_imibe=.false.
         L0_amibe=.false.
         L0_olibe=.false.
         L0_elibe=.false.
         L0_ukibe=.false.
         L0_ikibe=.false.
         L0_akibe=.false.
         L0_ifibe=.false.
         L0_afibe=.false.
         L0_odibe=.false.
         L0_edibe=.false.
         L0_ubibe=.false.
         L0_ibibe=.false.
         L0_abibe=.false.
         L0_oxebe=.false.
         L0_exebe=.false.
         L0_uvebe=.false.
         L0_evebe=.false.
      endif

      if(L0_aribe)I0_iribe=I0_iribe+1
      if(L0_ufibe)I0_iribe=I0_iribe+1
      if(L0_ovebe)I0_iribe=I0_iribe+1
      if(L0_avebe)I0_iribe=I0_iribe+1
      if(L0_utebe)I0_iribe=I0_iribe+1
      if(L0_otebe)I0_iribe=I0_iribe+1
      if(L0_itebe)I0_iribe=I0_iribe+1
      if(L0_etebe)I0_iribe=I0_iribe+1
      if(L0_atebe)I0_iribe=I0_iribe+1
      if(L0_opibe)I0_iribe=I0_iribe+1
      if(L0_epibe)I0_iribe=I0_iribe+1
      if(L0_umibe)I0_iribe=I0_iribe+1
      if(L0_imibe)I0_iribe=I0_iribe+1
      if(L0_amibe)I0_iribe=I0_iribe+1
      if(L0_olibe)I0_iribe=I0_iribe+1
      if(L0_elibe)I0_iribe=I0_iribe+1
      if(L0_ukibe)I0_iribe=I0_iribe+1
      if(L0_ikibe)I0_iribe=I0_iribe+1
      if(L0_akibe)I0_iribe=I0_iribe+1
      if(L0_ifibe)I0_iribe=I0_iribe+1
      if(L0_afibe)I0_iribe=I0_iribe+1
      if(L0_odibe)I0_iribe=I0_iribe+1
      if(L0_edibe)I0_iribe=I0_iribe+1
      if(L0_ubibe)I0_iribe=I0_iribe+1
      if(L0_ibibe)I0_iribe=I0_iribe+1
      if(L0_abibe)I0_iribe=I0_iribe+1
      if(L0_oxebe)I0_iribe=I0_iribe+1
      if(L0_exebe)I0_iribe=I0_iribe+1
      if(L0_uvebe)I0_iribe=I0_iribe+1
      if(L0_evebe)I0_iribe=I0_iribe+1


      if(.not.L0_oribe.and.L_eribe.and..not.L0_aribe)then
        I0_akube=1
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(149).and..not.L0_ufibe)then
        I0_akube=2
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(148).and..not.L0_ovebe)then
        I0_akube=3
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(147).and..not.L0_avebe)then
        I0_akube=4
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(146).and..not.L0_utebe)then
        I0_akube=5
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(145).and..not.L0_otebe)then
        I0_akube=6
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(144).and..not.L0_itebe)then
        I0_akube=7
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(143).and..not.L0_etebe)then
        I0_akube=8
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L_(142).and..not.L0_atebe)then
        I0_akube=9
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_upibe.and..not.L0_opibe)then
        I0_akube=10
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ipibe.and..not.L0_epibe)then
        I0_akube=11
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_apibe.and..not.L0_umibe)then
        I0_akube=12
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_omibe.and..not.L0_imibe)then
        I0_akube=13
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_emibe.and..not.L0_amibe)then
        I0_akube=14
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ulibe.and..not.L0_olibe)then
        I0_akube=15
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ilibe.and..not.L0_elibe)then
        I0_akube=16
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_alibe.and..not.L0_ukibe)then
        I0_akube=17
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_okibe.and..not.L0_ikibe)then
        I0_akube=18
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ekibe.and..not.L0_akibe)then
        I0_akube=19
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ofibe.and..not.L0_ifibe)then
        I0_akube=20
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_efibe.and..not.L0_afibe)then
        I0_akube=21
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_udibe.and..not.L0_odibe)then
        I0_akube=22
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_idibe.and..not.L0_edibe)then
        I0_akube=23
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_adibe.and..not.L0_ubibe)then
        I0_akube=24
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_obibe.and..not.L0_ibibe)then
        I0_akube=25
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ebibe.and..not.L0_abibe)then
        I0_akube=26
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_uxebe.and..not.L0_oxebe)then
        I0_akube=27
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ixebe.and..not.L0_exebe)then
        I0_akube=28
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_axebe.and..not.L0_uvebe)then
        I0_akube=29
        L0_oribe=.true.
      endif

      if(.not.L0_oribe.and.L0_ivebe.and..not.L0_evebe)then
        I0_akube=30
        L0_oribe=.true.
      endif

      if(I0_iribe.eq.1)then
         L0_aribe=.false.
         L0_ufibe=.false.
         L0_ovebe=.false.
         L0_avebe=.false.
         L0_utebe=.false.
         L0_otebe=.false.
         L0_itebe=.false.
         L0_etebe=.false.
         L0_atebe=.false.
         L0_opibe=.false.
         L0_epibe=.false.
         L0_umibe=.false.
         L0_imibe=.false.
         L0_amibe=.false.
         L0_olibe=.false.
         L0_elibe=.false.
         L0_ukibe=.false.
         L0_ikibe=.false.
         L0_akibe=.false.
         L0_ifibe=.false.
         L0_afibe=.false.
         L0_odibe=.false.
         L0_edibe=.false.
         L0_ubibe=.false.
         L0_ibibe=.false.
         L0_abibe=.false.
         L0_oxebe=.false.
         L0_exebe=.false.
         L0_uvebe=.false.
         L0_evebe=.false.
      else
         L0_aribe=L_eribe
         L0_ufibe=L_(149)
         L0_ovebe=L_(148)
         L0_avebe=L_(147)
         L0_utebe=L_(146)
         L0_otebe=L_(145)
         L0_itebe=L_(144)
         L0_etebe=L_(143)
         L0_atebe=L_(142)
         L0_opibe=L0_upibe
         L0_epibe=L0_ipibe
         L0_umibe=L0_apibe
         L0_imibe=L0_omibe
         L0_amibe=L0_emibe
         L0_olibe=L0_ulibe
         L0_elibe=L0_ilibe
         L0_ukibe=L0_alibe
         L0_ikibe=L0_okibe
         L0_akibe=L0_ekibe
         L0_ifibe=L0_ofibe
         L0_afibe=L0_efibe
         L0_odibe=L0_udibe
         L0_edibe=L0_idibe
         L0_ubibe=L0_adibe
         L0_ibibe=L0_obibe
         L0_abibe=L0_ebibe
         L0_oxebe=L0_uxebe
         L0_exebe=L0_ixebe
         L0_uvebe=L0_axebe
         L0_evebe=L0_ivebe
      endif
C control_th.fgi( 371, 196):��������� �������
      select case (I0_akube)
      case (1)
        C255_itobe="���.�� �������"
      case (2)
        C255_itobe="���.������ ������ ������� �������"
      case (3)
        C255_itobe="���.�������� ������ ������� �������"
      case (4)
        C255_itobe="���.����������� ������ ������� �������"
      case (5)
        C255_itobe="���.��������� ������ ������� �������"
      case (6)
        C255_itobe="���.������ �� ������ ������� �������"
      case (7)
        C255_itobe="���.�������� �� ������ ������� �������"
      case (8)
        C255_itobe="���.����������� �� ������ ������� �������"
      case (9)
        C255_itobe="���.��������� �� ������ ������� �������"
      case (10)
        C255_itobe="???"
      case (11)
        C255_itobe="???"
      case (12)
        C255_itobe="???"
      case (13)
        C255_itobe="???"
      case (14)
        C255_itobe="???"
      case (15)
        C255_itobe="???"
      case (16)
        C255_itobe="???"
      case (17)
        C255_itobe="???"
      case (18)
        C255_itobe="???"
      case (19)
        C255_itobe="???"
      case (20)
        C255_itobe="???"
      case (21)
        C255_itobe="???"
      case (22)
        C255_itobe="???"
      case (23)
        C255_itobe="???"
      case (24)
        C255_itobe="???"
      case (25)
        C255_itobe="???"
      case (26)
        C255_itobe="???"
      case (27)
        C255_itobe="???"
      case (28)
        C255_itobe="???"
      case (29)
        C255_itobe="???"
      case (30)
        C255_itobe="???"
      end select
C control_th.fgi( 386, 225):MarkVI text out 30 variants,mo_text2
      I_(47) = I_obex + (-I_edex)
C control_tg.fgi(  32,  47):��������
C label 1467  try1467=try1467-1
      I_(46)=abs(I_(47))
C control_tg.fgi(  39,  47):���������� ��������
      L_(25)=I_(46).lt.I0_erav
C control_tg.fgi(  49,  47):���������� �������������
      if(L_esax) then
         I0_isax=I_ufox
      endif
      I_usax=I0_isax
C control_tg.fgi( 126,  57):�������-��������
      I_(45) = I_ufox + (-I_usax)
C control_tg.fgi(  32,  39):��������
      I_(44)=abs(I_(45))
C control_tg.fgi(  39,  39):���������� ��������
      L_(24)=I_(44).lt.I0_arav
C control_tg.fgi(  49,  39):���������� �������������
      L_(26) = L_(25).AND.L_(24).AND.L_alov
C control_tg.fgi(  66,  45):�
      L_(21)=L_(26).and..not.L0_upav
      L0_upav=L_(26)
C control_tg.fgi(  86,  45):������������  �� 1 ���
      L_esax=(L_(21).or.L_esax).and..not.(L_(22))
      L_(23)=.not.L_esax
C control_tg.fgi(  95,  43):RS �������
      L_ebex=L_esax
C control_tg.fgi( 155,  45):������,capt_take
      if(L_ebex) then
         I0_abex=I_edex
      endif
      I_(53)=I0_abex
C control_tg.fgi(  68,  81):�������-��������
      R_(11)=I_(53)
C control_tg.fgi(  73,  82):��������� IN->RE
      R_(10) = (-R_(11)) + R_(7)
C control_tg.fgi(  36,  75):��������
      if(L_ebex) then
         R0_uxax=R_(11)
      elseif(L_(43)) then
         R0_uxax=R0_uxax
      else
         R0_uxax=R0_uxax+deltat/R_(8)*R_(10)
      endif
      if(R0_uxax.gt.R_(11)) then
         R0_uxax=R_(11)
      elseif(R0_uxax.lt.R_(9)) then
         R0_uxax=R_(9)
      endif
C control_tg.fgi(  46,  75):����������
      I_(54)=R0_uxax
C control_tg.fgi(  56,  75):��������� RE->IN
      if(L_ebex) then
         I_obex=I_edex
      else
         I_obex=I_(54)
      endif
C control_tg.fgi(  67,  73):���� RE IN LO CH7
C sav1=I_(47)
      I_(47) = I_obex + (-I_edex)
C control_tg.fgi(  32,  47):recalc:��������
C if(sav1.ne.I_(47) .and. try1467.gt.0) goto 1467
      I_ibex=I_obex
C control_tg.fgi( 155,  80):������,bun_y_coord
      L0_usov=.false.
      I0_osov=0

      if(L0_amade)then
        I0_ekix=1
         L0_esov=.false.
         L0_ukov=.false.
         L0_ixiv=.false.
         L0_uviv=.false.
         L0_iviv=.false.
         L0_aviv=.false.
         L0_otiv=.false.
         L0_etiv=.false.
         L0_usiv=.false.
         L0_urov=.false.
         L0_irov=.false.
         L0_arov=.false.
         L0_opov=.false.
         L0_epov=.false.
         L0_umov=.false.
         L0_imov=.false.
         L0_amov=.false.
         L0_olov=.false.
         L0_elov=.false.
         L0_ikov=.false.
         L0_akov=.false.
         L0_ofov=.false.
         L0_efov=.false.
         L0_udov=.false.
         L0_idov=.false.
         L0_adov=.false.
         L0_obov=.false.
         L0_ebov=.false.
         L0_uxiv=.false.
         L0_axiv=.false.
      endif

      if(L0_esov)I0_osov=I0_osov+1
      if(L0_ukov)I0_osov=I0_osov+1
      if(L0_ixiv)I0_osov=I0_osov+1
      if(L0_uviv)I0_osov=I0_osov+1
      if(L0_iviv)I0_osov=I0_osov+1
      if(L0_aviv)I0_osov=I0_osov+1
      if(L0_otiv)I0_osov=I0_osov+1
      if(L0_etiv)I0_osov=I0_osov+1
      if(L0_usiv)I0_osov=I0_osov+1
      if(L0_urov)I0_osov=I0_osov+1
      if(L0_irov)I0_osov=I0_osov+1
      if(L0_arov)I0_osov=I0_osov+1
      if(L0_opov)I0_osov=I0_osov+1
      if(L0_epov)I0_osov=I0_osov+1
      if(L0_umov)I0_osov=I0_osov+1
      if(L0_imov)I0_osov=I0_osov+1
      if(L0_amov)I0_osov=I0_osov+1
      if(L0_olov)I0_osov=I0_osov+1
      if(L0_elov)I0_osov=I0_osov+1
      if(L0_ikov)I0_osov=I0_osov+1
      if(L0_akov)I0_osov=I0_osov+1
      if(L0_ofov)I0_osov=I0_osov+1
      if(L0_efov)I0_osov=I0_osov+1
      if(L0_udov)I0_osov=I0_osov+1
      if(L0_idov)I0_osov=I0_osov+1
      if(L0_adov)I0_osov=I0_osov+1
      if(L0_obov)I0_osov=I0_osov+1
      if(L0_ebov)I0_osov=I0_osov+1
      if(L0_uxiv)I0_osov=I0_osov+1
      if(L0_axiv)I0_osov=I0_osov+1


      if(.not.L0_usov.and.L0_isov.and..not.L0_esov)then
        I0_ekix=1
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L_alov.and..not.L0_ukov)then
        I0_ekix=2
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L_oxiv.and..not.L0_ixiv)then
        I0_ekix=3
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L_ebex.and..not.L0_uviv)then
        I0_ekix=4
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_oviv.and..not.L0_iviv)then
        I0_ekix=5
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_eviv.and..not.L0_aviv)then
        I0_ekix=6
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_utiv.and..not.L0_otiv)then
        I0_ekix=7
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_itiv.and..not.L0_etiv)then
        I0_ekix=8
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ativ.and..not.L0_usiv)then
        I0_ekix=9
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_asov.and..not.L0_urov)then
        I0_ekix=10
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_orov.and..not.L0_irov)then
        I0_ekix=11
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_erov.and..not.L0_arov)then
        I0_ekix=12
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_upov.and..not.L0_opov)then
        I0_ekix=13
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ipov.and..not.L0_epov)then
        I0_ekix=14
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_apov.and..not.L0_umov)then
        I0_ekix=15
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_omov.and..not.L0_imov)then
        I0_ekix=16
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_emov.and..not.L0_amov)then
        I0_ekix=17
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ulov.and..not.L0_olov)then
        I0_ekix=18
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ilov.and..not.L0_elov)then
        I0_ekix=19
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_okov.and..not.L0_ikov)then
        I0_ekix=20
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ekov.and..not.L0_akov)then
        I0_ekix=21
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ufov.and..not.L0_ofov)then
        I0_ekix=22
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ifov.and..not.L0_efov)then
        I0_ekix=23
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_afov.and..not.L0_udov)then
        I0_ekix=24
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_odov.and..not.L0_idov)then
        I0_ekix=25
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_edov.and..not.L0_adov)then
        I0_ekix=26
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ubov.and..not.L0_obov)then
        I0_ekix=27
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_ibov.and..not.L0_ebov)then
        I0_ekix=28
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_abov.and..not.L0_uxiv)then
        I0_ekix=29
        L0_usov=.true.
      endif

      if(.not.L0_usov.and.L0_exiv.and..not.L0_axiv)then
        I0_ekix=30
        L0_usov=.true.
      endif

      if(I0_osov.eq.1)then
         L0_esov=.false.
         L0_ukov=.false.
         L0_ixiv=.false.
         L0_uviv=.false.
         L0_iviv=.false.
         L0_aviv=.false.
         L0_otiv=.false.
         L0_etiv=.false.
         L0_usiv=.false.
         L0_urov=.false.
         L0_irov=.false.
         L0_arov=.false.
         L0_opov=.false.
         L0_epov=.false.
         L0_umov=.false.
         L0_imov=.false.
         L0_amov=.false.
         L0_olov=.false.
         L0_elov=.false.
         L0_ikov=.false.
         L0_akov=.false.
         L0_ofov=.false.
         L0_efov=.false.
         L0_udov=.false.
         L0_idov=.false.
         L0_adov=.false.
         L0_obov=.false.
         L0_ebov=.false.
         L0_uxiv=.false.
         L0_axiv=.false.
      else
         L0_esov=L0_isov
         L0_ukov=L_alov
         L0_ixiv=L_oxiv
         L0_uviv=L_ebex
         L0_iviv=L0_oviv
         L0_aviv=L0_eviv
         L0_otiv=L0_utiv
         L0_etiv=L0_itiv
         L0_usiv=L0_ativ
         L0_urov=L0_asov
         L0_irov=L0_orov
         L0_arov=L0_erov
         L0_opov=L0_upov
         L0_epov=L0_ipov
         L0_umov=L0_apov
         L0_imov=L0_omov
         L0_amov=L0_emov
         L0_olov=L0_ulov
         L0_elov=L0_ilov
         L0_ikov=L0_okov
         L0_akov=L0_ekov
         L0_ofov=L0_ufov
         L0_efov=L0_ifov
         L0_udov=L0_afov
         L0_idov=L0_odov
         L0_adov=L0_edov
         L0_obov=L0_ubov
         L0_ebov=L0_ibov
         L0_uxiv=L0_abov
         L0_axiv=L0_exiv
      endif
C control_tg.fgi( 371, 196):��������� �������
      select case (I0_ekix)
      case (1)
        C255_itex="��������� �������� ������������"
      case (2)
        C255_itex="������� ������"
      case (3)
        C255_itex="������� �������"
      case (4)
        C255_itex="������� �����"
      case (5)
        C255_itex="???"
      case (6)
        C255_itex="???"
      case (7)
        C255_itex="???"
      case (8)
        C255_itex="???"
      case (9)
        C255_itex="???"
      case (10)
        C255_itex="???"
      case (11)
        C255_itex="???"
      case (12)
        C255_itex="???"
      case (13)
        C255_itex="???"
      case (14)
        C255_itex="???"
      case (15)
        C255_itex="???"
      case (16)
        C255_itex="???"
      case (17)
        C255_itex="???"
      case (18)
        C255_itex="???"
      case (19)
        C255_itex="???"
      case (20)
        C255_itex="???"
      case (21)
        C255_itex="???"
      case (22)
        C255_itex="???"
      case (23)
        C255_itex="???"
      case (24)
        C255_itex="???"
      case (25)
        C255_itex="???"
      case (26)
        C255_itex="???"
      case (27)
        C255_itex="???"
      case (28)
        C255_itex="???"
      case (29)
        C255_itex="???"
      case (30)
        C255_itex="???"
      end select
C control_tg.fgi( 386, 225):MarkVI text out 30 variants,tg_text2
      if(L_ebex) then
         I_etax=1
      else
         I_etax=0
      endif
C control_tg.fgi(  73,  66):��������� LO->1
      I_atax=I_etax
C control_tg.fgi( 155,  70):������,capt1
      if(L_esax) then
         I_ufube=I_(42)
      else
         I_ufube=I_(43)
      endif
C control_tg.fgi( 125,  38):���� RE IN LO CH7
      select case (I_ufube)
      case (1)
        C255_etobe="������� ��������� �� ���."
      case (2)
        C255_etobe="�������� ����"
      case (3)
        C255_etobe="��������� ���"
      case (4)
        C255_etobe="???"
      case (5)
        C255_etobe="???"
      case (6)
        C255_etobe="???"
      case (7)
        C255_etobe="???"
      case (8)
        C255_etobe="???"
      case (9)
        C255_etobe="???"
      case (10)
        C255_etobe="???"
      case (11)
        C255_etobe="???"
      case (12)
        C255_etobe="???"
      case (13)
        C255_etobe="???"
      case (14)
        C255_etobe="???"
      case (15)
        C255_etobe="???"
      case (16)
        C255_etobe="???"
      case (17)
        C255_etobe="???"
      case (18)
        C255_etobe="???"
      case (19)
        C255_etobe="???"
      case (20)
        C255_etobe="???"
      case (21)
        C255_etobe="???"
      case (22)
        C255_etobe="???"
      case (23)
        C255_etobe="???"
      case (24)
        C255_etobe="???"
      case (25)
        C255_etobe="???"
      case (26)
        C255_etobe="???"
      case (27)
        C255_etobe="???"
      case (28)
        C255_etobe="???"
      case (29)
        C255_etobe="???"
      case (30)
        C255_etobe="???"
      end select
C control_th.fgi( 386, 155):MarkVI text out 30 variants,mo_text3
      if(L_(24)) then
         I_ipav=I_(41)
      else
         I_ipav=I_(38)
      endif
C control_tg.fgi( 125,  29):���� RE IN LO CH7
      I_osax=I_usax
C control_tg.fgi( 155,  62):������,bun_x_coord
      End
