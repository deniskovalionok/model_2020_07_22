      Subroutine FDB(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDB_ConIn
      R_(8)=R0_idix
C FDB_lamp.fgi(  63, 147):pre: �������� ��������� ������
      R_(7)=R0_ubix
C FDB_lamp.fgi(  63, 158):pre: �������� ��������� ������
      R_(6)=R0_ebix
C FDB_lamp.fgi(  63, 169):pre: �������� ��������� ������
      R_(5)=R0_ilex
C FDB_lamp.fgi(  63, 130):pre: �������� ��������� ������
      R_(4)=R0_ukex
C FDB_lamp.fgi(  63, 119):pre: �������� ��������� ������
      R_(3)=R0_ekex
C FDB_lamp.fgi(  63, 108):pre: �������� ��������� ������
      R_(2)=R0_ofex
C FDB_lamp.fgi(  63,  97):pre: �������� ��������� ������
      R_(1)=R0_ebex
C FDB_lamp.fgi(  69, 181):pre: �������� ��������� ������
      L_idod=.false.
      L_ibod=.false.
      L_ikod=.false.
      L_ifod=.false.
C FDB_mechanic_vlv.fgi(  15,  20):pre: ���������� ������� ����� ��,BOAT01T
      !{
      Call TELEGKA_HANDLER(deltat,REAL(R_ur,4),R8_el,R_ele
     &,
     & L_ise,R_oke,L_ure,
     & L_are,L_ere,R_uke,L_ase,
     & R_ale,L_ese,L_eme,
     & L_ome,L_ime,L_er,L_ume,
     & L_ape,L_epe,L_ar,L_ule,
     & L_ame,L_ile,L_ole,L_ofe,
     & L_ot,L_ut,L_av,L_ev,L_uv,L_as,L_iv,L_ov,L_is,L_es,
     & L_os,L_(1),L_(2),R_ad,R_ed,I_ibe,I_afe,R_ek,
     & REAL(R_ok,4),R_of,REAL(R_ak,4),I_ep,
     & I_ap,I_obe,I_ade,C20_em,C20_im,C8_om,R_id,
     & REAL(R_ud,4),L_ef,R_af,
     & REAL(R_if,4),I_ude,R_op,R_ip,I_ede,I_ube,I_abe,
     & L_uk,L_ife,L_ipe,L_ik,L_uf,
     & L_ax,L_ex,L_il,L_ol,REAL(R_am,4),L_ake,
     & L_od,L_um,L_ire,L_ir,L_or,REAL(R8_eduk,8),
     & REAL(1.0,4),R8_umuk,L_i,L_e,L_ufe,I_efe,L_u,L_o,R_ux
     &,
     & L_ope,R_al,REAL(R_ul,4),L_upe,L_ode,L_ore,
     & REAL(R_ur,4),L_us,L_at,L_et,L_ix,L_ox,L_it)
      !}
C FDB_mechanic_vlv.fgi( 116, 127):���������� �������,20FDB50AE402
      !{
      Call TELEGKA_HANDLER(deltat,REAL(R_ali,4),R8_ibi,R_ibo
     &,
     & L_olo,R_uxi,L_alo,
     & L_eko,L_iko,R_abo,L_elo,
     & R_ebo,L_ilo,L_ido,
     & L_udo,L_odo,L_iki,L_afo,
     & L_efo,L_ifo,L_eki,L_ado,
     & L_edo,L_obo,L_ubo,L_uvi,
     & L_umi,L_api,L_epi,L_ipi,L_ari,L_eli,L_opi,L_upi,L_oli
     &,L_ili,
     & L_uli,L_(3),L_(4),R_ixod,R_ite,I_osi,I_evi,R_ixe,
     & REAL(R_uxe,4),R_uve,REAL(R_exe,4),I_ifi,
     & I_efi,I_usi,I_eti,C20_idi,C20_odi,C8_udi,R_ote,
     & REAL(R_ave,4),L_ive,R_eve,
     & REAL(R_ove,4),I_avi,R_ufi,R_ofi,I_iti,I_ati,I_esi,
     & L_abi,L_ovi,L_ofo,L_oxe,L_axe,
     & L_eri,L_iri,L_obi,L_ubi,REAL(R_edi,4),L_exi,
     & L_ute,L_afi,L_oko,L_oki,L_uki,REAL(R8_eduk,8),
     & REAL(1.0,4),R8_umuk,L_use,L_ose,L_axi,I_ivi,L_ete,L_ate
     &,R_asi,
     & L_ufo,R_ebi,REAL(R_adi,4),L_ako,L_uti,L_uko,
     & REAL(R_ali,4),L_ami,L_emi,L_imi,L_ori,L_uri,L_omi)
      !}
C FDB_mechanic_vlv.fgi(  88, 127):���������� �������,20FDB50AE400
      Call LODOCHKA_HANDLER_IT_MAIN(deltat,REAL(R_ixod,4)
     &,
     & REAL(R_ebud,4),L_uvod,
     & L_evod,L_ivod,L_axod,
     & L_avod,L_utod,R_ekud,R_udud,L_erud,
     & L_oxod,L_oxid,L_ixid,R_ikud,
     & R_afud,L_irud,L_exid,R_okud,
     & R_efud,L_orud,I_urud,I_asud,I_esud,
     & I_isud,I_osud,INT(I_atod,4),L_etod,
     & L_imod,L_omod,L_umod,
     & L_apod,L_epod,L_ipod,
     & L_opod,L_upod,L_arod,L_erod,
     & I_ubud,I_avid,R_usud,REAL(R_asod,4),R_atud,
     & REAL(R_urod,4),REAL(R_orod,4),R_etud,R_itud,
     & R_otud,REAL(R_itod,4),R_utud,
     & REAL(R_otod,4),R_ofud,R_edud,L_opud,L_ifud,
     & L_uxod,REAL(R_abud,4),R_elaf,R_evud,
     & R_ivud,R_ovud,REAL(R_ulod,4),R_uvud,
     & REAL(R_ilod,4),REAL(R_ukod,4),R_axud,
     & REAL(R_alod,4),R_exud,R_ufud,R_idud,
     & L_upud,R_ixud,R_oxud,R_uxud,L_idod,
     & R_abaf,R_ebaf,REAL(R_edod,4),R_ibaf,R_akud,
     & R_odud,L_arud,R_obaf,R_ubaf,R_adaf,R_edaf,
     & R_idaf,REAL(R_efod,4),R_odaf,L_ifod,
     & L_ukud,L_afod,REAL(R_udod,4),L_ofod,
     & R_udaf,R_afaf,R_efaf,REAL(R_ekod,4),R_ifaf,
     & L_ikod,L_alud,L_akod,
     & REAL(R_ufod,4),L_okod,R_ofaf,R_ufaf,
     & R_akaf,R_ekaf,REAL(R_ebod,4),R_ikaf,
     & L_ibod,L_adod,L_odod,L_elud,
     & L_ilud,L_abod,R_ilid,R_emud,
     & REAL(R_ubod,4),REAL(R_uxid,4),L_obod,
     & R_imud,I_ivid,R_urid,R_omud,R_avud,R_arid,C20_esid
     &)
C FDB_mechanic_vlv.fgi(  15,  20):���������� ������� ����� ��,BOAT01T
      Call LODOCHKA_HANDLER_FDB60_COORD(deltat,I_esaf,R_asaf
     &,
     & L_ipaf,L_umaf,L_upaf,
     & L_epaf,L_eraf,L_ilaf,L_olaf,
     & L_ulaf,L_amaf,L_emaf,L_imaf,
     & L_omaf,L_opaf,L_apaf,I_isaf,
     & REAL(R_elaf,4),R_uraf,1,I_oraf,
     & L_araf,I_iraf)
C FDB_mechanic_vlv.fgi(  15,   8):���������� ���������� ������� �� ������� FDB60,BOAT01T
      !{
      Call SHIB_HANDLER(deltat,R8_evif,R_orof,R_urof,R_amof
     &,R_olof,
     & L_upof,R_emof,R_ulof,L_arof,R_elof,
     & R_ilof,R_opof,R_erof,R_irof,R_upif,
     & R_ipof,R_opif,REAL(R_ivif,4),C20_uvif,I_afof,I_ekof
     &,
     & R_atif,REAL(R_itif,4),R_isif,
     & REAL(R_usif,4),I_uxif,I_oxif,I_efof,I_ufof,C20_ovif
     &,
     & C20_axif,C8_exif,L_irif,R_erif,
     & REAL(R_orif,4),L_asif,R_urif,
     & REAL(R_esif,4),I_ofof,I_akof,I_ifof,I_udof,L_otif,
     & L_okof,L_omof,L_etif,L_osif,
     & L_avif,L_utif,L_alof,L_ixif,L_imof,L_ebof,L_ibof,
     & REAL(R8_arif,8),REAL(1.0,4),R8_abof,L_(16),L_apif,L_
     &(15),L_umif,
     & L_ukof,I_ikof,L_umof,L_ipif,L_epof,L_epif,L_obof,L_ubof
     &,L_adof,L_idof,
     & L_odof,L_edof)
      !}
C FDB_mechanic_vlv.fgi(  45, 178):���������� ������,20FDB23AE002
      !{
      Call SHIB_HANDLER(deltat,R8_imos,R_ufus,R_akus,R_ebus
     &,R_uxos,
     & L_afus,R_ibus,R_abus,L_efus,R_ixos,
     & R_oxos,R_udus,R_ifus,R_ofus,R_afos,
     & R_odus,R_udos,REAL(R_omos,4),C20_apos,I_etos,I_ivos
     &,
     & R_elos,REAL(R_olos,4),R_okos,
     & REAL(R_alos,4),I_aros,I_upos,I_itos,I_avos,C20_umos
     &,
     & C20_epos,C8_ipos,L_ofos,R_ifos,
     & REAL(R_ufos,4),L_ekos,R_akos,
     & REAL(R_ikos,4),I_utos,I_evos,I_otos,I_atos,L_ulos,
     & L_uvos,L_ubus,L_ilos,L_ukos,
     & L_emos,L_amos,L_exos,L_opos,L_obus,L_iros,L_oros,
     & REAL(R8_efos,8),REAL(1.0,4),R8_eros,L_(53),L_edos,L_
     &(52),L_ados,
     & L_axos,I_ovos,L_adus,L_odos,L_idus,L_idos,L_uros,L_asos
     &,L_esos,L_osos,
     & L_usos,L_isos)
      !}
C FDB_mechanic_vlv.fgi( 130, 178):���������� ������,20FDB50AB007
      !{
      Call SHIB_HANDLER(deltat,R8_orus,R_amat,R_emat,R_ifat
     &,R_afat,
     & L_elat,R_ofat,R_efat,L_ilat,R_odat,
     & R_udat,R_alat,R_olat,R_ulat,R_elus,
     & R_ukat,R_alus,REAL(R_urus,4),C20_esus,I_ixus,I_obat
     &,
     & R_ipus,REAL(R_upus,4),R_umus,
     & REAL(R_epus,4),I_etus,I_atus,I_oxus,I_ebat,C20_asus
     &,
     & C20_isus,C8_osus,L_ulus,R_olus,
     & REAL(R_amus,4),L_imus,R_emus,
     & REAL(R_omus,4),I_abat,I_ibat,I_uxus,I_exus,L_arus,
     & L_adat,L_akat,L_opus,L_apus,
     & L_irus,L_erus,L_idat,L_usus,L_ufat,L_otus,L_utus,
     & REAL(R8_ilus,8),REAL(1.0,4),R8_itus,L_(55),L_ikus,L_
     &(54),L_ekus,
     & L_edat,I_ubat,L_ekat,L_ukus,L_okat,L_okus,L_avus,L_evus
     &,L_ivus,L_uvus,
     & L_axus,L_ovus)
      !}
C FDB_mechanic_vlv.fgi( 116, 178):���������� ������,20FDB50AB006
      !{
      Call SHIB_HANDLER(deltat,R8_osor,R_apur,R_epur,R_ikur
     &,R_akur,
     & L_emur,R_okur,R_ekur,L_imur,R_ofur,
     & R_ufur,R_amur,R_omur,R_umur,R_emor,
     & R_ulur,R_amor,REAL(R_usor,4),C20_etor,I_ibur,I_odur
     &,
     & R_iror,REAL(R_uror,4),R_upor,
     & REAL(R_eror,4),I_evor,I_avor,I_obur,I_edur,C20_ator
     &,
     & C20_itor,C8_otor,L_umor,R_omor,
     & REAL(R_apor,4),L_ipor,R_epor,
     & REAL(R_opor,4),I_adur,I_idur,I_ubur,I_ebur,L_asor,
     & L_afur,L_alur,L_oror,L_aror,
     & L_isor,L_esor,L_ifur,L_utor,L_ukur,L_ovor,L_uvor,
     & REAL(R8_imor,8),REAL(1.0,4),R8_ivor,L_(45),L_ilor,L_
     &(44),L_elor,
     & L_efur,I_udur,L_elur,L_ulor,L_olur,L_olor,L_axor,L_exor
     &,L_ixor,L_uxor,
     & L_abur,L_oxor)
      !}
C FDB_mechanic_vlv.fgi( 102, 178):���������� ������,20FDB50AB005
      !{
      Call SHIB_HANDLER(deltat,R8_uvur,R_esas,R_isas,R_omas
     &,R_emas,
     & L_iras,R_umas,R_imas,L_oras,R_ulas,
     & R_amas,R_eras,R_uras,R_asas,R_irur,
     & R_aras,R_erur,REAL(R_axur,4),C20_ixur,I_ofas,I_ukas
     &,
     & R_otur,REAL(R_avur,4),R_atur,
     & REAL(R_itur,4),I_ibas,I_ebas,I_ufas,I_ikas,C20_exur
     &,
     & C20_oxur,C8_uxur,L_asur,R_urur,
     & REAL(R_esur,4),L_osur,R_isur,
     & REAL(R_usur,4),I_ekas,I_okas,I_akas,I_ifas,L_evur,
     & L_elas,L_epas,L_utur,L_etur,
     & L_ovur,L_ivur,L_olas,L_abas,L_apas,L_ubas,L_adas,
     & REAL(R8_orur,8),REAL(1.0,4),R8_obas,L_(47),L_opur,L_
     &(46),L_ipur,
     & L_ilas,I_alas,L_ipas,L_arur,L_upas,L_upur,L_edas,L_idas
     &,L_odas,L_afas,
     & L_efas,L_udas)
      !}
C FDB_mechanic_vlv.fgi(  88, 178):���������� ������,20FDB50AB004
      !{
      Call SHIB_HANDLER(deltat,R8_ades,R_ives,R_oves,R_ures
     &,R_ires,
     & L_otes,R_ases,R_ores,L_utes,R_ares,
     & R_eres,R_ites,R_aves,R_eves,R_otas,
     & R_etes,R_itas,REAL(R_edes,4),C20_odes,I_ules,I_apes
     &,
     & R_uxas,REAL(R_ebes,4),R_exas,
     & REAL(R_oxas,4),I_ofes,I_ifes,I_ames,I_omes,C20_ides
     &,
     & C20_udes,C8_afes,L_evas,R_avas,
     & REAL(R_ivas,4),L_uvas,R_ovas,
     & REAL(R_axas,4),I_imes,I_umes,I_emes,I_oles,L_ibes,
     & L_ipes,L_ises,L_abes,L_ixas,
     & L_ubes,L_obes,L_upes,L_efes,L_eses,L_akes,L_ekes,
     & REAL(R8_utas,8),REAL(1.0,4),R8_ufes,L_(49),L_usas,L_
     &(48),L_osas,
     & L_opes,I_epes,L_oses,L_etas,L_ates,L_atas,L_ikes,L_okes
     &,L_ukes,L_eles,
     & L_iles,L_ales)
      !}
C FDB_mechanic_vlv.fgi(  74, 178):���������� ������,20FDB50AB003
      !{
      Call SHIB_HANDLER(deltat,R8_ekis,R_obos,R_ubos,R_avis
     &,R_otis,
     & L_uxis,R_evis,R_utis,L_abos,R_etis,
     & R_itis,R_oxis,R_ebos,R_ibos,R_uxes,
     & R_ixis,R_oxes,REAL(R_ikis,4),C20_ukis,I_aris,I_esis
     &,
     & R_afis,REAL(R_ifis,4),R_idis,
     & REAL(R_udis,4),I_ulis,I_olis,I_eris,I_uris,C20_okis
     &,
     & C20_alis,C8_elis,L_ibis,R_ebis,
     & REAL(R_obis,4),L_adis,R_ubis,
     & REAL(R_edis,4),I_oris,I_asis,I_iris,I_upis,L_ofis,
     & L_osis,L_ovis,L_efis,L_odis,
     & L_akis,L_ufis,L_atis,L_ilis,L_ivis,L_emis,L_imis,
     & REAL(R8_abis,8),REAL(1.0,4),R8_amis,L_(51),L_axes,L_
     &(50),L_uves,
     & L_usis,I_isis,L_uvis,L_ixes,L_exis,L_exes,L_omis,L_umis
     &,L_apis,L_ipis,
     & L_opis,L_epis)
      !}
C FDB_mechanic_vlv.fgi(  60, 178):���������� ������,20FDB50AB002
      Call TELEGKA8_HANDLER(deltat,REAL(R_ubu,4),R_isu,R_ipu
     &,
     & L_afad,R_iru,REAL(R_imo,4),REAL(500.0,4),R_oru,
     & R_uru,R_asu,R_esu,L_ebad,
     & REAL(R_ubu,4),R_osu,R_opu,L_efad,R_usu,
     & R_upu,L_ifad,R_avu,L_okad,
     & R_itu,L_akad,L_adad,L_edad,
     & REAL(R_ito,4),R_otu,L_ekad,
     & R_utu,L_ikad,L_axu,
     & L_ixu,L_exu,L_ibu,L_oxu,
     & L_uxu,L_abad,L_ebu,R_ulu,
     & L_ovu,L_uvu,L_evu,
     & L_ivu,L_amu,L_imu,L_amo,L_ulo,
     & C20_oxo,L_eto,L_emu,L_ibad,L_omu,L_obu,L_ofu,
     & L_ufu,L_aku,L_eku,L_uku,L_adu,L_iku,L_oku,L_idu,L_edu
     &,L_odu,
     & L_afu,L_efu,L_ifu,R_ukad,R_alad,L_(5),R_apu,
     & R_epu,R_odad,R_udad,REAL(120.0,4),R_abu,R_emo,
     & R_aso,REAL(R_ato,4),R_omo,REAL(R_opo,4),L_aro,
     & R_upo,REAL(R_uro,4),L_alu,L_elu,L_umo,L_(8),L_(9),
     & L_eso,L_uxo,L_obad,L_ilu,L_(7),L_olu,L_(6),L_ubad,
     & L_idad,L_udu)
C FDB_mechanic_vlv.fgi( 130, 127):���������� ������� �� 8 ���������,20FDB50AE403
      Call TELEGKA8_HANDLER(deltat,REAL(R_ebed,4),R_ured,R_umed
     &,
     & L_idid,R_uped,REAL(R_ulad,4),REAL(3000.0,4),R_ared
     &,
     & R_ered,R_ired,R_ored,L_oxed,
     & REAL(R_ebed,4),R_ased,R_aped,L_odid,R_esed,
     & R_eped,L_udid,R_ited,L_akid,
     & R_used,L_ifid,L_ibid,L_obid,
     & REAL(R_usad,4),R_ated,L_ofid,
     & R_eted,L_ufid,L_ived,
     & L_uved,L_oved,L_uxad,L_axed,
     & L_exed,L_ixed,L_oxad,R_eled,
     & L_aved,L_eved,L_oted,
     & L_uted,L_iled,L_uled,L_ilad,L_elad,
     & C20_axad,L_osad,L_oled,L_uxed,L_amed,L_abed,L_afed
     &,
     & L_efed,L_ifed,L_ofed,L_eked,L_ibed,L_ufed,L_aked,L_ubed
     &,L_obed,L_aded,
     & L_ided,L_oded,L_uded,R_ekid,R_ikid,L_(10),R_imed,
     & R_omed,R_adid,R_edid,REAL(120.0,4),R_ixad,R_olad,
     & R_irad,REAL(R_isad,4),R_amad,REAL(R_apad,4),L_ipad
     &,
     & R_epad,REAL(R_erad,4),L_iked,L_oked,L_emad,L_(13),L_
     &(14),
     & L_orad,L_exad,L_abid,L_uked,L_(12),L_aled,L_(11),L_ebid
     &,
     & L_ubid,L_eded)
C FDB_mechanic_vlv.fgi( 102, 127):���������� ������� �� 8 ���������,20FDB50AE401
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_usem,4),REAL(R_iser
     &,4),R_obom,
     & R_ubom,R_elim,REAL(R_orem,4),R_ilim,R_olim,
     & R_ulim,R_amim,L_etim,REAL(R_usem,4),
     & R_arim,L_ibom,R_ipim,
     & L_uxim,L_evim,L_ivim,REAL(R_ilem,4),
     & R_opim,L_abom,R_upim,
     & L_ebom,REAL(R_emem,4),REAL(R_amem,4),L_asim,
     & L_isim,L_esim,L_esem,L_osim,
     & L_usim,L_atim,L_asem,R_ubim,
     & L_orim,L_urim,L_erim,
     & L_irim,L_efim,L_ekim,C20_omem,L_elem,
     & L_akim,L_otim,L_isem,L_ovem,L_uvem,L_axem,L_exem,L_uxem
     &,
     & L_atem,L_ixem,L_oxem,L_item,L_etem,L_otem,L_avem,L_evem
     &,L_ivem,L_(28),
     & L_(27),REAL(R_imem,4),R_ukim,R_alim,R_axim,R_exim,
     & I_idim,R_idem,R_edem,L_ibim,C20_umem,C20_apem,I_odim
     &,I_ofim,
     & R_okem,REAL(R_alem,4),R_udem,
     & REAL(R_efem,4),L_obem,L_ibem,I_erem,I_arem,I_edim,I_udim
     &,
     & C20_epem,C8_ipem,R_ifem,REAL(R_ufem,4),
     & L_ekem,R_akem,REAL(R_ikem,4),I_ifim,
     & L_(26),L_opem,L_ofem,I_afim,I_adim,L_ukem,
     & L_afem,L_abim,L_ebim,L_olem,L_ulem,L_okim,L_ovim,
     & L_upem,L_osem,REAL(R8_odem,8),REAL(1.0,4),R8_irem,L_obim
     &,L_ikim,
     & I_ufim,L_adem,L_ubem,L_avim,L_uvim,L_utem)
      !}
C FDB_mechanic_vlv.fgi( 130, 154):���������� ���������,20FDB50AE506
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_itom,4),REAL(R_iser
     &,4),R_edap,
     & R_idap,R_ulum,REAL(R_esom,4),R_amum,R_emum,
     & R_imum,R_omum,L_utum,REAL(R_itom,4),
     & R_orum,L_adap,R_arum,
     & L_ibap,L_uvum,L_axum,REAL(R_amom,4),
     & R_erum,L_obap,R_irum,
     & L_ubap,REAL(R_umom,4),REAL(R_omom,4),L_osum,
     & L_atum,L_usum,L_usom,L_etum,
     & L_itum,L_otum,L_osom,R_idum,
     & L_esum,L_isum,L_urum,
     & L_asum,L_ufum,L_ukum,C20_epom,L_ulom,
     & L_okum,L_evum,L_atom,L_exom,L_ixom,L_oxom,L_uxom,L_ibum
     &,
     & L_otom,L_abum,L_ebum,L_avom,L_utom,L_evom,L_ovom,L_uvom
     &,L_axom,L_(31),
     & L_(30),REAL(R_apom,4),R_ilum,R_olum,R_oxum,R_uxum,
     & I_afum,R_afom,R_udom,L_adum,C20_ipom,C20_opom,I_efum
     &,I_ekum,
     & R_elom,REAL(R_olom,4),R_ifom,
     & REAL(R_ufom,4),L_edom,L_adom,I_urom,I_orom,I_udum,I_ifum
     &,
     & C20_upom,C8_arom,R_akom,REAL(R_ikom,4),
     & L_ukom,R_okom,REAL(R_alom,4),I_akum,
     & L_(29),L_erom,L_ekom,I_ofum,I_odum,L_ilom,
     & L_ofom,L_obum,L_ubum,L_emom,L_imom,L_elum,L_exum,
     & L_irom,L_etom,REAL(R8_efom,8),REAL(1.0,4),R8_asom,L_edum
     &,L_alum,
     & I_ikum,L_odom,L_idom,L_ovum,L_ixum,L_ivom)
      !}
C FDB_mechanic_vlv.fgi( 116, 154):���������� ���������,20FDB50AE505
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_avap,4),REAL(R_iser
     &,4),R_udip,
     & R_afip,R_imep,REAL(R_usap,4),R_omep,R_umep,
     & R_apep,R_epep,L_ivep,REAL(R_avap,4),
     & R_esep,L_odip,R_orep,
     & L_adip,L_ixep,L_oxep,REAL(R_omap,4),
     & R_urep,L_edip,R_asep,
     & L_idip,REAL(R_ipap,4),REAL(R_epap,4),L_etep,
     & L_otep,L_itep,L_itap,L_utep,
     & L_avep,L_evep,L_etap,R_afep,
     & L_usep,L_atep,L_isep,
     & L_osep,L_ikep,L_ilep,C20_upap,L_imap,
     & L_elep,L_uvep,L_otap,L_uxap,L_abep,L_ebep,L_ibep,L_adep
     &,
     & L_evap,L_obep,L_ubep,L_ovap,L_ivap,L_uvap,L_exap,L_ixap
     &,L_oxap,L_(34),
     & L_(33),REAL(R_opap,4),R_amep,R_emep,R_ebip,R_ibip,
     & I_ofep,R_ofap,R_ifap,L_odep,C20_arap,C20_erap,I_ufep
     &,I_ukep,
     & R_ulap,REAL(R_emap,4),R_akap,
     & REAL(R_ikap,4),L_udap,L_odap,I_isap,I_esap,I_ifep,I_akep
     &,
     & C20_irap,C8_orap,R_okap,REAL(R_alap,4),
     & L_ilap,R_elap,REAL(R_olap,4),I_okep,
     & L_(32),L_urap,L_ukap,I_ekep,I_efep,L_amap,
     & L_ekap,L_edep,L_idep,L_umap,L_apap,L_ulep,L_uxep,
     & L_asap,L_utap,REAL(R8_ufap,8),REAL(1.0,4),R8_osap,L_udep
     &,L_olep,
     & I_alep,L_efap,L_afap,L_exep,L_abip,L_axap)
      !}
C FDB_mechanic_vlv.fgi( 102, 154):���������� ���������,20FDB50AE504
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_ovip,4),REAL(R_iser
     &,4),R_ifup,
     & R_ofup,R_apop,REAL(R_itip,4),R_epop,R_ipop,
     & R_opop,R_upop,L_axop,REAL(R_ovip,4),
     & R_usop,L_efup,R_esop,
     & L_odup,L_abup,L_ebup,REAL(R_epip,4),
     & R_isop,L_udup,R_osop,
     & L_afup,REAL(R_arip,4),REAL(R_upip,4),L_utop,
     & L_evop,L_avop,L_avip,L_ivop,
     & L_ovop,L_uvop,L_utip,R_ofop,
     & L_itop,L_otop,L_atop,
     & L_etop,L_alop,L_amop,C20_irip,L_apip,
     & L_ulop,L_ixop,L_evip,L_ibop,L_obop,L_ubop,L_adop,L_odop
     &,
     & L_uvip,L_edop,L_idop,L_exip,L_axip,L_ixip,L_uxip,L_abop
     &,L_ebop,L_(37),
     & L_(36),REAL(R_erip,4),R_omop,R_umop,R_ubup,R_adup,
     & I_ekop,R_ekip,R_akip,L_efop,C20_orip,C20_urip,I_ikop
     &,I_ilop,
     & R_imip,REAL(R_umip,4),R_okip,
     & REAL(R_alip,4),L_ifip,L_efip,I_atip,I_usip,I_akop,I_okop
     &,
     & C20_asip,C8_esip,R_elip,REAL(R_olip,4),
     & L_amip,R_ulip,REAL(R_emip,4),I_elop,
     & L_(35),L_isip,L_ilip,I_ukop,I_ufop,L_omip,
     & L_ukip,L_udop,L_afop,L_ipip,L_opip,L_imop,L_ibup,
     & L_osip,L_ivip,REAL(R8_ikip,8),REAL(1.0,4),R8_etip,L_ifop
     &,L_emop,
     & I_olop,L_ufip,L_ofip,L_uxop,L_obup,L_oxip)
      !}
C FDB_mechanic_vlv.fgi(  88, 154):���������� ���������,20FDB50AE503
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_exup,4),REAL(R_iser
     &,4),R_aker,
     & R_eker,R_opar,REAL(R_avup,4),R_upar,R_arar,
     & R_erar,R_irar,L_oxar,REAL(R_exup,4),
     & R_itar,L_ufer,R_usar,
     & L_efer,L_ober,L_uber,REAL(R_upup,4),
     & R_atar,L_ifer,R_etar,
     & L_ofer,REAL(R_orup,4),REAL(R_irup,4),L_ivar,
     & L_uvar,L_ovar,L_ovup,L_axar,
     & L_exar,L_ixar,L_ivup,R_ekar,
     & L_avar,L_evar,L_otar,
     & L_utar,L_olar,L_omar,C20_asup,L_opup,
     & L_imar,L_aber,L_uvup,L_adar,L_edar,L_idar,L_odar,L_efar
     &,
     & L_ixup,L_udar,L_afar,L_uxup,L_oxup,L_abar,L_ibar,L_obar
     &,L_ubar,L_(40),
     & L_(39),REAL(R_urup,4),R_epar,R_ipar,R_ider,R_oder,
     & I_ukar,R_ukup,R_okup,L_ufar,C20_esup,C20_isup,I_alar
     &,I_amar,
     & R_apup,REAL(R_ipup,4),R_elup,
     & REAL(R_olup,4),L_akup,L_ufup,I_otup,I_itup,I_okar,I_elar
     &,
     & C20_osup,C8_usup,R_ulup,REAL(R_emup,4),
     & L_omup,R_imup,REAL(R_umup,4),I_ular,
     & L_(38),L_atup,L_amup,I_ilar,I_ikar,L_epup,
     & L_ilup,L_ifar,L_ofar,L_arup,L_erup,L_apar,L_ader,
     & L_etup,L_axup,REAL(R8_alup,8),REAL(1.0,4),R8_utup,L_akar
     &,L_umar,
     & I_emar,L_ikup,L_ekup,L_iber,L_eder,L_ebar)
      !}
C FDB_mechanic_vlv.fgi(  74, 154):���������� ���������,20FDB50AE502
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_abir,4),REAL(R_iser
     &,4),R_ukor,
     & R_alor,R_irir,REAL(R_uver,4),R_orir,R_urir,
     & R_asir,R_esir,L_ibor,REAL(R_abir,4),
     & R_evir,L_okor,R_otir,
     & L_akor,L_idor,L_odor,REAL(R_irer,4),
     & R_utir,L_ekor,R_avir,
     & L_ikor,REAL(R_eser,4),REAL(R_aser,4),L_exir,
     & L_oxir,L_ixir,L_ixer,L_uxir,
     & L_abor,L_ebor,L_exer,R_alir,
     & L_uvir,L_axir,L_ivir,
     & L_ovir,L_imir,L_ipir,C20_user,L_erer,
     & L_epir,L_ubor,L_oxer,L_udir,L_afir,L_efir,L_ifir,L_akir
     &,
     & L_ebir,L_ofir,L_ufir,L_obir,L_ibir,L_ubir,L_edir,L_idir
     &,L_odir,L_(43),
     & L_(42),REAL(R_oser,4),R_arir,R_erir,R_efor,R_ifor,
     & I_olir,R_iler,R_eler,L_okir,C20_ater,C20_eter,I_ulir
     &,I_umir,
     & R_oper,REAL(R_arer,4),R_uler,
     & REAL(R_emer,4),L_oker,L_iker,I_iver,I_ever,I_ilir,I_amir
     &,
     & C20_iter,C8_oter,R_imer,REAL(R_umer,4),
     & L_eper,R_aper,REAL(R_iper,4),I_omir,
     & L_(41),L_uter,L_omer,I_emir,I_elir,L_uper,
     & L_amer,L_ekir,L_ikir,L_orer,L_urer,L_upir,L_udor,
     & L_aver,L_uxer,REAL(R8_oler,8),REAL(1.0,4),R8_over,L_ukir
     &,L_opir,
     & I_apir,L_aler,L_uker,L_edor,L_afor,L_adir)
      !}
C FDB_mechanic_vlv.fgi(  60, 154):���������� ���������,20FDB50AE501
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_esul,4),REAL(R_iser
     &,4),R_abem,
     & R_ebem,R_okam,REAL(R_arul,4),R_ukam,R_alam,
     & R_elam,R_ilam,L_osam,REAL(R_esul,4),
     & R_ipam,L_uxam,R_umam,
     & L_exam,L_otam,L_utam,REAL(R_ukul,4),
     & R_apam,L_ixam,R_epam,
     & L_oxam,REAL(R_olul,4),REAL(R_ilul,4),L_iram,
     & L_uram,L_oram,L_orul,L_asam,
     & L_esam,L_isam,L_irul,R_ebam,
     & L_aram,L_eram,L_opam,
     & L_upam,L_odam,L_ofam,C20_amul,L_okul,
     & L_ifam,L_atam,L_urul,L_avul,L_evul,L_ivul,L_ovul,L_exul
     &,
     & L_isul,L_uvul,L_axul,L_usul,L_osul,L_atul,L_itul,L_otul
     &,L_utul,L_(25),
     & L_(24),REAL(R_ulul,4),R_ekam,R_ikam,R_ivam,R_ovam,
     & I_ubam,R_ubul,R_obul,L_uxul,C20_emul,C20_imul,I_adam
     &,I_afam,
     & R_akul,REAL(R_ikul,4),R_edul,
     & REAL(R_odul,4),L_abul,L_uxol,I_opul,I_ipul,I_obam,I_edam
     &,
     & C20_omul,C8_umul,R_udul,REAL(R_eful,4),
     & L_oful,R_iful,REAL(R_uful,4),I_udam,
     & L_(23),L_apul,L_aful,I_idam,I_ibam,L_ekul,
     & L_idul,L_ixul,L_oxul,L_alul,L_elul,L_akam,L_avam,
     & L_epul,L_asul,REAL(R8_adul,8),REAL(1.0,4),R8_upul,L_abam
     &,L_ufam,
     & I_efam,L_ibul,L_ebul,L_itam,L_evam,L_etul)
      !}
C FDB_mechanic_vlv.fgi(  45, 154):���������� ���������,20FDB50AE500
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_oril,4),REAL(R_iser
     &,4),R_ixol,
     & R_oxol,R_akol,REAL(R_ipil,4),R_ekol,R_ikol,
     & R_okol,R_ukol,L_asol,REAL(R_oril,4),
     & R_umol,L_exol,R_emol,
     & L_ovol,L_atol,L_etol,REAL(R_ekil,4),
     & R_imol,L_uvol,R_omol,
     & L_axol,REAL(R_alil,4),REAL(R_ukil,4),L_upol,
     & L_erol,L_arol,L_aril,L_irol,
     & L_orol,L_urol,L_upil,R_oxil,
     & L_ipol,L_opol,L_apol,
     & L_epol,L_adol,L_afol,C20_ilil,L_akil,
     & L_udol,L_isol,L_eril,L_itil,L_otil,L_util,L_avil,L_ovil
     &,
     & L_uril,L_evil,L_ivil,L_esil,L_asil,L_isil,L_usil,L_atil
     &,L_etil,L_(22),
     & L_(21),REAL(R_elil,4),R_ofol,R_ufol,R_utol,R_avol,
     & I_ebol,R_ebil,R_abil,L_exil,C20_olil,C20_ulil,I_ibol
     &,I_idol,
     & R_ifil,REAL(R_ufil,4),R_obil,
     & REAL(R_adil,4),L_ixel,L_exel,I_apil,I_umil,I_abol,I_obol
     &,
     & C20_amil,C8_emil,R_edil,REAL(R_odil,4),
     & L_afil,R_udil,REAL(R_efil,4),I_edol,
     & L_(20),L_imil,L_idil,I_ubol,I_uxil,L_ofil,
     & L_ubil,L_uvil,L_axil,L_ikil,L_okil,L_ifol,L_itol,
     & L_omil,L_iril,REAL(R8_ibil,8),REAL(1.0,4),R8_epil,L_ixil
     &,L_efol,
     & I_odol,L_uxel,L_oxel,L_usol,L_otol,L_osil)
      !}
C FDB_mechanic_vlv.fgi(  30, 154):���������� ���������,20FDB50AW002
      !{
      Call TOLKATEL_HANDLER(deltat,REAL(R_aral,4),REAL(R_iser
     &,4),R_uvel,
     & R_axel,R_ifel,REAL(R_umal,4),R_ofel,R_ufel,
     & R_akel,R_ekel,L_irel,REAL(R_aral,4),
     & R_emel,L_ovel,R_olel,
     & L_avel,L_isel,L_osel,REAL(R_ofal,4),
     & R_ulel,L_evel,R_amel,
     & L_ivel,REAL(R_ikal,4),REAL(R_ekal,4),L_epel,
     & L_opel,L_ipel,L_ipal,L_upel,
     & L_arel,L_erel,L_epal,R_axal,
     & L_umel,L_apel,L_imel,
     & L_omel,L_ibel,L_idel,C20_ukal,L_ifal,
     & L_edel,L_urel,L_opal,L_usal,L_atal,L_etal,L_ital,L_aval
     &,
     & L_eral,L_otal,L_utal,L_oral,L_iral,L_ural,L_esal,L_isal
     &,L_osal,L_(19),
     & L_(18),REAL(R_okal,4),R_afel,R_efel,R_etel,R_itel,
     & I_oxal,R_oxuk,R_ixuk,L_oval,C20_alal,C20_elal,I_uxal
     &,I_ubel,
     & R_udal,REAL(R_efal,4),R_abal,
     & REAL(R_ibal,4),L_uvuk,L_ovuk,I_imal,I_emal,I_ixal,I_abel
     &,
     & C20_ilal,C8_olal,R_obal,REAL(R_adal,4),
     & L_idal,R_edal,REAL(R_odal,4),I_obel,
     & L_(17),L_ulal,L_ubal,I_ebel,I_exal,L_afal,
     & L_ebal,L_eval,L_ival,L_ufal,L_akal,L_udel,L_usel,
     & L_amal,L_upal,REAL(R8_uxuk,8),REAL(1.0,4),R8_omal,L_uval
     &,L_odel,
     & I_adel,L_exuk,L_axuk,L_esel,L_atel,L_asal)
      !}
C FDB_mechanic_vlv.fgi(  15, 154):���������� ���������,20FDB50AW001
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_okef,4),R8_obef,R_avaf
     &,R_utaf,C30_ifef,
     & R_uxaf,REAL(R_ebef,4),R_exaf,
     & REAL(R_oxaf,4),R_evaf,REAL(R_ovaf,4),R_ufef,
     & R_ofef,L_ibef,L_emef,L_epef,L_abef,
     & REAL(R_idef,4),L_adef,L_ubef,L_omef,L_ixaf,
     & L_ivaf,L_udef,L_upef,L_imef,L_ekef,L_ikef,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_ataf,L_itaf,L_etaf
     &,L_otaf,
     & L_ipef,R_amef,REAL(R_edef,4),L_opef,L_osaf,L_aref,L_usaf
     &,L_ukef,
     & L_alef,L_elef,L_olef,L_ulef,L_ilef)
      !}
C FDB_mechanic_vlv.fgi( 163,  45):���������� ������ ������������ �������,20FDB93AB001-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_efif,4),R8_exef,R_osef
     &,R_isef,C30_adif,
     & R_ivef,REAL(R_uvef,4),R_utef,
     & REAL(R_evef,4),R_usef,REAL(R_etef,4),R_idif,
     & R_edif,L_axef,L_ukif,L_ulif,L_ovef,
     & REAL(R_abif,4),L_oxef,L_ixef,L_elif,L_avef,
     & L_atef,L_ibif,L_imif,L_alif,L_udif,L_afif,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_oref,L_asef,L_uref
     &,L_esef,
     & L_amif,R_okif,REAL(R_uxef,4),L_emif,L_eref,L_omif,L_iref
     &,L_ifif,
     & L_ofif,L_ufif,L_ekif,L_ikif,L_akif)
      !}
C FDB_mechanic_vlv.fgi( 144,  45):���������� ������ ������������ �������,20FDB93AB010-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_akuf,4),R8_abuf,R_itof
     &,R_etof,C30_uduf,
     & R_exof,REAL(R_oxof,4),R_ovof,
     & REAL(R_axof,4),R_otof,REAL(R_avof,4),R_efuf,
     & R_afuf,L_uxof,L_oluf,L_omuf,L_ixof,
     & REAL(R_ubuf,4),L_ibuf,L_ebuf,L_amuf,L_uvof,
     & L_utof,L_eduf,L_epuf,L_uluf,L_ofuf,L_ufuf,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_isof,L_usof,L_osof
     &,L_atof,
     & L_umuf,R_iluf,REAL(R_obuf,4),L_apuf,L_asof,L_ipuf,L_esof
     &,L_ekuf,
     & L_ikuf,L_okuf,L_aluf,L_eluf,L_ukuf)
      !}
C FDB_mechanic_vlv.fgi( 124,  45):���������� ������ ������������ �������,20FDB22AB001-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_odak,4),R8_ovuf,R_asuf
     &,R_uruf,C30_ibak,
     & R_utuf,REAL(R_evuf,4),R_etuf,
     & REAL(R_otuf,4),R_esuf,REAL(R_osuf,4),R_ubak,
     & R_obak,L_ivuf,L_ekak,L_elak,L_avuf,
     & REAL(R_ixuf,4),L_axuf,L_uvuf,L_okak,L_ituf,
     & L_isuf,L_uxuf,L_ulak,L_ikak,L_edak,L_idak,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_aruf,L_iruf,L_eruf
     &,L_oruf,
     & L_ilak,R_akak,REAL(R_exuf,4),L_olak,L_opuf,L_amak,L_upuf
     &,L_udak,
     & L_afak,L_efak,L_ofak,L_ufak,L_ifak)
      !}
C FDB_mechanic_vlv.fgi( 105,  45):���������� ������ ������������ �������,20FDB91AB005-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_ebek,4),R8_etak,R_opak
     &,R_ipak,C30_axak,
     & R_isak,REAL(R_usak,4),R_urak,
     & REAL(R_esak,4),R_upak,REAL(R_erak,4),R_ixak,
     & R_exak,L_atak,L_udek,L_ufek,L_osak,
     & REAL(R_avak,4),L_otak,L_itak,L_efek,L_asak,
     & L_arak,L_ivak,L_ikek,L_afek,L_uxak,L_abek,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_omak,L_apak,L_umak
     &,L_epak,
     & L_akek,R_odek,REAL(R_utak,4),L_ekek,L_emak,L_okek,L_imak
     &,L_ibek,
     & L_obek,L_ubek,L_edek,L_idek,L_adek)
      !}
C FDB_mechanic_vlv.fgi(  86,  45):���������� ������ ������������ �������,20FDB91AB004-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_uvek,4),R8_urek,R_emek
     &,R_amek,C30_otek,
     & R_arek,REAL(R_irek,4),R_ipek,
     & REAL(R_upek,4),R_imek,REAL(R_umek,4),R_avek,
     & R_utek,L_orek,L_ibik,L_idik,L_erek,
     & REAL(R_osek,4),L_esek,L_asek,L_ubik,L_opek,
     & L_omek,L_atek,L_afik,L_obik,L_ivek,L_ovek,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_elek,L_olek,L_ilek
     &,L_ulek,
     & L_odik,R_ebik,REAL(R_isek,4),L_udik,L_ukek,L_efik,L_alek
     &,L_axek,
     & L_exek,L_ixek,L_uxek,L_abik,L_oxek)
      !}
C FDB_mechanic_vlv.fgi(  67,  45):���������� ������ ������������ �������,20FDB91AB003-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_itik,4),R8_ipik,R_ukik
     &,R_okik,C30_esik,
     & R_omik,REAL(R_apik,4),R_amik,
     & REAL(R_imik,4),R_alik,REAL(R_ilik,4),R_osik,
     & R_isik,L_epik,L_axik,L_abok,L_umik,
     & REAL(R_erik,4),L_upik,L_opik,L_ixik,L_emik,
     & L_elik,L_orik,L_obok,L_exik,L_atik,L_etik,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_ufik,L_ekik,L_akik
     &,L_ikik,
     & L_ebok,R_uvik,REAL(R_arik,4),L_ibok,L_ifik,L_ubok,L_ofik
     &,L_otik,
     & L_utik,L_avik,L_ivik,L_ovik,L_evik)
      !}
C FDB_mechanic_vlv.fgi(  48,  45):���������� ������ ������������ �������,20FDB91AB002-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_asok,4),R8_amok,R_ifok
     &,R_efok,C30_upok,
     & R_elok,REAL(R_olok,4),R_okok,
     & REAL(R_alok,4),R_ofok,REAL(R_akok,4),R_erok,
     & R_arok,L_ulok,L_otok,L_ovok,L_ilok,
     & REAL(R_umok,4),L_imok,L_emok,L_avok,L_ukok,
     & L_ufok,L_epok,L_exok,L_utok,L_orok,L_urok,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_idok,L_udok,L_odok
     &,L_afok,
     & L_uvok,R_itok,REAL(R_omok,4),L_axok,L_adok,L_ixok,L_edok
     &,L_esok,
     & L_isok,L_osok,L_atok,L_etok,L_usok)
      !}
C FDB_mechanic_vlv.fgi(  30,  45):���������� ������ ������������ �������,20FDB91AB001-M01
      !{
      Call SHIB_T_HANDLER(deltat,REAL(R_aruk,4),R8_ukuk,R_aduk
     &,R_ubuk,C30_omuk,
     & R_akuk,REAL(R_ikuk,4),R_ifuk,
     & REAL(R_ufuk,4),R_iduk,REAL(R_uduk,4),R_epuk,
     & R_apuk,L_okuk,L_osuk,L_otuk,L_ekuk,
     & REAL(R_oluk,4),L_eluk,L_aluk,L_atuk,L_ofuk,
     & L_oduk,L_amuk,L_evuk,L_usuk,L_opuk,L_upuk,
     & REAL(R8_eduk,8),REAL(1.0,4),R8_umuk,L_abuk,L_ibuk,L_ebuk
     &,L_obuk,
     & L_utuk,R_isuk,REAL(R_iluk,4),L_avuk,L_oxok,L_ivuk,L_uxok
     &,L_eruk,
     & L_iruk,L_oruk,L_asuk,L_esuk,L_uruk)
      !}
C FDB_mechanic_vlv.fgi(  12,  45):���������� ������ ������������ �������,20FDB91AB006-M01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omat,L_epat,R_apat,
     & REAL(R_ipat,4),L_opat,L_imat,I_umat)
      !}
C FDB_lamp.fgi( 326,  36):���������� ������� ���������,20FDB11AE702BQ63
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irat,L_asat,R_urat,
     & REAL(R_esat,4),L_isat,L_erat,I_orat)
      !}
C FDB_lamp.fgi( 252, 104):���������� ������� ���������,20FDB11AE702BQ62
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etat,L_utat,R_otat,
     & REAL(R_avat,4),L_evat,L_atat,I_itat)
      !}
C FDB_lamp.fgi( 326,  48):���������� ������� ���������,20FDB11AB001BQ06
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axat,L_oxat,R_ixat,
     & REAL(R_uxat,4),L_abet,L_uvat,I_exat)
      !}
C FDB_lamp.fgi( 252, 114):���������� ������� ���������,20FDB11AB001BQ05
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubet,L_idet,R_edet,
     & REAL(R_odet,4),L_udet,L_obet,I_adet)
      !}
C FDB_lamp.fgi( 288,  60):���������� ������� ���������,20FDB11AB001BQ04
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofet,L_eket,R_aket,
     & REAL(R_iket,4),L_oket,L_ifet,I_ufet)
      !}
C FDB_lamp.fgi( 288,  70):���������� ������� ���������,20FDB11AB001BQ03
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilet,L_amet,R_ulet,
     & REAL(R_emet,4),L_imet,L_elet,I_olet)
      !}
C FDB_lamp.fgi( 288,  82):���������� ������� ���������,20FDB11AB001BQ02
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epet,L_upet,R_opet,
     & REAL(R_aret,4),L_eret,L_apet,I_ipet)
      !}
C FDB_lamp.fgi( 288,  92):���������� ������� ���������,20FDB11AB001BQ01
      !{
      Call DAT_DISCR_HANDLER(deltat,I_aset,L_oset,R_iset,
     & REAL(R_uset,4),L_atet,L_uret,I_eset)
      !}
C FDB_lamp.fgi( 288, 104):���������� ������� ���������,20FDB11CG053
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utet,L_ivet,R_evet,
     & REAL(R_ovet,4),L_uvet,L_otet,I_avet)
      !}
C FDB_lamp.fgi( 288, 114):���������� ������� ���������,20FDB11CG052
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxet,L_ebit,R_abit,
     & REAL(R_ibit,4),L_obit,L_ixet,I_uxet)
      !}
C FDB_lamp.fgi( 306,  48):���������� ������� ���������,20FDB11CG051
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idit,L_afit,R_udit,
     & REAL(R_efit,4),L_ifit,L_edit,I_odit)
      !}
C FDB_lamp.fgi( 306,  60):���������� ������� ���������,20FDB11CG050
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekit,L_ukit,R_okit,
     & REAL(R_alit,4),L_elit,L_akit,I_ikit)
      !}
C FDB_lamp.fgi( 306,  70):���������� ������� ���������,20FDB11CG049
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amit,L_omit,R_imit,
     & REAL(R_umit,4),L_apit,L_ulit,I_emit)
      !}
C FDB_lamp.fgi( 306,  82):���������� ������� ���������,20FDB11AE703BQ61
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upit,L_irit,R_erit,
     & REAL(R_orit,4),L_urit,L_opit,I_arit)
      !}
C FDB_lamp.fgi( 306,  92):���������� ������� ���������,20FDB11AE703BQ60
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osit,L_etit,R_atit,
     & REAL(R_itit,4),L_otit,L_isit,I_usit)
      !}
C FDB_lamp.fgi( 306, 104):���������� ������� ���������,20FDB11AE703BQ59
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivit,L_axit,R_uvit,
     & REAL(R_exit,4),L_ixit,L_evit,I_ovit)
      !}
C FDB_lamp.fgi( 270, 104):���������� ������� ���������,20FDB11AE702BQ07
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebot,L_ubot,R_obot,
     & REAL(R_adot,4),L_edot,L_abot,I_ibot)
      !}
C FDB_lamp.fgi( 306, 114):���������� ������� ���������,20FDB11AE702BQ08
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afot,L_ofot,R_ifot,
     & REAL(R_ufot,4),L_akot,L_udot,I_efot)
      !}
C FDB_lamp.fgi( 326,  58):���������� ������� ���������,20FDB11AE702BQ10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukot,L_ilot,R_elot,
     & REAL(R_olot,4),L_ulot,L_okot,I_alot)
      !}
C FDB_lamp.fgi( 326,  70):���������� ������� ���������,20FDB11AE702BQ09
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omot,L_epot,R_apot,
     & REAL(R_ipot,4),L_opot,L_imot,I_umot)
      !}
C FDB_lamp.fgi( 326,  80):���������� ������� ���������,20FDB11AE702BQ12
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irot,L_asot,R_urot,
     & REAL(R_esot,4),L_isot,L_erot,I_orot)
      !}
C FDB_lamp.fgi( 326,  92):���������� ������� ���������,20FDB11AE702BQ11
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etot,L_utot,R_otot,
     & REAL(R_avot,4),L_evot,L_atot,I_itot)
      !}
C FDB_lamp.fgi( 326, 102):���������� ������� ���������,20FDB11AE702BQ20
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axot,L_oxot,R_ixot,
     & REAL(R_uxot,4),L_abut,L_uvot,I_exot)
      !}
C FDB_lamp.fgi( 326, 114):���������� ������� ���������,20FDB11AE702BQ19
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubut,L_idut,R_edut,
     & REAL(R_odut,4),L_udut,L_obut,I_adut)
      !}
C FDB_lamp.fgi( 326, 124):���������� ������� ���������,20FDB11AE702BQ22
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofut,L_ekut,R_akut,
     & REAL(R_ikut,4),L_okut,L_ifut,I_ufut)
      !}
C FDB_lamp.fgi( 326, 136):���������� ������� ���������,20FDB11AE702BQ21
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ilut,L_amut,R_ulut,
     & REAL(R_emut,4),L_imut,L_elut,I_olut)
      !}
C FDB_lamp.fgi( 326, 146):���������� ������� ���������,20FDB11AE702BQ18
      !{
      Call DAT_DISCR_HANDLER(deltat,I_eput,L_uput,R_oput,
     & REAL(R_arut,4),L_erut,L_aput,I_iput)
      !}
C FDB_lamp.fgi( 326, 158):���������� ������� ���������,20FDB11AE702BQ17
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asut,L_osut,R_isut,
     & REAL(R_usut,4),L_atut,L_urut,I_esut)
      !}
C FDB_lamp.fgi( 326, 168):���������� ������� ���������,20FDB11AE702BQ16
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utut,L_ivut,R_evut,
     & REAL(R_ovut,4),L_uvut,L_otut,I_avut)
      !}
C FDB_lamp.fgi( 326, 180):���������� ������� ���������,20FDB11AE702BQ15
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxut,L_ebav,R_abav,
     & REAL(R_ibav,4),L_obav,L_ixut,I_uxut)
      !}
C FDB_lamp.fgi( 252, 126):���������� ������� ���������,20FDB11AE702BQ14
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idav,L_afav,R_udav,
     & REAL(R_efav,4),L_ifav,L_edav,I_odav)
      !}
C FDB_lamp.fgi( 252, 136):���������� ������� ���������,20FDB11AE702BQ13
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekav,L_ukav,R_okav,
     & REAL(R_alav,4),L_elav,L_akav,I_ikav)
      !}
C FDB_lamp.fgi( 270, 114):���������� ������� ���������,20FDB11AL001BQ30
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amav,L_omav,R_imav,
     & REAL(R_umav,4),L_apav,L_ulav,I_emav)
      !}
C FDB_lamp.fgi( 270,  92):���������� ������� ���������,20FDB11AL001BQ29
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upav,L_irav,R_erav,
     & REAL(R_orav,4),L_urav,L_opav,I_arav)
      !}
C FDB_lamp.fgi( 252,  58):���������� ������� ���������,20FDB11AL001BQ28
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osav,L_etav,R_atav,
     & REAL(R_itav,4),L_otav,L_isav,I_usav)
      !}
C FDB_lamp.fgi( 252,  70):���������� ������� ���������,20FDB11AL001BQ27
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivav,L_axav,R_uvav,
     & REAL(R_exav,4),L_ixav,L_evav,I_ovav)
      !}
C FDB_lamp.fgi( 232,  70):���������� ������� ���������,20FDB11AL001BQ26
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebev,L_ubev,R_obev,
     & REAL(R_adev,4),L_edev,L_abev,I_ibev)
      !}
C FDB_lamp.fgi( 212,  92):���������� ������� ���������,20FDB11AL001BQ25
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afev,L_ofev,R_ifev,
     & REAL(R_ufev,4),L_akev,L_udev,I_efev)
      !}
C FDB_lamp.fgi( 232,  82):���������� ������� ���������,20FDB11AL001BQ24
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukev,L_ilev,R_elev,
     & REAL(R_olev,4),L_ulev,L_okev,I_alev)
      !}
C FDB_lamp.fgi( 252,  82):���������� ������� ���������,20FDB11AL001BQ23
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omev,L_epev,R_apev,
     & REAL(R_ipev,4),L_opev,L_imev,I_umev)
      !}
C FDB_lamp.fgi( 252,  92):���������� ������� ���������,20FDB11AL001BQ42
      !{
      Call DAT_DISCR_HANDLER(deltat,I_irev,L_asev,R_urev,
     & REAL(R_esev,4),L_isev,L_erev,I_orev)
      !}
C FDB_lamp.fgi( 232,  92):���������� ������� ���������,20FDB11AL001BQ41
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etev,L_utev,R_otev,
     & REAL(R_avev,4),L_evev,L_atev,I_itev)
      !}
C FDB_lamp.fgi( 232, 104):���������� ������� ���������,20FDB11AL002BQ40
      !{
      Call DAT_DISCR_HANDLER(deltat,I_axev,L_oxev,R_ixev,
     & REAL(R_uxev,4),L_abiv,L_uvev,I_exev)
      !}
C FDB_lamp.fgi( 232, 114):���������� ������� ���������,20FDB11AL002BQ39
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ubiv,L_idiv,R_ediv,
     & REAL(R_odiv,4),L_udiv,L_obiv,I_adiv)
      !}
C FDB_lamp.fgi( 232, 126):���������� ������� ���������,20FDB11AL002BQ38
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ofiv,L_ekiv,R_akiv,
     & REAL(R_ikiv,4),L_okiv,L_ifiv,I_ufiv)
      !}
C FDB_lamp.fgi( 232, 136):���������� ������� ���������,20FDB11AL002BQ37
      !{
      Call DAT_DISCR_HANDLER(deltat,I_iliv,L_amiv,R_uliv,
     & REAL(R_emiv,4),L_imiv,L_eliv,I_oliv)
      !}
C FDB_lamp.fgi( 212, 148):���������� ������� ���������,20FDB11AL002BQ36
      !{
      Call DAT_DISCR_HANDLER(deltat,I_epiv,L_upiv,R_opiv,
     & REAL(R_ariv,4),L_eriv,L_apiv,I_ipiv)
      !}
C FDB_lamp.fgi( 212,  36):���������� ������� ���������,20FDB11AL002BQ35
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asiv,L_osiv,R_isiv,
     & REAL(R_usiv,4),L_ativ,L_uriv,I_esiv)
      !}
C FDB_lamp.fgi( 212, 102):���������� ������� ���������,20FDB11AL002BQ34
      !{
      Call DAT_DISCR_HANDLER(deltat,I_utiv,L_iviv,R_eviv,
     & REAL(R_oviv,4),L_uviv,L_otiv,I_aviv)
      !}
C FDB_lamp.fgi( 212, 114):���������� ������� ���������,20FDB11AL002BQ33
      !{
      Call DAT_DISCR_HANDLER(deltat,I_oxiv,L_ebov,R_abov,
     & REAL(R_ibov,4),L_obov,L_ixiv,I_uxiv)
      !}
C FDB_lamp.fgi( 212, 124):���������� ������� ���������,20FDB11AL002BQ32
      !{
      Call DAT_DISCR_HANDLER(deltat,I_idov,L_afov,R_udov,
     & REAL(R_efov,4),L_ifov,L_edov,I_odov)
      !}
C FDB_lamp.fgi( 212, 158):���������� ������� ���������,20FDB11AL002BQ31
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekov,L_ukov,R_okov,
     & REAL(R_alov,4),L_elov,L_akov,I_ikov)
      !}
C FDB_lamp.fgi( 212, 170):���������� ������� ���������,20FDB11AB002BQ48
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amov,L_omov,R_imov,
     & REAL(R_umov,4),L_apov,L_ulov,I_emov)
      !}
C FDB_lamp.fgi( 212, 180):���������� ������� ���������,20FDB11AB002BQ47
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upov,L_irov,R_erov,
     & REAL(R_orov,4),L_urov,L_opov,I_arov)
      !}
C FDB_lamp.fgi( 212,  46):���������� ������� ���������,20FDB11AB002BQ46
      !{
      Call DAT_DISCR_HANDLER(deltat,I_osov,L_etov,R_atov,
     & REAL(R_itov,4),L_otov,L_isov,I_usov)
      !}
C FDB_lamp.fgi( 212,  58):���������� ������� ���������,20FDB11AB002BQ45
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ivov,L_axov,R_uvov,
     & REAL(R_exov,4),L_ixov,L_evov,I_ovov)
      !}
C FDB_lamp.fgi( 212,  68):���������� ������� ���������,20FDB11AB002BQ44
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ebuv,L_ubuv,R_obuv,
     & REAL(R_aduv,4),L_eduv,L_abuv,I_ibuv)
      !}
C FDB_lamp.fgi( 212,  80):���������� ������� ���������,20FDB11AB002BQ43
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afuv,L_ofuv,R_ifuv,
     & REAL(R_ufuv,4),L_akuv,L_uduv,I_efuv)
      !}
C FDB_lamp.fgi( 212, 136):���������� ������� ���������,20FDB42AE501QB05
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ukuv,L_iluv,R_eluv,
     & REAL(R_oluv,4),L_uluv,L_okuv,I_aluv)
      !}
C FDB_lamp.fgi( 270,  46):���������� ������� ���������,20FDB42AE401QH10
      !{
      Call DAT_DISCR_HANDLER(deltat,I_omuv,L_epuv,R_apuv,
     & REAL(R_ipuv,4),L_opuv,L_imuv,I_umuv)
      !}
C FDB_lamp.fgi( 270,  58):���������� ������� ���������,20FDB42AE401QH09
      !{
      Call DAT_DISCR_HANDLER(deltat,I_iruv,L_asuv,R_uruv,
     & REAL(R_esuv,4),L_isuv,L_eruv,I_oruv)
      !}
C FDB_lamp.fgi( 270,  70):���������� ������� ���������,20FDB42QB002
      !{
      Call DAT_DISCR_HANDLER(deltat,I_etuv,L_utuv,R_otuv,
     & REAL(R_avuv,4),L_evuv,L_atuv,I_ituv)
      !}
C FDB_lamp.fgi( 270,  82):���������� ������� ���������,20FDB42QB001
      !{
      Call DAT_DISCR_EMERG_HANDLER(deltat,L_ebax,R_abax,
     & REAL(R_ibax,4),R_ixuv,REAL(R_uxuv,4),L_odax,
     & L_oxuv,L_udax,L_obax,I_uvuv)
      !}
C FDB_lamp.fgi( 290,  31):���������� ������� ����. �������� ������������,20FDB42AE501QB04
      !{
      Call DAT_DISCR_EMERG_HANDLER(deltat,L_alax,R_ukax,
     & REAL(R_elax,4),R_ekax,REAL(R_okax,4),L_imax,
     & L_ikax,L_omax,L_ilax,I_ofax)
      !}
C FDB_lamp.fgi( 290,  47):���������� ������� ����. �������� ������������,20FDB42AE501QB03
      L_(58) =.NOT.(L_uxax.AND.L_oxax)
C FDB_lamp.fgi( 151, 154):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_isax,L_atax,R_usax,
     & REAL(R_etax,4),L_(58),L_esax,I_osax)
      !}
C FDB_lamp.fgi( 166, 152):���������� ������� ���������,20FDB42AE401QH07
      L_orax=(L_oxax.or.L_orax).and..not.(.NOT.L_oxax)
      L_(56)=.not.L_orax
C FDB_lamp.fgi( 152, 165):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_opax,L_erax,R_arax,
     & REAL(R_irax,4),L_orax,L_ipax,I_upax)
      !}
C FDB_lamp.fgi( 166, 166):���������� ������� ���������,20FDB42AE401QH08
      L_axax=(L_uxax.or.L_axax).and..not.(.NOT.L_uxax)
      L_(57)=.not.L_axax
C FDB_lamp.fgi( 152, 178):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_avax,L_ovax,R_ivax,
     & REAL(R_uvax,4),L_axax,L_utax,I_evax)
      !}
C FDB_lamp.fgi( 166, 178):���������� ������� ���������,20FDB42AE401QH06
      if(.not.L_obex) then
         R0_ebex=0.0
      elseif(.not.L0_ibex) then
         R0_ebex=R0_abex
      else
         R0_ebex=max(R_(1)-deltat,0.0)
      endif
      L_(59)=L_obex.and.R0_ebex.le.0.0
      L0_ibex=L_obex
C FDB_lamp.fgi(  69, 181):�������� ��������� ������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_adex,L_odex,R_idex,
     & REAL(R_udex,4),L_(59),L_ubex,I_edex)
      !}
C FDB_lamp.fgi(  82, 180):���������� ������� ���������,20FDB74AE403XU15
      if(.not.L_asex) then
         R0_ofex=0.0
      elseif(.not.L0_ufex) then
         R0_ofex=R0_ifex
      else
         R0_ofex=max(R_(2)-deltat,0.0)
      endif
      L_(60)=L_asex.and.R0_ofex.le.0.0
      L0_ufex=L_asex
C FDB_lamp.fgi(  63,  97):�������� ��������� ������
      L_(62) = L_(60).AND.(.NOT.L_esex).AND.(.NOT.L_isex).AND.
     &(.NOT.L_evex)
C FDB_lamp.fgi(  72, 100):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amex,L_omex,R_imex,
     & REAL(R_umex,4),L_(62),L_ulex,I_emex)
      !}
C FDB_lamp.fgi(  82,  98):���������� ������� ���������,20FDB71AE801QH12
      if(.not.L_esex) then
         R0_ekex=0.0
      elseif(.not.L0_ikex) then
         R0_ekex=R0_akex
      else
         R0_ekex=max(R_(3)-deltat,0.0)
      endif
      L_(61)=L_esex.and.R0_ekex.le.0.0
      L0_ikex=L_esex
C FDB_lamp.fgi(  63, 108):�������� ��������� ������
      L_(63) = L_(61).AND.(.NOT.L_asex).AND.(.NOT.L_isex).AND.
     &(.NOT.L_evex)
C FDB_lamp.fgi(  72, 111):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_opex,L_erex,R_arex,
     & REAL(R_irex,4),L_(63),L_ipex,I_upex)
      !}
C FDB_lamp.fgi(  82, 110):���������� ������� ���������,20FDB71AE801QH11
      if(.not.L_isex) then
         R0_ukex=0.0
      elseif(.not.L0_alex) then
         R0_ukex=R0_okex
      else
         R0_ukex=max(R_(4)-deltat,0.0)
      endif
      L_(64)=L_isex.and.R0_ukex.le.0.0
      L0_alex=L_isex
C FDB_lamp.fgi(  63, 119):�������� ��������� ������
      L_(66) = L_(64).AND.(.NOT.L_asex).AND.(.NOT.L_esex).AND.
     &(.NOT.L_evex)
C FDB_lamp.fgi(  72, 122):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_usex,L_itex,R_etex,
     & REAL(R_otex,4),L_(66),L_osex,I_atex)
      !}
C FDB_lamp.fgi(  82, 120):���������� ������� ���������,20FDB71AE202XU04
      if(.not.L_evex) then
         R0_ilex=0.0
      elseif(.not.L0_olex) then
         R0_ilex=R0_elex
      else
         R0_ilex=max(R_(5)-deltat,0.0)
      endif
      L_(65)=L_evex.and.R0_ilex.le.0.0
      L0_olex=L_evex
C FDB_lamp.fgi(  63, 130):�������� ��������� ������
      L_(67) = L_(65).AND.(.NOT.L_asex).AND.(.NOT.L_esex).AND.
     &(.NOT.L_isex)
C FDB_lamp.fgi(  72, 133):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ovex,L_exex,R_axex,
     & REAL(R_ixex,4),L_(67),L_ivex,I_uvex)
      !}
C FDB_lamp.fgi(  82, 132):���������� ������� ���������,20FDB71AE202XU01
      if(.not.L_upix) then
         R0_ebix=0.0
      elseif(.not.L0_ibix) then
         R0_ebix=R0_abix
      else
         R0_ebix=max(R_(6)-deltat,0.0)
      endif
      L_(68)=L_upix.and.R0_ebix.le.0.0
      L0_ibix=L_upix
C FDB_lamp.fgi(  63, 169):�������� ��������� ������
      L_(73) = L_(68).AND.(.NOT.L_opix).AND.(.NOT.L_upix)
C FDB_lamp.fgi(  70, 167):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_emix,L_umix,R_omix,
     & REAL(R_apix,4),L_(73),L_amix,I_imix)
      !}
C FDB_lamp.fgi(  82, 166):���������� ������� ���������,20FDB71AE202GH02
      if(.not.L_opix) then
         R0_ubix=0.0
      elseif(.not.L0_adix) then
         R0_ubix=R0_obix
      else
         R0_ubix=max(R_(7)-deltat,0.0)
      endif
      L_(70)=L_opix.and.R0_ubix.le.0.0
      L0_adix=L_opix
C FDB_lamp.fgi(  63, 158):�������� ��������� ������
      L_(72) = L_(70).AND.(.NOT.L_upix).AND.(.NOT.L_upix)
C FDB_lamp.fgi(  70, 156):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_okix,L_elix,R_alix,
     & REAL(R_ilix,4),L_(72),L_ikix,I_ukix)
      !}
C FDB_lamp.fgi(  82, 154):���������� ������� ���������,20FDB71AE202GH01
      if(.not.L_upix) then
         R0_idix=0.0
      elseif(.not.L0_odix) then
         R0_idix=R0_edix
      else
         R0_idix=max(R_(8)-deltat,0.0)
      endif
      L_(69)=L_upix.and.R0_idix.le.0.0
      L0_odix=L_upix
C FDB_lamp.fgi(  63, 147):�������� ��������� ������
      L_(71) = L_(69).AND.(.NOT.L_opix).AND.(.NOT.L_upix)
C FDB_lamp.fgi(  70, 145):�
      !{
      Call DAT_DISCR_HANDLER(deltat,I_afix,L_ofix,R_ifix,
     & REAL(R_ufix,4),L_(71),L_udix,I_efix)
      !}
C FDB_lamp.fgi(  82, 144):���������� ������� ���������,20FDB71AE801XU02
      End
