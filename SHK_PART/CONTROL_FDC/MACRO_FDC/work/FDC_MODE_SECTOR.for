      Subroutine FDC_MODE_SECTOR(ext_deltat,I_i,I_u,I_ad,I_ed
     &,I_id,I_od,I_ud,I_ef,I_if,I1_ak,I_ik,I1_ok,I1_uk,I_il
     &,I_ol,I1_im,I_ap,I_ar,I1_er)
C |I_i           |2 4 I|sel_mode_0_sector|������� ������ ��������� "�������� ��������� ���� ������\n������ ������ �������"|0|
C |I_u           |2 4 O|bgl_sector_mode1|���� ������ ����� ����������� ������ "��������������"||
C |I_ad          |2 4 O|bgu_sector_mode1|���� ������� ����� ����������� ������ "��������������"||
C |I_ed          |2 4 O|bgl_sector_mode0|���� ������ ����� ����������� ������ "������������������"||
C |I_id          |2 4 O|bgu_sector_mode0|���� ������� ����� ����������� ������ "������������������"||
C |I_od          |2 4 O|bgl_sector_mode2|���� ������ ����� ����������� ������ "���. ������."||
C |I_ud          |2 4 O|bgu_sector_mode2|���� ������� ����� ����������� ������ "���. ������."||
C |I_ef          |2 4 O|bgl_sector_mode3|���� ������ ����� ����������� ������ "������"||
C |I_if          |2 4 O|bgu_sector_mode3|���� ������� ����� ����������� ������ "������"||
C |I1_ak         |2 1 O|sector_mode1    |���� ���������� ������ "��������������"|0|
C |I_ik          |2 4 K|_lcmp2          |�������� ������ �����������|1|
C |I1_ok         |2 1 O|sector_mode0    |���� ���������� ������ "������������������"|0|
C |I1_uk         |2 1 O|sector_mode2    |���� ���������� ������ "���. ������."|0|
C |I_il          |2 4 K|_lcmp1          |�������� ������ �����������|0|
C |I_ol          |2 4 K|_lcmp3          |�������� ������ �����������|2|
C |I1_im         |2 1 O|sector_mode3    |���� ���������� ������ "������"|0|
C |I_ap          |2 4 K|_lcmp4          |�������� ������ �����������|3|
C |I_ar          |2 4 O|tbg_mode_0_sector|���� ���� ������ ���������� ������ ������ �������||
C |I1_er         |2 1 O|b_mode_0_sector |��������� ����� ������|1|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e
      INTEGER*4 I_i
      LOGICAL*1 L0_o
      INTEGER*4 I_u,I_ad,I_ed,I_id,I_od,I_ud,I0_af,I_ef,I_if
     &,I0_of,I0_uf
      INTEGER*1 I1_ak
      LOGICAL*1 L0_ek
      INTEGER*4 I_ik
      INTEGER*1 I1_ok,I1_uk
      INTEGER*4 I0_al
      LOGICAL*1 L0_el
      INTEGER*4 I_il,I_ol
      LOGICAL*1 L0_ul
      INTEGER*4 I0_am,I0_em
      INTEGER*1 I1_im
      INTEGER*4 I0_om,I0_um,I_ap
      LOGICAL*1 L0_ep
      INTEGER*4 I0_ip,I0_op,I0_up,I_ar
      INTEGER*1 I1_er

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_o=.false.
C FDC_MODE_SECTOR.fmg(  24, 162):��������� ���������� (�������)
      if(I_i.ne.0) then
         I0_om=I_i
         L0_e=I_i.ne.0
         I_i=0
      endif
C FDC_MODE_SECTOR.fmg(  33, 164):���������� ��������� ����������������
      I0_af = z'01000020'
C FDC_MODE_SECTOR.fmg(  43, 279):��������� ������������� IN (�������)
      I0_of = z'01000005'
C FDC_MODE_SECTOR.fmg(  43, 285):��������� ������������� IN (�������)
      I0_uf = z'01000023'
C FDC_MODE_SECTOR.fmg(  43, 287):��������� ������������� IN (�������)
      I0_al = z'01000005'
C FDC_MODE_SECTOR.fmg( 164, 268):��������� ������������� IN (�������)
      I0_em = z'0100002F'
C FDC_MODE_SECTOR.fmg( 138, 269):��������� ������������� IN (�������)
      I0_op = z'01000033'
C FDC_MODE_SECTOR.fmg(  69, 270):��������� ������������� IN (�������)
      I0_up = z'01000009'
C FDC_MODE_SECTOR.fmg(  69, 272):��������� ������������� IN (�������)
      I0_um = 1
C FDC_MODE_SECTOR.fmg(  46, 171):��������� ������������� IN (�������)
      I1_er = (-I0_um) + I0_om
C FDC_MODE_SECTOR.fmg(  50, 170):��������
      L0_ek=I1_er.eq.I_ik
C FDC_MODE_SECTOR.fmg(  69, 180):���������� �������������,2
      if(L0_ek) then
         I1_ak=1
      else
         I1_ak=0
      endif
C FDC_MODE_SECTOR.fmg(  79, 180):��������� LO->1
      if(L0_ek) then
         I_ad=I0_of
      else
         I_ad=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  73, 196):���� RE IN LO CH7
      if(L0_ek) then
         I_u=I0_af
      else
         I_u=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  82, 190):���� RE IN LO CH7
      L0_el=I1_er.eq.I_il
C FDC_MODE_SECTOR.fmg(  69, 215):���������� �������������,1
      if(L0_el) then
         I_id=I0_of
      else
         I_id=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  73, 227):���� RE IN LO CH7
      if(L0_el) then
         I1_ok=1
      else
         I1_ok=0
      endif
C FDC_MODE_SECTOR.fmg(  79, 211):��������� LO->1
      if(L0_el) then
         I_ed=I0_af
      else
         I_ed=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  82, 221):���� RE IN LO CH7
      L0_ul=I1_er.eq.I_ol
C FDC_MODE_SECTOR.fmg(  69, 239):���������� �������������,3
      if(L0_ul) then
         I_ud=I0_of
      else
         I_ud=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  73, 251):���� RE IN LO CH7
      if(L0_ul) then
         I1_uk=1
      else
         I1_uk=0
      endif
C FDC_MODE_SECTOR.fmg(  79, 235):��������� LO->1
      if(L0_ul) then
         I_od=I0_af
      else
         I_od=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  82, 245):���� RE IN LO CH7
      L0_ep=I1_er.eq.I_ap
C FDC_MODE_SECTOR.fmg(  69, 261):���������� �������������,4
      if(L0_ep) then
         I0_ip=I0_op
      else
         I0_ip=I0_up
      endif
C FDC_MODE_SECTOR.fmg(  73, 270):���� RE IN LO CH7
      if(L0_ul) then
         I0_am=I0_em
      else
         I0_am=I0_ip
      endif
C FDC_MODE_SECTOR.fmg( 141, 269):���� RE IN LO CH7
      if(L0_el) then
         I_ar=I0_al
      else
         I_ar=I0_am
      endif
C FDC_MODE_SECTOR.fmg( 167, 268):���� RE IN LO CH7
      if(L0_ep) then
         I1_im=1
      else
         I1_im=0
      endif
C FDC_MODE_SECTOR.fmg(  79, 261):��������� LO->1
      if(L0_ep) then
         I_if=I0_of
      else
         I_if=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  83, 285):���� RE IN LO CH7
      if(L0_ep) then
         I_ef=I0_af
      else
         I_ef=I0_uf
      endif
C FDC_MODE_SECTOR.fmg(  92, 279):���� RE IN LO CH7
      End
