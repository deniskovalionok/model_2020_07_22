      Interface
      Subroutine FDC_MODE_SECTOR(ext_deltat,I_i,I_u,I_ad,I_ed
     &,I_id,I_od,I_ud,I_ef,I_if,I1_ak,I_ik,I1_ok,I1_uk,I_il
     &,I_ol,I1_im,I_ap,I_ar,I1_er)
C |I_i           |2 4 I|sel_mode_0_sector|������� ������ ��������� "�������� ��������� ���� ������\n������ ������ �������"|0|
C |I_u           |2 4 O|bgl_sector_mode1|���� ������ ����� ����������� ������ "��������������"||
C |I_ad          |2 4 O|bgu_sector_mode1|���� ������� ����� ����������� ������ "��������������"||
C |I_ed          |2 4 O|bgl_sector_mode0|���� ������ ����� ����������� ������ "������������������"||
C |I_id          |2 4 O|bgu_sector_mode0|���� ������� ����� ����������� ������ "������������������"||
C |I_od          |2 4 O|bgl_sector_mode2|���� ������ ����� ����������� ������ "���. ������."||
C |I_ud          |2 4 O|bgu_sector_mode2|���� ������� ����� ����������� ������ "���. ������."||
C |I_ef          |2 4 O|bgl_sector_mode3|���� ������ ����� ����������� ������ "������"||
C |I_if          |2 4 O|bgu_sector_mode3|���� ������� ����� ����������� ������ "������"||
C |I1_ak         |2 1 O|sector_mode1    |���� ���������� ������ "��������������"|0|
C |I_ik          |2 4 K|_lcmp2          |�������� ������ �����������|1|
C |I1_ok         |2 1 O|sector_mode0    |���� ���������� ������ "������������������"|0|
C |I1_uk         |2 1 O|sector_mode2    |���� ���������� ������ "���. ������."|0|
C |I_il          |2 4 K|_lcmp1          |�������� ������ �����������|0|
C |I_ol          |2 4 K|_lcmp3          |�������� ������ �����������|2|
C |I1_im         |2 1 O|sector_mode3    |���� ���������� ������ "������"|0|
C |I_ap          |2 4 K|_lcmp4          |�������� ������ �����������|3|
C |I_ar          |2 4 O|tbg_mode_0_sector|���� ���� ������ ���������� ������ ������ �������||
C |I1_er         |2 1 O|b_mode_0_sector |��������� ����� ������|1|

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_i,I_u,I_ad,I_ed,I_id,I_od,I_ud,I_ef,I_if
      INTEGER*1 I1_ak
      INTEGER*4 I_ik
      INTEGER*1 I1_ok,I1_uk
      INTEGER*4 I_il,I_ol
      INTEGER*1 I1_im
      INTEGER*4 I_ap,I_ar
      INTEGER*1 I1_er
      End subroutine FDC_MODE_SECTOR
      End interface
