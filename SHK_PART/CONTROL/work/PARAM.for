      Subroutine PARAM(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'PARAM.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_i = R8_e
C sens_init_2.fgi( 250, 292):��������
      R_u = R8_o
C sens_init_2.fgi( 250, 295):��������
      R_ed = R8_ad
C sens_init_2.fgi( 250, 280):��������
      R_od = R8_id
C sens_init_2.fgi( 250, 283):��������
      R_af = R8_ud
C sens_init_2.fgi( 250, 286):��������
      R_if = R8_ef
C sens_init_2.fgi( 250, 289):��������
      R_of = R8_uf
C sens_init_1.fgi( 380, 152):��������
      R_ak = R8_ek
C sens_init_1.fgi( 380, 156):��������
      R_ik = R8_ok
C sens_init_1.fgi( 380, 160):��������
      R_uk = R8_al
C sens_init_1.fgi( 380, 164):��������
      R_el = R8_il
C sens_init_1.fgi( 380, 168):��������
      R_ol = R8_ul
C sens_init_1.fgi( 380, 172):��������
      R_am=R_em
C sens_init_1.fgi( 312, 260):������,20KPA02CG003XQ01_input
      R_im = R8_om
C sens_init_1.fgi( 299, 191):��������
      R_um = R8_ap
C sens_init_1.fgi( 299, 196):��������
      R_ep = R8_ip
C sens_init_1.fgi( 299, 200):��������
      R_op = R8_up
C sens_init_1.fgi( 299, 205):��������
      R_ure = R8_ase
C sens_init_1.fgi( 380, 188):��������
      R_ate = R8_ete
C sens_init_1.fgi( 380, 200):��������
      R_ute = R8_ave
C sens_init_1.fgi( 380, 210):��������
      R_ese = R8_ise
C sens_init_1.fgi( 380, 192):��������
      R_ite = R8_ote
C sens_init_1.fgi( 380, 205):��������
      R_ofi = R8_ufi
C sens_init_1.fgi(  46,  62):��������
      R_ar = R8_er
C sens_init_1.fgi( 299, 211):��������
      R_ir = R8_or
C sens_init_1.fgi( 299, 216):��������
      R_ur = R8_as
C sens_init_1.fgi( 299, 220):��������
      R_(1) = 1
C sens_init_1.fgi( 134,   6):��������� (RE4) (�������)
      R_es = R8_is * R_(1)
C sens_init_1.fgi( 137,   8):����������
      R_(2) = 1
C sens_init_1.fgi( 134,  12):��������� (RE4) (�������)
      R_os = R8_us * R_(2)
C sens_init_1.fgi( 137,  14):����������
      R_(3) = 1
C sens_init_1.fgi(  46,  86):��������� (RE4) (�������)
      R_at = R8_et * R_(3)
C sens_init_1.fgi(  49,  88):����������
      R_it = R8_ot
C sens_init_1.fgi(  46,  92):��������
      R_(17)=abs(R8_emi)
C sens_init_1.fgi(  46, 105):���������� ��������
      R_ami = R_(17)
C sens_init_1.fgi(  53, 105):��������
      R_(18)=abs(R8_upi)
C sens_init_1.fgi(  46, 123):���������� ��������
      R_opi = R_(18)
C sens_init_1.fgi(  53, 123):��������
      R_(4) = 1
C sens_init_1.fgi( 134,  19):��������� (RE4) (�������)
      R_ut = R8_av * R_(4)
C sens_init_1.fgi( 137,  21):����������
      R_(5) = 1
C sens_init_1.fgi( 134,  31):��������� (RE4) (�������)
      R_ev = R8_iv * R_(5)
C sens_init_1.fgi( 137,  33):����������
      R_(6) = 1
C sens_init_1.fgi( 134,  37):��������� (RE4) (�������)
      R_ov = R8_uv * R_(6)
C sens_init_1.fgi( 137,  39):����������
      R_(7) = 1
C sens_init_1.fgi( 134,  25):��������� (RE4) (�������)
      R_ax = R8_ex * R_(7)
C sens_init_1.fgi( 137,  27):����������
      R_idi = R8_odi
C sens_init_1.fgi( 299, 168):��������
      R_(8) = 0.001
C sens_init_1.fgi( 377, 178):��������� (RE4) (�������)
      R_are = R8_ere * R_(8)
C sens_init_1.fgi( 380, 180):����������
      R_(9) = 0.001
C sens_init_1.fgi( 296, 128):��������� (RE4) (�������)
      R_axe = R8_exe * R_(9)
C sens_init_1.fgi( 299, 130):����������
      R_(10) = 0.001
C sens_init_1.fgi( 296, 133):��������� (RE4) (�������)
      R_ixe = R8_oxe * R_(10)
C sens_init_1.fgi( 299, 135):����������
      R_(11) = 1
C sens_init_1.fgi(  46,  95):��������� (RE4) (�������)
      R_eli = R8_ili * R_(11)
C sens_init_1.fgi(  49,  97):����������
      R_(12) = 0.000001
C sens_init_1.fgi(  46, 108):��������� (RE4) (�������)
      R_imi = R8_omi * R_(12)
C sens_init_1.fgi(  49, 111):����������
      R_ix = R8_ox
C sens_init_1.fgi( 299, 249):��������
      R_ux = R8_abe
C sens_init_1.fgi( 299, 244):��������
      R_ebe = R8_ibe
C sens_init_1.fgi( 299, 239):��������
      R_obe = R8_ube
C sens_init_1.fgi( 299, 233):��������
      R_ade = R8_ede
C sens_init_1.fgi( 299, 229):��������
      R_ide = R8_ode
C sens_init_1.fgi( 299, 225):��������
      R_ude=R_afe
C sens_init_1.fgi( 312, 263):������,20FDB25CG001XQ01_input
      R_efe=R_ife
C sens_init_1.fgi( 312, 266):������,20FDB24CG001XQ01_input
      R_ofe=R_ufe
C sens_init_1.fgi( 312, 269):������,20FDB23CG001XQ01_input
      R_ake=R_eke
C sens_init_1.fgi( 312, 272):������,20FDB22CG001XQ01_input
      R_ike=R_oke
C sens_init_1.fgi( 312, 275):������,20FDB21CG001XQ01_input
      R_uke=R_ale
C sens_init_1.fgi( 312, 278):������,20FDB41CG001XQ01_input
      R_ele=R_ile
C sens_init_1.fgi( 312, 281):������,20FDA41CG001XQ01_input
      R_ole=R_ule
C sens_init_1.fgi( 312, 290):������,20KPA01CG001XQ01_input
      R_ame=R_eme
C sens_init_1.fgi( 312, 284):������,20KPA01CG003XQ01_input
      R_ime=R_ome
C sens_init_1.fgi( 312, 287):������,20KPA01CG002XQ01_input
      R_ume=R_ape
C sens_init_1.fgi( 235, 248):������,20FDA51CG001XQ01_input
      R_epe = R8_ipe
C sens_init_1.fgi( 211, 286):��������
      R_ope = R8_upe
C sens_init_1.fgi( 211, 290):��������
      R_ire = R8_ore
C sens_init_1.fgi( 380, 184):��������
      R_ose = R8_use
C sens_init_1.fgi( 380, 196):��������
      R_(13) = 13553
C sens_init_1.fgi( 380, 214):��������� (RE4) (�������)
      R_eve = R8_ive * R_(13)
C sens_init_1.fgi( 383, 216):����������
      R_(14) = 13553
C sens_init_1.fgi( 380, 220):��������� (RE4) (�������)
      R_ove = R8_uve * R_(14)
C sens_init_1.fgi( 383, 222):����������
      R_uxe = R8_abi
C sens_init_1.fgi( 299, 145):��������
      R_ebi = R8_ibi
C sens_init_1.fgi( 299, 156):��������
      R_obi = R8_ubi
C sens_init_1.fgi( 299, 160):��������
      R_adi = R8_edi
C sens_init_1.fgi( 299, 164):��������
      R_udi = R8_afi
C sens_init_1.fgi( 299, 172):��������
      R_(15) = 13553
C sens_init_1.fgi(  46,  70):��������� (RE4) (�������)
      R_iki = R8_oki * R_(15)
C sens_init_1.fgi(  49,  72):����������
      R_(16) = 13553
C sens_init_1.fgi(  46,  76):��������� (RE4) (�������)
      R_uki = R8_ali * R_(16)
C sens_init_1.fgi(  49,  78):����������
      R_efi = R8_ifi
C sens_init_1.fgi(  46,  58):��������
      R_aki = R8_eki
C sens_init_1.fgi(  46,  66):��������
      R_oli = R8_uli
C sens_init_1.fgi(  46, 101):��������
      R_umi = R8_api
C sens_init_1.fgi(  46, 115):��������
      R_epi = R8_ipi
C sens_init_1.fgi(  46, 119):��������
      End
